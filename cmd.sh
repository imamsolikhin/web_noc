# php artisan view:clear
# php artisan route:clear
# php artisan cache:clear
# php artisan config:clear
# php artisan clear-compiled
# composer dump-autoload
# php artisan migrate:refresh --seed
docker rm $(docker stop $(docker ps -a -q --filter name="web_noc" --format="{{.ID}}"))
docker build -t "web_noc" .
docker run -i -p 8000:80 --name="web_noc" -v $PWD:/app "web_noc"
