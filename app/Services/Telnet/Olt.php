<?php

namespace App\Services\Telnet;

use GuzzleHttp\Client;
use App\Services\Signature;
use Illuminate\Support\Facades\Log;

class Olt {

  public function checkAccess($host) {
      $url = env('IKB_NET_NOC').'/huawei/host/config';
      $url .= '?auth=all';
      $url .= '&config=check-host';
      $url .= '&hostcode='.$host->Code;
      $url .= '&hostname='.$host->IpAddress;
      $url .= '&username='.$host->Username;
      $url .= '&password='.$host->Password;
      $url .= '&lnprofile='.$host->ProfileId;

      $client = new Client(['http_errors' => false]);
      $response = $client->request('GET', $url);

      $contents = json_decode($response->getBody()->getContents());

      return $contents;
  }

  public function checkConfig($prm,$config = "all") {
      $url = env('IKB_NET_NOC') . '/checkhost?code=' . $prm.'&config='.$config;
      $client = new Client(['http_errors' => false]);
      $response = $client->request('GET', $url);

      $contents = json_decode($response->getBody()->getContents());

      return $contents;
  }

  public function clientstools($host,$clients,$config) {
      $url = env('IKB_NET_NOC').'/huawei/clients/tools';
      $url .= '?auth=all';
      $url .= '&config='.$config;
      $url .= '&hostcode='.$host->Code;
      $url .= '&hostname='.$host->IpAddress;
      $url .= '&username='.$host->Username;
      $url .= '&password='.$host->Password;
      $url .= '&lnprofile='.$host->ProfileId;
      $url .= '&frame='.$clients->FrameId;
      $url .= '&slot='.$clients->SlotId;
      $url .= '&port='.$clients->PortId;
      $url .= '&ont='.$clients->OntId;
      $client = new Client(['http_errors' => false]);
      $response = $client->request('GET', $url);

      $contents = json_decode($response->getBody()->getContents());

      return $contents;
  }

  public function autoFindAll() {
      $url = env('IKB_NET_NOC') . '/autofind';
      $client = new Client(['http_errors' => false]);
      $signature = (new Signature($url))->create();
      $response = $client->request('GET', $url);

      $contents = json_decode($response->getBody()->getContents());

      return $contents;
  }

  public function autoFindHost($host) {
      $url = env('IKB_NET_NOC') . '/autofind?code='.$host;
      $client = new Client(['http_errors' => false]);
      $signature = (new Signature($url))->create();
      $response = $client->request('GET', $url);

      $contents = json_decode($response->getBody()->getContents());

      return $contents;
  }

  public function checkPowerSn($host,$frame,$slot,$port,$ont,$sn,$config) {
      $url = env('IKB_NET_NOC').'/huawei/clients/tools';
      $url .= '?auth=all';
      $url .= '&config='.$config;
      $url .= '&hostcode='.$host->Code;
      $url .= '&hostname='.$host->IpAddress;
      $url .= '&username='.$host->Username;
      $url .= '&password='.$host->Password;
      $url .= '&frame='.$frame;
      $url .= '&slot='.$slot;
      $url .= '&port='.$port;
      $url .= '&ont='.$ont;
      $url .= '&sn='.$sn;
      // dd($url);
      $client = new Client(['http_errors' => false]);
      $response = $client->request('GET', $url);

      $contents = json_decode($response->getBody()->getContents());

      return $contents;
  }

  public function activation($url) {
      $url = env('IKB_NET_NOC') . '/huawei/clients/setting?'.$url;
      $client = new Client(['http_errors' => false]);
      $signature = (new Signature($url))->create();
      $response = $client->request('GET', $url);
      $contents = json_decode($response->getBody()->getContents());

      return $contents;
  }

  public function modifysn($url) {
      $url = env('IKB_NET_NOC') . '/activation?'.$url;
      $client = new Client(['http_errors' => false]);
      $signature = (new Signature($url))->create();
      $response = $client->request('GET', $url);

      $contents = json_decode($response->getBody()->getContents());

      return $contents;
  }

}
