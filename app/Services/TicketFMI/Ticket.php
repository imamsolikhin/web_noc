<?php

namespace App\Services\TicketFMI;

use GuzzleHttp\Client;
use App\Services\Signature;
use Illuminate\Support\Facades\Log;


class Ticket
{
	public function getAll($body = array())
	{
		$url =env('IKB_TCK_NOC') . '/fmi-ticket-open?'.http_build_query($body);
		$signature = (new Signature($url))->create();
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);
        $contents = json_decode($response->getBody()->getContents());
        return $contents;
	}

    public function detail($code = '')
    {
        $url =env('IKB_TCK_NOC') . '/fmi-ticket-open/'.$code;
        $signature = (new Signature($url))->create();
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);
        $contents = json_decode($response->getBody()->getContents());
        return $contents;
    }

    public function open_customer($code = '')
    {
        $url =env('IKB_TCK_NOC') . '/fmi-ticket-open-customer/'.$code;
        $signature = (new Signature($url))->create();
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);
        $contents = json_decode($response->getBody()->getContents());
        return $contents;
    }


    public function get_customer($code = '')
    {
        $url =env('IKB_MST_NOC') . '/customer-erp/'.$code;
        $signature = (new Signature($url))->create();
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);
        $contents = json_decode($response->getBody()->getContents());
        return $contents;
    }


    public function get_allBranch()
    {
        $url =env('IKB_MST_NOC').'/branch?limit=10000&page=1';
        $signature = (new Signature($url))->create();
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);
        $contents = json_decode($response->getBody()->getContents());
        return $contents;
    }


    public function get_allProblem()
    {
        $url =env('IKB_MST_NOC').'/problem-type?limit=1000&page=1';
        $signature = (new Signature($url))->create();
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);
        $contents = json_decode($response->getBody()->getContents());
        return $contents;
    }


    public function get_allDepartment()
    {
        $url =env('IKB_MST_NOC').'/department?limit=1000&page=1';
        $signature = (new Signature($url))->create();
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);
        $contents = json_decode($response->getBody()->getContents());
        return $contents;
    }
}
