<?php

namespace App\Services\Asset;

use GuzzleHttp\Client;
use App\Services\Signature;
use Illuminate\Support\Facades\Log;

class WhmInternalItemDetail {

    public function getAll($headercode) {
        $url = env('IKB_AST_NOC') . '/whm-internal-item-detail?limit=10000&page=1';
        $signature = (new Signature($url))->create();
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[AST IKB  NOC API - Get All WhmInternalItemDetail]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function getAllActive($headercode) {
        $url = env('IKB_AST_NOC') . '/whm-internal-item-detail?limit=10000&page=1&ActiveStatus=1&HeaderCode='.$headercode;
        $signature = (new Signature($url))->create();
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[AST IKB  NOC API - Get All WhmInternalItemDetail]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function show($code) {
        $url = env('IKB_AST_NOC') . '/whm-internal-item-detail/' . $code;
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[AST IKB  NOC API - Show WhmInternalItemDetail]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function post($data) {
        $url = env('IKB_AST_NOC') . '/whm-internal-item-detail';
        $client = new Client(['http_errors' => false]);
        $data = [
            'Code' => $data['Code'],
            'HeaderCode' => isset($data['HeaderCode']) ? $data['HeaderCode'] : null,
            'ItemCode' => isset($data['ItemCode']) ? $data['ItemCode'] : null,
            'Quantity' => isset($data['Quantity']) ? $data['Quantity'] : null,
            'Remark' => isset($data['Remark']) ? $data['Remark'] : null,
            'CreatedBy' => "admin",
            'CreatedDate' => date('Y-m-d H:i:s'),
        ];

        $response = $client->request('POST', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);
        // dd(json_decode($response->getBody()->getContents()));

        if ($response->getStatusCode() != 200) {
            Log::error("[AST IKB  NOC API - Add WhmInternalItemDetail]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

    public function put($code, $data) {
        $url = env('IKB_AST_NOC') . '/whm-internal-item-detail/' . $code;

        $client = new Client(['http_errors' => false]);
        $data = [
            'Code' => $data['Code'],
            'Headercode' => isset($data['Headercode']) ? $data['Headercode'] : null,
            'ItemCode' => isset($data['ItemCode']) ? $data['ItemCode'] : null,
            'Quantity' => isset($data['Quantity']) ? $data['Quantity'] : null,
            'Remark' => isset($data['Remark']) ? $data['Remark'] : null,
            'UpdatedBy' => 'admin',
            'UpdatedDate' => date('Y-m-d H:i:s'),
        ];

        $response = $client->request('PUT', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[AST IKB  NOC API - Update WhmInternalItemDetail]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

    public function delete($id) {
        $url = env('IKB_AST_NOC') . '/whm-internal-item-detail/' .$id.'?HeaderCode='.$id;
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('delete', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[AST IKB  NOC API - Deleted WhmInternalItemDetail]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

}
