<?php

namespace App\Services\Asset;

use GuzzleHttp\Client;
use App\Services\Signature;
use Illuminate\Support\Facades\Log;

class AdjustmentOutSerialNoDetail {

    public function getAll($headercode) {
        $url = env('IKB_AST_NOC') . '/adjustment-out-serial-no-detail?limit=10000&page=1';
        $signature = (new Signature($url))->create();
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[AST IKB  NOC API - Get All AdjustmentOutSerialNoDetail]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function getAllActive($headercode) {
        $url = env('IKB_AST_NOC') . '/adjustment-out-serial-no-detail?limit=10000&page=1&ActiveStatus=1&HeaderCode='.$headercode;
        $signature = (new Signature($url))->create();
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[AST IKB  NOC API - Get All AdjustmentOutSerialNoDetail]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
        dd(json_decode($response->getBody()->getContents()));
    }

    public function show($code) {
        $url = env('IKB_AST_NOC') . '/adjustment-out-serial-no-detail/' . $code;
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[AST IKB  NOC API - Show AdjustmentOutSerialNoDetail]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function post($data) {
        $url = env('IKB_AST_NOC') . '/adjustment-out-serial-no-detail';
        $client = new Client(['http_errors' => false]);
        $data = [
            'Code' => $data['Code'],
            'HeaderCode' => isset($data['HeaderCode']) ? $data['HeaderCode'] : null,
            'ItemCode' => isset($data['ItemCode']) ? $data['ItemCode'] : null,
            'SerialNo' => isset($data['SerialNo']) ? $data['SerialNo'] : null,
            'RemarkText' => isset($data['RemarkText']) ? $data['RemarkText'] : null,
            'RemarkDate' => isset($data['RemarkDate']) ? $data['RemarkDate'] : null,
            'RemarkQuantity' => isset($data['RemarkQuantity']) ? $data['RemarkQuantity'] : null,
            'LocationCode' => isset($data['LocationCode']) ? $data['LocationCode'] : null,
            'CreatedBy' => "admin",
            'CreatedDate' => date('Y-m-d H:i:s'),
        ];

        $response = $client->request('POST', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);
        // dd(json_decode($response->getBody()->getContents()));

        if ($response->getStatusCode() != 200) {
            Log::error("[AST IKB  NOC API - Add AdjustmentOutSerialNoDetail]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

    public function put($code, $data) {
        $url = env('IKB_AST_NOC') . '/adjustment-out-serial-no-detail/' . $code;

        $client = new Client(['http_errors' => false]);
        $data = [
            'Code' => $data['Code'],
            'HeaderCode' => isset($data['HeaderCode']) ? $data['HeaderCode'] : null,
            'ItemCode' => isset($data['ItemCode']) ? $data['ItemCode'] : null,
            'SerialNo' => isset($data['SerialNo']) ? $data['SerialNo'] : null,
            'RemarkText' => isset($data['RemarkText']) ? $data['RemarkText'] : null,
            'RemarkQuantity' => isset($data['RemarkQuantity']) ? $data['RemarkQuantity'] : null,
            'LocationCode' => isset($data['LocationCode']) ? $data['LocationCode'] : null,
            'UpdatedBy' => 'admin',
            'UpdatedDate' => date('Y-m-d H:i:s'),
        ];

        $response = $client->request('PUT', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[AST IKB  NOC API - Update AdjustmentOutSerialNoDetail]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

    public function delete($id) {
        $url = env('IKB_AST_NOC') . '/adjustment-out-serial-no-detail/' .$id.'?HeaderCode='.$id;
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('delete', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[AST IKB  NOC API - Deleted AdjustmentOutSerialNoDetail]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

}
