<?php

namespace App\Services\Asset;

use GuzzleHttp\Client;
use App\Services\Signature;
use Illuminate\Support\Facades\Log;

class WhmCustsnDetail {

    public function getAll($headercode) {
        $url = env('IKB_AST_NOC') . '/whm-custsn-detail?limit=10000&page=1';
        $signature = (new Signature($url))->create();
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[AST IKB  NOC API - Get All WhmCustsnDetail]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function getAllActive($headercode) {
        $url = env('IKB_AST_NOC') . '/whm-custsn-detail?limit=10000&page=1&ActiveStatus=1&HeaderCode='.$headercode;
        $signature = (new Signature($url))->create();
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[AST IKB  NOC API - Get All WhmCustsnDetail]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function show($code) {
        $url = env('IKB_AST_NOC') . '/whm-custsn-detail/' . $code;
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[AST IKB  NOC API - Show WhmCustsnDetail]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function post($data) {
        $url = env('IKB_AST_NOC') . '/whm-custsn-detail';
        $client = new Client(['http_errors' => false]);
        $data = [
            'Code' => $data['Code'],
            'HeaderCode' => isset($data['HeaderCode']) ? $data['HeaderCode'] : null,
            'ItemCode' => isset($data['ItemCode']) ? $data['ItemCode'] : null,
            'SerialNo' => isset($data['SerialNo']) ? $data['SerialNo'] : null,
            'InTransactionNo' => isset($data['InTransactionNo']) ? $data['InTransactionNo'] : null,
            'RemarkText' => isset($data['RemarkText']) ? $data['RemarkText'] : null,
            'RemarkDate' => isset($data['RemarkDate']) ? $data['RemarkDate'] : null,
            'RemarkQuantity' => isset($data['RemarkQuantity']) ? $data['RemarkQuantity'] : null,
            'LocationCode' => isset($data['LocationCode']) ? $data['LocationCode'] : null,
            'Remark' => isset($data['Remark']) ? $data['Remark'] : null,
            'CreatedBy' => "admin",
            'CreatedDate' => date('Y-m-d H:i:s'),
        ];

        $response = $client->request('POST', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);
        // dd(json_decode($response->getBody()->getContents()));

        if ($response->getStatusCode() != 200) {
            Log::error("[AST IKB  NOC API - Add WhmCustsnDetail]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

    public function put($code, $data) {
        $url = env('IKB_AST_NOC') . '/whm-custsn-detail/' . $code;

        $client = new Client(['http_errors' => false]);
        $data = [
            'Code' => $data['Code'],
            'HeaderCode' => isset($data['HeaderCode']) ? $data['HeaderCode'] : null,
            'ItemCode' => isset($data['ItemCode']) ? $data['ItemCode'] : null,
            'SerialNo' => isset($data['SerialNo']) ? $data['SerialNo'] : null,
            'InTransactionNo' => isset($data['InTransactionNo']) ? $data['InTransactionNo'] : null,
            'RemarkText' => isset($data['RemarkText']) ? $data['RemarkText'] : null,
            'RemarkDate' => isset($data['RemarkDate']) ? $data['RemarkDate'] : null,
            'RemarkQuantity' => isset($data['RemarkQuantity']) ? $data['RemarkQuantity'] : null,
            'LocationCode' => isset($data['LocationCode']) ? $data['LocationCode'] : null,
            'Remark' => isset($data['Remark']) ? $data['Remark'] : null,
            'UpdatedBy' => 'admin',
            'UpdatedDate' => date('Y-m-d H:i:s'),
        ];

        $response = $client->request('PUT', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[AST IKB  NOC API - Update WhmCustsnDetail]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

    public function delete($id) {
        $url = env('IKB_AST_NOC') . '/whm-custsn-detail/' .$id.'?HeaderCode='.$id;
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('delete', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[AST IKB  NOC API - Deleted WhmCustsnDetail]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

}
