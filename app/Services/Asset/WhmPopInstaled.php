<?php

namespace App\Services\Asset;

use GuzzleHttp\Client;
use App\Services\Signature;
use Illuminate\Support\Facades\Log;

class WhmPopInstaled {

    public function getAll() {
        $url = env('IKB_AST_NOC') . '/whm-pop-instaled?limit=10000&page=1';
        $signature = (new Signature($url))->create();
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[AST IKB  NOC API - Get All WhmPopInstaled]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function getAllActive() {
        $url = env('IKB_AST_NOC') . '/whm-pop-instaled?limit=10000&page=1&ActiveStatus=1';
        $signature = (new Signature($url))->create();
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[AST IKB  NOC API - Get All WhmPopInstaled]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function show($code) {
        $url = env('IKB_AST_NOC') . '/whm-pop-instaled/' . $code;
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[AST IKB  NOC API - Show WhmPopInstaled]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function post($data) {
        $url = env('IKB_AST_NOC') . '/whm-pop-instaled';
        $client = new Client(['http_errors' => false]);
        $data = [
            'Code' => isset($data['Code']) ? $data['Code'] : null,
            'BranchCode' => isset($data['BranchCode']) ? $data['BranchCode'] : null,
            'CurrencyCode' => isset($data['CurrencyCode']) ? $data['CurrencyCode'] : null,
            'CompanyCode' => isset($data['CompanyCode']) ? $data['CompanyCode'] : null,
            'ExchangeRate' => isset($data['ExchangeRate']) ? $data['ExchangeRate'] : null,
            'Transactiondate' => isset($data['Transactiondate']) ? $data['Transactiondate'] : null,
            'SourceWarehouseCode' => isset($data['SourceWarehouseCode']) ? $data['SourceWarehouseCode'] : null,
            'DestinationWarehouseCode' => isset($data['DestinationWarehouseCode']) ? $data['DestinationWarehouseCode'] : null,
            'RefNo' => isset($data['RefNo']) ? $data['RefNo'] : null,
            'Remark' => isset($data['Remark']) ? $data['Remark'] : null,
            'CreatedBy' => "admin",
            'CreatedDate' => date('Y-m-d H:i:s'),
        ];

        $response = $client->request('POST',  $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        // dd($data);
        // dd(json_decode($response->getBody()->getContents())->data);

        if ($response->getStatusCode() != 200) {
            Log::error("[AST IKB  NOC API - Add WhmPopInstaled]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
        return json_decode($response->getBody()->getContents());
    }

    public function put($code, $data) {
        $url = env('IKB_AST_NOC') . '/whm-pop-instaled/' . $code;

        $client = new Client(['http_errors' => false]);
        $data = [
            'Code' => isset($data['Code']) ? $data['Code'] : null,
            'BranchCode' => isset($data['BranchCode']) ? $data['BranchCode'] : null,
            'CurrencyCode' => isset($data['CurrencyCode']) ? $data['CurrencyCode'] : null,
            'CompanyCode' => isset($data['CompanyCode']) ? $data['CompanyCode'] : null,
            'ExchangeRate' => isset($data['ExchangeRate']) ? $data['ExchangeRate'] : null,
            'Transactiondate' => isset($data['Transactiondate']) ? $data['Transactiondate'] : null,
            'SourceWarehouseCode' => isset($data['SourceWarehouseCode']) ? $data['SourceWarehouseCode'] : null,
            'DestinationWarehouseCode' => isset($data['DestinationWarehouseCode']) ? $data['DestinationWarehouseCode'] : null,
            'RefNo' => isset($data['RefNo']) ? $data['RefNo'] : null,
            'Remark' => isset($data['Remark']) ? $data['Remark'] : null,
            'UpdatedBy' => 'admin',
            'UpdatedDate' => date('Y-m-d H:i:s'),
        ];

        $response = $client->request('PUT', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Update WhmPopInstaled]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
        return json_decode($response->getBody()->getContents())->data;
    }

    public function delete($code) {
        $url = env('IKB_AST_NOC') . '/whm-pop-instaled/' . $code;
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('delete', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[AST IKB  NOC API - Deleted WhmPopInstaled]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

}
