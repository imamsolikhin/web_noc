<?php

namespace App\Services\Olt;

use GuzzleHttp\Client;
use App\Services\Signature;
use Illuminate\Support\Facades\Log;

class SignalFormula {

    public function getAll($request) {
        $search = ($request->search) ? $request->search['value']:null;
        $from_date = ($request->from_date) ? $request->from_date:null;
        $to_date = ($request->to_date) ? $request->to_date:null;
        $limit = ($request->length) ? $request->length:intval(DT_LIMIT);
        $page = ($request->length) ? (int) ($request->start/$request->length)+1:1;
        $url = env('IKB_OLT_NOC') . '/signal-formula?limit='.$limit.'&page='.$page.'&from_date='.$from_date.'&to_date='.$to_date.'&search='.$search;
        $signature = (new Signature($url))->create();
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Get All SignalFormula]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function getAllActive() {
        $url = env('IKB_OLT_NOC') . '/signal-formula?limit=10000&page=1&ActiveStatus=1';
        $signature = (new Signature($url))->create();
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Get All SignalFormula]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function show($code) {
        $url = env('IKB_OLT_NOC') . '/signal-formula/' . $code;
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Show SignalFormula]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function post($data) {
        $url = env('IKB_OLT_NOC') . '/signal-formula';
        $client = new Client(['http_errors' => false]);
        $data = [
            'Code' => $data['Code'],
            'Formula' => isset ($data['Formula']) ? $data['Formula'] : null,
            'TxPower' => isset ($data['TxPower']) ? $data['TxPower'] : null,
            'Distance' => isset ($data['Distance']) ? $data['Distance'] : null,
            'LossDb' => isset ($data['LossDb']) ? $data['LossDb'] : null,
            'LossFixTop' => isset ($data['LossFixTop']) ? $data['LossFixTop'] : null,
            'LossFixBot' => isset ($data['LossFixBot']) ? $data['LossFixBot'] : null,
            'ActiveStatus' => isset ($data['ActiveStatus']) ? $data['ActiveStatus'] : null,
            'CreatedBy' => "admin",
            'CreatedDate' => date('Y-m-d H:i:s'),
        ];

        $response = $client->request('POST', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Add SignalFormula]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

    public function post_list($datas) {
        $url = env('IKB_OLT_NOC') . '/signal-formula?list=all';
        $client = new Client(['http_errors' => false]);
        $list = array();
        foreach ($datas as $data_val) {
            $dt = new \stdClass();
            $header= $data_val->HostCode;

            $datas->Code = $data_val[0];
            $datas->Formula = "";
            $datas->TxPower = 0;
            $datas->Distance = "";
            $datas->LossDb = "";
            $datas->LossFixTop = 0;
            $datas->LossFixBot = 0;
            $datas->ActiveStatus = 1;
            $datas->CreatedBy = "admin";
            $datas->CreatedDate = date('Y-m-d H:i:s');
            $datas->UpdatedBy = "admin";
            $datas->UpdatedDate = date('Y-m-d H:i:s');
            array_push($list,$dt);
        }
        $data["data"] = $datas;
        $response = $client->request('POST',  $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        // dd($data);
        // dd(json_decode($response->getBody()->getContents()));

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Add SignalFormula]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
        return json_decode($response->getBody()->getContents());
    }

    public function put($code, $data) {
        $url = env('IKB_OLT_NOC') . '/signal-formula/' . $code;

        $client = new Client(['http_errors' => false]);
        $data = [
            'Code' => $data['Code'],
            'Formula' => isset ($data['Formula']) ? $data['Formula'] : null,
            'TxPower' => isset ($data['TxPower']) ? $data['TxPower'] : '0.00',
            'Distance' => isset ($data['Distance']) ? $data['Distance'] : '0.00',
            'LossDb' => isset ($data['LossDb']) ? $data['LossDb'] : '0.00',
            'LossFixTop' => isset ($data['LossFixTop']) ? $data['LossFixTop'] : '0.00',
            'LossFixBot' => isset ($data['LossFixBot']) ? $data['LossFixBot'] : '0.00',
            'ActiveStatus' => isset ($data['ActiveStatus']) ? (int)$data['ActiveStatus'] : null,
            'UpdatedBy' => "admin",
            'UpdatedDate' => date('Y-m-d H:i:s'),
        ];

        $response = $client->request('PUT', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Update SignalFormula]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

    public function delete($code) {
        $url = env('IKB_OLT_NOC') . '/signal-formula/' . $code;
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('delete', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Deleted SignalFormula]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

}
