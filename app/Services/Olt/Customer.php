<?php

namespace App\Services\Olt;

use GuzzleHttp\Client;
use App\Services\Signature;
use Illuminate\Support\Facades\Log;

class Customer {

    public function getAll($request) {
        $search = ($request->search) ? $request->search['value']:null;
        $from_date = ($request->from_date) ? $request->from_date:null;
        $to_date = ($request->to_date) ? $request->to_date:null;
        $limit = ($request->length) ? $request->length:intval(global_limit());
        $page = ($request->length) ? (int) ($request->start/$request->length)+1:1;
        $url = env('IKB_OLT_NOC') . '/customer?limit='.$limit.'&page='.$page.'&from_date='.$from_date.'&to_date='.$to_date.'&search='.$search;
        $signature = (new Signature($url))->create();
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Get All Customer]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function getAllActive() {
        $url = env('IKB_OLT_NOC') . '/customer?limit=10000&page=1&ActiveStatus=1';
        $signature = (new Signature($url))->create();
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Get All Customer]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function show($code) {
        $url = env('IKB_OLT_NOC') . '/customer/' . $code;
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Show Customer]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function post($data) {
        $url = env('IKB_OLT_NOC') . '/customer';
        $client = new Client(['http_errors' => false]);
        $data = [
            'Code' => isset($data['Code']) ? $data['Code'] : null,
            'Name' => isset($data['Name']) ? $data['Name'] : null,
            'LinkID' => isset($data['LinkID']) ? $data['LinkID'] : null,
            'MemberID' => isset($data['MemberID']) ? $data['MemberID'] : null,
            'JobTitle' => isset($data['JobTitle']) ? $data['JobTitle'] : null,
            'ContactPerson' => isset($data['ContactPerson']) ? $data['ContactPerson'] : null,
            'Email' => isset($data['Email']) ? $data['Email'] : null,
            'Phone1' => isset($data['Phone1']) ? $data['Phone1'] : null,
            'Phone2' => isset($data['Phone2']) ? $data['Phone2'] : null,
            'PhoneNumber' => isset($data['PhoneNumber']) ? $data['PhoneNumber'] : null,
            'NPWP' => isset($data['NPWP']) ? $data['NPWP'] : null,
            'Fax' => isset($data['Fax']) ? $data['Fax'] : null,
            'Address' => isset($data['Address']) ? $data['Address'] : null,
            'CityCode' => isset($data['CityCode']) ? $data['CityCode'] : null,
            'City' => isset($data['City']) ? $data['City'] : null,
            'Province' => isset($data['Province']) ? $data['Province'] : null,
            'ZipCode' => isset($data['ZipCode']) ? $data['ZipCode'] : null,
            'Website' => isset($data['Website']) ? $data['Website'] : null,
            'IDCard' => isset($data['IDCard']) ? $data['IDCard'] : null,
            'IDCardNumber' => isset($data['IDCardNumber']) ? $data['IDCardNumber'] : null,
            'IDCardExpired' => isset($data['IDCardExpired']) ? $data['IDCardExpired'] : null,
            'RegistrationDate' => isset($data['RegistrationDate']) ? $data['RegistrationDate'] : null,
            'ProductCode' => isset($data['ProductCode']) ? $data['ProductCode'] : null,
            'ClientStatus' => isset($data['ClientStatus']) ? $data['ClientStatus'] : null,
            'ProductSite' => isset($data['ProductSite']) ? $data['ProductSite'] : null,
            'ProductGrup' => isset($data['ProductGrup']) ? $data['ProductGrup'] : null,
            'ProductPort' => isset($data['ProductPort']) ? $data['ProductPort'] : null,
            'ProductCategory' => isset($data['ProductCategory']) ? $data['ProductCategory'] : null,
            'BaseTransmissionStationCode' => isset($data['BaseTransmissionStationCode']) ? $data['BaseTransmissionStationCode'] : null,
            'ProductName' => isset($data['ProductName']) ? $data['ProductName'] : null,
            'ProductNotes' => isset($data['ProductNotes']) ? $data['ProductNotes'] : null,
            'ActiveStatus' => isset($data['ActiveStatus']) ? $data['ActiveStatus'] : null,
            'CreatedBy' => "admin",
            'CreatedDate' => date('Y-m-d H:i:s'),
        ];

        $response = $client->request('POST', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Add Customer]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

    public function post_list($datas) {
        $url = env('IKB_OLT_NOC') . '/customer?list=all';
        $client = new Client(['http_errors' => false]);
        $data["data"] = $datas;
        $response = $client->request('POST',  $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);


        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Add Customer]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
        return json_decode($response->getBody()->getContents());
    }

    public function put($code, $data) {
        $url = env('IKB_OLT_NOC') . '/customer/' . $code;

        $client = new Client(['http_errors' => false]);
        $data = [
            'Code' => isset($data['Code']) ? $data['Code'] : null,
            'Name' => isset($data['Name']) ? $data['Name'] : null,
            'LinkID' => isset($data['LinkID']) ? $data['LinkID'] : null,
            'MemberID' => isset($data['MemberID']) ? $data['MemberID'] : null,
            'JobTitle' => isset($data['JobTitle']) ? $data['JobTitle'] : null,
            'ContactPerson' => isset($data['ContactPerson']) ? $data['ContactPerson'] : null,
            'Email' => isset($data['Email']) ? $data['Email'] : null,
            'Phone1' => isset($data['Phone1']) ? $data['Phone1'] : null,
            'Phone2' => isset($data['Phone2']) ? $data['Phone2'] : null,
            'PhoneNumber' => isset($data['PhoneNumber']) ? $data['PhoneNumber'] : null,
            'NPWP' => isset($data['NPWP']) ? $data['NPWP'] : null,
            'Fax' => isset($data['Fax']) ? $data['Fax'] : null,
            'Address' => isset($data['Address']) ? $data['Address'] : null,
            'CityCode' => isset($data['CityCode']) ? $data['CityCode'] : null,
            'City' => isset($data['City']) ? $data['City'] : null,
            'Province' => isset($data['Province']) ? $data['Province'] : null,
            'ZipCode' => isset($data['ZipCode']) ? $data['ZipCode'] : null,
            'Website' => isset($data['Website']) ? $data['Website'] : null,
            'IDCard' => isset($data['IDCard']) ? $data['IDCard'] : null,
            'IDCardNumber' => isset($data['IDCardNumber']) ? $data['IDCardNumber'] : null,
            'IDCardExpired' => isset($data['IDCardExpired']) ? $data['IDCardExpired'] : null,
            'RegistrationDate' => isset($data['RegistrationDate']) ? $data['RegistrationDate'] : null,
            'ProductCode' => isset($data['ProductCode']) ? $data['ProductCode'] : null,
            'ClientStatus' => isset($data['ClientStatus']) ? $data['ClientStatus'] : null,
            'ProductSite' => isset($data['ProductSite']) ? $data['ProductSite'] : null,
            'ProductGrup' => isset($data['ProductGrup']) ? $data['ProductGrup'] : null,
            'ProductPort' => isset($data['ProductPort']) ? $data['ProductPort'] : null,
            'ProductCategory' => isset($data['ProductCategory']) ? $data['ProductCategory'] : null,
            'BaseTransmissionStationCode' => isset($data['BaseTransmissionStationCode']) ? $data['BaseTransmissionStationCode'] : null,
            'ProductName' => isset($data['ProductName']) ? $data['ProductName'] : null,
            'ProductNotes' => isset($data['ProductNotes']) ? $data['ProductNotes'] : null,
            'ActiveStatus' => isset($data['ActiveStatus']) ? $data['ActiveStatus'] : null,
            'UpdatedBy' => "admin",
            'UpdatedDate' => date('Y-m-d H:i:s'),
        ];

        $response = $client->request('PUT', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);
        // dd($response);
        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Update Customer]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

    public function delete($code) {
        $url = env('IKB_OLT_NOC') . '/customer/' . $code;
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('delete', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Deleted Customer]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

}
