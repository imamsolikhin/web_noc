<?php

namespace App\Services\Olt;

use GuzzleHttp\Client;
use App\Services\Signature;
use Illuminate\Support\Facades\Log;

class GemProfile {

    public function getAll() {
        $url = env('IKB_OLT_NOC') . '/gem-profile?limit=10000&page=1';
        $signature = (new Signature($url))->create();
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Get All GemProfile]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function getAllActive($code) {
        $url = env('IKB_OLT_NOC') . '/gem-profile?limit=10000&page=1&ActiveStatus=1&HostCode='.$code;
        $signature = (new Signature($url))->create();
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Get All GemProfile]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function show($code) {
        $url = env('IKB_OLT_NOC') . '/gem-profile/' . $code;
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Show GemProfile]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function post($data) {
        $url = env('IKB_OLT_NOC') . '/gem-profile';
        $client = new Client(['http_errors' => false]);
        $data = [
            'Code' => $data['Code'],
            'HostCode' => isset ($data['HostCode']) ? $data['HostCode'] : null,
            'ProfileId' => isset ($data['ProfileId']) ? $data['ProfileId'] : null,
            'GemId' => isset ($data['GemId']) ? $data['GemId'] : null,
            'GemMapping' => isset ($data['GemMapping']) ? $data['GemMapping'] : null,
            'GemVlan' => isset ($data['GemVlan']) ? $data['GemVlan'] : null,
            'Priority' => isset ($data['Priority']) ? $data['Priority'] : null,
            'Type' => isset ($data['Type']) ? $data['Type'] : null,
            'Port' => isset ($data['Port']) ? $data['Port'] : null,
            'Bundle' => isset ($data['Bundle']) ? $data['Bundle'] : null,
            'Flow' => isset ($data['Flow']) ? $data['Flow'] : null,
            'Transparent' => isset ($data['Transparent']) ? $data['Transparent'] : null,
            'ActiveStatus' => isset ($data['ActiveStatus']) ? $data['ActiveStatus'] : null,
            'CreatedBy' => "admin",
            'CreatedDate' => date('Y-m-d H:i:s'),
        ];

        $response = $client->request('POST', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Add GemProfile]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

    public function post_list($datas) {
        $url = env('IKB_OLT_NOC') . '/gem-profile?list=all';
        $client = new Client(['http_errors' => false]);
        $list = array();
        $i = 1;
        foreach ($datas as $data_val) {
            $dt = new \stdClass();
            $dt->Code = $data_val->host.'-'. $data_val->profile.'-'.($i++);
            $dt->HostCode = $data_val->host;
            $dt->ProfileId = $data_val->profile;
            $dt->GemId = $data_val->gem;
            $dt->GemMapping =  $data_val->mapping;
            $dt->GemVlan =  $data_val->vlan;
            $dt->Priority =  $data_val->priority;
            $dt->Type =  $data_val->type;
            $dt->Port =  (int)$data_val->port;
            $dt->Bundle =  $data_val->bundle;
            $dt->Flow =  $data_val->flow;
            $dt->Transparent =  $data_val->transparent;
            $dt->ActiveStatus =  1;
            $dt->CreatedBy = "admin";
            $dt->CreatedDate = date('Y-m-d H:i:s');
            $dt->UpdatedBy = "admin";
            $dt->UpdatedDate = date('Y-m-d H:i:s');
            $dt->InActiveBy = "admin";
            $dt->InActiveDate = date('Y-m-d H:i:s');
            array_push($list,$dt);
        }
        $data["data"] = $list;
        $response = $client->request('POST',  $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        // dd($data);
        // dd(json_decode($response->getBody()->getContents()));

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Add GemProfile]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
        return json_decode($response->getBody()->getContents());
    }

    public function put($code, $data) {
        $url = env('IKB_OLT_NOC') . '/gem-profile/' . $code;

        $client = new Client(['http_errors' => false]);
        $data = [
            'Code' => $data['Code'],
            'HostCode' => isset ($data['HostCode']) ? $data['HostCode'] : null,
            'ProfileId' => isset ($data['ProfileId']) ? $data['ProfileId'] : null,
            'GemId' => isset ($data['GemId']) ? $data['GemId'] : null,
            'GemMapping' => isset ($data['GemMapping']) ? $data['GemMapping'] : null,
            'GemVlan' => isset ($data['GemVlan']) ? $data['GemVlan'] : null,
            'Priority' => isset ($data['Priority']) ? $data['Priority'] : null,
            'Type' => isset ($data['Type']) ? $data['Type'] : null,
            'Port' => isset ($data['Port']) ? $data['Port'] : null,
            'Bundle' => isset ($data['Bundle']) ? $data['Bundle'] : null,
            'Flow' => isset ($data['Flow']) ? $data['Flow'] : null,
            'Transparent' => isset ($data['Transparent']) ? $data['Transparent'] : null,
            'ActiveStatus' => isset ($data['ActiveStatus']) ? $data['ActiveStatus'] : null,
            'UpdatedBy' => "admin",
            'UpdatedDate' => date('Y-m-d H:i:s'),
        ];

        $response = $client->request('PUT', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Update GemProfile]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

    public function delete_host($code) {
        $url = env('IKB_OLT_NOC') . '/gem-profile/' . $code. '?HostCode='.$code;
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('delete', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Deleted GemProfile]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

    public function delete($code) {
        $url = env('IKB_OLT_NOC') . '/gem-profile/' . $code;
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('delete', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Deleted GemProfile]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

}
