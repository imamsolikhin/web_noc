<?php

namespace App\Services\Olt;

use GuzzleHttp\Client;
use App\Services\Signature;
use Illuminate\Support\Facades\Log;

class OntPortState {

    public function getAll($hostcode) {
        $url = env('IKB_OLT_NOC') . '/ont-port-state?limit=10000&page=1&HostCode='.$hostcode;
        $signature = (new Signature($url))->create();
        $clientService = new Client(['http_errors' => false]);
        $response = $clientService->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Get All OntPortState]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function getAllActive($hostcode) {
        $url = env('IKB_OLT_NOC') . '/ont-port-state?limit=10000&page=1&ActiveStatus=1&HostCode='.$hostcode;
        $signature = (new Signature($url))->create();
        $clientService = new Client(['http_errors' => false]);
        $response = $clientService->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Get All OntPortState]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function show($code) {
        $url = env('IKB_OLT_NOC') . '/ont-port-state/' . $code;
        $clientService = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $clientService->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Show OntPortState]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function post($clients, $data) {
        $url = env('IKB_OLT_NOC') . '/ont-port-state';
        // if (!$data){
        //     $clientService = new Client(['http_errors' => false]);
        //     $model = [
        //         'Code' => isset($clients->Code) ? $clients->Code."-".$port : null,
        //         'CustomerCode' => isset($clients->Code) ? $clients->Code : null,
        //         'HostCode' => isset($clients->HostCode) ? $clients->HostCode : null,
        //         'FrameId' => isset($clients->FrameId) ? $clients->FrameId : null,
        //         'SlotId' => isset($clients->SlotId) ? $clients->SlotId : null,
        //         'PortId' => isset($clients->PortId) ? $clients->PortId : null,
        //         'OntId' => isset($clients->OntId) ? $clients->OntId : null,
        //         'OntSn' => isset($clients->OntSn) ? $clients->OntSn : null,
        //         'OntPortID' => isset($data->port_id) ? $data->port_id : null,
        //         'OntPortType' => isset($data->port_type) ? $data->port_type : null,
        //         'Speed' => isset($data->speed) ? $data->speed : null,
        //         'Duplex' => isset($data->duplex) ? $data->duplex : null,
        //         'LinkState' => isset($data->link_state) ? $data->link_state : null,
        //         'RingStatus' => isset($data->ring_status) ? $data->ring_status : null,
        //         'ActiveStatus' => 1,
        //         'CreatedBy' => "admin",
        //         'CreatedDate' => date('Y-m-d H:i:s'),
        //     ];
        //
        //     $response = $clientService->request('POST', $url, [
        //         'headers' => [
        //             'Accept' => 'application/json',
        //             'Content-Type' => 'application/json',
        //             'Signature' => (new Signature(json_encode($model)))->create()
        //         ],
        //         'json' => $model
        //     ]);
        //
        //     if ($response->getStatusCode() != 200) {
        //         Log::error("[OLT IKB  NOC API - Add OntPortState]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        //     }
        // }
        $i = 1;
        foreach ($data as $datas) {
          if ($datas != "" AND $datas != "-") {
            $clientService = new Client(['http_errors' => false]);
            $model = [
                'Code' => isset($clients->Code) ? $clients->Code."-".$i : null,
                'CustomerCode' => isset($clients->Code) ? $clients->Code : null,
                'HostCode' => isset($clients->HostCode) ? $clients->HostCode : null,
                'FrameId' => isset($clients->FrameId) ? $clients->FrameId : null,
                'SlotID' => isset($clients->SlotID) ? $clients->SlotID : null,
                'PortId' => isset($clients->PortId) ? $clients->PortId : null,
                'OntId' => isset($clients->OntId) ? $clients->OntId : null,
                'OntSn' => isset($clients->OntSn) ? $clients->OntSn : null,

                'OntPortID' => isset($datas->port_id) ? $datas->port_id : null,
                'OntPortType' => isset($datas->port_type) ? $datas->port_type : null,
                'Speed' => isset($datas->speed) ? $datas->speed : null,
                'Duplex' => isset($datas->duplex) ? $datas->duplex : null,
                'LinkState' => isset($datas->link_state) ? $datas->link_state : null,
                'RingStatus' => isset($datas->ring_status) ? $datas->ring_status : null,
                'ActiveStatus' => 1,
                'CreatedBy' => "admin",
                'CreatedDate' => date('Y-m-d H:i:s'),
            ];
            $i++;

            $response = $clientService->request('POST', $url, [
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json',
                    'Signature' => (new Signature(json_encode($model)))->create()
                ],
                'json' => $model
            ]);

            if ($response->getStatusCode() != 200) {
                Log::error("[OLT IKB  NOC API - Add OntPortState]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
            }
          }
        }
        return json_decode($response->getBody()->getContents());
    }

    public function put($code, $data) {
        // Step Simpan DB
        $url = env('IKB_OLT_NOC') . '/ont-port-state/' . $code;
        $clientService = new Client(['http_errors' => false]);
        $data = [
            'Code' => $data['Code'],
            'CustomerCode' => isset($data['CustomerCode']) ? $data['CustomerCode'] : null,
            'HostCode' => isset($data['HostCode']) ? $data['HostCode'] : null,
            'FrameId' => isset($data['FrameId']) ? $data['FrameId'] : null,
            'SlotID' => isset($data['SlotID']) ? $data['SlotID'] : null,
            'PortId' => isset($data['PortId']) ? $data['PortId'] : null,
            'OntId' => isset($data['OntId']) ? $data['OntId'] : null,
            'OntSn' => isset($data['OntSn']) ? $data['OntSn'] : null,
            'OntPortID' => isset($data['OntPortID']) ? $data['OntPortID'] : null,
            'OntPortType' => isset($data['OntPortType']) ? $data['OntPortType'] : null,
            'Speed' => isset($data['Speed']) ? $data['Speed'] : null,
            'Duplex' => isset($data['Duplex']) ? $data['Duplex'] : null,
            'LinkState' => isset($data['LinkState']) ? $data['LinkState'] : null,
            'RingStatus' => isset($data['RingStatus']) ? $data['RingStatus'] : null,
            'ActiveStatus' => 1,
            'UpdatedBy' => 'admin',
            'UpdatedDate' => date('Y-m-d H:i:s'),
        ];

        $response = $clientService->request('PUT', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Update OntPortState]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
        return json_decode($response->getBody()->getContents());
    }

    public function delete_host($code) {
        $url = env('IKB_OLT_NOC') . '/ont-port-state/' . $code. '?HostCode='.$code;
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('delete', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Deleted OntPortState]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

    public function delete($code,$customerCode=null) {
        $url = env('IKB_OLT_NOC') . '/ont-port-state/' . $code.'?CustomerCode='.$customerCode;
        $clientService = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $clientService->request('delete', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Deleted OntPortState]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

}
