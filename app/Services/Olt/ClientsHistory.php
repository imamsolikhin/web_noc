<?php

namespace App\Services\Olt;

use GuzzleHttp\Client;
use App\Services\Signature;
use Illuminate\Support\Facades\Log;

class ClientsHistory {

    public function getAll($request) {
        $url = env('IKB_OLT_NOC') . '/clients-history?limit=100000&page=1&from_date='.$request->from_date.'&to_date='.$request->to_date;
        $signature = (new Signature($url))->create();
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Get All ClientsHistory]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function getAllActive($customercode) {
        $url = env('IKB_OLT_NOC') . '/clients-history?limit=10000&page=1&ActiveStatus=1&CustomerCode='.$customercode;
        $signature = (new Signature($url))->create();
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);
        // dd(json_decode($response->getBody()->getContents())->data);
        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Get All ClientsHistory]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function show($code) {
        $url = env('IKB_OLT_NOC') . '/clients-history/' . $code;
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Show ClientsHistory]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function post($data) {
        $url = env('IKB_OLT_NOC') . '/clients-history';
        $client = new Client(['http_errors' => false]);
        // $data = [
        //     'Code' => isset($data['Code']) ? $data['Code'] : null,
        //     'IspCode' => isset($data['IspCode']) ? $data['IspCode'] : null,
        //     'HostCode' => isset($data['HostCode']) ? $data['HostCode'] : null,
        //     'CustomerCode' => isset($data['CustomerCode']) ? $data['CustomerCode'] : null,
        //     'CustomerName' => isset($data['CustomerName']) ? $data['CustomerName'] : null,
        //     'CustomerContactPerson' => isset($data['CustomerContactPerson']) ? $data['CustomerContactPerson'] : null,
        //     'CustomerAddress' => isset($data['CustomerAddress']) ? $data['CustomerAddress'] : null,
        //     'CustomerCity' => isset($data['CustomerCity']) ? $data['CustomerCity'] : null,
        //     'CustomerPhone1' => isset($data['CustomerPhone1']) ? $data['CustomerPhone1'] : null,
        //     'CustomerPhone2' => isset($data['CustomerPhone2']) ? $data['CustomerPhone2'] : null,
        //     'CustomerFax' => isset($data['CustomerFax']) ? $data['CustomerFax'] : null,
        //     'CustomerEmail' => isset($data['CustomerEmail']) ? $data['CustomerEmail'] : null,
        //     'CustomerType' => isset($data['CustomerType']) ? $data['CustomerType'] : null,
        //     'TemplateCode' => isset($data['TemplateCode']) ? $data['TemplateCode'] : null,
        //     'VlanDownLink' => isset($data['VlanDownLink']) ? $data['VlanDownLink'] : null,
        //     'FrameId' => isset($data['FrameId']) ? $data['FrameId'] : null,
        //     'SlotId' => isset($data['SlotId']) ? $data['SlotId'] : null,
        //     'PortId' => isset($data['PortId']) ? $data['PortId'] : null,
        //     'OntSn' => isset($data['Sn']) ? $data['Sn'] : null,
        //     'OntLineProfileId' => isset($data['OntLineProfileId']) ? $data['OntLineProfileId'] : null,
        //     'OntSrvProfileId' => isset($data['OntSrvProfileId']) ? $data['OntSrvProfileId'] : null,
        //     'VlanId' => isset($data['VlanId']) ? $data['VlanId'] : null,
        //     'VlanFrameId' => isset($data['VlanFrameId']) ? $data['VlanFrameId'] : null,
        //     'VlanSlotId' => isset($data['VlanSlotId']) ? $data['VlanSlotId'] : null,
        //     'VlanPortId' => isset($data['VlanPortId']) ? $data['VlanPortId'] : null,
        //     'FdtNo' => isset($data['FdtNo']) ? $data['FdtNo'] : null,
        //     'FatNo' => isset($data['FatNo']) ? $data['FatNo'] : null,
        //     'VlanAttribut' => isset($data['VlanAttribut']) ? $data['VlanAttribut'] : null,
        //     'NativeVlanEth1' => isset($data['NativeVlanEth1']) ? $data['NativeVlanEth1'] : null,
        //     'NativeVlanEth2' => isset($data['NativeVlanEth2']) ? $data['NativeVlanEth2'] : null,
        //     'NativeVlanEth3' => isset($data['NativeVlanEth3']) ? $data['NativeVlanEth3'] : null,
        //     'NativeVlanEth4' => isset($data['NativeVlanEth4']) ? $data['NativeVlanEth4'] : null,
        //     'ClientsHistorytatus' => "Active",
        //     'Remark' => isset($data['Remark']) ? $data['Remark'] : null,
        //     'CreatedBy' => "admin",
        //     'CreatedDate' => date('Y-m-d H:i:s'),
        // ];

        $response = $client->request('POST', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Add ClientsHistory]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
        return json_decode($response->getBody()->getContents());
    }

    public function post_list($datas) {
        $url = env('IKB_OLT_NOC') . '/clients-history?list=all';
        $client = new Client(['http_errors' => false]);
        $data["data"] = $datas;
        $response = $client->request('POST',  $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);
        
        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Add ClientsHistory]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
        return json_decode($response->getBody()->getContents());
    }

    public function put($code, $data) {
        // Step Simpan DB
        $url = env('IKB_OLT_NOC') . '/clients-history/' . $code;
        $client = new Client(['http_errors' => false]);
        $data = [
            'Code' => isset($data['Code']) ? $data['Code'] : null,
            'IspCode' => isset($data['IspCode']) ? $data['IspCode'] : null,
            'HostCode' => isset($data['HostCode']) ? $data['HostCode'] : null,
            'CustomerCode' => isset($data['CustomerCode']) ? $data['CustomerCode'] : null,
            'CustomerName' => isset($data['CustomerName']) ? $data['CustomerName'] : null,
            'CustomerContactPerson' => isset($data['CustomerContactPerson']) ? $data['CustomerContactPerson'] : null,
            'CustomerAddress' => isset($data['CustomerAddress']) ? $data['CustomerAddress'] : null,
            'CustomerCity' => isset($data['CustomerCity']) ? $data['CustomerCity'] : null,
            'CustomerPhone1' => isset($data['CustomerPhone1']) ? $data['CustomerPhone1'] : null,
            'CustomerPhone2' => isset($data['CustomerPhone2']) ? $data['CustomerPhone2'] : null,
            'CustomerFax' => isset($data['CustomerFax']) ? $data['CustomerFax'] : null,
            'CustomerEmail' => isset($data['CustomerEmail']) ? $data['CustomerEmail'] : null,
            'CustomerType' => isset($data['CustomerType']) ? $data['CustomerType'] : null,
            'TemplateCode' => isset($data['TemplateCode']) ? $data['TemplateCode'] : null,
            'VlanDownLink' => isset($data['VlanDownLink']) ? $data['VlanDownLink'] : null,
            'FrameId' => isset($data['FrameId']) ? $data['FrameId'] : null,
            'SlotId' => isset($data['SlotId']) ? $data['SlotId'] : null,
            'PortId' => isset($data['PortId']) ? $data['PortId'] : null,
            'OntVersion' => isset($data['OntVersion']) ? $data['OntVersion'] : null,
            'OntSoftware' => isset($data['OntSoftware']) ? $data['OntSoftware'] : null,
            'OntSn' => isset($data['Sn']) ? $data['Sn'] : null,
            'OntLineProfileId' => isset($data['OntLineProfileId']) ? $data['OntLineProfileId'] : null,
            'OntSrvProfileId' => isset($data['OntSrvProfileId']) ? $data['OntSrvProfileId'] : null,
            'VlanId' => isset($data['VlanId']) ? $data['VlanId'] : null,
            'VlanFrameId' => isset($data['VlanFrameId']) ? $data['VlanFrameId'] : null,
            'VlanSlotId' => isset($data['VlanSlotId']) ? $data['VlanSlotId'] : null,
            'VlanPortId' => isset($data['VlanPortId']) ? $data['VlanPortId'] : null,
            'FdtNo' => isset($data['FdtNo']) ? $data['FdtNo'] : null,
            'FatNo' => isset($data['FatNo']) ? $data['FatNo'] : null,
            'VlanAttribut' => isset($data['VlanAttribut']) ? $data['VlanAttribut'] : null,
            'NativeVlanEth1' => isset($data['NativeVlanEth1']) ? $data['NativeVlanEth1'] : null,
            'NativeVlanEth2' => isset($data['NativeVlanEth2']) ? $data['NativeVlanEth2'] : null,
            'NativeVlanEth3' => isset($data['NativeVlanEth3']) ? $data['NativeVlanEth3'] : null,
            'NativeVlanEth4' => isset($data['NativeVlanEth4']) ? $data['NativeVlanEth4'] : null,
            // 'ClientsHistorytatus' => isset($data['ClientsHistorytatus']) ? $data['ClientsHistorytatus'] : null,
            'Remark' => isset($data['Remark']) ? $data['Remark'] : null,
            'UpdatedBy' => 'admin',
            'UpdatedDate' => date('Y-m-d H:i:s'),
        ];

        $response = $client->request('PUT', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Update ClientsHistory]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
        return json_decode($response->getBody()->getContents());
    }

    public function put_spesific($code, $data) {
        // Step Simpan DB
        $url = env('IKB_OLT_NOC') . '/clients-history/' . $code;
        $client = new Client(['http_errors' => false]);
        $response = $client->request('PUT', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Update ClientsHistory]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
        return json_decode($response->getBody()->getContents());
    }

    public function delete($code) {
        $url = env('IKB_OLT_NOC') . '/clients-history/' . $code;
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('delete', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Deleted ClientsHistory]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

}
