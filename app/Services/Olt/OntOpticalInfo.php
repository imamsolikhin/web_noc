<?php

namespace App\Services\Olt;

use GuzzleHttp\Client;
use App\Services\Signature;
use Illuminate\Support\Facades\Log;

class OntOpticalInfo {

    public function getAll($hostcode) {
        $url = env('IKB_OLT_NOC') . '/ont-optical-info?limit=10000&page=1&HostCode='.$hostcode;
        $signature = (new Signature($url))->create();
        $clientService = new Client(['http_errors' => false]);
        $response = $clientService->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Get All OntOpticalInfo]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function getAllActive($hostcode) {
        $url = env('IKB_OLT_NOC') . '/ont-optical-info?limit=10000&page=1&ActiveStatus=1&HostCode='.$hostcode;
        $signature = (new Signature($url))->create();
        $clientService = new Client(['http_errors' => false]);
        $response = $clientService->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Get All OntOpticalInfo]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function show($code) {
        $url = env('IKB_OLT_NOC') . '/ont-optical-info/' . $code;
        $clientService = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $clientService->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Show OntOpticalInfo]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function post($clients, $data) {
        $url = env('IKB_OLT_NOC') . '/ont-optical-info';
        $clientService = new Client(['http_errors' => false]);
        $data = [
            'Code' => isset($clients->Code) ? $clients->Code : null,
            'CustomerCode' => isset($clients->Code) ? $clients->Code : null,
            'HostCode' => isset($clients->HostCode) ? $clients->Code : null,
            'FrameId' => isset($clients->FrameId) ? $clients->FrameId : null,
            'SlotId' => isset($clients->SlotId) ? $clients->SlotId : null,
            'PortId' => isset($clients->PortId) ? $clients->PortId : null,
            'OntId' => isset($clients->OntId) ? $clients->OntId : null,
            'OntSn' => isset($clients->OntSn) ? $clients->OntSn : null,
            'RxOpticalPower' => isset($data->rx_optical_power) ? $data->rx_optical_power : null,
            'TxOpticalPower' => isset($data->tx_optical_power) ? $data->tx_optical_power : null,
            'Temperature' => isset($data->temperature) ? $data->temperature : null,
            'OltRxOntOpticalPower' => isset($data->optical_power_precision) ? $data->optical_power_precision : null,
            'ActiveStatus' => 1,
            'CreatedBy' => "admin",
            'CreatedDate' => date('Y-m-d H:i:s'),
        ];

        $response = $clientService->request('POST', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Add OntOpticalInfo]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
        // dd(json_decode($response->getBody()->getContents()));
        return json_decode($response->getBody()->getContents());
    }

    public function put($code, $data) {
        // Step Simpan DB
        $url = env('IKB_OLT_NOC') . '/ont-optical-info/' . $code;
        $clientService = new Client(['http_errors' => false]);
        $data = [
            'Code' => $data['Code'],
            'HostCode' => isset ($data['HostCode']) ? $data['HostCode'] : null,
            'FrameId' => isset ($data['FrameId']) ? $data['FrameId'] : null,
            'SlotID' => isset ($data['SlotID']) ? $data['SlotID'] : null,
            'PortId' => isset ($data['PortId']) ? $data['PortId'] : null,
            'OntId' => isset ($data['OntId']) ? $data['OntId'] : null,
            'OntSn' => isset ($data['OntSn']) ? $data['OntSn'] : null,
            'RxOpticalPower' => isset ($data['RxOpticalPower']) ? $data['RxOpticalPower'] : null,
            'TxOpticalPower' => isset ($data['TxOpticalPower']) ? $data['TxOpticalPower'] : null,
            'Temperature' => isset ($data['Temperature']) ? $data['Temperature'] : null,
            'Voltage' => isset ($data['Voltage']) ? $data['Voltage'] : null,
            'OltRxOntOpticalPower' => isset ($data['OltRxOntOpticalPower']) ? $data['OltRxOntOpticalPower'] : null,
            'ActiveStatus' => 1,
            'UpdatedBy' => 'admin',
            'UpdatedDate' => date('Y-m-d H:i:s'),
        ];

        $response = $clientService->request('PUT', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Update OntOpticalInfo]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
        return json_decode($response->getBody()->getContents());
    }

    public function delete_host($code) {
        $url = env('IKB_OLT_NOC') . '/ont-optical-info/' . $code. '?HostCode='.$code;
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('delete', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Deleted OntOpticalInfo]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

    public function delete($code,$customerCode=null) {
        $url = env('IKB_OLT_NOC') . '/ont-optical-info/' . $code.'?CustomerCode='.$customerCode;
        $clientService = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $clientService->request('delete', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Deleted OntOpticalInfo]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

}
