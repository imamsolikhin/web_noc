<?php

namespace App\Services\Olt;

use GuzzleHttp\Client;
use App\Services\Signature;
use Illuminate\Support\Facades\Log;

class OntInfo {

    public function getAll($hostcode) {
        $url = env('IKB_OLT_NOC') . '/ont-info?limit=10000&page=1&HostCode='.$hostcode;
        $signature = (new Signature($url))->create();
        $clientService = new Client(['http_errors' => false]);
        $response = $clientService->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Get All OntInfo]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function getAllActive($hostcode) {
        $url = env('IKB_OLT_NOC') . '/ont-info?limit=10000&page=1&ActiveStatus=1&HostCode='.$hostcode;
        $signature = (new Signature($url))->create();
        $clientService = new Client(['http_errors' => false]);
        $response = $clientService->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Get All OntInfo]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function show($code) {
        $url = env('IKB_OLT_NOC') . '/ont-info/' . $code;
        $clientService = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $clientService->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Show OntInfo]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function post($clients ,$data) {
        $url = env('IKB_OLT_NOC') . '/ont-info';
        $clientService = new Client(['http_errors' => false]);
        $data = [
            'Code' => isset($clients->Code) ? $clients->Code : null,
            'CustomerCode' => isset($clients->Code) ? $clients->Code : null,
            'HostCode' => isset($clients->HostCode) ? $clients->HostCode : null,
            'FrameId' => isset($clients->FrameId) ? $clients->FrameId : null,
            'SlotID' => isset($clients->SlotID) ? $clients->SlotID : null,
            'PortId' => isset($clients->PortId) ? $clients->PortId : null,
            'OntId' => isset($clients->OntId) ? $clients->OntId : null,
            'OntSn' => isset($clients->OntSn) ? $clients->OntSn : null,
            'Flag' => isset($clients->OntFlag) ? $clients->OntFlag : null,
            'Run' => isset($clients->OntRun) ? $clients->OntRun : null,
            'Config' => isset($clients->OntConfig) ? $clients->OntConfig : null,
            'Match' => isset($clients->OntMatch) ? $clients->OntMatch : null,
            'OntDistance' => isset($data->ont_distance) ? $data->ont_distance : null,
            'Temperature' => isset($data->temperature) ? $data->temperature : null,
            'Description' => isset($data->Description) ? $data->Description : null,
            'LastDownCause' => isset($data->last_down_cause) ? $data->last_down_cause : null,
            'LastUpTime' => isset($data->last_up_time) ? $data->last_up_time : null,
            'LastDownTime' => isset($data->last_down_time) ? $data->last_down_time : null,
            'LastDyingGaspTime' => isset($data->last_dying_gasp_time) ? $data->last_dying_gasp_time : null,
            'OntOnlineDuration' => isset($data->ont_online_duration) ? $data->ont_online_duration : null,
            'ActiveStatus' => 1,
            'CreatedBy' => "admin",
            'CreatedDate' => date('Y-m-d H:i:s'),
        ];

        $response = $clientService->request('POST', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Add OntInfo]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
        // dd(json_decode($response->getBody()->getContents()));
        return json_decode($response->getBody()->getContents());
    }

    public function put($code, $data) {
        // Step Simpan DB
        $url = env('IKB_OLT_NOC') . '/ont-info/' . $code;
        $clientService = new Client(['http_errors' => false]);
        $data = [
            'Code' => $data['Code'],
            'HostCode' => isset($data['HostCode']) ? $data['HostCode'] : null,
            'FrameId' => isset($data['FrameId']) ? $data['FrameId'] : null,
            'SlotID' => isset($data['SlotID']) ? $data['SlotID'] : null,
            'PortId' => isset($data['PortId']) ? $data['PortId'] : null,
            'OntId' => isset($data['OntId']) ? $data['OntId'] : null,
            'OntSn' => isset($data['OntSn']) ? $data['OntSn'] : null,
            'Flag' => isset($data['Flag']) ? $data['Flag'] : null,
            'Run' => isset($data['Run']) ? $data['Run'] : null,
            'Config' => isset($data['Config']) ? $data['Config'] : null,
            'Match' => isset($data['Match']) ? $data['Match'] : null,
            'OntDistance' => isset($data['OntDistance']) ? $data['OntDistance'] : null,
            'Temperature' => isset($data['Temperature']) ? $data['Temperature'] : null,
            'Description' => isset($data['Description']) ? $data['Description'] : null,
            'LastDownCause' => isset($data['LastDownCause']) ? $data['LastDownCause'] : null,
            'LastUpTime' => isset($data['LastUpTime']) ? $data['LastUpTime'] : null,
            'LastDownTime' => isset($data['LastDownTime']) ? $data['LastDownTime'] : null,
            'LastDyingGaspTime' => isset($data['LastDyingGaspTime']) ? $data['LastDyingGaspTime'] : null,
            'OntOnlineDuration' => isset($data['OntOnlineDuration']) ? $data['OntOnlineDuration'] : null,
            'ActiveStatus' => 1,
            'UpdatedBy' => 'admin',
            'UpdatedDate' => date('Y-m-d H:i:s'),
        ];

        $response = $clientService->request('PUT', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Update OntInfo]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
        return json_decode($response->getBody()->getContents());
    }

    public function delete_host($code) {
        $url = env('IKB_OLT_NOC') . '/ont-info/' . $code. '?HostCode='.$code;
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('delete', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Deleted OntInfo]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

    public function delete($code,$customerCode=null) {
        $url = env('IKB_OLT_NOC') . '/ont-info/' . $code.'?CustomerCode='.$customerCode;
        $clientService = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $clientService->request('delete', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Deleted OntInfo]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

}
