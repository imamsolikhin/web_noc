<?php

namespace App\Services\Olt;

use GuzzleHttp\Client;
use App\Services\Signature;
use Illuminate\Support\Facades\Log;

class SrvPort {

    public function getAll() {
        $url = env('IKB_OLT_NOC') . '/srv-port?limit=10000&page=1';
        $signature = (new Signature($url))->create();
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Get All SrvPort]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function getAllActive($code) {
        $url = env('IKB_OLT_NOC') . '/srv-port?limit=10000&page=1&ActiveStatus=1&HostCode='.$code;
        $signature = (new Signature($url))->create();
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Get All SrvPort]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function show($code) {
        $url = env('IKB_OLT_NOC') . '/srv-port/' . $code;
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Show SrvPort]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function post($data) {
        $url = env('IKB_OLT_NOC') . '/srv-port';
        $client = new Client(['http_errors' => false]);
        $data = [
            'Code' => $data['Code'],
            'HostCode' => isset ($data['HostCode']) ? $data['HostCode'] : null,
            'Index' => isset ($data['Index']) ? $data['Index'] : null,
            'VlanId' => isset ($data['VlanId']) ? $data['VlanId'] : null,
            'VlanAttr' => isset ($data['VlanAttr']) ? $data['VlanAttr'] : null,
            'Type' => isset ($data['Type']) ? $data['Type'] : null,
            'FrameId' => isset ($data['FrameId']) ? $data['FrameId'] : null,
            'SlotId' => isset ($data['SlotId']) ? $data['SlotId'] : null,
            'PortId' => isset ($data['PortId']) ? $data['PortId'] : null,
            'VPI' => isset ($data['VPI']) ? $data['VPI'] : null,
            'VCI' => isset ($data['VCI']) ? $data['VCI'] : null,
            'FlowType' => isset ($data['FlowType']) ? $data['FlowType'] : null,
            'FlowPara' => isset ($data['FlowPara']) ? $data['FlowPara'] : null,
            'Rx' => isset ($data['Rx']) ? $data['Rx'] : null,
            'Tx' => isset ($data['Tx']) ? $data['Tx'] : null,
            'State' => isset ($data['State']) ? $data['State'] : null,
            'Transparent' => isset ($data['Transparent']) ? $data['Transparent'] : null,
            'ActiveStatus' => isset ($data['ActiveStatus']) ? $data['ActiveStatus'] : null,
            'CreatedBy' => "admin",
            'CreatedDate' => date('Y-m-d H:i:s'),
        ];

        $response = $client->request('POST', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Add SrvPort]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

    public function post_list($datas) {
        $url = env('IKB_OLT_NOC') . '/srv-port?list=all';
        $client = new Client(['http_errors' => false]);
        $list = array();
        foreach ($datas as $data_val) {
            $dt = new \stdClass();
            $dt->Code = $data_val->host.'-'.$data_val->index.'-'.rand(1,100);
            $dt->HostCode = $data_val->host;
            $dt->Index = $data_val->index;
            $dt->VlanId = $data_val->vlan;
            $dt->VlanAttr = $data_val->vlan_attr;
            $dt->Type = $data_val->type;
            $dt->FrameId = (int)$data_val->frame;
            $dt->SlotId = (int)$data_val->slot;
            $dt->PortId = (int)$data_val->port;
            $dt->VPI = $data_val->vpi;
            $dt->VCI = $data_val->vci;
            $dt->FlowType = $data_val->flow_type;
            $dt->FlowPara = $data_val->flow_para;
            $dt->Rx = $data_val->rx;
            $dt->Tx = $data_val->tx;
            $dt->State = $data_val->state;
            $dt->Transparent = "";
            $dt->ActiveStatus =  1;
            $dt->Fix =  null;
            $dt->CreatedBy = "admin";
            $dt->CreatedDate = date('Y-m-d H:i:s');
            $dt->UpdatedBy = "admin";
            $dt->UpdatedDate = date('Y-m-d H:i:s');
            $dt->InActiveBy = "admin";
            $dt->InActiveDate = date('Y-m-d H:i:s');
            array_push($list,$dt);
        }
        $data["data"] = $list;

        $response = $client->request('POST',  $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Add SrvPort]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
        return json_decode($response->getBody()->getContents());
    }

    public function put($code, $data) {
        $url = env('IKB_OLT_NOC') . '/srv-port/' . $code;

        $client = new Client(['http_errors' => false]);
        $data = [
            'Code' => $data['Code'],
            'HostCode' => isset ($data['HostCode']) ? $data['HostCode'] : null,
            'Index' => isset ($data['Index']) ? $data['Index'] : null,
            'VlanId' => isset ($data['VlanId']) ? $data['VlanId'] : null,
            'VlanAttr' => isset ($data['VlanAttr']) ? $data['VlanAttr'] : null,
            'Type' => isset ($data['Type']) ? $data['Type'] : null,
            'FrameId' => isset ($data['FrameId']) ? $data['FrameId'] : null,
            'SlotId' => isset ($data['SlotId']) ? $data['SlotId'] : null,
            'PortId' => isset ($data['PortId']) ? $data['PortId'] : null,
            'VPI' => isset ($data['VPI']) ? $data['VPI'] : null,
            'VCI' => isset ($data['VCI']) ? $data['VCI'] : null,
            'FlowType' => isset ($data['FlowType']) ? $data['FlowType'] : null,
            'FlowPara' => isset ($data['FlowPara']) ? $data['FlowPara'] : null,
            'Rx' => isset ($data['Rx']) ? $data['Rx'] : null,
            'Tx' => isset ($data['Tx']) ? $data['Tx'] : null,
            'State' => isset ($data['State']) ? $data['State'] : null,
            'Transparent' => isset ($data['Transparent']) ? $data['Transparent'] : null,
            'ActiveStatus' => isset ($data['ActiveStatus']) ? $data['ActiveStatus'] : null,
            'UpdatedBy' => "admin",
            'UpdatedDate' => date('Y-m-d H:i:s'),
        ];

        $response = $client->request('PUT', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Update SrvPort]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

    public function delete_host($code) {
        $url = env('IKB_OLT_NOC') . '/srv-port/' . $code. '?HostCode='.$code;
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('delete', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Deleted SrvPort]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

    public function delete($code) {
        $url = env('IKB_OLT_NOC') . '/srv-port/' . $code;
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('delete', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Deleted SrvPort]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

}
