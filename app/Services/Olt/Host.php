<?php

namespace App\Services\Olt;

use GuzzleHttp\Client;
use App\Services\Signature;
use Illuminate\Support\Facades\Log;

class Host
{
    public function getAll($request = null)
    {
        $search = ($request->search) ? $request->search['value']:null;
        $from_date = ($request->from_date) ? $request->from_date:null;
        $to_date = ($request->to_date) ? $request->to_date:null;
        $limit = ($request->length) ? $request->length:intval(global_limit());
        $page = ($request->length) ? (int) ($request->start/$request->length)+1:1;
        $url = env('IKB_OLT_NOC') . '/host?limit='.$limit.'&page='.$page.'&from_date='.$from_date.'&to_date='.$to_date.'&search='.$search;
        $signature = (new Signature($url))->create();
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Get All Host]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function getAllActive()
    {
        $url = env('IKB_OLT_NOC') . '/host?limit=10000&page=1&ActiveStatus=1';
        $signature = (new Signature($url))->create();
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Get All Host]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function show($code)
    {
        $url = env('IKB_OLT_NOC') . '/host/' . $code;
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Show Host]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function post($data)
    {
        $url = env('IKB_OLT_NOC') . '/host';
        $client = new Client(['http_errors' => false]);
        $data = [
            'Code' => isset($data['Code']) ? $data['Code'] : null,
            'Hostname' => isset($data['Hostname']) ? $data['Hostname'] : null,
            'IpAddress' => isset($data['IpAddress']) ? $data['IpAddress'] : null,
            'Username' => isset($data['Username']) ? $data['Username'] : null,
            'Password' => isset($data['Password']) ? $data['Password'] : null,
            'Port' => isset($data['Port']) ? $data['Port'] : null,
            'FrameId' => isset($data['FrameId']) ? $data['FrameId'] : null,
            'SlotId' => isset($data['SlotId']) ? $data['SlotId'] : null,
            'PortId' => isset($data['PortId']) ? $data['PortId'] : null,
            'Sysname' => isset($data['Sysname']) ? $data['Sysname'] : null,
            'SnmpCommunity' => isset($data['SnmpCommunity']) ? $data['SnmpCommunity'] : null,
            'BtsCode' => isset($data['BtsCode']) ? $data['BtsCode'] : null,
            'DeviceCode' => isset($data['DeviceCode']) ? $data['DeviceCode'] : null,
            'ActiveStatus' => isset($data['ActiveStatus']) ? $data['ActiveStatus'] : null,
            'Remark' => isset($data['Remark']) ? $data['Remark'] : null,
            'CreatedBy' => "admin",
            'CreatedDate' => date('Y-m-d H:i:s'),
        ];

        $response = $client->request('POST', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Add Host]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return json_decode($response->getBody()->getContents());
    }

    public function post_list($datas) {
        $url = env('IKB_OLT_NOC') . '/host?list=all';
        $client = new Client(['http_errors' => false]);
        $list = array();
        foreach ($datas as $data_val) {
            $dt = new \stdClass();
            $header= $data_val->HostCode;

            $datas->Code = $data_val[1].'-'.$data_val[0];
            $datas->ActiveStatus =  1;
            $datas->CreatedBy = "admin";
            $datas->CreatedDate = date('Y-m-d H:i:s');
            $datas->UpdatedBy = "admin";
            $datas->UpdatedDate = date('Y-m-d H:i:s');
            $datas->InActiveBy = "admin";
            $datas->InActiveDate = date('Y-m-d H:i:s');
            array_push($list,$dt);
        }
        $data["data"] = $datas;
        $response = $client->request('POST',  $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        // dd($data);
        // dd(json_decode($response->getBody()->getContents()));

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Add Host]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
        return json_decode($response->getBody()->getContents());
    }

    public function put($code, $data)
    {
        $url = env('IKB_OLT_NOC') . '/host/' . $code;

        $client = new Client(['http_errors' => false]);
        $data = [
            'Code' => isset($data['Code']) ? $data['Code'] : null,
            'Hostname' => isset($data['Hostname']) ? $data['Hostname'] : null,
            'IpAddress' => isset($data['IpAddress']) ? $data['IpAddress'] : null,
            'Username' => isset($data['Username']) ? $data['Username'] : null,
            'Password' => isset($data['Password']) ? $data['Password'] : null,
            'Port' => isset($data['Port']) ? (int)$data['Port'] : null,
            'FrameId' => isset($data['FrameId']) ? (int)$data['FrameId'] : null,
            'SlotId' => isset($data['SlotId']) ? (int)$data['SlotId'] : null,
            'PortId' => isset($data['PortId']) ? (int)$data['PortId'] : null,
            'Sysname' => isset($data['Sysname']) ? $data['Sysname'] : null,
            'SnmpCommunity' => isset($data['SnmpCommunity']) ? $data['SnmpCommunity'] : null,
            'BtsCode' => isset($data['BtsCode']) ? $data['BtsCode'] : null,
            'DeviceCode' => isset($data['DeviceCode']) ? $data['DeviceCode'] : null,
            'ActiveStatus' => isset($data['ActiveStatus']) ? $data['ActiveStatus'] : null,
            'Remark' => isset($data['Remark']) ? $data['Remark'] : null,
            'UpdatedBy' => 'admin',
            'UpdatedDate' => date('Y-m-d H:i:s'),
        ];

        $response = $client->request('PUT', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Update Host]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
        // dd(json_decode($response->getBody()->getContents()));
        return json_decode($response->getBody()->getContents());
    }

    public function delete($code)
    {
        $url = env('IKB_OLT_NOC') . '/host/' . $code;
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('delete', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Deleted Host]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }
}
