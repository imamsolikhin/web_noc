<?php

namespace App\Services\Olt;

use GuzzleHttp\Client;
use App\Services\Signature;
use Illuminate\Support\Facades\Log;

class CustomerRegistration {

    public function getAll() {
        $url = env('IKB_OLT_NOC') . '/customer-registration?limit=10000&page=1';
        $signature = (new Signature($url))->create();
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Get All CustomerRegistration]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function getAllActive() {
        $url = env('IKB_OLT_NOC') . '/customer-registration?limit=10000&page=1&ActiveStatus=1';
        $signature = (new Signature($url))->create();
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Get All CustomerRegistration]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function show($code) {
        $url = env('IKB_OLT_NOC') . '/customer-registration/' . $code;
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Show CustomerRegistration]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function post($data) {
        $url = env('IKB_OLT_NOC') . '/customer-registration';
        $client = new Client(['http_errors' => false]);
        $data = [
            'Code' => $data['Code'],
            'CompanyCode' => isset($data['CompanyCode']) ? $data['CompanyCode'] : null,
            'CompanyName' => isset($data['CompanyName']) ? $data['CompanyName'] : null,
            'CompanyInisial' => isset($data['CompanyInisial']) ? $data['CompanyInisial'] : null,
            'CompanyGroup' => isset($data['CompanyGroup']) ? $data['CompanyGroup'] : null,
            'LineofBusiness' => isset($data['LineofBusiness']) ? $data['LineofBusiness'] : null,
            'CustomerCode' => isset($data['CustomerCode']) ? $data['CustomerCode'] : null,
            'CustomerCompanyCode' => isset($data['CustomerCompanyCode']) ? $data['CustomerCompanyCode'] : null,
            'CustomerCompanyName' => isset($data['CustomerCompanyName']) ? $data['CustomerCompanyName'] : null,
            'CustomerProvinceBirth' => isset($data['CustomerProvinceBirth']) ? $data['CustomerProvinceBirth'] : null,
            'CustomerCityBirth' => isset($data['CustomerCityBirth']) ? $data['CustomerCityBirth'] : null,
            'CustomerBirthDate' => isset($data['CustomerBirthDate']) ? $data['CustomerBirthDate'] : null,
            'CustomerJobTitle' => isset($data['CustomerJobTitle']) ? $data['CustomerJobTitle'] : null,
            'CustomerPhone' => isset($data['CustomerPhone']) ? $data['CustomerPhone'] : null,
            'CustomerFax' => isset($data['CustomerFax']) ? $data['CustomerFax'] : null,
            'CustomerEmail' => isset($data['CustomerEmail']) ? $data['CustomerEmail'] : null,
            'CustomerPhoneNumber' => isset($data['CustomerPhoneNumber']) ? $data['CustomerPhoneNumber'] : null,
            'CustomerNPWP' => isset($data['CustomerNPWP']) ? $data['CustomerNPWP'] : null,
            'CustomerAddress' => isset($data['CustomerAddress']) ? $data['CustomerAddress'] : null,
            'CustomerProvinceCode' => isset($data['CustomerProvinceCode']) ? $data['CustomerProvinceCode'] : null,
            'CustomerProvinceName' => isset($data['CustomerProvinceName']) ? $data['CustomerProvinceName'] : null,
            'CustomerCityCode' => isset($data['CustomerCityCode']) ? $data['CustomerCityCode'] : null,
            'CustomerCityName' => isset($data['CustomerCityName']) ? $data['CustomerCityName'] : null,
            'CustomerZipCode' => isset($data['CustomerZipCode']) ? $data['CustomerZipCode'] : null,
            'CustomerWebsite' => isset($data['CustomerWebsite']) ? $data['CustomerWebsite'] : null,
            'CustomerIDCard' => isset($data['CustomerIDCard']) ? $data['CustomerIDCard'] : null,
            'CustomerIDCardNumber' => isset($data['CustomerIDCardNumber']) ? $data['CustomerIDCardNumber'] : null,
            'CustomerIDCardExpired' => isset($data['CustomerIDCardExpired']) ? $data['CustomerIDCardExpired'] : null,
            'CustomerProductSite' => isset($data['CustomerProductSite']) ? $data['CustomerProductSite'] : null,
            'CustomerRegistrationDate' => isset($data['CustomerRegistrationDate']) ? $data['CustomerRegistrationDate'] : null,
            'CustomerProductCode' => isset($data['CustomerProductCode']) ? $data['CustomerProductCode'] : null,
            'CustomerStatusClient' => isset($data['CustomerStatusClient']) ? $data['CustomerStatusClient'] : null,
            'CustomerSalesCode' => isset($data['CustomerSalesCode']) ? $data['CustomerSalesCode'] : null,
            'CustomerSalesName' => isset($data['CustomerSalesName']) ? $data['CustomerSalesName'] : null,
            'TechnicalPicCode' => isset($data['TechnicalPicCode']) ? $data['TechnicalPicCode'] : null,
            'TechnicalPicCompanyCode' => isset($data['TechnicalPicCompanyCode']) ? $data['TechnicalPicCompanyCode'] : null,
            'TechnicalPicName' => isset($data['TechnicalPicName']) ? $data['TechnicalPicName'] : null,
            'TechnicalPicDepartment' => isset($data['TechnicalPicDepartment']) ? $data['TechnicalPicDepartment'] : null,
            'TechnicalPicJobTitle' => isset($data['TechnicalPicJobTitle']) ? $data['TechnicalPicJobTitle'] : null,
            'TechnicalPicPhoneNumber' => isset($data['TechnicalPicPhoneNumber']) ? $data['TechnicalPicPhoneNumber'] : null,
            'TechnicalPicFax' => isset($data['TechnicalPicFax']) ? $data['TechnicalPicFax'] : null,
            'TechnicalPicEmail' => isset($data['TechnicalPicEmail']) ? $data['TechnicalPicEmail'] : null,
            'TechnicalPicSalesInfo' => isset($data['TechnicalPicSalesInfo']) ? $data['TechnicalPicSalesInfo'] : null,
            'TechnicalPicNIP' => isset($data['TechnicalPicNIP']) ? $data['TechnicalPicNIP'] : null,
            'ActiveStatus' => isset($data['ActiveStatus']) ? $data['ActiveStatus'] : null,
            'CreatedBy' => "admin",
            'CreatedDate' => date('Y-m-d H:i:s'),
        ];

        $response = $client->request('POST', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Add CustomerRegistration]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

    public function post_list($datas) {
        $url = env('IKB_OLT_NOC') . '/customer-registration?list=all';
        $client = new Client(['http_errors' => false]);
        $data["data"] = $datas;
        $response = $client->request('POST',  $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);


        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Add CustomerRegistration]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
        return json_decode($response->getBody()->getContents());
    }

    public function put($code, $data) {
        $url = env('IKB_OLT_NOC') . '/customer-registration/' . $code;

        $client = new Client(['http_errors' => false]);
        $data = [
            'Code' => $data['Code'],
            'CompanyCode' => isset($data['CompanyCode']) ? $data['CompanyCode'] : null,
            'CompanyName' => isset($data['CompanyName']) ? $data['CompanyName'] : null,
            'CompanyInisial' => isset($data['CompanyInisial']) ? $data['CompanyInisial'] : null,
            'CompanyGroup' => isset($data['CompanyGroup']) ? $data['CompanyGroup'] : null,
            'LineofBusiness' => isset($data['LineofBusiness']) ? $data['LineofBusiness'] : null,
            'CustomerCode' => isset($data['CustomerCode']) ? $data['CustomerCode'] : null,
            'CustomerCompanyCode' => isset($data['CustomerCompanyCode']) ? $data['CustomerCompanyCode'] : null,
            'CustomerCompanyName' => isset($data['CustomerCompanyName']) ? $data['CustomerCompanyName'] : null,
            'CustomerProvinceBirth' => isset($data['CustomerProvinceBirth']) ? $data['CustomerProvinceBirth'] : null,
            'CustomerCityBirth' => isset($data['CustomerCityBirth']) ? $data['CustomerCityBirth'] : null,
            'CustomerBirthDate' => isset($data['CustomerBirthDate']) ? $data['CustomerBirthDate'] : null,
            'CustomerJobTitle' => isset($data['CustomerJobTitle']) ? $data['CustomerJobTitle'] : null,
            'CustomerPhone' => isset($data['CustomerPhone']) ? $data['CustomerPhone'] : null,
            'CustomerFax' => isset($data['CustomerFax']) ? $data['CustomerFax'] : null,
            'CustomerEmail' => isset($data['CustomerEmail']) ? $data['CustomerEmail'] : null,
            'CustomerPhoneNumber' => isset($data['CustomerPhoneNumber']) ? $data['CustomerPhoneNumber'] : null,
            'CustomerNPWP' => isset($data['CustomerNPWP']) ? $data['CustomerNPWP'] : null,
            'CustomerAddress' => isset($data['CustomerAddress']) ? $data['CustomerAddress'] : null,
            'CustomerProvinceCode' => isset($data['CustomerProvinceCode']) ? $data['CustomerProvinceCode'] : null,
            'CustomerProvinceName' => isset($data['CustomerProvinceName']) ? $data['CustomerProvinceName'] : null,
            'CustomerCityCode' => isset($data['CustomerCityCode']) ? $data['CustomerCityCode'] : null,
            'CustomerCityName' => isset($data['CustomerCityName']) ? $data['CustomerCityName'] : null,
            'CustomerZipCode' => isset($data['CustomerZipCode']) ? $data['CustomerZipCode'] : null,
            'CustomerWebsite' => isset($data['CustomerWebsite']) ? $data['CustomerWebsite'] : null,
            'CustomerIDCard' => isset($data['CustomerIDCard']) ? $data['CustomerIDCard'] : null,
            'CustomerIDCardNumber' => isset($data['CustomerIDCardNumber']) ? $data['CustomerIDCardNumber'] : null,
            'CustomerIDCardExpired' => isset($data['CustomerIDCardExpired']) ? $data['CustomerIDCardExpired'] : null,
            'CustomerProductSite' => isset($data['CustomerProductSite']) ? $data['CustomerProductSite'] : null,
            'CustomerRegistrationDate' => isset($data['CustomerRegistrationDate']) ? $data['CustomerRegistrationDate'] : null,
            'CustomerProductCode' => isset($data['CustomerProductCode']) ? $data['CustomerProductCode'] : null,
            'CustomerStatusClient' => isset($data['CustomerStatusClient']) ? $data['CustomerStatusClient'] : null,
            'CustomerSalesCode' => isset($data['CustomerSalesCode']) ? $data['CustomerSalesCode'] : null,
            'CustomerSalesName' => isset($data['CustomerSalesName']) ? $data['CustomerSalesName'] : null,
            'TechnicalPicCode' => isset($data['TechnicalPicCode']) ? $data['TechnicalPicCode'] : null,
            'TechnicalPicCompanyCode' => isset($data['TechnicalPicCompanyCode']) ? $data['TechnicalPicCompanyCode'] : null,
            'TechnicalPicName' => isset($data['TechnicalPicName']) ? $data['TechnicalPicName'] : null,
            'TechnicalPicDepartment' => isset($data['TechnicalPicDepartment']) ? $data['TechnicalPicDepartment'] : null,
            'TechnicalPicJobTitle' => isset($data['TechnicalPicJobTitle']) ? $data['TechnicalPicJobTitle'] : null,
            'TechnicalPicPhoneNumber' => isset($data['TechnicalPicPhoneNumber']) ? $data['TechnicalPicPhoneNumber'] : null,
            'TechnicalPicFax' => isset($data['TechnicalPicFax']) ? $data['TechnicalPicFax'] : null,
            'TechnicalPicEmail' => isset($data['TechnicalPicEmail']) ? $data['TechnicalPicEmail'] : null,
            'TechnicalPicSalesInfo' => isset($data['TechnicalPicSalesInfo']) ? $data['TechnicalPicSalesInfo'] : null,
            'TechnicalPicNIP' => isset($data['TechnicalPicNIP']) ? $data['TechnicalPicNIP'] : null,
            'ActiveStatus' => isset($data['ActiveStatus']) ? $data['ActiveStatus'] : null,
            'UpdatedBy' => 'admin',
            'UpdatedDate' => date('Y-m-d H:i:s'),
        ];

        $response = $client->request('PUT', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Update CustomerRegistration]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

    public function delete($code) {
        $url = env('IKB_OLT_NOC') . '/customer-registration/' . $code;
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('delete', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Deleted Customer]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

}
