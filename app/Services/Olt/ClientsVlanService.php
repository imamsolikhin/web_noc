<?php

namespace App\Services\Olt;

use GuzzleHttp\Client;
use App\Services\Signature;
use Illuminate\Support\Facades\Log;

class ClientsVlanService {

    public function getAll($CustomerCode) {
        $url = env('IKB_OLT_NOC') . '/clients-vlan-service?limit=10000&page=1&CustomerCode='.$CustomerCode;
        $signature = (new Signature($url))->create();
        $clientService = new Client(['http_errors' => false]);
        $response = $clientService->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Get All ClientsVlanService]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function getAllActive($CustomerCode) {
        $url = env('IKB_OLT_NOC') . '/clients-vlan-service?limit=10000&page=1&ActiveStatus=1&CustomerCode='.$CustomerCode;
        $signature = (new Signature($url))->create();
        $clientService = new Client(['http_errors' => false]);
        $response = $clientService->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Get All ClientsVlanService]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function show($code) {
        $url = env('IKB_OLT_NOC') . '/clients-vlan-service/' . $code;
        $clientService = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $clientService->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Show ClientsVlanService]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function post($data) {
        $url = env('IKB_OLT_NOC') . '/clients-vlan-service';
        $clientService = new Client(['http_errors' => false]);
        $data = [
            'Code' => isset($data['Code']) ? $data['Code'] : null,
            'CustomerCode' => isset($data['CustomerCode']) ? $data['CustomerCode'] : null,
            'ServicePortId' => isset($data['ServicePortId']) ? $data['ServicePortId'] : null,
            'Vlan' => isset($data['Vlan']) ? $data['Vlan'] : null,
            'GemPort' => isset($data['GemPort']) ? $data['GemPort'] : null,
            'UserVlan' => isset($data['UserVlan']) ? $data['UserVlan'] : null,
            'InnerVlan' => isset($data['InnerVlan']) ? $data['InnerVlan'] : null,
            'TagTransform' => isset($data['TagTransform']) ? $data['TagTransform'] : null,
            'TrafficTable' => isset($data['TrafficTable']) ? $data['TrafficTable'] : null,
            'ActiveStatus' => 1,
            'CreatedBy' => "admin",
            'CreatedDate' => date('Y-m-d H:i:s'),
        ];

        $response = $clientService->request('POST', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Add ClientsVlanService]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
        return json_decode($response->getBody()->getContents());
    }

    public function post_list($datas) {
        $url = env('IKB_OLT_NOC') . '/clients-vlan-service?list=all';
        $client = new Client(['http_errors' => false]);
        $data["data"] = $datas;
        $response = $client->request('POST',  $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Add ClientsVlanService]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
        return json_decode($response->getBody()->getContents());
    }

    public function put($code, $data) {
        // Step Simpan DB
        $url = env('IKB_OLT_NOC') . '/clients-vlan-service/' . $code;
        $clientService = new Client(['http_errors' => false]);
        $data = [
            'Code' => isset($data['Code']) ? $data['Code'] : null,
            'CustomerCode' => isset($data['CustomerCode']) ? $data['CustomerCode'] : null,
            'ServicePortId' => isset($data['ServicePortId']) ? $data['ServicePortId'] : null,
            'Vlan' => isset($data['Vlan']) ? $data['Vlan'] : null,
            'GemPort' => isset($data['GemPort']) ? $data['GemPort'] : null,
            'UserVlan' => isset($data['UserVlan']) ? $data['UserVlan'] : null,
            'InnerVlan' => isset($data['InnerVlan']) ? $data['InnerVlan'] : null,
            'TagTransform' => isset($data['TagTransform']) ? $data['TagTransform'] : null,
            'TrafficTable' => isset($data['TrafficTable']) ? $data['TrafficTable'] : null,
            'ActiveStatus' => 1,
            'UpdatedBy' => 'admin',
            'UpdatedDate' => date('Y-m-d H:i:s'),
        ];

        $response = $clientService->request('PUT', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Update ClientsVlanService]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
        return json_decode($response->getBody()->getContents());
    }

    public function delete($code) {
        $url = env('IKB_OLT_NOC') . '/clients-vlan-service/' . $code.'?CustomerCode='.$code;
        $clientService = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $clientService->request('delete', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Deleted ClientsVlanService]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

}
