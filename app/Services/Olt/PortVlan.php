<?php

namespace App\Services\Olt;

use GuzzleHttp\Client;
use App\Services\Signature;
use Illuminate\Support\Facades\Log;

class PortVlan {

    public function getAll($hostcode) {
        $url = env('IKB_OLT_NOC') . '/port-vlan?limit=10000&page=1&HostCode='.$hostcode;
        $signature = (new Signature($url))->create();
        $clientService = new Client(['http_errors' => false]);
        $response = $clientService->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Get All PortVlan]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function getAllActive($hostcode) {
        $url = env('IKB_OLT_NOC') . '/port-vlan?limit=10000&page=1&ActiveStatus=1&HostCode='.$hostcode;
        $signature = (new Signature($url))->create();
        $clientService = new Client(['http_errors' => false]);
        $response = $clientService->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Get All PortVlan]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function show($code) {
        $url = env('IKB_OLT_NOC') . '/port-vlan/' . $code;
        $clientService = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $clientService->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Show PortVlan]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function post($data) {
        $url = env('IKB_OLT_NOC') . '/port-vlan';
        $clientService = new Client(['http_errors' => false]);
        $data = [
            'Code' => $data['Code'],
            'HostCode' => isset($data['HostCode']) ? $data['HostCode'] : null,
            'Index' => isset($data['Index']) ? $data['Index'] : null,
            'Type' => isset($data['Type']) ? $data['Type'] : null,
            'Desc' => isset($data['Desc']) ? $data['Desc'] : null,
            'Attrib' => isset($data['Attrib']) ? $data['Attrib'] : null,
            'Frame' => isset($data['Frame']) ? $data['Frame'] : null,
            'Slot' => isset($data['Slot']) ? $data['Slot'] : null,
            'Port' => isset($data['Port']) ? $data['Port'] : null,
            'ActiveStatus' => 1,
            'CreatedBy' => "admin",
            'CreatedDate' => date('Y-m-d H:i:s'),
        ];

        $response = $clientService->request('POST', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Add PortVlan]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
        return json_decode($response->getBody()->getContents());
    }

    public function post_list($datas) {
        $url = env('IKB_OLT_NOC') . '/port-vlan?list=all';
        $client = new Client(['http_errors' => false]);
        $list = array();
        $i = 1;
        foreach ($datas as $data_val) {
            $dt = new \stdClass();
            $dt->Code = $data_val->host.'-'.$data_val->index.'-'.($i++);
            $dt->HostCode = $data_val->host;
            $dt->Index = (int)$data_val->index;
            $dt->Type = $data_val->type;
            $dt->Desc = $data_val->desc;
            $dt->Attrib = $data_val->attrib;
            $dt->Frame = (int)$data_val->frame;
            $dt->Slot = (int)$data_val->slot;
            $dt->Port = (int)$data_val->port;
            $dt->ActiveStatus =  1;
            $dt->CreatedBy = "admin";
            $dt->CreatedDate = date('Y-m-d H:i:s');
            $dt->UpdatedBy = "admin";
            $dt->UpdatedDate = date('Y-m-d H:i:s');
            $dt->InActiveBy = "admin";
            $dt->InActiveDate = date('Y-m-d H:i:s');
            array_push($list,$dt);
        }
        $data["data"] = $list;

        $response = $client->request('POST',  $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Add PortVlan]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
        return json_decode($response->getBody()->getContents());
    }

    public function put($code, $data) {
        // Step Simpan DB
        $url = env('IKB_OLT_NOC') . '/port-vlan/' . $code;
        $clientService = new Client(['http_errors' => false]);
        $data = [
            'Code' => $data['Code'],
            'HostCode' => isset($data['HostCode']) ? $data['HostCode'] : null,
            'Index' => isset($data['Index']) ? $data['Index'] : null,
            'Type' => isset($data['Type']) ? $data['Type'] : null,
            'Desc' => isset($data['Desc']) ? $data['Desc'] : null,
            'Attrib' => isset($data['Attrib']) ? $data['Attrib'] : null,
            'Frame' => isset($data['Frame']) ? $data['Frame'] : null,
            'Slot' => isset($data['Slot']) ? $data['Slot'] : null,
            'Port' => isset($data['Port']) ? $data['Port'] : null,
            'ActiveStatus' => 1,
            'UpdatedBy' => 'admin',
            'UpdatedDate' => date('Y-m-d H:i:s'),
        ];

        $response = $clientService->request('PUT', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Update PortVlan]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
        return json_decode($response->getBody()->getContents());
    }

    public function delete_host($code) {
        $url = env('IKB_OLT_NOC') . '/port-vlan/' . $code. '?HostCode='.$code;
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('delete', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Deleted PortVlan]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

    public function delete($code,$customerCode=null) {
        $url = env('IKB_OLT_NOC') . '/port-vlan/' . $code.'?CustomerCode='.$customerCode;
        $clientService = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $clientService->request('delete', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Deleted PortVlan]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

}
