<?php

namespace App\Services\Olt;

use GuzzleHttp\Client;
use App\Services\Signature;
use Illuminate\Support\Facades\Log;

class Clients
{
    public function dashboard()
    {
        $url = env('IKB_OLT_NOC') . '/clients/dashboard';
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Show Dashboard Clients]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function getAll($request)
    {
        $search = ($request->search) ? $request->search['value']:null;
        $from_date = ($request->from_date) ? $request->from_date:null;
        $to_date = ($request->to_date) ? $request->to_date:null;
        $limit = ($request->length) ? $request->length:intval(global_limit());
        $page = ($request->length) ? (int) ($request->start/$request->length)+1:1;
        $search .= ($request->HostCode) ? '&HostCode='.$request->HostCode:'';
        $search .= ($request->ClientStatus) ? '&ClientStatus='.$request->ClientStatus:'';
        $url = env('IKB_OLT_NOC') . '/clients?limit='.$limit.'&page='.$page.'&from_date='.$from_date.'&to_date='.$to_date.'&search='.$search;
        $signature = (new Signature($url))->create();
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Get All Clients]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function getAllActive()
    {
        $url = env('IKB_OLT_NOC') . '/clients?limit=10000&page=1&ActiveStatus=1';
        $signature = (new Signature($url))->create();
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Get All Clients]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function show($code)
    {
        $url = env('IKB_OLT_NOC') . '/clients/' . $code;
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Show Clients]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function show_setting($code)
    {
        $url = env('IKB_OLT_NOC') . '/clients/' . $code.'?setting=true';
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Show Clients]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function post($data)
    {
        $url = env('IKB_OLT_NOC') . '/clients';
        $client = new Client(['http_errors' => false]);
        $data = [
            'Code' => isset($data['Code']) ? str_replace(' ', '', $data['Code']) : null,
            'IspCode' => isset($data['IspCode']) ? $data['IspCode'] : null,
            'HostCode' => isset($data['HostCode']) ? $data['HostCode'] : null,
            'CustomerCode' => isset($data['CustomerCode']) ? $data['CustomerCode'] : null,
            'CustomerName' => isset($data['CustomerName']) ? $data['CustomerName'] : null,
            'CustomerContactPerson' => isset($data['CustomerContactPerson']) ? $data['CustomerContactPerson'] : null,
            'CustomerAddress' => isset($data['CustomerAddress']) ? $data['CustomerAddress'] : null,
            'CustomerCity' => isset($data['CustomerCity']) ? $data['CustomerCity'] : null,
            'CustomerPhone1' => isset($data['CustomerPhone1']) ? $data['CustomerPhone1'] : null,
            'CustomerPhone2' => isset($data['CustomerPhone2']) ? $data['CustomerPhone2'] : null,
            'CustomerFax' => isset($data['CustomerFax']) ? $data['CustomerFax'] : null,
            'CustomerEmail' => isset($data['CustomerEmail']) ? $data['CustomerEmail'] : null,
            'CustomerType' => isset($data['CustomerType']) ? $data['CustomerType'] : null,
            'TemplateCode' => isset($data['TemplateCode']) ? $data['TemplateCode'] : null,
            'VlanDownLink' => isset($data['VlanDownLink']) ? $data['VlanDownLink'] : null,
            'FrameId' => isset($data['FrameId']) ? (int)$data['FrameId'] : null,
            'SlotId' => isset($data['SlotId']) ? (int)$data['SlotId'] : null,
            'PortId' => isset($data['PortId']) ? (int)$data['PortId'] : null,
            'OntVersion' => isset($data['OntVersion']) ? $data['OntVersion'] : null,
            'OntSoftware' => isset($data['OntSoftware']) ? $data['OntSoftware'] : null,
            'OntSn' => isset($data['Sn']) ? $data['Sn'] : null,
            'OntId' => isset($data['OntId']) ? $data['OntId'] : null,
            'OntRun' => isset($data['OntRun']) ? $data['OntRun'] : null,
            // 'LasDownCause' => isset($data['LasDownCause']) ? $data['LasDownCause'] : null,
            // 'lastUpTime' => isset($data['lastUpTime']) ? $data['lastUpTime'] : null,
            // 'lasDownTime' => isset($data['lasDownTime']) ? $data['lasDownTime'] : null,
            // 'lasDyingGasp' => isset($data['lasDyingGasp']) ? $data['lasDyingGasp'] : null,
            // 'ontRx' => isset($data['ontRx']) ? $data['ontRx'] : null,
            // 'ontRxPower' => isset($data['ontRxPower']) ? $data['ontRxPower'] : null,
            // 'ontDistance' => isset($data['ontDistance']) ? $data['ontDistance'] : null,
            // 'serviceType' => isset($data['serviceType']) ? $data['serviceType'] : null,
            // 'connectionType' => isset($data['connectionType']) ? $data['connectionType'] : null,
            // 'ipv4ConnectionStatus' => isset($data['ipv4ConnectionStatus']) ? $data['ipv4ConnectionStatus'] : null,
            // 'portState' => isset($data['portState']) ? $data['portState'] : null,
            // 'statusPort' => isset($data['statusPort']) ? $data['statusPort'] : null,
            // 'ontOnlineDuration' => isset($data['ontOnlineDuration']) ? $data['ontOnlineDuration'] : null,
            'OntLineProfileId' => isset($data['OntLineProfileId']) ? $data['OntLineProfileId'] : null,
            'OntSrvProfileId' => isset($data['OntSrvProfileId']) ? $data['OntSrvProfileId'] : null,
            'VlanId' => isset($data['VlanId']) ? $data['VlanId'] : null,
            'VlanFrameId' => isset($data['VlanFrameId']) ? (int)$data['VlanFrameId'] : null,
            'VlanSlotId' => isset($data['VlanSlotId']) ? (int)$data['VlanSlotId'] : null,
            'VlanPortId' => isset($data['VlanPortId']) ?(int)$data['VlanPortId'] : null,
            'FdtNo' => isset($data['FdtNo']) ? $data['FdtNo'] : null,
            'FatNo' => isset($data['FatNo']) ? $data['FatNo'] : null,
            'VlanAttribut' => isset($data['VlanAttribut']) ? $data['VlanAttribut'] : null,
            'NativeVlanEth1' => isset($data['NativeVlanEth1']) ? (int)$data['NativeVlanEth1'] : null,
            'NativeVlanEth2' => isset($data['NativeVlanEth2']) ? (int)$data['NativeVlanEth2'] : null,
            'NativeVlanEth3' => isset($data['NativeVlanEth3']) ? (int)$data['NativeVlanEth3'] : null,
            'NativeVlanEth4' => isset($data['NativeVlanEth4']) ? (int)$data['NativeVlanEth4'] : null,
            'ClientStatus' => "Active",
            'Remark' => isset($data['Remark']) ? $data['Remark'] : null,
            'Response' => isset($data['Response']) ? $data['Response'] : null,
            'UpdatedDate' => date('Y-m-d H:i:s'),
            'CreatedBy' => sess_user("name"),
            'CreatedDate' => date('Y-m-d H:i:s'),
        ];
          // dd($data);
        $response = $client->request('POST', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Add Clients]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
        return json_decode($response->getBody()->getContents());
    }

    public function post_list($datas) {
        $url = env('IKB_OLT_NOC') . '/clients?list=all';
        $client = new Client(['http_errors' => false]);
        $data["data"] = $datas;
        $response = $client->request('POST',  $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);


        // dd(json_decode($response->getBody()->getContents()));
        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Add Clients]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
        return json_decode($response->getBody()->getContents());
    }

    public function put($code, $data)
    {
        // Step Simpan DB
        $url = env('IKB_OLT_NOC') . '/clients/' . $code;
        $client = new Client(['http_errors' => false]);
        $data = [
            'Code' => isset($data['Code']) ? $data['Code'] : null,
            'IspCode' => isset($data['IspCode']) ? $data['IspCode'] : null,
            'HostCode' => isset($data['HostCode']) ? $data['HostCode'] : null,
            'CustomerCode' => isset($data['CustomerCode']) ? $data['CustomerCode'] : null,
            'CustomerName' => isset($data['CustomerName']) ? $data['CustomerName'] : null,
            'CustomerContactPerson' => isset($data['CustomerContactPerson']) ? $data['CustomerContactPerson'] : null,
            'CustomerAddress' => isset($data['CustomerAddress']) ? $data['CustomerAddress'] : null,
            'CustomerCity' => isset($data['CustomerCity']) ? $data['CustomerCity'] : null,
            'CustomerPhone1' => isset($data['CustomerPhone1']) ? $data['CustomerPhone1'] : null,
            'CustomerPhone2' => isset($data['CustomerPhone2']) ? $data['CustomerPhone2'] : null,
            'CustomerFax' => isset($data['CustomerFax']) ? $data['CustomerFax'] : null,
            'CustomerEmail' => isset($data['CustomerEmail']) ? $data['CustomerEmail'] : null,
            'CustomerType' => isset($data['CustomerType']) ? $data['CustomerType'] : null,
            'TemplateCode' => isset($data['TemplateCode']) ? $data['TemplateCode'] : null,
            'VlanDownLink' => isset($data['VlanDownLink']) ? (int)$data['VlanDownLink'] : null,
            'FrameId' => isset($data['FrameId']) ? (int)$data['FrameId'] : null,
            'SlotId' => isset($data['SlotId']) ? (int)$data['SlotId'] : null,
            'PortId' => isset($data['PortId']) ? (int)$data['PortId'] : null,
            'OntVersion' => isset($data['OntVersion']) ? $data['OntVersion'] : null,
            'OntSoftware' => isset($data['OntSoftware']) ? $data['OntSoftware'] : null,
            'OntSn' => isset($data['Sn']) ? $data['Sn'] : null,
            'OntId' => isset($data['OntId']) ? (int)$data['OntId'] : null,
            'OntRun' => isset($data['OntRun']) ? $data['OntRun'] : null,
            // 'LasDownCause' => isset($data['LasDownCause']) ? $data['LasDownCause'] : null,
            // 'lastUpTime' => isset($data['lastUpTime']) ? $data['lastUpTime'] : null,
            // 'lasDownTime' => isset($data['lasDownTime']) ? $data['lasDownTime'] : null,
            // 'lasDyingGasp' => isset($data['lasDyingGasp']) ? $data['lasDyingGasp'] : null,
            // 'ontRx' => isset($data['ontRx']) ? $data['ontRx'] : null,
            // 'ontRxPower' => isset($data['ontRxPower']) ? $data['ontRxPower'] : null,
            // 'ontDistance' => isset($data['ontDistance']) ? $data['ontDistance'] : null,
            // 'serviceType' => isset($data['serviceType']) ? $data['serviceType'] : null,
            // 'connectionType' => isset($data['connectionType']) ? $data['connectionType'] : null,
            // 'ipv4ConnectionStatus' => isset($data['ipv4ConnectionStatus']) ? $data['ipv4ConnectionStatus'] : null,
            // 'portState' => isset($data['portState']) ? $data['portState'] : null,
            // 'statusPort' => isset($data['statusPort']) ? $data['statusPort'] : null,
            // 'ontOnlineDuration' => isset($data['ontOnlineDuration']) ? $data['ontOnlineDuration'] : null,
            'OntLineProfileId' => isset($data['OntLineProfileId']) ? (int)$data['OntLineProfileId'] : null,
            'OntSrvProfileId' => isset($data['OntSrvProfileId']) ? (int)$data['OntSrvProfileId'] : null,
            'VlanId' => isset($data['VlanId']) ? $data['VlanId'] : null,
            'VlanFrameId' => isset($data['VlanFrameId']) ? (int)$data['VlanFrameId'] : null,
            'VlanSlotId' => isset($data['VlanSlotId']) ? (int)$data['VlanSlotId'] : null,
            'VlanPortId' => isset($data['VlanPortId']) ? (int)$data['VlanPortId'] : null,
            'FdtNo' => isset($data['FdtNo']) ? $data['FdtNo'] : null,
            'FatNo' => isset($data['FatNo']) ? $data['FatNo'] : null,
            'VlanAttribut' => isset($data['VlanAttribut']) ? $data['VlanAttribut'] : null,
            'NativeVlanEth1' => isset($data['NativeVlanEth1']) ? (int)$data['NativeVlanEth1'] : null,
            'NativeVlanEth2' => isset($data['NativeVlanEth2']) ? (int)$data['NativeVlanEth2'] : null,
            'NativeVlanEth3' => isset($data['NativeVlanEth3']) ? (int)$data['NativeVlanEth3'] : null,
            'NativeVlanEth4' => isset($data['NativeVlanEth4']) ? (int)  $data['NativeVlanEth4'] : null,
            'ClientStatus' => isset($data['ClientStatus']) ? $data['ClientStatus'] : null,
            'Remark' => isset($data['Remark']) ? $data['Remark'] : null,
            'Response' => isset($data['Response']) ? $data['Response'] : null,
            'UpdatedBy' => 'admin',
            'UpdatedDate' => date('Y-m-d H:i:s'),
        ];
        $response = $client->request('PUT', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Update Clients]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
        return json_decode($response->getBody()->getContents());
    }

    public function put_spesific($code, $data)
    {
        // Step Simpan DB
        $url = env('IKB_OLT_NOC') . '/clients/' . $code;
        $client = new Client(['http_errors' => false]);
        $response = $client->request('PUT', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Update Clients]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
        return json_decode($response->getBody()->getContents());
    }

    public function delete($code)
    {
        $url = env('IKB_OLT_NOC') . '/clients/' . $code;
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('delete', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Deleted Clients]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }
}
