<?php

namespace App\Services\Olt;

use GuzzleHttp\Client;
use App\Services\Signature;
use Illuminate\Support\Facades\Log;

class ClientsTools {

    public function getAll() {
        $url = env('IKB_OLT_NOC') . '/clients-tools?limit=10000&page=1';
        $signature = (new Signature($url))->create();
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Get All ClientsTools]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function getAllActive($id) {
        $url = env('IKB_OLT_NOC') . '/clients-tools?limit=10000&page=1&ActiveStatus=1';
        $signature = (new Signature($url))->create();
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Get All ClientsTools]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function show($code) {
        $url = env('IKB_OLT_NOC') . '/clients-tools/' . $code;
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Show ClientsTools]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function post($data) {
      // dd($data["ont-version"]);
        $url = env('IKB_OLT_NOC') . '/clients-tools';
        $client = new Client(['http_errors' => false]);
        $data = [
            'Code' => $data['Code'],
            'ClientsCode' => $data['Code'],
            'Version' => isset ($data['Version']) ? $data['Version'] : null,
            'OntInfo' => isset ($data['OntInfo']) ? $data['OntInfo'] : null,
            'WanInfo' => isset ($data['WanInfo']) ? $data['WanInfo'] : null,
            'Opticalinfo' => isset ($data['Opticalinfo']) ? $data['Opticalinfo'] : null,
            'Registerinfo' => isset ($data['Registerinfo']) ? $data['Registerinfo'] : null,
            'MacAddress' => isset ($data['MacAddress']) ? $data['MacAddress'] : null,
            'PortState' => isset ($data['PortState']) ? $data['PortState'] : null,
            'PortAttribute' => isset ($data['PortAttribute']) ? $data['PortAttribute'] : null,
            'FecCrcError' => isset ($data['FecCrcError']) ? $data['FecCrcError'] : null,
            'ActiveStatus' => isset ($data['ActiveStatus']) ? $data['ActiveStatus'] : null,
            'CreatedBy' => "admin",
            'CreatedDate' => date('Y-m-d H:i:s')
        ];

        $response = $client->request('POST', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);
        // dd(json_decode($response->getBody()->getContents()));

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Add ClientsTools]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

    public function post_list($datas) {
        $url = env('IKB_OLT_NOC') . '/clients-tools?list=all';
        $client = new Client(['http_errors' => false]);
        $data["data"] = $datas;
        $response = $client->request('POST',  $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Add ClientsTools]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
        return json_decode($response->getBody()->getContents());
    }

    public function put($code, $data) {
        $url = env('IKB_OLT_NOC') . '/clients-tools/' . $code;

        $client = new Client(['http_errors' => false]);
        $data = [
            'Code' => $data->Code,
            'ClientsCode' => $data->ClientsCode,
            'Version' => isset ($data->Version) ? $data->Version : null,
            'OntInfo' => isset ($data->OntInfo) ? $data->OntInfo : null,
            'WanInfo' => isset ($data->WanInfo) ? $data->WanInfo : null,
            'Opticalinfo' => isset ($data->Opticalinfo) ? $data->Opticalinfo : null,
            'Registerinfo' => isset ($data->Registerinfo) ? $data->Registerinfo : null,
            'MacAddress' => isset ($data->MacAddress) ? $data->MacAddress : null,
            'PortState' => isset ($data->PortState) ? $data->PortState : null,
            'PortAttribute' => isset ($data->PortAttribute) ? $data->PortAttribute : null,
            'FecCrcError' => isset ($data->FecCrcError) ? $data->FecCrcError : null,
            'ActiveStatus' => isset ($data->ActiveStatus) ? $data->ActiveStatus : null,
            'UpdatedBy' => "admin",
            'UpdatedDate' => date('Y-m-d H:i:s'),
        ];

        $response = $client->request('PUT', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);
        // dd(json_decode($response->getBody()->getContents()));

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Update ClientsTools]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

    public function delete($code) {
        $url = env('IKB_OLT_NOC') . '/clients-tools/' . $code;
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('delete', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Deleted ClientsTools]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

}
