<?php

namespace App\Services\Olt;

use GuzzleHttp\Client;
use App\Services\Signature;
use Illuminate\Support\Facades\Log;

class TrafficTableLast {

    public function getAll() {
        $url = env('IKB_OLT_NOC') . '/traffic-table-last?limit=10000&page=1';
        $signature = (new Signature($url))->create();
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Get All TrafficTableLast]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function getAllActive($code) {
        $url = env('IKB_OLT_NOC') . '/traffic-table-last?limit=10000&page=1&ActiveStatus=1&HostCode='.$code;
        $signature = (new Signature($url))->create();
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Get All TrafficTableLast]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function show($code) {
        $url = env('IKB_OLT_NOC') . '/traffic-table-last/' . $code;
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Show TrafficTableLast]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function post($data) {
        $url = env('IKB_OLT_NOC') . '/traffic-table-last';
        $client = new Client(['http_errors' => false]);
        $data = [
            'Code' => $data['Code'],
            'HostCode' => isset ($data['HostCode']) ? $data['HostCode'] : null,
            'Index' => isset ($data['Index']) ? $data['Index'] : null,
            'Name' => isset ($data['Name']) ? $data['Name'] : null,
            'Cir' => isset ($data['Cir']) ? $data['Cir'] : null,
            'Cbs' => isset ($data['Cbs']) ? $data['Cbs'] : null,
            'Pir' => isset ($data['Pir']) ? $data['Pir'] : null,
            'Pbs' => isset ($data['Pbs']) ? $data['Pbs'] : null,
            'Priority' => isset ($data['Priority']) ? $data['Priority'] : null,
            'ActiveStatus' => isset ($data['ActiveStatus']) ? $data['ActiveStatus'] : null,
            'CreatedBy' => "admin",
            'CreatedDate' => date('Y-m-d H:i:s'),
        ];

        $response = $client->request('POST', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Add TrafficTableLast]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

    public function post_list($datas) {
        $url = env('IKB_OLT_NOC') . '/traffic-table-last?list=all';
        $client = new Client(['http_errors' => false]);
        $list = array();
        foreach ($datas as $data_val) {
            $dt = new \stdClass();
            $dt->Code = $data_val->Code;
            $dt->HostCode = $data_val->HostCode;
            $dt->Index = $data_val->Index;
            $dt->Name = $data_val->Name;
            $dt->Cir = $data_val->Cir;
            $dt->Cbs = $data_val->Cbs;
            $dt->Pir =$data_val->Pir;
            $dt->Pbs =$data_val->Pbs;
            $dt->Priority =$data_val->Priority;
            $dt->ActiveStatus =  1;
            $dt->CreatedBy = "admin";
            $dt->CreatedDate = date('Y-m-d H:i:s');
            $dt->UpdatedBy = "admin";
            $dt->UpdatedDate = date('Y-m-d H:i:s');
            $dt->InActiveBy = "admin";
            $dt->InActiveDate = date('Y-m-d H:i:s');
            array_push($list,$dt);
        }
        $data["data"] = $list;
        $response = $client->request('POST',  $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);
    }

    public function put($code, $data) {
        $url = env('IKB_OLT_NOC') . '/traffic-table-last/' . $code;

        $client = new Client(['http_errors' => false]);
        $data = [
            'Code' => $data['Code'],
            'HostCode' => isset ($data['HostCode']) ? $data['HostCode'] : null,
            'Index' => isset ($data['Index']) ? $data['Index'] : null,
            'Name' => isset ($data['Name']) ? $data['Name'] : null,
            'Cir' => isset ($data['Cir']) ? $data['Cir'] : null,
            'Cbs' => isset ($data['Cbs']) ? $data['Cbs'] : null,
            'Pir' => isset ($data['Pir']) ? $data['Pir'] : null,
            'Pbs' => isset ($data['Pbs']) ? $data['Pbs'] : null,
            'Priority' => isset ($data['Priority']) ? $data['Priority'] : null,
            'ActiveStatus' => isset ($data['ActiveStatus']) ? $data['ActiveStatus'] : null,
            'UpdatedBy' => "admin",
            'UpdatedDate' => date('Y-m-d H:i:s'),
        ];

        $response = $client->request('PUT', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Update TrafficTableLast]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

    public function delete_host($code) {
        $url = env('IKB_OLT_NOC') . '/traffic-table-last/' . $code. '?HostCode='.$code;
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('delete', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Deleted TrafficTable]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

    public function delete($code) {
        $url = env('IKB_OLT_NOC') . '/traffic-table-last/' . $code;
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('delete', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Deleted TrafficTableLast]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

}
