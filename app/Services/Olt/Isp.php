<?php

namespace App\Services\Olt;

use GuzzleHttp\Client;
use App\Services\Signature;
use Illuminate\Support\Facades\Log;

class Isp {

    public function getAll($request) {
        $search = ($request->search) ? $request->search['value']:null;
        $from_date = ($request->from_date) ? $request->from_date:null;
        $to_date = ($request->to_date) ? $request->to_date:null;
        $limit = ($request->length) ? $request->length:intval(global_limit());
        $page = ($request->length) ? (int) ($request->start/$request->length)+1:1;
        $url = env('IKB_OLT_NOC') . '/isp?limit='.$limit.'&page='.$page.'&from_date='.$from_date.'&to_date='.$to_date.'&search='.$search;
        $signature = (new Signature($url))->create();
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Get All Isp]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function getAllActive() {
        $url = env('IKB_OLT_NOC') . '/isp?limit=10000&page=1&ActiveStatus=1';
        $signature = (new Signature($url))->create();
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Get All Host]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function show($code) {
        $url = env('IKB_OLT_NOC') . '/isp/' . $code;
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Show Isp]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function post($data) {
        $url = env('IKB_OLT_NOC') . '/isp';
        $client = new Client(['http_errors' => false]);
        $data = [
            'Code' => isset($data['Code']) ? $data['Code'] : null,
            'LinkID' => isset($data['LinkID']) ? $data['LinkID'] : null,
            'Name' => isset($data['Name']) ? $data['Name'] : null,
            'ContactPerson' => isset($data['ContactPerson']) ? $data['ContactPerson'] : null,
            'Address' => isset($data['Address']) ? $data['Address'] : null,
            'CityCode' => isset($data['CityCode']) ? $data['CityCode'] : null,
            'Phone1' => isset($data['Phone1']) ? $data['Phone1'] : null,
            'Phone2' => isset($data['Phone2']) ? $data['Phone2'] : null,
            'Fax' => isset($data['Fax']) ? $data['Fax'] : null,
            'Email' => isset($data['Email']) ? $data['Email'] : null,
            'ActiveStatus' => isset($data['ActiveStatus']) ? $data['ActiveStatus'] : null,
            'CreatedBy' => "admin",
            'CreatedDate' => date('Y-m-d H:i:s'),
        ];

        $response = $client->request('POST',  $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        // dd($response);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Add Isp]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

    public function post_list($datas) {
        $url = env('IKB_OLT_NOC') . '/isp?list=all';
        $client = new Client(['http_errors' => false]);
        $data["data"] = $datas;
        $response = $client->request('POST',  $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        // dd($data);
        // dd(json_decode($response->getBody()->getContents()));

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Add Isp]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
        return json_decode($response->getBody()->getContents());
    }

    public function put($code, $data) {
        $url = env('IKB_OLT_NOC') . '/isp/' . $code;

        $client = new Client(['http_errors' => false]);
        $data = [
            'Code' => isset($data['Code']) ? $data['Code'] : null,
            'Name' => isset($data['Name']) ? $data['Name'] : null,
            'LinkID' => isset($data['LinkID']) ? $data['LinkID'] : null,
            'ContactPerson' => isset($data['ContactPerson']) ? $data['ContactPerson'] : null,
            'Address' => isset($data['Address']) ? $data['Address'] : null,
            'CityCode' => isset($data['CityCode']) ? $data['CityCode'] : null,
            'Phone1' => isset($data['Phone1']) ? $data['Phone1'] : null,
            'Phone2' => isset($data['Phone2']) ? $data['Phone2'] : null,
            'Fax' => isset($data['Fax']) ? $data['Fax'] : null,
            'Email' => isset($data['Email']) ? $data['Email'] : null,
            'ActiveStatus' => isset($data['ActiveStatus']) ? $data['ActiveStatus'] : null,
            'UpdatedBy' => 'admin',
            'UpdatedDate' => date('Y-m-d H:i:s'),
        ];

        $response = $client->request('PUT', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Update Isp]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

    public function delete($code) {
        $url = env('IKB_OLT_NOC') . '/isp/' . $code;
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('delete', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Deleted Isp]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

}
