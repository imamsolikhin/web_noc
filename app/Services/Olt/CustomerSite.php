<?php

namespace App\Services\Olt;

use GuzzleHttp\Client;
use App\Services\Signature;
use Illuminate\Support\Facades\Log;

class CustomerSite {

    public function getAll() {
        $url = env('IKB_OLT_NOC') . '/customer-site?limit=10000&page=1';
        $signature = (new Signature($url))->create();
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Get All CustomerSite]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function getAllActive() {
        $url = env('IKB_OLT_NOC') . '/customer-site?limit=10000&page=1&ActiveStatus=1';
        $signature = (new Signature($url))->create();
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Get All CustomerSite]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function show($code) {
        $url = env('IKB_OLT_NOC') . '/customer-site/' . $code;
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Show CustomerSite]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function post($data) {
        $url = env('IKB_OLT_NOC') . '/isp';
        $client = new Client(['http_errors' => false]);
        $data = [
            'Code' => $data['Code'],
            'LinkID' => isset($data['LinkID']) ? $data['LinkID'] : null,
            'IDPelanggan' => isset($data['IDPelanggan']) ? $data['IDPelanggan'] : null,
            'CompanyName' => isset($data['CompanyName']) ? $data['CompanyName'] : null,
            'Province' => isset($data['Province']) ? $data['Province'] : null,
            'City' => isset($data['City']) ? $data['City'] : null,
            'Address' => isset($data['Address']) ? $data['Address'] : null,
            'ZipCode' => isset($data['ZipCode']) ? $data['ZipCode'] : null,
            'Website' => isset($data['Website']) ? $data['Website'] : null,
            'Email' => isset($data['Email']) ? $data['Email'] : null,
            'Phone' => isset($data['Phone']) ? $data['Phone'] : null,
            'Fax' => isset($data['Fax']) ? $data['Fax'] : null,
            'ProductSite' => isset($data['ProductSite']) ? $data['ProductSite'] : null,
            'ProductGrup' => isset($data['ProductGrup']) ? $data['ProductGrup'] : null,
            'ProductPort' => isset($data['ProductPort']) ? $data['ProductPort'] : null,
            'ProductCategory' => isset($data['ProductCategory']) ? $data['ProductCategory'] : null,
            'ProductName' => isset($data['ProductName']) ? $data['ProductName'] : null,
            'ProductNotes' => isset($data['ProductNotes']) ? $data['ProductNotes'] : null,
            'ActiveStatus' => isset($data['ActiveStatus']) ? $data['ActiveStatus'] : null,
            'CreatedBy' => "admin",
            'CreatedDate' => date('Y-m-d H:i:s'),
        ];

        $response = $client->request('POST',  $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        // dd($response);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Add CustomerSite]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

    public function post_list($datas) {
        $url = env('IKB_OLT_NOC') . '/customer-site?list=all';
        $client = new Client(['http_errors' => false]);
        $data["data"] = $datas;
        $response = $client->request('POST',  $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Add CustomerSite]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
        return json_decode($response->getBody()->getContents());
    }

    public function put($code, $data) {
        $url = env('IKB_OLT_NOC') . '/isp/' . $code;

        $client = new Client(['http_errors' => false]);
        $data = [
            'Code' => $data['Code'],
            'LinkID' => isset($data['LinkID']) ? $data['LinkID'] : null,
            'IDPelanggan' => isset($data['IDPelanggan']) ? $data['IDPelanggan'] : null,
            'CompanyName' => isset($data['CompanyName']) ? $data['CompanyName'] : null,
            'Province' => isset($data['Province']) ? $data['Province'] : null,
            'City' => isset($data['City']) ? $data['City'] : null,
            'Address' => isset($data['Address']) ? $data['Address'] : null,
            'ZipCode' => isset($data['ZipCode']) ? $data['ZipCode'] : null,
            'Website' => isset($data['Website']) ? $data['Website'] : null,
            'Email' => isset($data['Email']) ? $data['Email'] : null,
            'Phone' => isset($data['Phone']) ? $data['Phone'] : null,
            'Fax' => isset($data['Fax']) ? $data['Fax'] : null,
            'ProductSite' => isset($data['ProductSite']) ? $data['ProductSite'] : null,
            'ProductGrup' => isset($data['ProductGrup']) ? $data['ProductGrup'] : null,
            'ProductPort' => isset($data['ProductPort']) ? $data['ProductPort'] : null,
            'ProductCategory' => isset($data['ProductCategory']) ? $data['ProductCategory'] : null,
            'ProductName' => isset($data['ProductName']) ? $data['ProductName'] : null,
            'ProductNotes' => isset($data['ProductNotes']) ? $data['ProductNotes'] : null,
            'ActiveStatus' => isset($data['ActiveStatus']) ? $data['ActiveStatus'] : null,
            'UpdatedBy' => 'admin',
            'UpdatedDate' => date('Y-m-d H:i:s'),
        ];

        $response = $client->request('PUT', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Update CustomerSite]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

    public function delete($code) {
        $url = env('IKB_OLT_NOC') . '/customer-site/' . $code;
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('delete', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Deleted CustomerSite]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

}
