<?php

namespace App\Services\Olt;

use GuzzleHttp\Client;
use App\Services\Signature;
use Illuminate\Support\Facades\Log;

class TemplateDetail {

    public function getAll($headercode) {
        $url = env('IKB_OLT_NOC') . '/template-detail?limit=10000&page=1';
        $signature = (new Signature($url))->create();
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Get All TemplateDetail]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function getAllActive($headercode) {
        $url = env('IKB_OLT_NOC') . '/template-detail?limit=10000&page=1&ActiveStatus=1&HeaderCode=' .$headercode;
        $signature = (new Signature($url))->create();
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Get All Host]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function show($code) {
        $url = env('IKB_OLT_NOC') . '/template-detail/' . $code;
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Show TemplateDetail]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function post($data) {
        $url = env('IKB_OLT_NOC') . '/template-detail';
        $client = new Client(['http_errors' => false]);
        $data = [
            'Code' => $data['Code'],
            'HeaderCode' => isset($data['HeaderCode']) ? $data['HeaderCode'] : null,
            'Vlan' => isset($data['Vlan']) ? $data['Vlan'] : null,
            'UserVlan' => isset($data['UserVlan']) ? $data['UserVlan'] : null,
            'InnerVlan' => isset($data['InnerVlan']) ? $data['InnerVlan'] : null,
            'TagTransform' => isset($data['TagTransform']) ? $data['TagTransform'] : null,
            'TrafficTable' => isset($data['TrafficTable']) ? $data['TrafficTable'] : null,
            'GemPort' => isset($data['GemPort']) ? $data['GemPort'] : null,
            'Remark' => isset($data['Remark']) ? $data['Remark'] : null,
            'CreatedBy' => "admin",
            'CreatedDate' => date('Y-m-d H:i:s'),
        ];
        // dd($data);
        $response = $client->request('POST',  $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        // dd($data);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Add TemplateDetail]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

    public function post_list($datas) {
        $url = env('IKB_OLT_NOC') . '/template-detail?list=all';
        $client = new Client(['http_errors' => false]);
        $data["data"] = $datas;
        $response = $client->request('POST',  $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        // dd($data);
        // dd(json_decode($response->getBody()->getContents()));

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Add TemplateDetail]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
        return json_decode($response->getBody()->getContents());
    }

    public function put($code, $data) {
        $url = env('IKB_OLT_NOC') . '/template-detail/' . $code;

        $client = new Client(['http_errors' => false]);
        $data = [
            'Code' => $data['Code'],
            'HeaderCode' => isset($data['HeaderCode']) ? $data['HeaderCode'] : null,
            'Vlan' => isset($data['Vlan']) ? $data['Vlan'] : null,
            'UserVlan' => isset($data['UserVlan']) ? $data['UserVlan'] : null,
            'InnerVlan' => isset($data['InnerVlan']) ? $data['InnerVlan'] : null,
            'TagTransform' => isset($data['TagTransform']) ? $data['TagTransform'] : null,
            'TrafficTable' => isset($data['TrafficTable']) ? $data['TrafficTable'] : null,
            'GemPort' => isset($data['GemPort']) ? $data['GemPort'] : null,
            'Remark' => isset($data['Remark']) ? $data['Remark'] : null,
            'UpdatedBy' => 'admin',
            'UpdatedDate' => date('Y-m-d H:i:s'),
        ];

        $response = $client->request('PUT', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Update TemplateDetail]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

    public function delete($id) {
        $url = env('IKB_OLT_NOC') . '/template-detail/' .$id.'?HeaderCode='.$id;
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('delete', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Deleted TemplateDetail]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

}
