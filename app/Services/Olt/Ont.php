<?php

namespace App\Services\Olt;

use GuzzleHttp\Client;
use App\Services\Signature;
use Illuminate\Support\Facades\Log;

class Ont {

    public function getAll($request) {
        $search = ($request->search) ? $request->search['value']:null;
        $from_date = ($request->from_date) ? $request->from_date:null;
        $to_date = ($request->to_date) ? $request->to_date:null;
        $limit = ($request->length) ? $request->length:intval(global_limit());
        $page = ($request->length) ? (int) ($request->start/$request->length)+1:1;
        $url = env('IKB_OLT_NOC') . '/ont?limit='.$limit.'&page='.$page.'&from_date='.$from_date.'&to_date='.$to_date.'&search='.$search;
        $signature = (new Signature($url))->create();
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $url, [
          'headers' => [
              'Accept' => 'application/json',
              'Signature' => $signature
          ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Get All Ont]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function getAllActive($HostCode) {
        $url = env('IKB_OLT_NOC') . '/ont?limit=10000&page=1&ActiveStatus=1&HostCode='.$HostCode;
        $signature = (new Signature($url))->create();
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Get All Host]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function show($code) {
        $url = env('IKB_OLT_NOC') . '/ont/' . $code;
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Show Ont]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function show_sn($hostcode, $sn) {
        $url = env('IKB_OLT_NOC') . '/ont/' . $sn.'?HostCode='.$hostcode.'&findsn=true';
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Show Ont]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function post($data) {
        $url = env('IKB_OLT_NOC') . '/ont';
        $client = new Client(['http_errors' => false]);
        $data = [
            'Code' => $data['Code'],
            'HostCode' => isset($data['HostCode']) ? $data['HostCode'] : null,
            'Sn' => isset($data['Sn']) ? $data['Sn'] : null,
            'Version' => isset($data['Version']) ? $data['Version'] : null,
            'SoftwareVersion' => isset($data['SoftwareVersion']) ? $data['SoftwareVersion'] : null,
            'EquipmentId' => isset($data['EquipmentId']) ? $data['EquipmentId'] : null,
            'FrameId' => isset($data['FrameId']) ? $data['FrameId'] : null,
            'SlotId' => isset($data['SlotId']) ? $data['SlotId'] : null,
            'PortId' => isset($data['PortId']) ? $data['PortId'] : null,
            'RxPower' => isset($data['RxPower']) ? $data['RxPower'] : null,
            'TxPower' => isset($data['TxPower']) ? $data['TxPower'] : null,
            'Remark' => isset($data['Remark']) ? $data['Remark'] : null,
            'ActiveStatus' => isset($data['ActiveStatus']) ? $data['ActiveStatus'] : null,
            'CreatedBy' => "admin",
            'CreatedDate' => date('Y-m-d H:i:s'),
        ];

        $response = $client->request('POST',  $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        // dd($response);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Add Ont]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

    public function post_list($datas) {
        $url = env('IKB_OLT_NOC') . '/ont?list=all';
        $client = new Client(['http_errors' => false]);
        $list = array();
        $i = 1;
        foreach ($datas as $data_val) {
            $dt = new \stdClass();
            $dt->Code = $data_val->ont_sn.'-'.$data_val->host.'-'.($i++);
            $dt->HostCode = $data_val->host;
            $dt->Sn = $data_val->ont_sn;
            $dt->Version = $data_val->version;
            $dt->SoftwareVersion = $data_val->software;
            $dt->EquipmentId = $data_val->equipment;
            $dt->FrameId = (int)$data_val->frame;
            $dt->SlotId = (int)$data_val->slot;
            $dt->PortId = (int)$data_val->port;
            $dt->RxPower = 0;
            $dt->TxPower = 0;
            $dt->Remark = "";
            $dt->ActiveStatus =  1;
            $dt->CreatedBy = "admin";
            $dt->CreatedDate = date('Y-m-d H:i:s');
            $dt->UpdatedBy = "admin";
            $dt->UpdatedDate = date('Y-m-d H:i:s');
            $dt->InActiveBy = "admin";
            $dt->InActiveDate = date('Y-m-d H:i:s');
            array_push($list,$dt);
        }
        $data["data"] = $list;

        $response = $client->request('POST',  $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                // 'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Add Ont]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

    public function put($code, $data) {
        $url = env('IKB_OLT_NOC') . '/ont/' . $code;

        $client = new Client(['http_errors' => false]);
        $data = [
            'Code' => $data['Code'],
            'HostCode' => isset($data['HostCode']) ? $data['HostCode'] : null,
            'Sn' => isset($data['Sn']) ? $data['Sn'] : null,
            'Version' => isset($data['Version']) ? $data['Version'] : null,
            'SoftwareVersion' => isset($data['SoftwareVersion']) ? $data['SoftwareVersion'] : null,
            'EquipmentId' => isset($data['EquipmentId']) ? $data['EquipmentId'] : null,
            'FrameId' => isset($data['FrameId']) ? $data['FrameId'] : null,
            'SlotId' => isset($data['SlotId']) ? $data['SlotId'] : null,
            'PortId' => isset($data['PortId']) ? $data['PortId'] : null,
            'RxPower' => isset($data['RxPower']) ? $data['RxPower'] : null,
            'TxPower' => isset($data['TxPower']) ? $data['TxPower'] : null,
            'Remark' => isset($data['Remark']) ? $data['Remark'] : null,
            'ActiveStatus' => isset($data['ActiveStatus']) ? $data['ActiveStatus'] : null,
            'UpdatedBy' => 'admin',
            'UpdatedDate' => date('Y-m-d H:i:s'),
        ];

        $response = $client->request('PUT', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Update Ont]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

    public function delete($code) {
        $url = env('IKB_OLT_NOC') . '/ont/' . $code;
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('delete', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Deleted Ont]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

    public function delete_host($code) {
        $url = env('IKB_OLT_NOC') . '/ont/' . $code. '?HostCode='.$code;
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('delete', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Deleted Ont]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

    public function delete_all() {
        $url = env('IKB_OLT_NOC') . '/ont/all?Config=1';
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('delete', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Deleted Ont]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

}
