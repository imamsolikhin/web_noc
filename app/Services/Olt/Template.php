<?php

namespace App\Services\Olt;

use GuzzleHttp\Client;
use App\Services\Signature;
use Illuminate\Support\Facades\Log;

class Template {

    public function getAll($request) {
        $search = ($request->search) ? $request->search['value']:null;
        $from_date = ($request->from_date) ? $request->from_date:null;
        $to_date = ($request->to_date) ? $request->to_date:null;
        $limit = ($request->length) ? $request->length:intval(DT_LIMIT);
        $page = ($request->length) ? (int) ($request->start/$request->length)+1:1;
        $url = env('IKB_OLT_NOC') . '/template?limit='.$limit.'&page='.$page.'&from_date='.$from_date.'&to_date='.$to_date.'&search='.$search;
        $signature = (new Signature($url))->create();
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Get All Template]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function getAllActive() {
        $url = env('IKB_OLT_NOC') . '/template?limit=10000&page=1&ActiveStatus=1';
        $signature = (new Signature($url))->create();
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Get All Host]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function show($code) {
        $url = env('IKB_OLT_NOC') . '/template/' . $code;
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Show Template]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function post($data) {
        $url = env('IKB_OLT_NOC') . '/template';
        $client = new Client(['http_errors' => false]);
        $data = [
            'Code' => $data['Code'],
            'Name' => isset($data['Name']) ? $data['Name'] : null,
            'VlanDownLink' => isset($data['VlanDownLink']) ? $data['VlanDownLink'] : null,
            'OntLineProfileId' => isset($data['OntLineProfileId']) ? $data['OntLineProfileId'] : null,
            'OntSrvProfileId' => isset($data['OntSrvProfileId']) ? $data['OntSrvProfileId'] : null,
            'NativeVlanEth1' => isset($data['NativeVlanEth1']) ? $data['NativeVlanEth1'] : null,
            'NativeVlanEth2' => isset($data['NativeVlanEth2']) ? $data['NativeVlanEth2'] : null,
            'NativeVlanEth3' => isset($data['NativeVlanEth3']) ? $data['NativeVlanEth3'] : null,
            'NativeVlanEth4' => isset($data['NativeVlanEth4']) ? $data['NativeVlanEth4'] : null,
            'VlanAttribut' => isset($data['VlanAttribut']) ? $data['VlanAttribut'] : null,
            'Remark' => isset($data['Remark']) ? $data['Remark'] : null,
            'CreatedBy' => "admin",
            'CreatedDate' => date('Y-m-d H:i:s'),
        ];

        $response = $client->request('POST',  $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Add Template]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
        $contents = json_decode($response->getBody()->getContents());
        return $contents;
    }

    public function post_list($datas) {
        $url = env('IKB_OLT_NOC') . '/template?list=all';
        $client = new Client(['http_errors' => false]);
        $data["data"] = $datas;
        $response = $client->request('POST',  $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        // dd($data);
        // dd(json_decode($response->getBody()->getContents()));

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Add Template]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
        return json_decode($response->getBody()->getContents());
    }

    public function put($code, $data) {
        $url = env('IKB_OLT_NOC') . '/template/' . $code;

        $client = new Client(['http_errors' => false]);
        $data = [
            'Code' => $data['Code'],
            'Name' => isset($data['Name']) ? $data['Name'] : null,
            'VlanDownLink' => isset($data['VlanDownLink']) ? $data['VlanDownLink'] : null,
            'OntLineProfileId' => isset($data['OntLineProfileId']) ? $data['OntLineProfileId'] : null,
            'OntSrvProfileId' => isset($data['OntSrvProfileId']) ? $data['OntSrvProfileId'] : null,
            'NativeVlanEth1' => isset($data['NativeVlanEth1']) ? $data['NativeVlanEth1'] : null,
            'NativeVlanEth2' => isset($data['NativeVlanEth2']) ? $data['NativeVlanEth2'] : null,
            'NativeVlanEth3' => isset($data['NativeVlanEth3']) ? $data['NativeVlanEth3'] : null,
            'NativeVlanEth4' => isset($data['NativeVlanEth4']) ? $data['NativeVlanEth4'] : null,
            'VlanAttribut' => isset($data['VlanAttribut']) ? $data['VlanAttribut'] : null,
            'Remark' => isset($data['Remark']) ? $data['Remark'] : null,
            'UpdatedBy' => 'admin',
            'UpdatedDate' => date('Y-m-d H:i:s'),
        ];

        $response = $client->request('PUT', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Update Template]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
        $contents = json_decode($response->getBody()->getContents());
        return $contents;
    }

    public function delete($code) {
        $url = env('IKB_OLT_NOC') . '/template/' . $code;
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('delete', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[OLT IKB  NOC API - Deleted Template]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

}
