<?php

namespace App\Services\Ticketing;

use Carbon\Carbon;
use GuzzleHttp\Client;
use App\Services\Signature;
use Illuminate\Support\Facades\Log;

class FMITicketStatus
{
    public function getAll($code=null)
    {
        $url =env('IKB_TCK_NOC') . '/fmi-ticket-status'.(!empty($code)?'?search='.$code:'');
        $signature = (new Signature($url))->create();

        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);
        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[RLM IKB  NOC API - Get All FMI Open Ticket]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function show($code)
    {
        $url =env('IKB_TCK_NOC') . '/fmi-ticket-status/'.$code;
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[RLM IKB  NOC API - Show Bandwidth Package]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function post($data)
    {
        $url =env('IKB_TCK_NOC') . '/fmi-ticket-status';

        $client = new Client(['http_errors' => false]);

        $code = $data['code'];
        $transactionDate = Carbon::parse($data['TransactionDate'])->format('Y-m-d H:i:s');
        $BranchCode = $data['BranchCode'];
        $TicketCode = $data['TicketCode'];
        $TicketStatus = $data['TicketStatus'];
        $RefNo = $data['RefNo'];
        $Remark = $data['Remark'];

        $data=[
            'code' => $code,
            'TransactionDate' => $transactionDate,
            'BranchCode' => $BranchCode,
            'TicketCode' => $TicketCode,
            'TicketStatus' => $TicketStatus,
            'RefNo' => $RefNo,
            'Remark' => $Remark
        ];

        $response = $client->request('POST', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);


        if ($response->getStatusCode() != 201) {
            Log::error("[RLM IKB  NOC API - Add FMI Open Ticket]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

    public function put($code, $data)
    {
        $url =env('IKB_TCK_NOC') . '/fmi-ticket-status/'.$code;
        $client = new Client(['http_errors' => false]);
        //$transactionDate = Carbon::parse($data['TransactionDate'])->format('Y-m-d H:i:s');
        //$BranchCode = $data['branch'];
        $RefNo = $data['ts_ref_no'];
        $Remark = $data['ts_Remark'];
        $TicketStatus = $data['TicketStatus'];

        $data=[
            //'TransactionDate' => $transactionDate,
            //'BranchCode' => $BranchCode,
            //'TicketCode' => $code,
            'TicketStatus' => $TicketStatus,
            'RefNo' => $RefNo,
            'Remark' => $Remark
        ];

        $response = $client->request('PUT', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);
        if ($response->getStatusCode() != 200) {
            Log::error("[RLM IKB  NOC API - Update FMI Open Ticket]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

    public function delete($code)
    {
        $url =env('IKB_TCK_NOC') . '/fmi-ticket-status/'.$code;
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('delete', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[RLM IKB  NOC API - Deleted FMI Open Ticket]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

    public function postStatus($data)
    {
        $url =env('IKB_TCK_NOC') . '/fmi-ticket-status';

        $client = new Client(['http_errors' => false]);


        $response = $client->request('POST', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        if ($response->getStatusCode() != 201) {
            Log::error("[RLM IKB  NOC API - Add FMI Open Status]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

    public function showStatus($code=null)
    {
        $url =env('IKB_TCK_NOC') . '/fmi-ticket-status?limit=15&page=1&search='.$code;
        $signature = (new Signature($url))->create();

        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[RLM IKB  NOC API - Get All FMI Ticket Status]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }
}
