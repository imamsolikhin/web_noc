<?php

namespace App\Services\Ticketing;

use Carbon\Carbon;
use GuzzleHttp\Client;
use App\Services\Signature;
use Illuminate\Support\Facades\Log;

class FMIOpenTicket
{

    // created by Septian Ramadhan - itdev.septian@gmail.com
    private function hit_api($api = 'fmi-ticket-open',$param = 'limit=1000000&page=1')
    {
        $url =env('IKB_TCK_NOC') . '/'.$api.'?'.$param;
        $signature = (new Signature($url))->create();

        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);
        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[RLM IKB  NOC API - Get All FMI Ticket Status]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }
    public function getAllTicket($limit=null, $page=null)
    {
        return $this->hit_api('fmi-ticket-open', 'filter_not_equal_ticket_category=NULL&limit=10000000&page=1');
    }
    public function getNonMinorTicket($limit=null, $page=null)
    {
        return $this->hit_api('fmi-ticket-open', 'filter_not_equal_ticket_category=MINOR&limit=10000000&page=1');
    }
    public function getMinorTicket($limit=null, $page=null)
    {
        return $this->hit_api('fmi-ticket-open', 'filter_ticket_category=MINOR&limit=10000000&page=1');
    }
    // end created by Septian Ramadhan




    public function getAll()
    {
        $url =env('IKB_TCK_NOC') . '/fmi-ticket-open?limit=15&page=1';
        $signature = (new Signature($url))->create();

        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[RLM IKB  NOC API - Get All FMI Open Ticket]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function show($code)
    {
        $url =env('IKB_TCK_NOC') . '/fmi-ticket-open/'.$code;
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[RLM IKB  NOC API - Show Bandwidth Package]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function post($data)
    {
        $url =env('IKB_TCK_NOC') . '/fmi-ticket-open';

        $client = new Client(['http_errors' => false]);

        $code = $data['tck_no'];
        $transactionDate = empty(@$data['TransactionDate']) ? date('Y-m-d H:i:s') : Carbon::parse($data['TransactionDate'])->format('Y-m-d H:i:s');
        $BranchCode = $data['BranchCode'];
        $TicketCategory = $data['TicketCategory'];
        $scheduleData = empty(@$data['ScheduleDate']) ? $transactionDate : Carbon::parse($data['ScheduleDate'])->format('Y-m-d H:i:s');
        $ProblemTypeCode = $data['ProblemTypeCode'];
        $CustomerCode = $data['TicketCategory']=='MINOR'?$data['customerCode']:null;
        $ResponsibilityDepartmentCode = $data['ResponsibilityDepartmentCode'];
        $TicketDescription = $data['ticket_description'];
        $RefNo = $data['ref_no'];
        $Remark = $data['Remark'];

        $data=[
            'code' => $code,
            'TransactionDate' => $transactionDate,
            'BranchCode' => $BranchCode,
            'TicketCategory' => $TicketCategory,
            'ScheduleDate' => $scheduleData,
            'ProblemTypeCode' => $ProblemTypeCode,
            'CustomerCode' => $CustomerCode,
            'ResponsibilityDepartmentCode' => $ResponsibilityDepartmentCode,
            'TicketDescription' => $TicketDescription,
            'RefNo' => $RefNo,
            'Remark' => $Remark
        ];

        $response = $client->request('POST', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);


        if ($response->getStatusCode() != 201) {
            Log::error("[RLM IKB  NOC API - Add FMI Open Ticket]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
        return $response;
    }

    public function put($code, $data)
    {
        $url =env('IKB_TCK_NOC') . '/fmi-ticket-open/'.$code;
        $client = new Client(['http_errors' => false]);

        $transactionDate = Carbon::parse($data['TransactionDate'])->format('Y-m-d H:i:s');
        $BranchCode = $data['branch'];
        $TicketCategory = $data['TicketCategory'];
        $scheduleData = Carbon::parse($data['ScheduleDate'])->format('Y-m-d H:i:s');
        $ProblemTypeCode = $data['problemType'];
        $CustomerCode = $data['open_customer'];
        $ResponsibilityDepartmentCode = $data['ResponsibilityDepartmentCode'];
        $TicketDescription = $data['ticket_description'];
        $RefNo = $data['ref_no'];
        $Remark = $data['Remark'];

        $data=[
            'TransactionDate' => $transactionDate,
            'BranchCode' => $BranchCode,
            'TicketCategory' => $TicketCategory,
            'ScheduleDate' => $scheduleData,
            'ProblemTypeCode' => $ProblemTypeCode,
            'CustomerCode' => $CustomerCode,
            'ResponsibilityDepartmentCode' => $ResponsibilityDepartmentCode,
            'TicketDescription' => $TicketDescription,
            'RefNo' => $RefNo,
            'Remark' => $Remark
        ];

        $response = $client->request('PUT', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[RLM IKB  NOC API - Update FMI Open Ticket]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

    public function delete($code)
    {
        $url =env('IKB_TCK_NOC') . '/fmi-ticket-open/'.$code;
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('delete', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[RLM IKB  NOC API - Deleted FMI Open Ticket]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

    public function postStatus($data)
    {
        $url =env('IKB_TCK_NOC') . '/fmi-ticket-status';

        $client = new Client(['http_errors' => false]);


        $response = $client->request('POST', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);

        if ($response->getStatusCode() != 201) {
            Log::error("[RLM IKB  NOC API - Add FMI Open Status]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

    public function showStatus($code=null)
    {
        $url =env('IKB_TCK_NOC') . '/fmi-ticket-status?limit=15&page=1&search='.$code;
        $signature = (new Signature($url))->create();

        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[RLM IKB  NOC API - Get All FMI Ticket Status]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }
}
