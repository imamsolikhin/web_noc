<?php

namespace App\Services\Ticketing\Master;

use Carbon\Carbon;
use GuzzleHttp\Client;
use App\Services\Signature;
use Illuminate\Support\Facades\Log;

class OpenDeviceTypeFo
{
    public function getAll($id=null)
    {
        $url =env('IKB_TCK_NOC') . '/fmi-ticket-open-device-type-fo?limit=15000&page=1&search='.$id;
        $signature = (new Signature($url))->create();

        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);
        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[RLM IKB  NOC API - Get All FMI Open Device Type Fo]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function post($data)
    {
        $url =env('IKB_TCK_NOC') . '/fmi-ticket-open-device-type-fo';

        $client = new Client(['http_errors' => false]);
        $data=[
            'code' => $data['code'],
            'HeaderCode' => $data['HeaderCode'],
            'DeviceTypeFO' => $data['DeviceTypeFO']
        ];

        $response = $client->request('POST', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);


        if ($response->getStatusCode() != 201) {
            Log::error("[RLM IKB  NOC API - Add Open Device Type Fo]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

    public function delete($code)
    {
        $url =env('IKB_TCK_NOC') . '/fmi-ticket-open-device-type-fo/'.$code;
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('delete', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);
        if ($response->getStatusCode() != 200) {
            Log::error("[RLM IKB  NOC API - Deleted Open Device Type Fo]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }
}
