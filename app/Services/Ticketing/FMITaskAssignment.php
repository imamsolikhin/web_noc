<?php

namespace App\Services\Ticketing;

use Carbon\Carbon;
use GuzzleHttp\Client;
use App\Services\Signature;
use Illuminate\Support\Facades\Log;

class FMITaskAssignment
{
    public function getAll($code=null)
    {
        $url =env('IKB_TCK_NOC') . '/fmi-task-assignment?limit=15&page=1&search='.$code;

        $signature = (new Signature($url))->create();

        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[RLM IKB  NOC API - Get All FMI Task Assigment]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function post($data)
    {
        $url =env('IKB_TCK_NOC') . '/fmi-task-assignment';

        $client = new Client(['http_errors' => false]);
        unset($data['select']);
        unset($data['_token']);
        $data['TicketCode'] = $data['TicketCode'];
        unset($data['tck_no']);
        $data = [
          'code' => $data['code'],
          'TransactionDate' => Carbon::parse($data['TransactionDate'])->format('Y-m-d H:i:s'),
          'BranchCode' => $data['BranchCode'],
          'TicketCode' => $data['TicketCode'],
          'ScheduleDate' => Carbon::parse($data['ScheduleDate'])->format('Y-m-d H:i:s'),
          'MitraCode' => $data['MitraCode'],
          'TeamCode' => $data['TeamCode'],
          'ERPTaskCode' => $data['ERPTaskCode'],
          'RefNo' => $data['RefNo'],
          'Remark' => $data['Remark']
        ];
        $response = $client->request('POST', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => $data
        ]);
        if ($response->getStatusCode() != 201) {
            Log::error("[RLM IKB  NOC API - Add FMI Task Assigment]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

    public function show($code)
    {
        $url =env('IKB_TCK_NOC') . '/fmi-task-assignment/'.$code;
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() != 200) {
            Log::error("[RLM IKB  NOC API - Show FMI Task Assigment]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }

        return $contents;
    }

    public function put($code, $data)
    {
        $url =env('IKB_TCK_NOC') . '/fmi-task-assignment/'.$code;
        $client = new Client(['http_errors' => true]);
        unset($data['_token']);
        unset($data['_method']);
        $data['TransactionDate'] = Carbon::parse($data['TransactionDate'])->format('Y-m-d H:i:s');
        $data['ScheduleDate'] = Carbon::parse($data['ScheduleDate'])->format('Y-m-d H:i:s');
        $response = $client->request('PUT', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Signature' => (new Signature(json_encode($data)))->create()
            ],
            'json' => (array)$data
        ]);
        if ($response->getStatusCode() != 200) {
            Log::error("[RLM IKB  NOC API - Update FMI Task Assigment]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }

    public function delete($code)
    {
        $url =env('IKB_TCK_NOC') . '/fmi-task-assignment/'.$code;
        $client = new Client(['http_errors' => false]);
        $signature = (new Signature($url))->create();
        $response = $client->request('delete', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Signature' => $signature
            ]
        ]);

        if ($response->getStatusCode() != 200) {
            Log::error("[RLM IKB  NOC API - Deleted FMI Task Assigment]\r\nStatus Code\r\n{$response->getStatusCode()}\r\n\r\nResponse\r\n{$response->getBody()}");
        }
    }
}
