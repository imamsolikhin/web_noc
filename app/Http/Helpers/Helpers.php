<?php

function global_limit(){
  return 5;
}

function paginator($data){
    $result = new ArrayObject();
    if(isset($data->paginator)){
      $result = $data->paginator;
    }else{
      $result->total_records = 0;
      $result->total_pages = 0;
      $result->current_page = 0;
      $result->per_page = 0;
    }
    // dd($result);
    return $result;
}
/**
 * Inkombizz | inkombizz@gmail.com | inkombizz.com
 * Make a Response.
 *
 * @param  int  $statusCode
 * @param  string  $status
 * @param  string  $message
 * @param  array|object|null  $data
 * @param  array  $headers
 * @return json
 */
function makeResponse($statusCode, $status, $message, $data = null, $headers = [])
{
    $result = [
        'status_code' => $statusCode,
        'status' => $status == 'pagination' ? 'success' : $status,
        'message' => $message,
        'data' => $data,
    ];

    if ($status == 'pagination') {
        $result = array_merge($result, ['paginator' => [
            'total_records' => (int) $data->total(),
            'total_pages' => (int) $data->lastPage(),
            'current_page' => (int) $data->currentPage(),
            'per_page' => (int) $data->perPage(),
        ]]);
    }

    return response()->json($result, $statusCode, $headers);
}

function currDate() {
  date_default_timezone_set ('Asia/Jakarta');
  return date("Y-m-d H:i", time());
}

function sess_company($prm = null)
{
    if ($prm) {
        if (Session::get('com') != null) {
            return Session::get('com')[$prm];
        }
    }
    return null;
}

function sess_user($prm = null)
{
    if ($prm) {
        if (Session::get('user') != null) {
            return Session::get('user')[$prm];
        }
    }
    return null;
}

function prefix($str, $length) {
    return str_pad($str, $length, '0', STR_PAD_LEFT);
}

function getUserIP() {
    $client = @$_SERVER['HTTP_CLIENT_IP'];
    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
    $remote = $_SERVER['REMOTE_ADDR'];

    if (filter_var($client, FILTER_VALIDATE_IP)) {
        $ip = $client;
    } elseif (filter_var($forward, FILTER_VALIDATE_IP)) {
        $ip = $forward;
    } else {
        $ip = $remote;
    }

    return $ip;
}

function swalError($message = 'Validation Error!') {
    while (DB::transactionLevel() > 0) {
        DB::rollBack();
    }
    return redirect()->back()->withInput(request()->except('_token'))->with('swal_error', $message);
}

function user() {
    $user = Auth::user();

    return $user;
}

function getSql($model) {
    $replace = function ($sql, $bindings) {
        $needle = '?';
        foreach ($bindings as $replace) {
            $pos = strpos($sql, $needle);
            if ($pos !== false) {
                if (gettype($replace) === "string") {
                    $replace = ' "' . addslashes($replace) . '" ';
                }
                $sql = substr_replace($sql, $replace, $pos, strlen($needle));
            }
        }
        return $sql;
    };
    $sql = $replace($model->toSql(), $model->getBindings());

    return $sql;
}


function listMenu()
{
    $menus = config_menu(0);
    $list['items'] = [];
    $dashboard = [
          'title' => 'Dashboard',
          'root' => true,
          'icon' => 'media/svg/icons/Home/Flower3.svg',
          'page' => sess_user('dashboard_url'),
          'new-tab' => false,
        ];
    array_push($list,$dashboard);
    if(sess_user('role_id') == 'NOC'){
      array_push($list,['section' => 'MENU SYSTEM']);
    }
    foreach ($menus as $id => $rs) {
      $head_mn = $rs[2];
      $sub_mn = config_menu($rs[0]);
      $head_mn["submenu"] = $sub_mn;
      if($sub_mn){
        if(sess_user('role_id') != 'NOC'){
          array_push($list,['section' => $rs[1]]);
          array_push($list,$sub_mn);
        }else{
          array_push($list,$head_mn);
        }
      }
    }
    $dashboard = [
          'title' => 'Logout',
          'root' => true,
          'icon' => 'media/svg/icons/Home/Flower3.svg',
          'page' => 'logout',
          'new-tab' => false,
        ];
    array_push($list,$dashboard);
    return $list;
}

function config_menu($parent)
{
    $table = new App\Models\RoleAuth();
    $menus = \DB::table($table->getTable())
              ->select(
                 "role_menu_auth.*"
                ,"menus.name as menu_name"
                ,"menus.url as menu_url"
                ,"menu_icons.icon_url as menu_icon"
              )
              ->join('roles', 'roles.id', '=', 'role_menu_auth.role_id')
              ->join('menus', 'menus.id', '=', 'role_menu_auth.menu_id')
              ->join('menu_icons', 'menu_icons.id', '=', 'menus.icon')
              ->where('role_menu_auth.view',1)
              ->where(function ($query) use ($parent) {
                  $query->where('menus.parent_id',$parent);
                  if($parent){
                      $query->where('role_menu_auth.role_id',sess_user('role_id'));
                  }
              })
              ->groupBy('menus.id')
              ->get();
    $list = [];
    foreach ($menus as $rs) {
      $mn = [
            'title' => $rs->menu_name,
            'root' => true,
            'icon' => $rs->menu_icon,
            'page' => $rs->menu_url,
            'new-tab' => false
          ];

      if($parent){
        array_push($list,$mn);
      }else {
        array_push($list,[$rs->id,$rs->menu_name,$mn]);
      }

    }
    return $list;
}

function getAuthMenu($module,$key) {
    $flag = App\Models\RoleAuth::select("role_menu_auth.".$key." AS key")
                  ->join('menus', 'menus.id', '=', 'role_menu_auth.menu_id')
                  ->where('role_menu_auth.role_id',sess_user('role_id'))
                  ->where('menus.url',\URL::route($module,[],false))
                  ->first();
    if($flag){
      return $flag->key;
    }
    return False;
}

function to_bool($val = null) {
    if ($val == "on" || $val != null) {
        return 1;
    } else {
        return 0;
    }
}


function generateRandomString($length = 20, $company = null, $branch = null) {
    if($company==null){
      $company= env('APP_COMPANY');
    }

    if($branch==null){
      $branch=env('APP_BRANCH');
    }

    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $company.$branch.$randomString;
}
