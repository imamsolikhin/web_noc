<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Olt\Clients;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function noauth(){
        return view('auth.noauth');
    }

    public function index()
    {

        return redirect()->route('dashboard');
    }

    public function dashboard()
    {
      $data[] = null;
      $dashboard = (new Clients())->dashboard();
      if ($dashboard->data){
          $data["clients_dashboard"] = $dashboard->data;
      }
      return view('dashboard',$data);
    }
}
