<?php

namespace App\Http\Controllers\Ticketing;

use DataTables;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Services\Master\Team;
use App\Services\Master\Mitra;
use App\Services\Master\Branch;
use App\Services\Master\Department;
use App\Http\Controllers\Controller;
use App\Services\Master\ProblemType;
use App\Services\Ticketing\FMIOpenTicket;
use App\Services\Ticketing\FMITaskAssignment;
use App\Services\Ticketing\FMITicketStatus;
use App\Services\Master\BaseTransmissionStation;
use App\Services\Olt\Host;
use App\Services\Olt\Clients;
use App\Services\Master\CustomerNoc;
use App\Services\Ticketing\Master\OpenBts;
use App\Services\Ticketing\Master\OpenCustomer;
use App\Services\Ticketing\Master\OpenDeviceTypeFo;
use App\Services\Ticketing\Master\OpenDeviceTypeWll;

class FMITicketStatusController extends Controller
{
    public function index()
    {
      if(!getAuthMenu('ticketing.fmi.ticket-open.index',VIEW)){
        return redirect()->route('noauth');
      }
        return view('ticketing.fmi.status-ticket.index');
    }

    public function create()
    {
    	$branchs = (new Branch)->getAll()->data;
    	$problemTypes = (new ProblemType)->getAll()->data;
    	$openCustomers = (new OpenCustomer)->getAll()->data;
      $openBts = (new OpenBts)->getAll()->data;
      $openDeviceTypeFoes = (new OpenDeviceTypeFo)->getAll()->data;
      $openDeviceTypeWlls = (new OpenDeviceTypeWll)->getAll()->data;
      $departments = (new Department)->getAll()->data;
      $baseTransmissionStations = (new BaseTransmissionStation)->getAll()->data;
      $oltHost = (new Host)->getAll()->data;
    	return view('ticketing.fmi.status-ticket.create', compact('branchs', 'problemTypes', 'departments','openCustomers', 'openBts', 'openDeviceTypeFoes', 'openDeviceTypeWlls', 'baseTransmissionStations', 'oltHost'));
    }

    public function getCustomerByBTSOLT(Request $request)
    {
      $customerNoc = (new CustomerNoc)->getAll()->data;
      $filteredCustomerNoc = array();
      if(!empty(@$request->open_bts))
      {
        foreach ($customerNoc as $key => $row) {
          if(in_array($row->BTSCode, @$request->open_bts))
          {
            $filteredCustomerNoc[] = $row;
          }
        }
      }
      if(!empty(@$request->olt_host))
      {
        $oltClients = (new Clients)->getAll()->data;
        $filteredClients = array();
        foreach ($oltClients as $key => $row) {
          if(in_array($row->HostCode,@$request->olt_host))
          {
            $filteredClients[] = $row->CustomerCode;
          }
        }
        foreach ($customerNoc as $key => $row) {
          if(in_array($row->Code, $filteredClients) && empty($row->BTSCode))
          {
            $filteredCustomerNoc[] = $row;
          }
        }
      }
      return response(json_encode(['status'=>'true', 'data'=>$filteredCustomerNoc]), 200)->header('Content-Type', 'application/json');
      exit();
    }

    public function testing()
    {
        $branchs = (new Branch)->getAll()->data;
        $departments = (new Department)->getAll()->data;
        $problemTypes = (new ProblemType)->getAll()->data;
        $baseTransmissionStations = (new BaseTransmissionStation)->getAll()->data;

        return view('ticketing.fmi.status-ticket.form', compact('branchs', 'problemTypes', 'departments', 'baseTransmissionStations'));
    }

    public function store(Request $request)
    {
        // dd($request->all());
    	$this->validate($request, [
    		'tck_no' => 'required',
    		'TransactionDate' => 'required|date_format:d-m-Y',
        'ScheduleDate' => 'required|date_format:d-m-Y',
    		'BranchCode' => 'required',
        'TicketCategory' => 'required',
        'ProblemTypeCode' => 'required',
        'ResponsibilityDepartmentCode' => 'required',
    		'RefNo' => 'nullable|string|max:250',
    		'Remark' => 'nullable|string|max:250'
    	]);

    	  $taskAssignment = (new FMIOpenTicket)->post($request->all());
        if($taskAssignment->getStatusCode()=='201')
        {
          (new FMITicketStatus)->post([
            'code' => $request->tck_no,
            'TransactionDate' => $request->TransactionDate,
            'BranchCode' => $request->BranchCode,
            'TicketCode' => $request->tck_no,
            'TicketStatus' => 'OPEN',
            'RefNo' => $request->RefNo,
            'Remark' => $request->Remark
          ]);

          $LastOpenCustomer = (new OpenCustomer)->getAll()->data;
          if(!empty($request->input('open_customer')))
          {

            foreach ($request->input('open_customer', []) as $customer => $value) {
      	    	$openCustomer = [
      	    		'code' => '001'.($customer+count($LastOpenCustomer)),
      	    		'HeaderCode' => $request->tck_no,
      	    		'CustomerCode' => $value
      	    	];

            	$openCustomer = (new OpenCustomer)->post($openCustomer);
            }
          }

          $LastOpenBts = (new OpenBts)->getAll()->data;
          if(is_array(@$request->input('open_bts')))
          {
            //FOR BULK
            foreach ($request->input('open_bts', []) as $openBts => $value) {
      	    	$openBts = [
      	    		'code' => $value.'/001'.($openBts+count($LastOpenBts)),
      	    		'HeaderCode' => $request->tck_no,
      	    		'BTSCode' => $value
      	    	];
              $openBts = (new OpenBts)->post($openBts);
            }
          }
          else {
            //FOR 1
            $openBts = [
              'code' => $request->input('open_bts').'/001'.(1+count($LastOpenBts)),
              'HeaderCode' => $request->tck_no,
              'BTSCode' => @$request->input('open_bts')
            ];
            $openBts = (new OpenBts)->post($openBts);
          }

          $LastOpenFo = (new OpenDeviceTypeFo)->getAll()->data;
          if(is_array(@$request->input('olt_host')))
          {
            //FOR BULK
            foreach ($request->input('olt_host', []) as $openFo => $value) {
              $dataFo = [
      	    		'code' => $value.'/001'.($openFo+count($LastOpenFo)),
      	    		'HeaderCode' => $request->tck_no,
      	    		'DeviceTypeFO' => 'OLT'
      	    	];
              $openDeviceTypeFo = (new OpenDeviceTypeFo)->post($dataFo);
            }
          }
          else
          {
            //FOR 1
            $dataFo = [
              'code' => $request->input('olt_host').'/001'.(1+count($LastOpenFo)),
              'HeaderCode' => $request->tck_no,
              'DeviceTypeFO' => 'OLT'
            ];
            $openDeviceTypeFo = (new OpenDeviceTypeFo)->post($dataFo);
          }

          return redirect()->route('ticketing.fmi.ticket-open.index')->with('notif_success', 'Open Ticket has been saved successfully');
        }
        else
        {
          return redirect()->route('ticketing.fmi.ticket-open.index')->with('notif_success', 'Open Ticket was failed because duplicate ticket no');
        }

        //
        // foreach ($request->input('wll', []) as $openWll => $value) {
	    	// $dataWll = [
	    	// 	'code' => '001'.$openWll,
	    	// 	'HeaderCode' => $request->tck_no,
	    	// 	'DeviceTypeWLLCode' => $value
	    	// ];
        //
	    	// $openDeviceTypeWll = (new OpenDeviceTypeWll)->post($dataWll);
        //
        // }


    }

    public function show($id)
    {
      $openBts = (new OpenBts)->getAll($id)->data;
      $fMIOpenTicket = (new FMIOpenTicket)->show($id)->data;
      $fMITicketStatus = (new FMITicketStatus)->getAll($id)->data;
      $openOlt = (new OpenDeviceTypeFo)->getAll($id)->data;
      $branchs = (new Branch)->getAll()->data;
      $departments = (new Department)->getAll()->data;
      $problemTypes = (new ProblemType)->getAll()->data;
      $baseTransmissionStations = (new BaseTransmissionStation)->getAll()->data;
      $transactionDate = Carbon::parse($fMIOpenTicket->TransactionDate)->format('d-m-Y');
      $scheduleDate = Carbon::parse($fMIOpenTicket->ScheduleDate)->format('d-m-Y');
      $openDeviceTypeFoes = (new OpenDeviceTypeFo)->getAll($id)->data;
      $openDeviceTypeWlls = (new OpenDeviceTypeWll)->getAll($id)->data;
      $openCustomers = (new OpenCustomer)->getAll($id)->data;
      $oltHost = (new Host)->getAll()->data;

      return view('ticketing.fmi.status-ticket.show', compact('fMIOpenTicket', 'fMITicketStatus', 'branchs', 'departments', 'problemTypes', 'baseTransmissionStations', 'transactionDate', 'scheduleDate', 'openBts', 'openDeviceTypeFoes', 'openDeviceTypeWlls', 'openCustomers', 'oltHost', 'openOlt'));
    }

    public function edit($id)
    {
      if(!getAuthMenu('ticketing.fmi.ticket-status.index',UPDATE)){
        return redirect()->route('noauth');
      }

      $openBts = (new OpenBts)->getAll($id)->data;
      $fMIOpenTicket = (new FMIOpenTicket)->show($id)->data;
      $fMITicketStatus = (new FMITicketStatus)->getAll($id)->data;
      $openOlt = (new OpenDeviceTypeFo)->getAll($id)->data;
      $branchs = (new Branch)->getAll()->data;
      $departments = (new Department)->getAll()->data;
      $problemTypes = (new ProblemType)->getAll()->data;
      $baseTransmissionStations = (new BaseTransmissionStation)->getAll()->data;
      $transactionDate = Carbon::parse($fMIOpenTicket->TransactionDate)->format('d-m-Y');
      $scheduleDate = Carbon::parse($fMIOpenTicket->ScheduleDate)->format('d-m-Y');
      $openDeviceTypeFoes = (new OpenDeviceTypeFo)->getAll($id)->data;
      $openDeviceTypeWlls = (new OpenDeviceTypeWll)->getAll($id)->data;
      $openCustomers = (new OpenCustomer)->getAll($id)->data;
      $oltHost = (new Host)->getAll()->data;

      return view('ticketing.fmi.status-ticket.edit', compact('fMIOpenTicket', 'fMITicketStatus', 'branchs', 'departments', 'problemTypes', 'baseTransmissionStations', 'transactionDate', 'scheduleDate', 'openBts', 'openDeviceTypeFoes', 'openDeviceTypeWlls', 'openCustomers', 'oltHost', 'openOlt', 'fMITicketStatus'));
    }

    public function update($id, Request $request)
    {
        $code = explode('-',$request->latestStatusCode);
        $code[3] = str_pad((int)$code[3]+1, 2, '0', STR_PAD_LEFT);
        $code = implode('-',$code);

        (new FMITicketStatus)->post([
          'code' => $code,
          'TransactionDate' => $request->TransactionDate,
          'BranchCode' => $request->BranchCode,
          'TicketCode' => $id,
          'TicketStatus' => $request->TicketStatus,
          'RefNo' => $request->ts_ref_no,
          'Remark' => $request->ts_Remark
        ]);
        //$statusTicket = (New fMITicketStatus)->put($id, $request->all());

        return redirect()->route('ticketing.fmi.ticket-status.index')->with('notif_success', 'Open tiket has been Updated');
    }

    public function updateStatus($id, Request $request)
    {
        $prefix = 'T';
        $year = date('y');
        $month = date('m');
        $day = date('d');
        $hours = date('His');
        $sequence = str_pad($id, 7, '0', STR_PAD_LEFT);
        $codeStatus = $prefix . $year . $month . $day. $hours .$sequence;
        $transactionDate = Carbon::parse(now())->format('Y-m-d H:i:s');
        $fMIOpenTicket = (new FMIOpenTicket)->show($id)->data;
        $dataStatus = [
            'code' => $codeStatus,
            'TransactionDate' => $transactionDate,
            'BranchCode' => optional($fMIOpenTicket)->BranchCode,
            'TicketCode' => $id,
            'TicketStatus' => $request->TicketStatus,
            'RefNo' => $request->ref_no_status,
            'Remark' => $request->remark_staus,
            'CreatedBy' => 'admin'
        ];

        $ticketStatus = (new FMIOpenTicket)->postStatus($dataStatus);

        return redirect()->route('ticketing.fmi.ticket-open.show', $id)->with('notif_success', 'Ticket Status has been saved successfully');
    }

    public function destroy($id)
    {
      if(!getAuthMenu('ticketing.fmi.ticket-status.index',DELETE)){
        return redirect()->route('noauth');
      }
        $openBts = (new OpenBts)->getAll($id)->data;
        foreach ($openBts as $list) {
            (new OpenBts)->delete($list->code);
        }

        $openDeviceTypeFoes = (new OpenDeviceTypeFo)->getAll($id)->data;
        foreach ($openDeviceTypeFoes as $list) {
            (new OpenDeviceTypeFo)->delete($list->code);
        }

        $openDeviceTypeWlls = (new OpenDeviceTypeWll)->getAll($id)->data;
        foreach ($openDeviceTypeWlls as $list) {
            (new OpenDeviceTypeWll)->delete($list->code);
        }

        $openTicket = (New FMIOpenTicket)->delete($id);

        return redirect()->route('ticketing.fmi.ticket-open.index')->with('notif_success', 'Open tiket has been deleted');
    }

    public function getData()
    {
      $ticket = (new FMIOpenTicket)->getAllTicket()->data;
      $ticket = (array)$this->unique_ticket($ticket);
      return DataTables::of($ticket)
      ->editColumn('transaction_date', function($ticket) {
        return date("d M Y - H:i:s", strtotime($ticket->TransactionDate));
      })
      ->editColumn('schedule_date', function($ticket) {
        return date("d M Y - H:i:s", strtotime($ticket->ScheduleDate));
      })
      ->addColumn('action', function($ticket) {
        $show ='<a href="' . route('ticketing.fmi.ticket-status.show', $ticket->code) . '" class="btn btn-icon btn-light btn-hover-success btn-sm" data-toggle="tooltip" data-placement="top" title="Detail">
        <span class="svg-icon svg-icon-md svg-icon-success">
          <!--begin::Svg Icon | path:media/svg/icons/General/Settings-1.svg-->
          <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
              <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                  <rect x="0" y="0" width="24" height="24"/>
                  <path d="M7,3 L17,3 C19.209139,3 21,4.790861 21,7 C21,9.209139 19.209139,11 17,11 L7,11 C4.790861,11 3,9.209139 3,7 C3,4.790861 4.790861,3 7,3 Z M7,9 C8.1045695,9 9,8.1045695 9,7 C9,5.8954305 8.1045695,5 7,5 C5.8954305,5 5,5.8954305 5,7 C5,8.1045695 5.8954305,9 7,9 Z" fill="#000000"/>
                  <path d="M7,13 L17,13 C19.209139,13 21,14.790861 21,17 C21,19.209139 19.209139,21 17,21 L7,21 C4.790861,21 3,19.209139 3,17 C3,14.790861 4.790861,13 7,13 Z M17,19 C18.1045695,19 19,18.1045695 19,17 C19,15.8954305 18.1045695,15 17,15 C15.8954305,15 15,15.8954305 15,17 C15,18.1045695 15.8954305,19 17,19 Z" fill="#000000" opacity="0.3"/>
              </g>
          </svg>
          <!--end::Svg Icon-->
      </span>
        </a>';
        $edit = '<a href="' . route('ticketing.fmi.ticket-status.edit', $ticket->code) . '" class="btn btn-icon btn-light btn-hover-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Edit">
        <span class="svg-icon svg-icon-md svg-icon-primary">
        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <rect x="0" y="0" width="24" height="24"/>
        <path d="M12.2674799,18.2323597 L12.0084872,5.45852451 C12.0004303,5.06114792 12.1504154,4.6768183 12.4255037,4.38993949 L15.0030167,1.70195304 L17.5910752,4.40093695 C17.8599071,4.6812911 18.0095067,5.05499603 18.0083938,5.44341307 L17.9718262,18.2062508 C17.9694575,19.0329966 17.2985816,19.701953 16.4718324,19.701953 L13.7671717,19.701953 C12.9505952,19.701953 12.2840328,19.0487684 12.2674799,18.2323597 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.701953, 10.701953) rotate(-135.000000) translate(-14.701953, -10.701953) "/>
        <path d="M12.9,2 C13.4522847,2 13.9,2.44771525 13.9,3 C13.9,3.55228475 13.4522847,4 12.9,4 L6,4 C4.8954305,4 4,4.8954305 4,6 L4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 L2,6 C2,3.790861 3.790861,2 6,2 L12.9,2 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
        </g>
        </svg>
        </span>
        </a>';
        return $show .' '. $edit;
        })
      ->rawColumns(['action', 'transaction_date', 'schedule_date'])
      ->make(true);
    }

    protected function unique_ticket($data = null)
    {
      $data = array_reverse($data);
      if($data == null)
      {
        return (object)array();
      }
      else
      {
        $unique = array();
        foreach ($data as $value)
        {
            $unique[$value->code] = $value;
        }
        $data = array_values($unique);

        return (object)$unique;
      }
    }

}
