<?php

namespace App\Http\Controllers\Ticketing;

use DataTables;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Services\Master\Team;
use App\Services\Master\Mitra;
use App\Services\Master\Branch;
use App\Services\Master\Department;
use App\Http\Controllers\Controller;
use App\Services\Master\ProblemType;
use App\Services\Ticketing\FMIOpenTicket;
use App\Services\Ticketing\FMITaskAssignment;
use App\Services\Ticketing\FMITicketStatus;
use App\Services\Master\BaseTransmissionStation;
use App\Services\Olt\Host;
use App\Services\Olt\Clients;
use App\Services\Master\CustomerNoc;
use App\Services\Master\CustomerNocTicket;
use App\Services\Master\CustomerErp;
use App\Services\Ticketing\Master\OpenBts;
use App\Services\Ticketing\Master\OpenCustomer;
use App\Services\Ticketing\Master\OpenDeviceTypeFo;
use App\Services\Ticketing\Master\OpenDeviceTypeWll;

class FMITicketOpenController extends Controller
{
  public function index()
  {
    if(!getAuthMenu('ticketing.fmi.ticket-open.index',VIEW)){
      return redirect()->route('noauth');
    }
// $taskAssigment = (new FMIOpenTicket)->getAll();
// $ticketStatus = (new FMITicketStatus)->getAll();
// dd($taskAssigment);
    return view('ticketing.fmi.open-ticket.index');
  }

  public function create($ticketType = 'MINOR')
  {
    $branchs = (new Branch)->getAll()->data;
    $problemTypes = (new ProblemType)->getAll()->data;
    $customersErp = (new CustomerErp)->getAll()->data;
    $openBts = (new OpenBts)->getAll()->data;
    $openDeviceTypeFoes = (new OpenDeviceTypeFo)->getAll()->data;
    $openDeviceTypeWlls = (new OpenDeviceTypeWll)->getAll()->data;
    $departments = (new Department)->getAll()->data;
    $baseTransmissionStations = (new BaseTransmissionStation)->getAll()->data;
    $oltHost = (new Host)->getAll()->data;
    return view('ticketing.fmi.open-ticket.create', compact('ticketType','branchs', 'problemTypes', 'departments','customersErp', 'openBts', 'openDeviceTypeFoes', 'openDeviceTypeWlls', 'baseTransmissionStations', 'oltHost'));
  }

  public function getCustomerByBTSOLT(Request $request)
  {
    $customerNoc = (new CustomerNocTicket)->getDataIn($request->open_bts,$request->olt_host)->data;
    return response(json_encode(['status'=>'true', 'data'=>$customerNoc]), 200)->header('Content-Type', 'application/json');
    exit();
  }

  public function getCustomerNoc(Request $request)
  {
    if(empty(@$request->q))
    {
      $search = '00';
    }
    else
    {
      $search = $request->q;
    }
    $customerNocTicket = (new CustomerNocTicket)->getData($search)->data;
    $return = array();
    if(!empty($customerNocTicket))
    {
      foreach ($customerNocTicket as $key => $value) {
        $return[] = array('Code'=>$value->Code,'Name'=>$value->Code.' | '.$value->Name);
      }
    }

    return response(json_encode($return), 200)->header('Content-Type', 'application/json');
    exit();
  }

  public function testing()
  {
    $branchs = (new Branch)->getAll()->data;
    $departments = (new Department)->getAll()->data;
    $problemTypes = (new ProblemType)->getAll()->data;
    $baseTransmissionStations = (new BaseTransmissionStation)->getAll()->data;

    return view('ticketing.fmi.open-ticket.form', compact('branchs', 'problemTypes', 'departments', 'baseTransmissionStations'));
  }

  public function store(Request $request)
  {
    $this->validate($request, [
      'tck_no' => 'required',
      //'TransactionDate' => 'required|date_format:d-m-Y H:i:s',
      //'ScheduleDate' => 'required|date_format:d-m-Y H:i:s',
      'BranchCode' => 'required',
      'TicketCategory' => 'required',
      'ProblemTypeCode' => 'required',
      'ResponsibilityDepartmentCode' => 'required',
      'RefNo' => 'nullable|string|max:250',
      'Remark' => 'nullable|string|max:250'
    ]);

    $openTicket = (new FMIOpenTicket)->post($request->all());

    if($openTicket->getStatusCode()=='201')
    {
      $lastCreatedCode = json_decode($openTicket->getBody()->getContents())->data->code;
      $counterStatus = str_pad('1', 2, '0', STR_PAD_LEFT);
      $lastCreatedStatusCode = str_replace('TCK', 'TCK-STAT-'.$counterStatus, $lastCreatedCode);

      (new FMITicketStatus)->post([
        'code' => $lastCreatedStatusCode,
        'TransactionDate' => $request->TransactionDate,
        'BranchCode' => $request->BranchCode,
        'TicketCode' => $lastCreatedCode,
        'TicketStatus' => 'OPEN',
        'RefNo' => '',//$request->RefNo,
        'Remark' => ''//$request->Remark
      ]);

      if(!empty($request->input('open_customer')))
      {
        $counterOpenCustomer = 1;
        foreach ($request->input('open_customer', []) as $customer => $value) {
          $counterOpenCustomerPad = str_pad((string)$counterOpenCustomer++, 2,'0', STR_PAD_LEFT);
          $openCustomer = [
            'code' => str_replace('TCK', 'TCK-OC-'.$counterOpenCustomerPad, $lastCreatedCode),
            'HeaderCode' => $lastCreatedCode,
            'CustomerCode' => $value
          ];

          $openCustomer = (new OpenCustomer)->post($openCustomer);
        }
      }

      if(is_array(@$request->input('open_bts')))
      {
      //FOR BULK
        $counterOpenBTS = 1;
        foreach ($request->input('open_bts', []) as $openBts => $value) {
          $counterOpenBTSPad = str_pad((string)$counterOpenBTS++, 2, '0', STR_PAD_LEFT);
          $openBts = [
            'code' => str_replace('TCK', 'TCK-BTS-'.$counterOpenBTSPad, $lastCreatedCode),
            'HeaderCode' => $lastCreatedCode,
            'BTSCode' => $value
          ];
          $openBts = (new OpenBts)->post($openBts);
        }
      }
      else {
      //FOR 1
        $openBts = [
          'code' => str_replace('TCK', 'TCK-BTS-01', $lastCreatedCode),
          'HeaderCode' => $lastCreatedCode,
          'BTSCode' => @$request->input('open_bts')
        ];
        $openBts = (new OpenBts)->post($openBts);
      }

      $LastOpenFo = (new OpenDeviceTypeFo)->getAll()->data;
      if(is_array(@$request->input('olt_host')))
      {
      //FOR BULK
        $counterOpenOLT = 1;
        foreach ($request->input('olt_host', []) as $openFo => $value) {
          $counterOpenOLTPad = str_pad((string)$counterOpenOLT++, 2, '0', STR_PAD_LEFT);
          $dataFo = [
            'code' => $value.'~'.str_replace('TCK', 'TCK-DEV-'.$counterOpenOLTPad, $lastCreatedCode),
            'HeaderCode' => $lastCreatedCode,
            'DeviceTypeFO' => 'OLT'
          ];
          $openDeviceTypeFo = (new OpenDeviceTypeFo)->post($dataFo);
        }
      }
      else
      {
      //FOR 1
        $dataFo = [
          'code' => $request->input('olt_host').'~'.str_replace('TCK', 'TCK-DEV-01', $lastCreatedCode),
          'HeaderCode' => $lastCreatedCode,
          'DeviceTypeFO' => 'OLT'
        ];
        $openDeviceTypeFo = (new OpenDeviceTypeFo)->post($dataFo);
      }
      return redirect()->route('ticketing.fmi.ticket-open.index')->with('notif_success', 'Open Ticket has been saved successfully');
    }
    else
    {
      return redirect()->route('ticketing.fmi.ticket-open.index')->with('notif_success', 'Open Ticket was failed because duplicate ticket no');
    }

  }

  public function storeAssignment($id, Request $request)
  {
    $prefix = 'TA';
    $year = date('y');
    $month = date('m');
    $day = date('d');
    $hours = date('His');
    $sequence = str_pad($id, 7, '0', STR_PAD_LEFT);
    $codeAssignment = $prefix . $year . $month . $day. $hours .$sequence;
    $transactionDate = Carbon::parse(now())->format('Y-m-d H:i:s');
    $scheduleData = Carbon::parse($request->ScheduleDate)->format('Y-m-d H:i:s');
    $fMIOpenTicket = (new FMIOpenTicket)->show($id)->data;

    $dataAssignment = [
      'code' => $codeAssignment,
      'TransactionDate' => $transactionDate,
      'BranchCode' => optional($fMIOpenTicket)->BranchCode,
      'TicketCode' => $id,
      'ScheduleDate' => $scheduleData,
      'MitraCode' => $request->MitraCode,
      'TeamCode' => $request->team,
      'ERPTaskCode' => '001',
      'RefNo' => $request->ref_no_status,
      'Remark' => $request->remark_staus,
      'CreatedBy' => 'admin'
    ];

    $taskAssignment = (new FMITaskAssignment)->post($dataAssignment);

    return redirect()->route('ticketing.fmi.ticket-open.show-assigment', $id)->with('notif_success', 'Task Assignment has been saved successfully');
  }

  public function show($id)
  {
    $openBts = (new OpenBts)->getAll($id)->data;
    $fMIOpenTicket = (new FMIOpenTicket)->show($id)->data;
    $fMITicketStatus = (new FMITicketStatus)->getAll($id)->data;
    $openOlt = (new OpenDeviceTypeFo)->getAll($id)->data;


    $branchs = (new Branch)->getAll()->data;
    $departments = (new Department)->getAll()->data;
    $problemTypes = (new ProblemType)->getAll()->data;
    $baseTransmissionStations = (new BaseTransmissionStation)->getAll()->data;
    $transactionDate = Carbon::parse($fMIOpenTicket->TransactionDate)->format('d-m-Y');
    $scheduleDate = Carbon::parse($fMIOpenTicket->ScheduleDate)->format('d-m-Y');
    $openDeviceTypeFoes = (new OpenDeviceTypeFo)->getAll($id)->data;
    $openDeviceTypeWlls = (new OpenDeviceTypeWll)->getAll($id)->data;
    $openCustomers = (new OpenCustomer)->getAll($id)->data;
    $oltHost = (new Host)->getAll()->data;

    return view('ticketing.fmi.open-ticket.show', compact('fMIOpenTicket', 'fMITicketStatus', 'branchs', 'departments', 'problemTypes', 'baseTransmissionStations', 'transactionDate', 'scheduleDate', 'openBts', 'openDeviceTypeFoes', 'openDeviceTypeWlls', 'openCustomers', 'oltHost', 'openOlt'));
  }

  public function showAssigment($id)
  {
    $openBts = (new OpenBts)->getAll($id)->data;
    $fMIOpenTicket = (new FMIOpenTicket)->show($id)->data;

    $showStatus = (new FMIOpenTicket)->showStatus($id)->data;
    $mitras = (new Mitra)->getAll()->data;
    $branchs = (new Branch)->getAll()->data;
    $departments = (new Department)->getAll()->data;
    $problemTypes = (new ProblemType)->getAll()->data;
    $baseTransmissionStations = (new BaseTransmissionStation)->getAll()->data;
    $transactionDate = Carbon::parse($fMIOpenTicket->TransactionDate)->format('d-m-Y');
    $scheduleDate = Carbon::parse($fMIOpenTicket->ScheduleDate)->format('d-m-Y');
    $openDeviceTypeFoes = (new OpenDeviceTypeFo)->getAll($id)->data;
    $openDeviceTypeWlls = (new OpenDeviceTypeWll)->getAll($id)->data;
    $teams = (new Team)->getAll()->data;

    return view('ticketing.fmi.open-ticket.show-assignment', compact('fMIOpenTicket', 'branchs', 'departments', 'problemTypes', 'baseTransmissionStations', 'transactionDate', 'scheduleDate', 'openBts', 'openDeviceTypeFoes', 'openDeviceTypeWlls', 'mitras', 'teams'));
  }

  public function getDataAssignment($code)
  {
    $taskAssigments = (new FMITaskAssignment)->getAll()->data;

    return DataTables::of($taskAssigments)
    ->editColumn('transaction_date', function($taskAssigment) {
      return date("d M Y H:i:s", strtotime($taskAssigment->TransactionDate));
    })
    ->editColumn('schedule_date', function($taskAssigment) {
      return date("d M Y H:i:s", strtotime($taskAssigment->ScheduleDate));
    })
    ->addColumn('action', function($taskAssigment) {
      $edit ='<a href="' . route('ticketing.fmi.task-assignment.edit', $taskAssigment->code) . '" class="btn btn-icon btn-light btn-hover-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Edit">
      <span class="svg-icon svg-icon-md svg-icon-primary">
      <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
      <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
      <rect x="0" y="0" width="24" height="24"/>
      <path d="M12.2674799,18.2323597 L12.0084872,5.45852451 C12.0004303,5.06114792 12.1504154,4.6768183 12.4255037,4.38993949 L15.0030167,1.70195304 L17.5910752,4.40093695 C17.8599071,4.6812911 18.0095067,5.05499603 18.0083938,5.44341307 L17.9718262,18.2062508 C17.9694575,19.0329966 17.2985816,19.701953 16.4718324,19.701953 L13.7671717,19.701953 C12.9505952,19.701953 12.2840328,19.0487684 12.2674799,18.2323597 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.701953, 10.701953) rotate(-135.000000) translate(-14.701953, -10.701953) "/>
      <path d="M12.9,2 C13.4522847,2 13.9,2.44771525 13.9,3 C13.9,3.55228475 13.4522847,4 12.9,4 L6,4 C4.8954305,4 4,4.8954305 4,6 L4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 L2,6 C2,3.790861 3.790861,2 6,2 L12.9,2 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
      </g>
      </svg>
      </span>
      </a>';
      $delete = '<a data-href="' . route('ticketing.fmi.task-assignment.destroy', $taskAssigment->code) . '" class="btn btn-icon btn-light btn-hover-danger btn-sm" "data-toggle="tooltip" data-placement="top" title="Delete" data-toggle="modal" data-target="#confirm-delete-modal">
      <span class="svg-icon svg-icon-md svg-icon-danger">
      <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
      <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
      <rect x="0" y="0" width="24" height="24"/>
      <path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero"/>
      <path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3"/>
      </g>
      </svg>
      </span>
      </a>';

      return $edit .' '. $delete;
    })
    ->rawColumns(['action', 'transaction_date', 'schedule_date'])
    ->make(true);
  }

  public function edit($id)
  {
    if(!getAuthMenu('ticketing.fmi.ticket-open.index',UPDATE)){
      return redirect()->route('noauth');
    }

    $openBts = (new OpenBts)->getAll($id)->data;
    $fMIOpenTicket = (new FMIOpenTicket)->show($id)->data;
    $fMITicketStatus = (new FMITicketStatus)->getAll($id)->data;
    $openOlt = (new OpenDeviceTypeFo)->getAll($id)->data;
    $branchs = (new Branch)->getAll()->data;
    $departments = (new Department)->getAll()->data;
    $problemTypes = (new ProblemType)->getAll()->data;
    $baseTransmissionStations = (new BaseTransmissionStation)->getAll()->data;
    $transactionDate = Carbon::parse($fMIOpenTicket->TransactionDate)->format('d-m-Y');
    $scheduleDate = Carbon::parse($fMIOpenTicket->ScheduleDate)->format('d-m-Y');
    $openDeviceTypeFoes = (new OpenDeviceTypeFo)->getAll($id)->data;
    $openDeviceTypeWlls = (new OpenDeviceTypeWll)->getAll($id)->data;
    $openCustomers = (new OpenCustomer)->getAll($id)->data;
    $oltHost = (new Host)->getAll()->data;

    return view('ticketing.fmi.open-ticket.edit', compact('fMIOpenTicket', 'fMITicketStatus', 'branchs', 'departments', 'problemTypes', 'baseTransmissionStations', 'transactionDate', 'scheduleDate', 'openBts', 'openDeviceTypeFoes', 'openDeviceTypeWlls', 'openCustomers', 'oltHost', 'openOlt'));
  }

  public function update($id, Request $request)
  {
    $openTicket = (New FMIOpenTicket)->put($id, $request->all());

    foreach ($request->input('graveyard_bts', []) as $delBts => $value) {
      (new OpenBts)->delete($value);
    }

    foreach ($request->input('graveyard_olt', []) as $delOlt => $value) {
      (new OpenDeviceTypeFo)->delete($value);
    }

    foreach ($request->input('graveyard_cust', []) as $delCust => $value) {
      (new OpenCustomer)->delete($value);
    }

    if(is_array($request->input('open_customer')))
    {
      $counterOpenCustomer = 1;
      foreach ($request->input('open_customer', []) as $customer => $value) {
        $counterOpenCustomerPad = str_pad((string)$counterOpenCustomer++, 2,'0', STR_PAD_LEFT);
        $openCustomer = [
          'code' => str_replace('TCK', 'TCK-OC-'.$counterOpenCustomerPad, $id),
          'HeaderCode' => $id,
          'CustomerCode' => $value
        ];
        $openCustomer = (new OpenCustomer)->post($openCustomer);
      }
    }
    else
    {
      $openCustomer = [
        'code' => str_replace('TCK', 'TCK-OC-'.'01', $id),
        'HeaderCode' => $id,
        'CustomerCode' => $value
      ];
      $openCustomer = (new OpenCustomer)->post($openCustomer);
    }

    if(is_array(@$request->input('open_bts')))
    {
//FOR BULK
      $counterOpenBTS = 1;
      foreach ($request->input('open_bts', []) as $openBts => $value) {
        $counterOpenBTSPad = str_pad((string)$counterOpenBTS++, 2, '0', STR_PAD_LEFT);
        $openBts = [
          'code' => str_replace('TCK', 'TCK-BTS-'.$counterOpenBTSPad, $id),
          'HeaderCode' => $id,
          'BTSCode' => $value
        ];
        $openBts = (new OpenBts)->post($openBts);
      }
    }
    else {
//FOR 1
      $openBts = [
        'code' => str_replace('TCK', 'TCK-BTS-'.'01', $id),
        'HeaderCode' => $id,
        'BTSCode' => @$request->input('open_bts')
      ];
      $openBts = (new OpenBts)->post($openBts);
    }

    $LastOpenFo = (new OpenDeviceTypeFo)->getAll()->data;
    if(is_array(@$request->input('olt_host')))
    {
//FOR BULK
      $counterOpenOLT = 1;
      foreach ($request->input('olt_host', []) as $openFo => $value) {
        $counterOpenOLTPad = str_pad((string)$counterOpenOLT++, 2, '0', STR_PAD_LEFT);
        $dataFo = [
          'code' => $value.'~'.str_replace('TCK', 'TCK-DEV-'.$counterOpenOLTPad, $id),
          'HeaderCode' => $id,
          'DeviceTypeFO' => 'OLT'
        ];
        $openDeviceTypeFo = (new OpenDeviceTypeFo)->post($dataFo);
      }
    }
    else
    {
//FOR 1
      $dataFo = [
        'code' => $request->input('olt_host').'~'.str_replace('TCK', 'TCK-DEV-01', $id),
        'HeaderCode' => $id,
        'DeviceTypeFO' => 'OLT'
      ];
      $openDeviceTypeFo = (new OpenDeviceTypeFo)->post($dataFo);
    }


    return redirect()->route('ticketing.fmi.ticket-open.index')->with('notif_success', 'Open tiket has been Updated');
  }

  public function updateStatus($id, Request $request)
  {
    $prefix = 'T';
    $year = date('y');
    $month = date('m');
    $day = date('d');
    $hours = date('His');
    $sequence = str_pad($id, 7, '0', STR_PAD_LEFT);
    $codeStatus = $prefix . $year . $month . $day. $hours .$sequence;
    $transactionDate = Carbon::parse(now())->format('Y-m-d H:i:s');
    $fMIOpenTicket = (new FMIOpenTicket)->show($id)->data;
    $dataStatus = [
      'code' => $codeStatus,
      'TransactionDate' => $transactionDate,
      'BranchCode' => optional($fMIOpenTicket)->BranchCode,
      'TicketCode' => $id,
      'TicketStatus' => $request->TicketStatus,
      'RefNo' => $request->ref_no_status,
      'Remark' => $request->remark_staus,
      'CreatedBy' => 'admin'
    ];

    $ticketStatus = (new FMIOpenTicket)->postStatus($dataStatus);

    return redirect()->route('ticketing.fmi.ticket-open.show', $id)->with('notif_success', 'Ticket Status has been saved successfully');
  }

  public function destroy($id)
  {
    if(!getAuthMenu('ticketing.fmi.ticket-open.index',DELETE)){
      return redirect()->route('noauth');
    }
    $openBts = (new OpenBts)->getAll($id)->data;

    foreach ($openBts as $list) {
      (new OpenBts)->delete($list->code);
    }

    $openDeviceTypeFoes = (new OpenDeviceTypeFo)->getAll($id)->data;

    foreach ($openDeviceTypeFoes as $list) {
      (new OpenDeviceTypeFo)->delete($list->code);
    }

    $openDeviceTypeWlls = (new OpenDeviceTypeWll)->getAll($id)->data;

    foreach ($openDeviceTypeWlls as $list) {
      (new OpenDeviceTypeWll)->delete($list->code);
    }

    $ticketStatusCode = @(new FMITicketStatus)->getAll($id)->data[0]->code;

    if(!empty($ticketStatusCode))
    {
      $deleteTicketStatus = (new FMITicketStatus)->delete($ticketStatusCode);
    }


    $openTicket = (New FMIOpenTicket)->delete($id);

    return redirect()->route('ticketing.fmi.ticket-open.index')->with('notif_success', 'Open tiket has been deleted');
  }

  public function getDataNonMinor(Request $request)
  {
    $ticket = (new FMIOpenTicket)->getNonMinorTicket()->data;
    $ticket = (array)$this->unique_ticket($ticket);
    return DataTables::of($ticket)
    ->editColumn('transaction_date', function($ticket) {
      return date("d M Y - H:i:s", strtotime($ticket->TransactionDate));
    })
    ->editColumn('schedule_date', function($ticket) {
      return date("d M Y - H:i:s", strtotime($ticket->ScheduleDate));
    })
    ->addColumn('action', function($ticket) {
      $edit ='<a href="' . route('ticketing.fmi.ticket-open.edit', $ticket->code) . '" class="btn btn-icon btn-light btn-hover-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Edit">
      <span class="svg-icon svg-icon-md svg-icon-primary">
      <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
      <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
      <rect x="0" y="0" width="24" height="24"/>
      <path d="M12.2674799,18.2323597 L12.0084872,5.45852451 C12.0004303,5.06114792 12.1504154,4.6768183 12.4255037,4.38993949 L15.0030167,1.70195304 L17.5910752,4.40093695 C17.8599071,4.6812911 18.0095067,5.05499603 18.0083938,5.44341307 L17.9718262,18.2062508 C17.9694575,19.0329966 17.2985816,19.701953 16.4718324,19.701953 L13.7671717,19.701953 C12.9505952,19.701953 12.2840328,19.0487684 12.2674799,18.2323597 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.701953, 10.701953) rotate(-135.000000) translate(-14.701953, -10.701953) "/>
      <path d="M12.9,2 C13.4522847,2 13.9,2.44771525 13.9,3 C13.9,3.55228475 13.4522847,4 12.9,4 L6,4 C4.8954305,4 4,4.8954305 4,6 L4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 L2,6 C2,3.790861 3.790861,2 6,2 L12.9,2 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
      </g>
      </svg>
      </span>
      </a>';
      $delete = '<a data-href="' . route('ticketing.fmi.ticket-open.destroy', $ticket->code) . '" class="btn btn-icon btn-light btn-hover-danger btn-sm" "data-toggle="tooltip" data-placement="top" title="Delete" data-toggle="modal" data-target="#confirm-delete-modal">
      <span class="svg-icon svg-icon-md svg-icon-danger">
      <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
      <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
      <rect x="0" y="0" width="24" height="24"/>
      <path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero"/>
      <path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3"/>
      </g>
      </svg>
      </span>
      </a>';

      return $edit .' '. $delete;
    })
    ->rawColumns(['action', 'transaction_date', 'schedule_date'])
    ->make(true);
  }

  public function getDataStatus($id)
  {
    $showStatus = (new FMIOpenTicket)->showStatus($id)->data;
    return DataTables::of($showStatus)
    ->editColumn('transaction_date', function($showStatus) {
      return date("d M Y", strtotime($showStatus->TransactionDate));
    })
    ->rawColumns(['transaction_date'])
    ->make(true);
  }

  public function getDataMinor(Request $request)
  {
    $ticket = (new FMIOpenTicket)->getMinorTicket()->data;
    $ticket = (array)$this->unique_ticket($ticket);
    return DataTables::of($ticket)
    ->editColumn('transaction_date', function($ticket) {
      return date("d M Y - H:i:s", strtotime($ticket->TransactionDate));
    })
    ->editColumn('schedule_date', function($ticket) {
      return date("d M Y - H:i:s", strtotime($ticket->ScheduleDate));
    })
    ->addColumn('action', function($ticket) {
      $edit ='<a href="' . route('ticketing.fmi.ticket-open.edit', $ticket->code) . '" class="btn btn-icon btn-light btn-hover-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Edit">
      <span class="svg-icon svg-icon-md svg-icon-primary">
      <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
      <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
      <rect x="0" y="0" width="24" height="24"/>
      <path d="M12.2674799,18.2323597 L12.0084872,5.45852451 C12.0004303,5.06114792 12.1504154,4.6768183 12.4255037,4.38993949 L15.0030167,1.70195304 L17.5910752,4.40093695 C17.8599071,4.6812911 18.0095067,5.05499603 18.0083938,5.44341307 L17.9718262,18.2062508 C17.9694575,19.0329966 17.2985816,19.701953 16.4718324,19.701953 L13.7671717,19.701953 C12.9505952,19.701953 12.2840328,19.0487684 12.2674799,18.2323597 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.701953, 10.701953) rotate(-135.000000) translate(-14.701953, -10.701953) "/>
      <path d="M12.9,2 C13.4522847,2 13.9,2.44771525 13.9,3 C13.9,3.55228475 13.4522847,4 12.9,4 L6,4 C4.8954305,4 4,4.8954305 4,6 L4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 L2,6 C2,3.790861 3.790861,2 6,2 L12.9,2 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
      </g>
      </svg>
      </span>
      </a>';
      $delete = '<a data-href="' . route('ticketing.fmi.ticket-open.destroy', $ticket->code) . '" class="btn btn-icon btn-light btn-hover-danger btn-sm" "data-toggle="tooltip" data-placement="top" title="Delete" data-toggle="modal" data-target="#confirm-delete-modal">
      <span class="svg-icon svg-icon-md svg-icon-danger">
      <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
      <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
      <rect x="0" y="0" width="24" height="24"/>
      <path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero"/>
      <path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3"/>
      </g>
      </svg>
      </span>
      </a>';

      return $edit .' '. $delete;
    })
    ->rawColumns(['action', 'transaction_date', 'schedule_date'])
    ->make(true);
  }

  protected function unique_ticket($data = null)
  {
    $data = array_reverse($data);
    if($data == null)
    {
      return (object)array();
    }
    else
    {
      $unique = array();
      foreach ($data as $value)
      {
          $unique[$value->code] = $value;
      }
      $data = array_values($unique);

      return (object)$unique;
    }
  }
}
