<?php

namespace App\Http\Controllers\Ticketing;

use DataTables;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Services\Master\Team;
use App\Services\Master\Mitra;
use App\Services\Master\Branch;
use App\Services\Master\Department;
use App\Http\Controllers\Controller;
use App\Services\Master\ProblemType;
use App\Services\Ticketing\FMIOpenTicket;
use App\Services\Ticketing\FMITaskAssignment;
use App\Services\Ticketing\FMITicketStatus;
use App\Services\Master\BaseTransmissionStation;
use App\Services\Olt\Host;
use App\Services\Olt\Clients;
use App\Services\Master\CustomerNoc;
use App\Services\Ticketing\Master\OpenBts;
use App\Services\Ticketing\Master\OpenCustomer;
use App\Services\Ticketing\Master\OpenDeviceTypeFo;
use App\Services\Ticketing\Master\OpenDeviceTypeWll;

class FMITaskAssignmentController extends Controller
{
    public function index()
    {
      if(!getAuthMenu('ticketing.fmi.ticket-open.index',VIEW)){
        return redirect()->route('noauth');
      }
        return view('ticketing.fmi.task-assignment.index');
    }

    public function create()
    {
    	$branchs = (new Branch)->getAll()->data;
    	$problemTypes = (new ProblemType)->getAll()->data;
    	$openCustomers = (new OpenCustomer)->getAll()->data;
      $openBts = (new OpenBts)->getAll()->data;
      $openDeviceTypeFoes = (new OpenDeviceTypeFo)->getAll()->data;
      $openDeviceTypeWlls = (new OpenDeviceTypeWll)->getAll()->data;
      $departments = (new Department)->getAll()->data;
      $baseTransmissionStations = (new BaseTransmissionStation)->getAll()->data;
      $oltHost = (new Host)->getAll()->data;
    	return view('ticketing.fmi.status-ticket.create', compact('branchs', 'problemTypes', 'departments','openCustomers', 'openBts', 'openDeviceTypeFoes', 'openDeviceTypeWlls', 'baseTransmissionStations', 'oltHost'));
    }

    public function getCustomerByBTSOLT(Request $request)
    {
      $customerNoc = (new CustomerNoc)->getAll()->data;
      $filteredCustomerNoc = array();
      if(!empty(@$request->open_bts))
      {
        foreach ($customerNoc as $key => $row) {
          if(in_array($row->BTSCode, @$request->open_bts))
          {
            $filteredCustomerNoc[] = $row;
          }
        }
      }
      if(!empty(@$request->olt_host))
      {
        $oltClients = (new Clients)->getAll()->data;
        $filteredClients = array();
        foreach ($oltClients as $key => $row) {
          if(in_array($row->HostCode,@$request->olt_host))
          {
            $filteredClients[] = $row->CustomerCode;
          }
        }
        foreach ($customerNoc as $key => $row) {
          if(in_array($row->Code, $filteredClients) && empty($row->BTSCode))
          {
            $filteredCustomerNoc[] = $row;
          }
        }
      }
      return response(json_encode(['status'=>'true', 'data'=>$filteredCustomerNoc]), 200)->header('Content-Type', 'application/json');
      exit();
    }

    public function testing()
    {
        $branchs = (new Branch)->getAll()->data;
        $departments = (new Department)->getAll()->data;
        $problemTypes = (new ProblemType)->getAll()->data;
        $baseTransmissionStations = (new BaseTransmissionStation)->getAll()->data;

        return view('ticketing.fmi.status-ticket.form', compact('branchs', 'problemTypes', 'departments', 'baseTransmissionStations'));
    }

    public function store(Request $request)
    {
        // dd($request->all());
    	$this->validate($request, [
    		'TicketCode' => 'required',
    		'TransactionDate' => 'required',
        'ScheduleDate' => 'required',
    		'RefNo' => 'nullable|string|max:250',
    		'Remark' => 'nullable|string|max:250'
    	]);

      $taskAssignment = (new FMITaskAssignment)->post($request->all());
      return redirect()->route('ticketing.fmi.task-assignment.show',$request->TicketCode)->with('notif_success', 'Task Assignment has been saved successfully');
    }

    public function show($id)
    {
        $openBts = (new OpenBts)->getAll($id)->data;
        $fMIOpenTicket = (new FMIOpenTicket)->show($id)->data;
        $fMITicketStatus = @(new FMITicketStatus)->getAll($id)->data;
        $mitras = (new Mitra)->getAll()->data;
        $teams = (new Team)->getAll()->data;
        $openOlt = (new OpenDeviceTypeFo)->getAll($id)->data;
        $branchs = (new Branch)->getAll()->data;
        $departments = (new Department)->getAll()->data;
        $problemTypes = (new ProblemType)->getAll()->data;
        $baseTransmissionStations = (new BaseTransmissionStation)->getAll()->data;
        $transactionDate = Carbon::parse($fMIOpenTicket->TransactionDate)->format('d-m-Y');
        $scheduleDate = Carbon::parse($fMIOpenTicket->ScheduleDate)->format('d-m-Y');
        $openDeviceTypeFoes = (new OpenDeviceTypeFo)->getAll($id)->data;
        $openDeviceTypeWlls = (new OpenDeviceTypeWll)->getAll($id)->data;
        $openCustomers = (new OpenCustomer)->getAll($id)->data;
        $oltHost = (new Host)->getAll()->data;
        $fMITaskAssignment = (new FMITaskAssignment)->getAll($id)->data;
    	return view('ticketing.fmi.task-assignment.show', compact('mitras', 'teams', 'fMIOpenTicket', 'fMITaskAssignment', 'fMITicketStatus', 'branchs', 'departments', 'problemTypes', 'baseTransmissionStations', 'transactionDate', 'scheduleDate', 'openBts', 'openDeviceTypeFoes', 'openDeviceTypeWlls', 'openCustomers', 'oltHost', 'openOlt'));
    }

    public function edit($id)
    {
      if(!getAuthMenu('ticketing.fmi.ticket-open.index',UPDATE)){
        return redirect()->route('noauth');
      }

        $openBts = (new OpenBts)->getAll($id)->data;
        $fMIOpenTicket = (new FMIOpenTicket)->show($id)->data;
        $fMITicketStatus = (new FMITicketStatus)->show($id)->data;
        $openOlt = (new OpenDeviceTypeFo)->getAll($id)->data;
        $branchs = (new Branch)->getAll()->data;
        $departments = (new Department)->getAll()->data;
        $problemTypes = (new ProblemType)->getAll()->data;
        $baseTransmissionStations = (new BaseTransmissionStation)->getAll()->data;
        $transactionDate = Carbon::parse($fMIOpenTicket->TransactionDate)->format('d-m-Y');
        $scheduleDate = Carbon::parse($fMIOpenTicket->ScheduleDate)->format('d-m-Y');
        $openDeviceTypeFoes = (new OpenDeviceTypeFo)->getAll($id)->data;
        $openDeviceTypeWlls = (new OpenDeviceTypeWll)->getAll($id)->data;
        $openCustomers = (new OpenCustomer)->getAll($id)->data;
        $oltHost = (new Host)->getAll()->data;

    	  return view('ticketing.fmi.status-ticket.edit', compact('fMIOpenTicket', 'fMITicketStatus', 'branchs', 'departments', 'problemTypes', 'baseTransmissionStations', 'transactionDate', 'scheduleDate', 'openBts', 'openDeviceTypeFoes', 'openDeviceTypeWlls', 'openCustomers', 'oltHost', 'openOlt'));
    }

    public function update($id, Request $request)
    {

        $ticketCode = (New FMITaskAssignment)->show($id)->data->TicketCode;
        $taskAssignment = (New FMITaskAssignment)->put($id, $request->all());
        return redirect()->route('ticketing.fmi.task-assignment.show',$ticketCode)->with('notif_success', 'Open tiket has been Updated');
    }

    public function updateStatus($id, Request $request)
    {
        $prefix = 'T';
        $year = date('y');
        $month = date('m');
        $day = date('d');
        $hours = date('His');
        $sequence = str_pad($id, 7, '0', STR_PAD_LEFT);
        $codeStatus = $prefix . $year . $month . $day. $hours .$sequence;
        $transactionDate = Carbon::parse(now())->format('Y-m-d H:i:s');
        $fMIOpenTicket = (new FMIOpenTicket)->show($id)->data;
        $dataStatus = [
            'code' => $codeStatus,
            'TransactionDate' => $transactionDate,
            'BranchCode' => optional($fMIOpenTicket)->BranchCode,
            'TicketCode' => $id,
            'TicketStatus' => $request->TicketStatus,
            'RefNo' => $request->ref_no_status,
            'Remark' => $request->remark_staus,
            'CreatedBy' => 'admin'
        ];

        $ticketStatus = (new FMIOpenTicket)->postStatus($dataStatus);

        return redirect()->route('ticketing.fmi.ticket-open.show', $id)->with('notif_success', 'Ticket Status has been saved successfully');
    }

    public function destroy($id)
    {
      if(!getAuthMenu('ticketing.fmi.ticket-open.index',DELETE)){
        return redirect()->route('noauth');
      }
        $ticketCode = (New FMITaskAssignment)->show(pack("H*",$id))->data->TicketCode;
        $taskAssigment = (New FMITaskAssignment)->delete(pack("H*",$id));

        return redirect()->route('ticketing.fmi.task-assignment.show',$ticketCode)->with('notif_success', 'Task assignment has been deleted');
    }

    public function getData()
    {
    	$ticket = (new FMIOpenTicket)->getAllTicket()->data;
      $ticket = (array)$this->unique_ticket($ticket);
      $departments = (new Department)->getAll()->data;
      $departmentsAttributeOnlyWithTicketCodeAsKey = array();

      foreach ($departments as $key => $row) {
        $departmentsAttributeOnlyWithTicketCodeAsKey[$row->Code] = $row->Name;
      }

      return DataTables::of($ticket)
                ->editColumn('ResponsibilityDepartmentCode', function($ticket) use($departmentsAttributeOnlyWithTicketCodeAsKey) {
                  return !empty($departmentsAttributeOnlyWithTicketCodeAsKey[$ticket->ResponsibilityDepartmentCode]) ? $departmentsAttributeOnlyWithTicketCodeAsKey[$ticket->ResponsibilityDepartmentCode] : '-';
                })
                ->editColumn('transaction_date', function($ticket) {
                	return date("d M Y - H:i:s", strtotime($ticket->TransactionDate));
                })
                ->editColumn('schedule_date', function($ticket) {
                	return date("d M Y - H:i:s", strtotime($ticket->ScheduleDate));
                })
                ->addColumn('action', function($ticket) {
                   $detail = '<a href="' .route('ticketing.fmi.task-assignment.show', $ticket->code). '" class="btn btn-icon btn-light btn-hover-success btn-sm" data-toggle="tooltip" data-placement="top" title="Detail">
  					            <span class="svg-icon svg-icon-md svg-icon-success">
  					                <!--begin::Svg Icon | path:media/svg/icons/General/Settings-1.svg-->
  					                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
  					                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
  					                        <rect x="0" y="0" width="24" height="24"/>
  					                        <path d="M7,3 L17,3 C19.209139,3 21,4.790861 21,7 C21,9.209139 19.209139,11 17,11 L7,11 C4.790861,11 3,9.209139 3,7 C3,4.790861 4.790861,3 7,3 Z M7,9 C8.1045695,9 9,8.1045695 9,7 C9,5.8954305 8.1045695,5 7,5 C5.8954305,5 5,5.8954305 5,7 C5,8.1045695 5.8954305,9 7,9 Z" fill="#000000"/>
  					                        <path d="M7,13 L17,13 C19.209139,13 21,14.790861 21,17 C21,19.209139 19.209139,21 17,21 L7,21 C4.790861,21 3,19.209139 3,17 C3,14.790861 4.790861,13 7,13 Z M17,19 C18.1045695,19 19,18.1045695 19,17 C19,15.8954305 18.1045695,15 17,15 C15.8954305,15 15,15.8954305 15,17 C15,18.1045695 15.8954305,19 17,19 Z" fill="#000000" opacity="0.3"/>
  					                    </g>
  					                </svg>
  					                <!--end::Svg Icon-->
  					            </span>
  					        </a>';

                    return $detail;
                })
                ->rawColumns(['action', 'transaction_date', 'schedule_date'])
                ->make(true);
    }

    protected function unique_ticket($data = null)
    {
      $data = array_reverse($data);
      if($data == null)
      {
        return (object)array();
      }
      else
      {
        $unique = array();
        foreach ($data as $value)
        {
            $unique[$value->code] = $value;
        }
        $data = array_values($unique);

        return (object)$unique;
      }
    }

}
