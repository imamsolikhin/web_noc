<?php
namespace App\Http\Controllers\Olt;

use DataTables;
use Illuminate\Http\Request;
use App\Services\Olt\Customer;
use App\Services\Olt\Clients;
use App\Services\Olt\Isp;
use App\Http\Controllers\Controller;

class CustomerController extends Controller
{

    public function index()
    {
        if(!getAuthMenu('olt.customer.index',VIEW)){
          return redirect()->route('noauth');
        }

        return view('olt.customer.index');
    }

    public function page($page)
    {
        $datas['isp_list'] = (new Isp())->getAllActive()->data;
        if ($page == "new") {
            if(!getAuthMenu('olt.customer.index',CREATE)){
              return redirect()->route('noauth');
            }
            $datas['customer'] = (new Customer())->show($page)->data;
            return view('olt.customer.form', $datas);
        } else {
            if(!getAuthMenu('olt.customer.index',UPDATE)){
              return redirect()->route('noauth');
            }
          $datas['customer'] = (new Customer())->show($page)->data;
          return view('olt.customer.form', $datas);
      }
  }

    public function store(Request $request)
    {
        if(!getAuthMenu('olt.customer.index',CREATE)){
          return redirect()->route('noauth');
        }
        $this->validate($request, [
          'Name' => 'nullable|max:250',
          'LinkID' => 'nullable|max:250',
          'MemberID' => 'nullable|max:250',
          'JobTitle' => 'nullable|max:250',
          'ContactPerson' => 'nullable|max:250',
          'Email' => 'nullable|max:250',
          'Phone1' => 'nullable|max:250',
          'Phone2' => 'nullable|max:250',
          'PhoneNumber' => 'nullable|max:250',
          'NPWP' => 'nullable|max:250',
          'Fax' => 'nullable|max:250',
          'Address' => 'nullable|max:250',
          'CityCode' => 'nullable|max:250',
          'City' => 'nullable|max:250',
          'Province' => 'nullable|max:250',
          'ZipCode' => 'nullable|max:250',
          'Website' => 'nullable|max:250',
          'IDCard' => 'nullable|max:250',
          'IDCardNumber' => 'nullable|max:250',
          'IDCardExpired' => 'nullable|max:250',
          'RegistrationDate' => 'nullable|max:250',
          'ProductCode' => 'nullable|max:250',
          'ClientStatus' => 'nullable|max:250',
          'ProductSite' => 'nullable|max:250',
          'ProductGrup' => 'nullable|max:250',
          'ProductPort' => 'nullable|max:250',
          'ProductCategory' => 'nullable|max:250',
          'BaseTransmissionStationCode' => 'nullable|max:250',
          'ProductName' => 'nullable|max:250',
          'ProductNotes' => 'nullable|max:250',
          'ActiveStatus' => 'nullable|max:250'
        ]);
        // dd($customer);
        $customer = (new Customer())->post($request->all());

        return redirect()->route('olt.customer.index')->with('notif_success', 'New Customer has been added successfully!');
    }

    public function regist($id)
    {
        $clients = (new Clients())->show($id);
        $customer = (new Customer())->show($id)->data;
        if(!isset($clients->data)){
          $data["Code"] = str_replace(' ', '', $customer->Code);
          $data["CustomerCode"] = $customer->Code;
          $data["IspCode"] = $customer->LinkID;
          $clients = (new Clients())->post($data);
        }
        return redirect(route('olt.clients.page', $customer->Code));
    }

    public function show($id)
    {
        $customer = (new Customer())->show($id)->data;

        return makeResponse(200, 'success', null, $customer);
    }

    public function update($id, Request $request)
    {
        if(!getAuthMenu('olt.customer.index',UPDATE)){
          return redirect()->route('noauth');
        }
        $this->validate($request, [
          'Name' => 'nullable|max:250',
          'LinkID' => 'nullable|max:250',
          'MemberID' => 'nullable|max:250',
          'JobTitle' => 'nullable|max:250',
          'ContactPerson' => 'nullable|max:250',
          'Email' => 'nullable|max:250',
          'Phone1' => 'nullable|max:250',
          'Phone2' => 'nullable|max:250',
          'PhoneNumber' => 'nullable|max:250',
          'NPWP' => 'nullable|max:250',
          'Fax' => 'nullable|max:250',
          'Address' => 'nullable|max:250',
          'CityCode' => 'nullable|max:250',
          'City' => 'nullable|max:250',
          'Province' => 'nullable|max:250',
          'ZipCode' => 'nullable|max:250',
          'Website' => 'nullable|max:250',
          'IDCard' => 'nullable|max:250',
          'IDCardNumber' => 'nullable|max:250',
          'IDCardExpired' => 'nullable|max:250',
          'RegistrationDate' => 'nullable|max:250',
          'ProductCode' => 'nullable|max:250',
          'ClientStatus' => 'nullable|max:250',
          'ProductSite' => 'nullable|max:250',
          'ProductGrup' => 'nullable|max:250',
          'ProductPort' => 'nullable|max:250',
          'ProductCategory' => 'nullable|max:250',
          'BaseTransmissionStationCode' => 'nullable|max:250',
          'ProductName' => 'nullable|max:250',
          'ProductNotes' => 'nullable|max:250',
          'ActiveStatus' => 'nullable|max:250'
        ]);
        // dd($request);
        $customer = (new Customer())->put($id, $request->all());

        return redirect()->route('olt.customer.index')->with('notif_success', 'Customer has been update successfully!');
    }

    public function destroy($id)
    {
        if(!getAuthMenu('olt.customer.index',DELETE)){
          return redirect()->route('noauth');
        }
        $customer = (new Customer())->delete($id);
        return redirect()->back()->with('notif_success', 'Customer has been deleted!');
    }

    public function getData(Request $request)
    {
        $results = (new Customer())->getAll($request);
        $paging = paginator($results);
        $list = array();
        $no = intval($request->start);
        foreach ($results->data as $datas) {
            $no++;
            $row = array();
            $datas->DT_RowIndex = $no;
            $list[] = $datas;
        }
        return response()->json([
            "draw" => intval($request->draw),
            "recordsTotal"    => intval($paging->total_records),
            "recordsFiltered" => intval($paging->total_records),
            "data" => $list
        ]);
    }
}
