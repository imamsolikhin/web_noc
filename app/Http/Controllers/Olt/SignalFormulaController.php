<?php
namespace App\Http\Controllers\Olt;

use DataTables;
use Illuminate\Http\Request;
use App\Services\Olt\SignalFormula;
use App\Http\Controllers\Controller;

class SignalFormulaController extends Controller
{

    public function index()
    {
        if(!getAuthMenu('olt.signal-formula.index',VIEW)){
          return redirect()->route('noauth');
        }
        return view('olt.signal-formula.index');
    }

    public function store(Request $request)
    {
        if(!getAuthMenu('olt.signal-formula.index',CREATE)){
          return redirect()->route('noauth');
        }
        $this->validate($request, [
            'Code' => 'nullable|max:250',
            'Formula' => 'nullable|max:250',
            'TxPower' => 'nullable|max:250',
            'Distance' => 'nullable|max:250',
            'LossDb' => 'nullable|max:250',
            'LossFixTop' => 'nullable|max:250',
            'LossFixBot' => 'nullable|max:250',
            'ActiveStatus' => 'nullable|max:250'
        ]);

        $SignalFormula = (new SignalFormula())->post($request->all());

        return redirect()->route('olt.signal-formula.index')->with('notif_success', 'New SignalFormula has been added successfully!');
    }

    public function show($id)
    {
        $SignalFormula = (new SignalFormula())->show($id)->data;

        return makeResponse(200, 'success', null, $SignalFormula);
    }

    public function update($id, Request $request)
    {
        if(!getAuthMenu('olt.signal-formula.index',UPDATE)){
          return redirect()->route('noauth');
        }
        $this->validate($request, [
            'Code' => 'nullable|max:250',
            'Formula' => 'nullable|max:250',
            'TxPower' => 'nullable|max:250',
            'Distance' => 'nullable|max:250',
            'LossDb' => 'nullable|max:250',
            'LossFixTop' => 'nullable|max:250',
            'LossFixBot' => 'nullable|max:250',
            'ActiveStatus' => 'nullable|max:250'
        ]);

        $SignalFormula = (new SignalFormula())->put($id, $request->all());

        return redirect()->route('olt.signal-formula.index')->with('notif_success', 'SignalFormula has been update successfully!');
    }

    public function destroy($id)
    {
        if(!getAuthMenu('olt.signal-formula.index',DELETE)){
          return redirect()->route('noauth');
        }
        $SignalFormula = (new SignalFormula())->delete($id);

        return redirect()->back()->with('notif_success', 'SignalFormula has been deleted!');
    }

    public function getData(Request $request)
    {
        $results = (new SignalFormula())->getAllActive($request);
        $paging = paginator($results);
        $list = array();
        $no = intval($request->start);
        foreach ($results->data as $datas) {
            $no++;
            $row = array();
            $datas->DT_RowIndex = $no;
            $list[] = $datas;
        }
        return response()->json([
            "draw" => intval($request->draw),
            "recordsTotal"    => intval($paging->total_records),
            "recordsFiltered" => intval($paging->total_records),
            "data" => $list
        ]);
    }
}
