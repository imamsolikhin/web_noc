<?php
namespace App\Http\Controllers\Olt;

use DataTables;
use Illuminate\Http\Request;
use App\Services\Olt\Ont;
use App\Services\Olt\Host;
use App\Http\Controllers\Controller;
use App\Services\Telnet\Olt;
use \stdClass;


class OntController extends Controller
{

    public function index()
    {
        // if(!getAuthMenu('olt.ont.index',VIEW)){
        //   return redirect()->route('noauth');
        // }
        $host_list = (new Host())->getAllActive();
        $datas['host_list'] = (isset($host_list->data)) ? $host_list->data : null;

        return view('olt.ont.index', $datas);
    }

    public function store(Request $request)
    {
        if(!getAuthMenu('olt.ont.index',CREATE)){
          return redirect()->route('noauth');
        }
        $this->validate($request, [
            'Code' => 'nullable|max:250',
            'HostCode' => 'nullable|max:250',
            'Sn' => 'nullable|max:250',
            'Version' => 'nullable|max:250',
            'SoftwareVersion' => 'nullable|max:250',
            'EquipmentId' => 'nullable|max:250',
            'FrameId' => 'nullable|max:250',
            'SlotId' => 'nullable|max:250',
            'PortId' => 'nullable|max:250',
            'RxPower' => 'nullable|max:250',
            'TxPower' => 'nullable|max:250',
            'ActiveStatus' => 'nullable|max:250'

            // 'Name' => 'required|string|max:250',
            // 'CityCode' => 'nullable|string|max:250',
            // 'Phone1' => 'nullable|numeric|digits_between:6,15',
            // 'Phone2' => 'nullable|numeric|digits_between:6,15',
            // 'Fax' => 'nullable|string|max:250',
            // 'Email' => 'nullable|email',
            // 'COntactPerson' => 'nullable|string|max:250'
        ]);

        $Ont = (new Ont())->post($request->all());

        return redirect()->route('olt.ont.index')->with('notif_success', 'New Ont has been added successfully!');
    }

    public function show($id)
    {
        $Ont = (new Ont())->show($id)->data;
        return makeResponse(200, 'success', null, $Ont);
    }

    public function update($id, Request $request)
    {
        if(!getAuthMenu('olt.ont.index',UPDATE)){
          return redirect()->route('noauth');
        }
        $this->validate($request, [
          'Code' => 'nullable|max:250',
          'HostCode' => 'nullable|max:250',
          'Sn' => 'nullable|max:250',
          'Version' => 'nullable|max:250',
          'SoftwareVersion' => 'nullable|max:250',
          'EquipmentId' => 'nullable|max:250',
          'FrameId' => 'nullable|max:250',
          'SlotId' => 'nullable|max:250',
          'PortId' => 'nullable|max:250',
          'RxPower' => 'nullable|max:250',
          'TxPower' => 'nullable|max:250',
          'ActiveStatus' => 'nullable|max:250'
        ]);

        $Ont = (new Ont())->put($id, $request->all());

        return redirect()->route('olt.ont.index')->with('notif_success', 'Ont has been update successfully!');
    }

    public function destroy($id)
    {
        if(!getAuthMenu('olt.ont.index',DELETE)){
          return redirect()->route('noauth');
        }
        $Ont = (new Ont())->delete($id);

        return redirect()->back()->with('notif_success', 'Ont has been deleted!');
    }

    public function getData(Request $request)
    {
        $results = (new Ont)->getAllActive(null);
        $paging = paginator($results);
        $list = array();
        $no = intval($request->start);
        foreach ($results->data as $datas) {
            $no++;
            $row = array();
            $datas->DT_RowIndex = $no;
            $list[] = $datas;
        }
        return response()->json([
            "draw" => intval($request->draw),
            "recordsTotal"    => intval($paging->total_records),
            "recordsFiltered" => intval($paging->total_records),
            "data" => $list
        ]);
    }
  }
