<?php
namespace App\Http\Controllers\Olt;

use DataTables;
use Illuminate\Http\Request;
use App\Services\Olt\Clients;
use App\Services\Olt\ClientsVlanService;
use App\Services\Olt\Isp;
use App\Services\Olt\Host;
use App\Services\Olt\Ont;
use App\Services\Olt\Customer;
use App\Services\Olt\Template;
use App\Services\Olt\LineProfile;
use App\Services\Olt\SrvProfile;
use App\Services\Olt\TrafficTable;
use App\Services\Olt\ClientsHistory;
use App\Services\Telnet\Olt;
use App\Services\Olt\ClientsTools;
use App\Http\Controllers\Controller;

class ClientsHistoryController extends Controller
{

    public function index()
    {
        if(!getAuthMenu('olt.clients-history.index',VIEW)){
          return redirect()->route('noauth');
        }

        $ont_list = (new Ont())->getAllActive(Null,Null);
        $datas['ont_list'] = (isset($ont_list->data)) ? $ont_list->data : null;
        return view('olt.clients-history.index',$datas);
    }

    public function store(Request $request)
    {
        if(!getAuthMenu('olt.clients-history.index',CREATE)){
          return redirect()->route('noauth');
        }

        $this->validate($request, [
            'IspCode' => 'nullable|max:250',
            'HostCode' => 'nullable|max:250',
            'CustomerCode' => 'nullable|max:250',
            'TemplateCode' => 'nullable|max:250',
            'VlanDownLink' => 'nullable|max:250',
            'FrameId' => 'nullable|max:250',
            'SlotId' => 'nullable|max:250',
            'PortId' => 'nullable|max:250',
            'OntVersion' => 'nullable|max:250',
            'OntSoftware' => 'nullable|max:250',
            'OntSn' => 'nullable|max:250',
            'ManagementMode' => 'nullable|max:250',
            'OntLineProfileId' => 'nullable|max:250',
            'OntSrvProfileId' => 'nullable|max:250',
            'VlanId'=> 'nullable|max:250',
            'VlanFrameId' => 'nullable|max:250',
            'VlanSlotId' => 'nullable|max:250',
            'VlanPortId' => 'nullable|max:250',
            'BoardID' => 'nullable|max:250',
            'TrafficTableId' => 'nullable|max:250',
            'NativeVlanEth1' => 'nullable|max:250',
            'NativeVlanEth2' => 'nullable|max:250',
            'NativeVlanEth3' => 'nullable|max:250',
            'NativeVlanEth4' => 'nullable|max:250'
        ]);

        $clients = (new Clients())->post($request->all());

        return redirect()->route('olt.clients-history.index')->with('notif_success', 'New Clients has been added successfully!');
    }

    public function show($id)
    {
        $results["clients"] = (new Clients())->show($id)->data;
        $results["host"] = (new Host)->show($results['clients']->HostCode)->data;
        $results["list_clientstools"] = (new ClientsTools)->show($id)->data;
        $results["clients_vlan_service"] = (new ClientsVlanService())->getAllActive($results["clients"]->CustomerCode)->data;

        return makeResponse(200, 'success', null, $results);
    }

    public function getData(Request $request)
    {
        $results = (new clients())->getAll($request)->data;
        // dd($results);

        return DataTables::of($results)
                          ->addIndexColumn()
                          ->addColumn('active', function ($clients) {
                                return '<i class="la la-close icon-lg text-danger"></i>';
                          })
                          ->rawColumns(['active'])
                          ->make(true);
    }
}
