<?php
namespace App\Http\Controllers\Olt;

use DataTables;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use App\Services\Olt\Clients;
use App\Services\Olt\ClientsHistory;
use App\Services\Olt\ClientsVlanService;
use App\Services\Olt\Isp;
use App\Services\Olt\Host;
use App\Services\Olt\Ont;
use App\Services\Olt\Customer;
use App\Services\Olt\Template;
use App\Services\Olt\LineProfile;
use App\Services\Olt\SrvProfile;
use App\Services\Olt\TrafficTable;
use App\Services\Olt\TemplateDetail;
use App\Services\Telnet\Olt;
use App\Services\Olt\ClientsTools;
use App\Services\Olt\OntOpticalInfo;
use App\Http\Controllers\Controller;

class ClientsController extends Controller
{

    public function index()
    {
        if(!getAuthMenu('olt.clients.index',VIEW)){
          return redirect()->route('noauth');
        }

        $ont_list = (new Ont())->getAllActive(Null,Null);
        $datas = [];
        $host_list = (new Host())->getAllActive();
        $datas['host_list'] = (isset($host_list->data)) ? $host_list->data : null;
        // $datas['ont_list'] = (isset($ont_list->data)) ? $ont_list->data : null;
        return view('olt.clients.index',$datas);
    }

    public function page($page)
    {
        if ($page == "new") {
            if(!getAuthMenu('olt.clients.index',CREATE)){
              return redirect()->route('noauth');
            }
            $datas['$clients'] = Null;
            return view('olt.clients.form', $datas);
        } else {
            if(!getAuthMenu('olt.clients.index',UPDATE)){
              return redirect()->route('noauth');
            }
            $datas['clients'] = (new Clients())->show(str_replace(' ', '', $page))->data;
            if(isset($datas['clients']->HistoryDate)){
               $datas['clients']->HistoryDate = date_format(date_create($datas['clients']->HistoryDate),'d-m-Y H:i:s');
            }
            $host_list = (new Host())->getAllActive();
            $ont_list = (new Ont())->getAllActive(Null,Null);
            $services_list = (new ClientsVlanService())->getAllActive($datas['clients']->CustomerCode);
            $lineprofile_list = (new LineProfile())->getAllActive($datas['clients']->HostCode);
            $serviceprofile_list = (new SrvProfile())->getAllActive($datas['clients']->HostCode);
            $traffictable_list = (new TrafficTable())->getAllActive($datas['clients']->HostCode);
            $datas['host_list'] = (isset($host_list->data)) ? $host_list->data : null;
            $datas['ont_list'] = (isset($ont_list->data)) ? $ont_list->data : null;
            $datas['services_list'] = (isset($services_list->data)) ? $services_list->data : null;
            $datas['lineprofile_list'] = (isset($lineprofile_list->data)) ? $lineprofile_list->data : null;
            $datas['serviceprofile_list'] = (isset($serviceprofile_list->data)) ? $serviceprofile_list->data : null;
            $datas['traffictable_list'] = (isset($traffictable_list->data)) ? $traffictable_list->data : null;
            $datas['template_list'] = (isset((new Template())->getAllActive()->data)) ? (new Template())->getAllActive()->data : null;
            return view('olt.clients.form', $datas);
        }
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'IspCode' => 'nullable|max:250',
            'HostCode' => 'nullable|max:250',
            'CustomerCode' => 'nullable|max:250',
            'TemplateCode' => 'nullable|max:250',
            'VlanDownLink' => 'nullable|max:250',
            'FrameId' => 'nullable|max:250',
            'SlotId' => 'nullable|max:250',
            'PortId' => 'nullable|max:250',
            'OntVersion' => 'nullable|max:250',
            'OntSoftware' => 'nullable|max:250',
            'OntSn' => 'nullable|max:250',
            'OntId' => 'nullable|max:250',
            'OntRun' => 'nullable|max:250',
            'ManagementMode' => 'nullable|max:250',
            'OntLineProfileId' => 'nullable|max:250',
            'OntSrvProfileId' => 'nullable|max:250',
            'VlanId'=> 'nullable|max:250',
            'VlanFrameId' => 'nullable|max:250',
            'VlanSlotId' => 'nullable|max:250',
            'VlanPortId' => 'nullable|max:250',
            'BoardID' => 'nullable|max:250',
            'Response' => 'nullable|max:250',
            // 'LasDownCause' => 'nullable|max:250',
            // 'LastUpTime' => 'nullable|max:250',
            // 'LasDownTime' => 'nullable|max:250',
            // 'LasDyingGasp' => 'nullable|max:250',
            // 'OntOnlineDuration' => 'nullable|max:250',
            'TrafficTableId' => 'nullable|max:250',
            'NativeVlanEth1' => 'nullable|max:250',
            'NativeVlanEth2' => 'nullable|max:250',
            'NativeVlanEth3' => 'nullable|max:250',
            'NativeVlanEth4' => 'nullable|max:250',
            'UpdatedDate' => 'nullable|date_format:Y-m-d H:i:s'
        ]);

        $clients = (new Clients())->post($request->all());

        return redirect()->route('olt.clients.index')->with('notif_success', 'New Clients has been added successfully!');
    }

    public function show($id)
    {
        $results["clients"] = (new Clients())->show($id)->data;
        $results["host"] = (new Host)->show($results['clients']->HostCode)->data;
        $results["list_clientstools"] = (new ClientsTools)->show($id)->data;
        // $results["list_ontopticalinfo"] = (new OntOpticalInfo)->show($id)->data;
        $results["clients_vlan_service"] = (new ClientsVlanService())->getAllActive($id)->data;

        return makeResponse(200, 'success', null, $results);
    }

    public function show_header($id)
    {

        $results["clients"] = $clients = (new Clients())->show($id)->data;

        $clients_history = (new ClientsHistory())->getAllActive($id);
        $template_detail = (new TemplateDetail())->getAllActive($id);
        $results["clients_history"] = (isset($clients_history->data)) ? $clients_history->data : null;
        $results["template_detail"] = (isset($template_detail->data)) ? $template_detail->data : null;

        return makeResponse(200, 'success', null, $results);
    }

    public function update_version($id) {

      // $results["clients"] = $clients = (new Clients())->show($id)->data;
      $results["update-version"] = (new Clients())->getAllActive($results["clients"]->OntRun)->data;

      return makeResponse(200, 'success', null, $results);
    }

    public function update($id, Request $request)
    {
        if(!getAuthMenu('olt.clients.index',UPDATE)){
          return redirect()->route('noauth');
        }

        $this->validate($request, [
            'Code' => 'nullable|max:250',
            'IspCode' => 'nullable|max:250',
            'HostCode' => 'nullable|max:250',
            'CustomerCode' => 'nullable|max:250',
            'TemplateCode' => 'nullable|max:250',
            'VlanDownLink' => 'nullable|max:250',
            'FrameId' => 'nullable|max:250',
            'SlotId' => 'nullable|max:250',
            'PortId' => 'nullable|max:250',
            'OntVersion' => 'nullable|max:250',
            'OntSoftware' => 'nullable|max:250',
            'OntSn' => 'nullable|max:250',
            'OntId' => 'nullable|max:250',
            'OntRun' => 'nullable|max:250',
            'ManagementMode' => 'nullable|max:250',
            'OntLineProfileId' => 'nullable|max:250',
            'OntSrvProfileId' => 'nullable|max:250',
            'VlanId' => 'nullable|max:250',
            'VlanFrameId' => 'nullable|max:250',
            'VlanSlotId' => 'nullable|max:250',
            'VlanPortId' => 'nullable|max:250',
            'BoardID' => 'nullable|max:250',
            'Response' => 'nullable|max:250',
            // 'lasDownCause' => 'nullable|max:250',
            // 'lastUpTime' => 'nullable|max:250',
            // 'lasDownTime' => 'nullable|max:250',
            // 'lasDyingGasp' => 'nullable|max:250',
            // 'ontOnlineDuration' => 'nullable|max:250',
            'TrafficTableId' => 'nullable|max:250',
            'NativeVlanEth1' => 'nullable|max:250',
            'NativeVlanEth2' => 'nullable|max:250',
            'NativeVlanEth3' => 'nullable|max:250',
            'NativeVlanEth4' => 'nullable|max:250',
            'UpdatedDate' => 'nullable|date_format:Y-m-d H:i:s'
        ]);
        // $clients_old = (new Clients())->show($request->Code)->data;
        $clients = (new Clients())->put($id, $request->all())->data;
        if(isset($request->s_vlan)){
          $clients_delete = (new ClientsVlanService())->delete($request->Code);
          for ($i=0; $i < count($request->s_vlan); $i++){
              $data_vlan = [
                'Code' => $request->CustomerCode."-".$request->Sn."-".($i+1),
                'CustomerCode' => $request->CustomerCode,
                'ServicePortId' => $request->s_idx[$i],
                'Vlan' => $request->s_vlan[$i],
                'GemPort' => $request->s_gemport[$i],
                'InnerVlan' => $request->s_inner[$i],
                'UserVlan' => $request->s_user[$i],
                'TagTransform' => $request->s_tag[$i],
                'TrafficTable' => $request->s_traffic[$i],
              ];

              $clients_vlan = (new ClientsVlanService())->post($data_vlan);
          }
        }
        return redirect()->route('olt.clients.index')->with('notif_success', 'Clients has been update successfully!');
    }

    public function destroy($id)
    {
        $clients = (new Clients())->delete($id);
        $clients_delete = (new ClientsVlanService())->delete($id);

        return redirect()->back()->with('notif_success', 'Clients has been deleted!');
    }



    public function getData(Request $request)
    {
        $results = (new clients())->getAll($request);
        $paging = paginator($results);
        $list = array();
        $no = intval($request->start);
        if ($results){
          foreach ($results->data as $datas) {
              $no++;
              $row = array();
              $datas->DT_RowIndex = $no;
              $list[] = $datas;
          }
        }
        return response()->json([
            "draw" => intval($request->draw),
            "recordsTotal"    => intval($paging->total_records),
            "recordsFiltered" => intval($paging->total_records),
            "data" => $list
        ]);
    }
}
