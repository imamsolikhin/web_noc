<?php
namespace App\Http\Controllers\Olt;

use DataTables;
use Illuminate\Http\Request;
use App\Services\Olt\Host;
use App\Services\Olt\TrafficTable;
use App\Services\Olt\SrvProfile;
use App\Services\Olt\LineProfile;
use App\Services\Olt\PortVlan;
use App\Services\Olt\TrafficTableLast;
use App\Services\Olt\SrvProfileLast;
use App\Services\Olt\LineProfileLast;
use App\Services\Olt\PortVlanLast;
use App\Services\Olt\DbaProfile;
use App\Services\Olt\DbaProfileLast;
use App\Services\Olt\GemProfile;
use App\Services\Olt\GemProfileLast;
use App\Services\Olt\SrvPort;
use App\Services\Olt\SrvPortLast;
use App\Services\Olt\InterfaceGpon;
use App\Services\Olt\InterfaceGponLast;
use App\Services\Olt\Ont;
use App\Services\Olt\OntLast;
use App\Services\Olt\Brand;
use App\Services\Master\BaseTransmissionStation;
use App\Services\Telnet\Olt;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class HostController extends Controller
{

    public function index()
    {
        if(!getAuthMenu('olt.host.index',VIEW)){
          return redirect()->route('noauth');
        }
        $bts_list = (new BaseTransmissionStation())->getAll();
        $brand_list = (new Brand())->getAllActive();
        if ($bts_list)
          $data['bts_list'] = $bts_list->data;
        if ($brand_list)
          $data['device_list'] = $brand_list->data;

        return view('olt.host.index', $data);
    }

    public function store(Request $request)
    {
        if(!getAuthMenu('olt.host.index',CREATE)){
          return redirect()->route('noauth');
        }
        $this->validate($request, [
            'Hostname' => 'nullable|max:250',
            'IpAddress' => 'nullable|max:250',
            'Username' => 'nullable|max:250',
            'Password' => 'nullable|max:250',
            'Remark' => 'nullable|max:250',
            'Port' => 'nullable|max:250',
            'SnmpCommunity' => 'nullable|max:250',
            'Sysname' => 'nullable|max:250',
            'DeviceModel' => 'nullable|max:250',
            'DeviceVersion' => 'nullable|max:250'
        ]);

        $host = (new Host())->post($request->all());

        return redirect()->route('olt.host.index')->with('notif_success', 'New Host has been added successfully!');
    }

    public function show($id)
    {
        $host = (new Host())->show($id)->data;

        return makeResponse(200, 'success', null, $host);
    }

    public function show_config($id)
    {

        $results["host"] = $host = (new Host())->show($id)->data;
        $results["traffic-table"] = (new TrafficTable())->getAllActive($results["host"]->Code)->data;
        $results["traffic-table-last"] = (new TrafficTableLast())->getAllActive($results["host"]->Code)->data;
        $results["srv-profile"] = (new SrvProfile())->getAllActive($results["host"]->Code)->data;
        $results["line-profile"] = (new LineProfile())->getAllActive($results["host"]->Code)->data;
        $results["srv-profile-last"] = (new SrvProfileLast())->getAllActive($results["host"]->Code)->data;
        $results["line-profile-last"] = (new LineProfileLast())->getAllActive($results["host"]->Code)->data;
        $results["dba-profile"] = (new DbaProfile())->getAllActive($results["host"]->Code)->data;
        $results["dba-profile-last"] = (new DbaProfileLast())->getAllActive($results["host"]->Code)->data;
        $results["port-vlan"] = (new PortVlan())->getAllActive($results["host"]->Code)->data;
        $results["port-vlan-last"] = (new PortVlanLast())->getAllActive($results["host"]->Code)->data;
        $results["gem-port"] = (new GemProfile())->getAllActive($results["host"]->Code)->data;
        $results["gem-port-last"] = (new GemProfileLast())->getAllActive($results["host"]->Code)->data;
        $results["service-port"] = (new SrvPort())->getAllActive($results["host"]->Code)->data;
        $results["service-port-last"] = (new SrvPortLast())->getAllActive($results["host"]->Code)->data;
        $results["ont-info"] = (new InterfaceGpon())->getAllActive($results["host"]->Code)->data;
        $results["ont-info-last"] = (new InterfaceGponLast())->getAllActive($results["host"]->Code)->data;
        $results["ont-autofind"] = (new Ont())->getAllActive($results["host"]->Code)->data;
        $results["ont-autofind-last"] = (new OntLast())->getAllActive($results["host"]->Code)->data;

        return makeResponse(200, 'success', null, $results);
    }

    public function update($id, Request $request)
    {
        if(!getAuthMenu('olt.host.index',UPDATE)){
          return redirect()->route('noauth');
        }
        $this->validate($request, [
          'Hostname' => 'nullable|max:250',
          'IpAddress' => 'nullable|max:250',
          'Username' => 'nullable|max:250',
          'Password' => 'nullable|max:250',
          'Remark' => 'nullable|max:250',
          'Port' => 'nullable|max:250',
          'SnmpCommunity' => 'nullable|max:250',
          'Sysname' => 'nullable|max:250',
          'DeviceModel' => 'nullable|max:250',
          'DeviceVersion' => 'nullable|max:250'

        ]);
        // dd($message);
        $host = (new Host())->put($id, $request->all());
        // $olt = (new Olt)->checkAccess($host->Code)->message;

        return redirect()->route('olt.host.index')->with('notif_success', 'Host has been update successfully!');
    }

    public function destroy($id)
    {
        if(!getAuthMenu('olt.host.index',DELETE)){
          return redirect()->route('noauth');
        }
        $host = (new Host())->delete($id);

        return redirect()->back()->with('notif_success', 'Host has been deleted!');
    }

    public function getData(Request $request)
    {
        $results = (new Host())->getAll($request);
        $paging = paginator($results);
        $list = array();
        $no = intval($request->start);
        foreach ($results->data as $datas) {
            $no++;
            $row = array();
            $datas->DT_RowIndex = $no;
            $list[] = $datas;
        }
        return response()->json([
            "draw" => intval($request->draw),
            "recordsTotal"    => intval($paging->total_records),
            "recordsFiltered" => intval($paging->total_records),
            "data" => $list
        ]);
    }
}
