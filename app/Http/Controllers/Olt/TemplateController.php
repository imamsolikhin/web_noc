<?php

namespace App\Http\Controllers\Olt;

use DataTables;
use Illuminate\Http\Request;
use App\Services\Olt\Template;
use App\Services\Olt\TemplateDetail;
use App\Http\Controllers\Controller;

class TemplateController extends Controller {

    public function index() {

      if(!getAuthMenu('olt.template.index',VIEW)){
        return redirect()->route('noauth');
      }
        return view('olt.template.index');
    }

    public function page($page)
    {
        if ($page == "new") {
            if(!getAuthMenu('olt.template.index',CREATE)){
              return redirect()->route('noauth');
            }
            $datas['template'] = (new Template())->show($page)->data;
            return view('olt.template.form', $datas);
        } else {
            if(!getAuthMenu('olt.template.index',UPDATE)){
              return redirect()->route('noauth');
            }
          $datas['template'] = (new Template())->show($page)->data;
          $datas["template_detail"] = (new TemplateDetail())->getAllActive($datas["template"]->Code)->data;
          return view('olt.template.form', $datas);
      }
  }


    public function store(Request $request) {
        $this->validate($request, [
            'Name' => 'nullable|max:250',
            'VlanDownLink' => 'nullable|max:250',
            'OntLineProfileId' => 'nullable|max:250',
            'OntSrvProfileId' => 'nullable|max:250',
            'NativeVlanEth1' => 'nullable|max:250',
            'NativeVlanEth2' => 'nullable|max:250',
            'NativeVlanEth3' => 'nullable|max:250',
            'NativeVlanEth4' => 'nullable|max:250',
            'VlanAttribut' => 'nullable|max:250',
            'Remark' => 'nullable||max:250'
        ]);

        $template = (new Template)->post($request->all())->data;

        $template_delete = (new TemplateDetail())->delete($template->Code);
        if(isset($request->its_vlan)){
          for ($i=0; $i < count($request->its_vlan); $i++){
              $data_template = [
                'Code' => $template->Code.'-'.str_pad(($i+1), 3, '0', STR_PAD_LEFT),
                'HeaderCode' => $template->Code,
                'GemPort' => $request->its_gemport[$i],
                'Vlan' => $request->its_vlan[$i],
                'UserVlan' => $request->its_uservlan[$i],
                'InnerVlan' => $request->its_innervlan[$i],
                'TagTransform' => $request->its_tagtransform[$i],
                'TrafficTable' => $request->its_traffictable[$i],
                'Remark' => $request->its_remark[$i],
              ];
              $template_detail = (new TemplateDetail())->post($data_template);
          }
        }

        return redirect()->route('olt.template.index')->with('notif_success', 'New Template has been added successfully!');
    }

    public function show($id) {
        $template = (new Template)->show($id)->data;

        return makeResponse(200, 'success', null, $template);
    }

    public function show_header($id)
    {

        $results["template"] = $template = (new Template())->show($id)->data;

        $template_detail = (new TemplateDetail())->getAllActive($id);
        $results["template_detail"] = (isset($template_detail->data)) ? $template_detail->data : null;

        return makeResponse(200, 'success', null, $results);
    }

    public function update($id, Request $request) {
        $this->validate($request, [
            'Name' => 'nullable|max:250',
            'VlanDownLink' => 'nullable|max:250',
            'OntLineProfileId' => 'nullable|max:250',
            'OntSrvProfileId' => 'nullable|max:250',
            'NativeVlanEth1' => 'nullable|max:250',
            'NativeVlanEth2' => 'nullable|max:250',
            'NativeVlanEth3' => 'nullable|max:250',
            'NativeVlanEth4' => 'nullable|max:250',
            'VlanAttribut' => 'nullable|max:250',
            'Remark' => 'nullable||max:250'

        ]);

        $template = (new Template)->put($id, $request->all())->data;

        $template_delete = (new TemplateDetail())->delete($template->Code);
        if(isset($request->its_vlan)){
          for ($i=0; $i < count($request->its_vlan); $i++){
              $data_template = [
                'Code' => $template->Code.'-'.str_pad(($i+1), 3, '0', STR_PAD_LEFT),
                'HeaderCode' => $template->Code,
                'GemPort' => $request->its_gemport[$i],
                'Vlan' => $request->its_vlan[$i],
                'UserVlan' => $request->its_uservlan[$i],
                'InnerVlan' => $request->its_innervlan[$i],
                'TagTransform' => $request->its_tagtransform[$i],
                'TrafficTable' => $request->its_traffictable[$i],
                'Remark' => $request->its_remark[$i],
              ];
              $template_detail = (new TemplateDetail())->post($data_template);
          }
        }

        return redirect()->route('olt.template.index')->with('notif_success', 'Template has been update successfully!');
    }

    public function destroy($id) {
        $template = (new Template)->delete($id);
        $template_delete = (new TemplateDetail())->delete($id);

        return redirect()->back()->with('notif_success', 'Template has been deleted!');
    }

    public function getData(Request $request) {

        $results = (new Template())->getAll($request);
        $paging = paginator($results);
        $list = array();
        $no = intval($request->start);
        foreach ($results->data as $datas) {
            $no++;
            $row = array();
            $datas->DT_RowIndex = $no;
            $list[] = $datas;
        }
        return response()->json([
            "draw" => intval($request->draw),
            "recordsTotal"    => intval($paging->total_records),
            "recordsFiltered" => intval($paging->total_records),
            "data" => $list
        ]);
    }

}
