<?php
namespace App\Http\Controllers\Olt;

use DataTables;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Services\Olt\Isp;
use App\Http\Controllers\Controller;

class IspController extends Controller
{

    public function index()
    {
        if(!getAuthMenu('olt.isp.index',VIEW)){
          return redirect()->route('noauth');
        }
        return view('olt.isp.index');
    }

    public function store(Request $request)
    {
        if(!getAuthMenu('olt.isp.index',CREATE)){
          return redirect()->route('noauth');
        }
        $this->validate($request, [
            'Name' => 'nullable|max:250',
            'ContactPerson' => 'nullable|max:250',
            'Address' => 'nullable|max:250',
            'CityCode' => 'nullable|max:250',
            'Phone1' => 'nullable|max:250',
            'Phone2' => 'nullable|max:250',
            'Fax' => 'nullable|max:250',
            'Email' => 'nullable|max:250'
        ]);

        $isp = (new Isp())->post($request->all());

        return redirect()->route('olt.isp.index')->with('notif_success', 'New Isp has been added successfully!');
    }

    public function show($id)
    {
        $isp = (new Isp())->show($id)->data;

        return makeResponse(200, 'success', null, $isp);
    }

    public function update($id, Request $request)
    {
        if(!getAuthMenu('olt.isp.index',UPDATE)){
          return redirect()->route('noauth');
        }
        $this->validate($request, [
            'Name' => 'nullable|max:250',
            'ContactPerson' => 'nullable|max:250',
            'Address' => 'nullable|max:250',
            'CityCode' => 'nullable|max:250',
            'Phone1' => 'nullable|max:250',
            'Phone2' => 'nullable|max:250',
            'Fax' => 'nullable|max:250',
            'Email' => 'nullable|max:250'
        ]);

        $isp = (new Isp())->put($id, $request->all());

        return redirect()->route('olt.isp.index')->with('notif_success', 'Isp has been update successfully!');
    }

    public function destroy($id)
    {
        if(!getAuthMenu('olt.isp.index',DELETE)){
          return redirect()->route('noauth');
        }
        $isp = (new Isp())->delete($id);

        return redirect()->back()->with('notif_success', 'Isp has been deleted!');
    }

    public function getData(Request $request)
    {
        $results = (new Isp())->getAll($request);
        $paging = paginator($results);
        $list = array();
        $no = intval($request->start);
        foreach ($results->data as $datas) {
            $no++;
            $row = array();
            $datas->DT_RowIndex = $no;
            $list[] = $datas;
        }
        return response()->json([
            "draw" => intval($request->draw),
            "recordsTotal"    => intval($paging->total_records),
            "recordsFiltered" => intval($paging->total_records),
            "data" => $list
        ]);
    }
}
