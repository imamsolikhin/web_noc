<?php
namespace App\Http\Controllers\Olt;

use DataTables;
use Illuminate\Http\Request;
use App\Services\Olt\Brand;
use App\Http\Controllers\Controller;

class BrandController extends Controller
{

    public function index()
    {
        if(!getAuthMenu('olt.brand.index',VIEW)){
          return redirect()->route('noauth');
        }

        return view('olt.brand.index');
    }

    public function store(Request $request)
    {
        if(!getAuthMenu('olt.brand.index',CREATE)){
          return redirect()->route('noauth');
        }

        $this->validate($request, [
            'Name' => 'nullable|max:250',
            'Model' => 'nullable|max:250',
            'Type' => 'nullable|max:250',
            'Version' => 'nullable|max:250',
            'Remark' => 'nullable|max:250'
        ]);

        $device = (new Brand())->post($request->all());

        return redirect()->route('olt.brand.index')->with('notif_success', 'New Brand has been added successfully!');
    }

    public function show($id)
    {

        $device = (new Brand())->show($id)->data;

        return makeResponse(200, 'success', null, $device);
    }

    public function update($id, Request $request)
    {
        if(!getAuthMenu('olt.brand.index',UPDATE)){
          return redirect()->route('noauth');
        }

        $this->validate($request, [
            'Name' => 'nullable|max:250',
            'Model' => 'nullable|max:250',
            'Type' => 'nullable|max:250',
            'Version' => 'nullable|max:250',
            'Remark' => 'nullable|max:250'
        ]);

        $device = (new Brand())->put($id, $request->all());

        return redirect()->route('olt.brand.index')->with('notif_success', 'Brand has been update successfully!');
    }

    public function destroy($id)
    {
        if(!getAuthMenu('olt.brand.index',DELETE)){
          return redirect()->route('noauth');
        }

        $device = (new Brand())->delete($id);

        return redirect()->back()->with('notif_success', 'Brand has been deleted!');
    }

    public function getData(Request $request)
    {
        $results = (new Brand())->getAll($request);
        $paging = paginator($results);
        $list = array();
        $no = intval($request->start);
        foreach ($results->data as $datas) {
            $no++;
            $row = array();
            $datas->DT_RowIndex = $no;
            $list[] = $datas;
        }
        return response()->json([
            "draw" => intval($request->draw),
            "recordsTotal"    => intval($paging->total_records),
            "recordsFiltered" => intval($paging->total_records),
            "data" => $list
        ]);
    }
}
