<?php

namespace App\Http\Controllers\Asset;

use App\Http\Controllers\Controller;

class WarehouseMutationInternalController extends Controller {

    public function index() {
        $page_title = 'WAREHOUSE MUTATION INTERNAL';
        $page_description = 'Asset/Warehouse Mutation Internal';

        $roles = "Test";
        $clientProperties = "Tests";
        $clients = "Test";

        return view('asset.warehouse-mutation-internal.index', compact('page_title', 'page_description', 'roles', 'clientProperties', 'clients'));
    }

}
