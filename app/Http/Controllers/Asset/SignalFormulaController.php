<?php

namespace App\Http\Controllers\Asset;

use App\Http\Controllers\Controller;

class SignalFormulaController extends Controller {

    public function index() {
        $page_title = 'Signal Formula';
        $page_description = 'Asset/Signal Formula';

        $roles = "Test";
        $clientProperties = "Tests";
        $clients = "Test";

        return view('asset.signal-formula.index', compact('page_title', 'page_description', 'roles', 'clientProperties', 'clients'));
    }

}
