<?php

namespace App\Http\Controllers\Telnet;

use DataTables;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;
use App\Services\Olt\Host;
use App\Services\Olt\SignalFormula;
use App\Services\Olt\Ont;
use App\Services\Olt\OntLast;
use App\Services\Olt\DbaProfile;
use App\Services\Olt\DbaProfileLast;
use App\Services\Olt\PortVlan;
use App\Services\Olt\PortVlanLast;
use App\Services\Olt\LineProfile;
use App\Services\Olt\LineProfileLast;
use App\Services\Olt\SrvProfile;
use App\Services\Olt\SrvProfileLast;
use App\Services\Olt\SrvPort;
use App\Services\Olt\SrvPortLast;
use App\Services\Olt\GemProfile;
use App\Services\Olt\GemProfileLast;
use App\Services\Olt\TrafficTable;
use App\Services\Olt\TrafficTableLast;
use App\Services\Olt\InterfaceGpon;
use App\Services\Olt\InterfaceGponLast;
use App\Services\Telnet\Olt;
use App\Services\Olt\ClientsTools;
use App\Services\Olt\OntInfo;
use App\Services\Olt\OntOpticalInfo;
use App\Services\Olt\OntWanInfo;
use App\Services\Olt\OntPortState;
use App\Services\Olt\Clients;
use App\Services\Olt\ClientsVlanService;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class OltConfigController extends Controller {

    public function checkAccess($id) {
        $host = (new Host)->show($id)->data;
        $olt = (new Olt)->checkAccess($host->Code)->message;

        $message = "";
        $message .= "Hostname : ".$host->Hostname;
        $message .= "<br>Ip Address : ".$host->IpAddress;
        $message .= "<br>User Name : ".$host->Username;
        $message .= "<br>Password : ".$host->Password;
        $message .= "<br>Message : ".$olt;

        return makeResponse(200, 'success', null, $message);
    }

    public function checkConfig($id,$config, Request $request) {
        if($config == "ont-autofind" && $request->data){
            $response['ont-autofind'] = (new Ont)->getAllActive($id)->data;
            return makeResponse(200, 'success', null, $response);
        }
        $host = (new Host)->show($id);

        $arr_uri;
        $client = new \GuzzleHttp\Client();
        $response = array();
        $response['host'] = $host;
        if($host) {
          $host = $host->data;
          $url = env('IKB_NET_NOC').'/huawei/host/config';
          $url .= '?auth=all';
          $url .= '&config='.$config;
          $url .= '&hostcode='.$host->Code;
          $url .= '&hostname='.$host->IpAddress;
          $url .= '&username='.$host->Username;
          $url .= '&password='.$host->Password;
          $url .= '&lnprofile='.$host->ProfileId;
          $arr_uri[] = $client->requestAsync('GET', $url)->then(
                        function ($response) {
                            $data = json_decode($response->getBody()->getContents());
                            if($data->response){
                              $no = 1;
                              $results = $data->response;
                              foreach ($results as $val) {
                                if($val['value']){
                                  foreach ($val['value'] as $rs) {
                                    if ($rs[0] == 'current-config'){
                                      $response['current-config'] = $rs[2];
                                    }
                                    if ($rs[0] == 'traffic-table'){
                                      $response['traffic-table-last'] = (new TrafficTable)->getAllActive($rs[1])->data;
                                      $delete_data = (new TrafficTable)->delete_host($rs[1]);
                                      if(!empty($rs[2][0])){
                                        $post_data = (new TrafficTable)->post_list($rs[2]);
                                        $response['traffic-table'] = (new TrafficTable)->getAllActive($rs[1])->data;
                                        $delete_data_last = (new TrafficTableLast)->delete_host($rs[1]);
                                        $post_data_last = (new TrafficTableLast)->post_list($response['traffic-table-last']);
                                        $message .= $no.'.) Traffic Table Updated ('.count($response['traffic-table']).')<br class="pt-0 pb-0"/>';
                                        $no = $no+1;
                                      }
                                    }
                                    if ($rs[0] == 'line-profile'){
                                      $response['line-profile'] = $rs[2];
                                      $response['line-profile-last'] = (new LineProfile)->getAllActive($rs[1])->data;
                                      if(!empty($rs[2][0])){
                                        $delete_data = (new LineProfile)->delete_host($rs[1]);
                                        $post_data = (new LineProfile)->post_list($rs[2]);
                                        $response['line-profile'] = (new LineProfile)->getAllActive($rs[1])->data;
                                        $delete_data_last = (new LineProfileLast)->delete_host($rs[1]);
                                        $post_data_last = (new LineProfileLast)->post_list($response['line-profile-last']);
                                        $message .= $no.'.) Line Profile Updated ('.count($response['line-profile']).')<br class="pt-0 pb-0"/>';
                                        $no = $no+1;
                                      }
                                    }
                                    if ($rs[0] == 'srv-profile'){
                                      $response['srv-profile-last'] = (new SrvProfile)->getAllActive($rs[1])->data;
                                      $delete_data = (new SrvProfile)->delete_host($rs[1]);
                                      if(!empty($rs[2][0])){
                                        $post_data = (new SrvProfile)->post_list($rs[2]);
                                        $response['srv-profile'] = (new SrvProfile)->getAllActive($rs[1])->data;
                                        $delete_data_last = (new SrvProfileLast)->delete_host($rs[1]);
                                        $post_data_last = (new SrvProfileLast)->post_list($response['srv-profile-last']);
                                        $message .= $no.'.) Service Profile Updated ('.count($response['srv-profile']).')<br class="pt-0 pb-0"/>';
                                        $no = $no+1;
                                      }
                                    }
                                    if ($rs[0] == 'port-vlan'){
                                      $response['port-vlan-last'] = (new PortVlan)->getAllActive($rs[1])->data;
                                      $delete_data = (new PortVlan)->delete_host($rs[1]);
                                      if(!empty($rs[2][0])){
                                        $post_data = (new PortVlan)->post_list($rs[2]);
                                        $response['port-vlan'] = (new PortVlan)->getAllActive($rs[1])->data;
                                        $delete_data_last = (new PortVlanLast)->delete_host($rs[1]);
                                        $post_data_last = (new PortVlanLast)->post_list($response['port-vlan-last']);
                                        $message .= $no.'.) Port Vlan Updated ('.count($response['port-vlan']).')<br class="pt-0 pb-0"/>';
                                        $no = $no+1;
                                      }
                                    }
                                    if ($rs[0] == 'dba-profile'){
                                      $response['dba-profile-last'] = (new DbaProfile)->getAllActive($rs[1])->data;
                                      $delete_data = (new DbaProfile)->delete_host($rs[1]);
                                      if(!empty($rs[2][0])){
                                        $post_data = (new DbaProfile)->post_list($rs[2]);
                                        $response['dba-profile'] = (new DbaProfile)->getAllActive($rs[1])->data;
                                        $delete_data_last = (new DbaProfileLast)->delete_host($rs[1]);
                                        $post_data_last = (new DbaProfileLast)->post_list($response['dba-profile-last']);
                                        $message .= $no.'.) DBA Profile Updated ('.count($response['dba-profile']).')<br class="pt-0 pb-0"/>';
                                        $no = $no+1;
                                      }
                                    }
                                    if ($rs[0] == 'service-port'){
                                      $response['service-port-last'] = (new SrvPort)->getAllActive($rs[1])->data;
                                      $delete_data = (new SrvPort)->delete_host($rs[1]);
                                      if(!empty($rs[2][0])){
                                        $post_data = (new SrvPort)->post_list($rs[2]);
                                        $response['service-port'] = (new SrvPort)->getAllActive($rs[1])->data;
                                        $delete_data_last = (new SrvPortLast)->delete_host($rs[1]);
                                        $post_data_last = (new SrvPortLast)->post_list($response['service-port-last']);
                                        $message .= $no.'.) Service Port Updated ('.count($response['service-port']).')<br class="pt-0 pb-0"/>';
                                      }
                                    }
                                    if ($rs[0] == 'gem-port'){
                                      $response['gem-port-last'] = (new GemProfile)->getAllActive($rs[1])->data;
                                      $delete_data = (new GemProfile)->delete_host($rs[1]);
                                      if(!empty($rs[2][0])){
                                        $post_data = (new GemProfile)->post_list($rs[2]);
                                        $response['gem-port'] = (new GemProfile)->getAllActive($rs[1])->data;
                                        $delete_data_last = (new GemProfileLast)->delete_host($rs[1]);
                                        $post_data_last = (new GemProfileLast)->post_list($response['gem-port-last']);
                                        $message .= $no.'.) Gem Port Updated ('.count($response['gem-port']).')<br class="pt-0 pb-0"/>';
                                        $no = $no+1;
                                      }
                                    }
                                    if ($rs[0] == 'ont-info'){
                                      $response['ont-info-last'] = (new InterfaceGpon)->getAllActive($rs[1])->data;
                                      $delete_data = (new InterfaceGpon)->delete_host($rs[1]);
                                      if(!empty($rs[2][0])){
                                        $post_data = (new InterfaceGpon)->post_list($rs[2]);
                                        $response['ont-info'] = (new InterfaceGpon)->getAllActive($rs[1])->data;
                                        $delete_data_last = (new InterfaceGponLast)->delete_host($rs[1]);
                                        $post_data_last = (new InterfaceGponLast)->post_list($response['ont-info-last']);
                                        $message .= $no.'.) Register ONT Updated ('.count($response['ont-info']).')<br class="pt-0 pb-0"/>';
                                        $no = $no+1;
                                      }
                                    }
                                    if ($rs[0] == 'ont-autofind'){
                                      $response['ont-autofind-last'] = (new Ont)->getAllActive($rs[1])->data;
                                      $delete_data = (new Ont)->delete_host($rs[1]);
                                      if(!empty($rs[2][0])){
                                        $post_data = (new Ont)->post_list($rs[2]);
                                        $response['ont-autofind'] = (new Ont)->getAllActive($rs[1])->data;
                                        $delete_data_last = (new OntLast)->delete_host($rs[1]);
                                        $post_data_last = (new OntLast)->post_list($response['ont-autofind-last']);
                                        $message .= $no.'.) ONT IDLE Updated ('.count($response['ont-autofind']).')<br class="pt-0 pb-0"/>';
                                        $no = $no+1;
                                      }
                                    }
                                  }
                                }
                              }
                              return $data->response;
                            }
                        }, function ($exception) {
                            return $exception->getMessage();
                        }
                      );
        }
        $promises = $arr_uri;
        $results = \GuzzleHttp\Promise\settle($promises)->wait();

        $message = '<h5 class="pt-0 pb-0">List Update:</h5>';
        $message .= '<h5 class="pt-0 pb-0">'.$host->Hostname.' : '.$host->IpAddress.'</h5>';

        return makeResponse(200, 'success', $message, $response);
    }

    public function autoFindSn($id) {
        $host = (new Host)->show($id)->data;
        $olt = (new Olt)->checkAccess($host->Code)->message;

        $message = "";
        $message .= "Hostname : ".$host->Hostname;
        $message .= "<br>Ip Address : ".$host->IpAddress;
        $message .= "<br>User Name : ".$host->Username;
        $message .= "<br>Password : ".$host->Password;
        $message .= "<br>Message : ".$olt;

        return makeResponse(200, 'success', null, $message);
    }

    public function autoFindAll() {
        $data = array();
        $host_list = (new Host())->getAllActive();
        $host_list = (isset($host_list->data)) ? $host_list->data : null;

        if ($host_list){
            $arr_uri;
            $client = new \GuzzleHttp\Client();
            $response = array();
            $response['ont-autofind'] = [];
            foreach ($host_list as $host) {
              $url = env('IKB_NET_NOC').'/huawei/host/config';
              $url .= '?auth=all';
              $url .= '&config=ont-autofind';
              $url .= '&hostcode='.$host->Code;
              $url .= '&hostname='.$host->IpAddress;
              $url .= '&username='.$host->Username;
              $url .= '&password='.$host->Password;
              $url .= '&lnprofile='.$host->ProfileId;
              $arr_uri[] = $client->requestAsync('GET', $url)->then(
                            function ($response) {
                                $data = json_decode($response->getBody()->getContents());
                                if($data->response){
                                  $results = $data->response;
                                  foreach ($results as $val) {
                                    if (!empty($val['value'][0][2])) {
                                      foreach ($val['value'][0][2] as $val) {
                                        array_push($response['ont-autofind'],$val);
                                      }
                                    }
                                  }
                                  return $data->response;
                                }
                            }, function ($exception) {
                                return $exception->getMessage();
                            }
                          );
            }
            $promises = $arr_uri;
            $results = \GuzzleHttp\Promise\settle($promises)->wait();
        }

        $list_ont = (new Ont)->delete_all();
        $post_data = (new Ont)->post_list($response['ont-autofind']);
        $response = (new Ont)->getAllActive(null)->data;
        return makeResponse(200, 'success', null, $response);
    }

    public function autoFindHost($id, Request $request) {
        $host = (new Host)->show($id)->data;
        $arr_uri;
        $client = new \GuzzleHttp\Client();
        $url = env('IKB_NET_NOC').'/huawei/host/config';
        $url .= '?auth=all';
        $url .= '&config=ont-autofind';
        $url .= '&hostcode='.$host->Code;
        $url .= '&hostname='.$host->IpAddress;
        $url .= '&username='.$host->Username;
        $url .= '&password='.$host->Password;
        $url .= '&lnprofile='.$host->ProfileId;
        $response = array();
        $arr_uri[] = $client->requestAsync('GET', $url)->then(
                      function ($response) {
                          $data = json_decode($response->getBody()->getContents());
                          if($data->response){
                            $results = $data->response;
                            foreach ($results as $val) {
                              if($val['value'] != null){
                                foreach ($val['value'] as $rs) {
                                  if ($rs[0] == 'ont-autofind'){
                                    $response['ont-autofind'] = $rs[2];
                                    $list_ont = (new Ont)->delete_host($rs[1]);
                                    $post_data = (new Ont)->post_list($response['ont-autofind']);
                                  }
                                }
                              }
                            }
                            return $data->response;
                          }
                      }, function ($exception) {
                          return $exception->getMessage();
                      }
                    );
        $promises = $arr_uri;
        $results = \GuzzleHttp\Promise\settle($promises)->wait();
        $response = (new Ont)->getAllActive($id)->data;
        return makeResponse(200, 'success', null, $response);
    }

    public function clientstools($id,$config,$setting) {
      $data["tools"] = (new ClientsTools)->show($id)->data;
      $data['clients'] = (new Clients)->show($id)->data;
      $data['host'] = (new Host)->show($data['clients']->HostCode)->data;

      if(isset($data["tools"]->Code) or is_null($data["tools"])) {
        $datas = (new ClientsTools())->post(array('Code'=>$data['clients']->Code, 'ClientsCode'=>$data['clients']->Code, 'ActiveStatus'=>1));
        $data["tools"] = (new ClientsTools)->show($id)->data;
      }
      // dd($data["tools"]);

      if($setting and $data['clients'] and $data['host']){
          $clientstools = (new Olt)->clientstools($data["host"],$data['clients'],$config);
          $data["tools"]->Code = $data['clients']->Code;
          $data["tools"]->ClientsCode = $data['clients']->Code;
          // dd($data["tools"]);
          if($config == "general-info" and is_array($clientstools->response)){
            (new OntInfo)->delete($data["clients"]->Code, $data["clients"]->Code);
            (new OntInfo)->post($data["clients"], $clientstools->response[1][1]);
            $data["tools"]->OntInfo = implode("\n",$clientstools->response[0][1]);

            (new OntWanInfo)->delete($data["clients"]->Code, $data["clients"]->Code);
            (new OntWanInfo)->post($data["clients"], $clientstools->response[3][1]);
            $data["tools"]->WanInfo = implode("\n",$clientstools->response[2][1]);

            (new OntPortState)->delete($data["clients"]->Code, $data["clients"]->Code);
            (new OntPortState)->post($data["clients"], $clientstools->response[5][1]);
            $data["tools"]->PortState = implode("\n",$clientstools->response[4][1]);

            (new OntOpticalInfo)->delete($data["clients"]->Code, $data["clients"]->Code);
            (new OntOpticalInfo)->post($data["clients"], $clientstools->response[7][1]);
            $data["tools"]->Opticalinfo = implode("\n",$clientstools->response[6][1]);

            (new ClientsTools())->put($data["tools"]->Code, $data["tools"]);
          }
          if($config == "ont-version"){
            $data["tools"]->Version = implode("\n",$clientstools->response[0][1]);
          }
          if($config == "ont-info"){
            $data["tools"]->OntInfo = implode("\n",$clientstools->response[0][1]);
          }
          if($config == "wan-info"){
            $data["tools"]->WanInfo = implode("\n",$clientstools->response[0][1]);
          }
          if($config == "optical-info"){
            $data["tools"]->Opticalinfo = implode("\n",$clientstools->response[0][1]);
          }
          if($config == "register-info"){
            $data["tools"]->Registerinfo = implode("\n",$clientstools->response[0][1]);
          }
          if($config == "mac-address"){
            $data["tools"]->MacAddress = implode("\n",$clientstools->response[0][1]);
          }
          if($config == "port-state"){
            $data["tools"]->PortState = implode("\n",$clientstools->response[0][1]);
          }
          if($config == "port-attribute"){
            $data["tools"]->PortAttribute = implode("\n",$clientstools->response[0][1]);
          }
          if($config == "fec-crc-error"){
            $data["tools"]->FecCrcError = implode("\n",$clientstools->response[0][1]);
          }

          if ($config != "general-info"){
            $datas = (new ClientsTools())->put($data["tools"]->Code, $data["tools"]);
          }
      }

      if($config == "general-info"){
        $resp = (new OntInfo)->show($id);
        $data["general-info"][0]= (isset($resp->data)) ? $resp->data:null;
        $resp = (new OntOpticalInfo)->show($id);
        $data["general-info"][1]= (isset($resp->data)) ? $resp->data:null;
        $resp = (new OntWanInfo)->show($id);
        $data["general-info"][2]= (isset($resp->data)) ? $resp->data:null;
        $resp = (new OntPortState)->show($id);
        $data["general-info"][3]= (isset($resp->data)) ? $resp->data:null;
      }else{
        $data["tools"] = (new ClientsTools)->show($id)->data;
      }
      return makeResponse(200, 'success', null, $data);
    }

    public function activation($id, $setting, $script, Request $request){
        $data['url'] = "";
        $clients = (new Clients())->show($id)->data;
        $config = (new Clients())->show_setting($id)->data;
        $config->clients->ClientStatus = $request->ClientStatus;
        $data['clients'] = $config->clients;
        $data['service'] = $config->service;
        $data['host'] = (new Host)->show($data['clients']->HostCode)->data;
        $data['url'] .= 'auth='. $auth = ($script)? "run":"script";
        if($clients->OntId == null){
          if(!$config->vlan){
            $data['url'] .= '&config=setting-vlan';
          }
          if($config->srvProfile){
            $data['url'] .= '&config=setting-service-profile';
          }
          // if(!$config->lnprofile){
            $data['url'] .= '&config=setting-line-profile';
          // }
          if(!$config->InterfaceGpon){
            $data['url'] .= '&config=setting-ont';
          }
          if(!$config->srvPort){
            $data['url'] .= '&config=setting-service-port';
          }
        }else{
          $data['url'] .= '&config=setting-clients';
          $data['url'] .= '&config=setting-traffic';
        }
        $data['url'] .= '&hostcode='.$data['host']->Code;
        $data['url'] .= '&hostname='.$data['host']->IpAddress;
        $data['url'] .= '&username='.$data['host']->Username;
        $data['url'] .= '&password='.$data['host']->Password;
        $data['url'] .= '&bframe='.$data['host']->FrameId;
        $data['url'] .= '&bslot='.$data['host']->SlotId;
        $data['url'] .= '&bport='.$data['host']->PortId;

        $data['url'] .= '&desc='.$data['clients']->CustomerName;
        $data['url'] .= '&status='.$data['clients']->ClientStatus;
        $data['url'] .= '&frame='.$data['clients']->FrameId;
        $data['url'] .= '&slot='.$data['clients']->SlotId;
        $data['url'] .= '&port='.$data['clients']->PortId;
        $data['url'] .= '&ont='.$data['clients']->OntId;
        $data['url'] .= '&sn='.$data['clients']->OntSn;
        $data['url'] .= '&srvprofile='.$data['clients']->OntSrvProfileId;
        $data['url'] .= '&lnprofile='.$data['clients']->OntLineProfileId;
        $data['url'] .= '&attribut='.$data['clients']->VlanAttribut;
        $data['url'] .= '&vlan='.$data['clients']->VlanId;
        $data['url'] .= '&vlan1='.$data['clients']->NativeVlanEth1;
        $data['url'] .= '&vlan2='.$data['clients']->NativeVlanEth2;
        $data['url'] .= '&vlan3='.$data['clients']->NativeVlanEth3;
        $data['url'] .= '&vlan4='.$data['clients']->NativeVlanEth4;
        // dd($config);

        foreach($config->service as $srv){
          $data['url'] .= '&s_status='.$srv->ActiveStatus;
          $data['url'] .= '&srvid='.$srv->ServicePortId;
          $data['url'] .= '&gem='.$srv->GemPort;
          $data['url'] .= '&svlan='.$srv->Vlan;
          $data['url'] .= '&uservlan='.$srv->UserVlan;
          $data['url'] .= '&innervlan='.$srv->InnerVlan;
          $data['url'] .= '&tag='.$srv->TagTransform;
          $data['url'] .= '&traffic='.$srv->TrafficTable;
          $data['url'] .= '&eth='.$srv->ServicePortId;
        }
        $data['script'] = (new Olt)->activation($data['url'])->response;
        if($script){
            $clients_delete = (new ClientsVlanService())->delete($request->Code);
            $no =1;
            foreach($config->service as $srv){
              $no++;
                  $data_vlan = [
                    'Code' => $clients->Code."-".$clients->OntSn."-".$no,
                    'CustomerCode' => $clients->CustomerCode,
                    'ServicePortId' =>$rs=($request->ClientStatus =='Closed')? null:$srv->ServicePortId,
                    'Vlan' => $srv->Vlan,
                    'GemPort' =>$rs=($request->ClientStatus =='Closed')? null:$srv->GemPort,
                    'InnerVlan' => $srv->InnerVlan,
                    'UserVlan' => $srv->UserVlan,
                    'TagTransform' => $srv->TagTransform,
                    'TrafficTable' => $srv->TrafficTable,
                  ];
                  $clients_vlans = (new ClientsVlanService())->post($data_vlan);
              }
              $services_list = (new ClientsVlanService())->getAllActive($clients->CustomerCode);
              $data_clients['Code'] = $config->clients->Code;
              $data_clients['OntId'] = $rs=($request->ClientStatus =='Closed')? null:$config->clients->OntId;
              $data_clients['OntSrvProfileId'] = $config->clients->OntSrvProfileId;
              $data_clients['OntLineProfileId'] = $config->clients->OntLineProfileId;
              $data_clients['FrameId'] = $config->clients->FrameId;
              $data_clients['SlotId'] = $config->clients->SlotId;
              $data_clients['PortId'] = $config->clients->PortId;
              $data_clients['NativeVlanEth1'] = $config->clients->NativeVlanEth1;
              $data_clients['NativeVlanEth2'] = $config->clients->NativeVlanEth2;
              $data_clients['NativeVlanEth3'] = $config->clients->NativeVlanEth3;
              $data_clients['NativeVlanEth4'] = $config->clients->NativeVlanEth4;
              $data_clients['VlanFrameId'] = $config->clients->VlanFrameId;
              $data_clients['VlanSlotId'] = $config->clients->VlanSlotId;
              $data_clients['VlanPortId'] = $config->clients->VlanPortId;
              $data_clients['ClientStatus'] = $request->ClientStatus;
              $data_clients['Response'] = implode("\n",$data['script']);
              $clients = (new Clients())->put($id, $data_clients)->data;
              $curl = curl_init();
              if($request->ClientStatus == "Postpone"){
                curl_setopt($curl, CURLOPT_URL, env('CRON_HOST')."/api/config-host/".$data['host']->Code."?token=p2lbgWkFrykA4QyUmpHihzmc5BNzIABq");
                // $data = static::checkConfig($data['host']->Code,"ont-info");
              }elseif ($request->ClientStatus == "Closed") {
                curl_setopt($curl, CURLOPT_URL, env('CRON_HOST')."/api/config-host/".$data['host']->Code."?token=p2lbgWkFrykA4QyUmpHihzmc5BNzIABq");
                // $data = static::checkConfig($data['host']->Code,"service-port&config=ont-info&config=ont-autofind");
              }else{
                curl_setopt($curl, CURLOPT_URL, env('CRON_HOST')."/api/config-host/".$data['host']->Code."?token=p2lbgWkFrykA4QyUmpHihzmc5BNzIABq");
                // $data = static::checkConfig($data['host']->Code,"port-vlan&config=dba-profile&config=service-port&config=gem-port&config=ont-info");
              }
              curl_setopt($curl, CURLOPT_RETURNTRANSFER, 0);
              curl_setopt($curl, CURLOPT_TIMEOUT_MS, 1);
              curl_exec($curl);
              $info = curl_getinfo($curl);
              curl_close($curl);
            return redirect()->route('olt.clients.index')->with('notif_success', 'Clients has been update successfully!');
        }
          return makeResponse(200, 'success', null, $data);
        // }

        // $olt = (new Olt)->Activation($clients->Code, $request->ClientStatus);
        // $olt = (new Olt)->checkConfig($clients->HostCode,'ont-info');
        // if($request->ClientStatus != 'Postpone'){
        //   $client = new \GuzzleHttp\Client();
        //   $client->getAsync(env('IKB_TLN_NOC').'/checkhost?code='.$clients->HostCode.'&config=ont-autofind')->then();
        // }
        // return redirect()->route('olt.clients.index')->with('notif_success', 'Clients has been update successfully!');
    }

    public function modifysn($id, $sn, $script, Request $request){
          $data['url'] = "";
          $clients = (new Clients())->show($id)->data;
          $ont = (new Ont())->show($sn)->data;
          // dd($ont);
          $sn = ($ont)? $ont->Sn:"Chose ONT";
          $data['clients'] = $clients;
          $data['ont'] = $ont;
          $data['host'] = (new Host)->show($data['clients']->HostCode)->data;
          $data['url'] .= 'auth='. $auth = ($script)? "run":"script";
          $data['url'] .= '&config=setting-clients';
          $data['url'] .= '&hostcode='.$data['host']->Code;
          $data['url'] .= '&hostname='.$data['host']->IpAddress;
          $data['url'] .= '&username='.$data['host']->Username;
          $data['url'] .= '&password='.$data['host']->Password;
          $data['url'] .= '&bframe='.$data['host']->FrameId;
          $data['url'] .= '&bslot='.$data['host']->SlotId;
          $data['url'] .= '&bport='.$data['host']->PortId;

          $data['url'] .= '&desc='.$data['clients']->CustomerName;
          $data['url'] .= '&status=Modify';
          $data['url'] .= '&frame='.$data['clients']->FrameId;
          $data['url'] .= '&slot='.$data['clients']->SlotId;
          $data['url'] .= '&port='.$data['clients']->PortId;
          $data['url'] .= '&ont='.$data['clients']->OntId;
          $data['url'] .= '&sn='.$sn;
          $data['script'] = (new Olt)->activation($data['url'])->response;
          if($script){
              $data_clients['Code'] = $clients->Code;
              $data_clients['OntId'] = $clients->OntId;
              $data_clients['Sn'] = $sn;
              $data_clients['OntSrvProfileId'] = $clients->OntSrvProfileId;
              $data_clients['OntLineProfileId'] = $clients->OntLineProfileId;
              $data_clients['FrameId'] = $clients->FrameId;
              $data_clients['SlotId'] = $clients->SlotId;
              $data_clients['PortId'] = $clients->PortId;
              $data_clients['NativeVlanEth1'] = $clients->NativeVlanEth1;
              $data_clients['NativeVlanEth2'] = $clients->NativeVlanEth2;
              $data_clients['NativeVlanEth3'] = $clients->NativeVlanEth3;
              $data_clients['NativeVlanEth4'] = $clients->NativeVlanEth4;
              $data_clients['VlanFrameId'] = $clients->VlanFrameId;
              $data_clients['VlanSlotId'] = $clients->VlanSlotId;
              $data_clients['VlanPortId'] = $clients->VlanPortId;
              $data_clients['Response'] = implode("\n",$data['script']);
              $clients = (new Clients())->put($id, $data_clients)->data;
              $data = static::checkConfig($data['host']->Code,"ont-info&config=ont-autofind");

              return redirect()->route('olt.clients.index')->with('notif_success', 'Clients has been update successfully!');
            }
          return makeResponse(200, 'success', null, $data);
    }

    public function modifytraffic($id, $new_traffic, $script, Request $request){

      $data['url'] = "";
      $srv = (new ClientsVlanService())->show($id)->data;
      $clients = (new Clients())->show($srv->CustomerCode)->data;

      $data['clients'] = $clients;
      $data['service'] = $srv;
      $data['host'] = (new Host)->show($data['clients']->HostCode)->data;
      $data['url'] .= 'auth='. $auth = ($script)? "run":"script";
      $data['url'] .= '&config=setting-traffic';
      $data['url'] .= '&config=setting-traffic-new';
      $data['url'] .= '&hostcode='.$data['host']->Code;
      $data['url'] .= '&hostname='.$data['host']->IpAddress;
      $data['url'] .= '&username='.$data['host']->Username;
      $data['url'] .= '&password='.$data['host']->Password;
      $data['url'] .= '&status=Modify';
      $data['url'] .= '&srvid='.$srv->ServicePortId;
      $data['url'] .= '&traffic_new='.$new_traffic;
      $data['url'] .= '&traffic_old='.$srv->TrafficTable;

      $data['script'] = (new Olt)->activation($data['url'])->response;
      if($script){
        $data_clients = [
          'Code' => $srv->Code,
          'CustomerCode' => $clients->CustomerCode,
          'ServicePortId' => $srv->ServicePortId,
          'Vlan' => $srv->Vlan,
          'GemPort' => $srv->GemPort,
          'InnerVlan' => $srv->InnerVlan,
          'UserVlan' => $srv->UserVlan,
          'TagTransform' => $srv->TagTransform,
          'TrafficTable' => $new_traffic,
        ];
        $clients_service = (new ClientsVlanService())->put($srv->Code, $data_clients)->data;

        $dt_clients['Code'] = $clients->Code;
        $dt_clients['OntId'] = $clients->OntId;
        $dt_clients['OntSrvProfileId'] = $clients->OntSrvProfileId;
        $dt_clients['OntLineProfileId'] = $clients->OntLineProfileId;
        $dt_clients['FrameId'] = $clients->FrameId;
        $dt_clients['SlotId'] = $clients->SlotId;
        $dt_clients['PortId'] = $clients->PortId;
        $dt_clients['NativeVlanEth1'] = $clients->NativeVlanEth1;
        $dt_clients['NativeVlanEth2'] = $clients->NativeVlanEth2;
        $dt_clients['NativeVlanEth3'] = $clients->NativeVlanEth3;
        $dt_clients['NativeVlanEth4'] = $clients->NativeVlanEth4;
        $dt_clients['VlanFrameId'] = $clients->VlanFrameId;
        $dt_clients['VlanSlotId'] = $clients->VlanSlotId;
        $dt_clients['VlanPortId'] = $clients->VlanPortId;
        $dt_clients['Response'] = implode("\n",$data['script']);
        $clients = (new Clients())->put($srv->CustomerCode, $dt_clients)->data;
        $data = static::checkConfig($data['host']->Code,"port-vlan&config=service-port");
        return redirect()->route('olt.clients.index')->with('notif_success', 'Traffic has been update successfully!');
      }
      return makeResponse(200, 'success', null, $data);

    }

    public function checkPowerSn($hostcode,$ontsn) {
        $host   = null;
        $frame  = null;
        $slot   = null;
        $port   = null;
        $ont    = null;
        $sn     = null;
        $list_signal = (new SignalFormula)->getAllActive()->data;
        if(!$list_signal){
            return makeResponse(200, 'success', null, "Signal formula Not found!!, Please check data formula!!");
        }
        $data = (new Ont)->show_sn($hostcode, $ontsn)->data;
        if(!$data){
            return makeResponse(200, 'success', null, "Ont Not found!!, Please check once again!!");
        }
        $host = (new Host)->show($hostcode)->data;
        $config = 'check-power';
        $frame = $data->FrameId;
        $slot = $data->SlotId;
        $port = $data->PortId;
        $sn = $data->Sn;
        $ont = $data->OntId;
        if($data->OntId){
          $ont = $data->OntId;
        }
        $clientstools = (new Olt)->checkPowerSn($host,$frame,$slot,$port,$ont,$sn,$config);

        if (is_array($clientstools->response) AND !empty($clientstools->response[0][1])){
          $rs = $clientstools->response[0][1];
          $tx_power   = $rs->tx_power;
          $rx_power   = $rs->rx_power;
          $ont_dist   = ($rs->distance == "-")? (float)0:$rs->distance;
          $ont_range  = $rs->range;
          $ont_hostname  = $host->Hostname;
          $ont_ipaddress  = $host->IpAddress;
          $formatcopy  = "";
          $formatcopy .= "<br>";
          $formatcopy .= " HOST  : ".$ont_hostname."<br>";
          $formatcopy .= " IP        : ".$ont_ipaddress."<br>";
          $formatcopy .= " SN      : ".$sn."<br>";
          $id_copy = "'format_copy_replace'";

          $b = "<br>";
          $message = '<div class="col-12 row">
                        <h3 class="card-title font-weight-bolder text-dark col-8 mt-0 pt-0 mb-0 pb-0">SN: '.$sn.'<br>'.$ont_hostname.'</h3>
                        <div class="card-title font-weight-bolder text-dark col-4 mb-0 pb-2">
                          <h4 class="card-title font-weight-bolder text-dark col-12 mt-0 pt-0 mb-0 pb-0">'.$ont_ipaddress.'<br>
                            <button class="btn font-weight-bold btn-lg btn-outline-danger" onclick="copyToClipboard('.$id_copy.')">Copy Format</button>
                          </h4>
                        </div>
                    </div>
                    ';
          foreach($list_signal as $signal){
            $tx_power   = ($signal->TxPower == "" || $signal->TxPower <= 0) ? $tx_power:$signal->TxPower;
            $formula    = $signal->Formula;
            $distance   = $ont_dist/$signal->Distance;
            $losdb      = $distance*$signal->LossDb;
            $losfixtop  = $tx_power-($losdb*$signal->LossFixTop+17);
            $losfixbot  = $tx_power-($losdb*$signal->LossFixBot+17);
            $id_copy = "'copy_".$signal->Code."'";

            $ideal = "Not Ditect";
            $ideal_cp = "Not Ditect";
            if($rx_power < $losfixtop && $rx_power > $losfixbot){
                $ideal = '<span class="col-4 mb-0 pb-0 label label-xl label-rounded label-warning"><h3 class="font-weight-bolder text-white">Good</h3></span>';
                $ideal_cp = "Good";
            }else if($rx_power < $losfixbot){
                $ideal = '<span class="col-4 mb-0 pb-0 label label-xl label-rounded label-danger"><h3 class="font-weight-bolder text-white">Bad</h3></span>';
                $ideal_cp = "Bad";
            }else if($rx_power > $losfixtop){
                $ideal = '<span class="col-4 mb-0 pb-0 label label-xl label-rounded label-success"><h3 class="font-weight-bolder text-white">Very Good</h3></span>';
                $ideal_cp = "Very Good";
            }

            $formatcopy .= "=============================<br>";
            $formatcopy .= ' FORMULA       : '.$formula.' ['.$ideal_cp.']<br>';
            $formatcopy .= ' Rx Power         : '.$rx_power.' (dBm)<br>';
            $formatcopy .= ' Ont Distance  : '.$ont_dist.' (m)<br>';
            $formatcopy .= ' Ideal Top         : '.$losfixtop.' (dBm)<br>';
            $formatcopy .= ' Ideal Bot         : '.$losfixbot.' (dBm)<br>';

            $message .= '
              <div class="card card-stretch pt-0 pb-0">
    						<!--begin::Header-->
    						<div class="card-header border-0 pt-2 pb-0">
                <div class="card-toolbar card-body d-flex pt-0  row mb-0 pb-0">
  								<h3 class="card-title font-weight-bolder text-dark col-4 mb-0 pb-0">'.$formula.'</h3>
  								'.$ideal.'
                </div>
    						<!--end::Header-->
    						<!--begin::Body-->
    						<div class="card-body d-flex flex-column pt-0 pb-0">
    							<!--begin::Items-->
    							<div class="mt-2 mb-5 pt-0 pb-0 mb-1">
    								<div class="row row-paddingless mb-2">
    									<!--begin::Item-->
    									<div class="col">
    										<div class="d-flex align-items-center mr-2 pt-0 pb-0 mb-1">
    											<!--begin::Symbol-->
    											<div class="symbol symbol-25 symbol-light-info mr-4 flex-shrink-0">
    												<div class="symbol-label">
    													<span class="svg-icon svg-icon-lg svg-icon-info">
    														<!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Shopping/Cart3.svg-->
    														<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                  <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                      <rect x="0" y="0" width="24" height="24"/>
                                      <path d="M13.5,21 L13.5,18 C13.5,17.4477153 13.0522847,17 12.5,17 L11.5,17 C10.9477153,17 10.5,17.4477153 10.5,18 L10.5,21 L5,21 L5,4 C5,2.8954305 5.8954305,2 7,2 L17,2 C18.1045695,2 19,2.8954305 19,4 L19,21 L13.5,21 Z M9,4 C8.44771525,4 8,4.44771525 8,5 L8,6 C8,6.55228475 8.44771525,7 9,7 L10,7 C10.5522847,7 11,6.55228475 11,6 L11,5 C11,4.44771525 10.5522847,4 10,4 L9,4 Z M14,4 C13.4477153,4 13,4.44771525 13,5 L13,6 C13,6.55228475 13.4477153,7 14,7 L15,7 C15.5522847,7 16,6.55228475 16,6 L16,5 C16,4.44771525 15.5522847,4 15,4 L14,4 Z M9,8 C8.44771525,8 8,8.44771525 8,9 L8,10 C8,10.5522847 8.44771525,11 9,11 L10,11 C10.5522847,11 11,10.5522847 11,10 L11,9 C11,8.44771525 10.5522847,8 10,8 L9,8 Z M9,12 C8.44771525,12 8,12.4477153 8,13 L8,14 C8,14.5522847 8.44771525,15 9,15 L10,15 C10.5522847,15 11,14.5522847 11,14 L11,13 C11,12.4477153 10.5522847,12 10,12 L9,12 Z M14,12 C13.4477153,12 13,12.4477153 13,13 L13,14 C13,14.5522847 13.4477153,15 14,15 L15,15 C15.5522847,15 16,14.5522847 16,14 L16,13 C16,12.4477153 15.5522847,12 15,12 L14,12 Z" fill="#000000"/>
                                      <rect fill="#FFFFFF" x="13" y="8" width="3" height="3" rx="1"/>
                                      <path d="M4,21 L20,21 C20.5522847,21 21,21.4477153 21,22 L21,22.4 C21,22.7313708 20.7313708,23 20.4,23 L3.6,23 C3.26862915,23 3,22.7313708 3,22.4 L3,22 C3,21.4477153 3.44771525,21 4,21 Z" fill="#000000" opacity="0.3"/>
                                  </g>
    														</svg>
    														<!--end::Svg Icon-->
    													</span>
    												</div>
    											</div>
    											<!--end::Symbol-->
    											<!--begin::Title-->
    											<div>
    												<div class="font-size-h4 text-dark-75 font-weight-bolder">'.$rx_power.' (dBm)</div>
    												<div class="font-size-sm text-muted font-weight-bold mt-1">Rx Power</div>
    											</div>
    											<!--end::Title-->
    										</div>
    									</div>
    									<!--end::Item-->
    									<!--begin::Item-->
    									<div class="col">
    										<div class="d-flex align-items-center mr-2">
    											<!--begin::Symbol-->
    											<div class="symbol symbol-25 symbol-light-success mr-4 flex-shrink-0">
    												<div class="symbol-label">
    													<span class="svg-icon svg-icon-lg svg-icon-success">
    														<!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Shopping/Cart3.svg-->
    														<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                  <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                      <polygon points="0 0 24 0 24 24 0 24"/>
                                      <rect fill="#000000" opacity="0.3" x="11" y="3" width="2" height="14" rx="1"/>
                                      <path d="M6.70710678,10.7071068 C6.31658249,11.0976311 5.68341751,11.0976311 5.29289322,10.7071068 C4.90236893,10.3165825 4.90236893,9.68341751 5.29289322,9.29289322 L11.2928932,3.29289322 C11.6714722,2.91431428 12.2810586,2.90106866 12.6757246,3.26284586 L18.6757246,8.76284586 C19.0828436,9.13603827 19.1103465,9.76860564 18.7371541,10.1757246 C18.3639617,10.5828436 17.7313944,10.6103465 17.3242754,10.2371541 L12.0300757,5.38413782 L6.70710678,10.7071068 Z" fill="#000000" fill-rule="nonzero"/>
                                      <rect fill="#000000" opacity="0.3" x="3" y="19" width="18" height="2" rx="1"/>
                                  </g>
    														</svg>
    														<!--end::Svg Icon-->
    													</span>
    												</div>
    											</div>
    											<!--end::Symbol-->
    											<!--begin::Title-->
    											<div>
    												<div class="font-size-h4 text-dark-75 font-weight-bolder">'.$losfixtop.' (dBm)</div>
    												<div class="font-size-sm text-muted font-weight-bold mt-1">Ideal Top</div>
    											</div>
    											<!--end::Title-->
    										</div>
    									</div>
    									<!--end::Item-->
    								</div>
    								<div class="row row-paddingless">
                      <!--begin::Item-->
                      <div class="col">
                        <div class="d-flex align-items-center mr-2 mb-1">
                          <!--begin::Symbol-->
                          <div class="symbol symbol-25 symbol-light-danger mr-4 flex-shrink-0">
                            <div class="symbol-label">
                              <span class="svg-icon svg-icon-lg svg-icon-danger">
                                <!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Home/Library.svg-->
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                  <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                      <rect x="0" y="0" width="24" height="24"/>
                                      <path d="M13.5,21 L13.5,18 C13.5,17.4477153 13.0522847,17 12.5,17 L11.5,17 C10.9477153,17 10.5,17.4477153 10.5,18 L10.5,21 L5,21 L5,4 C5,2.8954305 5.8954305,2 7,2 L17,2 C18.1045695,2 19,2.8954305 19,4 L19,21 L13.5,21 Z M9,4 C8.44771525,4 8,4.44771525 8,5 L8,6 C8,6.55228475 8.44771525,7 9,7 L10,7 C10.5522847,7 11,6.55228475 11,6 L11,5 C11,4.44771525 10.5522847,4 10,4 L9,4 Z M14,4 C13.4477153,4 13,4.44771525 13,5 L13,6 C13,6.55228475 13.4477153,7 14,7 L15,7 C15.5522847,7 16,6.55228475 16,6 L16,5 C16,4.44771525 15.5522847,4 15,4 L14,4 Z M9,8 C8.44771525,8 8,8.44771525 8,9 L8,10 C8,10.5522847 8.44771525,11 9,11 L10,11 C10.5522847,11 11,10.5522847 11,10 L11,9 C11,8.44771525 10.5522847,8 10,8 L9,8 Z M9,12 C8.44771525,12 8,12.4477153 8,13 L8,14 C8,14.5522847 8.44771525,15 9,15 L10,15 C10.5522847,15 11,14.5522847 11,14 L11,13 C11,12.4477153 10.5522847,12 10,12 L9,12 Z M14,12 C13.4477153,12 13,12.4477153 13,13 L13,14 C13,14.5522847 13.4477153,15 14,15 L15,15 C15.5522847,15 16,14.5522847 16,14 L16,13 C16,12.4477153 15.5522847,12 15,12 L14,12 Z" fill="#000000"/>
                                      <rect fill="#FFFFFF" x="13" y="8" width="3" height="3" rx="1"/>
                                      <path d="M4,21 L20,21 C20.5522847,21 21,21.4477153 21,22 L21,22.4 C21,22.7313708 20.7313708,23 20.4,23 L3.6,23 C3.26862915,23 3,22.7313708 3,22.4 L3,22 C3,21.4477153 3.44771525,21 4,21 Z" fill="#000000" opacity="0.3"/>
                                  </g>
                                </svg>
                                <!--end::Svg Icon-->
                              </span>
                            </div>
                          </div>
                          <!--end::Symbol-->
                          <!--begin::Title-->
                          <div>
                            <div class="font-size-h4 text-dark-75 font-weight-bolder">'.$ont_dist.' (m)</div>
                            <div class="font-size-sm text-muted font-weight-bold mt-1">Ont Distance</div>
                          </div>
                          <!--end::Title-->
                        </div>
                      </div>
                      <!--end::Item-->
    									<!--begin::Item-->
    									<div class="col">
    										<div class="d-flex align-items-center mr-2">
    											<!--begin::Symbol-->
    											<div class="symbol symbol-25 symbol-light-primary mr-4 flex-shrink-0">
    												<div class="symbol-label">
    													<span class="svg-icon svg-icon-lg svg-icon-primary">
    														<!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Shopping/Barcode-read.svg-->
    														<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                  <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                      <polygon points="0 0 24 0 24 24 0 24"/>
                                      <rect fill="#000000" opacity="0.3" x="11" y="7" width="2" height="14" rx="1"/>
                                      <path d="M6.70710678,20.7071068 C6.31658249,21.0976311 5.68341751,21.0976311 5.29289322,20.7071068 C4.90236893,20.3165825 4.90236893,19.6834175 5.29289322,19.2928932 L11.2928932,13.2928932 C11.6714722,12.9143143 12.2810586,12.9010687 12.6757246,13.2628459 L18.6757246,18.7628459 C19.0828436,19.1360383 19.1103465,19.7686056 18.7371541,20.1757246 C18.3639617,20.5828436 17.7313944,20.6103465 17.3242754,20.2371541 L12.0300757,15.3841378 L6.70710678,20.7071068 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000003, 16.999999) scale(1, -1) translate(-12.000003, -16.999999) "/>
                                      <rect fill="#000000" opacity="0.3" x="3" y="3" width="18" height="2" rx="1"/>
                                  </g>
    														</svg>
    														<!--end::Svg Icon-->
    													</span>
    												</div>
    											</div>
    											<!--end::Symbol-->
    											<!--begin::Title-->
    											<div>
    												<div class="font-size-h4 text-dark-75 font-weight-bolder">'.$losfixbot.' (dBm)</div>
    												<div class="font-size-sm text-muted font-weight-bold mt-1">Ideal Bot</div>
    											</div>
    											<!--end::Title-->
    										</div>
    									</div>
    									<!--end::Item-->
    								</div>
    							</div>
    							<!--end::Items-->
    						</div>
    						<!--end::Body-->
                </div>
    					</div>
            ';
          }

          $message = str_replace("format_copy_replace",$formatcopy,$message);
          // $message = str_replace("format_copy_replace",str_replace("<br>","\r\n",$formatcopy),$message);
        }else{
          $message = "Check power fail, ont not online!";
        }

        return makeResponse(200, 'success', null, $message);
    }

    public function label($msg,$tag) {
      return "<".$tag.">".$msg."</".$tag.">";
    }

}
