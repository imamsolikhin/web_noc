<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Http;
use App\Models\Vps;
use Illuminate\Http\Request;
use App\Services\Olt\Host;
use App\Services\Olt\SignalFormula;
use App\Services\Olt\Ont;
use App\Services\Olt\OntLast;
use App\Services\Olt\DbaProfile;
use App\Services\Olt\DbaProfileLast;
use App\Services\Olt\PortVlan;
use App\Services\Olt\PortVlanLast;
use App\Services\Olt\LineProfile;
use App\Services\Olt\LineProfileLast;
use App\Services\Olt\SrvProfile;
use App\Services\Olt\SrvProfileLast;
use App\Services\Olt\SrvPort;
use App\Services\Olt\SrvPortLast;
use App\Services\Olt\GemProfile;
use App\Services\Olt\GemProfileLast;
use App\Services\Olt\TrafficTable;
use App\Services\Olt\TrafficTableLast;
use App\Services\Olt\InterfaceGpon;
use App\Services\Olt\InterfaceGponLast;
use App\Services\Telnet\Olt;
use App\Services\Olt\ClientsTools;
use App\Services\Olt\Clients;
use App\Services\Olt\ClientsVlanService;

class ApiController extends Controller{

    public static function checkAccess($id, $request) {
        $host = (new Host)->show($id)->data;
        $olt = (new Olt)->checkAccess($host);

        if (is_array($olt->response)){
          $host->ActiveStatus = 1;
          $host = (new Host())->put($id, (array) $host);
        }else{
          $host->ActiveStatus = 0;
          $host = (new Host())->put($id, (array) $host);
        }
          $host = (new Host)->show($id)->data;
        return makeResponse(200, 'success', $olt->message, $host);
    }

    public static function clientstools($id,$config,$setting) {
      $data["tools"] = (new ClientsTools)->show($id)->data;
      $data['clients'] = (new Clients)->show($id)->data;
      $data['host'] = (new Host)->show($data['clients']->HostCode)->data;

      if($config == "general-info"){
        $data["general-info"][0]= (new OntInfo)->show($id)->data;
        $data["general-info"][1]= (new OntOpticalInfo)->show($id)->data;
        $data["general-info"][2]= (new OntWanInfo)->show($id)->data;
        $data["general-info"][3]= (new OntPortState)->show($id)->data;
      }else{
         if($setting){
            $clientstools = (new Olt)->clientstools($data["host"],$data['clients'],$config);
            $data["tools"]["Code"] = $data['clients']->Code;
            $data["tools"]["ClientsCode"] = $data['clients']->Code;
            if($config == "ont-version"){
              $data["tools"]["Version"] = implode("\n",$clientstools->response[0][1]);
            }
            if($config == "ont-info"){
              $data["tools"]["OntInfo"] = implode("\n",$clientstools->response[0][1]);
            }
            if($config == "wan-info"){
              $data["tools"]["WanInfo"] = implode("\n",$clientstools->response[0][1]);
            }
            if($config == "optical-info"){
              $data["tools"]["Opticalinfo"] = implode("\n",$clientstools->response[0][1]);
            }
            if($config == "register-info"){
              $data["tools"]["Registerinfo"] = implode("\n",$clientstools->response[0][1]);
            }
            if($config == "mac-address"){
              $data["tools"]["MacAddress"] = implode("\n",$clientstools->response[0][1]);
            }
            if($config == "port-state"){
              $data["tools"]["PortState"] = implode("\n",$clientstools->response[0][1]);
            }
            if($config == "port-attribut"){
              $data["tools"]["PortAttribute"] = implode("\n",$clientstools->response[0][1]);
            }
            if($config == "fec-crc-error"){
              $data["tools"]["FecCrcError"] = implode("\n",$clientstools->response[0][1]);
            }

            if(!$data["tools"]){
                $datas = (new ClientsTools())->post($data["tools"]);
            }else{
              $datas = (new ClientsTools())->put($data["tools"]->Code, $data["tools"]);
            }
          }
      }
      $data["tools"] = (new ClientsTools)->show($id)->data;
      return makeResponse(200, 'success', null, $data);
    }

    public static function confighost($host, $request){
      $host = (new Host)->show($host);

      $arr_uri;
      $client = new \GuzzleHttp\Client();
      if($host) {
        $host = $host->data;
        $url = env('IKB_NET_NOC').'/huawei/host/config';
        $url .= '?auth=all';
        $url .= '&config=traffic-table';
        $url .= '&config=line-profile';
        $url .= '&config=srv-profile';
        $url .= '&config=port-vlan';
        $url .= '&config=dba-profile';
        $url .= '&config=service-port';
        $url .= '&config=gem-port';
        $url .= '&config=ont-info';
        $url .= '&hostcode='.$host->Code;
        $url .= '&hostname='.$host->IpAddress;
        $url .= '&username='.$host->Username;
        $url .= '&password='.$host->Password;
        $url .= '&lnprofile='.$host->ProfileId;
        $arr_uri[] = $client->requestAsync('GET', $url)->then(
                      function ($response) {
                          $data = json_decode($response->getBody()->getContents());
                          if($data->response){
                            $results = $data->response;
                            foreach ($results as $val) {
                              if($val['value']){
                                foreach ($val['value'] as $rs) {
                                  if ($rs[0] == 'current-config'){
                                    $response['current-config'] = $rs[2];
                                  }
                                  if ($rs[0] == 'traffic-table'){
                                    $response['traffic-table-last'] = (new TrafficTable)->getAllActive($rs[1])->data;
                                    $delete_data = (new TrafficTable)->delete_host($rs[1]);
                                    if(!empty($rs[2][0])){
                                      $post_data = (new TrafficTable)->post_list($rs[2]);
                                      $response['traffic-table'] = (new TrafficTable)->getAllActive($rs[1])->data;
                                      $delete_data_last = (new TrafficTableLast)->delete_host($rs[1]);
                                      $post_data_last = (new TrafficTableLast)->post_list($response['traffic-table-last']);
                                      $message .= $no.'.) Traffic Table Updated ('.count($response['traffic-table']).')<br class="pt-0 pb-0"/>';
                                      $no = $no+1;
                                    }
                                  }
                                  if ($rs[0] == 'line-profile'){
                                    $response['line-profile'] = $rs[2];
                                    $response['line-profile-last'] = (new LineProfile)->getAllActive($rs[1])->data;
                                    if(!empty($rs[2][0])){
                                      $delete_data = (new LineProfile)->delete_host($rs[1]);
                                      $post_data = (new LineProfile)->post_list($rs[2]);
                                      $response['line-profile'] = (new LineProfile)->getAllActive($rs[1])->data;
                                      $delete_data_last = (new LineProfileLast)->delete_host($rs[1]);
                                      $post_data_last = (new LineProfileLast)->post_list($response['line-profile-last']);
                                      $message .= $no.'.) Line Profile Updated ('.count($response['line-profile']).')<br class="pt-0 pb-0"/>';
                                      $no = $no+1;
                                    }
                                  }
                                  if ($rs[0] == 'srv-profile'){
                                    $response['srv-profile-last'] = (new SrvProfile)->getAllActive($rs[1])->data;
                                    $delete_data = (new SrvProfile)->delete_host($rs[1]);
                                    if(!empty($rs[2][0])){
                                      $post_data = (new SrvProfile)->post_list($rs[2]);
                                      $response['srv-profile'] = (new SrvProfile)->getAllActive($rs[1])->data;
                                      $delete_data_last = (new SrvProfileLast)->delete_host($rs[1]);
                                      $post_data_last = (new SrvProfileLast)->post_list($response['srv-profile-last']);
                                      $message .= $no.'.) Service Profile Updated ('.count($response['srv-profile']).')<br class="pt-0 pb-0"/>';
                                      $no = $no+1;
                                    }
                                  }
                                  if ($rs[0] == 'port-vlan'){
                                    $response['port-vlan-last'] = (new PortVlan)->getAllActive($rs[1])->data;
                                    $delete_data = (new PortVlan)->delete_host($rs[1]);
                                    if(!empty($rs[2][0])){
                                      $post_data = (new PortVlan)->post_list($rs[2]);
                                      $response['port-vlan'] = (new PortVlan)->getAllActive($rs[1])->data;
                                      $delete_data_last = (new PortVlanLast)->delete_host($rs[1]);
                                      $post_data_last = (new PortVlanLast)->post_list($response['port-vlan-last']);
                                      $message .= $no.'.) Port Vlan Updated ('.count($response['port-vlan']).')<br class="pt-0 pb-0"/>';
                                      $no = $no+1;
                                    }
                                  }
                                  if ($rs[0] == 'dba-profile'){
                                    $response['dba-profile-last'] = (new DbaProfile)->getAllActive($rs[1])->data;
                                    $delete_data = (new DbaProfile)->delete_host($rs[1]);
                                    if(!empty($rs[2][0])){
                                      $post_data = (new DbaProfile)->post_list($rs[2]);
                                      $response['dba-profile'] = (new DbaProfile)->getAllActive($rs[1])->data;
                                      $delete_data_last = (new DbaProfileLast)->delete_host($rs[1]);
                                      $post_data_last = (new DbaProfileLast)->post_list($response['dba-profile-last']);
                                      $message .= $no.'.) DBA Profile Updated ('.count($response['dba-profile']).')<br class="pt-0 pb-0"/>';
                                      $no = $no+1;
                                    }
                                  }
                                  if ($rs[0] == 'service-port'){
                                    $response['service-port-last'] = (new SrvPort)->getAllActive($rs[1])->data;
                                    $delete_data = (new SrvPort)->delete_host($rs[1]);
                                    if(!empty($rs[2][0])){
                                      $post_data = (new SrvPort)->post_list($rs[2]);
                                      $response['service-port'] = (new SrvPort)->getAllActive($rs[1])->data;
                                      $delete_data_last = (new SrvPortLast)->delete_host($rs[1]);
                                      $post_data_last = (new SrvPortLast)->post_list($response['service-port-last']);
                                      $message .= $no.'.) Service Port Updated ('.count($response['service-port']).')<br class="pt-0 pb-0"/>';
                                    }
                                  }
                                  if ($rs[0] == 'gem-port'){
                                    $response['gem-port-last'] = (new GemProfile)->getAllActive($rs[1])->data;
                                    $delete_data = (new GemProfile)->delete_host($rs[1]);
                                    if(!empty($rs[2][0])){
                                      $post_data = (new GemProfile)->post_list($rs[2]);
                                      $response['gem-port'] = (new GemProfile)->getAllActive($rs[1])->data;
                                      $delete_data_last = (new GemProfileLast)->delete_host($rs[1]);
                                      $post_data_last = (new GemProfileLast)->post_list($response['gem-port-last']);
                                      $message .= $no.'.) Gem Port Updated ('.count($response['gem-port']).')<br class="pt-0 pb-0"/>';
                                      $no = $no+1;
                                    }
                                  }
                                  if ($rs[0] == 'ont-info'){
                                    $response['ont-info-last'] = (new InterfaceGpon)->getAllActive($rs[1])->data;
                                    $delete_data = (new InterfaceGpon)->delete_host($rs[1]);
                                    if(!empty($rs[2][0])){
                                      $post_data = (new InterfaceGpon)->post_list($rs[2]);
                                      $response['ont-info'] = (new InterfaceGpon)->getAllActive($rs[1])->data;
                                      $delete_data_last = (new InterfaceGponLast)->delete_host($rs[1]);
                                      $post_data_last = (new InterfaceGponLast)->post_list($response['ont-info-last']);
                                      $message .= $no.'.) Register ONT Updated ('.count($response['ont-info']).')<br class="pt-0 pb-0"/>';
                                      $no = $no+1;
                                    }
                                  }
                                }
                              }
                            }
                            return $data->response;
                          }
                      }, function ($exception) {
                          return $exception->getMessage();
                      }
                    );
      }
      $promises = $arr_uri;
      $results = \GuzzleHttp\Promise\settle($promises)->wait();
      $response = array();
      $response['host'] = $host;

      $message = '<h5 class="pt-0 pb-0">List Update:</h5>';
      $message .= '<h5 class="pt-0 pb-0">'.$host->Hostname.' : '.$host->IpAddress.'</h5>';
      $no = 1;

      return makeResponse(200, 'success', $message, $response);
    }

    public static function findidle($host, $request){
        $time_start = microtime(true);
        $response = array();
        $data = array();
        $host = (new Host)->show($host);
        $host = (isset($host->data)) ? $host->data : null;

        if ($host){
            $arr_uri;
            $client = new \GuzzleHttp\Client();
            $url = env('IKB_NET_NOC').'/huawei/host/config';
            $url .= '?auth=0';
            $url .= '&config=ont-autofind';
            $url .= '&hostcode='.$host->Code;
            $url .= '&hostname='.$host->IpAddress;
            $url .= '&username='.$host->Username;
            $url .= '&password='.$host->Password;
            $url .= '&lnprofile='.$host->ProfileId;
            $arr_uri[] = $client->requestAsync('GET', $url)->then(
                          function ($response) {
                              $data = json_decode($response->getBody()->getContents());
                              if($data->response){
                                $results = $data->response;
                                foreach ($results as $val) {
                                  if (!empty($val['value'][0][2])) {
                                    foreach ($val['value'][0][2] as $val) {
                                      array_push($response,$val);
                                    }
                                  }
                                }

                                $last_ont = (new Ont)->getAllActive($host->Code)->data;
                                $delete_data = (new Ont)->delete_host($host->Code);
                                if(!empty($response)){
                                  $post_data = (new Ont)->post_list($response);
                                }
                                $delete_data_last = (new OntLast)->delete_host($host->Code);
                                $post_data_last = (new OntLast)->post_list($last_ont);

                                return $data->response;
                              }
                          }, function ($exception) {
                              return $exception->getMessage();
                          }
                        );

            $promises = $arr_uri;
            $results = \GuzzleHttp\Promise\settle($promises)->wait();
        }

        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start)/60;
        $message = 'ONT IDLE Updated ('.count($response).')';
        return makeResponse(200, 'success', $message, $response);
    }
}
