<?php

namespace App\Http\Controllers\Monitoring;

use DataTables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GraphicController extends Controller {

    public function index() {
        return view('monitoring.graphic.index');
    }
}
