<?php

namespace App\Http\Controllers\Monitoring;

use DataTables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FiberOpticController extends Controller {

    public function index() {
        return view('monitoring.fiber-optic.index');
    }
}
