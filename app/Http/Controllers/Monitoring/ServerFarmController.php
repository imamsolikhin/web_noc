<?php

namespace App\Http\Controllers\Monitoring;

use DataTables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServerFarmController extends Controller {

    public function index() {
        return view('monitoring.server-farm.index');
    }
}
