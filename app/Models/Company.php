<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Company extends Model
{
     protected $table = 'mst_company';
     protected $primaryKey = 'Code';

     public $incrementing = false;
     protected $keyType = 'string';
}
