<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Vps extends Model
{
     protected $table = 'sys_vps';
}
