<?php

// Aside menu
$menu=[];

$dashboard=[
            [
              'title' => 'Dashboard',
              'root' => true,
              'icon' => 'media/svg/icons/Design/Layers.svg',
              'page' => 'dashboard',
              'new-tab' => false,
            ],
            [
              'section' => 'Menu Management'
            ],
          ];
array_push($menu,$dashboard);

$monitoring=[
              [
                'title' => 'Monitoring',
                'root' => true,
                'icon' => 'media/svg/icons/Home/Commode2.svg',
                'submenu' => [
                    [
                      'title' => 'Fiber Optic Client',
                      'root' => true,
                      'icon' => 'media/svg/icons/Code/Terminal.svg',
                      'bullet' => 'dot',
                      'page' => 'monitoring/fiber-optic'
                    ],
                    [
                      'title' => 'Wireless Clients',
                      'root' => true,
                      'icon' => 'media/svg/icons/Code/Terminal.svg',
                      'bullet' => 'dot',
                      'page' => 'monitoring/wireless'
                    ],
                    [
                      'title' => 'BTS Lists',
                      'root' => true,
                      'icon' => 'media/svg/icons/Code/Terminal.svg',
                      'bullet' => 'dot',
                      'page' => 'monitoring/bts'
                    ],
                    [
                      'title' => 'Graphic',
                      'root' => true,
                      'icon' => 'media/svg/icons/Code/Terminal.svg',
                      'bullet' => 'dot',
                      'page' => 'monitoring/graphic'
                    ],
                    [
                      'title' => 'Network Mapping',
                      'root' => true,
                      'icon' => 'media/svg/icons/Code/Terminal.svg',
                      'bullet' => 'dot',
                      'page' => 'monitoring/network-map'
                    ],
                    [
                      'title' => 'Servers Farm',
                      'root' => true,
                      'icon' => 'media/svg/icons/Code/Terminal.svg',
                      'bullet' => 'dot',
                      'page' => 'monitoring/server-farm'
                    ],
                    [
                      'title' => 'Core Network',
                      'root' => true,
                      'icon' => 'media/svg/icons/Code/Terminal.svg',
                      'bullet' => 'dot',
                      'page' => 'monitoring/core-network'
                    ],
                ],
              ]
            ];
array_push($menu,$monitoring);

$olt = [
        [
          'title' => 'NOC',
          'root' => true,
          'icon' => 'media/svg/icons/Home/Commode2.svg',
          'submenu' => [
              [
                'title' => 'Setting',
                'root' => true,
                'icon' => 'media/svg/icons/Home/Commode2.svg',
                'submenu' => [
                    [
                      'title' => 'Brand',
                      'root' => true,
                      'icon' => 'media/svg/icons/Code/Terminal.svg',
                      'bullet' => 'dot',
                      'page' => 'olt/brand'
                    ],
                    [
                      'title' => 'Signal Formula',
                      'root' => true,
                      'icon' => 'media/svg/icons/Code/Terminal.svg',
                      'bullet' => 'dot',
                      'page' => 'olt/signal-formula'
                    ],
                    [
                      'title' => 'Ont Idle',
                      'root' => true,
                      'icon' => 'media/svg/icons/Code/Terminal.svg',
                      'bullet' => 'dot',
                      'page' => 'olt/ont'
                    ],
                    [
                      'title' => 'Templates',
                      'root' => true,
                      'icon' => 'media/svg/icons/Code/Terminal.svg',
                      'bullet' => 'dot',
                      'page' => 'olt/template'
                    ],
                ],
              ],
              [
                'title' => 'Host',
                'root' => true,
                'icon' => 'media/svg/icons/Code/Terminal.svg',
                'bullet' => 'dot',
                'page' => 'olt/host'
              ],
              [
                'title' => 'Client Olt',
                'root' => true,
                'icon' => 'media/svg/icons/Code/Terminal.svg',
                'bullet' => 'dot',
                'page' => 'olt/clients'
              ],
              [
                'title' => 'Client History',
                'root' => true,
                'icon' => 'media/svg/icons/Code/Terminal.svg',
                'bullet' => 'dot',
                'page' => 'olt/clients-history'
              ],
          ],
        ]
      ];

array_push($menu,$olt);

$customer = [
        [
          'title' => 'Customer Data',
          'root' => true,
          'icon' => 'media/svg/icons/Home/Commode2.svg',
          'submenu' => [
              [
                'title' => 'Isp',
                'root' => true,
                'icon' => 'media/svg/icons/Code/Terminal.svg',
                'bullet' => 'dot',
                'page' => 'olt/isp'
              ],
              [
                'title' => 'Customer',
                'root' => true,
                'icon' => 'media/svg/icons/Code/Terminal.svg',
                'bullet' => 'dot',
                'page' => 'olt/customer'
              ],
              [
                'title' => 'Customer Registration',
                'root' => true,
                'icon' => 'media/svg/icons/Code/Terminal.svg',
                'bullet' => 'dot',
                'page' => 'olt/customer-registration'
              ],
              [
                'title' => 'Customer Site',
                'root' => true,
                'icon' => 'media/svg/icons/Code/Terminal.svg',
                'bullet' => 'dot',
                'page' => 'olt/customer-site'
              ],
          ],
        ]
      ];
array_push($menu,$customer);

$ticketing = [
        [
          'title' => 'Ticketing',
          'root' => true,
          'icon' => 'media/svg/icons/Home/Commode2.svg',
          'submenu' => [
              [
                'title' => 'Master',
                'root' => true,
                'icon' => 'media/svg/icons/Home/Commode2.svg',
                'submenu' => [
                    [
                      'title' => 'Open Bts',
                      'root' => true,
                      'icon' => 'media/svg/icons/Code/Terminal.svg',
                      'bullet' => 'dot',
                      'page' => 'ticketing/master/open-bts'
                    ],
                    [
                      'title' => 'Open Customer',
                      'root' => true,
                      'icon' => 'media/svg/icons/Code/Terminal.svg',
                      'bullet' => 'dot',
                      'page' => 'ticketing/master/open-customer'
                    ],
                    [
                      'title' => 'Device Type FO',
                      'root' => true,
                      'icon' => 'media/svg/icons/Code/Terminal.svg',
                      'bullet' => 'dot',
                      'page' => 'ticketing/master/open-device-type-fo'
                    ],
                    [
                      'title' => 'Device Type FO',
                      'root' => true,
                      'icon' => 'media/svg/icons/Code/Terminal.svg',
                      'bullet' => 'dot',
                      'page' => 'ticketing/master/open-device-type-wll'
                    ],
                ],
              ],
              [
                'title' => 'Open Ticket List',
                'root' => true,
                'icon' => 'media/svg/icons/Code/Terminal.svg',
                'bullet' => 'dot',
                'page' => 'ticketing/fmi/ticket-open'
              ],
              [
                'title' => 'Task Assigment',
                'root' => true,
                'icon' => 'media/svg/icons/Code/Terminal.svg',
                'bullet' => 'dot',
                'page' => 'ticketing/fmi/task-assignment'
              ],
              [
                'title' => 'Ticket Status',
                'root' => true,
                'icon' => 'media/svg/icons/Code/Terminal.svg',
                'bullet' => 'dot',
                'page' => 'ticketing/fmi/ticket-status'
              ],
          ],
        ]
      ];
array_push($menu,$ticketing);

$asset = [
        [
          'title' => 'Asset management',
          'root' => true,
          'icon' => 'media/svg/icons/Home/Commode2.svg',
          'submenu' => [
            [
              'title' => 'Adjustment In',
              'root' => true,
              'icon' => 'media/svg/icons/Code/Terminal.svg',
              'bullet' => 'dot',
              'page' => 'asset-management/adjustment-in'
            ],
            [
              'title' => 'Adjustment Out',
              'root' => true,
              'icon' => 'media/svg/icons/Code/Terminal.svg',
              'bullet' => 'dot',
              'page' => 'asset-management/adjustment-out'
            ],
            [
              'title' => 'Warehouse Customer Installation',
              'root' => true,
              'icon' => 'media/svg/icons/Code/Terminal.svg',
              'bullet' => 'dot',
              'page' => 'asset-management/whm-cus-installation'
            ],
            [
              'title' => 'Warehouse Customer Uninstallation',
              'root' => true,
              'icon' => 'media/svg/icons/Code/Terminal.svg',
              'bullet' => 'dot',
              'page' => 'asset-management/whm-cus-uninstallation'
            ],
            [
              'title' => 'Warehouse External',
              'root' => true,
              'icon' => 'media/svg/icons/Code/Terminal.svg',
              'bullet' => 'dot',
              'page' => 'asset-management/whm-external'
            ],
            [
              'title' => 'Warehouse Internal',
              'root' => true,
              'icon' => 'media/svg/icons/Code/Terminal.svg',
              'bullet' => 'dot',
              'page' => 'asset-management/whm-internal'
            ],
            [
              'title' => 'Pop Instaled',
              'root' => true,
              'icon' => 'media/svg/icons/Code/Terminal.svg',
              'bullet' => 'dot',
              'page' => 'asset-management/whm-pop-instaled'
            ],
            [
              'title' => 'Pop Uninstaled',
              'root' => true,
              'icon' => 'media/svg/icons/Code/Terminal.svg',
              'bullet' => 'dot',
              'page' => 'asset-management/whm-pop-uninstaled'
            ],
          ],
        ]
      ];
array_push($menu,$asset);

$master = [
        [
          'title' => 'Master Data',
          'root' => true,
          'icon' => 'media/svg/icons/Home/Commode2.svg',
          'submenu' => [
              [
                'title' => 'Bandwidth Package',
                'root' => true,
                'icon' => 'media/svg/icons/Code/Terminal.svg',
                'bullet' => 'dot',
                'page' => 'master/bandwidth-package'
              ],
              [
                'title' => 'Base Transmission Station',
                'root' => true,
                'icon' => 'media/svg/icons/Code/Terminal.svg',
                'bullet' => 'dot',
                'page' => 'master/base-transmission-station'
              ],
              [
                'title' => 'Branch',
                'root' => true,
                'icon' => 'media/svg/icons/Code/Terminal.svg',
                'bullet' => 'dot',
                'page' => 'master/branch'
              ],
              [
                'title' => 'City',
                'root' => true,
                'icon' => 'media/svg/icons/Code/Terminal.svg',
                'bullet' => 'dot',
                'page' => 'master/city'
              ],
              [
                'title' => 'Client Type',
                'root' => true,
                'icon' => 'media/svg/icons/Code/Terminal.svg',
                'bullet' => 'dot',
                'page' => 'master/client-type'
              ],
              [
                'title' => 'Country',
                'root' => true,
                'icon' => 'media/svg/icons/Code/Terminal.svg',
                'bullet' => 'dot',
                'page' => 'master/country'
              ],
              [
                'title' => 'Currency',
                'root' => true,
                'icon' => 'media/svg/icons/Code/Terminal.svg',
                'bullet' => 'dot',
                'page' => 'master/currency'
              ],
              [
                'title' => 'Customer ERP',
                'root' => true,
                'icon' => 'media/svg/icons/Code/Terminal.svg',
                'bullet' => 'dot',
                'page' => 'master/customer-erp'
              ],
              [
                'title' => 'Customer NOC',
                'root' => true,
                'icon' => 'media/svg/icons/Code/Terminal.svg',
                'bullet' => 'dot',
                'page' => 'master/customer-noc'
              ],
              [
                'title' => 'Department',
                'root' => true,
                'icon' => 'media/svg/icons/Code/Terminal.svg',
                'bullet' => 'dot',
                'page' => 'master/department'
              ],
              [
                'title' => 'Division',
                'root' => true,
                'icon' => 'media/svg/icons/Code/Terminal.svg',
                'bullet' => 'dot',
                'page' => 'master/division'
              ],
              [
                'title' => 'Island',
                'root' => true,
                'icon' => 'media/svg/icons/Code/Terminal.svg',
                'bullet' => 'dot',
                'page' => 'master/island'
              ],
              [
                'title' => 'Item',
                'root' => true,
                'icon' => 'media/svg/icons/Code/Terminal.svg',
                'bullet' => 'dot',
                'page' => 'master/item'
              ],
              [
                'title' => 'Item Brand',
                'root' => true,
                'icon' => 'media/svg/icons/Code/Terminal.svg',
                'bullet' => 'dot',
                'page' => 'master/item-brand'
              ],
              [
                'title' => 'Item Category',
                'root' => true,
                'icon' => 'media/svg/icons/Code/Terminal.svg',
                'bullet' => 'dot',
                'page' => 'master/item-category'
              ],
              [
                'title' => 'Item Division',
                'root' => true,
                'icon' => 'media/svg/icons/Code/Terminal.svg',
                'bullet' => 'dot',
                'page' => 'master/item-division'
              ],
              [
                'title' => 'Item Sub Category',
                'root' => true,
                'icon' => 'media/svg/icons/Code/Terminal.svg',
                'bullet' => 'dot',
                'page' => 'master/item-sub-category'
              ],
              [
                'title' => 'Reason',
                'root' => true,
                'icon' => 'media/svg/icons/Code/Terminal.svg',
                'bullet' => 'dot',
                'page' => 'master/reason'
              ],
              [
                'title' => 'Job Position',
                'root' => true,
                'icon' => 'media/svg/icons/Code/Terminal.svg',
                'bullet' => 'dot',
                'page' => 'master/job-position'
              ],
              [
                'title' => 'Mitra',
                'root' => true,
                'icon' => 'media/svg/icons/Code/Terminal.svg',
                'bullet' => 'dot',
                'page' => 'master/mitra'
              ],
              [
                'title' => 'Problem Type',
                'root' => true,
                'icon' => 'media/svg/icons/Code/Terminal.svg',
                'bullet' => 'dot',
                'page' => 'master/problem-type'
              ],
              [
                'title' => 'Product',
                'root' => true,
                'icon' => 'media/svg/icons/Code/Terminal.svg',
                'bullet' => 'dot',
                'page' => 'master/product'
              ],
              [
                'title' => 'Province',
                'root' => true,
                'icon' => 'media/svg/icons/Code/Terminal.svg',
                'bullet' => 'dot',
                'page' => 'master/province'
              ],
              // [
              //   'title' => 'Serial No Detail',
              //   'root' => true,
              //   'icon' => 'media/svg/icons/Code/Terminal.svg',
              //   'bullet' => 'dot',
              //   'page' => 'master/serial-no-detail'
              // ],
              [
                'title' => 'Team',
                'root' => true,
                'icon' => 'media/svg/icons/Code/Terminal.svg',
                'bullet' => 'dot',
                'page' => 'master/team'
              ],
              [
                'title' => 'Unit Of Measure',
                'root' => true,
                'icon' => 'media/svg/icons/Code/Terminal.svg',
                'bullet' => 'dot',
                'page' => 'master/unit-of-measure'
              ],
              [
                'title' => 'Warehouse',
                'root' => true,
                'icon' => 'media/svg/icons/Code/Terminal.svg',
                'bullet' => 'dot',
                'page' => 'master/warehouse'
              ],
          ],
        ]
      ];
array_push($menu,$master);

$seting = [
        [
            'section' => 'Settings'
        ],
        [
            'title' => 'User Management',
            'root' => true,
            'icon' => 'media/svg/icons/General/User.svg',
            'bullet' => 'dot',
            'page' => 'management/user'
        ],
        [
            'title' => 'Role & Permission',
            'root' => true,
            'icon' => 'media/svg/icons/General/Shield-check.svg',
            'bullet' => 'dot',
            'page' => 'management/role'
        ],
        [
            'title' => 'Menu',
            'root' => true,
            'icon' => 'media/svg/icons/General/Shield-check.svg',
            'bullet' => 'dot',
            'page' => 'management/menu'
        ],
        [
            'title' => 'User Login History',
            'root' => true,
            'icon' => 'media/svg/icons/Code/Time-schedule.svg',
            'bullet' => 'dot',
            'page' => 'management/login-history'
        ],
        [
            'title' => 'Log Out',
            'root' => true,
            'icon' => 'media/svg/icons/Navigation/Sign-out.svg',
            'bullet' => 'dot',
            'page' => 'logout',
            'color' => 'red'
        ]
      ];
array_push($menu,$seting);

$asside['items'] = [$menu];
return $asside;
