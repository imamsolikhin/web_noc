<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApiController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

\URL::forceRootUrl(env('APP_URL'));
Route::middleware('api')->get('config-host/{host}', function ($host, Request $request) {
   return ApiController::confighost($host, $request);
});
Route::middleware('api')->get('ont-idle/{host}', function ($host, Request $request) {
   return ApiController::findidle($host, $request);
});
Route::middleware('api')->get('check-host/{host}', function ($host, Request $request) {
   return ApiController::checkAccess($host, $request);
});

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
