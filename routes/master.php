<?php

\URL::forceRootUrl(env('APP_URL'));

//bandwidth-package
Route::resource('bandwidth-package', 'BandwidthPackageController', ['except' => ['create']]);
Route::post('bandwidth-package/data', 'BandwidthPackageController@getData')->name('bandwidth-package.data');

//base-transmission-station
Route::resource('base-transmission-station', 'BaseTransmissionStationController', ['except' => ['create']]);
Route::post('base-transmission-station/data', 'BaseTransmissionStationController@getData')->name('base-transmission-station.data');

//branch
Route::resource('branch', 'BranchController', ['except' => ['create']]);
Route::post('branch/data', 'BranchController@getData')->name('branch.data');

//city
Route::resource('city', 'CityController', ['except' => ['create']]);
Route::post('city/data', 'CityController@getData')->name('city.data');

//client-type
Route::resource('client-type', 'ClientTypeController', ['except' => ['create']]);
Route::post('client-type/data', 'ClientTypeController@getData')->name('client-type.data');

//country
Route::resource('country', 'CountryController', ['except' => ['create']]);
Route::post('country/data', 'CountryController@getData')->name('country.data');

//currency
Route::resource('currency', 'CurrencyController', ['except' => ['create']]);
Route::post('currency/data', 'CurrencyController@getData')->name('currency.data');

//customer-erp
Route::resource('customer-erp', 'CustomerErpController', ['except' => ['create']]);
Route::post('customer-erp/data', 'CustomerErpController@getData')->name('customer-erp.data');

//customer-noc
Route::resource('customer-noc', 'CustomerNocController', ['except' => ['create']]);
Route::post('customer-noc/data', 'CustomerNocController@getData')->name('customer-noc.data');

//department
Route::resource('department', 'DepartmentController', ['except' => ['create']]);
Route::post('department/data', 'DepartmentController@getData')->name('department.data');

//division
Route::resource('division', 'DivisionController', ['except' => ['create']]);
Route::post('division/data', 'DivisionController@getData')->name('division.data');

//island
Route::resource('island', 'IslandController', ['except' => ['create']]);
Route::post('island/data', 'IslandController@getData')->name('island.data');

//reason
Route::resource('reason', 'ReasonController', ['except' => ['create']]);
Route::post('reason/data', 'ReasonController@getData')->name('reason.data');

//item
Route::resource('item', 'ItemController', ['except' => ['create']]);
Route::post('item/data', 'ItemController@getData')->name('item.data');

//item-brand
Route::resource('item-brand', 'ItemBrandController', ['except' => ['create']]);
Route::post('item-brand/data', 'ItemBrandController@getData')->name('item-brand.data');

//item-category
Route::resource('item-category', 'ItemCategoryController', ['except' => ['create']]);
Route::post('item-category/data', 'ItemCategoryController@getData')->name('item-category.data');

//item-division
Route::resource('item-division', 'ItemDivisionController', ['except' => ['create']]);
Route::post('item-division/data', 'ItemDivisionController@getData')->name('item-division.data');

//item-sub-category
Route::resource('item-sub-category', 'ItemSubCategoryController', ['except' => ['create']]);
Route::post('item-sub-category/data', 'ItemSubCategoryController@getData')->name('item-sub-category.data');

//job-position
Route::resource('job-position', 'JobPositionController', ['except' => ['create']]);
Route::post('job-position/data', 'JobPositionController@getData')->name('job-position.data');

//mitra
Route::resource('mitra', 'MitraController', ['except' => ['create']]);
Route::post('mitra/data', 'MitraController@getData')->name('mitra.data');

//problem-type
Route::resource('problem-type', 'ProblemTypeController', ['except' => ['create']]);
Route::post('problem-type/data', 'ProblemTypeController@getData')->name('problem-type.data');

//product
Route::resource('product', 'ProductController', ['except' => ['create']]);
Route::post('product/data', 'ProductController@getData')->name('product.data');

//province
Route::resource('province', 'ProvinceController', ['except' => ['create']]);
Route::post('province/data', 'ProvinceController@getData')->name('province.data');

//serial-no-detail
// Route::resource('serial-no-detail', 'SerialnoDetailController', ['except' => ['create']]);
// Route::post('serial-no-detail/data', 'SerialnoDetailController@getData')->name('serial-no-detail.data');

//team
Route::resource('team', 'TeamController', ['except' => ['create']]);
Route::post('team/data', 'TeamController@getData')->name('team.data');

//unit-of-measure
Route::resource('unit-of-measure', 'UnitofMeasureController', ['except' => ['create']]);
Route::post('unit-of-measure/data', 'UnitofMeasureController@getData')->name('unit-of-measure.data');

//warehouse
Route::resource('warehouse', 'WarehouseController', ['except' => ['create']]);
Route::post('warehouse/data', 'WarehouseController@getData')->name('warehouse.data');
