<?php

use Illuminate\Support\Facades\Route;

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */
 \URL::forceRootUrl(env('APP_URL'));
 Route::get('/', 'HomeController@index');
 Route::get('dashboard', 'HomeController@dashboard')->name('dashboard');
 Route::get('/quick-search', 'PagesController@quickSearch')->name('quick-search');
 Route::get('/noauth', 'HomeController@noauth')->name('noauth');

 Route::middleware('web')->group(function() {
    Route::middleware('guest')->namespace('Auth')->group(function() {
     Route::get('/', 'LoginController@showLoginForm')->name('login.main');
     Route::get('/attribute', 'LoginController@attribute')->name('login.attribute');
    	Route::get('login', 'LoginController@showLoginForm')->name('login.show-form');
    	Route::post('login', 'LoginController@login')->name('login');
    });
   Route::get('logout', 'Auth\LoginController@logout')->name('logout');
 });

 Route::middleware('web')->group(function() {
   Route::prefix('management')->namespace('Management')->as('management.')->group(function() {
       Route::prefix('user')->as('user.')->group(function() {
           Route::post('data', 'UserController@getData')->name('data');
           Route::delete('{id}', 'UserController@destroy')->name('show');
           Route::get('{id}', 'UserController@edit')->name('show');
           Route::get('{id}/change-password', 'UserController@changeOtherPassword')->name('other.change-password');
           Route::put('{id}/update-password', 'UserController@updateOtherPassword')->name('other.update-password');
       });
       Route::resource('user', 'UserController', ['except' => ['create', 'show']]);


       Route::get('login-history', 'UserHistoryController@showLoginHistory')->name('login-history');
       Route::post('login-history/data', 'UserHistoryController@getLoginHistoryData')->name('login-history.data');

       Route::prefix('role')->as('role.')->group(function() {
           Route::post('data', 'RoleController@getData')->name('data');
           Route::get('{id}', 'RoleController@show')->name('show');
           Route::get('auth/{id}', 'RoleController@show_auth')->name('show.auth');
           Route::post('{id}/{col}/{val}', 'RoleController@update_auth')->name('update.auth');
       });
       Route::resource('role', 'RoleController', ['except' => ['create', 'show']]);

       Route::prefix('menu')->as('menu.')->group(function() {
           Route::post('data', 'MenuController@getData')->name('data');
           Route::get('{id}', 'MenuController@show')->name('show');
       });
       Route::resource('menu', 'MenuController', ['except' => ['create', 'show']]);
   });
 });

Route::prefix('telnet')->namespace('Telnet')->as('telnet.')->group(function() {
    Route::prefix('olt-config')->as('olt-config.')->group(function() {
        Route::get('/clientstools/{id}/{config}/{setting}', 'OltConfigController@clientstools')->name('clientstools');
        Route::get('/autofindall', 'OltConfigController@autoFindAll')->name('autofindall');
        Route::get('/autofindhost/{host}', 'OltConfigController@autoFindHost')->name('autofindhost');
        Route::get('/checkpower/{host}/{sn}', 'OltConfigController@checkPowerSn')->name('checkpower');
        Route::get('/checkconfig/{id}/{config}', 'OltConfigController@checkConfig')->name('check-config');
    });

    Route::prefix('olt-clients')->as('olt-clients.')->group(function() {
        Route::post('/activation/{id}/{setting}/{config}', 'OltConfigController@activation')->name('activation');
        Route::post('/modify-sn/{id}/{sn}/{config}', 'OltConfigController@modifysn')->name('modify-sn');
        Route::post('/modify-traffic/{id}/{old}/{new}', 'OltConfigController@modifytraffic')->name('modify-traffic');
        Route::get('/activation/{id}/{setting}/{config}', 'OltConfigController@activation')->name('activation');
        Route::get('/modify-sn/{id}/{sn}/{config}', 'OltConfigController@modifysn')->name('modify-sn');
        Route::get('/modify-traffic/{id}/{new}/{config}', 'OltConfigController@modifytraffic')->name('modify-traffic');
    });
});


Route::prefix('olt')->namespace('Olt')->as('olt.')->group(function() {
    Route::prefix('ont')->as('ont.')->group(function() {
        Route::post('data', 'OntController@getData')->name('data');
        Route::get('/{id}', 'OntController@show')->name('show');
    });
    Route::resource('ont', 'OntController', ['except' => ['create', 'show']]);

    Route::prefix('host')->as('host.')->group(function() {
        Route::post('data', 'HostController@getData')->name('data');
        Route::get('/{id}', 'HostController@show')->name('show');
        Route::get('show-config/{id}', 'HostController@show_config')->name('show-config');
    });
    Route::resource('host', 'HostController', ['except' => ['create', 'show']]);

    Route::prefix('signal-formula')->as('signal-formula.')->group(function() {
        Route::post('data', 'SignalFormulaController@getData')->name('data');
        Route::get('/{id}', 'SignalFormulaController@show')->name('show');
    });
    Route::resource('signal-formula', 'SignalFormulaController', ['except' => ['create', 'show']]);

    Route::prefix('brand')->as('brand.')->group(function() {
        Route::post('data', 'BrandController@getData')->name('data');
        Route::get('/{id}', 'BrandController@show')->name('show');
    });
    Route::resource('brand', 'BrandController', ['except' => ['create', 'show']]);

    Route::prefix('clients')->as('clients.')->group(function() {
        Route::post('data', 'ClientsController@getData')->name('data');
        Route::get('data', 'ClientsController@getData')->name('data');
        Route::get('show-header/{id}', 'ClientsController@show_header')->name('show-header');
        Route::get('/{id}', 'ClientsController@show')->name('show');
        Route::get('/{form}/page', 'ClientsController@page')->name('page');
    });
    Route::resource('clients', 'ClientsController', ['except' => ['create', 'show']]);

    Route::prefix('clients-history')->as('clients-history.')->group(function() {
        Route::post('data', 'ClientsHistoryController@getData')->name('data');
        Route::get('/{id}', 'ClientsHistoryController@show')->name('show');
        Route::get('show-header/{id}', 'ClientsHistoryController@show_header')->name('show-header');
    });
    Route::resource('clients-history', 'ClientsHistoryController', ['except' => ['create', 'show']]);

    Route::prefix('isp')->as('isp.')->group(function() {
        Route::post('data', 'IspController@getData')->name('data');
        Route::get('/{id}', 'IspController@show')->name('show');
    });
    Route::resource('isp', 'IspController', ['except' => ['create', 'show']]);

    Route::prefix('customer')->as('customer.')->group(function() {
        Route::post('data', 'CustomerController@getData')->name('data');
        Route::get('regist/{id}', 'CustomerController@regist')->name('regist');
        Route::get('/{id}', 'CustomerController@show')->name('show');
        Route::get('/{form}/page', 'CustomerController@page')->name('page');

    });
    Route::resource('customer', 'CustomerController', ['except' => ['create', 'show']]);

    Route::prefix('customer-registration')->as('customer-registration.')->group(function() {
        Route::post('data', 'CustomerRegistrationController@getData')->name('data');
        Route::get('/{id}', 'CustomerRegistrationController@show')->name('show');
    });
    Route::resource('customer-registration', 'CustomerRegistrationController', ['except' => ['create', 'show']]);

    Route::prefix('customer-site')->as('customer-site.')->group(function() {
        Route::post('data', 'CustomerSiteController@getData')->name('data');
        Route::get('/{id}', 'CustomerSiteController@show')->name('show');
    });
    Route::resource('customer-site', 'CustomerSiteController', ['except' => ['create', 'show']]);

    Route::prefix('customer-switch')->as('customer-switch.')->group(function() {
        Route::post('data', 'CustomerSwitchController@getData')->name('data');
        Route::get('/{id}', 'CustomerSwitchController@show')->name('show');
        Route::get('/{form}/page', 'CustomerSwitchController@page')->name('page');

    });
    Route::resource('customer-switch', 'CustomerSwitchController', ['except' => ['create', 'show']]);

    Route::prefix('template')->as('template.')->group(function() {
        Route::post('data', 'TemplateController@getData')->name('data');
        Route::get('/{id}', 'TemplateController@show')->name('show');
        Route::get('/{form}/page', 'TemplateController@page')->name('page');
        Route::get('show-header/{id}', 'TemplateController@show_header')->name('show-header');


    });
    Route::resource('template', 'TemplateController', ['except' => ['create', 'show']]);

});

Route::prefix('monitoring')->namespace('Monitoring')->as('monitoring.')->group(function() {
    Route::resource('fiber-optic', 'FiberOpticController', ['except' => ['create', 'show']]);
    Route::resource('wireless', 'WirelessController', ['except' => ['create', 'show']]);
    Route::resource('bts', 'BtsController', ['except' => ['create', 'show']]);
    Route::resource('graphic', 'GraphicController', ['except' => ['create', 'show']]);
    Route::resource('network-map', 'NetworkMapController', ['except' => ['create', 'show']]);
    Route::resource('server-farm', 'ServerFarmController', ['except' => ['create', 'show']]);
    Route::resource('core-network', 'CoreNetworkController', ['except' => ['create', 'show']]);
});

Route::prefix('asset-management')->namespace('Asset')->as('asset.')->group(function() {
     Route::prefix('adjustment-in')->as('adjustment-in.')->group(function() {
        Route::post('data', 'AdjustmentInController@getData')->name('data');
        Route::get('/{id}', 'AdjustmentInController@show')->name('show');
        Route::get('/{form}/page', 'AdjustmentInController@page')->name('page');
        // Route::get('/{id}/store', 'BranchController@getData')->name('store');
        Route::get('show-header/{id}', 'AdjustmentInController@show_header')->name('show-header');

    });
    Route::resource('adjustment-in', 'AdjustmentInController', ['except' => ['create', 'show']]);

   Route::prefix('adjustment-in-item-detail')->as('adjustment-in-item-detail.')->group(function() {
        Route::post('data', 'AdjustmentInItemDetailController@getData')->name('data');
        Route::get('/{id}', 'AdjustmentInItemDetailController@show')->name('show');
    });
    Route::resource('adjustment-in-item-detail', 'AdjustmentInItemDetailController', ['except' => ['create', 'show']]);

     Route::prefix('adjustment-in-serial-no-detail')->as('adjustment-in-serial-no-detail.')->group(function() {
        Route::post('data', 'AdjustmentInSerialNoDetailController@getData')->name('data');
        Route::get('/{id}', 'AdjustmentInSerialNoDetailController@show')->name('show');
    });
    Route::resource('adjustment-in-serial-no-detail', 'AdjustmentInSerialNoDetailController', ['except' => ['create', 'show']]);

    Route::prefix('adjustment-out')->as('adjustment-out.')->group(function() {
        Route::post('data', 'AdjustmentOutController@getData')->name('data');
        Route::get('/{id}', 'AdjustmentOutController@show')->name('show');
        Route::get('/{form}/page', 'AdjustmentOutController@page')->name('page');
        Route::get('show-header/{id}', 'AdjustmentOutController@show_header')->name('show-header');
    });
    Route::resource('adjustment-out', 'AdjustmentOutController', ['except' => ['create', 'show']]);

    Route::prefix('adjustment-out-item-detail')->as('adjustment-out-item-detail.')->group(function() {
        Route::post('data', 'AdjustmentOutItemDetailController@getData')->name('data');
        Route::get('/{id}', 'AdjustmentOutItemDetailController@show')->name('show');

        // Route::get('show-header/{id}', 'AdjustmentOutController@show_header')->name('show-header');


    });
    Route::resource('adjustment-out-item-detail', 'AdjustmentOutItemDetailController', ['except' => ['create', 'show']]);

      Route::prefix('adjustment-out-serial-no-detail')->as('adjustment-out-serial-no-detail.')->group(function() {
        Route::post('data', 'AdjustmentOutSerialNoDetailController@getData')->name('data');
        Route::get('/{id}', 'AdjustmentOutSerialNoDetailController@show')->name('show');

    });
    Route::resource('adjustment-out-serial-no-detail', 'AdjustmentOutSerialNoDetailController', ['except' => ['create', 'show']]);

    Route::prefix('whm-cus-installation')->as('whm-cus-installation.')->group(function() {
        Route::post('data', 'WhmCusInstallationController@getData')->name('data');
        Route::get('/{id}', 'WhmCusInstallationController@show')->name('show');
        Route::get('/{form}/page', 'WhmCusInstallationController@page')->name('page');
        Route::get('show-header/{id}', 'WhmCusInstallationController@show_header')->name('show-header');

    });
    Route::resource('whm-cus-installation', 'WhmCusInstallationController', ['except' => ['create', 'show']]);

    Route::prefix('whm-cus-installation-item-detail')->as('whm-cus-installation-item-detail.')->group(function() {
        Route::post('data', 'WhmCusInstallationItemDetailController@getData')->name('data');
        Route::get('/{id}', 'WhmCusInstallationItemDetailController@show')->name('show');
    });
    Route::resource('whm-cus-installation-item-detail', 'WhmCusInstallationItemDetailController', ['except' => ['create', 'show']]);

    Route::prefix('whm-custsn-detail')->as('whm-custsn-detail.')->group(function() {
        Route::post('data', 'WhmCusInstallationSerialNoDetailController@getData')->name('data');
        Route::get('/{id}', 'WhmCusInstallationSerialNoDetailController@show')->name('show');
    });
    Route::resource('whm-custsn-detail', 'WhmCusInstallationSerialNoDetailController', ['except' => ['create', 'show']]);

    Route::prefix('whm-cus-uninstallation')->as('whm-cus-uninstallation.')->group(function() {
        Route::post('data', 'WhmCusUninstallationController@getData')->name('data');
        Route::get('/{id}', 'WhmCusUninstallationController@show')->name('show');
        Route::get('/{form}/page', 'WhmCusUninstallationController@page')->name('page');
        Route::get('show-header/{id}', 'WhmCusUninstallationController@show_header')->name('show-header');

    });
    Route::resource('whm-cus-uninstallation', 'WhmCusUninstallationController', ['except' => ['create', 'show']]);

    Route::prefix('whm-cus-uninstal-item-detail')->as('whm-customer-uninstal-item-detail.')->group(function() {
        Route::post('data', 'WhmCusUninstalItemDetailController@getData')->name('data');
        Route::get('/{id}', 'WhmCusUninstalItemDetailController@show')->name('show');
    });
    Route::resource('whm-cus-uninstal-item-detail', 'WhmCusUninstalItemDetailController', ['except' => ['create', 'show']]);

    Route::prefix('whm-custunsn-detail')->as('whm-custunsn-detail.')->group(function() {
        Route::post('data', 'WhmCusUninstalSerialNoDetailController@getData')->name('data');
        Route::get('/{id}', 'WhmCusUninstalSerialNoDetailController@show')->name('show');
    });
    Route::resource('whm-custunsn-detail', 'WhmCusUninstalSerialNoDetailController', ['except' => ['create', 'show']]);

    Route::prefix('whm-external')->as('whm-external.')->group(function() {
        Route::post('data', 'WhmExternalController@getData')->name('data');
        Route::get('/{id}', 'WhmExternalController@show')->name('show');
        Route::get('/{form}/page', 'WhmExternalController@page')->name('page');
        Route::get('show-header/{id}', 'WhmExternalController@show_header')->name('show-header');

    });
    Route::resource('whm-external', 'WhmExternalController', ['except' => ['create', 'show']]);

    Route::prefix('whm-external-item-detail')->as('whm-external-item-detail.')->group(function() {
        Route::post('data', 'WhmExternalItemDetailController@getData')->name('data');
        Route::get('/{id}', 'WhmExternalItemDetailController@show')->name('show');
    });
    Route::resource('whm-external-item-detail', 'WhmExternalItemDetailController', ['except' => ['create', 'show']]);

     Route::prefix('whm-external-serial-no-detail')->as('whm-external-serial-no-detail.')->group(function() {
        Route::post('data', 'WhmExternalSerialNoDetailController@getData')->name('data');
        Route::get('/{id}', 'WhmExternalSerialNoDetailController@show')->name('show');
    });
    Route::resource('whm-external-serial-no-detail', 'WhmExternalSerialNoDetailController', ['except' => ['create', 'show']]);

     Route::prefix('whm-internal')->as('whm-internal.')->group(function() {
        Route::post('data', 'WhmInternalController@getData')->name('data');
        Route::get('/{id}', 'WhmInternalController@show')->name('show');
        Route::get('/{form}/page', 'WhmInternalController@page')->name('page');
        Route::get('show-header/{id}', 'WhmInternalController@show_header')->name('show-header');

    });
    Route::resource('whm-internal', 'WhmInternalController', ['except' => ['create', 'show']]);

     Route::prefix('whm-internal-item-detail')->as('whm-internal-item-detail.')->group(function() {
        Route::post('data', 'WhmInternalItemDetailController@getData')->name('data');
        Route::get('/{id}', 'WhmInternalItemDetailController@show')->name('show');
    });
    Route::resource('whm-internal-item-detail', 'WhmInternalItemDetailController', ['except' => ['create', 'show']]);

     Route::prefix('whm-internal-serial-no-detail')->as('whm-internal-serial-no-detail.')->group(function() {
        Route::post('data', 'WhmInternalSerialNoDetailController@getData')->name('data');
        Route::get('/{id}', 'WhmInternalSerialNoDetailController@show')->name('show');
    });
    Route::resource('whm-internal-serial-no-detail', 'WhmInternalSerialNoDetailController', ['except' => ['create', 'show']]);

    Route::prefix('whm-pop-instaled')->as('whm-pop-instaled.')->group(function() {
        Route::post('data', 'WhmPopInstaledController@getData')->name('data');
        Route::get('/{id}', 'WhmPopInstaledController@show')->name('show');
        Route::get('/{form}/page', 'WhmPopInstaledController@page')->name('page');
        Route::get('show-header/{id}', 'WhmPopInstaledController@show_header')->name('show-header');

    });
    Route::resource('whm-pop-instaled', 'WhmPopInstaledController', ['except' => ['create', 'show']]);

    Route::prefix('whm-pop-instaled-item-detail')->as('whm-pop-instaled-item-detail.')->group(function() {
        Route::post('data', 'WhmPopInstaledItemDetailController@getData')->name('data');
        Route::get('/{id}', 'WhmPopInstaledItemDetailController@show')->name('show');
        Route::get('/{form}/page', 'WhmPopInstaledController@page')->name('page');
    });
    Route::resource('whm-pop-instaled-item-detail', 'WhmPopInstaledItemDetailController', ['except' => ['create', 'show']]);

    Route::prefix('whm-popinstalsn-detail')->as('whm-popinstalsn-detail.')->group(function() {
        Route::post('data', 'WhmPopInstaledSerialNoDetailController@getData')->name('data');
        Route::get('/{id}', 'WhmPopInstaledSerialNoDetailController@show')->name('show');
    });
    Route::resource('whm-popinstalsn-detail', 'WhmPopInstaledSerialNoDetailController', ['except' => ['create', 'show']]);

    Route::prefix('whm-pop-uninstaled')->as('whm-pop-uninstaled.')->group(function() {
        Route::post('data', 'WhmPopUninstaledController@getData')->name('data');
        Route::get('/{id}', 'WhmPopUninstaledController@show')->name('show');
        Route::get('/{form}/page', 'WhmPopUninstaledController@page')->name('page');
        Route::get('show-header/{id}', 'WhmPopUninstaledController@show_header')->name('show-header');

    });
    Route::resource('whm-pop-uninstaled', 'WhmPopUninstaledController', ['except' => ['create', 'show']]);

    Route::prefix('whm-pop-uninstaled-item-detail')->as('whm-pop-uninstaled-item-detail.')->group(function() {
        Route::post('data', 'WhmPopUninstaledItemDetailController@getData')->name('data');
        Route::get('/{id}', 'WhmPopUninstaledItemDetailController@show')->name('show');
    });
    Route::resource('whm-pop-uninstaled-item-detail', 'WhmPopUninstaledItemDetailController', ['except' => ['create', 'show']]);

    Route::prefix('whm-popuninstalsn-detail')->as('whm-popuninstalsn-detail.')->group(function() {
        Route::post('data', 'WhmPopUninstaledSerialNoDetailController@getData')->name('data');
        Route::get('/{id}', 'WhmPopUninstaledSerialNoDetailController@show')->name('show');
    });
    Route::resource('whm-popuninstalsn-detail', 'WhmPopUninstaledSerialNoDetailController', ['except' => ['create', 'show']]);

});

Route::prefix('ticketing')->namespace('Ticketing')->as('ticketing.')->group(function() {
    Route::prefix('fmi')->as('fmi.')->group(function() {
        //ticket-open
        Route::get('ticket-open/form', 'FMITicketOpenController@create')->name('ticket-open.create');
        Route::post('data-non-minor/data', 'FMITicketOpenController@getDataNonMinor')->name('data-non-minor.data');
        Route::post('data-minor/data', 'FMITicketOpenController@getDataMinor')->name('data-minor.data');


        Route::resource('ticket-open', 'FMITicketOpenController');
		Route::get('ticket-open/create/{ticketType}', 'FMITicketOpenController@create')->name('ticket-open.createType');
		Route::get('ticket-open/{ticket_open}/show', 'FMITicketOpenController@showAssigment')->name('ticket-open.show-assigment');
        Route::POST('ticket-open/{id}/tast-assignment', 'FMITicketOpenController@storeAssignment')->name('ticket-open.tast-assignment');
        Route::POST('ticket-open/tast-assignment/{id}/data', 'FMITicketOpenController@getDataAssignment')->name('ticket-open.data-assignment');

        Route::post('ticket-open/getCustomer', 'FMITicketOpenController@getCustomerByBTSOLT')->name('ticket-open.get-by-olt-bts');
        Route::get('ticket-open/getCustomer/NOC', 'FMITicketOpenController@getCustomerNoc')->name('ticket-open.get-customer-noc');
        Route::PUT('ticket-open/{id}/status', 'FMITicketOpenController@updateStatus')->name('ticket-open.status');
        Route::POST('ticket-open/status/{id}/data', 'FMITicketOpenController@getDataStatus')->name('ticket-open.data-status');

        //task-assignment
		Route::resource('task-assignment', 'FMITaskAssignmentController');
        Route::get('task-assignment/', 'FMITaskAssignmentController@index')->name('task-assignment.index');
        Route::get('task-assignment/form', 'FMITaskAssignmentController@create')->name('task-assignment.create');
        Route::post('task-assignment/data', 'FMITaskAssignmentController@getData')->name('task-assignment.data');

        //ticket-status
        Route::resource('ticket-status', 'FMITicketStatusController');
        Route::post('ticket-status/data-major/data', 'FMITicketStatusController@getData')->name('ticket-status.data');
    });

    Route::prefix('master')->as('master.')->group(function() {
        //open-bts
        Route::resource('open-bts', 'OpenBtsController');
        Route::post('open-bts/data', 'OpenBtsController@getData')->name('open-bts.data');
        //open-customer
        Route::resource('open-customer', 'OpenCustomerController');
        Route::post('open-customer/data', 'OpenCustomerController@getData')->name('open-customer.data');
        //open-device-type-fo
        Route::resource('open-device-type-fo', 'OpenDeviceTypeFoController');
        Route::post('open-device-type-fo/data', 'OpenDeviceTypeFoController@getData')->name('open-device-type-fo.data');
        //open-device-type-wll
        Route::resource('open-device-type-wll', 'OpenDeviceTypeWllController');
        Route::post('open-device-type-wll/data', 'OpenDeviceTypeWllController@getData')->name('open-device-type-wll.data');
    });
});
