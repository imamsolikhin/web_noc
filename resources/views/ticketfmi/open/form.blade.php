@extends('layout.default')

@section('content')
<div class="subheader py-2 {{ Metronic::printClasses('subheader', false) }}" id="kt_subheader">
    <div class="{{ Metronic::printClasses('subheader-container', false) }} d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
        <div class="d-flex align-items-baseline flex-wrap mr-5">
            <h5 class="text-dark font-weight-bold my-1 mr-5">
                Create Ticket                               
            </h5>
            <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                <li class="breadcrumb-item">
                    <a href="{{ route('dashboard') }}" class="text-muted">Home</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="" class="text-muted">Create Ticket</a>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="card card-custom mb-0">
    <form class="form" action="" method="POST">
        {!! csrf_field() !!}
        <div class="card-header border-1 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Create Ticket
                </h3>
            </div>
        </div>
        <div class="card-body">
            <div class="mb-1">
                <div class="form-group row mb-0">
                    <div class="col-lg-4">
                        <label>TCK No</label>
                        <input type="text" class="form-control form-control-sm" name="code" value="" />
                    </div>

                    <div class="col-lg-4">
                        <label>Transaction Date</label>
                        <input type="text" class="form-control form-control-sm" name="TransactionDate" value="{{ date('Y-m-d H:i:s') }}" />
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-lg-6">
                        <label>Ticket Category</label>
                        <div>
                            <input type="radio" name="TicketCategory" value="CRITICAL" class="ticketCategory" data-id='critical' id="critical"> <label for="critical">Critical</label>
                        </div>
                        <div>
                            <input type="radio" name="TicketCategory" value="MAJOR" class="ticketCategory" data-id='major' id="major"> <label for="major">Major</label>
                        </div>
                        <div>
                            <input type="radio" name="TicketCategory" value="MINOR" class="ticketCategory" data-id='minor' id="minor"> <label for="minor">Minor</label>
                        </div>
                        <div>
                            <input type="radio" name="TicketCategory" value="PLN" class="ticketCategory" data-id='pln' id="pln"> <label for="pln">PLN</label>
                        </div>
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-lg-6">
                        <label>Scedule Date</label>
                        <input type="text" name="SceduleDate" class="form-control form-control-sm datepicker">
                    </div>
                </div> 

                <div class="form-group row mb-0">
                    <div class="col-lg-6">
                        <label>Branch</label>
                        <select class="form-control form-control-sm" name="BranchCode">
                            @foreach($branch as $list)
                            <option value="{{$list->Code}}" >{{ $list->Name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-lg-6">
                        <label>Problem Type</label>
                        <select class="form-control form-control-sm" name="ProblemTypeCode">
                            @foreach($problem as $list)
                            <option value="{{$list->Code}}" >{{ $list->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-lg-6">
                        <label>Responsibility Department</label>
                        <select class="form-control form-control-sm" name="ResponsibilityDepartmentCode">
                            @foreach($department as $list)
                            <option value="{{$list->Code}}" >{{ $list->Name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-lg-6">
                        <label>Ref No</label>
                        <input type="text" class="form-control form-control-sm" name="RefNo">
                    </div>
                </div>
                <div class="form-group row mb-0">
                    <div class="col-lg-6">
                        <label>Ticket Description</label>
                        <textarea class="form-control form-control-sm" name="TicketDescription"></textarea>
                    </div> 

                    <div class="col-lg-6">
                        <label>Remark</label>
                        <textarea class="form-control form-control-sm" name="Remark"></textarea>
                    </div>
                </div>



            </div>
        </div>
           
        <div class="card-footer">
            <div class="row">
                <div class="col-lg-6">
                    <a href="{{ route('master.city.index') }}" id="cancel-btn" class="btn btn-danger">Cancel</a>
                </div>
                <div class="col-lg-6 text-right">
                    <button type="submit" class="btn btn-primary mr-2">Update</button>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection