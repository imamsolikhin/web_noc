@extends('layout.default')

@section('content')
    <div class="subheader py-2 {{ Metronic::printClasses('subheader', false) }}" id="kt_subheader">
        <div class="{{ Metronic::printClasses('subheader-container', false) }} d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-baseline flex-wrap mr-5">
                <h5 class="text-dark font-weight-bold my-1 mr-5">
                    TICKET
                </h5>
                <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                    <li class="breadcrumb-item">
                        <a href="{{ route('dashboard') }}" class="text-muted">Home</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="" class="text-muted">Ticket FMI</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    @include('inc.success-notif')
    <div class="card card-custom">
        <div class="card-header flex-wrap border-1 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Critical / Major / PLN
                    <div class="text-muted pt-2 font-size-sm">show data ticket</div>
                </h3>
            </div>
            <div class="card-toolbar">
                <a href="#" class="btn btn-primary py-2" id="add-btn">
                    <i class="menu-icon">
		                <span class="svg-icon svg-icon-md">
		                    <!--begin::Svg Icon | path:media/svg/icons/Design/Flatten.svg-->
		                    <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
		                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
		                            <rect x="0" y="0" width="24" height="24"/>
		                            <circle fill="#000000" cx="9" cy="15" r="6"/>
		                            <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3"/>
		                        </g>
		                    </svg>
		                    <!--end::Svg Icon-->
		                </span>
                    </i>
                    New Ticket
                </a>
            </div>
        </div>
        <div class="card-body">
	        <div class="table-responsive">
	            <table style="width: 1070px !important;" class="table table-borderless table-vertical-center" id="datatable-major"></table>
	        </div>
        </div>
    </div>


    <div class="card card-custom mt-5">
        <div class="card-header flex-wrap border-1 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Minor
                    <div class="text-muted pt-2 font-size-sm">show data ticket</div>
                </h3>
            </div>
            <div class="card-toolbar">
                <a href="#" class="btn btn-primary py-2" id="add-btn">
                    <i class="menu-icon">
                        <span class="svg-icon svg-icon-md">
                            <!--begin::Svg Icon | path:media/svg/icons/Design/Flatten.svg-->
                            <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <rect x="0" y="0" width="24" height="24"/>
                                    <circle fill="#000000" cx="9" cy="15" r="6"/>
                                    <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3"/>
                                </g>
                            </svg>
                            <!--end::Svg Icon-->
                        </span>
                    </i>
                    New Ticket
                </a>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table style="width: 1070px !important;" class="table table-borderless table-vertical-center" id="datatable-minor">
                    <thead>
                        <tr>
                           <th>TCK NO</th>
                           <th>Transaction Date</th>
                           <th>Customer Code</th>
                           <th>Customer Name</th>
                           <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <br>
@endsection

{{-- Styles Section --}}
@section('styles')
    <link rel="stylesheet" href="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.css">
    <link rel="stylesheet" href="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.min.css">
    <link rel="stylesheet" href="{{ config('app.url') }}plugins/custom/datatables/datatables.bundle.css">
@endsection

{{-- Scripts Section --}}
@section('scripts')
    @include ('inc.confirm-delete-modal')
    <script src="{{ config('app.url') }}global/vendor/datatables/jquery.dataTables.js"></script>
    <script src="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.js"></script>
    <script src="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.js"></script>


    <script type="text/javascript">

        //table 1
        var oTable;
        var page = 1;
        $(document).ready(function() {
          oTable = $('#datatable-major').dataTable({
            pageLength: 10,
            responsive: false,
            order: [[ 0, "asc" ]],

            processing: true,
            serverSide: true,
            language: {
              paginate: {
                previous: "<i class='fas fa-angle-left'>",
                next: "<i class='fas fa-angle-right'>"
              }
            },
            ajax: {
              url: "{{ route('fmi.data-major') }}",
              dataType: "json",
              type: "GET",
              data: function ( d ) {
                oSearch = {};
                $('.filter-field').each( function () {
                  key = $(this).attr('name');
                  val = $(this).val();
                  oSearch[key] = val;
                });
                return $.extend(false, {page : page}, oSearch, d);
              }
            },
            preDrawCallback: function( settings ) {
              var api = this.api();
              page = parseInt(api.rows().page()) + 1;
            },
            columns: [
              {data : 'code'},
              {data : 'TransactionDate'},
              {data : 'TicketCategory'},
              {data : 'action'}
            ],
          });

          $('.filter-btn').on('click', function(){
            oTable.api().draw();
          });

          $('.filter-clean').on('click', function(){
            $('.filter-field').val('');
            oTable.api().draw();
          });
        });


        //table 2

        var oTable2;
        var page2 = 1;
        $(document).ready(function() {
          oTable2 = $('#datatable-minor').dataTable({
            pageLength: 10,
            responsive: false,
            order: [[ 0, "asc" ]],

            processing: true,
            serverSide: true,
            language: {
              paginate: {
                previous: "<i class='fas fa-angle-left'>",
                next: "<i class='fas fa-angle-right'>"
              }
            },
            ajax: {
              url: "{{ route('fmi.data-minor') }}",
              dataType: "json",
              type: "GET",
              data: function ( d ) {
                oSearch = {};
                $('.filter-field').each( function () {
                  key = $(this).attr('name');
                  val = $(this).val();
                  oSearch[key] = val;
                });
                return $.extend(false, {page : page2}, oSearch, d);
              }
            },
            preDrawCallback: function( settings ) {
              var api = this.api();
              page = parseInt(api.rows().page()) + 1;
            },
            columns: [
              {data : 'code'},
              {data : 'TransactionDate'},
              {data : 'customer_code'},
              {data : 'customer_name'},
              {data : 'action'}
            ],
          });

          $('.filter-btn').on('click', function(){
            oTable.api().draw();
          });

          $('.filter-clean').on('click', function(){
            $('.filter-field').val('');
            oTable.api().draw();
          });
        });



    </script>
@endsection
