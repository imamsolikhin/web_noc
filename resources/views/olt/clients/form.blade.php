@extends('layout.default')

@section('content')
<div class="subheader py-2 {{ Metronic::printClasses('subheader', false) }}" id="kt_subheader">
  <div class="{{ Metronic::printClasses('subheader-container', false) }} d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
    <div class="d-flex align-items-baseline flex-wrap mr-5">
      <h5 class="text-dark font-weight-bold my-1 mr-5">
        Clients
      </h5>
      <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
        <li class="breadcrumb-item">
          <a href="{{ route('dashboard') }}" class="text-muted">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
          <a href="{{ route('olt.clients.index') }}" class="text-muted">Clients</a>
        </li>
        <li class="breadcrumb-item">
          <a href="#form" class="text-muted">Form Input</a>
        </li>
      </ul>
    </div>
  </div>
</div>

@include('inc.error-list')
@include('inc.success-notif')
<div class="card card-custom">
  <div class="card-header bg-danger flex-wrap border-1 pt-1 pb-0" style="min-height: 0;">
    <div class="card-title pt-1 pb-0">
      <h3 class="card-label font-weight-bolder text-white">Clients
      </h3>
    </div>
  </div>
  <!-- <div class="card-body pt-1"> -->
    <form class="form pt-0" id="form-input" action="@isset ($clients->Code) {{ route('olt.clients.update',$clients->Code) }} @endisset" method="POST">
        {!! csrf_field() !!}
        {!! method_field('PUT') !!}
        <div class="card-body pt-0 pl-3 pr-3">
            <div class="row">
                <div class="col-lg-8">
                    <div class="card card-custom mb-1 col-lg-12 ">
                        <div class="card-body pt-1 pr-0 pl-0">
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">Code</label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control form-control-sm" id="Code" name="Code" placeholder="Enter Code" value="@isset ($clients->Code) {{ $clients->Code }} @endisset" readonly/>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">Isp Code</label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control form-control-sm" id="IspCode" name="IspCode" placeholder="Enter Isp Code" value="@isset ($clients->IspCode) {{ $clients->IspCode }} @endisset" readonly/>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">Isp Name</label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control form-control-sm" id="IspName" name="IspName" placeholder="Enter Isp Contact Person" value="@isset ($clients->IspName) {{ $clients->IspName }} @endisset" readonly/>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">Isp Address</label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control form-control-sm" id="IspAddress" name="IspAddress" placeholder="Enter Isp Address" value="@isset ($clients->IspAddress) {{ $clients->IspAddress }} @endisset" readonly/>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">Customer Code</label>
                                    <div class="col-lg-8">
                                      <input type="text" class="form-control form-control-sm" id="CustomerCode" name="CustomerCode" placeholder="Enter Customer Code" value="@isset ($clients->CustomerCode) {{ $clients->CustomerCode }} @endisset" readonly/>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">Customer Name</label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control form-control-sm"  id="CustomerName" name="CustomerName" placeholder="Enter Customer Name" value="@isset ($clients->CustomerName) {{ $clients->CustomerName }} @endisset" readonly/>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">Contact Person</label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control form-control-sm" id="CustomerContactPerson" name="CustomerContactPerson"  placeholder="Enter Contact Person" value="@isset ($clients->CustomerContactPerson) {{ $clients->CustomerContactPerson }} @endisset" readonly/>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">Address</label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control form-control-sm" id="CustomerAddress" name="CustomerAddress"  placeholder="Enter Address" value="@isset ($clients->CustomerAddress) {{ $clients->CustomerAddress }} @endisset" readonly/>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">City</label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control form-control-sm" id="CustomerCity" name="CustomerCity" placeholder="Enter City" value="@isset ($clients->CustomerCity) {{ $clients->CustomerCity }} @endisset" readonly/>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">Phone 1</label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control form-control-sm" id="CustomerPhone1" name="CustomerPhone1" placeholder="Enter Phone 1" value="@isset ($clients->CustomerPhone1) {{ $clients->CustomerPhone1 }} @endisset" readonly/>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">Fax</label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control form-control-sm" id="CustomerFax" name="CustomerFax" placeholder="Enter Fax" value="@isset ($clients->CustomerFax) {{ $clients->CustomerFax }} @endisset" readonly/>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">Email</label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control form-control-sm" id="CustomerEmail" name="CustomerEmail" placeholder="Enter Email" value="@isset ($clients->CustomerEmail) {{ $clients->CustomerEmail }} @endisset" readonly/>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">Product Grup</label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control form-control-sm" id="CustomerProductGroup" name="CustomerProductGrup" placeholder="Enter Product Grup" value="@isset ($clients->CustomerProductGrup) {{ $clients->CustomerProductGrup }} @endisset" readonly/>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">Product Port</label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control form-control-sm" id="CustomerProductPort" name="ProductPort" placeholder="Enter Product Port" value="@isset ($clients->CustomerProductPort) {{ $clients->CustomerProductPort }} @endisset" readonly/>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">Product Category</label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control form-control-sm" id="CustomerProductCategory" name="ProductCategory" placeholder="Enter Product Category" value="@isset ($clients->CustomerProductCategory) {{ $clients->CustomerProductCategory }} @endisset" readonly/>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">Product Name</label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control form-control-sm" id="CustomerProductName" name="ProductName" placeholder="Enter Product Name" value="@isset ($clients->CustomerProductName) {{ $clients->CustomerProductName }} @endisset" readonly/>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-2">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">Description</label>
                                    <div class="col-lg-8">
                                        <textarea type="text" class="form-control form-control-sm" id="CustomerProductNotes" name="CustomerProductNotes" placeholder="Enter Description" value="@isset ($clients->CustomerProductNotes) {{ ($clients->CustomerProductNotes) }} @endisset" readonly>@isset ($clients->CustomerProductNotes) {{ $clients->CustomerProductNotes }} @endisset</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">History Ont</label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control form-control-sm" placeholder="Enter History" value="@isset ($clients->HistoryOntSn) {{ $clients->HistoryOntSn }} @endisset" readonly/>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">History Status</label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control form-control-sm" placeholder="Enter History" value="@isset ($clients->HistoryDate) {{ $clients->HistoryDate }} @endisset @isset ($clients->HistoryStatus) {{ $clients->HistoryStatus }} @endisset" readonly/>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">History Note</label>
                                    <div class="col-lg-8">
                                        <textarea type="text" class="form-control form-control-sm" value="@isset ($clients->HistoryRemark) {{ ($clients->HistoryRemark) }} @endisset" readonly>@isset ($clients->HistoryRemark) {{ $clients->HistoryRemark }} @endisset</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="py-1">
                              <div class="form-group row mb-0 pr-50">
                                  <h5 class="col-lg-4 font-weight-bolder"></h5>
                                  <div class="col-lg-8 col-form-label">
                                      <button type="submit" class="btn btn-success font-weight-bold">Save</button>
                                      <a type="button" class="btn btn-danger" href="{{ route('olt.clients.index') }}">Cancel</a>
                                  </div>
                              </div>
                            </div>
                            <div class="py-1">
                              <div class="card-custom mb-1">
                                  <div class="card-header border-0 pt-1 pb-1 pr-0 pl-0">
                                      <div class="card-toolbar float-left">
                                          <h3 class="card-title font-weight-bolder text-dark">Vlan Port Service</h3>
                                      </div>
                                      <div class="card-toolbar float-right">
                                          <a type="button" onclick="addService()" class="btn btn-warning btn-sm"><i class="fas fa-plus"></i> Add</a>
                                      </div>
                                    </div>
                                  <div class="card-body pr-0 pl-0 pt-1 pb-1 pr-0 pl-0">
                                    <table id="datatable" class="table dataTable table-bordered table-hover w100" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th class="text-center text-white dt-body-nowrap">Idx</th>
                                                <th class="text-center text-white dt-body-nowrap">Vlan</th>
                                                <th class="text-center text-white dt-body-nowrap">Gem</th>
                                                <th class="text-center text-white dt-body-nowrap">User-VLAN</th>
                                                <th class="text-center text-white dt-body-nowrap" style="width:15rem;">Tag-Transform</th>
                                                <th class="text-center text-white dt-body-nowrap">Inner-VLAN</th>
                                                <th class="text-center text-white dt-body-nowrap">Traffic</th>
                                                <th class="text-center text-white dt-body-nowrap">Act</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                         @isset ($services_list)
                                           @foreach($services_list as $services)
                                             <tr>
                                                <td><input style="padding-right:1px;padding-left:1px; text-align:center;" name="s_idx[]" class="form-input col-12" value="{{ $services->ServicePortId }}"></td>
                                                <td><input style="padding-right:1px;padding-left:1px; text-align:center;" name="s_vlan[]" class="form-input col-12" value="{{ $services->Vlan }}"></td>
                                                <td><input style="padding-right:1px;padding-left:1px; text-align:center;" name="s_gemport[]" class="form-input col-12" value="{{ $services->GemPort }}"></td>
                                                <td><input style="padding-right:1px;padding-left:1px; text-align:center;" name="s_user[]" class="form-input col-12" value="{{ $services->UserVlan }}"></td>
                                                <td>
                                                  <select name="s_tag[]" class="col-12" value="{{ $services->TagTransform }}">
                                                    @if ($services->TagTransform === "translate-and-add")
                                                      <option value="default">default</option>
                                                      <option value="translate">translate</option>
                                                      <option value="translate-and-add" selected>translate-and-add</option>
                                                    @elseif ($services->TagTransform === "translate")
                                                      <option value="default">default</option>
                                                      <option value="translate" selected>translate</option>
                                                      <option value="translate-and-add">translate-and-add</option>
                                                    @else
                                                      <option value="default" selected>default</option>
                                                      <option value="translate">translate</option>
                                                      <option value="translate-and-add" >translate-and-add</option>
                                                    @endif
                                                  </select>
                                                </td>
                                                <td><input style="padding-right:1px;padding-left:1px; text-align:center;" name="s_inner[]" class="form-input col-12" value="{{ $services->InnerVlan }}"></td>
                                                <td><input style="padding-right:1px;padding-left:1px; text-align:center;" name="s_traffic[]" class="form-input col-12" value="{{ $services->TrafficTable }}"></td>
                                                <td><a type="button" value="del" onclick="deleteRow(this)" class="pt-2"><i class="text-center text-danger fas fa-backspace"></i><a/></td>
                                            </tr>
                                         @endforeach
                                       @endisset
                                        </tbody>
                                    </table>
                                  </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="card-custom mb-1">
                        <div class="card card-custom">
                            <div class="card-header border-0">
                                <!-- <h3 class="card-title font-weight-bolder text-dark">Template</h3> -->
                                <div class="card-toolbar col-8">
                                    <select class="form-control form-control-sm" id="TemplateCode" tabindex="-1" aria-hidden="true" style="width:100%">
                                        <option value="" selected>Chose Template</option>
                                         @isset ($template_list)
                                           @foreach($template_list as $template)
                                            <option value="{{ $template->Code }}">{{ $template->Name }}</option>
                                           @endforeach
                                         @endisset
                                    </select>
                                </div>
                                <div class="card-toolbar">
                                    <div class="dropdown dropdown-inline">
                                        <button type="button" class="btn btn-tool btn-lg" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="la la-download text-white"></i> Tools
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                                            <ul class="navi flex-column navi-hover py-2" id="sample_3_tools">
                                                <li class="navi-header font-weight-bolder text-uppercase font-size-xs text-primary pb-2">
                                                    Export Tools
                                                </li>
                                                <li class="navi-item">
                                                    <a href="javascript:;" data-action="0" class="navi-link tool-action" id="btn-auto-find">
                                                        <span class="navi-icon"><i class="fa fa-search"></i></span>
                                                        <span class="navi-text">Auto Find SN</span>
                                                    </a>
                                                </li>
                                                <li class="navi-item">
                                                    <a href="javascript:;" data-action="0" class="navi-link tool-action" id="btn-find-by-host">
                                                        <span class="navi-icon"><i class="fa fa-search"></i></span>
                                                        <span class="navi-text">Find By Host</span>
                                                    </a>
                                                </li>
                                                <li class="navi-item">
                                                    <a href="javascript:;" data-action="0" class="navi-link tool-action" id="btn-check-power">
                                                        <span class="navi-icon"><i class="fa fa-search"></i></span>
                                                        <span class="navi-text">Check Power</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body pt-0" id="ont-group">
                                <div class="mb-1">
                                    <div class="form-group row mb-0">
                                        <label class="col-lg-4 col-form-label">Find SN ONT</label>
                                        <div class="col-lg-8">
                                            <select class="form-control form-control-sm" id="ListOnt" tabindex="-1" aria-hidden="true" style="width:100%">
                                                 @isset ($ont_list)
                                                   @foreach($ont_list as $ont)
                                                    <option value="{{ $ont->Code }}" @isset ($clients->OntSn) {{ ($clients->OntSn == $ont->Code) ? 'true' : 'false' }} @endisset > {{ $ont->Sn }}-{{ $ont->Hostname }}</option>
                                                   @endforeach
                                                   <option value="" selected>All Ont {{count($ont_list)}}</option>
                                                 @endisset
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-1">
                                    <div class="form-group row mb-0">
                                        <label class="col-lg-4 col-form-label">SN Ont</label>
                                        <div class="col-lg-8">
                                            <input type="text" class="form-control form-control-sm" id="Sn" name="Sn" placeholder="Enter SN" value="@isset ($clients->OntSn) {{ $clients->OntSn }} @endisset" readonly/>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-1">
                                    <div class="form-group row mb-0">
                                        <label class="col-lg-4 col-form-label">SN Ont ID</label>
                                        <div class="col-lg-8">
                                            <input type="text" class="form-control form-control-sm" id="OntId" name="OntId" placeholder="Enter SN ID" value="@isset ($clients->OntId) {{ $clients->OntId }} @endisset"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-1" hidden>
                                    <div class="form-group row mb-0">
                                        <label class="col-lg-4 col-form-label">OLT Host</label>
                                        <div class="col-lg-8">
                                            <input type="text" class="form-control form-control-sm" id="HostCode" name="HostCode" placeholder="Enter Host" value="@isset ($clients->HostCode) {{ $clients->HostCode }} @endisset" readonly/>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-1">
                                    <div class="form-group row mb-0">
                                        <label class="col-lg-4 col-form-label">Hostname</label>
                                        <div class="col-lg-8">
                                            <input type="text" class="form-control form-control-sm" id="Hostname" name="Hostname" placeholder="Enter Hostname" value="@isset ($clients->Hostname) {{ $clients->Hostname }} @endisset" readonly/>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-1">
                                    <div class="form-group row mb-0">
                                        <label class="col-lg-4 col-form-label">IP Host</label>
                                        <div class="col-lg-8">
                                            <input type="text" class="form-control form-control-sm" id="IpAddress" name="IpAddress" placeholder="Enter Ip Address" value="@isset ($clients->IpAddress) {{ $clients->IpAddress }} @endisset" readonly/>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-1">
                                    <div class="form-group row mb-0">
                                        <label class="col-lg-4 col-form-label">Sysname Host</label>
                                        <div class="col-lg-8">
                                            <input type="text" class="form-control form-control-sm" id="Sysname" name="Sysname" placeholder="Enter Sysname" value="@isset ($clients->Sysname) {{ $clients->Sysname }} @endisset" readonly/>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-1">
                                    <div class="form-group row mb-0">
                                        <label class="col-lg-4 col-form-label">Frame ID</label>
                                        <div class="col-lg-8">
                                            <input type="text" class="form-control form-control-sm"  id="FrameId" name="FrameId" placeholder="Enter Frame ID" value="@isset ($clients->FrameId) {{ $clients->FrameId }} @endisset" readonly/>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-1">
                                    <div class="form-group row mb-0">
                                        <label class="col-lg-4 col-form-label">Slot Id</label>
                                        <div class="col-lg-8">
                                            <input type="text" class="form-control form-control-sm"  id="SlotId" name="SlotId" placeholder="Enter SlOT ID" value="@isset ($clients->SlotId) {{ $clients->SlotId }} @endisset" readonly/>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-1">
                                    <div class="form-group row mb-0">
                                        <label class="col-lg-4 col-form-label">Port Id</label>
                                        <div class="col-lg-8">
                                            <input type="text" class="form-control form-control-sm" id="PortId" name="PortId" placeholder="Enter Port ID" value="@isset ($clients->PortId) {{ $clients->PortId }} @endisset" readonly/>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-1">
                                    <div class="form-group row mb-0">
                                        <label class="col-lg-4 col-form-label">Ont Version</label>
                                        <div class="col-lg-8">
                                            <input type="text" class="form-control form-control-sm" id="OntVersion" name="OntVersion" placeholder="Enter Ont Version" value="@isset ($clients->OntVersion) {{ $clients->OntVersion }} @endisset" readonly/>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-1">
                                    <div class="form-group row mb-0">
                                        <label class="col-lg-4 col-form-label">Ont Software</label>
                                        <div class="col-lg-8">
                                            <input type="text" class="form-control form-control-sm" id="OntSoftware" name="OntSoftware" placeholder="Enter Ont Software" value="@isset ($clients->OntSoftware) {{ $clients->OntSoftware }} @endisset" readonly/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-custom mb-1">
                        <div class="card card-custom">
                            <div class="card-header border-0">
                                <h3 class="card-title font-weight-bolder text-dark">Native Vlan</h3>
                            </div>
                            <div class="card-body pt-0" id="TemplateCode">
                                <div class="mb-1">
                                    <div class="form-group row mb-0">
                                        <label class="col-lg-4 col-form-label">Vlan Downlink</label>
                                        <div class="col-lg-8">
                                            <input type="text" class="form-control form-control-sm" id="VlanDownLink" name="VlanDownLink" placeholder="Enter Vlan Downlink" value="@isset ($clients->VlanDownLink) {{ $clients->VlanDownLink }} @endisset"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-1">
                                    <div class="form-group row mb-0">
                                        <label class="col-lg-4 col-form-label">Vlan Port 1</label>
                                        <div class="col-lg-8">
                                            <input type="text" class="form-control form-control-sm"  id="NativeVlanEth1" name="NativeVlanEth1" placeholder="Enter Vlan Port 1"  value="@isset ($clients->NativeVlanEth1) {{ $clients->NativeVlanEth1 }} @endisset"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-1">
                                    <div class="form-group row mb-0">
                                        <label class="col-lg-4 col-form-label">Vlan Port 2</label>
                                        <div class="col-lg-8">
                                            <input type="text" class="form-control form-control-sm" id="NativeVlanEth2" name="NativeVlanEth2" placeholder="Enter Vlan Port 2"  value="@isset ($clients->NativeVlanEth2) {{ $clients->NativeVlanEth2 }} @endisset"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-1">
                                    <div class="form-group row mb-0">
                                        <label class="col-lg-4 col-form-label">Vlan Port 3</label>
                                        <div class="col-lg-8">
                                            <input type="text" class="form-control form-control-sm" id="NativeVlanEth3" name="NativeVlanEth3" placeholder="Enter Vlan Port 3"  value="@isset ($clients->NativeVlanEth3) {{ $clients->NativeVlanEth3 }} @endisset"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-1">
                                    <div class="form-group row mb-0">
                                        <label class="col-lg-4 col-form-label">Vlan Port 4</label>
                                        <div class="col-lg-8">
                                            <input type="text" class="form-control form-control-sm" id="NativeVlanEth4" name="NativeVlanEth4" placeholder="Enter Vlan Port 4"  value="@isset ($clients->NativeVlanEth4) {{ $clients->NativeVlanEth4 }} @endisset"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-custom mb-1">
                        <div class="card card-custom">
                            <div class="card-header border-0">
                                <h3 class="card-title font-weight-bolder text-dark">Network Info</h3>
                            </div>
                            <div class="card-body pt-1">
                                <div class="mb-1"  id="network-service">
                                    <div class="form-group row mb-0">
                                        <label class="col-lg-4 col-form-label">Srv Profile</label>
                                        <div class="col-lg-3">
                                            <input type="text" class="form-control form-control-sm" id="OntSrvProfileId" name="OntSrvProfileId" placeholder="Enter Srv Profile"  value="@isset ($clients->OntSrvProfileId) {{ $clients->OntSrvProfileId }} @endisset"/>
                                        </div>
                                        <div class="col-lg-5">
                                            <select class="form-control form-control-sm" id="OntServiceProfile" tabindex="-1" aria-hidden="true" style="width:100%">
                                                  <option value="" title="new profile">New</option>
                                                 @isset ($serviceprofile_list)
                                                   @foreach($serviceprofile_list as $serviceprofile)
                                                    <option value="{{ $serviceprofile->ProfileId }}" title="{{ $serviceprofile->ProfileName }}">{{ $serviceprofile->ProfileId }}</option>
                                                   @endforeach
                                                 @endisset
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-1"  id="network-line">
                                    <div class="form-group row mb-0">
                                        <label class="col-lg-4 col-form-label">Line Profile</label>
                                        <div class="col-lg-3">
                                            <input type="text" class="form-control form-control-sm" id="OntLineProfileId" name="OntLineProfileId" placeholder="Enter Line Profile"  value="@isset ($clients->OntLineProfileId) {{ $clients->OntLineProfileId }} @endisset" />
                                        </div>
                                        <div class="col-lg-5">
                                            <select class="form-control form-control-sm" id="OntLineProfile" tabindex="-1" aria-hidden="true" style="width:100%">
                                            <option value="" title="new profile">New</option>
                                                 @isset ($lineprofile_list)
                                                   @foreach($lineprofile_list as $lineprofile)
                                                    <option value="{{ $lineprofile->ProfileId }}" title="{{ $lineprofile->ProfileName }}">{{ $lineprofile->ProfileId }}</option>
                                                   @endforeach
                                                 @endisset
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-1">
                                    <div class="form-group row mb-0">
                                        <label class="col-lg-4 col-form-label">FDT</label>
                                        <div class="col-lg-8">
                                            <select class="form-control form-control-sm" id="FdtNo" name="FdtNo" tabindex="-1" aria-hidden="true" style="width:100%">
                                                  <option value="">New</option>
                                                 @isset ($Fat_list)
                                                   @foreach($Fat_list as $Fat)
                                                    <option value="{{ $Fat->Code }}">{{ $Fat->Name }}</option>
                                                   @endforeach
                                                 @endisset
                                                 <option value="FAT-001">FAT-001</option>
                                                 <option value="FAT-002">FAT-002</option>
                                                 <option value="FAT-003">FAT-003</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-1">
                                    <div class="form-group row mb-0">
                                        <label class="col-lg-4 col-form-label">FAT</label>
                                        <div class="col-lg-8">
                                            <input type="text" class="form-control form-control-sm" id="FatNo" name="FatNo" placeholder="Enter FTD No"  value="@isset ($clients->FatNo) {{ $clients->FatNo }} @endisset"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-1">
                                    <div class="form-group row mb-0">
                                        <label class="col-lg-4 col-form-label">Board Vlan</label>
                                        <div class="col-lg-7">
                                            <div class="mb-1">
                                                <div class="form-group row mb-0">
                                                    <label class="col-lg-3 col-form-label">F</label>
                                                    <div class="input-group col-lg-9">
                                                        <input type="text" class="form-control form-control-sm" id="VlanFrameId" name="VlanFrameId" placeholder="Frame" value="@isset ($clients->VlanFrameId) {{ $clients->VlanFrameId }} @endisset" readonly/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mb-1">
                                                <div class="form-group row mb-0">
                                                    <label class="col-lg-3 col-form-label">S</label>
                                                    <div class="input-group col-lg-9">
                                                        <input type="text" class="form-control form-control-sm"  id="VlanSlotId" name="VlanSlotId" placeholder="Slot" value="@isset ($clients->VlanSlotId) {{ $clients->VlanSlotId }} @endisset" readonly/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mb-1">
                                                <div class="form-group row mb-0">
                                                    <label class="col-lg-3 col-form-label">P</label>
                                                    <div class="input-group col-lg-9">
                                                        <input type="text" class="form-control form-control-sm" id="VlanPortId" name="VlanPortId" placeholder="Port" value="@isset ($clients->VlanPortId) {{ $clients->VlanPortId }} @endisset" readonly/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-1">
                                    <div class="form-group row mb-0">
                                        <label class="col-lg-4 col-form-label">Vlan Attribut</label>
                                        <div class=" col-lg-8 col-md-8 col-sm-8">
                                            <div class="radio-inline">
                                              @if (isset($clients->VlanAttribut))
                                                @if ($clients->VlanAttribut === "Stacking")
                                                  <label class="radio"><input type="radio" name="VlanAttribut" value="Standard" /><span></span>Stand</label>
                                                  <label class="radio"><input type="radio" name="VlanAttribut" value="Stacking" checked="checked" /><span></span>Stack</label>
                                                  <label class="radio"><input type="radio" name="VlanAttribut" value="QinQ"/><span></span>QinQ</label>
                                                @elseif ($clients->VlanAttribut === "QinQ")
                                                  <label class="radio"><input type="radio" name="VlanAttribut" value="Standard" /><span></span>Stand</label>
                                                  <label class="radio"><input type="radio" name="VlanAttribut" value="Stacking"/><span></span>Stack</label>
                                                  <label class="radio"><input type="radio" name="VlanAttribut" value="QinQ" checked="checked" /><span></span>QinQ</label>
                                                @else
                                                    <label class="radio"><input type="radio" name="VlanAttribut" value="Standard" checked="checked" /><span></span>Stand</label>
                                                    <label class="radio"><input type="radio" name="VlanAttribut" value="Stacking"/><span></span>Stack</label>
                                                    <label class="radio"><input type="radio" name="VlanAttribut" value="QinQ"/><span></span>QinQ</label>
                                                @endif
                                              @else
                                                  <label class="radio"><input type="radio" name="VlanAttribut" value="Standard" checked="checked" /><span></span>Stand</label>
                                                  <label class="radio"><input type="radio" name="VlanAttribut" value="Stacking"/><span></span>Stack</label>
                                                  <label class="radio"><input type="radio" name="VlanAttribut" value="QinQ"/><span></span>QinQ</label>
                                              @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<!-- Modal-->
<div class="modal fade" id="modal-find-by-host" data-backdrop="static"
	tabindex="-1" role="dialog" aria-labelledby="staticBackdrop"
	aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-danger pt-3 pb-3">
				<h5 class="modal-title text-white bold" id="modal">Select By Host</h5>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<i aria-hidden="true" class="ki ki-close"></i>
				</button>
			</div>
			<form class="form" action="{{ route('olt.host.store') }}" method="POST">
				<div class="card-body pt-3">
          <div class="mb-2">
            <div class="form-group row mb-0">
              <label class="col-lg-4 col-form-label">Host Code</label>
              <div class="col-lg-8">
                <select class="form-control form-control-sm" id="SerachHostCode" name="SerachHostCode" style="width: 100%" required>
                  <option value="" selected>Chose</option>
                  @isset ($host_list)
                    @foreach($host_list as $host)
                      <option value="{{ $host->Code }}">{{$host->Hostname }}</option>
                    @endforeach
                  @endisset
                </select>
              </div>
            </div>
          </div>
					<div class="mb-2">
						<div class="form-group row mb-0">
							<label class="col-lg-4 col-form-label"></label>
							<div class="col-lg-8">
								<button type="button" id="find-by-host" class="btn btn-info font-weight-bold">Find ONT IDLE</button>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

@endsection

{{-- Styles Section --}}
@section('styles')
<link rel="stylesheet" href="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.css">
<link rel="stylesheet" href="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.min.css">
<link rel="stylesheet" href="{{ config('app.url') }}plugins/custom/datatables/datatables.bundle.css">
<link rel="stylesheet" href="{{ config('app.url') }}css/inject.css">
<style>
.table thead th, .table thead td{
  padding-top: .25rem;
  padding-bottom: .25rem;
}
</style>
@endsection


@section('scripts')
@include ('inc.confirm-delete-modal')
<script src="{{ config('app.url') }}global/vendor/datatables/jquery.dataTables.js"></script>
<script src="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.js"></script>
<script src="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
      $('[data-switch=true]').bootstrapSwitch();
      $("#OntServiceProfile").select2({
        templateResult: formatOption
      });
      $("#OntLineProfile").select2({
        templateResult: formatOption
      });
      $("#FdtNo").select2();
      $("#ListOnt").select2();
      $("#SerachHostCode").select2();
      $("#TemplateCode").select2();
      @isset ($clients->OntLineProfileId)
        $('#OntLineProfile').val("{{ $clients->OntLineProfileId }}").trigger('change');
        $('#OntLineProfileId').val("{{ $clients->OntLineProfileId }}");
      @endisset
      @isset ($clients->OntSrvProfileId)
        $('#OntServiceProfile').val("{{ $clients->OntSrvProfileId }}").trigger('change');
        $('#OntSrvProfileId').val("{{ $clients->OntSrvProfileId }}");
      @endisset
      @isset ($clients->FdtNo)
        $('#FdtNo').val("{{ $clients->FdtNo }}").trigger('change');
      @endisset
    });

    function copyText(id){
       var el = document.getElementById(id);
       el.html = el.text;
       el.select();
       try {
          var successful = document.execCommand('copy');
          var msg = successful ? 'successful' : 'unsuccessful';
          console.log('Copying text command was ' + msg);
        } catch (err) {
          console.log('Oops, unable to copy');
        }
    }


    $('#btn-auto-find').on( 'click', function () {
      $("#ont-group").loading("toggle");
      $.ajax({
          url: "{{ route('telnet.olt-config.autofindall')}}",
          type: "GET",
          success: function (datas) {
            $('#ListOnt').empty();
            if(datas.data != null){
              for (var i in datas.data){
                $('#ListOnt').append('<option value="'+datas.data[i].Code +'">'+datas.data[i].Sn+'-'+datas.data[i].Hostname+'-'+datas.data[i].IpAddress+'</option>');
              }
              $('#ListOnt').append('<option value="" selected>All Ont '+(Number(i)+1)+'</option>');
            }else{
              $('#ListOnt').append('<option value="" selected>  Ont Not Found</option>');
            }
            $('#ListOnt').val("").trigger('change');
            $("#ont-group").loading("stop");
          },
          error: function (xhr, status, error) {
             $("#ont-group").loading("stop");
            // showDialog.show(xhr.status + " " + status + " " + error, false);
          }
      });
    });

    $('#find-by-host').on( 'click', function () {
      $("#ont-group").loading("toggle");
      $('#modal-find-by-host').modal('hide');
      var host = $("#SerachHostCode").val();
      $.ajax({
          url: "{{ route('telnet.olt-config.check-config',['',''])}}/"+host+"/ont-autofind",
          type: "GET",
          success: function (response) {
            $('#ListOnt').empty();
            if(response.data["ont-autofind"]){
              for (var i in response.data["ont-autofind"]){
                  var data = response.data["ont-autofind"][i];
                  $('#ListOnt').append('<option value="'+data.Code +'">'+data.Sn+'-'+data.Hostname+'</option>');
              }
              $('#ListOnt').append('<option value="" selected>All Ont '+(Number(i)+1)+'</option>');
            }else{
              $('#ListOnt').append('<option value="" selected>  Ont Not Found</option>');
            }
            $('#ListOnt').val("").trigger('change');
            $("#ont-group").loading("stop");
          },
          error: function (xhr, status, error) {
             $("#ont-group").loading("stop");
          }
      });
    });

    $('#btn-find-by-host').on( 'click', function () {
      $('#modal-find-by-host').modal('show');
    });

    $('#btn-check-power').on( 'click', function () {
      if($("#Sn").val().trim()){
        $("#ont-group").loading("toggle");
        $.ajax({
            url: "{{ route('telnet.olt-config.checkpower',['',''])}}/"+$("#SerachHostCode").val().trim()+"/"+$("#Sn").val().trim(),
            type: "GET",
            success: function (datas) {
               $("#ont-group").loading("stop");
               showDialog.show(datas.data, false);
            },
            error: function (xhr, status, error) {
               $("#ont-group").loading("stop");
               // showDialog.show(xhr.status + " " + status + " " + error, false);
            }
        });
      }else{
        showDialog.show("Please chose SN!!", false);
      }
    });

  $('#ListOnt').on('change', function() {
    if(this.value!=""){
      $.ajax({
          url: "{{ route('olt.ont.store')}}/" + this.value,
          type: "GET",
          headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
          },
          success: function (response) {
              $('#Sn').val(response.data.Sn);
              $('#OntId').val("");
              $('#HostCode').val(response.data.HostCode);
              $('#Hostname').val(response.data.Hostname);
              $('#IpAddress').val(response.data.IpAddress);
              $('#Sysname').val(response.data.Sysname);
              $('#FrameId').val(response.data.FrameId);
              $('#SlotId').val(response.data.SlotId);
              $('#PortId').val(response.data.PortId);
              $('#OntVersion').val(response.data.Version);
              $('#OntSoftware').val(response.data.SoftwareVersion);
              $('#VlanFrameId').val(response.data.BoardFrameId);
              $('#VlanSlotId').val(response.data.BoardSlotId);
              $('#VlanPortId').val(response.data.BoardPortId);
              if (response.data.HistoryRemark){
                var text = '<h1 style="font-weight:900;color:red;">WARNING!!!<h1>';
                text += '<h2 font-weight:800;>ONT SN : <b style="color:orange;">'+response.data.Sn+'</b><br>';
                text += 'History Date : <b style="color:orange;">'+format_date(response.data.HistoryDate)+'</b><br>';
                text += 'History Note : <b style="color:orange;">'+response.data.HistoryRemark+'</b><br></h2>';
                showDialog.show(text, false);
              }

              $("#network-service").loading("start");
              $.ajax({
                  url: "{{ route('telnet.olt-config.check-config',['',''])}}/"+response.data.HostCode+"/line-profile",
                  type: "GET",
                  success: function (response) {
                    $('#OntLineProfile').empty();
                    $('#OntLineProfile').append('<option value="">New</option>');
                    if(response.data["line-profile"]){
            					for (var i in response.data["line-profile"]){
                          var data = response.data["line-profile"][i];
                          $('#OntLineProfile').append('<option value="'+data.ProfileId +'" title="'+data.ProfileName+'">'+data.ProfileId+'</option>');
            					}
                      var dt = $('#OntLineProfileId').val();
                      setTimeout(function(){
                        $('#OntLineProfile').val($('#OntLineProfileId').val()).trigger('change');
                        setTimeout(function(){
                          $('#OntLineProfileId').val(dt);
                        }, 115);
                      }, 115);
            				}
                   $("#network-service").loading("stop");
                  },
                  error: function (xhr, status, error) {
                     $("#network-service").loading("stop");
                    // showDialog.show(xhr.status + " " + status + " " + error, false);
                  }
              });
              $("#network-line").loading("start");
              $.ajax({
                  url: "{{ route('telnet.olt-config.check-config',['',''])}}/"+response.data.HostCode+"/srv-profile",
                  type: "GET",
                  success: function (response) {
                    $('#OntServiceProfile').empty();
                    $('#OntServiceProfile').append('<option value="">New</option>');
                    if(response.data["srv-profile"]){
            					for (var i in response.data["srv-profile"]){
                          var data = response.data["srv-profile"][i];
                          $('#OntServiceProfile').append('<option value="'+data.ProfileId +'" title="'+data.ProfileName+'">'+data.ProfileId+'</option>');
            					}
                      var dt = $('#OntServiceProfileId').val();
                      setTimeout(function(){
                        $('#OntServiceProfile').val($('#OntServiceProfileId').val()).trigger('change');
                        setTimeout(function(){
                          $('#OntServiceProfileId').val(dt);
                        }, 115);
                      }, 115);
            				}
                   $("#network-line").loading("stop");
                  },
                  error: function (xhr, status, error) {
                     $("#network-line").loading("stop");
                    // showDialog.show(xhr.status + " " + status + " " + error, false);
                  }
              });
          },
          error: function (xhr, status, error) {
              // showDialog.show(xhr.status + " " + status + " " + error, false);
          }
      });
    }else{
        $('#Sn').val("");
        $('#HostCode').val("");
        $('#Hostname').val("");
        $('#IpAddress').val("");
        $('#Sysname').val("");
        $('#FrameId').val("");
        $('#SlotId').val("");
        $('#PortId').val("");
        $('#VlanFrameId').val("");
        $('#VlanSlotId').val("");
        $('#VlanPortId').val("");
        $('#OntVersion').val("");
        $('#OntSoftware').val("");
   }
  });

  $('#TemplateCode').on('change', function() {
    if(this.value!=""){
      $.ajax({
          url: "{{ route('olt.template.show-header','')}}/" + this.value,
          type: "GET",
          headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
          },
          success: function (response) {
              var datas = response.data;
              $('#VlanDownLink').val(datas['template'].VlanDownLink);
              $('#NativeVlanEth1').val(datas['template'].NativeVlanEth1);
              $('#NativeVlanEth2').val(datas['template'].NativeVlanEth2);
              $('#NativeVlanEth3').val(datas['template'].NativeVlanEth3);
              $('#NativeVlanEth4').val(datas['template'].NativeVlanEth4);
              $('#OntLineProfile').val(datas['template'].OntLineProfileId).trigger('change');
              setTimeout(function(){
                $('#OntLineProfileId').val(datas['template'].OntLineProfileId);
              }, 115);
              $('#OntServiceProfile').val(datas['template'].OntSrvProfileId).trigger('change');
              setTimeout(function(){
                $('#OntServiceProfileId').val(datas['template'].OntLineProfileId);
              }, 115);
              $("input[name=VlanAttribut][value=" + datas['template'].VlanAttribut + "]").prop('checked', true);

              $("#datatable tbody>tr").remove();
              if(datas["template_detail"]){
                $(".TemplateCode").text(datas["template_detail"].length+" Vlan");
                for (var i in datas["template_detail"]){
                    var option = "";
                    option += ' <select name="s_tag[]" class="col-12" >';
                    if(datas["template_detail"][i].TagTransform == "translate"){
                      option += '  <option value="translate" selected>translate</option>';
                      option += '  <option value="translate-and-add">translate-and-add</option>';
                      option += '  <option value="default">default</option>';
                    }

                    if(datas["template_detail"][i].TagTransform == "translate-and-add"){
                      option += '  <option value="translate">translate</option>';
                      option += '  <option value="translate-and-add" selected>translate-and-add</option>';
                      option += '  <option value="default">default</option>';
                    }
                    if(datas["template_detail"][i].TagTransform == "default"){
                      option += '  <option value="translate">translate</option>';
                      option += '  <option value="translate-and-add">translate-and-add</option>';
                      option += '  <option value="default" selected>default</option>';
                    }
                    option += ' </select>';
                    var table = document.getElementById("datatable").getElementsByTagName('tbody')[0];
                    var row = table.insertRow(-1);
                    var cell1 = row.insertCell(0).innerHTML = '<input name="s_idx[]" class="form-input col-12" value="">';
                    if (datas["template_detail"][i].Vlan){
                      var cell2 = row.insertCell(1).innerHTML = '<input name="s_vlan[]" class="form-input col-12" value="'+datas["template_detail"][i].Vlan+'">';
                    }else{
                      var cell2 = row.insertCell(1).innerHTML = '<input name="s_vlan[]" class="form-input col-12" value="">';
                    }

                    if (datas["template_detail"][i].GemPort){
                      var cell3 = row.insertCell(2).innerHTML = '<input name="s_gemport[]" class="form-input col-12" value="'+datas["template_detail"][i].GemPort+'">';
                    }else{
                      var cell3 = row.insertCell(2).innerHTML = '<input name="s_gemport[]" class="form-input col-12" value="">';
                    }

                    // var cell3 = row.insertCell(2).innerHTML = '<input name="s_gemport[]" class="form-input col-12" value="">';

                    if (datas["template_detail"][i].UserVlan){
                      var cell4 = row.insertCell(3).innerHTML = '<input name="s_user[]" class="form-input col-12" value="'+datas["template_detail"][i].UserVlan+'">';
                    }else{
                      var cell4 = row.insertCell(3).innerHTML = '<input name="s_user[]" class="form-input col-12" value="">';
                    }

                    var cell5 = row.insertCell(4).innerHTML = option;

                    if (datas["template_detail"][i].InnerVlan){
                      var cell6 = row.insertCell(5).innerHTML = '<input name="s_inner[]" class="form-input col-12" value="'+datas["template_detail"][i].InnerVlan+'">';
                    }else{
                      var cell6 = row.insertCell(5).innerHTML = '<input name="s_inner[]" class="form-input col-12" value="">';
                    }

                    if (datas["template_detail"][i].TrafficTable){
                      var cell7 = row.insertCell(6).innerHTML = '<input name="s_traffic[]" class="form-input col-12" value="'+datas["template_detail"][i].TrafficTable+'">';
                    }else{
                      var cell7 = row.insertCell(6).innerHTML = '<input name="s_traffic[]" class="form-input col-12" value="">';
                    }

                    var cell8 = row.insertCell(7).innerHTML = '<a type="button" value="del" onclick="deleteRow(this)" class="pt-2"><i class="text-center text-danger fas fa-backspace"></i><a/>';
                }
              }
          },
          error: function (xhr, status, error) {
              // showDialog.show(xhr.status + " " + status + " " + error, false);
          }
      });
    }else{
        $("#datatable tbody>tr").remove();
        $('#VlanDownLink').val("");
        $('#NativeVlanEth1').val("");
        $('#NativeVlanEth2').val("");
        $('#NativeVlanEth3').val("");
        $('#NativeVlanEth4').val("");
   }
  });

  $('#FdtNo').on('change', function() {
    if(this.value!=""){
        $('#FatNo').val(this.value);
    }else{
        $('#FatNo').val("");
   }
  });


  $('#OntLineProfile').on('change', function() {
    if(this.value!=""){
      $('#OntLineProfileId').attr("readonly", true);
      $('#OntLineProfileId').val(this.value);
    }else{
      $('#OntLineProfileId').attr("readonly", false);
      $('#OntLineProfileId').val("");
   }
  });

  $('#Vlan').on('change', function() {
    if(this.value!=""){
      $('#FatNo').attr("readonly", true);
      $('#FatNo').val(this.value);
    }else{
      $('#FatNo').attr("readonly", false);
      $('#FatNo').val("");
   }
  });

  $('#OntServiceProfile').on('change', function() {
    if(this.value!=""){
      $('#OntSrvProfileId').attr("readonly", true);
      $('#OntSrvProfileId').val(this.value);
    }else{
      $('#OntSrvProfileId').attr("readonly", false);
      $('#OntSrvProfileId').val("");
   }
  });

  $('#TrafficTable').on('change', function() {
    if(this.value!=""){
      $('#TrafficTableId').attr("readonly", true);
      $('#TrafficTableId').val(this.value);
    }else{
      $('#TrafficTableId').attr("readonly", false);
      $('#TrafficTableId').val("");
   }
  });

  function format_date(val) {
      var date_arr = val.split(" ");
      var time = date_arr[1];
      var date_id_arr = date_arr[0].split("-");
      var date_fix = date_id_arr[2] + "-" + date_id_arr[1] + "-" + date_id_arr[0];
      var date_transaction = date_fix + " " + time;
      return date_transaction;
  }

  function addService() {
    var option = "";
    option += ' <select style="padding-right:1px;padding-left:1px; text-align:center;" name="s_tag[]" class="col-12" >';
    option += '  <option value=""></option>';
    option += '  <option value="translate">translate</option>';
    option += '  <option value="translate-and-add">translate-and-add</option>';
    option += '  <option value="default" selected>default</option>';
    option += ' </select>';
    var table = document.getElementById("datatable").getElementsByTagName('tbody')[0];
    var row = table.insertRow(-1);
    row.insertCell(0).innerHTML = '<input style="padding-right:1px;padding-left:1px; text-align:center;" name="s_idx[]" class="form-input col-12" value="">';
    row.insertCell(1).innerHTML = '<input style="padding-right:1px;padding-left:1px; text-align:center;" name="s_vlan[]" class="form-input col-12" value="">';
    row.insertCell(2).innerHTML = '<input style="padding-right:1px;padding-left:1px; text-align:center;" name="s_gemport[]" class="form-input col-12" value="">';
    row.insertCell(3).innerHTML = '<input style="padding-right:1px;padding-left:1px; text-align:center;" name="s_user[]" class="form-input col-12" value="">';
    row.insertCell(4).innerHTML = option;
    row.insertCell(5).innerHTML = '<input style="padding-right:1px;padding-left:1px; text-align:center;" name="s_inner[]" class="form-input col-12" value="">';
    row.insertCell(6).innerHTML = '<input style="padding-right:1px;padding-left:1px; text-align:center;" name="s_traffic[]" class="form-input col-12" value="">';
    row.insertCell(7).innerHTML = '<a type="button" value="del" onclick="deleteRow(this)" class="pt-2"><i class="text-center text-danger fas fa-backspace"></i><a/>';
  }

  function deleteRow(btn) {
    var row = btn.parentNode.parentNode;
    row.parentNode.removeChild(row);
  }

  function formatOption (option) {
    var $option = $(
      '<div><strong>' + option.text + '</strong></div><div>' + option.title + '</div>'
    );
    return $option;
  };
</script>
@endsection
