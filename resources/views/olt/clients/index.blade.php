@extends('layout.default') @section('content')
<div
	class="subheader py-2 {{ Metronic::printClasses('subheader', false) }}"
	id="kt_subheader">
	<div
		class="{{ Metronic::printClasses('subheader-container', false) }} d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
		<div class="d-flex align-items-baseline flex-wrap mr-5">
			<h5 class="text-dark font-weight-bold my-1 mr-5">Clients</h5>
			<ul
				class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
				<li class="breadcrumb-item"><a href="{{ route('dashboard') }}"
					class="text-muted">Dashboard</a></li>
				<li class="breadcrumb-item"><a
					href="{{ route('olt.clients.index') }}" class="text-muted">Clients</a>
				</li>
				<li class="breadcrumb-item"><a href="#view" class="text-muted">Clients</a>
				</li>
			</ul>
		</div>
	</div>
</div>

@include('inc.error-list')
@include('inc.success-notif')
<div class="container pb-2 body-container">
  <div class="col-lg-12">
    <div class="card card-custom body-container">
      <div class="col-lg-12">
        <h1 class="font-weight-bolder text-dark mb-0">Filter:</h1>
      </div>
      <div class="card-body rounded p-1 bg-light">
        <div class=" d-flex flex-wrap justify-content-center">
				<div class="col-lg-6">
					<select class="form-control select2" id="SearchHostCode" name="SearchHostCode" style="width: 100%" required>
						<option value="" selected>Chose Host</option>
						@isset ($host_list)
							@foreach($host_list as $host)
								<option value="{{ $host->Code }}">{{$host->Hostname }}</option>
							@endforeach
						@endisset
					</select>
				</div>
          <div class="col-lg-6">
            <select class="form-control select2" id="SearchClientsStatus" name="SearchClientsStatus" style="width: 100%;">
              <option value="Active" selected>Clients Active</option>
              <option value="Postpone">Clients Postpone</option>
              <option value="Closed">Clients Closed</option>
            </select>
          </div>
        </div>
        <div class="d-none d-md-flex flex-row-fluid bgi-no-repeat bgi-position-y-center bgi-position-x-left bgi-size-cover" ></div>
      </div>
    </div>
  </div>
</div>
<div class="card card-custom">
	<div class="card-header bg-danger flex-wrap border-1 pt-1 pb-0 mb-2" style="min-height: 0;">
		<div class="card-title pt-1 pb-1">
			<h3 class="card-label font-weight-bolder text-white">
				Clients
				<div class="text-muted pt-2 font-size-lg">show Datatable from table
					Clients</div>
			</h3>
		</div>
	</div>
	<div class="card-body pt-1">
    <div hidden><input id="start-date"/><input id="end-date"/></div>
		<table class="table table-bordered table-hover w100" cellspacing="0" id="datatable" style="width: 1070px !important;"></table>
	</div>
	<div class="card-body pt-1">
		<table class="table table-bordered table-hover w100" cellspacing="0" id="datatable" style="width: 1070px !important;"></table>
	</div>

	<div class="card-body py-1">
		<div class="card card-custom" id="data-response">
			<div class="card-header bg-danger flex-wrap border-1 pt-1 pb-0 mb-2" style="min-height: 0;">
				<div class="card-title pt-1 pb-1">
					<h3 class="card-label font-weight-bolder text-white">Response Command <label class="client-status">-</label></h3>
				</div>
			</div>
			<div class="card mb-8">
				<div class="row">
					<div class="col-lg-12">
						<pre class="container-fluid pt-2 client-response">-</pre>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="card-body py-1">
		<div class="card card-custom" id="data-clients">
			<div class="card-header bg-danger flex-wrap border-1 pt-1 pb-0 mb-2" style="min-height: 0;">
				<div class="card-title pt-1 pb-1">
					<h3 class="card-label font-weight-bolder text-white">Data Aktivasi</h3>
				</div>
			</div>
			<div class="card mb-8">
				<div class="row">
					<div class="col-lg-3">
							<ul class="navi navi-link-rounded navi-accent navi-hover navi-active nav flex-column mb-8 mb-lg-0" role="tablist">
								<li class="navi-item mt-2 mb-2">
									<a class="navi-link active" data-toggle="tab" href="#v-pills-home">
										<span class="navi-text text-dark-50 font-size-h5 font-weight-bold">Data Clients</span>
									</a>
								</li>
								<li class="navi-item mt-2">
									<a class="navi-link" data-toggle="tab" onclick="get_clientstools('general-info', 0)" href="#v-pills-general-info">
											<span class="navi-text text-dark-50 font-size-h5 font-weight-bold">General Info</span>
									</a>
								</li>
								<li class="navi-item mb-2">
									<a class="navi-link" data-toggle="tab" href="#v-pills-host-current-config" hidden="true">
										<span class="navi-text text-dark-50 font-size-h5 font-weight-bold">Host Current Config</span>
									</a>
								</li>
								<li class="navi-item mb-2">
									<!-- onclick="get_clientstools('ont-version', 0)" -->
									<a class="navi-link" data-toggle="tab"  onclick="get_clientstools('ont-version', 0)" href="#v-pills-version">
										<span class="navi-text text-dark-50 font-size-h5 font-weight-bold">Version</span>
									</a>
								</li>
								<li class="navi-item mb-2">
									<a class="navi-link" data-toggle="tab" onclick="get_clientstools('ont-info', 0)" href="#v-pills-ont-info">
										<span class="navi-text text-dark-50 font-size-h5 font-weight-bold">Ont Info</span>
									</a>
								</li>
								<li class="navi-item mb-2">
									<a class="navi-link" data-toggle="tab" onclick="get_clientstools('wan-info', 0)" href="#v-pills-wan-info">
										<span class="navi-text text-dark-50 font-size-h5 font-weight-bold">Wan Info</span>
									</a>
								</li>
								<li class="navi-item mb-2">
									<a class="navi-link" data-toggle="tab" onclick="get_clientstools('optical-info', 0)" href="#v-pills-optical-info">
										<span class="navi-text text-dark-50 font-size-h5 font-weight-bold">Optical Info</span>
									</a>
								</li>
								<li class="navi-item mb-2">
									<a class="navi-link" data-toggle="tab" onclick="get_clientstools('register-info', 0)" href="#v-pills-register-info">
										<span class="navi-text text-dark-50 font-size-h5 font-weight-bold">Register Info</span>
									</a>
								</li>
								<li class="navi-item mb-2">
									<a class="navi-link" data-toggle="tab" onclick="get_clientstools('mac-address', 0)" href="#v-pills-mac-address">
										<span class="navi-text text-dark-50 font-size-h5 font-weight-bold">Mac Address</span>
									</a>
								</li>
								<li class="navi-item mb-2">
									<a class="navi-link" data-toggle="tab" onclick="get_clientstools('port-state', 0)" href="#v-pills-port-state">
										<span class="navi-text text-dark-50 font-size-h5 font-weight-bold">Port State</span>
									</a>
								</li>
								<li class="navi-item mb-2">
									<a class="navi-link" data-toggle="tab" onclick="get_clientstools('port-attribute', 0)" href="#v-pills-port-attribute">
										<span class="navi-text text-dark-50 font-size-h5 font-weight-bold">Port Attribute</span>
									</a>
								</li>
								<li class="navi-item mb-2">
									<a class="navi-link" data-toggle="tab" onclick="get_clientstools('fec-crc-error', 0)" href="#v-pills-fec-crc-error">
										<span class="navi-text text-dark-50 font-size-h5 font-weight-bold">FEC/CRC Error</span>
									</a>
								</li>
							</ul>
						</div>
					<div class="col-lg-9">
						<div class="tab-content">
						  <div class="tab-pane fade show active mt-2" id="v-pills-home" role="tab">
								<div class="card-body">
									<div class="d-flex">
										<div class="flex-grow-1 col-md-9">
											<div class="d-flex align-items-center justify-content-between flex-wrap mt-2">
												<div class="mr-3">
													<a class="d-flex align-items-center text-dark text-hover-primary font-size-h5 font-weight-bold mr-3">
														<label class="CustomerName">-</label>
													</a>
													<div class="d-flex flex-wrap my-2">
														<a class="text-muted text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">
														<span class="svg-icon svg-icon-md svg-icon-gray-500 mr-1">
															<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																	<rect x="0" y="0" width="24" height="24"></rect>
																	<path d="M21,12.0829584 C20.6747915,12.0283988 20.3407122,12 20,12 C16.6862915,12 14,14.6862915 14,18 C14,18.3407122 14.0283988,18.6747915 14.0829584,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,8 C3,6.8954305 3.8954305,6 5,6 L19,6 C20.1045695,6 21,6.8954305 21,8 L21,12.0829584 Z M18.1444251,7.83964668 L12,11.1481833 L5.85557487,7.83964668 C5.4908718,7.6432681 5.03602525,7.77972206 4.83964668,8.14442513 C4.6432681,8.5091282 4.77972206,8.96397475 5.14442513,9.16035332 L11.6444251,12.6603533 C11.8664074,12.7798822 12.1335926,12.7798822 12.3555749,12.6603533 L18.8555749,9.16035332 C19.2202779,8.96397475 19.3567319,8.5091282 19.1603533,8.14442513 C18.9639747,7.77972206 18.5091282,7.6432681 18.1444251,7.83964668 Z" fill="#000000"></path>
																	<circle fill="#000000" opacity="0.3" cx="19.5" cy="17.5" r="2.5"></circle>
																</g>
															</svg>
														</span><label class="CustomerEmail">-</label></a>
														<a class="text-muted text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">
														<span class="svg-icon svg-icon-md svg-icon-gray-500 mr-1">
															<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																	<mask fill="white">
																		<use xlink:href="#path-1"></use>
																	</mask>
																	<g></g>
																	<path d="M7,10 L7,8 C7,5.23857625 9.23857625,3 12,3 C14.7614237,3 17,5.23857625 17,8 L17,10 L18,10 C19.1045695,10 20,10.8954305 20,12 L20,18 C20,19.1045695 19.1045695,20 18,20 L6,20 C4.8954305,20 4,19.1045695 4,18 L4,12 C4,10.8954305 4.8954305,10 6,10 L7,10 Z M12,5 C10.3431458,5 9,6.34314575 9,8 L9,10 L15,10 L15,8 C15,6.34314575 13.6568542,5 12,5 Z" fill="#000000"></path>
																</g>
															</svg>
														</span><label class="CustomerPhone">-</label></a>
														<a class="text-muted text-hover-primary font-weight-bold">
														<span class="svg-icon svg-icon-md svg-icon-gray-500 mr-1">
															<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																	<rect x="0" y="0" width="24" height="24"></rect>
																	<path d="M9.82829464,16.6565893 C7.02541569,15.7427556 5,13.1079084 5,10 C5,6.13400675 8.13400675,3 12,3 C15.8659932,3 19,6.13400675 19,10 C19,13.1079084 16.9745843,15.7427556 14.1717054,16.6565893 L12,21 L9.82829464,16.6565893 Z M12,12 C13.1045695,12 14,11.1045695 14,10 C14,8.8954305 13.1045695,8 12,8 C10.8954305,8 10,8.8954305 10,10 C10,11.1045695 10.8954305,12 12,12 Z" fill="#000000"></path>
																</g>
															</svg>
														</span><label class="CustomerAddress">-</label></a>
													</div>
												</div>
											</div>
											<div class="d-flex align-items-center flex-wrap justify-content-between">
												<div class="flex-grow-1 font-weight-bold text-dark-50 py-2 py-lg-2 mr-5">
													<h3>Status: <label class="CustomerStatusClients">-</label></h3>
												</div>
											</div>
											<div class="d-flex align-items-center flex-wrap justify-content-between">
												<div class="flex-grow-1 font-weight-bold text-dark-50 py-2 py-lg-2 mr-5">
													<h3>Notes:</h3>
													<label class="CustomerRemark">-</label>
												</div>
											</div>
										</div>
										<div class="card-toolbar pt-1 pb-0 pr-3">
												<a id="check-power"
													class="btn btn-info font-weight-bolder"
													style="background-color: #e89e17; border-color: #FFFFFF;"> <span
													class="svg-icon svg-icon-md"> <svg
															xmlns="http://www.w3.org/2000/svg"
															xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
															height="24px" viewBox="0 0 24 24" version="1.1">
																<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																	<rect x="0" y="0" width="24" height="24"></rect>
																	<path d="M11.7573593,15.2426407 L8.75735931,15.2426407 C8.20507456,15.2426407 7.75735931,15.6903559 7.75735931,16.2426407 C7.75735931,16.7949254 8.20507456,17.2426407 8.75735931,17.2426407 L11.7573593,17.2426407 L11.7573593,18.2426407 C11.7573593,19.3472102 10.8619288,20.2426407 9.75735931,20.2426407 L5.75735931,20.2426407 C4.65278981,20.2426407 3.75735931,19.3472102 3.75735931,18.2426407 L3.75735931,14.2426407 C3.75735931,13.1380712 4.65278981,12.2426407 5.75735931,12.2426407 L9.75735931,12.2426407 C10.8619288,12.2426407 11.7573593,13.1380712 11.7573593,14.2426407 L11.7573593,15.2426407 Z" fill="#000000" opacity="0.3" transform="translate(7.757359, 16.242641) rotate(-45.000000) translate(-7.757359, -16.242641) "></path>
																	<path d="M12.2426407,8.75735931 L15.2426407,8.75735931 C15.7949254,8.75735931 16.2426407,8.30964406 16.2426407,7.75735931 C16.2426407,7.20507456 15.7949254,6.75735931 15.2426407,6.75735931 L12.2426407,6.75735931 L12.2426407,5.75735931 C12.2426407,4.65278981 13.1380712,3.75735931 14.2426407,3.75735931 L18.2426407,3.75735931 C19.3472102,3.75735931 20.2426407,4.65278981 20.2426407,5.75735931 L20.2426407,9.75735931 C20.2426407,10.8619288 19.3472102,11.7573593 18.2426407,11.7573593 L14.2426407,11.7573593 C13.1380712,11.7573593 12.2426407,10.8619288 12.2426407,9.75735931 L12.2426407,8.75735931 Z" fill="#000000" transform="translate(16.242641, 7.757359) rotate(-45.000000) translate(-16.242641, -7.757359) "></path>
																	<path d="M5.89339828,3.42893219 C6.44568303,3.42893219 6.89339828,3.87664744 6.89339828,4.42893219 L6.89339828,6.42893219 C6.89339828,6.98121694 6.44568303,7.42893219 5.89339828,7.42893219 C5.34111353,7.42893219 4.89339828,6.98121694 4.89339828,6.42893219 L4.89339828,4.42893219 C4.89339828,3.87664744 5.34111353,3.42893219 5.89339828,3.42893219 Z M11.4289322,5.13603897 C11.8194565,5.52656326 11.8194565,6.15972824 11.4289322,6.55025253 L10.0147186,7.96446609 C9.62419433,8.35499039 8.99102936,8.35499039 8.60050506,7.96446609 C8.20998077,7.5739418 8.20998077,6.94077682 8.60050506,6.55025253 L10.0147186,5.13603897 C10.4052429,4.74551468 11.0384079,4.74551468 11.4289322,5.13603897 Z M0.600505063,5.13603897 C0.991029355,4.74551468 1.62419433,4.74551468 2.01471863,5.13603897 L3.42893219,6.55025253 C3.81945648,6.94077682 3.81945648,7.5739418 3.42893219,7.96446609 C3.0384079,8.35499039 2.40524292,8.35499039 2.01471863,7.96446609 L0.600505063,6.55025253 C0.209980772,6.15972824 0.209980772,5.52656326 0.600505063,5.13603897 Z" fill="#000000" opacity="0.3" transform="translate(6.014719, 5.843146) rotate(-45.000000) translate(-6.014719, -5.843146) "></path>
																	<path d="M17.9142136,15.4497475 C18.4664983,15.4497475 18.9142136,15.8974627 18.9142136,16.4497475 L18.9142136,18.4497475 C18.9142136,19.0020322 18.4664983,19.4497475 17.9142136,19.4497475 C17.3619288,19.4497475 16.9142136,19.0020322 16.9142136,18.4497475 L16.9142136,16.4497475 C16.9142136,15.8974627 17.3619288,15.4497475 17.9142136,15.4497475 Z M23.4497475,17.1568542 C23.8402718,17.5473785 23.8402718,18.1805435 23.4497475,18.5710678 L22.0355339,19.9852814 C21.6450096,20.3758057 21.0118446,20.3758057 20.6213203,19.9852814 C20.2307961,19.5947571 20.2307961,18.9615921 20.6213203,18.5710678 L22.0355339,17.1568542 C22.4260582,16.76633 23.0592232,16.76633 23.4497475,17.1568542 Z M12.6213203,17.1568542 C13.0118446,16.76633 13.6450096,16.76633 14.0355339,17.1568542 L15.4497475,18.5710678 C15.8402718,18.9615921 15.8402718,19.5947571 15.4497475,19.9852814 C15.0592232,20.3758057 14.4260582,20.3758057 14.0355339,19.9852814 L12.6213203,18.5710678 C12.2307961,18.1805435 12.2307961,17.5473785 12.6213203,17.1568542 Z" fill="#000000" opacity="0.3" transform="translate(18.035534, 17.863961) scale(1, -1) rotate(45.000000) translate(-18.035534, -17.863961) "></path>
															</g>
														</svg>
												</span>Check Power
												</a>
											</div>
									</div>
									<div class="separator separator-solid my-7"></div>
									<div class="d-flex align-items-center flex-wrap">
										<div class="col-12 d-flex">
												<div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
													<span class="mr-4">
														<i class="flaticon-piggy-bank icon-2x text-muted font-weight-bold"></i>
													</span>
													<div class="d-flex flex-column text-dark-75">
														<span class="font-weight-bolder font-size-sm">Product Speed</span>
														<span class="font-weight-bolder font-size-h5">
															<label class="CustomerProductName">-</label>
														</span>
													</div>
												</div>
												<div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
													<span class="mr-4">
														<i class="flaticon-confetti icon-2x text-muted font-weight-bold"></i>
													</span>
													<div class="d-flex flex-column text-dark-75">
														<span class="font-weight-bolder font-size-sm">Service Port</span>
														<span class="font-weight-bolder font-size-h5">
															<label class="CustomerServicePort">-</label>
														</span>
													</div>
												</div>
												<div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
													<span class="mr-4">
														<i class="flaticon-pie-chart icon-2x text-muted font-weight-bold"></i>
													</span>
													<div class="d-flex flex-column text-dark-75">
														<span class="font-weight-bolder foLast Notent-size-sm">VL Down link</span>
														<span class="font-weight-bolder font-size-h5">
															<label class="VlanDownLink">-</label>
														</span>
													</div>
												</div>
												<div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
													<span class="mr-4">
														<i class="flaticon-file-2 icon-2x text-muted font-weight-bold"></i>
													</span>
													<div class="d-flex flex-column flex-lg-fill">
														<span class="text-dark-75 font-weight-bolder font-size-sm">Status</span>
														<a class="text-primary font-weight-bolder"><label class="OntFlag">-</label></a>
													</div>
												</div>
												<div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
													<span class="mr-4">
														<i class="flaticon-chat-1 icon-2x text-muted font-weight-bold"></i>
													</span>
													<div class="d-flex flex-column">
														<span class="text-dark-75 font-weight-bolder font-size-sm">LP</span>
														<a class="text-primary font-weight-bolder"><label class="OntLineProfileId">-</label></a>
													</div>
												</div>
												<div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
													<span class="mr-4">
														<i class="flaticon-chat-1 icon-2x text-muted font-weight-bold"></i>
													</span>
													<div class="d-flex flex-column">
														<span class="text-dark-75 font-weight-bolder font-size-sm">SP</span>
														<a class="text-primary font-weight-bolder"><label class="OntSrvProfileId">-</label></a>
													</div>
												</div>
										</div>
										<div class="col-12 d-flex">
												<div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
													<span class="mr-4">
														<i class="flaticon-piggy-bank icon-2x text-muted font-weight-bold"></i>
													</span>
													<div class="d-flex flex-column text-dark-75">
														<span class="font-weight-bolder font-size-sm">ONT INFO [ ID <label class="OntId" style="font-weight:900; color:red;">-</label> ]</span>
														<span class="font-weight-bolder font-size-h5">
															<label class="OntInfo">-</label>
														</span>
													</div>
												</div>
												<div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
													<span class="mr-4">
														<i class="flaticon-confetti icon-2x text-muted font-weight-bold"></i>
													</span>
													<div class="d-flex flex-column text-dark-75">
														<span class="font-weight-bolder font-size-sm">NATIVE VLAN</span>
														<span class="font-weight-bolder font-size-h5">
															<label class="NativeVlan">-</label>
														</span>
													</div>
												</div>
												<div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
													<span class="mr-4">
														<i class="flaticon-pie-chart icon-2x text-muted font-weight-bold"></i>
													</span>
													<div class="d-flex flex-column text-dark-75">
														<span class="font-weight-bolder font-size-sm">BOARD VLAN</span>
														<span class="font-weight-bolder font-size-h5">
															<label class="BoardVlan">-</label>
														</span>
													</div>
												</div>
										</div>
										<div class="col-12">
												<div class="d-flex align-items-center flex-lg-fill my-1">
													<table id="datatable_vlan_service" class="table dataTable table-bordered table-hover w100" cellspacing="0">
															<thead>
																<tr>
																		<th class="text-center text-white dt-body-nowrap">Index</th>
																		<th class="text-center text-white dt-body-nowrap">Vlan</th>
																		<th class="text-center text-white dt-body-nowrap">Gemport</th>
																		<th class="text-center text-white dt-body-nowrap">User-VLAN</th>
																		<th class="text-center text-white dt-body-nowrap">Tag-Transform</th>
																		<th class="text-center text-white dt-body-nowrap">Inner-VLAN</th>
																		<th class="text-center text-white dt-body-nowrap">Traffic-Table</th>
																		<th class="text-center text-white dt-body-nowrap">Action</th>
																</tr>
															</thead>
															<tbody>
															</tbody>
													</table>
												</div>
										</div>
									</div>
								</div>
							</div>
						  <div class="tab-pane fade mt-2" id="v-pills-general-info" role="tab">
								<div class="card-body">
									<div class="d-flex">
										<div class="flex-grow-1 col-md-12">
											<div class="row justify-content-center py-1 px-1">
												<div class="card-toolbar col-md-7 pt-1 pb-0 pr-3">
													<span>IP:<label class="IpAddress">-</label>&nbsp;-&nbsp;<label class="Hostname">-</label></span><br/>
														<span><label>Client Name</label>&nbsp;:&nbsp;<label class="CustomerName">-</label></span>
												</div>
												<div class="col-md-5">
													<button type="button" class="btn btn-tool btn-sm" onclick="get_clientstools('general-info', 1)">
															<i class="la la-download text-white"></i> Update Info
													</button>
													<div class="d-flex justify-content-between pb-2 flex-column flex-md-row">
														<h3 class="font-weight-boldest mb-10">SN: <label class="OntSn">-</label></h3>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="d-flex flex-wrap">
										<div class="flex-grow-1 col-md-6">
											<div class="row mt-2">
												<div class="mr-3">
													<a class="d-flex align-items-center text-dark text-hover-primary font-size-h5 font-weight-bold mr-3">
														<label>ONT INFO</label>
													</a>
												</div>
												<div class="col-md-12 d-flex flex-wrap">
													<div class="col-md-4"><label class="font-weight-bolder font-size-sm">Status</label></div>
													<div class="col-md-8">: <label class="Flag">-</label></div>
												</div>
												<div class="col-md-12 d-flex flex-wrap">
													<div class="col-md-4"><label class="font-weight-bolder font-size-sm">Run State</label></div>
													<div class="col-md-8">: <label class="Run">-</label></div>
												</div>
												<div class="col-md-12 d-flex flex-wrap">
													<div class="col-md-4"><label class="font-weight-bolder font-size-sm">LastUp Time</label></div>
													<div class="col-md-8">: <label class="LastUpTime">-</label></div>
												</div>
												<div class="col-md-12 d-flex flex-wrap">
													<div class="col-md-4"><label class="font-weight-bolder font-size-sm">LastDown Time</label></div>
													<div class="col-md-8">: <label class="LastDownTime">-</label></div>
												</div>
												<div class="col-md-12 d-flex flex-wrap">
													<div class="col-md-4"><label class="font-weight-bolder font-size-sm">LastDying Gasp</label></div>
													<div class="col-md-8">: <label class="LastDyingGaspTime">-</label></div>
												</div>
												<div class="col-md-12 d-flex flex-wrap">
													<div class="col-md-4"><label class="font-weight-bolder font-size-sm">ONT Online Duration</label></div>
													<div class="col-md-8">: <label class="OntOnlineDuration">-</label></div>
												</div>
											</div>
										</div>
										<div class="flex-grow-1 col-md-6">
											<div class="row mt-2">
												<div class="mr-3">
													<a class="d-flex align-items-center text-dark text-hover-primary font-size-h5 font-weight-bold mr-3">
														<label>ONT WAN INFO</label>
													</a>
												</div>
												<div class="col-md-12 d-flex flex-wrap">
													<div class="col-md-4"><label class="font-weight-bolder font-size-sm">Mac Address</label></div>
													<div class="col-md-8">: <label class="MacAddress">-</label></div>
												</div>
												<div class="col-md-12 d-flex flex-wrap">
													<div class="col-md-4"><label class="font-weight-bolder font-size-sm">Service Type</label></div>
													<div class="col-md-8">: <label class="ServiceType">-</label></div>
												</div>
												<div class="col-md-12 d-flex flex-wrap">
													<div class="col-md-4"><label class="font-weight-bolder font-size-sm">Connection type</label></div>
													<div class="col-md-8">: <label class="ConnectionType">-</label></div>
												</div>
												<div class="col-md-12 d-flex flex-wrap">
													<div class="col-md-4"><label class="font-weight-bolder font-size-sm">IPv4 Connection status</label></div>
													<div class="col-md-8">: <label class="ConnectionStatus">-</label></div>
												</div>
											</div>
										</div>
										<div class="flex-grow-1 col-md-6">
											<div class="row mt-2">
												<div class="mr-3">
													<a class="d-flex align-items-center text-dark text-hover-primary font-size-h5 font-weight-bold mr-3">
														<label>ONT OPTICAL INFO</label>
													</a>
												</div>
												<div class="col-md-12 d-flex flex-wrap">
													<div class="col-md-4"><label class="font-weight-bolder font-size-sm">ONT Tx Power</label></div>
													<div class="col-md-8">: <label class="TxOpticalPower">-</label></div>
												</div>
												<div class="col-md-12 d-flex flex-wrap">
													<div class="col-md-4"><label class="font-weight-bolder font-size-sm">ONT Rx Power</label></div>
													<div class="col-md-8">: <label class="RxOpticalPower">-</label></div>
												</div>
												<!-- <div class="col-md-12 d-flex flex-wrap">
													<div class="col-md-4"><label class="font-weight-bolder font-size-sm">OLT Rx Power Precision</label></div>
													<div class="col-md-8">: <label class="OltRxOntOpticalPower">-</label></div>
												</div> -->
												<div class="col-md-12 d-flex flex-wrap">
													<div class="col-md-4"><label class="font-weight-bolder font-size-sm">ONT Distance</label></div>
													<div class="col-md-8">: <label class="OntDistance">-</label></div>
												</div>
											</div>
										</div>
										<div class="flex-grow-1 col-md-6">
											<div class="row mt-2">
												<div class="mr-3">
													<a class="d-flex align-items-center text-dark text-hover-primary font-size-h5 font-weight-bold mr-3">
														<label>ONT ETHERNET INFO</label>
													</a>
												</div>
												<div class="col-md-12 d-flex flex-wrap">
													<div class="col-md-10">
														<div class="table-responsive">
																<table cellspacing="0" id="datatable_general-info">
																		<thead>
																			<tr>
																					<td>Speed(Mbps) &nbsp;</td>
																					<td>Duplex &nbsp;</td>
																					<td>LinkStats &nbsp;</td>
																			</tr>
																		</thead>
																		<tbody>
																		</tbody>
																</table>
														</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tab-pane fade mt-2" id="v-pills-version" role="tab">
							<div class="card-body p-0">
								<div class="row justify-content-center py-1 px-1">
									<div class="card-toolbar col-md-7 pt-1 pb-0 pr-3">
										<span>IP:<label class="IpAddress">-</label>&nbsp;-&nbsp;<label class="Hostname">-</label></span><br/>
											<span><label>Client Name</label>&nbsp;:&nbsp;<label class="CustomerName">-</label></span>
									</div>
									<div class="col-md-4">
										<button type="button" class="btn btn-tool btn-sm" onclick="get_clientstools('ont-version', 1)">
												<i class="la la-download text-white"></i> Update Version
										</button>
										<div class="d-flex justify-content-between pb-5 pb-md-5 flex-column flex-md-row">
											<h3 class="font-weight-boldest mb-10">SN: <label class="OntSn">-</label></h3>
										</div>
									</div>
								</div>
								<div class="col-md-12">
									<pre class="container-fluid pt-2 ont-version">-</pre>
								</div>
							</div>
						</div>
						<div class="tab-pane fade mt-2" id="v-pills-ont-info" role="tab">
							<div class="card-body p-0">
								<div class="row justify-content-center py-1 px-1">
									<div class="card-toolbar col-md-7 pt-1 pb-0 pr-3">
										<span>IP:<label class="IpAddress">-</label>&nbsp;-&nbsp;<label class="Hostname">-</label></span><br/>
											<span><label>Client Name</label>&nbsp;:&nbsp;<label class="CustomerName">-</label></span>
									</div>
									<div class="col-md-5">
										<button type="button" class="btn btn-tool btn-sm" onclick="get_clientstools('ont-info', 1)">
												<i class="la la-download text-white"></i> Update Ont Info
										</button>
									<div class="d-flex justify-content-between pb-5 pb-md-5 flex-column flex-md-row">
										<h3 class="font-weight-boldest mb-10">SN: <label class="OntSn">-</label></h3>
									</div>
								</div>
							</div>
								<div class="col-md-12">
									<pre class="container-fluid pt-2 ont-info">-</pre>
								</div>
							</div>
						</div>
						<div class="tab-pane fade mt-2" id="v-pills-wan-info" role="tab">
							<div class="card-body p-0">
								<div class="row justify-content-center py-1 px-1">
									<div class="card-toolbar col-md-7 pt-1 pb-0 pr-3">
										<span>IP:<label class="IpAddress">-</label>&nbsp;-&nbsp;<label class="Hostname">-</label></span><br/>
											<span><label>Client Name</label>&nbsp;:&nbsp;<label class="CustomerName">-</label></span>
									</div>
									<div class="col-md-5">
											<button type="button" class="btn btn-tool btn-sm" onclick="get_clientstools('wan-info', 1)">
													<i class="la la-download text-white"></i> Update Wan Info
											</button>
										<div class="d-flex justify-content-between pb-5 pb-md-5 flex-column flex-md-row">
											<h3 class="font-weight-boldest mb-10">SN: <label class="OntSn">-</label></h3>
										</div>
									</div>
								</div>
								<div class="col-md-12">
									<pre class="container-fluid pt-2 wan-info">-</pre>
								</div>
							</div>
						</div>
						<div class="tab-pane fade mt-2" id="v-pills-optical-info" role="tab">
							<div class="card-body p-0">
								<div class="row justify-content-center py-1 px-1">
									<div class="card-toolbar col-md-7 pt-1 pb-0 pr-3">
										<span>IP:<label class="IpAddress">-</label>&nbsp;-&nbsp;<label class="Hostname">-</label></span><br/>
											<span><label>Client Name</label>&nbsp;:&nbsp;<label class="CustomerName">-</label></span>
									</div>
									<div class="col-md-5">
											<button type="button" class="btn btn-tool btn-sm" onclick="get_clientstools('optical-info', 1)">
													<i class="la la-download text-white"></i> Update Optocal Info
											</button>
										<div class="d-flex justify-content-between pb-5 pb-md-5 flex-column flex-md-row">
											<h3 class="font-weight-boldest mb-10">SN: <label class="OntSn">-</label></h3>
										</div>
									</div>
								</div>
								<div class="col-md-12">
									<pre class="container-fluid pt-2 optical-info">-</pre>
								</div>
							</div>
						</div>
						<div class="tab-pane fade mt-2" id="v-pills-register-info" role="tab">
							<div class="card-body p-0">
								<div class="row justify-content-center py-1 px-1">
									<div class="card-toolbar col-md-7 pt-1 pb-0 pr-3">
										<span>IP:<label class="IpAddress">-</label>&nbsp;-&nbsp;<label class="Hostname">-</label></span><br/>
											<span><label>Client Name</label>&nbsp;:&nbsp;<label class="CustomerName">-</label></span>
									</div>
									<div class="col-md-5">
											<button type="button" class="btn btn-tool btn-sm" onclick="get_clientstools('register-info', 1)">
													<i class="la la-download text-white"></i> Update Register Info
											</button>
										<div class="d-flex justify-content-between pb-5 pb-md-5 flex-column flex-md-row">
											<h3 class="font-weight-boldest mb-10">SN: <label class="OntSn">-</label></h3>
										</div>
									</div>
								</div>
								<div class="col-md-12">
									<pre class="container-fluid pt-2 register-info">-</pre>
								</div>
							</div>
						</div>
						<div class="tab-pane fade mt-2" id="v-pills-mac-address" role="tab">
							<div class="card-body p-0">
								<div class="row justify-content-center py-1 px-1">
									<div class="card-toolbar col-md-7 pt-1 pb-0 pr-3">
										<span>IP:<label class="IpAddress">-</label>&nbsp;-&nbsp;<label class="Hostname">-</label></span><br/>
											<span><label>Client Name</label>&nbsp;:&nbsp;<label class="CustomerName">-</label></span>
									</div>
									<div class="col-md-5">
											<button type="button" class="btn btn-tool btn-sm" onclick="get_clientstools('mac-address', 1)">
													<i class="la la-download text-white"></i> Update Mac Address
											</button>
										<div class="d-flex justify-content-between pb-5 pb-md-5 flex-column flex-md-row">
											<h3 class="font-weight-boldest mb-10">SN: <label class="OntSn">-</label></h3>
										</div>
									</div>
								</div>
								<div class="col-md-12">
									<pre class="container-fluid pt-2 mac-address">-</pre>
								</div>
							</div>
						</div>
						<div class="tab-pane fade mt-2" id="v-pills-port-state" role="tab">
							<div class="card-body p-0">
								<div class="row justify-content-center py-1 px-1">
									<div class="card-toolbar col-md-7 pt-1 pb-0 pr-3">
										<span>IP:<label class="IpAddress">-</label>&nbsp;-&nbsp;<label class="Hostname">-</label></span><br/>
											<span><label>Client Name</label>&nbsp;:&nbsp;<label class="CustomerName">-</label></span>
									</div>
									<div class="col-md-5">
											<button type="button" class="btn btn-tool btn-sm" onclick="get_clientstools('port-state', 1)">
													<i class="la la-download text-white"></i> Update Port State
											</button>
										<div class="d-flex justify-content-between pb-5 pb-md-5 flex-column flex-md-row">
											<h3 class="font-weight-boldest mb-10">SN: <label class="OntSn">-</label></h3>
										</div>
									</div>
								</div>
								<div class="col-md-12">
									<pre class="container-fluid pt-2 port-state">-</pre>
								</div>
							</div>
						</div>
						<div class="tab-pane fade mt-2" id="v-pills-port-attribute" role="tab">
							<div class="card-body p-0">
								<div class="row justify-content-center py-1 px-1">
									<div class="card-toolbar col-md-7 pt-1 pb-0 pr-3">
										<span>IP:<label class="IpAddress">-</label>&nbsp;-&nbsp;<label class="Hostname">-</label></span><br/>
											<span><label>Client Name</label>&nbsp;:&nbsp;<label class="CustomerName">-</label></span>
									</div>
									<div class="col-md-5">
											<button type="button" class="btn btn-tool btn-sm" onclick="get_clientstools('port-attribute', 1)">
													<i class="la la-download text-white"></i> Update Port Attribute
											</button>
										<div class="d-flex justify-content-between pb-5 pb-md-5 flex-column flex-md-row">
											<h3 class="font-weight-boldest mb-10">SN: <label class="OntSn">-</label></h3>
										</div>
									</div>
								</div>
								<div class="col-md-12">
									<pre class="container-fluid pt-2 port-attribute">-</pre>
								</div>
							</div>
						</div>
						<div class="tab-pane fade mt-2" id="v-pills-fec-crc-error" role="tab">
							<div class="card-body p-0">
								<div class="row justify-content-center py-1 px-1">
									<div class="card-toolbar col-md-7 pt-1 pb-0 pr-3">
										<span>IP:<label class="IpAddress">-</label>&nbsp;-&nbsp;<label class="Hostname">-</label></span><br/>
											<span><label>Client Name</label>&nbsp;:&nbsp;<label class="CustomerName">-</label></span>
									</div>
									<div class="col-md-5">
											<button type="button" class="btn btn-tool btn-sm" onclick="get_clientstools('fec-crc-error', 1)">
													<i class="la la-download text-white"></i> Update FEC/CEC Error
											</button>
										<div class="d-flex justify-content-between pb-5 pb-md-5 flex-column flex-md-row">
											<h3 class="font-weight-boldest mb-10">SN: <label class="OntSn">-</label></h3>
										</div>
									</div>
								</div>
								<div class="col-md-12">
									<pre class="container-fluid pt-2 fec-crc-error">-</pre>
								</div>
							</div>
						</div>
						<div class="tab-pane fade mt-2" id="v-pills-host-current-config" role="tab">
							<div class="card-body p-0">
								<div class="row justify-content-center py-1 px-1">
									<div class="col-md-9">
										<div class="d-flex justify-content-between pb-5 pb-md-5 flex-column flex-md-row">
											<h3 class="font-weight-boldest mb-10">SN: <label class="OntSn">-</label></h3>
										</div>
									</div>
									<div class="card-toolbar pt-1 pb-0 pr-3">
										<span>IP:<label class="IpAddress">-</label>&nbsp;-&nbsp;<label class="Hostname">-</label></span><br/>
											<span><label>Client Name</label>&nbsp;:&nbsp;<label class="CustomerName">-</label></span>
									</div>
								</div>
								<div class="col-md-12">
									<pre class="container-fluid pt-2 host-current-config">-</pre>
								</div>
							</div>
						</div>
					  <div class="tab-pane fade mt-2" id="v-pills-profile" role="tab">
							<div class="card-body p-0">
								<div class="row justify-content-center py-1 px-1">
									<div class="col-md-9">
										<div class="d-flex justify-content-between pb-5 pb-md-5 flex-column flex-md-row">
											<h3 class="font-weight-boldest mb-10">SN: <label class="OntSn">-</label></h3>
											<div class="d-flex flex-column align-items-md-end px-0">
												<span class="d-flex flex-column align-items-md-end opacity-70">
													<span><label class="Hostname">-</label>,<label class="IspAddress">-</label></span>
													<span><label class="IpAddress">-</label></span>
												</span>
											</div>
										</div>
										<div class="border-bottom w-100"></div>
										<div class="d-flex justify-content-between pt-6">
											<div class="d-flex flex-column flex-root">
												<span class="font-weight-bolder mb-2">ONT.</span>
												<span class="opacity-70">SN    : <label class="OntSn">-</label></span>
												<span class="opacity-70">Frame : <label class="FrameId">-</label></span>
												<span class="opacity-70">Slot  : <label class="SlotId">-</label></span>
												<span class="opacity-70">Port  : <label class="PortId">-</label></span>
											</div>
											<div class="d-flex flex-column flex-root">
												<span class="font-weight-bolder mb-2">VLAN.</span>
												<span class="opacity-70 pt-2 pb-0">Down Link: <label class="VlanDownLink">-</label></span>
												<!-- <span class="opacity-70 pt-0 pb-0">Vlan:</span>
												<span class="opacity-70 pt-0 pb-2">
													<label class="NativeVlanEth1">-</label> [1]/
													<label class="NativeVlanEth2">-</label> [2]/
													<label class="NativeVlanEth3">-</label> [3]/
													<label class="NativeVlanEth4">-</label> [4]
												</span> -->

												<span class="opacity-70">Vlan 1: <label class="NativeVlanEth1">-</label></span>
												<span class="opacity-70">Vlan 2: <label class="NativeVlanEth2">-</label></span>
												<span class="opacity-70">Vlan 3: <label class="NativeVlanEth3">-</label></span>
												<span class="opacity-70">Vlan 4: <label class="NativeVlanEth4">-</label></span>
											</div>
											<div class="d-flex flex-column flex-root">
												<span class="font-weight-bolder mb-2">Board.</span>
												<!-- <span class="opacity-70 pt-2 pb-0">Board Vlan: <label class="VlanId">-</label></span> -->
												<!-- <span class="opacity-70 pt-0 pb-0">Board:</span>
												<span class="opacity-70 pt-0 pb-2">
													<label class="VlanFrameId">-</label> [F]/
													<label class="VlanSlotId">-</label> [S]/
													<label class="VlanPortId">-</label> [P]
												</span> -->
												<!-- <span class="opacity-70">Board Vlan: <label class="VlanId">-</label></span> -->
												<span class="opacity-70">Board Frame: <label class="VlanFrameId">-</label></span>
												<span class="opacity-70">Board Slot: <label class="VlanSlotId">-</label></span>
												<span class="opacity-70">Board Port: <label class="VlanPortId">-</label></span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<!-- Modal Activation-->
<div class="modal fade" id="modal-activation" data-backdrop="static"
	tabindex="-1" role="dialog" aria-labelledby="staticBackdrop"
	aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header bg-danger pt-3 pb-3">
				<h5 class="modal-title text-white bold" id="modal">Activation Clients</h5>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<i aria-hidden="true" class="ki ki-close"></i>
				</button>
			</div>
			<form class="form" id="formActivation" action="{{ route('telnet.olt-clients.activation',['','',''])}} }}" method="POST">
				{!! csrf_field() !!}
				<div class="card-body pt-3">
					<div class="mb-1" >
						<div class="form-group row mb-0">
							<label class="col-lg-3 col-form-label">Code</label>
							<div class="col-lg-9">
								<input type="text" class="form-control form-control-sm Code" id="Code" name="Code" placeholder="Enter Code" value="" />
							</div>
						</div>
					</div>
					<div class="mb-1">
						<div class="form-group row mb-0">
							<label class="col-lg-3 col-form-label">Isp Name</label>
							<div class="col-lg-9">
								<input type="text" class="form-control form-control-sm IspName" id="IspName" name="IspName" placeholder="Enter Code" value="" readonly/>
							</div>
						</div>
					</div>
					<div class="mb-1">
						<div class="form-group row mb-0">
							<label class="col-lg-3 col-form-label">Customer Name</label>
							<div class="col-lg-9">
								<input type="text" class="form-control form-control-sm CustomerName" id="CustomerName" name="CustomerName" placeholder="Enter Code" value="" readonly/>
							</div>
						</div>
					</div>
					<div class="mb-1">
						<div class="form-group row mb-0">
							<label class="col-lg-3 col-form-label">Current ONT SN</label>
							<div class="col-lg-9">
								<input type="text" class="form-control form-control-sm CurrOntSn" id="CurrOntSn" name="CurrOntSn" placeholder="Enter Code" value="" readonly/>
							</div>
						</div>
					</div>
					<div class="mb-1" >
						<div class="form-group row mb-0">
							<label class="col-lg-3 col-form-label">Script</label>
							<div class="col-lg-9">
									<pre class="container-fluid pt-2 script-activation" style="background-color: cornsilk;">-</pre>
							</div>
						</div>
					</div>
					<div class="mb-1" >
						<div class="form-group row mb-0">
							<h5 class="col-lg-3 font-weight-bolder">Last Note</h5>
							<div class="col-lg-9">
									<textarea type="text" class="form-control form-control-sm Remark" id="Remark" name="Remark" placeholder="Enter Note" value="" ></textarea>
							</div>
						</div>
					</div>
					<div class="py-1">
							<div class="form-group row mb-0">
									<h5 class="col-lg-3 font-weight-bolder">Status</h5>
									<div class="form-group row mb-0">
											<div class="col-lg-12 col-form-label">
													<div class="radio-inline">
															<label class="radio"><input type="radio" name="ClientStatus" value="Active" /><span></span>Active</label>
															<label class="radio"><input type="radio" name="ClientStatus" value="Postpone"/><span></span>Postpone</label>
															<label class="radio"><input type="radio" name="ClientStatus" value="Closed" checked="checked" /><span></span>Closed</label>
													</div>
													<span class="form-text text-muted">Some help text goes here</span>
											</div>
									</div>
							</div>
					</div>
					<div class="mb-2">
						<div class="form-group row mb-0">
							<label class="col-lg-3 col-form-label"></label>
							<div class="col-lg-9">
								<button type="submit" class="btn btn-success font-weight-bold">Run Activation</button>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- Modal Modify-->
<div class="modal fade" id="modal-modify" data-backdrop="static"
	tabindex="-1" role="dialog" aria-labelledby="staticBackdrop"
	aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header bg-danger pt-3 pb-3">
				<h5 class="modal-title text-white bold" id="modal">Modify ONT Clients</h5>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<i aria-hidden="true" class="ki ki-close"></i>
				</button>
			</div>
			<form class="form" id="formModify" action="{{ route('telnet.olt-clients.modify-sn',['','',''])}} }}" method="POST">
				{!! csrf_field() !!}
				<div class="card-body pt-3">
					<div class="mb-1" >
						<div class="form-group row mb-0">
							<label class="col-lg-3 col-form-label">Code</label>
							<div class="col-lg-9">
								<input type="text" class="form-control form-control-sm Code" id="Code" name="Code" placeholder="Enter Code" value="" required/>
							</div>
						</div>
					</div>
					<div class="mb-1" hidden>
						<div class="form-group row mb-0">
							<label class="col-lg-3 col-form-label">Host Code</label>
							<div class="col-lg-9">
								<input type="text" class="form-control form-control-sm HostCode" id="HostCode" name="HostCode" placeholder="Enter Code" value="" />
							</div>
						</div>
					</div>
					<div class="mb-1">
						<div class="form-group row mb-0">
							<label class="col-lg-3 col-form-label">Isp Name</label>
							<div class="col-lg-9">
								<input type="text" class="form-control form-control-sm IspName" id="IspName" name="IspName" placeholder="Enter Code" value="" readonly required/>
							</div>
						</div>
					</div>
					<div class="mb-1">
						<div class="form-group row mb-0">
							<label class="col-lg-3 col-form-label">Customer Name</label>
							<div class="col-lg-9">
								<input type="text" class="form-control form-control-sm CustomerName" id="CustomerName" name="CustomerName" placeholder="Enter Code" value="" readonly required/>
							</div>
						</div>
					</div>
					<div class="mb-1">
						<div class="form-group row mb-0">
							<label class="col-lg-3 col-form-label">Current ONT SN</label>
							<div class="col-lg-9">
								<input type="text" class="form-control form-control-sm CurrOntSn" id="CurrOntSn" name="CurrOntSn" placeholder="Enter Code" value="" readonly required/>
							</div>
						</div>
					</div>
					<div class="mb-1" >
						<div class="form-group row mb-0">
							<label class="col-lg-3 col-form-label">Script</label>
							<div class="col-lg-9">
									<pre class="container-fluid pt-2 script-modify" style="background-color: cornsilk;">-</pre>
							</div>
						</div>
					</div>
					<div class="mb-2">
							<div class="form-group row mb-0">
									<div class="col-lg-3">
										<button type="button" class="btn btn-info font-weight-bold" id="btn-auto-find">Auto Find ONT</button>
									</div>
									<div class="col-lg-9">
											<select class="form-control form-control-sm" id="ont-list-mdfy" name="ont-list-mdfy" tabindex="-1" aria-hidden="true" style="width:100%" required>
													 @isset ($ont_list)
														 @foreach($ont_list as $ont)
															<option value="{{ $ont->Sn }}" @isset ($clients->OntSn) {{ ($clients->OntSn == $ont->Sn) ? 'true' : 'false' }} @endisset > {{ $ont->Sn }}-{{ $ont->Hostname }}</option>
														 @endforeach
														 <option value="None" selected>All Ont {{count($ont_list)}}</option>
													 @endisset
											</select>
									</div>
							</div>
					</div>
					<div class="py-1">
							<div class="form-group row mb-0">
									<h5 class="col-lg-3 font-weight-bolder">Status</h5>
									<div class="form-group row mb-0">
											<div class="col-lg-12 col-form-label">
													<div class="radio-inline">
															<label class="radio"><input type="radio" name="noteflag" value="Maintenance" checked /><span></span>Maintenance</label>
															<label class="radio"><input type="radio" name="noteflag" value="Trouble"/><span></span>Trouble</label>
													</div>
													<span class="form-text text-muted">Some help text goes here</span>
											</div>
									</div>
							</div>
					</div>
					<div class="mb-1" >
						<div class="form-group row mb-0">
							<h5 class="col-lg-3 font-weight-bolder">Last Note</h5>
							<div class="col-lg-9">
									<textarea type="text" class="form-control form-control-sm Remark" id="Remark" name="Remark" placeholder="Enter Note" value=""  required></textarea>
							</div>
						</div>
					</div>
					<div class="mb-2">
						<div class="form-group row mb-0">
							<label class="col-lg-3 col-form-label"></label>
							<div class="col-lg-9">
								<button type="submit" class="btn btn-success font-weight-bold">Run Modify</button>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- Modal Modify-->
<div class="modal fade" id="modal-modify-service" data-backdrop="static"
	tabindex="-1" role="dialog" aria-labelledby="staticBackdrop"
	aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header bg-danger pt-3 pb-3">
				<h5 class="modal-title text-white bold" id="modal">Modify Traffic Clients</h5>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<i aria-hidden="true" class="ki ki-close"></i>
				</button>
			</div>
			<form class="form" id="formModifyTraffic" action="{{ route('telnet.olt-clients.modify-traffic',['','',''])}} }}" method="POST">
				{!! csrf_field() !!}
				<div class="card-body pt-3">
					<div class="mb-1" hidden>
						<div class="form-group row mb-0">
							<label class="col-lg-3 col-form-label">Host Code</label>
							<div class="col-lg-9">
								<input type="text" class="form-control form-control-sm HostCode" name="HostCode" placeholder="Enter Code" value="" />
							</div>
						</div>
					</div>
					<div class="mb-1" >
						<div class="form-group row mb-0">
							<label class="col-lg-3 col-form-label">Code</label>
							<div class="col-lg-9">
								<input type="text" class="form-control form-control-sm ServicePortId" name="ServicePortId" placeholder="Enter ServicePortId" value="" required/>
							</div>
						</div>
					</div>
					<div class="mb-1">
						<div class="form-group row mb-0">
							<label class="col-lg-3 col-form-label">Isp Name</label>
							<div class="col-lg-9">
								<input type="text" class="form-control form-control-sm IspName" id="IspName" name="IspName" placeholder="Enter Code" value="" readonly/>
							</div>
						</div>
					</div>
					<div class="mb-1">
						<div class="form-group row mb-0">
							<label class="col-lg-3 col-form-label">Customer Name</label>
							<div class="col-lg-9">
								<input type="text" class="form-control form-control-sm CustomerName" id="CustomerName" name="CustomerName" placeholder="Enter Code" value="" readonly/>
							</div>
						</div>
					</div>
					<div class="mb-1">
						<div class="form-group row mb-0">
							<label class="col-lg-3 col-form-label">Inner Vlan</label>
							<div class="col-lg-9">
								<input type="text" class="form-control form-control-sm InnerVlan" name="InnerVlan" placeholder="Enter InnerVlan" value="" readonly required/>
							</div>
						</div>
					</div>
					<div class="mb-1">
						<div class="form-group row mb-0">
							<label class="col-lg-3 col-form-label">User Vlan</label>
							<div class="col-lg-9">
								<input type="text" class="form-control form-control-sm UserVlan" name="UserVlan" placeholder="Enter UserVlan" value="" readonly required/>
							</div>
						</div>
					</div>
					<div class="mb-1">
						<div class="form-group row mb-0">
							<label class="col-lg-3 col-form-label">Modify Traffic</label>
							<div class="col-lg-4">
								<input type="text" class="form-control form-control-sm TrafficTable" name="TrafficTable" placeholder="Enter TrafficTable" value="" readonly/>
							</div>
							<label class="col-lg-1 col-form-label">To</label>
							<div class="col-lg-4">
								<input type="text" class="form-control form-control-sm NewTrafficTable" name="NewTrafficTable" placeholder="Enter TrafficTable" value=""/>
							</div>
						</div>
					</div>
					<div class="mb-1" >
						<div class="form-group row mb-0">
							<label class="col-lg-3 col-form-label">Script</label>
							<div class="col-lg-9">
									<pre class="container-fluid pt-2 script-modify-traffic" style="background-color: cornsilk;">-</pre>
							</div>
						</div>
					</div>
					<div class="mb-2">
						<div class="form-group row mb-0">
							<label class="col-lg-3 col-form-label"></label>
							<div class="col-lg-9">
								<button type="submit" class="btn btn-success font-weight-bold">Run Modify</button>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

@endsection

{{-- Styles Section --}}
@section('styles')
<link rel="stylesheet"
	href="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.css">
<link rel="stylesheet"
	href="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.min.css">
<link rel="stylesheet"
	href="{{ config('app.url') }}plugins/custom/datatables/datatables.bundle.css">
<link rel="stylesheet" href="{{ config('app.url') }}css/inject.css">
@endsection
{{-- Scripts Section --}}

@section('scripts')
@include('inc.confirm-delete-modal')
<script
	src="{{ config('app.url') }}global/vendor/datatables/jquery.dataTables.js"></script>
<script
	src="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.js"></script>
<script
	src="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.js"></script>
<script type="text/javascript">

$(document).ready(function() {
	var id_clients ="";
	var sn_clients ="";
	var ht_clients ="";
  $('[data-switch=true]').bootstrapSwitch();
  $('.select2').select2();
	$('#ont-list-mdfy').select2();
	$("div.datesearchbox").html('<div class="input-group"> <div class="input-group-addon"> <i class="glyphicon glyphicon-calendar"></i> </div><input type="text" class="form-control pull-right" id="datesearch" placeholder="Search by date range"> </div>');
	document.getElementsByClassName("datesearchbox")[0].style.textAlign = "center";
	$("#datesearch").attr("readonly",true);
	$('#datesearch').daterangepicker({
		 autoUpdateInput: false
	 });

	//menangani proses saat apply date range
	 $('#datesearch').on('apply.daterangepicker', function(ev, picker) {
			$(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
			$("#start-date").val(picker.startDate.format('YYYY-MM-DD'));
			$("#end-date").val(picker.endDate.format('YYYY-MM-DD'));
			refresh_table();
	 });

	 $('#datesearch').on('cancel.daterangepicker', function(ev, picker) {
		 $(this).val('');
		 $("#start-date").val('');
		 $("#end-date").val('');
		 refresh_table();
	 });

	 $('#SearchHostCode, #SearchClientsStatus').on('change', function(ev) {
		 refresh_table();
	 });

 });

  var table = $('#datatable').dataTable({
    pageLength: 5,
    scrollX: true,
		scrollCollapse: true,
		fixedColumns:   {
        leftColumns: 2,
        rightColumns: -1
    },
    searchDelay: 800,
		// pagingTypeSince : 'numbers',
    // pagingType      : 'full_numbers',
		keys: true,
		// render: true,
		// stateSave: true,
		serverSide: true,
    processing: true,
    searching: true,
		// select: true,
		// rowId: 'id',
    lengthMenu: [[5, 10, 25, 50, 100, 200], [5, 10, 25, 50, 100, 200]],
    ajax: {
      method: 'POST',
      url : '{{ route('olt.clients.data') }}',
      headers: {
        'X-CSRF-TOKEN': '{{ csrf_token() }}'
      },
			data: function (d) {
				d.from_date = $("#start-date").val();
				d.to_date = $(" #end-date").val();
				d.HostCode = $(" #SearchHostCode").val();
				d.ClientStatus = $(" #SearchClientsStatus").val();
			},
    },
    columns: [
      {title: "No", data: 'DT_RowIndex', defaultContent: '-', class: 'text-center dt-body-nowrap', orderable: false, searchable: false},
      {title: "Code", data: 'Code', defaultContent: '-', class: 'text-center dt-body-nowrap', searchable: false},
      {title: "Host Code", data: 'HostCode', defaultContent: '-', class: 'text-center dt-body-nowrap', searchable: false},
      {title: "Hostname", data: 'Hostname', defaultContent: '-', class: 'text-center dt-body-nowrap', searchable: false},
			{title: "OntSn ", data: 'OntSn', defaultContent: '-', class: 'text-center dt-body-nowrap', searchable: false},
      {title: "Isp Name", data: 'IspName', defaultContent: '-', class: 'text-center dt-body-nowrap', searchable: false},
      {title: "Customer Name", data: 'CustomerName', defaultContent: '-', class: 'text-center dt-body-nowrap', searchable: false },
      {title: "Customer Email", data: 'CustomerEmail', defaultContent: '-', class: 'text-center dt-body-nowrap', searchable: false},
      {title: "Join Date", data: 'CreatedDate', defaultContent: '-', class: 'text-center dt-body-nowrap', searchable: false},
      {title: "Actions", data: 'action', orderable: false, responsivePriority: -1},
    ],
		dom:  "<'row'<'col-sm-5'l><'col-sm-3' <'datesearchbox'>><'col-sm-3'f>>" +
          "<'row'<'col-sm-12'tr>>" +
          "<'row'<'col-sm-5'i><'col-sm-7'p>>",
    order: [[1, 'asc']],
    bStateSave: true,
		createdRow: function( row, data, dataIndex){
        if(!data.OntRun){
            $(row).addClass('bg-warning');
        }
    },
    columnDefs: [
      {
        render: function ( data, type, row ) {
						var url = "{{route('olt.clients.page', 'prm_code')}}";
						var url_destroy = "{{route('olt.clients.destroy', 'prm_code')}}";
						var code = "'"+row.Code+"'";
						var status = "'"+row.ClientStatus+"'";
						var modify = "'None'";
						var html = ""
        				html+= '<a onclick="activation('+code+','+status+')" class="btn btn-icon btn-light btn-hover-info btn-sm" data-toggle="tooltip" data-placement="top" title="Activation"><span class="svg-icon svg-icon-md svg-icon-info"><i class="far flaticon-technology-2 icon-lg"></i></span></a>';
        				html+= '<a onclick="modify_ont('+code+','+modify+')" class="btn btn-icon btn-light btn-hover-warning btn-sm" data-toggle="tooltip" data-placement="top" title="Modify"><span class="svg-icon svg-icon-md svg-icon-warning"><i class="far flaticon2-chronometer icon-lg"></i></span></a>';
        				html+= '<a href="'+url.replaceAll('prm_code',row.Code)+'" class="btn btn-icon btn-light btn-hover-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Setting"><span class="svg-icon svg-icon-md svg-icon-primary"><i class="far flaticon-interface-8 icon-lg"></i></span></a>';
        				html+= '<a data-href="'+url_destroy.replaceAll('prm_code',row.Code)+'" class="btn btn-icon btn-light btn-hover-danger btn-sm" "data-toggle="tooltip" data-placement="top" title="Delete" data-toggle="modal" data-target="#confirm-delete-modal" ><span class="svg-icon svg-icon-md svg-icon-danger"><i class="far flaticon-delete icon-lg"></i></span></a>';
						return html;
        },
        targets: [-1],
        className: 'text-center dt-body-nowrap'
      },
    ],
    initComplete: function() {
      $('.tl-tip').tooltip();
    }
  });

  $('#check-power').on( 'click', function () {
		if (id_clients != ""){
			$("#data-clients").loading("start");
      $.ajax({
          url: "{{ route('telnet.olt-config.checkpower',['',''])}}/"+ht_clients+"/"+sn_clients,
          type: "GET",
          success: function (datas) {
             $("#data-clients").loading("stop");
             showDialog.show(datas.data, false);
          },
          error: function (xhr, status, error) {
             $("#data-clients").loading("stop");
             // showDialog.show(xhr.status + " " + status + " " + error, false);
          }
      });
    }else{
      showDialog.show("Please chose SN!!", false);
    }
  });

	$('#btn-auto-find').on( 'click', function () {
		$('#ont-list-mdfy').empty();
		$('#ont-list-mdfy').append('<option value="" selected> WAITING LOAD ONT....</option>');
		$.ajax({
				url: "{{ route('telnet.olt-config.check-config',['',''])}}/"+$('#HostCode').val()+"/ont-autofind",
				type: "GET",
				success: function (response) {
					$('#ont-list-mdfy').empty();
					if(response.data["ont-autofind"]){
						for (var i in response.data["ont-autofind"]){
								var data = response.data["ont-autofind"][i];
								$('#ont-list-mdfy').append('<option value="'+data.Code +'">'+data.Sn+'-'+data.Hostname+'</option>');
						}
						$('#ont-list-mdfy').append('<option value="" selected>All Ont '+(Number(i)+1)+'</option>');
					}else{
						$('#ont-list-mdfy').append('<option value="" selected>  Ont Not Found</option>');
					}
					$('#ont-list-mdfy').val("").trigger('change');
					$("#ont-group").loading("stop");
				},
				error: function (xhr, status, error) {
					 $("#ont-group").loading("stop");
					// showDialog.show(xhr.status + " " + status + " " + error, false);
				}
		});
	});

	$('input[type=radio][name="ClientStatus"]').change(function() {
		activation($("#Code").val(),$(this).val());
  });

  function activation(id = "", config = "") {
			$('.script-activation').html("LOADING COMMAND ACTIVATION.......<hr/>");
			$("#formActivation").attr("action", "{{ route('telnet.olt-clients.activation',['','',''])}}/"+id+"/"+config+"/1");
      if (id !== "") {
          $.ajax({
              url: "{{ route('telnet.olt-clients.activation',['','',''])}}/"+id+"/"+config+"/0",
              type: "POST",
							data : $('#formActivation').serialize() ,
              headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
              },
              success: function (response) {
									$('#modal-activation').modal('show');
                  $('.Code').attr("readonly", true);

                  $('.Code').val(response.data['clients'].Code);
                  $('.IspName').val(response.data['clients'].IspName);
                  $('.CustomerName').val(response.data['clients'].CustomerName);
                  $('.HostCode').val(response.data['clients'].HostCode);
                  $('.CurrOntSn').val(response.data['clients'].OntSn);
                  $('.Remark').text(response.data['clients'].Remark);
									$("input[name=ClientStatus][value=" + response.data['clients'].ClientStatus + "]").prop('checked', true);

                  $('.script-activation').text("-");
									var script = "";
									script = script+"START COMMAND ACTIVATION:<hr/>";
									for (i in response.data['script']){
										if(script != "" || script != null){
											script = script+"&nbsp;"+response.data['script'][i]+"<br/>";
										}
									}
									script = script+"<br/><br/>";
									$('.script-activation').html(script);
              },
              error: function (xhr, status, error) {
                  // showDialog.show(xhr.status + " " + status + " " + error, false);
              }
          });
      } else {
				$('#modal-activation').modal('hide');
				return;
      }
  }

	$('#ont-list-mdfy').on('change', function() {
		if($(this).val() != ""){
				modify_ont($("#Code").val(),$(this).val());
		}
	});

  function modify_ont(id = "", sn = "") {
			$('.script-modify').html("LOADING COMMAND MODIFY......<hr/>");
			$("#formModify").attr("action", "{{ route('telnet.olt-clients.modify-sn',['','',''])}}/"+id+"/"+sn+"/1");
			$('#modal-modify').modal('show');
			$('#ont-list-mdfy').empty();
			$('#ont-list-mdfy').append('<option value="" selected> WAITING LOAD ONT....</option>');
      if (id !== "") {
          $.ajax({
              url: "{{ route('telnet.olt-clients.modify-sn',['','',''])}}/"+id+"/"+sn+"/0",
              type: "POST",
							data : $('#formModify').serialize() ,
              headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
              },
              success: function (response) {
                  $('.Code').attr("readonly", true);
                  $('.Code').val(response.data['clients'].Code);
                  $('.IspName').val(response.data['clients'].IspName);
                  $('.CustomerName').val(response.data['clients'].CustomerName);
                  $('.HostCode').val(response.data['clients'].HostCode);
                  $('.CurrOntSn').val(response.data['clients'].OntSn);
                  $('.Remark').text(response.data['clients'].Remark);

									$('.script-modify').text("-");
									var script = "";
									script = script+"START COMMAND MODIFY:<hr/>";
									for (i in response.data['script']){
										if(script != "" || script != null){
											script = script+"&nbsp;"+response.data['script'][i]+"<br/>";
										}
									}
									script = script+"<br/><br/>";
									$('.script-modify').html(script);
									$.ajax({
											url: "{{ route('telnet.olt-config.check-config',['',''])}}/"+response.data['clients'].HostCode+"/ont-autofind?data=true",
											type: "GET",
											success: function (response) {
												$('#ont-list-mdfy').empty();
												if(response.data["ont-autofind"]){
													for (var i in response.data["ont-autofind"]){
															var data = response.data["ont-autofind"][i];
															$('#ont-list-mdfy').append('<option value="'+data.Code +'">'+data.Sn+'-'+data.Hostname+'</option>');
													}
													$('#ont-list-mdfy').append('<option value="" selected>All Ont '+(Number(i)+1)+'</option>');
												}else{
													$('#ont-list-mdfy').append('<option value="" selected>  Ont Not Found</option>');
												}
												$('#ont-list-mdfy').val("").trigger('change');
												$("#ont-group").loading("stop");
											},
											error: function (xhr, status, error) {
												 $("#ont-group").loading("stop");
												// showDialog.show(xhr.status + " " + status + " " + error, false);
											}
									});
              },
              error: function (xhr, status, error) {
                  // showDialog.show(xhr.status + " " + status + " " + error, false);
              }
          });
      } else {
				$('#modal-modify').modal('hide');
				return;
      }
  }

	$('.NewTrafficTable').keyup(function() {
		if($(this).val() != ""){
				modify_traffic($(".ServicePortId").val(),$(this).val());
		}
	});

  function modify_traffic(id = "", traffic = "") {
		$('.script-modify-traffic').html("LOADING MODIFY TRAFFIC......<hr/>");
		$("#formModifyTraffic").attr("action", "{{ route('telnet.olt-clients.modify-traffic',['','',''])}}/"+id+"/"+traffic+"/1");
		if(!id && id == ""){
			id = "None";
		}
		if(!traffic && traffic == ""){
			traffic = "None";
		}
		$.ajax({
				url: "{{ route('telnet.olt-clients.modify-traffic',['','',''])}}/"+id+"/"+traffic+"/0",
				type: "POST",
				data : $('#formModify').serialize() ,
				headers: {
					'X-CSRF-TOKEN': '{{ csrf_token() }}'
				},
				success: function (response) {
						$('.ServicePortId').attr("readonly", true);
						$('.ServicePortId').val(response.data['service'].Code);
						$('.InnerVlan').val(response.data['service'].InnerVlan);
						$('.UserVlan').val(response.data['service'].UserVlan);
						$('.TrafficTable').val(response.data['service'].TrafficTable);
						$('.CustomerName').val(response.data['clients'].CustomerName);
						$('.IspName').val(response.data['clients'].IspName);
						$('.TrafficTable').text(response.data['clients'].TrafficTable);
						$('.NewTrafficTable').text("None");
						$('.script-modify-traffic').text("-");
						var script = "";
						script = script+"START MODIFY TRAFFIC:<hr/>";
						for (i in response.data['script']){
							if(script != "" || script != null){
								script = script+"&nbsp;"+response.data['script'][i]+"<br/>";
							}
						}
						script = script+"<br/><br/>";
						$('.script-modify-traffic').html(script);
						$('#modal-modify-service').modal('show');
						// if(sn == "" || sn == "None"){
						// 		setTimeout(() => { $('#btn-auto-find').trigger('click'); }, 115);
						// }
				},
				error: function (xhr, status, error) {
						// showDialog.show(xhr.status + " " + status + " " + error, false);
				}
		});
	}

	function get_clientstools(config, setting) {
				$("#datatable_general-info tbody>tr").remove();
				$(".ont-version").text("-");
				$(".general-info").text("-");
				$(".ont-info").text("-");
				$(".wan-info").text("-");
				$(".optical-info").text("-");
				$(".register-info").text("-");
				$(".mac-address").text("-");
				$(".port-state").text("-");
				$(".port-attribute").text("-");
				$(".host-current-config").text("-");
				$(".fec-crc-error").text("-");
			if (id_clients == ""){
				showDialog.show("Data Clients on grid not selected!");
				return;
			}
			$("#data-clients").loading("start");
			$.ajax({
				url: "{{ route('telnet.olt-config.clientstools',['','',''])}}/"+id_clients+"/"+config+"/"+setting,
				type: "GET",
				success: function (res) {
					if(config == "general-info"){
						var rs = '';
						if (res.data["general-info"][0]){
							rs = res.data["general-info"][0];
							$(".Flag").text(rs.Flag);
							$(".Run").text(rs.Run);
							$(".LastDownCause").text(rs.LastDownCause);
							$(".LastUpTime").text(rs.LastUpTime);
							$(".LastDownTime").text(rs.LastDownTime);
							$(".LastDyingGaspTime").text(rs.LastDyingGaspTime);
							$(".OntOnlineDuration").text(rs.OntOnlineDuration);
							$(".OntDistance").text(rs.OntDistance);
						}else{
							$(".Flag").text("-");
							$(".Run").text("-");
							$(".LastDownCause").text("-");
							$(".LastUpTime").text("-");
							$(".LastDownTime").text("-");
							$(".LastDyingGaspTime").text("-");
							$(".OntOnlineDuration").text("-");
							$(".OntDistance").text("-");
						}
						if (res.data["general-info"][1]){
							rs = res.data["general-info"][1];
							$(".OltRxOntOpticalPower").text(rs.OltRxOntOpticalPower);
							$(".RxOpticalPower").text(rs.RxOpticalPower);
							$(".TxOpticalPower").text(rs.TxOpticalPower);
						}else{
							$(".OltRxOntOpticalPower").text("-");
							$(".RxOpticalPower").text("-");
							$(".TxOpticalPower").text("-");
						}
						if (res.data["general-info"][2]){
							rs = res.data["general-info"][2];
							$(".ServiceType").text(rs.ServiceType);
							$(".ConnectionType").text(rs.ConnectionType);
							$(".ConnectionStatus").text(rs.ConnectionStatus);
							$(".MacAddress").text(rs.MacAddress);
						}else{
							$(".ServiceType").text("-");
							$(".ConnectionType").text("-");
							$(".ConnectionStatus").text("-");
							$(".MacAddress").text("-");
						}
						$("#datatable_general-info tbody>tr").remove();
						if(res.data["general-info"][3]){
								for (var i in res.data["general-info"][3]){
										var table = document.getElementById("datatable_general-info").getElementsByTagName('tbody')[0];
										var row = table.insertRow(-1);
										row.insertCell(0).innerHTML = '<label class="col-12 text-center">'+res.data["general-info"][3][i].Speed+'</label>';
										row.insertCell(1).innerHTML = '<label class="col-12 text-center">'+res.data["general-info"][3][i].Duplex+'</label>';
										row.insertCell(2).innerHTML = '<label class="col-12 text-center">'+res.data["general-info"][3][i].LinkState+'</label>';
								}
						}
					}else{
					$("#datatable_general-info tbody>tr").remove();
					 if(res.data["tools"]){
						if(config == "ont-version"){
							rs = res.data["tools"].Version;
						}
						if(config == "ont-info"){
							rs = res.data["tools"].OntInfo;
						}
						if(config == "wan-info"){
							rs = res.data["tools"].WanInfo;
						}
						if(config == "optical-info"){
							rs = res.data["tools"].Opticalinfo;
						}
						if(config == "register-info"){
							rs = res.data["tools"].Registerinfo;
						}
						if(config == "mac-address"){
							rs = res.data["tools"].MacAddress;
						}
						if(config == "port-state"){
							rs = res.data["tools"].PortState;
						}
						if(config == "port-attribute"){
							rs = res.data["tools"].PortAttribute;
						}
						if(config == "fec-crc-error"){
							rs = res.data["tools"].FecCrcError;
						}
						$("."+config).html(rs);
					}
				}
					$(	"#data-clients").loading("stop");
			},
			error: function (xhr, status, error) {
				 $("#data-clients").loading("stop");
				// showDialog.show(xhr.status + " " + status + " " + error, false);
			}
		});
	};

  $('#datatable tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
						id_clients = "";
						sn_clients = "";
						ht_clients = "";
						$("#datatable_vlan_service tbody>tr").remove();
						$("#datatable_vlan_service_temp tbody>tr").remove();
						$("#datatable_general-info tbody>tr").remove();
						$(".CustomerServicePort").html("-");
						$(".CustomerName").html("-");
						$(".CustomerEmail").text("-");
						$(".CustomerPhone").text("-");
						$(".CustomerAddress").text("-");
						$(".CustomerProductName").text("-");
						$(".CustomerProductCategory").text("-");
						$(".CustomerProductNotes").text("-");
						$(".OntSn").text("-");
						$(".FrameId").text("-");
						$(".SlotId").text("-");
						$(".PortId").text("-");
						$(".VlanDownLink").text("-");
						$(".NativeVlanEth1").text("-");
						$(".NativeVlanEth2").text("-");
						$(".NativeVlanEth3").text("-");
						$(".NativeVlanEth4").text("-");
						$(".OntSrvProfileId").text("-");
						$(".VlanId").text("-");
						$(".VlanFrameId").text("-");
						$(".VlanSlotId").text("-");
						$(".VlanPortId").text("-");
						$(".Hostname").text("-");
						$(".IpAddress").text("-");
						$(".IspAddress").text("-");


						$(".ont-version").text("-");
						$(".general-info").text("-");
						$(".ont-info").text("-");
						$(".wan-info").text("-");
						$(".optical-info").text("-");
						$(".register-info").text("-");
						$(".mac-address").text("-");
						$(".port-state").text("-");
						$(".port-attribute").text("-");
						$(".fec-crc-error").text("-");
						$(".host-current-config").text("-");
						$(".client-status").text("-");
						$(".client-response").text("-");
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
		      	var id = $(this).find("td:eq(1)").html();
		      	var sn = $(this).find("td:eq(4)").html();
		      	var host = $(this).find("td:eq(2)").html();
						id_clients = id;
						sn_clients = sn;
						ht_clients = host;
						$("#datatable_vlan_service tbody>tr").remove();
						$("#datatable_vlan_service_temp tbody>tr").remove();
						$("#datatable_general-info tbody>tr").remove();
					 	$("#data-clients").loading("start");
						get_clientstools('general-info', 0);
						$.ajax({
							url: "{{ route('olt.clients.show','')}}/"+id,
							type: "GET",
							success: function (datas) {
								if(datas.data["clients"]){
									var icon = '<i class="flaticon2-cancel text-danger icon-md ml-2" title="Non Active"></i>';
									$(".OntFlag").text("Not Set");
									if(datas.data["clients"].OntConfig){
										$(".OntConfig").text(datas.data["clients"].OntConfig);
									}else{
										$(".OntConfig").text("Not Set");
									}
									if(datas.data["clients"].OntFlag){
										if(datas.data["clients"].OntFlag == "active"){
											icon = '<i class="flaticon2-correct text-success icon-md ml-2" title="Active"></i>';
											$(".OntFlag").text(datas.data["clients"].OntFlag);
										}else{
											icon = '<i class="flaticon2-warning text-warning icon-md ml-2" title="'+datas.data["clients"].OntFlag+'"></i>';
											$(".OntFlag").text(datas.data["clients"].OntFlag);
										}
									}
									$(".CustomerName").html(datas.data["clients"].CustomerName+icon);
									$(".CustomerEmail").text(datas.data["clients"].CustomerEmail);
									$(".CustomerPhone").text(datas.data["clients"].CustomerPhone1);
									$(".CustomerAddress").text(datas.data["clients"].CustomerAddress);
									$(".CustomerProductName").text(datas.data["clients"].CustomerProductName);
									$(".CustomerProductCategory").text(datas.data["clients"].CustomerProductCategory);
									$(".CustomerProductNotes").text(datas.data["clients"].CustomerProductNotes);
									$(".OntSn").text(datas.data["clients"].OntSn);
									$(".OntId").text(datas.data["clients"].OntId);

									var OntInfo = "";
									OntInfo = OntInfo+"F | S | P [";
									OntInfo = OntInfo+datas.data["clients"].FrameId+" / ";
									OntInfo = OntInfo+datas.data["clients"].SlotId+" / ";
									OntInfo = OntInfo+datas.data["clients"].PortId+"] ";
									$(".OntInfo").text(OntInfo);
									var NativeVlan = "";
									NativeVlan = NativeVlan+"1 | 2 | 3 | 4 [";
									NativeVlan = NativeVlan+datas.data["clients"].NativeVlanEth1+" / ";
									NativeVlan = NativeVlan+datas.data["clients"].NativeVlanEth2+" / ";
									NativeVlan = NativeVlan+datas.data["clients"].NativeVlanEth3+" / ";
									NativeVlan = NativeVlan+datas.data["clients"].NativeVlanEth4+"] ";
									$(".NativeVlan").text(NativeVlan);

									var BoardVlan = "";
									BoardVlan = BoardVlan+"F | S | P [";
									BoardVlan = BoardVlan+datas.data["clients"].BoardFrameId+" / ";
									BoardVlan = BoardVlan+datas.data["clients"].BoardSlotId+" / ";
									BoardVlan = BoardVlan+datas.data["clients"].BoardPortId+"] ";
									$(".BoardVlan").text(BoardVlan);

									$(".CustomerStatusClients").text(datas.data["clients"].ClientStatus);
									$(".CustomerRemark").text(datas.data["clients"].Remark);
									$(".UpdatedDate").text(datas.data["clients"].UpdatedDate);

									$(".FrameId").text(datas.data["clients"].FrameId);
									$(".SlotId").text(datas.data["clients"].SlotId);
									$(".PortId").text(datas.data["clients"].PortId);
									$(".VlanDownLink").text(datas.data["clients"].VlanDownLink);
									$(".NativeVlanEth1").text(datas.data["clients"].NativeVlanEth1);
									$(".NativeVlanEth2").text(datas.data["clients"].NativeVlanEth2);
									$(".NativeVlanEth3").text(datas.data["clients"].NativeVlanEth3);
									$(".NativeVlanEth4").text(datas.data["clients"].NativeVlanEth4);
									$(".OntLineProfileId").text(datas.data["clients"].OntLineProfileId);
									$(".OntSrvProfileId").text(datas.data["clients"].OntSrvProfileId);
									$(".VlanId").text(datas.data["clients"].VlanId);
									$(".VlanFrameId").text(datas.data["clients"].VlanFrameId);
									$(".VlanSlotId").text(datas.data["clients"].VlanSlotId);
									$(".VlanPortId").text(datas.data["clients"].VlanPortId);
									$(".Hostname").text(datas.data["clients"].Hostname);
									$(".IpAddress").text(datas.data["clients"].IpAddress);
									$(".IspAddress").text(datas.data["clients"].IspAddress);
									$(".client-status").text(datas.data["clients"].ClientStatus);
									$(".client-response").text(datas.data["clients"].Response);
									$(".host-current-config").text(datas.data["host"].CurrentConfig);

								// 	if(datas.data["list_clientstools"]){
								// 		$(".ont-version").text(datas.data["list_clientstools"].Version);
								// 		$(".ont-info").text(datas.data["list_clientstools"].OntInfo);
								// 		$(".wan-info").text(datas.data["list_clientstools"].WanInfo);
								// 		$(".optical-info").text(datas.data["list_clientstools"].Opticalinfo);
								// 		$(".register-info").text(datas.data["list_clientstools"].Registerinfo);
								// 		$(".mac-address").text(datas.data["list_clientstools"].MacAddress);
								// 		$(".port-state").text(datas.data["list_clientstools"].PortState);
								// 		$(".port-attribute").text(datas.data["list_clientstools"].PortAttribute);
								// 	}else{
								// 		$(".ont-version").text("-");
								// 		$(".ont-info").text("-");
								// 		$(".wan-info").text("-");
								// 		$(".optical-info").text("-");
								// 		$(".register-info").text("-");
								// 		$(".mac-address").text("-");
								// 		$(".port-state").text("-");
								// 		$(".port-attribute").text("-");
								// 		$(".host-current-config").text("-");
								// 	}
								}
								if(datas.data["clients_vlan_service"]){

									$(".CustomerServicePort").text(datas.data["clients_vlan_service"].length+" Vlan");
									for (var i in datas.data["clients_vlan_service"]){
									    var table = document.getElementById("datatable_vlan_service").getElementsByTagName('tbody')[0];
									    var row = table.insertRow(-1);
									    var cell1 = row.insertCell(0).innerHTML = '<label class="col-12 text-center">'+datas.data["clients_vlan_service"][i].ServicePortId+'</label>';
									    var cell2 = row.insertCell(1).innerHTML = '<label class="col-12 text-center">'+datas.data["clients_vlan_service"][i].Vlan+'</label>';
									    var cell3 = row.insertCell(2).innerHTML = '<label class="col-12 text-center">'+datas.data["clients_vlan_service"][i].GemPort+'</label>';
									    var cell4 = row.insertCell(3).innerHTML = '<label class="col-12 text-center">'+datas.data["clients_vlan_service"][i].UserVlan+'</label>';
									    var cell5 = row.insertCell(4).innerHTML = '<label class="col-12 text-center">'+datas.data["clients_vlan_service"][i].TagTransform+'</label>';
									    var cell6 = row.insertCell(5).innerHTML = '<label class="col-12 text-center">'+datas.data["clients_vlan_service"][i].InnerVlan+'</label>';
									    var cell7 = row.insertCell(6).innerHTML = '<label class="col-12 text-center">'+datas.data["clients_vlan_service"][i].TrafficTable+'</label>';
											var code = "'"+datas.data["clients_vlan_service"][i].Code+"'";
											var traffic = "'None'";
											if(datas.data["clients_vlan_service"][i].TrafficTable){
												traffic = "'"+datas.data["clients_vlan_service"][i].TrafficTable+"'";
											}
									    var cell7 = row.insertCell(7).innerHTML = '<a onclick="modify_traffic('+code+','+traffic+')" class="btn btn-icon btn-light btn-hover-warning btn-sm" data-toggle="tooltip" data-placement="top" title="Modify"><span class="svg-icon svg-icon-md svg-icon-warning"><i class="far flaticon2-chronometer icon-lg"></i></span></a>';
									}
								}

							 	$("#data-clients").loading("stop");
						},
						error: function (xhr, status, error) {
							 id_clients = "";
							 sn_clients = "";
							 $("#data-clients").loading("stop");
							// showDialog.show(xhr.status + " " + status + " " + error, false);
						}
					});
        }
    });


		function refresh_table() {
				$('#datatable').dataTable().ajax.reload();
		}

  function format_date(val) {
      var date_arr = val.split(" ");
      var time = date_arr[1];
      var date_id_arr = date_arr[0].split("-");
      var date_fix = date_id_arr[2] + "-" + date_id_arr[1] + "-" + date_id_arr[0];
      var date_transaction = date_fix + " " + time;
      return date_transaction;
  }

	function refresh_table() {
      table.fnDraw();
  }
</script>
@endsection
