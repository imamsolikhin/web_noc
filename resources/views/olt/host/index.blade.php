@extends('layout.default')
@section('content')
<div
	class="subheader py-2 {{ Metronic::printClasses('subheader', false) }}"
	id="kt_subheader">
	<div
		class="{{ Metronic::printClasses('subheader-container', false) }} d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
		<div class="d-flex align-items-baseline flex-wrap mr-5">
			<h5 class="text-dark font-weight-bold my-1 mr-5">Host</h5>
			<ul
				class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
				<li class="breadcrumb-item"><a href="{{ route('dashboard') }}"
					class="text-muted">Dashboard</a></li>
				<li class="breadcrumb-item"><a href="{{ route('olt.host.index') }}"
					class="text-muted">Olt</a></li>
				<li class="breadcrumb-item"><a href="#view" class="text-muted">Host</a>
				</li>
			</ul>
		</div>
	</div>
</div>

<!-- Modal-->
<div class="modal fade" id="modal-form" data-backdrop="static"
	tabindex="-1" role="dialog" aria-labelledby="staticBackdrop"
	aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-danger pt-3 pb-3">
				<h5 class="modal-title text-white bold" id="modal">New Host Optical
					Line Terminal</h5>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<i aria-hidden="true" class="ki ki-close"></i>
				</button>
			</div>
			<form class="form" id="form-input"
				action="{{ route('olt.host.store') }}" method="POST">
				<div class="card-body pt-3">
					<div class="mb-1" hidden>
						<div class="form-group row mb-0">
							<label class="col-lg-4 col-form-label">Code</label>
							<div class="col-lg-8">
								{!! csrf_field() !!} <input type="hidden"
									class="form-control form-control-sm" id="method" name="_method"
									placeholder="Enter method" value="POST" /> <input type="text"
									class="form-control form-control-sm" id="Code" name="Code"
									placeholder="Enter Code"
									value="@isset($host->Code)){!! $host->Code !!}@endisset " />
							</div>
						</div>
					</div>
					<div class="mb-1">
						<div class="form-group row mb-0">
							<label class="col-lg-4 font-weight-bolder">Hostname <span class="text-danger">*</span></label>
							<div class="col-lg-8">
								<input type="text" class="form-control form-control-sm" id="Hostname" name="Hostname" placeholder="Enter Hostname" value="" required/>
							</div>
						</div>
					</div>
					<div class="mb-1">
						<div class="form-group row mb-0" >
							<label class="col-lg-4 font-weight-bolder">IP Address <span class="text-danger">*</span></label>
							<div class="col-lg-8">
								<input type="text" class="form-control form-control-sm" id="IpAddress" name="IpAddress" placeholder="Enter IP Address" value="" required/>
							</div>
						</div>
					</div>
					<div class="mb-1">
						<div class="form-group row mb-0">
							<label class="col-lg-4 font-weight-bolder">User <span class="text-danger">*</span></label>
							<div class="col-lg-8">
								<input type="text" class="form-control form-control-sm" id="Username" name="Username" placeholder="Enter User" value="" required/>
							</div>
						</div>
					</div>
					<div class="mb-1">
						<div class="form-group row mb-0">
							<label class="col-lg-4 font-weight-bolder">Password <span class="text-danger">*</span></label>
							<div class="col-lg-8">
								<input type="text" class="form-control form-control-sm" id="Password" name="Password" placeholder="Enter Password" value="" required/>
							</div>
						</div>
					</div>
					<div class="mb-1">
						<div class="form-group row mb-0">
							<label class="col-lg-4 font-weight-bolder">Port <span class="text-danger">*</span></label>
							<div class="col-lg-8">
								<input type="text" class="form-control form-control-sm"
									id="Port" name="Port" placeholder="Enter Port" value="" required/>
							</div>
						</div>
					</div>
					<div class="mb-1">
						<div class="form-group row mb-0">
							<label class="col-lg-4 col-form-label">Sysname</label>
							<div class="col-lg-8">
								<input type="text" class="form-control form-control-sm"
									id="Sysname" name="Sysname" placeholder="Enter Sysname"
									value="" />
							</div>
						</div>
					</div>
					<div class="mb-1">
						<div class="form-group row mb-0">
							<label class="col-lg-4 col-form-label">SNMP</label>
							<div class="col-lg-8">
								<input type="text" class="form-control form-control-sm"
									id="SnmpCommunity" name="SnmpCommunity"
									placeholder="Enter SNMP" value="" />
							</div>
						</div>
					</div>
					<div class="mb-1">
						<div class="form-group row mb-0">
							<label class="col-lg-4 col-form-label">Board F/S/P</label>
							<div class="col-lg-8 row">
								<div class="col-lg-4">
									<input type="text" class="form-control form-control-sm" id="FrameId" name="FrameId" placeholder="Enter Frame" value="" />
								</div>
								<div class="col-lg-4">
									<input type="text" class="form-control form-control-sm" id="SlotId" name="SlotId" placeholder="Enter Slot" value="" />
								</div>
								<div class="col-lg-4">
									<input type="text" class="form-control form-control-sm" id="PortId" name="PortId" placeholder="Enter Port" value="" />
								</div>
							</div>
						</div>
					</div>
					<div class="mb-2">
						<div class="form-group row mb-0">
							<label class="col-lg-4 col-form-label">BtS Code</label>
							<div class="col-lg-8">
								<select class="form-control form-control-sm" id="BtsCode" name="BtsCode" style="width: 100%" required>
									<option value="" selected>Chose</option>
									@isset ($bts_list)
										@foreach($bts_list as $bts)
											<option value="{{ $bts->Code }}">{{$bts->Name }}</option>
										@endforeach
									@endisset
								</select>
							</div>
						</div>
					</div>
					<div class="mb-2">
						<div class="form-group row mb-0">
							<label class="col-lg-4 font-weight-bolder">Brand <span class="text-danger">*</span></label>
							<div class="col-lg-8">
								<select class="form-control form-control-sm" id="DeviceCode" name="DeviceCode" style="width: 100%" required>
									<option value="" selected>Chose</option> @isset ($device_list)
									@foreach($device_list as $device)
									<option value="{{ $device->Code }}">{{ $device->Model }}-{{
										$device->Name }}-{{ $device->Version }}</option> @endforeach
									@endisset
								</select>
							</div>
						</div>
					</div>
					<div class="mb-1">
						<div class="form-group row mb-0">
							<label class="col-lg-4 col-form-label">Brand Model</label>
							<div class="col-lg-8">
								<input type="text" class="form-control form-control-sm"
									id="DeviceModel" name="DeviceModel"
									placeholder="Enter Brand Version" value="" readonly />
							</div>
						</div>
					</div>
					<div class="mb-1">
						<div class="form-group row mb-0">
							<label class="col-lg-4 col-form-label">Brand Type</label>
							<div class="col-lg-8">
								<input type="text" class="form-control form-control-sm"
									id="DeviceType" name="DeviceType"
									placeholder="Enter Brand Version" value="" readonly />
							</div>
						</div>
					</div>
					<div class="mb-1">
						<div class="form-group row mb-0">
							<label class="col-lg-4 col-form-label">Brand Version</label>
							<div class="col-lg-8">
								<input type="text" class="form-control form-control-sm"
									id="DeviceVersion" name="DeviceVersion"
									placeholder="Enter Brand Version" value="" readonly />
							</div>
						</div>
					</div>
					<div class="mb-1">
						<div class="form-group row mb-0">
							<label class="col-lg-4 col-form-label">Notes</label>
							<div class="col-lg-8">
								<textarea type="text" class="form-control form-control-sm" id="Remark" name="Remark" placeholder="Enter Notes" value=""></textarea>
							</div>
						</div>
					</div>
					<div class="mb-2">
						<div class="form-group row mb-0">
							<label class="col-lg-4 col-form-label">Status</label>
							<div class="col-lg-8">
								<input id="ActiveStatus" name="ActiveStatus" data-switch="true"
									type="checkbox" checked="checked" data-on-text="Enabled"
									data-handle-width="200" data-handle-font="1"
									data-off-text="Disabled" data-on-color="warning" />
							</div>
						</div>
					</div>
					<div class="mb-2">
						<div class="form-group row mb-0">
							<label class="col-lg-4 col-form-label"></label>
							<div class="col-lg-8">
								<button type="submit" class="btn btn-success font-weight-bold">Save</button>
								<button type="button" data-dismiss="modal"
									class="btn btn-danger">Cancel</button>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

@include('inc.error-list')
@include('inc.success-notif')
<div class="card card-custom">
	<div class="card-header bg-danger flex-wrap border-1 pt-1 pb-0 mb-2"
		style="min-height: 0;">
		<div class="card-title pt-1 pb-1">
			<h3 class="card-label font-weight-bolder text-white">
				Host
				<div class="text-muted pt-2 font-size-lg">show Datatable from table
					Host</div>
			</h3>
		</div>
		<div class="card-toolbar pt-1 pb-0">
			<a href="#" onclick="show_data('')"
				class="btn btn-primary font-weight-bolder"
				style="background-color: #1e1e2d; border-color: #0c8eff;"> <span
				class="svg-icon svg-icon-md"> <svg
						xmlns="http://www.w3.org/2000/svg"
						xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
						height="24px" viewBox="0 0 24 24" version="1.1">
            <g stroke="none" stroke-width="1" fill="none"
							fill-rule="evenodd">
              <rect x="0" y="0" width="24" height="24"></rect>
              <circle fill="#000000" cx="9" cy="15" r="6"></circle>
              <path
							d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z"
							fill="#000000" opacity="0.3"></path>
            </g>
          </svg>
			</span>Add New
			</a>
		</div>
	</div>
	<div class="card-body pt-1">
    <div hidden><input id="start-date"/><input id="end-date"/></div>
		<table class="table table-bordered table-hover w100" cellspacing="0" id="datatable" style="width: 1070px !important;"></table>
	</div>
	<div class="card-body py-1">
		<div class="card card-custom" id="data-olt">
			<div class="card-header bg-danger flex-wrap border-1 pt-1 pb-0 mb-2" style="min-height: 0;">
				<div class="card-title pt-1 pb-1">
					<h3 class="card-label font-weight-bolder text-white">
						Data <span><label class="Hostname">-</label></span>
					</h3>
				</div>
				<div class="dropdown dropdown-inline">
	          <button type="button" class="btn btn-tool btn-lg" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	              <i class="la la-download text-white"></i> Update Current Config
	          </button>
	          <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
	              <ul class="navi flex-column navi-hover py-2" id="btn_tools">
	                  <li class="navi-item">
	                      <a class="navi-link tool-action" id="check_all_config">
	                          <span class="navi-icon"><i class="la la-print"></i></span>
	                          <span class="navi-text">Check All Config</span>
	                      </a>
	                  </li>
	                  <li class="navi-item">
	                      <a class="navi-link tool-action" id="check_current_config">
	                          <span class="navi-icon"><i class="la la-print"></i></span>
	                          <span class="navi-text">Current Config</span>
	                      </a>
	                  </li>
	                  <li class="navi-item">
	                      <a class="navi-link tool-action" id="check_traffic-table">
	                          <span class="navi-icon"><i class="la la-print"></i></span>
	                          <span class="navi-text">Check Traffic Table</span>
	                      </a>
	                  </li>
	                  <li class="navi-item">
	                      <a class="navi-link tool-action" id="check_line-profile">
	                          <span class="navi-icon"><i class="la la-print"></i></span>
	                          <span class="navi-text">Check lineprofile</span>
	                      </a>
	                  </li>
	                  <li class="navi-item">
	                      <a class="navi-link tool-action" id="check_srv-profile">
	                          <span class="navi-icon"><i class="la la-print"></i></span>
	                          <span class="navi-text">Check Srv Profile</span>
	                      </a>
	                  </li>
	                  <li class="navi-item">
	                      <a class="navi-link tool-action" id="check_port-vlan">
	                          <span class="navi-icon"><i class="la la-print"></i></span>
	                          <span class="navi-text">Check Port Vlan</span>
	                      </a>
	                  </li>
	                  <li class="navi-item">
	                      <a class="navi-link tool-action" id="check_dba-profile">
	                          <span class="navi-icon"><i class="la la-print"></i></span>
	                          <span class="navi-text">Check Dba Profile</span>
	                      </a>
	                  </li>
	                  <li class="navi-item">
	                      <a class="navi-link tool-action" id="check_service_port">
	                          <span class="navi-icon"><i class="la la-print"></i></span>
	                          <span class="navi-text">Check Service Port</span>
	                      </a>
	                  </li>
	                  <li class="navi-item">
	                      <a class="navi-link tool-action" id="check_gem_port">
	                          <span class="navi-icon"><i class="la la-print"></i></span>
	                          <span class="navi-text">Check Gem Port</span>
	                      </a>
	                  </li>
	                  <li class="navi-item">
	                      <a class="navi-link tool-action" id="check_ont_info">
	                          <span class="navi-icon"><i class="la la-print"></i></span>
	                          <span class="navi-text">Check Register ONT</span>
	                      </a>
	                  </li>
	                  <li class="navi-item">
	                      <a class="navi-link tool-action" id="check_ont_idle">
	                          <span class="navi-icon"><i class="la la-print"></i></span>
	                          <span class="navi-text">Check ONT IDLE</span>
	                      </a>
	                  </li>
	              </ul>
	          </div>
	      </div>
			</div>
			<div class="card mb-8">
				<div class="row">
					<div class="col-lg-3">
						<ul class="navi navi-link-rounded navi-accent navi-hover navi-active nav flex-column mb-8 mb-lg-0" role="tablist">
							<li class="navi-item mt-2 mb-2">
								<a class="navi-link active" data-toggle="tab" href="#v-pills-home">
									<span class="navi-text text-dark-50 font-size-h5 font-weight-bold">Commend Config</span>
								</a>
							</li>
							<li class="navi-item mb-2">
								<a class="navi-link" data-toggle="tab" href="#v-pills-profile">
									<span class="navi-text text-dark-50 font-size-h5 font-weight-bold">Current Config</span>
								</a>
							</li>
							<li class="navi-item mb-2">
								<a class="navi-link" data-toggle="tab" href="#v-pills-profile-last">
									<span class="navi-text text-dark-50 font-size-h5 font-weight-bold">Last Config</span>
								</a>
							</li>
						</ul>
					</div>
					<div class="col-lg-9">
						<div class="tab-content">
						  <div class="tab-pane fade show active mt-2" id="v-pills-home" role="tab">
								<div class="card-body px-0">
									<div class="d-flex">
										<div class="flex-grow-1">
											<div class="d-flex align-items-center flex-wrap justify-content-between">
												<div class="flex-grow-1 font-weight-boldest text-dark py-2 py-lg-2 mr-5">
													<div class="row justify-content-center py-1 px-1 mr-2">
														<div class="col-md-12">
															<div class="d-flex justify-content-between pb-0 pb-md-0 flex-column flex-md-row">
																<h1 class="font-weight-boldest mb-0">IP: <label class="IpAddress">-</label></h1>
																<h1 class="font-weight-boldest mb-0"><label class="Hostname">-</label></h1>
															</div>
														</div>
													</div>
													<div class="overflow-auto col-12 pt-2 d-flex" style="max-width: 68rem">
														<pre class="container-fluid CurrentConfig d-flex">-</pre>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						  <div class="tab-pane fade mt-2" id="v-pills-profile" role="tab">
								<div class="card-body p-0">
									<!-- begin: Invoice-->
									<!-- begin: Invoice header-->
									<div class="row justify-content-center py-1 px-1 mr-2">
										<div class="col-md-12">
											<div class="d-flex justify-content-between pb-0 pb-md-0 flex-column flex-md-row">
												<h1 class="font-weight-boldest mb-0">IP: <label class="IpAddress">-</label></h1>
												<h1 class="font-weight-boldest mb-0"><label class="Hostname">-</label></h1>
											</div>
										</div>
									</div>
									<!-- end: Invoice header-->
									<!-- begin: Invoice body-->
									<div class="row justify-content-center py-8 px-8 py-md-10 px-md-0 mr-2" style="background-color: #fafad252;">
										<div class="col-md-12">
											<div class="border-bottom w-100"></div>
											<div class="table-responsive">
												<h3 class="pt-3"><div id="color-picker-1" class="mx-auto">Traffic Table</div></h3>
													<table id="datatable_traffic-table" class="table dataTable table-bordered table-hover w100" cellspacing="0">
															<thead>
																<tr>
																		<th class="text-center text-white dt-body-nowrap">No</th>
																		<th class="text-center text-white dt-body-nowrap">Index</th>
																		<th class="text-center text-white dt-body-nowrap">Name</th>
																		<th class="text-center text-white dt-body-nowrap">Cir</th>
																		<th class="text-center text-white dt-body-nowrap">Cbs</th>
																		<th class="text-center text-white dt-body-nowrap">Pir</th>
																		<th class="text-center text-white dt-body-nowrap">Pbs</th>
																		<th class="text-center text-white dt-body-nowrap">Priority</th>
																		<th class="text-center text-white dt-body-nowrap">Sync</th>
																</tr>
															</thead>
															<tbody>
															</tbody>
													</table>
											</div>
										</div>
										<div class="col-md-12">
											<div class="border-bottom w-100"></div>
											<div class="table-responsive">
												<h3 class="pt-3"><div id="color-picker-1" class="mx-auto">Service Profile</div></h3>
													<table id="datatable_srv-profile" class="table dataTable table-bordered table-hover w100" cellspacing="0">
															<thead>
																<tr>
																		<th class="text-center text-white dt-body-nowrap">No</th>
																		<th class="text-center text-white dt-body-nowrap">ProfileId</th>
																		<th class="text-center text-white dt-body-nowrap">ProfileName</th>
																		<th class="text-center text-white dt-body-nowrap">ProfileBinding</th>
																		<th class="text-center text-white dt-body-nowrap">Sync</th>
																</tr>
															</thead>
															<tbody>
															</tbody>
													</table>
											</div>
										</div>
										<div class="col-md-12">
											<div class="border-bottom w-100"></div>
											<div class="table-responsive">
												<h3 class="pt-3"><div id="color-picker-1" class="mx-auto">Line Profile</div></h3>
													<table id="datatable_line-profile" class="table dataTable table-bordered table-hover w100" cellspacing="0">
															<thead>
																<tr>
																		<th class="text-center text-white dt-body-nowrap">No</th>
																		<th class="text-center text-white dt-body-nowrap">ProfileId</th>
																		<th class="text-center text-white dt-body-nowrap">ProfileName</th>
																		<th class="text-center text-white dt-body-nowrap">ProfileBinding</th>
																		<th class="text-center text-white dt-body-nowrap">Sync</th>
																</tr>
															</thead>
															<tbody>
															</tbody>
													</table>
											</div>
										</div>
										<div class="col-md-12">
											<div class="border-bottom w-100"></div>
											<div class="table-responsive">
												<h3 class="pt-3"><div id="color-picker-1" class="mx-auto">Port Vlan</div></h3>
													<table id="datatable_port-vlan" class="table dataTable table-bordered table-hover w100" cellspacing="0">
															<thead>
																<tr>
																		<th class="text-center text-white dt-body-nowrap">No</th>
																		<th class="text-center text-white dt-body-nowrap">Index</th>
																		<th class="text-center text-white dt-body-nowrap">Type</th>
																		<th class="text-center text-white dt-body-nowrap">Desc</th>
																		<th class="text-center text-white dt-body-nowrap">Attrib</th>
																		<th class="text-center text-white dt-body-nowrap">Frame</th>
																		<th class="text-center text-white dt-body-nowrap">Slot</th>
																		<th class="text-center text-white dt-body-nowrap">Port</th>
																		<th class="text-center text-white dt-body-nowrap">Sync</th>
																</tr>
															</thead>
															<tbody>
															</tbody>
													</table>
											</div>
										</div>
										<div class="col-md-12">
											<div class="border-bottom w-100"></div>
											<div class="table-responsive">
												<h3 class="pt-3"><div id="color-picker-1" class="mx-auto">Dba Profile</div></h3>
													<table id="datatable_dba-profile" class="table dataTable table-bordered table-hover w100" cellspacing="0">
															<thead>
																<tr>
																		<th class="text-center text-white dt-body-nowrap">No</th>
																		<th class="text-center text-white dt-body-nowrap">ProfileId</th>
																		<th class="text-center text-white dt-body-nowrap">Type</th>
																		<th class="text-center text-white dt-body-nowrap">Bandwidth</th>
																		<th class="text-center text-white dt-body-nowrap">Fix</th>
																		<th class="text-center text-white dt-body-nowrap">Assure</th>
																		<th class="text-center text-white dt-body-nowrap">Max</th>
																		<th class="text-center text-white dt-body-nowrap">Bind</th>
																		<th class="text-center text-white dt-body-nowrap">Sync</th>
																</tr>
															</thead>
															<tbody>
															</tbody>
													</table>
											</div>
										</div>
										<div class="col-md-12">
											<div class="border-bottom w-100"></div>
											<div class="table-responsive">
												<h3 class="pt-3"><div id="color-picker-1" class="mx-auto">Gem Profile</div></h3>
													<table id="datatable_gem-port" class="table dataTable table-bordered table-hover w100" cellspacing="0">
															<thead>
																<tr>
																		<th class="text-center text-white dt-body-nowrap">No</th>
																		<th class="text-center text-white dt-body-nowrap">ProfileId</th>
																		<th class="text-center text-white dt-body-nowrap">GemId</th>
																		<th class="text-center text-white dt-body-nowrap">MappingIndex</th>
																		<th class="text-center text-white dt-body-nowrap">GemVlan</th>
																		<th class="text-center text-white dt-body-nowrap">Priority</th>
																		<th class="text-center text-white dt-body-nowrap">Type</th>
																		<th class="text-center text-white dt-body-nowrap">Port</th>
																		<th class="text-center text-white dt-body-nowrap">Bundle</th>
																		<th class="text-center text-white dt-body-nowrap">Flow</th>
																		<th class="text-center text-white dt-body-nowrap">Sync</th>
																</tr>
															</thead>
															<tbody>
															</tbody>
													</table>
											</div>
										</div>
										<div class="col-md-12">
											<div class="border-bottom w-100"></div>
											<div class="table-responsive">
												<h3 class="pt-3"><div id="color-picker-1" class="mx-auto">Service Port</div></h3>
													<table id="datatable_service-port" class="table dataTable table-bordered table-hover w100" cellspacing="0">
															<thead>
																<tr>
																		<th class="text-center text-white dt-body-nowrap">No</th>
																		<th class="text-center text-white dt-body-nowrap">Index</th>
																		<th class="text-center text-white dt-body-nowrap">VlanId</th>
																		<th class="text-center text-white dt-body-nowrap">VlanAttr</th>
																		<th class="text-center text-white dt-body-nowrap">Type</th>
																		<th class="text-center text-white dt-body-nowrap">FrameId</th>
																		<th class="text-center text-white dt-body-nowrap">SlotId</th>
																		<th class="text-center text-white dt-body-nowrap">PortId</th>
																		<th class="text-center text-white dt-body-nowrap">VPI</th>
																		<th class="text-center text-white dt-body-nowrap">VCI</th>
																		<th class="text-center text-white dt-body-nowrap">FlowType</th>
																		<th class="text-center text-white dt-body-nowrap">FlowPara</th>
																		<th class="text-center text-white dt-body-nowrap">Rx</th>
																		<th class="text-center text-white dt-body-nowrap">Tx</th>
																		<th class="text-center text-white dt-body-nowrap">State</th>
																		<th class="text-center text-white dt-body-nowrap">Transparent</th>
																		<th class="text-center text-white dt-body-nowrap">Sync</th>
																</tr>
															</thead>
															<tbody>
															</tbody>
													</table>
											</div>
										</div>
										<div class="col-md-12">
											<div class="border-bottom w-100"></div>
											<div class="table-responsive">
												<h3 class="pt-3"><div id="color-picker-1" class="mx-auto">Register ONT</div></h3>
													<table id="datatable_ont-info" class="table dataTable table-bordered table-hover w100" cellspacing="0">
															<thead>
																<tr>
																		<th class="text-center text-white dt-body-nowrap">No</th>
																		<th class="text-center text-white dt-body-nowrap">FrameId</th>
																		<th class="text-center text-white dt-body-nowrap">SlotID</th>
																		<th class="text-center text-white dt-body-nowrap">PortId</th>
																		<th class="text-center text-white dt-body-nowrap">OntId</th>
																		<th class="text-center text-white dt-body-nowrap">OntSn</th>
																		<th class="text-center text-white dt-body-nowrap">Description</th>
																		<th class="text-center text-white dt-body-nowrap">Flag</th>
																		<th class="text-center text-white dt-body-nowrap">Run</th>
																		<th class="text-center text-white dt-body-nowrap">Config</th>
																		<th class="text-center text-white dt-body-nowrap">Match</th>
																		<th class="text-center text-white dt-body-nowrap">Protect</th>
																		<th class="text-center text-white dt-body-nowrap">Sync</th>
																</tr>
															</thead>
															<tbody>
															</tbody>
													</table>
											</div>
										</div>
										<div class="col-md-12">
											<div class="border-bottom w-100"></div>
											<div class="table-responsive">
												<h3 class="pt-3"><div id="color-picker-1" class="mx-auto">Ont Idle</div></h3>
													<table id="datatable_ont-autofind" class="table dataTable table-bordered table-hover w100" cellspacing="0">
															<thead>
																<tr>
																		<th class="text-center text-white dt-body-nowrap">No</th>
																		<th class="text-center text-white dt-body-nowrap">Sn</th>
																		<th class="text-center text-white dt-body-nowrap">Version</th>
																		<th class="text-center text-white dt-body-nowrap">SoftwareVersion</th>
																		<th class="text-center text-white dt-body-nowrap">EquipmentId</th>
																		<th class="text-center text-white dt-body-nowrap">FrameId</th>
																		<th class="text-center text-white dt-body-nowrap">SlotId</th>
																		<th class="text-center text-white dt-body-nowrap">PortId</th>
																		<th class="text-center text-white dt-body-nowrap">RxPower</th>
																		<th class="text-center text-white dt-body-nowrap">TxPower</th>
																		<th class="text-center text-white dt-body-nowrap">Remark</th>
																		<th class="text-center text-white dt-body-nowrap">Sync</th>
																</tr>
															</thead>
															<tbody>
															</tbody>
													</table>
											</div>
										</div>
									</div>
								</div>

							</div>
							<div class="tab-pane fade mt-2" id="v-pills-profile-last" role="tab">
								<div class="card-body p-0">
									<!-- begin: Invoice-->
									<!-- begin: Invoice header-->
									<div class="row justify-content-center py-1 px-1 mr-2">
										<div class="col-md-12">
											<div class="d-flex justify-content-between pb-0 pb-md-0 flex-column flex-md-row">
												<h1 class="font-weight-boldest mb-0">IP: <label class="IpAddress">-</label></h1>
												<h1 class="font-weight-boldest mb-0"><label class="Hostname">-</label></h1>
											</div>
										</div>
									</div>
									<!-- end: Invoice header-->
									<!-- begin: Invoice body-->
									<div class="row justify-content-center py-8 px-8 py-md-10 px-md-0 mr-2" style="background-color: #fafad252;">
										<div class="col-md-12">
											<div class="border-bottom w-100"></div>
											<div class="table-responsive">
												<h3 class="pt-3"><div id="color-picker-1" class="mx-auto">Traffic Table Last</div></h3>
													<table id="datatable_traffic-table-last" class="table dataTable table-bordered table-hover w100" cellspacing="0">
															<thead>
																<tr>
																		<th class="text-center text-white dt-body-nowrap">Index</th>
																		<th class="text-center text-white dt-body-nowrap">Name</th>
																		<th class="text-center text-white dt-body-nowrap">Cir</th>
																		<th class="text-center text-white dt-body-nowrap">Cbs</th>
																		<th class="text-center text-white dt-body-nowrap">Pir</th>
																		<th class="text-center text-white dt-body-nowrap">Pbs</th>
																		<th class="text-center text-white dt-body-nowrap">Priority</th>
																		<th class="text-center text-white dt-body-nowrap">Created Date</th>
																</tr>
															</thead>
															<tbody>
															</tbody>
													</table>
											</div>
										</div>
										<div class="col-md-12">
											<div class="border-bottom w-100"></div>
											<div class="table-responsive">
												<h3 class="pt-3"><div id="color-picker-1" class="mx-auto">Service Profile Last</div></h3>
													<table id="datatable_srv-profile-last" class="table dataTable table-bordered table-hover w100" cellspacing="0">
															<thead>
																<tr>
																		<th class="text-center text-white dt-body-nowrap">ProfileId</th>
																		<th class="text-center text-white dt-body-nowrap">ProfileName</th>
																		<th class="text-center text-white dt-body-nowrap">ProfileBinding</th>
																		<th class="text-center text-white dt-body-nowrap">Created Date</th>
																</tr>
															</thead>
															<tbody>
															</tbody>
													</table>
											</div>
										</div>
										<div class="col-md-12">
											<div class="border-bottom w-100"></div>
											<div class="table-responsive">
												<h3 class="pt-3"><div id="color-picker-1" class="mx-auto">Line Profile Last</div></h3>
													<table id="datatable_line-profile-last" class="table dataTable table-bordered table-hover w100" cellspacing="0">
															<thead>
																<tr>
																		<th class="text-center text-white dt-body-nowrap">ProfileId</th>
																		<th class="text-center text-white dt-body-nowrap">ProfileName</th>
																		<th class="text-center text-white dt-body-nowrap">ProfileBinding</th>
																		<th class="text-center text-white dt-body-nowrap">Created Date</th>
																</tr>
															</thead>
															<tbody>
															</tbody>
													</table>
											</div>
										</div>
										<div class="col-md-12">
											<div class="border-bottom w-100"></div>
											<div class="table-responsive">
												<h3 class="pt-3"><div id="color-picker-1" class="mx-auto">Port Vlan Last</div></h3>
													<table id="datatable_port-vlan-last" class="table dataTable table-bordered table-hover w100" cellspacing="0">
															<thead>
																<tr>
																		<th class="text-center text-white dt-body-nowrap">Index</th>
																		<th class="text-center text-white dt-body-nowrap">Type</th>
																		<th class="text-center text-white dt-body-nowrap">Desc</th>
																		<th class="text-center text-white dt-body-nowrap">Attrib</th>
																		<th class="text-center text-white dt-body-nowrap">Frame</th>
																		<th class="text-center text-white dt-body-nowrap">Slot</th>
																		<th class="text-center text-white dt-body-nowrap">Port</th>
																		<th class="text-center text-white dt-body-nowrap">CreatedDate</th>
																</tr>
															</thead>
															<tbody>
															</tbody>
													</table>
											</div>
										</div>
										<div class="col-md-12">
											<div class="border-bottom w-100"></div>
											<div class="table-responsive">
												<h3 class="pt-3"><div id="color-picker-1" class="mx-auto">Dba Profile Last</div></h3>
													<table id="datatable_dba-profile-last" class="table dataTable table-bordered table-hover w100" cellspacing="0">
															<thead>
																<tr>
																		<th class="text-center text-white dt-body-nowrap">ProfileId</th>
																		<th class="text-center text-white dt-body-nowrap">Type</th>
																		<th class="text-center text-white dt-body-nowrap">Bandwidth</th>
																		<th class="text-center text-white dt-body-nowrap">Fix</th>
																		<th class="text-center text-white dt-body-nowrap">Assure</th>
																		<th class="text-center text-white dt-body-nowrap">Max</th>
																		<th class="text-center text-white dt-body-nowrap">Bind</th>
																		<th class="text-center text-white dt-body-nowrap">CreatedDate</th>
																</tr>
															</thead>
															<tbody>
															</tbody>
													</table>
											</div>
										</div>
										<div class="col-md-12">
											<div class="border-bottom w-100"></div>
											<div class="table-responsive">
												<h3 class="pt-3"><div id="color-picker-1" class="mx-auto">Gem Profile Last</div></h3>
													<table id="datatable_gem-port-last" class="table dataTable table-bordered table-hover w100" cellspacing="0">
															<thead>
																<tr>
																		<th class="text-center text-white dt-body-nowrap">ProfileId</th>
																		<th class="text-center text-white dt-body-nowrap">GemId</th>
																		<th class="text-center text-white dt-body-nowrap">MappingIndex</th>
																		<th class="text-center text-white dt-body-nowrap">GemVlan</th>
																		<th class="text-center text-white dt-body-nowrap">Priority</th>
																		<th class="text-center text-white dt-body-nowrap">Type</th>
																		<th class="text-center text-white dt-body-nowrap">Port</th>
																		<th class="text-center text-white dt-body-nowrap">Bundle</th>
																		<th class="text-center text-white dt-body-nowrap">Flow</th>
																		<th class="text-center text-white dt-body-nowrap">CreatedDate</th>
																</tr>
															</thead>
															<tbody>
															</tbody>
													</table>
											</div>
										</div>
										<div class="col-md-12">
											<div class="border-bottom w-100"></div>
											<div class="table-responsive">
												<h3 class="pt-3"><div id="color-picker-1" class="mx-auto">Service Port Last</div></h3>
													<table id="datatable_service-port-last" class="table dataTable table-bordered table-hover w100" cellspacing="0">
															<thead>
																<tr>
																		<th class="text-center text-white dt-body-nowrap">Index</th>
																		<th class="text-center text-white dt-body-nowrap">VlanId</th>
																		<th class="text-center text-white dt-body-nowrap">VlanAttr</th>
																		<th class="text-center text-white dt-body-nowrap">Type</th>
																		<th class="text-center text-white dt-body-nowrap">FrameId</th>
																		<th class="text-center text-white dt-body-nowrap">SlotId</th>
																		<th class="text-center text-white dt-body-nowrap">PortId</th>
																		<th class="text-center text-white dt-body-nowrap">VPI</th>
																		<th class="text-center text-white dt-body-nowrap">VCI</th>
																		<th class="text-center text-white dt-body-nowrap">FlowType</th>
																		<th class="text-center text-white dt-body-nowrap">FlowPara</th>
																		<th class="text-center text-white dt-body-nowrap">Rx</th>
																		<th class="text-center text-white dt-body-nowrap">Tx</th>
																		<th class="text-center text-white dt-body-nowrap">State</th>
																		<th class="text-center text-white dt-body-nowrap">Transparent</th>
																		<th class="text-center text-white dt-body-nowrap">CreatedDate</th>
																</tr>
															</thead>
															<tbody>
															</tbody>
													</table>
											</div>
										</div>
										<div class="col-md-12">
											<div class="border-bottom w-100"></div>
											<div class="table-responsive">
												<h3 class="pt-3"><div id="color-picker-1" class="mx-auto">Register ONT Last</div></h3>
													<table id="datatable_ont-info-last" class="table dataTable table-bordered table-hover w100" cellspacing="0">
															<thead>
																<tr>
																		<th class="text-center text-white dt-body-nowrap">FrameId</th>
																		<th class="text-center text-white dt-body-nowrap">SlotID</th>
																		<th class="text-center text-white dt-body-nowrap">PortId</th>
																		<th class="text-center text-white dt-body-nowrap">OntId</th>
																		<th class="text-center text-white dt-body-nowrap">OntSn</th>
																		<th class="text-center text-white dt-body-nowrap">Description</th>
																		<th class="text-center text-white dt-body-nowrap">Flag</th>
																		<th class="text-center text-white dt-body-nowrap">Run</th>
																		<th class="text-center text-white dt-body-nowrap">Config</th>
																		<th class="text-center text-white dt-body-nowrap">Match</th>
																		<th class="text-center text-white dt-body-nowrap">Protect</th>
																		<th class="text-center text-white dt-body-nowrap">CreatedDate</th>
																</tr>
															</thead>
															<tbody>
															</tbody>
													</table>
											</div>
										</div>
										<div class="col-md-12">
											<div class="border-bottom w-100"></div>
											<div class="table-responsive">
												<h3 class="pt-3"><div id="color-picker-1" class="mx-auto">Ont Idle Last</div></h3>
													<table id="datatable_ont-autofind-last" class="table dataTable table-bordered table-hover w100" cellspacing="0">
															<thead>
																<tr>
																		<th class="text-center text-white dt-body-nowrap">Sn</th>
																		<th class="text-center text-white dt-body-nowrap">Version</th>
																		<th class="text-center text-white dt-body-nowrap">SoftwareVersion</th>
																		<th class="text-center text-white dt-body-nowrap">EquipmentId</th>
																		<th class="text-center text-white dt-body-nowrap">FrameId</th>
																		<th class="text-center text-white dt-body-nowrap">SlotId</th>
																		<th class="text-center text-white dt-body-nowrap">PortId</th>
																		<th class="text-center text-white dt-body-nowrap">RxPower</th>
																		<th class="text-center text-white dt-body-nowrap">TxPower</th>
																		<th class="text-center text-white dt-body-nowrap">Remark</th>
																		<th class="text-center text-white dt-body-nowrap">CreatedDate</th>
																</tr>
															</thead>
															<tbody>
															</tbody>
													</table>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection {{-- Styles Section --}}
@section('styles')
<link rel="stylesheet"
	href="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.css">
<link rel="stylesheet"
	href="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.min.css">
<link rel="stylesheet"
	href="{{ config('app.url') }}plugins/custom/datatables/datatables.bundle.css">
	<style>
	pre {
			white-space: pre-wrap;
			white-space: -moz-pre-wrap;
			white-space: -pre-wrap;
			white-space: -o-pre-wrap;
			word-wrap: break-word;
			background-color: #00000082;
			color: #0bff14;
			font-weight: 900;
			font-size: 1rem;
	}

	.table thead th, .table thead td {
	    font-weight: 600;
	    font-size: 1rem;
	    border-bottom-width: 1px;
	    padding-top: 0.2rem;
	    padding-bottom: 0.2rem;
	}
	.table tbody th, .table tbody td {
	    font-weight: 600;
	    font-size: 0.8rem;
	    border-bottom-width: 1px;
	    padding-top: 0.2rem;
	    padding-bottom: 0.2rem;
	    white-space: nowrap;
	}
	</style>
@endsection {{-- Scripts Section --}}
@section('scripts')
@include('inc.confirm-delete-modal')
<script
	src="{{ config('app.url') }}global/vendor/datatables/jquery.dataTables.js"></script>
<script
	src="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.js"></script>
<script
	src="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.js"></script>
<script type="text/javascript">

$(document).ready(function() {
  $('[data-switch=true]').bootstrapSwitch();
  $("#DeviceCode").select2();
  $("#BtsCode").select2();

	$("div.datesearchbox").html('<div class="input-group"> <div class="input-group-addon"> <i class="glyphicon glyphicon-calendar"></i> </div><input type="text" class="form-control pull-right" id="datesearch" placeholder="Search by date range"> </div>');
	document.getElementsByClassName("datesearchbox")[0].style.textAlign = "center";
	$("#datesearch").attr("readonly",true);
	$('#datesearch').daterangepicker({
		 autoUpdateInput: false
	 });

	//menangani proses saat apply date range
	 $('#datesearch').on('apply.daterangepicker', function(ev, picker) {
			$(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
			$("#start-date").val(picker.startDate.format('YYYY-MM-DD'));
			$("#end-date").val(picker.endDate.format('YYYY-MM-DD'));
			refresh_table();
	 });

	 $('#datesearch').on('cancel.daterangepicker', function(ev, picker) {
		 $(this).val('');
		 $("#start-date").val('');
		 $("#end-date").val('');
		 refresh_table();
	 });
 });

  $('#DeviceCode').on('change', function() {
    if (this.value !="") {
      $.ajax({
          url: "{{ route('olt.brand.store')}}/" + this.value,
          type: "GET",
          headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
          },
          success: function (response) {
            $('#DeviceModel').val(response.data.Model);
            $('#DeviceType').val(response.data.Type);
            $('#DeviceVersion').val(response.data.Version);
          },
          error: function (xhr, status, error) {
            // showDialog.show(xhr.status + " " + status + " " + error, false);
          }
      });
    }else{
        $('#DeviceVersion').val("");
    }
  });
  var table = $('#datatable').dataTable({
    pageLength: 5,
    scrollX: true,
		scrollCollapse: true,
		fixedColumns:   {
        leftColumns: 2,
        rightColumns: -1
    },
    searchDelay: 800,
		// pagingTypeSince : 'numbers',
    // pagingType      : 'full_numbers',
		keys: true,
		// render: true,
		// stateSave: true,
		serverSide: true,
    processing: true,
    searching: true,
		// select: true,
		// rowId: 'id',
    lengthMenu: [[5, 10, 25, 50, 100, 200], [5, 10, 25, 50, 100, 200]],
    ajax: {
      method: 'POST',
      url : '{{ route('olt.host.data') }}',
      headers: {
        'X-CSRF-TOKEN': '{{ csrf_token() }}'
      },
      data: function (d) {
        d.from_date = $("#start-date").val();
        d.to_date = $(" #end-date").val();
    	}
    },
    columns: [
      {title: "No", data: 'DT_RowIndex', defaultContent: '-', class: 'text-center dt-body-nowrap', orderable: false, searchable: false},
      {title: "Code", data: 'Code', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "Hostname", data: 'Hostname', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "IP Address", data: 'IpAddress', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      // {title: "Status", data: 'active', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      // {title: "CheckStatus", data: 'CheckStatus', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      // {title: "CheckDate", data: 'CheckDate', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "Username", data: 'Username', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "Password", data: 'Password', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "Port", data: 'Port', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "Vlan Frame", data: 'FrameId', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "Vlan Slot", data: 'SlotId', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "Vlan Port", data: 'PortId', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "Sysname", data: 'Sysname', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "SNMP", data: 'SnmpCommunity', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "Bts Name", data: 'BtsCode', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "Brand Model", data: 'DeviceModel', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "Brand", data: 'DeviceName', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "Note", data: 'Remark', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "Actions", data: 'action', orderable: false},
    ],
    dom:  "<'row'<'col-sm-5'l><'col-sm-3' <'datesearchbox'>><'col-sm-3'f>>" +
          "<'row'<'col-sm-12'tr>>" +
          "<'row'<'col-sm-5'i><'col-sm-7'p>>",
    order: [[1, 'asc']],
		createdRow: function( row, data, dataIndex){
        if(!data.ActiveStatus){
            $(row).addClass('bg-warning');
        }
    },
    columnDefs: [
      {
        render: function ( data, type, row ) {
						var url_destroy = "{{route('olt.host.destroy', 'prm_code')}}";
						var url_update = "{{route('olt.host.edit', 'prm_code')}}";
						var code = "'"+row.Code+"'";
						var html = ""
        				// html+= '<a onclick="checkhost('+code+')" class="btn btn-icon btn-light btn-hover-info btn-sm" data-toggle="tooltip" data-placement="top" title="CheckHost"><span class="svg-icon svg-icon-md svg-icon-info"><i class="far flaticon2-rocket icon-lg"></i></span></a>';
        				html+= '<a onclick="show_data('+code+')" class="btn btn-icon btn-light btn-hover-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Update"><span class="svg-icon svg-icon-md svg-icon-primary"><i class="far flaticon-edit icon-lg"></i></span></a>';
        				html+= '<a data-href="'+url_destroy.replaceAll('prm_code',row.Code)+'" class="btn btn-icon btn-light btn-hover-danger btn-sm" "data-toggle="tooltip" data-placement="top" title="Delete" data-toggle="modal" data-target="#confirm-delete-modal" ><span class="svg-icon svg-icon-md svg-icon-danger"><i class="far flaticon-delete icon-lg"></i></span></a>';

						return html;
        },
        targets: [-1],
        className: 'text-center dt-body-nowrap'
      },
    ],


    initComplete: function() {
      $('.tl-tip').tooltip();
    }
  });

	  $('#datatable tbody').on( 'click', 'tr', function () {
			// alert($(this).val());
			// alert($(this).find("td:eq(-1)").text().toString());
			// if ($(this).find("td:eq(-1)").text().toString() == ""){
			// 	alert("here");
			// 	return;
			// }
	        if ($(this).hasClass('selected')) {
	            $(this).removeClass('selected');
							$(".CurrentConfig").html("Data not selected");
							$('.Hostname').html("Data not selected");
							$('.IpAddress').html("Data not selected");
							$('.CheckStatus').html("Data not selected");
							$('.CheckDate').html("Data not selected");
							$("#datatable_traffic-table tbody>tr").remove();
							$("#datatable_srv-profile tbody>tr").remove();
							$("#datatable_line-profile tbody>tr").remove();
							$("#datatable_port-vlan tbody>tr").remove();
							$("#datatable_dba-profile tbody>tr").remove();
							$("#datatable_traffic-table-last tbody>tr").remove();
							$("#datatable_srv-profile-last tbody>tr").remove();
							$("#datatable_line-profile-last tbody>tr").remove();
							$("#datatable_port-vlan-last tbody>tr").remove();
							$("#datatable_dba-profile-last tbody>tr").remove();
							$("#datatable_gem-port tbody>tr").remove();
							$("#datatable_gem-port-last tbody>tr").remove();
							$("#datatable_service-port tbody>tr").remove();
							$("#datatable_service-port-last tbody>tr").remove();
							$("#datatable_ont-info tbody>tr").remove();
							$("#datatable_ont-info-last tbody>tr").remove();
							$("#datatable_ont-autofind tbody>tr").remove();
							$("#datatable_ont-autofind-last tbody>tr").remove();
							document.getElementById('check_all_config').setAttribute( "onClick", "#no_select_table" );
							document.getElementById('check_current_config').setAttribute( "onClick", "#no_select_table" );
							document.getElementById('check_traffic-table').setAttribute( "onClick", "#no_select_table" );
							document.getElementById('check_line-profile').setAttribute( "onClick", "#no_select_table" );
							document.getElementById('check_srv-profile').setAttribute( "onClick", "#no_select_table" );
							document.getElementById('check_port-vlan').setAttribute( "onClick", "#no_select_table" );
							document.getElementById('check_dba-profile').setAttribute( "onClick", "#no_select_table" );
							document.getElementById('check_service_port').setAttribute( "onClick", "#no_select_table" );
							document.getElementById('check_gem_port').setAttribute( "onClick", "#no_select_table" );
							document.getElementById('check_ont_info').setAttribute( "onClick", "#no_select_table" );
							document.getElementById('check_ont_idle').setAttribute( "onClick", "#no_select_table" );
	        }
	        else {
	            table.$('tr.selected').removeClass('selected');
	            $(this).addClass('selected');
			      	var id = $(this).find("td:eq(1)").html();
							document.getElementById('check_all_config').setAttribute( "onClick", "check_current_config('"+id+"','all')" );
							document.getElementById('check_current_config').setAttribute( "onClick", "check_current_config('"+id+"','current-config')" );
							document.getElementById('check_traffic-table').setAttribute( "onClick", "check_current_config('"+id+"','traffic-table')" );
							document.getElementById('check_line-profile').setAttribute( "onClick", "check_current_config('"+id+"','line-profile')" );
							document.getElementById('check_srv-profile').setAttribute( "onClick", "check_current_config('"+id+"','srv-profile')" );
							document.getElementById('check_port-vlan').setAttribute( "onClick", "check_current_config('"+id+"','port-vlan')" );
							document.getElementById('check_dba-profile').setAttribute( "onClick", "check_current_config('"+id+"','dba-profile')" );
							document.getElementById('check_service_port').setAttribute( "onClick", "check_current_config('"+id+"','service-port')" );
							document.getElementById('check_gem_port').setAttribute( "onClick", "check_current_config('"+id+"','gem-port')" );
							document.getElementById('check_ont_info').setAttribute( "onClick", "check_current_config('"+id+"','ont-info')" );
							document.getElementById('check_ont_idle').setAttribute( "onClick", "check_current_config('"+id+"','ont-autofind')" );
					 	 	$("#data-olt").loading("start");
		          $.ajax({
		              url: "{{ route('olt.host.show-config','')}}/" + id,
		              type: "GET",
		              headers: {
		                'X-CSRF-TOKEN': '{{ csrf_token() }}'
		              },
		              success: function (response) {
											$("#data-olt").loading("stop");
											show_detail(response);
		              },
		              error: function (xhr, status, error) {
										$("#data-olt").loading("stop");
		                // showDialog.show(xhr.status + " " + status + " " + error, false);
		              }
		          });
	        }
	    });


  function show_data(id = "") {
      if (id !== "") {
          $.ajax({
              url: "{{ route('olt.host.store')}}/" + id,
              type: "GET",
              headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
              },
              success: function (response) {
                  $("#form-input").attr("action", "{{ route('olt.host.update','')}}/"+id);
                  $('#form-input').trigger("reset");
                  $('#method').val("PUT");
                  $('#Code').attr("readonly", true);
                  $('#Code').val(response.data.Code);
                  $('#Hostname').val(response.data.Hostname);
                  $('#IpAddress').val(response.data.IpAddress);
                  $('#Username').val(response.data.Username);
                  $('#Password').val(response.data.Password);
                  $('#Remark').text(response.data.Remark);
                  $('#Port').val(response.data.Port);
                  $('#FrameId').val(response.data.FrameId);
                  $('#SlotId').val(response.data.SlotId);
                  $('#PortId').val(response.data.PortId);
                  $('#Sysname').val(response.data.Sysname);
                  $('#SnmpCommunity').val(response.data.SnmpCommunity);
                  $('#BtsCode').val(response.data.BtsCode).trigger('change');
                  $('#DeviceCode').val(response.data.DeviceCode).trigger('change');
                  $('#Port').val(response.data.Port);
                  if (response.data.ActiveStatus === 1) {
                      $('#ActiveStatus').bootstrapSwitch('state', true);
                  } else {
                      $('#ActiveStatus').bootstrapSwitch('state', false);
                  }
                  $('#modal-form').modal('show');
                  $('#Code').focus();
              },
              error: function (xhr, status, error) {
                // showDialog.show(xhr.status + " " + status + " " + error, false);
              }
          });
      } else {
          $("#form-input").attr("action", "{{ route('olt.host.store')}}");
          $('#method').val("POST");
          $('#Code').attr("readonly", false);
          $('#Code').focus();
          $('#form-input').trigger("reset");
          $('#modal-form').modal('show');
          $('#Code').focus();
      }
  }

  function refresh_table() {
      $('#datatable').dataTable().ajax.reload();
  }

  function format_date(val) {
      var date_arr = val.split(" ");
      var time = date_arr[1];
      var date_id_arr = date_arr[0].split("-");
      var date_fix = date_id_arr[2] + "-" + date_id_arr[1] + "-" + date_id_arr[0];
      var date_transaction = date_fix + " " + time;
      return date_transaction;
  }

	function numberWithCommas(x) {
	    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}
  function check_current_config(id,config){
			$("#data-olt").loading("start");
      $.ajax({
          url: "{{ route('telnet.olt-config.check-config',['',''])}}/"+id+"/"+config,
          type: "GET",
          success: function (response) {

							showDialog.show("<h1>"+response.message+"</h1>");
							$("#data-olt").loading("stop");
							show_detail(response);
          },
          error: function (xhr, status, error) {
						$("#data-olt").loading("stop");
            // showDialog.show(xhr.status + " " + status + " " + error, false);
          }
      });
  }

	function show_detail(response){
			if(response.data["host"]){
				$('.Hostname').html(response.data["host"].Hostname);
				$('.IpAddress').html(response.data["host"].IpAddress);
				$('.CheckStatus').html(response.data["host"].CheckStatus);
				$('.CheckDate').html(response.data["host"].CheckDate);
				$(".CurrentConfig").html("Please check config again!!");
				if(response.data["host"].CurrentConfig){
					var data = response.data["host"].CurrentConfig.replaceAll(" ","&nbsp;");
					$('.CurrentConfig').html(data);
				}
			}
			if(response.data["current-config"]){
				var data = "";
				for (var i in response.data["current-config"]){
					data += response.data["current-config"][i].replaceAll("<","[").replaceAll(">","]")+"<br>";
				}
				$('.CurrentConfig').html(data);
			}
			if(response.data["traffic-table"]){
					for (var i in response.data["traffic-table"]){
							var table = document.getElementById("datatable_traffic-table").getElementsByTagName('tbody')[0];
							var row = table.insertRow(-1);
							row.insertCell(0).innerHTML = Number(i)+1;
							row.insertCell(1).innerHTML = '<label class="col-12 text-center">'+response.data["traffic-table"][i].Index+'</label>';
							row.insertCell(2).innerHTML = '<label class="col-12 text-center">'+response.data["traffic-table"][i].Name+'</label>';
							row.insertCell(3).innerHTML = '<label class="col-12 text-center">'+numberWithCommas(Number(response.data["traffic-table"][i].Cir))+'</label>';
							row.insertCell(4).innerHTML = '<label class="col-12 text-center">'+numberWithCommas(Number(response.data["traffic-table"][i].Cbs))+'</label>';
							row.insertCell(5).innerHTML = '<label class="col-12 text-center">'+numberWithCommas(Number(response.data["traffic-table"][i].Pir))+'</label>';
							row.insertCell(6).innerHTML = '<label class="col-12 text-center">'+numberWithCommas(Number(response.data["traffic-table"][i].Pbs))+'</label>';
							row.insertCell(7).innerHTML = '<label class="col-12 text-center">'+numberWithCommas(Number(response.data["traffic-table"][i].Priority))+'</label>';
							row.insertCell(8).innerHTML = '<label class="col-12 text-center">'+format_date(response.data["traffic-table"][i].CreatedDate)+'</label>';
					}
				}
			if(response.data["srv-profile"]){
					$("#datatable_srv-profile tbody>tr").remove();
					for (var i in response.data["srv-profile"]){
							var table = document.getElementById("datatable_srv-profile").getElementsByTagName('tbody')[0];
							var row = table.insertRow(-1);
							var pre = document.getElementById("datatable_srv-profile").getElementsByTagName('tbody')[0];
							row.insertCell(0).innerHTML =	Number(i)+1;
							row.insertCell(1).innerHTML = '<label class="col-12 text-center">'+response.data["srv-profile"][i].ProfileId+'</label>';
							row.insertCell(2).innerHTML = '<label class="col-12 text-center">'+response.data["srv-profile"][i].ProfileName+'</label>';
							row.insertCell(3).innerHTML = '<label class="col-12 text-center">'+response.data["srv-profile"][i].ProfileBinding+'</label>';
							row.insertCell(4).innerHTML = '<label class="col-12 text-center">'+format_date(response.data["srv-profile"][i].CreatedDate)+'</label>';
					}

				}
			if(response.data["line-profile"]){
					$("#datatable_line-profile tbody>tr").remove();
					for (var i in response.data["line-profile"]){
							var table = document.getElementById("datatable_line-profile").getElementsByTagName('tbody')[0];
							var row = table.insertRow(-1);
							row.insertCell(0).innerHTML = Number(i)+1;
							row.insertCell(1).innerHTML = '<label class="col-12 text-center">'+response.data["line-profile"][i].ProfileId+'</label>';
							row.insertCell(2).innerHTML = '<label class="col-12 text-center">'+response.data["line-profile"][i].ProfileName+'</label>';
							row.insertCell(3).innerHTML = '<label class="col-12 text-center">'+response.data["line-profile"][i].ProfileBinding+'</label>';
							row.insertCell(4).innerHTML = '<label class="col-12 text-center">'+format_date(response.data["line-profile"][i].CreatedDate)+'</label>';
					}

				}
			if(response.data["port-vlan"]){
					$("#datatable_port-vlan tbody>tr").remove();
					for (var i in response.data["port-vlan"]){
							var table = document.getElementById("datatable_port-vlan").getElementsByTagName('tbody')[0];
							var row = table.insertRow(-1);
							row.insertCell(0).innerHTML = Number(i)+1;
							row.insertCell(1).innerHTML = '<label class="col-12 text-center">'+response.data["port-vlan"][i].Index+'</label>';
							row.insertCell(2).innerHTML = '<label class="col-12 text-center">'+response.data["port-vlan"][i].Type+'</label>';
							row.insertCell(3).innerHTML = '<label class="col-12 text-center">'+response.data["port-vlan"][i].Desc+'</label>';
							row.insertCell(4).innerHTML = '<label class="col-12 text-center">'+response.data["port-vlan"][i].Attrib+'</label>';
							row.insertCell(5).innerHTML = '<label class="col-12 text-center">'+response.data["port-vlan"][i].Frame+'</label>';
							row.insertCell(6).innerHTML = '<label class="col-12 text-center">'+response.data["port-vlan"][i].Slot+'</label>';
							row.insertCell(7).innerHTML = '<label class="col-12 text-center">'+response.data["port-vlan"][i].Port+'</label>';
							row.insertCell(8).innerHTML = '<label class="col-12 text-center">'+format_date(response.data["port-vlan"][i].CreatedDate)+'</label>';
					}

				}
			if(response.data["dba-profile"]){
					$("#datatable_dba-profile tbody>tr").remove();
					for (var i in response.data["dba-profile"]){
							var table = document.getElementById("datatable_dba-profile").getElementsByTagName('tbody')[0];
							var row = table.insertRow(-1);
							row.insertCell(0).innerHTML = Number(i)+1;
							row.insertCell(1).innerHTML = '<label class="col-12 text-center">'+response.data["dba-profile"][i].ProfileId+'</label>';
							row.insertCell(2).innerHTML = '<label class="col-12 text-center">'+response.data["dba-profile"][i].Type+'</label>';
							row.insertCell(3).innerHTML = '<label class="col-12 text-center">'+response.data["dba-profile"][i].Bandwidth+'</label>';
							row.insertCell(4).innerHTML = '<label class="col-12 text-center">'+response.data["dba-profile"][i].Fix+'</label>';
							row.insertCell(5).innerHTML = '<label class="col-12 text-center">'+response.data["dba-profile"][i].Assure+'</label>';
							row.insertCell(6).innerHTML = '<label class="col-12 text-center">'+response.data["dba-profile"][i].Max+'</label>';
							row.insertCell(7).innerHTML = '<label class="col-12 text-center">'+response.data["dba-profile"][i].Bind+'</label>';
							row.insertCell(8).innerHTML = '<label class="col-12 text-center">'+format_date(response.data["dba-profile"][i].CreatedDate)+'</label>';
					}

				}
			if(response.data["gem-port"]){
					$("#datatable_gem-port tbody>tr").remove();
					for (var i in response.data["gem-port"]){
							var table = document.getElementById("datatable_gem-port").getElementsByTagName('tbody')[0];
							var row = table.insertRow(-1);
							row.insertCell(0).innerHTML = Number(i)+1;
							row.insertCell(1).innerHTML = '<label class="col-12 text-center">'+response.data["gem-port"][i].ProfileId+'</label>';
							row.insertCell(2).innerHTML = '<label class="col-12 text-center">'+response.data["gem-port"][i].GemId+'</label>';
							row.insertCell(3).innerHTML = '<label class="col-12 text-center">'+response.data["gem-port"][i].GemMapping+'</label>';
							row.insertCell(4).innerHTML = '<label class="col-12 text-center">'+response.data["gem-port"][i].GemVlan+'</label>';
							row.insertCell(5).innerHTML = '<label class="col-12 text-center">'+response.data["gem-port"][i].Priority+'</label>';
							row.insertCell(6).innerHTML = '<label class="col-12 text-center">'+response.data["gem-port"][i].Type+'</label>';
							row.insertCell(7).innerHTML = '<label class="col-12 text-center">'+response.data["gem-port"][i].Port+'</label>';
							row.insertCell(8).innerHTML = '<label class="col-12 text-center">'+response.data["gem-port"][i].Flow+'</label>';
							row.insertCell(9).innerHTML = '<label class="col-12 text-center">'+response.data["gem-port"][i].Transparent+'</label>';
							row.insertCell(10).innerHTML = '<label class="col-12 text-center">'+format_date(response.data["gem-port"][i].CreatedDate)+'</label>';
					}

				}
			if(response.data["service-port"]){
					$("#datatable_service-port tbody>tr").remove();
					for (var i in response.data["service-port"]){
							var table = document.getElementById("datatable_service-port").getElementsByTagName('tbody')[0];
							var row = table.insertRow(-1);
							row.insertCell(0).innerHTML = Number(i)+1;
							row.insertCell(1).innerHTML = '<label class="col-12 text-center">'+response.data["service-port"][i].Index+'</label>';
							row.insertCell(2).innerHTML = '<label class="col-12 text-center">'+response.data["service-port"][i].VlanId+'</label>';
							row.insertCell(3).innerHTML = '<label class="col-12 text-center">'+response.data["service-port"][i].VlanAttr+'</label>';
							row.insertCell(4).innerHTML = '<label class="col-12 text-center">'+response.data["service-port"][i].Type+'</label>';
							row.insertCell(5).innerHTML = '<label class="col-12 text-center">'+response.data["service-port"][i].FrameId+'</label>';
							row.insertCell(6).innerHTML = '<label class="col-12 text-center">'+response.data["service-port"][i].SlotId+'</label>';
							row.insertCell(7).innerHTML = '<label class="col-12 text-center">'+response.data["service-port"][i].PortId+'</label>';
							row.insertCell(8).innerHTML = '<label class="col-12 text-center">'+response.data["service-port"][i].VPI+'</label>';
							row.insertCell(9).innerHTML = '<label class="col-12 text-center">'+response.data["service-port"][i].VCI+'</label>';
							row.insertCell(10).innerHTML = '<label class="col-12 text-center">'+response.data["service-port"][i].FlowType+'</label>';
							row.insertCell(11).innerHTML = '<label class="col-12 text-center">'+response.data["service-port"][i].FlowPara+'</label>';
							row.insertCell(12).innerHTML = '<label class="col-12 text-center">'+response.data["service-port"][i].Rx+'</label>';
							row.insertCell(13).innerHTML = '<label class="col-12 text-center">'+response.data["service-port"][i].Tx+'</label>';
							row.insertCell(14).innerHTML = '<label class="col-12 text-center">'+response.data["service-port"][i].State+'</label>';
							row.insertCell(15).innerHTML = '<label class="col-12 text-center">'+response.data["service-port"][i].Transparent+'</label>';
							row.insertCell(16).innerHTML = '<label class="col-12 text-center">'+format_date(response.data["service-port"][i].CreatedDate)+'</label>';
					}

				}
			if(response.data["ont-info"]){
					$("#datatable_ont-info tbody>tr").remove();
					for (var i in response.data["ont-info"]){
							var table = document.getElementById("datatable_ont-info").getElementsByTagName('tbody')[0];
							var row = table.insertRow(-1);
							if(response.data["ont-info"][i].Run != "online"){
								row.style.backgroundColor="#ffa800";
							}
							row.insertCell(0).innerHTML = Number(i)+1;
							row.insertCell(1).innerHTML = '<label class="col-12 text-center">'+response.data["ont-info"][i].FrameId+'</label>';
							row.insertCell(2).innerHTML = '<label class="col-12 text-center">'+response.data["ont-info"][i].SlotID+'</label>';
							row.insertCell(3).innerHTML = '<label class="col-12 text-center">'+response.data["ont-info"][i].PortId+'</label>';
							row.insertCell(4).innerHTML = '<label class="col-12 text-center">'+response.data["ont-info"][i].OntId+'</label>';
							row.insertCell(5).innerHTML = '<label class="col-12 text-center">'+response.data["ont-info"][i].OntSn+'</label>';
							row.insertCell(6).innerHTML = '<label class="col-12 text-center">'+response.data["ont-info"][i].Description+'</label>';
							row.insertCell(7).innerHTML = '<label class="col-12 text-center">'+response.data["ont-info"][i].Flag+'</label>';
							row.insertCell(8).innerHTML = '<label class="col-12 text-center">'+response.data["ont-info"][i].Run+'</label>';
							row.insertCell(9).innerHTML = '<label class="col-12 text-center">'+response.data["ont-info"][i].Config+'</label>';
							row.insertCell(10).innerHTML = '<label class="col-12 text-center">'+response.data["ont-info"][i].Match+'</label>';
							row.insertCell(11).innerHTML = '<label class="col-12 text-center">'+response.data["ont-info"][i].Protect+'</label>';
							row.insertCell(12).innerHTML = '<label class="col-12 text-center">'+format_date(response.data["ont-info"][i].CreatedDate)+'</label>';
					}

				}
			if(response.data["ont-autofind"]){
					$("#datatable_ont-autofind tbody>tr").remove();
					for (var i in response.data["ont-autofind"]){
							var table = document.getElementById("datatable_ont-autofind").getElementsByTagName('tbody')[0];
							var row = table.insertRow(-1);
							row.insertCell(0).innerHTML = Number(i)+1;
							row.insertCell(1).innerHTML = '<label class="col-12 text-center">'+response.data["ont-autofind"][i].Sn+'</label>';
							row.insertCell(2).innerHTML = '<label class="col-12 text-center">'+response.data["ont-autofind"][i].Version+'</label>';
							row.insertCell(3).innerHTML = '<label class="col-12 text-center">'+response.data["ont-autofind"][i].SoftwareVersion+'</label>';
							row.insertCell(4).innerHTML = '<label class="col-12 text-center">'+response.data["ont-autofind"][i].EquipmentId+'</label>';
							row.insertCell(5).innerHTML = '<label class="col-12 text-center">'+response.data["ont-autofind"][i].FrameId+'</label>';
							row.insertCell(6).innerHTML = '<label class="col-12 text-center">'+response.data["ont-autofind"][i].SlotId+'</label>';
							row.insertCell(7).innerHTML = '<label class="col-12 text-center">'+response.data["ont-autofind"][i].PortId+'</label>';
							row.insertCell(8).innerHTML = '<label class="col-12 text-center">'+response.data["ont-autofind"][i].RxPower+'</label>';
							row.insertCell(9).innerHTML = '<label class="col-12 text-center">'+response.data["ont-autofind"][i].TxPower+'</label>';
							row.insertCell(10).innerHTML = '<label class="col-12 text-center">'+response.data["ont-autofind"][i].Remark+'</label>';
							row.insertCell(11).innerHTML = '<label class="col-12 text-center">'+response.data["ont-autofind"][i].CreatedDate+'</label>';
					}

				}
				if(response.data["traffic-table-last"]){
						$("#datatable_traffic-table-last tbody>tr").remove();
						for (var i in response.data["traffic-table-last"]){
								var table = document.getElementById("datatable_traffic-table-last").getElementsByTagName('tbody')[0];
								var row = table.insertRow(-1);
								row.insertCell(0).innerHTML = '<label class="col-12 text-center">'+response.data["traffic-table-last"][i].Index+'</label>';
								row.insertCell(1).innerHTML = '<label class="col-12 text-center">'+response.data["traffic-table-last"][i].Name+'</label>';
								row.insertCell(2).innerHTML = '<label class="col-12 text-center">'+numberWithCommas(Number(response.data["traffic-table-last"][i].Cir))+'</label>';
								row.insertCell(3).innerHTML = '<label class="col-12 text-center">'+numberWithCommas(Number(response.data["traffic-table-last"][i].Cbs))+'</label>';
								row.insertCell(4).innerHTML = '<label class="col-12 text-center">'+numberWithCommas(Number(response.data["traffic-table-last"][i].Pir))+'</label>';
								row.insertCell(5).innerHTML = '<label class="col-12 text-center">'+numberWithCommas(Number(response.data["traffic-table-last"][i].Pbs))+'</label>';
								row.insertCell(6).innerHTML = '<label class="col-12 text-center">'+numberWithCommas(Number(response.data["traffic-table-last"][i].Priority))+'</label>';
								row.insertCell(7).innerHTML = '<label class="col-12 text-center">'+format_date(response.data["traffic-table-last"][i].CreatedDate)+'</label>';
						}
					}
				if(response.data["srv-profile-last"]){
						$("#datatable_srv-profile-last tbody>tr").remove();
						for (var i in response.data["srv-profile-last"]){
								var table = document.getElementById("datatable_srv-profile-last").getElementsByTagName('tbody')[0];
								var row = table.insertRow(-1);
								row.insertCell(0).innerHTML = '<label class="col-12 text-center">'+response.data["srv-profile-last"][i].ProfileId+'</label>';
								row.insertCell(1).innerHTML = '<label class="col-12 text-center">'+response.data["srv-profile-last"][i].ProfileName+'</label>';
								row.insertCell(2).innerHTML = '<label class="col-12 text-center">'+response.data["srv-profile-last"][i].ProfileBinding+'</label>';
								row.insertCell(3).innerHTML = '<label class="col-12 text-center">'+format_date(response.data["srv-profile-last"][i].CreatedDate)+'</label>';
						}
					}
				if(response.data["line-profile-last"]){
						$("#datatable_line-profile-last tbody>tr").remove();
						for (var i in response.data["line-profile-last"]){
								var table = document.getElementById("datatable_line-profile-last").getElementsByTagName('tbody')[0];
								var row = table.insertRow(-1);
								row.insertCell(0).innerHTML = '<label class="col-12 text-center">'+response.data["line-profile-last"][i].ProfileId+'</label>';
								row.insertCell(1).innerHTML = '<label class="col-12 text-center">'+response.data["line-profile-last"][i].ProfileName+'</label>';
								row.insertCell(2).innerHTML = '<label class="col-12 text-center">'+response.data["line-profile-last"][i].ProfileBinding+'</label>';
								row.insertCell(3).innerHTML = '<label class="col-12 text-center">'+format_date(response.data["line-profile-last"][i].CreatedDate)+'</label>';
						}
					}
				if(response.data["port-vlan-last"]){
						$("#datatable_port-vlan-last tbody>tr").remove();
						for (var i in response.data["port-vlan-last"]){
								var table = document.getElementById("datatable_port-vlan-last").getElementsByTagName('tbody')[0];
								var row = table.insertRow(-1);
								row.insertCell(0).innerHTML = '<label class="col-12 text-center">'+response.data["port-vlan-last"][i].Index+'</label>';
								row.insertCell(1).innerHTML = '<label class="col-12 text-center">'+response.data["port-vlan-last"][i].Type+'</label>';
								row.insertCell(2).innerHTML = '<label class="col-12 text-center">'+response.data["port-vlan-last"][i].Desc+'</label>';
								row.insertCell(3).innerHTML = '<label class="col-12 text-center">'+response.data["port-vlan-last"][i].Attrib+'</label>';
								row.insertCell(4).innerHTML = '<label class="col-12 text-center">'+response.data["port-vlan-last"][i].Frame+'</label>';
								row.insertCell(5).innerHTML = '<label class="col-12 text-center">'+response.data["port-vlan-last"][i].Slot+'</label>';
								row.insertCell(6).innerHTML = '<label class="col-12 text-center">'+response.data["port-vlan-last"][i].Port+'</label>';
								row.insertCell(7).innerHTML = '<label class="col-12 text-center">'+format_date(response.data["port-vlan-last"][i].CreatedDate)+'</label>';
						}
					}
					if(response.data["dba-profile-last"]){
							$("#datatable_dba-profile-last tbody>tr").remove();
							for (var i in response.data["dba-profile-last"]){
									var table = document.getElementById("datatable_dba-profile-last").getElementsByTagName('tbody')[0];
									var row = table.insertRow(-1);
									row.insertCell(0).innerHTML = '<label class="col-12 text-center">'+response.data["dba-profile-last"][i].ProfileId+'</label>';
									row.insertCell(1).innerHTML = '<label class="col-12 text-center">'+response.data["dba-profile-last"][i].Type+'</label>';
									row.insertCell(2).innerHTML = '<label class="col-12 text-center">'+response.data["dba-profile-last"][i].Bandwidth+'</label>';
									row.insertCell(3).innerHTML = '<label class="col-12 text-center">'+response.data["dba-profile-last"][i].Fix+'</label>';
									row.insertCell(4).innerHTML = '<label class="col-12 text-center">'+response.data["dba-profile-last"][i].Assure+'</label>';
									row.insertCell(5).innerHTML = '<label class="col-12 text-center">'+response.data["dba-profile-last"][i].Max+'</label>';
									row.insertCell(6).innerHTML = '<label class="col-12 text-center">'+response.data["dba-profile-last"][i].Bind+'</label>';
									row.insertCell(7).innerHTML = '<label class="col-12 text-center">'+format_date(response.data["dba-profile-last"][i].CreatedDate)+'</label>';
							}
						}
					if(response.data["gem-port-last"]){
							$("#datatable_gem-port-last tbody>tr").remove();
							for (var i in response.data["gem-port-last"]){
									var table = document.getElementById("datatable_gem-port-last").getElementsByTagName('tbody')[0];
									var row = table.insertRow(-1);
									row.insertCell(0).innerHTML = '<label class="col-12 text-center">'+response.data["gem-port-last"][i].ProfileId+'</label>';
									row.insertCell(1).innerHTML = '<label class="col-12 text-center">'+response.data["gem-port-last"][i].GemId+'</label>';
									row.insertCell(2).innerHTML = '<label class="col-12 text-center">'+response.data["gem-port-last"][i].GemMapping+'</label>';
									row.insertCell(3).innerHTML = '<label class="col-12 text-center">'+response.data["gem-port-last"][i].GemVlan+'</label>';
									row.insertCell(4).innerHTML = '<label class="col-12 text-center">'+response.data["gem-port-last"][i].Priority+'</label>';
									row.insertCell(5).innerHTML = '<label class="col-12 text-center">'+response.data["gem-port-last"][i].Type+'</label>';
									row.insertCell(6).innerHTML = '<label class="col-12 text-center">'+response.data["gem-port-last"][i].Port+'</label>';
									row.insertCell(7).innerHTML = '<label class="col-12 text-center">'+response.data["gem-port-last"][i].Flow+'</label>';
									row.insertCell(8).innerHTML = '<label class="col-12 text-center">'+response.data["gem-port-last"][i].Transparent+'</label>';
									row.insertCell(9).innerHTML = '<label class="col-12 text-center">'+format_date(response.data["gem-port-last"][i].CreatedDate)+'</label>';
							}
						}
					if(response.data["service-port-last"]){
							$("#datatable_service-port-last tbody>tr").remove();
							for (var i in response.data["service-port-last"]){
									var table = document.getElementById("datatable_service-port-last").getElementsByTagName('tbody')[0];
									var row = table.insertRow(-1);
									row.insertCell(0).innerHTML = '<label class="col-12 text-center">'+response.data["service-port-last"][i].Index+'</label>';
									row.insertCell(1).innerHTML = '<label class="col-12 text-center">'+response.data["service-port-last"][i].VlanId+'</label>';
									row.insertCell(2).innerHTML = '<label class="col-12 text-center">'+response.data["service-port-last"][i].VlanAttr+'</label>';
									row.insertCell(3).innerHTML = '<label class="col-12 text-center">'+response.data["service-port-last"][i].Type+'</label>';
									row.insertCell(4).innerHTML = '<label class="col-12 text-center">'+response.data["service-port-last"][i].FrameId+'</label>';
									row.insertCell(5).innerHTML = '<label class="col-12 text-center">'+response.data["service-port-last"][i].SlotId+'</label>';
									row.insertCell(6).innerHTML = '<label class="col-12 text-center">'+response.data["service-port-last"][i].PortId+'</label>';
									row.insertCell(7).innerHTML = '<label class="col-12 text-center">'+response.data["service-port-last"][i].VPI+'</label>';
									row.insertCell(8).innerHTML = '<label class="col-12 text-center">'+response.data["service-port-last"][i].VCI+'</label>';
									row.insertCell(9).innerHTML = '<label class="col-12 text-center">'+response.data["service-port-last"][i].FlowType+'</label>';
									row.insertCell(10).innerHTML = '<label class="col-12 text-center">'+response.data["service-port-last"][i].FlowPara+'</label>';
									row.insertCell(11).innerHTML = '<label class="col-12 text-center">'+response.data["service-port-last"][i].Rx+'</label>';
									row.insertCell(12).innerHTML = '<label class="col-12 text-center">'+response.data["service-port-last"][i].Tx+'</label>';
									row.insertCell(13).innerHTML = '<label class="col-12 text-center">'+response.data["service-port-last"][i].State+'</label>';
									row.insertCell(14).innerHTML = '<label class="col-12 text-center">'+response.data["service-port-last"][i].Transparent+'</label>';
									row.insertCell(15).innerHTML = '<label class="col-12 text-center">'+format_date(response.data["service-port-last"][i].CreatedDate)+'</label>';
							}
						}
					if(response.data["ont-info-last"]){
							$("#datatable_ont-info-last tbody>tr").remove();
							for (var i in response.data["ont-info-last"]){
									var table = document.getElementById("datatable_ont-info-last").getElementsByTagName('tbody')[0];
									var row = table.insertRow(-1);
									if(response.data["datatable_ont-info-last"][i].Run != "online"){
										row.style.backgroundColor="#ffa800";
									}
									row.insertCell(0).innerHTML = '<label class="col-12 text-center">'+response.data["datatable_ont-info-last"][i].FrameId+'</label>';
									row.insertCell(1).innerHTML = '<label class="col-12 text-center">'+response.data["datatable_ont-info-last"][i].SlotID+'</label>';
									row.insertCell(2).innerHTML = '<label class="col-12 text-center">'+response.data["datatable_ont-info-last"][i].PortId+'</label>';
									row.insertCell(3).innerHTML = '<label class="col-12 text-center">'+response.data["datatable_ont-info-last"][i].OntId+'</label>';
									row.insertCell(4).innerHTML = '<label class="col-12 text-center">'+response.data["datatable_ont-info-last"][i].OntSn+'</label>';
									row.insertCell(5).innerHTML = '<label class="col-12 text-center">'+response.data["datatable_ont-info-last"][i].Description+'</label>';
									row.insertCell(6).innerHTML = '<label class="col-12 text-center">'+response.data["datatable_ont-info-last"][i].Flag+'</label>';
									row.insertCell(7).innerHTML = '<label class="col-12 text-center">'+response.data["datatable_ont-info-last"][i].Run+'</label>';
									row.insertCell(8).innerHTML = '<label class="col-12 text-center">'+response.data["datatable_ont-info-last"][i].Config+'</label>';
									row.insertCell(9).innerHTML = '<label class="col-12 text-center">'+response.data["datatable_ont-info-last"][i].Match+'</label>';
									row.insertCell(10).innerHTML = '<label class="col-12 text-center">'+response.data["datatable_ont-info-last"][i].Protect+'</label>';
									row.insertCell(11).innerHTML = '<label class="col-12 text-center">'+format_date(response.data["datatable_ont-info-last"][i].CreatedDate)+'</label>';
							}

						}
					if(response.data["ont-autofind-last"]){
							$("#datatable_ont-autofind-last tbody>tr").remove();
							for (var i in response.data["ont-autofind-last"]){
									var table = document.getElementById("datatable_ont-autofind-last").getElementsByTagName('tbody')[0];
									var row = table.insertRow(-1);
									row.insertCell(0).innerHTML = '<label class="col-12 text-center">'+response.data["ont-autofind-last"][i].Sn+'</label>';
									row.insertCell(1).innerHTML = '<label class="col-12 text-center">'+response.data["ont-autofind-last"][i].Version+'</label>';
									row.insertCell(2).innerHTML = '<label class="col-12 text-center">'+response.data["ont-autofind-last"][i].SoftwareVersion+'</label>';
									row.insertCell(3).innerHTML = '<label class="col-12 text-center">'+response.data["ont-autofind-last"][i].EquipmentId+'</label>';
									row.insertCell(4).innerHTML = '<label class="col-12 text-center">'+response.data["ont-autofind-last"][i].FrameId+'</label>';
									row.insertCell(5).innerHTML = '<label class="col-12 text-center">'+response.data["ont-autofind-last"][i].SlotId+'</label>';
									row.insertCell(6).innerHTML = '<label class="col-12 text-center">'+response.data["ont-autofind-last"][i].PortId+'</label>';
									row.insertCell(7).innerHTML = '<label class="col-12 text-center">'+response.data["ont-autofind-last"][i].RxPower+'</label>';
									row.insertCell(8).innerHTML = '<label class="col-12 text-center">'+response.data["ont-autofind-last"][i].TxPower+'</label>';
									row.insertCell(9).innerHTML = '<label class="col-12 text-center">'+response.data["ont-autofind-last"][i].Remark+'</label>';
									row.insertCell(10).innerHTML = '<label class="col-12 text-center">'+response.data["ont-autofind-last"][i].CreatedDate+'</label>';
							}

						}
	}
  function refresh_table() {
      table.fnDraw();
  }
</script>
@endsection
