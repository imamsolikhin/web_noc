@extends('layout.default')

@section('content')
<div class="subheader py-2 {{ Metronic::printClasses('subheader', false) }}" id="kt_subheader">
  <div class="{{ Metronic::printClasses('subheader-container', false) }} d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
    <div class="d-flex align-items-baseline flex-wrap mr-5">
      <h5 class="text-dark font-weight-bold my-1 mr-5">
        Clients Switch
      </h5>
      <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
        <li class="breadcrumb-item">
          <a href="{{ route('dashboard') }}" class="text-muted">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
          <a href="{{ route('olt.customer-switch.index') }}" class="text-muted">Olt</a>
        </li>
        <li class="breadcrumb-item">
            <a href="#form" class="text-muted">Form Input</a>
        </li>
      </ul>
    </div>
  </div>
</div>

@include('inc.error-list')
@include('inc.success-notif')
<div class="card card-custom">
  <div class="card-header bg-danger flex-wrap border-1 pt-1 pb-0" style="min-height: 0;">
    <div class="card-title pt-1 pb-0">
      <h3 class="card-label font-weight-bolder text-white">Form Input
      </h3>
    </div>
  </div>
  <!-- <div class="card-body pt-1"> -->
  <div>
    <form class="form pt-0" id="form" action="@if (isset($CustomerSwitch->Code)) {{ route('olt.customer-switch.update',$CustomerSwitch->Code) }} @else {{ route('olt.customer-switch.store') }} @endif" method="POST">
        {!! csrf_field() !!}
        @if (isset($CustomerSwitch->Code)) {!! method_field('PUT') !!}  @else {!! method_field('POST') !!} @endif
        <div class="card-body pt-0 pl-3 pr-3">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card card-custom mb-1">
                        <div id="formInputAdjusmentIn" class="col-lg-8">
                            <div class="mb-1" hidden>
                                <div class="form-group row mb-0 pt-2">
                                    <label class="col-lg-4 col-form-label">Code</label>
                                    <div class="col-lg-5">
                                        <input type="text" class="form-control form-control-sm" id="Code" name="Code" placeholder="Enter Code" value="@isset ($CustomerSwitch->Code) {{ $CustomerSwitch->Code }} @endisset" />
                                    </div>
                                </div>
                            </div><div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">Name</label>
                                    <div class="col-lg-5">
                                         <input class="form-control form-control-sm" id="Name" name="Name" placeholder="Enter Name" value="@isset ($CustomerSwitch->Name) {{ $CustomerSwitch->Name }} @endisset" />
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">CompanyName</label>
                                    <div class="col-lg-5">
                                        <input type="text" class="form-control form-control-sm" id="CompanyName" name="CompanyName" placeholder="Enter CompanyName" value="@isset ($CustomerSwitch->CompanyName) {{ $CustomerSwitch->CompanyName }} @endisset" required/>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">LinkID</label>
                      							<div class="col-lg-5">
                      								<select class="form-control form-control-sm" id="LinkID"
                      									name="LinkID" style="width: 100%">
                      									<option value="" selected>Chose</option> @isset ($isp_list)
                      									@foreach($isp_list as $isp)
                      									<option value="{{ $isp->Code }}">{{ $isp->Name }}</option> @endforeach
                      									@endisset
                      								</select>
                      							</div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">MemberID</label>
                                    <div class="col-lg-5">
                                        <input type="text" class="form-control form-control-sm" id="MemberID" name="MemberID" placeholder="Enter MemberID" value="@isset ($CustomerSwitch->MemberID) {{ $CustomerSwitch->MemberID }} @endisset"/>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">JobTitle</label>
                                    <div class="col-lg-5">
                                        <input type="text" class="form-control form-control-sm" id="JobTitle" name="JobTitle" placeholder="Enter JobTitle" value="@isset ($CustomerSwitch->JobTitle) {{ $CustomerSwitch->JobTitle }} @endisset" />
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">ContactPerson</label>
                                    <div class="col-lg-5">
                                        <input type="text" class="form-control form-control-sm" id="ContactPerson" name="ContactPerson" placeholder="Enter ContactPerson" value="@isset ($CustomerSwitch->ContactPerson) {{ $CustomerSwitch->ContactPerson }} @endisset" required/>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">Email</label>
                                    <div class="col-lg-5">
                                        <input type="text" class="form-control form-control-sm" id="Email" name="Email" placeholder="Enter Email" value="@isset ($CustomerSwitch->Email) {{ $CustomerSwitch->Email }} @endisset"/>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">Phone1</label>
                                    <div class="col-lg-5">
                                        <input type="text" class="form-control form-control-sm" id="Phone1" name="Phone1" placeholder="Enter Phone1" value="@isset ($CustomerSwitch->Phone1) {{ $CustomerSwitch->Phone1 }} @endisset"/>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">Phone2</label>
                                    <div class="col-lg-5">
                                        <input type="text" class="form-control form-control-sm" id="Phone2" name="Phone2" placeholder="Enter Phone2" value="@isset ($CustomerSwitch->Phone2) {{ $CustomerSwitch->Phone2 }} @endisset"/>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">PhoneNumber</label>
                                    <div class="col-lg-5">
                                        <input type="text" class="form-control form-control-sm" id="PhoneNumber" name="PhoneNumber" placeholder="Enter PhoneNumber" value="@isset ($CustomerSwitch->PhoneNumber) {{ $CustomerSwitch->PhoneNumber }} @endisset"/>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">NPWP</label>
                                    <div class="col-lg-5">
                                        <input type="text" class="form-control form-control-sm" id="NPWP" name="NPWP" placeholder="Enter NPWP" value="@isset ($CustomerSwitch->NPWP) {{ $CustomerSwitch->NPWP }} @endisset"/>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">Fax</label>
                                    <div class="col-lg-5">
                                        <input type="text" class="form-control form-control-sm" id="Fax" name="Fax" placeholder="Enter Fax" value="@isset ($CustomerSwitch->Fax) {{ $CustomerSwitch->Fax }} @endisset"/>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">Address</label>
                                    <div class="col-lg-5">
                                        <input type="text" class="form-control form-control-sm" id="Address" name="Address" placeholder="Enter Address" value="@isset ($CustomerSwitch->Address) {{ $CustomerSwitch->Address }} @endisset" required/>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">CityCode</label>
                                    <div class="col-lg-5">
                                        <input type="text" class="form-control form-control-sm" id="CityCode" name="CityCode" placeholder="Enter CityCode" value="@isset ($CustomerSwitch->CityCode) {{ $CustomerSwitch->CityCode }} @endisset"/>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">City</label>
                                    <div class="col-lg-5">
                                        <input type="text" class="form-control form-control-sm" id="City" name="City" placeholder="Enter City" value="@isset ($CustomerSwitch->City) {{ $CustomerSwitch->City }} @endisset" required/>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">Province</label>
                                    <div class="col-lg-5">
                                        <input type="text" class="form-control form-control-sm" id="Province" name="Province" placeholder="Enter Province" value="@isset ($CustomerSwitch->Province) {{ $CustomerSwitch->Province }} @endisset" required/>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">ZipCode</label>
                                    <div class="col-lg-5">
                                        <input type="text" class="form-control form-control-sm" id="ZipCode" name="ZipCode" placeholder="Enter ZipCode" value="@isset ($CustomerSwitch->ZipCode) {{ $CustomerSwitch->ZipCode }} @endisset" required/>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">Website</label>
                                    <div class="col-lg-5">
                                        <input type="text" class="form-control form-control-sm" id="Website" name="Website" placeholder="Enter Website" value="@isset ($CustomerSwitch->Website) {{ $CustomerSwitch->Website }} @endisset"/>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">IDCard</label>
                                    <div class="col-lg-5">
                                        <input type="text" class="form-control form-control-sm" id="IDCard" name="IDCard" placeholder="Enter IDCard" value="@isset ($CustomerSwitch->IDCard) {{ $CustomerSwitch->IDCard }} @endisset"/>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">IDCardNumber</label>
                                    <div class="col-lg-5">
                                        <input type="text" class="form-control form-control-sm" id="IDCardNumber" name="IDCardNumber" placeholder="Enter IDCardNumber" value="@isset ($CustomerSwitch->IDCardNumber) {{ $CustomerSwitch->IDCardNumber }} @endisset" required/>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">IDCardExpired</label>
                                    <div class="col-lg-5">
                                        <input type="text" class="form-control form-control-sm" id="IDCardExpired" name="IDCardExpired" placeholder="Enter IDCardExpired" value="@isset ($CustomerSwitch->IDCardExpired) {{ $CustomerSwitch->IDCardExpired }} @endisset"/>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">ProductCode</label>
                                    <div class="col-lg-5">
                                        <input type="text" class="form-control form-control-sm" id="ProductCode" name="ProductCode" placeholder="Enter ProductCode" value="@isset ($CustomerSwitch->ProductCode) {{ $CustomerSwitch->ProductCode }} @endisset"/>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">ClientStatus</label>
                                    <div class="col-lg-5">
                                        <input type="text" class="form-control form-control-sm" id="ClientStatus" name="ClientStatus" placeholder="Enter ClientStatus" value="@isset ($CustomerSwitch->ClientStatus) {{ $CustomerSwitch->ClientStatus }} @endisset"/>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">ProductSite</label>
                                    <div class="col-lg-5">
                                        <input type="text" class="form-control form-control-sm" id="ProductSite" name="ProductSite" placeholder="Enter ProductSite" value="@isset ($CustomerSwitch->ProductSite) {{ $CustomerSwitch->ProductSite }} @endisset"/>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">ProductGrup</label>
                                    <div class="col-lg-5">
                                        <input type="text" class="form-control form-control-sm" id="ProductGrup" name="ProductGrup" placeholder="Enter ProductGrup" value="@isset ($CustomerSwitch->ProductGrup) {{ $CustomerSwitch->ProductGrup }} @endisset"/>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">ProductPort</label>
                                    <div class="col-lg-5">
                                        <input type="text" class="form-control form-control-sm" id="ProductPort" name="ProductPort" placeholder="Enter ProductPort" value="@isset ($CustomerSwitch->ProductPort) {{ $CustomerSwitch->ProductPort }} @endisset"/>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">ProductCategory</label>
                                    <div class="col-lg-5">
                                        <input type="text" class="form-control form-control-sm" id="ProductCategory" name="ProductCategory" placeholder="Enter ProductCategory" value="@isset ($CustomerSwitch->ProductCategory) {{ $CustomerSwitch->ProductCategory }} @endisset"/>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">BaseTransmissionStationCode</label>
                                    <div class="col-lg-5">
                                        <input type="text" class="form-control form-control-sm" id="BaseTransmissionStationCode" name="BaseTransmissionStationCode" placeholder="Enter BaseTransmissionStationCode" value="@isset ($CustomerSwitch->BaseTransmissionStationCode) {{ $CustomerSwitch->BaseTransmissionStationCode }} @endisset"/>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">ProductName</label>
                                    <div class="col-lg-5">
                                        <input type="text" class="form-control form-control-sm" id="ProductName" name="ProductName" placeholder="Enter ProductName" value="@isset ($CustomerSwitch->ProductName) {{ $CustomerSwitch->ProductName }} @endisset" required/>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">ProductNotes</label>
                                    <div class="col-lg-5">
                                        <input type="text" class="form-control form-control-sm" id="ProductNotes" name="ProductNotes" placeholder="Enter ProductNotes" value="@isset ($CustomerSwitch->ProductNotes) {{ $CustomerSwitch->ProductNotes }} @endisset"/>
                                    </div>
                                </div>
                            </div>
                  					<div class="mb-2">
                  						<div class="form-group row mb-0">
                  							<label class="col-lg-4 col-form-label">Status</label>
                  							<div class="col-lg-8">
                  								<input id="ActiveStatus" name="ActiveStatus" data-switch="true"
                  									type="checkbox" checked="checked" data-on-text="Enabled" data-handle-width="200" data-handle-font="1"
                  									data-off-text="Disabled" data-on-color="warning" />
                  							</div>
                  						</div>
                  					</div>
                            <div class="mb-2">
                              <div class="form-group row mb-0">
                                  <label class="col-lg-4 col-form-label"></label>
                                  <div class="col-lg-8">
                                      <button type="submit" class="btn btn-success font-weight-bold">Save</button>
                                      <a type="button" class="btn btn-danger" href="{{ route('olt.customer-switch.index') }}">Cancel</a>
                                  </div>
                              </div>
                            </div>
                          </div>
                          </div>
                        <div>
                      </form>
                  </div>
              </div>
            @endsection

{{-- Styles Section --}}
@section('styles')
<link rel="stylesheet" href="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.css">
<link rel="stylesheet" href="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.min.css">
<link rel="stylesheet" href="{{ config('app.url') }}plugins/custom/datatables/datatables.bundle.css">
<link rel="stylesheet" href="{{ config('app.url') }}css/inject.css">
@endsection

{{-- Scripts Section --}}
@section('scripts')
@include ('inc.confirm-delete-modal')
<script src="{{ config('app.url') }}global/vendor/datatables/jquery.dataTables.js"></script>
<script src="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.js"></script>
<script src="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.js"></script>
<script type="text/javascript">
 $(document).ready(function() {
     $('[data-switch=true]').bootstrapSwitch();
     $("#Branch").select2();
     if($("#Code").val() != ""){
       $("#Code").attr('readonly',true);
     }

 });
</script>
@endsection
