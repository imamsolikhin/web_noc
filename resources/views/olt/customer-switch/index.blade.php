@extends('layout.default')
 @section('content')
<div
	class="subheader py-2 {{ Metronic::printClasses('subheader', false) }}"
	id="kt_subheader">
	<div
		class="{{ Metronic::printClasses('subheader-container', false) }} d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
		<div class="d-flex align-items-baseline flex-wrap mr-5">
			<h5 class="text-dark font-weight-bold my-1 mr-5">CLients Switch</h5>
			<ul
				class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
				<li class="breadcrumb-item"><a href="{{ route('dashboard') }}"
					class="text-muted">Dashboard</a></li>
				<li class="breadcrumb-item"><a
					href="{{ route('olt.customer.index') }}" class="text-muted">Olt</a>
				</li>
				<li class="breadcrumb-item"><a href="#view" class="text-muted">Clients Switch</a>
				</li>
			</ul>
		</div>
	</div>
</div>

@include('inc.error-list') @include('inc.success-notif')
<div class="card card-custom">
	<div class="card-header bg-danger flex-wrap border-1 pt-1 pb-0 mb-2"
		style="min-height: 0;">
		<div class="card-title pt-1 pb-1">
			<h3 class="card-label font-weight-bolder text-white">
				Client Switch
				<div class="text-muted pt-2 font-size-lg">show Datatable from table
					Client Switch</div>
			</h3>
		</div>
		<div class="card-toolbar pt-1 pb-0">
			<a  href="{{ route('olt.customer-switch.page','new') }}" onclick="show_datas('')" class="btn btn-primary font-weight-bolder" style="background-color: #1e1e2d;border-color: #0c8eff;">
				<span class="svg-icon svg-icon-md">
					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
						<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
							<rect x="0" y="0" width="24" height="24"></rect>
							<circle fill="#000000" cx="9" cy="15" r="6"></circle>
							<path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3"></path>
						</g>
					</svg>
				</span>Add New
			</a>
		</div>
	</div>
	<div class="card-body pt-1">
		<table class="table table-bordered table-hover w100" cellspacing="0"
			id="datatable" style="width: 1070px !important;"></table>
	</div>
</div>



@endsection {{-- Styles Section --}}
@section('styles')
<link rel="stylesheet"
	href="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.css">
<link rel="stylesheet"
	href="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.min.css">
<link rel="stylesheet"
	href="{{ config('app.url') }}plugins/custom/datatables/datatables.bundle.css">
<link rel="stylesheet" href="{{ config('app.url') }}css/inject.css">
@endsection {{-- Scripts Section --}} @section('scripts') @include('inc.confirm-delete-modal')
<script
	src="{{ config('app.url') }}global/vendor/datatables/jquery.dataTables.js"></script>
<script
	src="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.js"></script>
<script
	src="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.js"></script>
<script type="text/javascript">
  $("html").keydown(function(e){
    if(e.shiftKey && e.keyCode == 'N'.charCodeAt(0) && !e.altKey){
         show_data('');
     }
  });
  $(document).ready(function() {
    $("#datatable_wrapper").removeClass("dataTables_wrapper form-inline dt-bootstrap no-footer");
    $("#datatable_wrapper").addClass("dataTables_wrapper dt-bootstrap4 no-footer");
  });

  $('[data-switch=true]').bootstrapSwitch();
  $("#DeviceModel").select2();
  $("#DeviceVersion").select2();
  var table = $('#datatable').dataTable({
    pageLength: 5,
    responsive: true,
    searchDelay: 800,
    processing: true,
    serverSide: true,
    select: true,
    searching: true,
    lengthMenu: [[5, 10, 25, 50, 100, 200, -1], [5, 10, 25, 50, 100, 200, "All"]],
    ajax: {
      method: 'POST',
      url : '{{ route('olt.customer-switch.data') }}',
      headers: {
        'X-CSRF-TOKEN': '{{ csrf_token() }}'
      }
    },
    columns: [
			{title: "No", data: 'DT_RowIndex', defaultContent: '-', class: 'text-center dt-body-nowrap', orderable: false, searchable: false, autoHide: false},
      {title: "Code", data: 'Code', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "Name", data: 'Name', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "CompanyName", data: 'CompanyName', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "LinkID", data: 'LinkID', defaultContent: '-', class: 'text-center dt-body-nowrap'},
			{title: "MemberID", data: 'MemberID', defaultContent: '-', class: 'text-center'},
			{title: "JobTitle", data: 'JobTitle', defaultContent: '-', class: 'text-center'},
			{title: "ContactPerson", data: 'ContactPerson', defaultContent: '-', class: 'text-center'},
			{title: "Email", data: 'Email', defaultContent: '-', class: 'text-center'},
			{title: "Phone1", data: 'Phone1', defaultContent: '-', class: 'text-center'},
			{title: "Phone2", data: 'Phone2', defaultContent: '-', class: 'text-center'},
			{title: "PhoneNumber", data: 'PhoneNumber', defaultContent: '-', class: 'text-center'},
			{title: "NPWP", data: 'NPWP', defaultContent: '-', class: 'text-center'},
			{title: "Fax", data: 'Fax', defaultContent: '-', class: 'text-center'},
			{title: "Address", data: 'Address', defaultContent: '-', class: 'text-center'},
			{title: "CityCode", data: 'CityCode', defaultContent: '-', class: 'text-center'},
			{title: "City", data: 'City', defaultContent: '-', class: 'text-center'},
			{title: "Province", data: 'Province', defaultContent: '-', class: 'text-center'},
			{title: "ZipCode", data: 'ZipCode', defaultContent: '-', class: 'text-center'},
			{title: "Website", data: 'Website', defaultContent: '-', class: 'text-center'},
			{title: "IDCard", data: 'IDCard', defaultContent: '-', class: 'text-center'},
			{title: "IDCardNumber", data: 'IDCardNumber', defaultContent: '-', class: 'text-center'},
			{title: "IDCardExpired", data: 'IDCardExpired', defaultContent: '-', class: 'text-center'},
			{title: "RegistrationDate", data: 'RegistrationDate', defaultContent: '-', class: 'text-center'},
			{title: "ProductCode", data: 'ProductCode', defaultContent: '-', class: 'text-center'},
			{title: "ClientStatus", data: 'ClientStatus', defaultContent: '-', class: 'text-center'},
			{title: "ProductSite", data: 'ProductSite', defaultContent: '-', class: 'text-center'},
			{title: "ProductGrup", data: 'ProductGrup', defaultContent: '-', class: 'text-center'},
			{title: "ProductPort", data: 'ProductPort', defaultContent: '-', class: 'text-center'},
			{title: "ProductCategory", data: 'ProductCategory', defaultContent: '-', class: 'text-center'},
			{title: "BaseTransmissionStationCode", data: 'BaseTransmissionStationCode', defaultContent: '-', class: 'text-center'},
			{title: "ProductName", data: 'ProductName', defaultContent: '-', class: 'text-center'},
      {title: "ProductNotes", data: 'ProductNotes', defaultContent: '-', class: 'text-center'},
      {title: "Status", data: 'active', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "Actions", data: 'action', orderable: false, responsivePriority: -1},
    ],
    order: [[1, 'asc']],
    bStateSave: true,
    columnDefs: [
      {
        targets: [0,-1],
        className: 'text-center visible dt-body-nowrap'
      },
    ],
    buttons: [
      {
        extend: 'print',
        text: 'Print current page',
        autoPrint: true,
        customize: function (win) {
          $(win.document.body)
          .css('font-size', '10pt')
          .prepend(
            '<img src="http://datatables.net/media/images/logo-fade.png" style="position:absolute; top:0; left:0;" />'
          );
          $(win.document.body).find('table')
          .addClass('compact')
          .css('font-size', 'inherit');
        },
        exportOptions: {
          columns: [0, 1, 'visible'],
          modifier: {
            page: 'current'
          },
        }
      }, {
        extend: 'copy',
        className: 'btn default',
        exportOptions: {
          columns: [0, 1, 'visible']
        }
      }, {
        extend: 'pdf',
        className: 'btn default',
        exportOptions: {
          columns: [0, 1, 'visible']
        }
      }, {
        extend: 'excelHtml5',
        className: 'btn default',
        excelStyles: {
          template: 'blue_medium',
        },
        exportOptions: {
          columns: [0, 1, 'visible']
        }
      }, {
        extend: 'csvHtml5',
        className: 'btn default',
        exportOptions: {
          columns: [0, 1, 'visible']
        }
      }, {
        text: 'Reload',
        className: 'btn default',
        action: function (e, dt, node, config) {
          dt.ajax.reload();
          alert_show('Datatable reloaded!', false);
        }
      },
      'colvis'
    ],
    initComplete: function() {
      $('.tl-tip').tooltip();
      @if (count($errors) > 0)
      // jQuery("html, body").animate({
      //   scrollTop: $('#add-form').offset().top - 100 // 66 for sticky bar height
      // }, "slow");
      @endif
    }
  });


  function refresh_table() {
      $('#datatable').dataTable().ajax.reload();
  }

  function format_date(val) {
      var date_arr = val.split(" ");
      var time = date_arr[1];
      var date_id_arr = date_arr[0].split("-");
      var date_fix = date_id_arr[2] + "-" + date_id_arr[1] + "-" + date_id_arr[0];
      var date_transaction = date_fix + " " + time;
      return date_transaction;
  }
</script>
@endsection
