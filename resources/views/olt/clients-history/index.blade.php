@extends('layout.default') @section('content')
<div
	class="subheader py-2 {{ Metronic::printClasses('subheader', false) }}"
	id="kt_subheader">
	<div
		class="{{ Metronic::printClasses('subheader-container', false) }} d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
		<div class="d-flex align-items-baseline flex-wrap mr-5">
			<h5 class="text-dark font-weight-bold my-1 mr-5">Clients History</h5>
			<ul
				class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
				<li class="breadcrumb-item"><a href="{{ route('dashboard') }}"
					class="text-muted">Dashboard</a></li>
				<li class="breadcrumb-item"><a
					href="{{ route('olt.clients.index') }}" class="text-muted">Clients History</a>
				</li>
				<li class="breadcrumb-item"><a href="#view" class="text-muted">Clients History</a>
				</li>
			</ul>
		</div>
	</div>
</div>

@include('inc.error-list')

@include('inc.success-notif')
<div class="card card-custom">
	<div class="card-header bg-danger flex-wrap border-1 pt-1 pb-0 mb-2" style="min-height: 0;">
		<div class="card-title pt-1 pb-1">
			<h3 class="card-label font-weight-bolder text-white">
				Clients History
				<div class="text-muted pt-2 font-size-lg">show Datatable from table
					Clients History</div>
			</h3>
		</div>
	</div>
	<div class="card-body pt-1">
    <div hidden><input id="start-date"/><input id="end-date"/></div>
		<table class="table table-bordered table-hover w100" cellspacing="0" id="datatable" style="width: 1070px !important;"></table>
	</div>
	<div class="card-body pt-1">
		<table class="table table-bordered table-hover w100" cellspacing="0" id="datatable" style="width: 1070px !important;"></table>
	</div>
	<div class="card-body py-1">
			<div class="card mb-8">
					<div class="col-lg-12">
						<div class="row justify-content-center py-2 px-2 py-md-10 px-md-0">
							<div class="col-md-12">
								<!-- <div class="border-bottom w-100"></div> -->
								<div class="table-responsive">
									<h3 class="pt-0"><div id="color-picker-1" class="mx-auto">Clients History</div></h3>
										<table id="datatable_clients_history" class="table dataTable table-bordered table-hover w100" cellspacing="0">
												<thead>
													<tr>
															<th class="text-center text-white dt-body-nowrap">History Date</th>
															<th class="text-center text-white dt-body-nowrap">Hostname</th>
															<th class="text-center text-white dt-body-nowrap">OntSn</th>
															<th class="text-center text-white dt-body-nowrap">Isp Name</th>
															<th class="text-center text-white dt-body-nowrap">CustomerName</th>
															<th class="text-center text-white dt-body-nowrap">Status</th>
															<th class="text-center text-white dt-body-nowrap">Note</th>
													</tr>
												</thead>
												<tbody>
												</tbody>
										</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>


@endsection

{{-- Styles Section --}}
@section('styles')
<link rel="stylesheet"
	href="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.css">
<link rel="stylesheet"
	href="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.min.css">
<link rel="stylesheet"
	href="{{ config('app.url') }}plugins/custom/datatables/datatables.bundle.css">
<link rel="stylesheet" href="{{ config('app.url') }}css/inject.css">
@endsection
{{-- Scripts Section --}}

@section('scripts')
@include('inc.confirm-delete-modal')
<script
	src="{{ config('app.url') }}global/vendor/datatables/jquery.dataTables.js"></script>
<script
	src="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.js"></script>
<script
	src="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.js"></script>
<script type="text/javascript">

$(document).ready(function() {
  $('[data-switch=true]').bootstrapSwitch();
  $("#DeviceCode").select2();

	$("div.datesearchbox").html('<div class="input-group"> <div class="input-group-addon"> <i class="glyphicon glyphicon-calendar"></i> </div><input type="text" class="form-control pull-right" id="datesearch" placeholder="Search by date range"> </div>');
	document.getElementsByClassName("datesearchbox")[0].style.textAlign = "center";
	$("#datesearch").attr("readonly",true);
	$('#datesearch').daterangepicker({
		 autoUpdateInput: false
	 });

	//menangani proses saat apply date range
	 $('#datesearch').on('apply.daterangepicker', function(ev, picker) {
			$(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
			$("#start-date").val(picker.startDate.format('YYYY-MM-DD'));
			$("#end-date").val(picker.endDate.format('YYYY-MM-DD'));
			refresh_table();
	 });

	 $('#datesearch').on('cancel.daterangepicker', function(ev, picker) {
		 $(this).val('');
		 $("#start-date").val('');
		 $("#end-date").val('');
		 refresh_table();
	 });
 });

	$("#Ont").select2();
	var id_clients ="";
	var sn_clients ="";
  var table = $('#datatable').dataTable({
    pageLength: 5,
    responsive: true,
    searchDelay: 800,
    processing: true,
    serverSide: true,
    searching: true,
		select: true,
		rowId: 'id',
    lengthMenu: [[5, 10, 25, 50, 100, 200, -1], [5, 10, 25, 50, 100, 200, "All"]],
    ajax: {
      method: 'POST',
      url : '{{ route('olt.clients.data') }}',
      headers: {
        'X-CSRF-TOKEN': '{{ csrf_token() }}'
      },
			data: function (d) {
				d.from_date = $("#start-date").val();
				d.to_date = $(" #end-date").val();
			}
    },
    columns: [
      {title: "No", data: 'DT_RowIndex', defaultContent: '-', class: 'text-center dt-body-nowrap', orderable: false, searchable: false, autoHide: false},
      {title: "Code", data: 'Code', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "Hostname", data: 'Hostname', defaultContent: '-', class: 'text-center dt-body-nowrap'},
			{title: "OntSn ", data: 'OntSn', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "Isp Name", data: 'IspName', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "Customer Name", data: 'CustomerName', defaultContent: '-', class: 'text-center', },
      {title: "Customer Email", data: 'CustomerEmail', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "CustomerCity", data: 'CustomerCity', defaultContent: '-', class: 'text-center dt-body-nowrap'},
    ],
		dom:  "<'row'<'col-sm-5'l><'col-sm-3' <'datesearchbox'>><'col-sm-3'f>>" +
          "<'row'<'col-sm-12'tr>>" +
          "<'row'<'col-sm-5'i><'col-sm-7'p>>",
    order: [[1, 'asc']],
    bStateSave: true,
    columnDefs: [
      {
        targets: [0,-1],
        className: 'text-center dt-body-nowrap'
      },
    ],
    buttons: [
      {
				// table.DataTable().button(0).trigger();
        extend: 'print',
        text: 'Print current page',
        autoPrint: true,
        customize: function (win) {
          $(win.document.body)
          .css('font-size', '10pt')
          .prepend(
            '<img src="http://datatables.net/media/images/logo-fade.png" style="position:absolute; top:0; left:0;" />'
          );
          $(win.document.body).find('table')
          .addClass('compact')
          .css('font-size', 'inherit');
        },
        exportOptions: {
          columns: [0, 1, 'visible'],
          modifier: {
            page: 'current'
          },
        }
      }, {
				// table.DataTable().button(1).trigger();
        extend: 'copy',
        className: 'btn default',
        exportOptions: {
          columns: [0, 1, 'visible']
        }
      }, {
				// table.DataTable().button(2).trigger();
        extend: 'pdf',
        className: 'btn default',
        exportOptions: {
          columns: [0, 1, 'visible']
        }
      }, {
				// table.DataTable().button(3).trigger();
        extend: 'excelHtml5',
        className: 'btn default',
        excelStyles: {
          template: 'blue_medium',
        },
        exportOptions: {
          columns: [0, 1, 'visible']
        }
      }, {
				// table.DataTable().button(4).trigger();
        extend: 'csvHtml5',
        className: 'btn default',
        exportOptions: {
          columns: [0, 1, 'visible']
        }
      }, {
				// table.DataTable().button(5).trigger();
        text: 'Reload',
        className: 'btn default',
        action: function (e, dt, node, config) {
          dt.ajax.reload();
          showDialog.show('Datatable reloaded!', false);
        }
      },
      'colvis'
    ],
    initComplete: function() {
      $('.tl-tip').tooltip();
    }
  });

	$('#datatable tbody').on( 'click', 'tr', function () {
    $("#datatable_clients_history tbody>tr").remove();
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
            $('.Code').html("Data not selected");
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            var id = $(this).find("td:eq(1)").html();
            $("#data-olt").loading("start");
            $.ajax({
                url: "{{ route('olt.clients.show-header','')}}/" + id,
                type: "GET",
                headers: {
                  'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                success: function (response) {
                  if(response.data["clients_history"]){
                      $("#datatable_clients_history tbody>tr").remove();
                      for (var i in response.data["clients_history"]){
                          var table = document.getElementById("datatable_clients_history").getElementsByTagName('tbody')[0];
                          var row = table.insertRow(-1);
                          row.insertCell(0).innerHTML = '<label class="col-12 text-center">'+format_date(response.data["clients_history"][i].CreatedDate)+'</label>';
                          row.insertCell(1).innerHTML = '<label class="col-12 text-center">'+response.data["clients_history"][i].Hostname+'</label>';
                          row.insertCell(2).innerHTML = '<label class="col-12 text-center">'+response.data["clients_history"][i].OntSn+'</label>';
                          row.insertCell(3).innerHTML = '<label class="col-12 text-center">'+response.data["clients_history"][i].IspName+'</label>';
                          row.insertCell(4).innerHTML = '<label class="col-12 text-center">'+response.data["clients_history"][i].CustomerName+'</label>';
													row.insertCell(5).innerHTML = '<label class="col-12 text-center">'+response.data["clients_history"][i].ClientStatus+'</label>';
                          row.insertCell(6).innerHTML = '<label class="col-12 text-center">'+response.data["clients_history"][i].Remark+'</label>';
                      }
                    }
										$("#data-olt").loading("stop");
                },
                error: function (xhr, status, error) {
                  $("#data-olt").loading("stop");
                  // showDialog.show(xhr.status + " " + status + " " + error, false);
                }
            });
        }
    });

		function refresh_table() {
				$('#datatable').dataTable().ajax.reload();
		}

  function format_date(val) {
			if(val==null){
				return "notset";
			}
      var date_arr = val.split(" ");
      var time = date_arr[1];
      var date_id_arr = date_arr[0].split("-");
      var date_fix = date_id_arr[2] + "-" + date_id_arr[1] + "-" + date_id_arr[0];
      var date_transaction = date_fix + " " + time;
      return date_transaction;
  }
	function refresh_table() {
      table.fnDraw();
		}
</script>
@endsection
