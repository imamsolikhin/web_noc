@extends('layout.default') @section('content')
<div
	class="subheader py-2 {{ Metronic::printClasses('subheader', false) }}"
	id="kt_subheader">
	<div
		class="{{ Metronic::printClasses('subheader-container', false) }} d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
		<div class="d-flex align-items-baseline flex-wrap mr-5">
			<h5 class="text-dark font-weight-bold my-1 mr-5">Signal Formula</h5>
			<ul
				class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
				<li class="breadcrumb-item"><a href="{{ route('dashboard') }}"
					class="text-muted">Dashboard</a></li>
				<li class="breadcrumb-item"><a
					href="{{ route('olt.signal-formula.index') }}" class="text-muted">Olt</a>
				</li>
				<li class="breadcrumb-item"><a href="#view" class="text-muted">Signal
						Formula</a></li>
			</ul>
		</div>
	</div>
</div>

@include('inc.error-list')
@include('inc.success-notif')
<div class="card card-custom">
	<div class="card-header bg-danger flex-wrap border-1 pt-1 pb-0 mb-2"
		style="min-height: 0;">
		<div class="card-title pt-1 pb-1">
			<h3 class="card-label font-weight-bolder text-white">
				Signal Formula
				<div class="text-muted pt-2 font-size-lg">show Datatable from table
					Host</div>
			</h3>
		</div>
		<div class="card-toolbar pt-1 pb-0">
			<a href="#" onclick="show_data('')"
				class="btn btn-primary font-weight-bolder"
				style="background-color: #1e1e2d; border-color: #0c8eff;"> <span
				class="svg-icon svg-icon-md"> <svg
						xmlns="http://www.w3.org/2000/svg"
						xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
						height="24px" viewBox="0 0 24 24" version="1.1">
            <g stroke="none" stroke-width="1" fill="none"
							fill-rule="evenodd">
              <rect x="0" y="0" width="24" height="24"></rect>
              <circle fill="#000000" cx="9" cy="15" r="6"></circle>
              <path
							d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z"
							fill="#000000" opacity="0.3"></path>
            </g>
          </svg>
			</span>Add New
			</a>
		</div>
	</div>
	<div class="card-body pt-1">
		<table class="table table-bordered table-hover w100" cellspacing="0"
			id="datatable" style="width: 1070px !important;"></table>
	</div>
</div>


<!-- Modal-->
<div class="modal fade" id="modal-form" data-backdrop="static"
	tabindex="-1" role="dialog" aria-labelledby="staticBackdrop"
	aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-danger pt-3 pb-3">
				<h5 class="modal-title text-white bold" id="modal">New Host Optical
					Line Terminal</h5>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<i aria-hidden="true" class="ki ki-close"></i>
				</button>
			</div>
			<form class="form" id="form-input"
				action="{{ route('olt.signal-formula.store') }}" method="POST">
				<div class="card-body pt-3">
					<div class="alert alert-custom alert-default pt-0 pb-0"
						role="alert">
						<div class="alert-icon">
							<i class="flaticon-warning text-primary"></i>
						</div>
						&nbsp;
						<div class="row">
							<div class="alert-text">
								Ideal <span class="svg-icon svg-icon-primary svg-icon-2x">
									<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                      <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                          <polygon points="0 0 24 0 24 24 0 24" />
                          <rect fill="#000000" opacity="0.3" x="11" y="3" width="2" height="14" rx="1" />
                          <path d="M6.70710678,10.7071068 C6.31658249,11.0976311 5.68341751,11.0976311 5.29289322,10.7071068 C4.90236893,10.3165825 4.90236893,9.68341751 5.29289322,9.29289322 L11.2928932,3.29289322 C11.6714722,2.91431428 12.2810586,2.90106866 12.6757246,3.26284586 L18.6757246,8.76284586 C19.0828436,9.13603827 19.1103465,9.76860564 18.7371541,10.1757246 C18.3639617,10.5828436 17.7313944,10.6103465 17.3242754,10.2371541 L12.0300757,5.38413782 L6.70710678,10.7071068 Z" fill="#000000" fill-rule="nonzero" />
                          <rect fill="#000000" opacity="0.3" x="3" y="19" width="18" height="2" rx="1" />
                      </g>
                  </svg>
								</span>
								=> TX Power - ((((Ont Distance/km)+ LossDb)+ LossFixTop)+ 17).
							</div>
							<div class="alert-text">
								Ideal <span class="svg-icon svg-icon-primary svg-icon-2x">
									<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                      <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                          <polygon points="0 0 24 0 24 24 0 24" />
                          <rect fill="#000000" opacity="0.3" x="11" y="7" width="2" height="14" rx="1" />
                          <path d="M6.70710678,20.7071068 C6.31658249,21.0976311 5.68341751,21.0976311 5.29289322,20.7071068 C4.90236893,20.3165825 4.90236893,19.6834175 5.29289322,19.2928932 L11.2928932,13.2928932 C11.6714722,12.9143143 12.2810586,12.9010687 12.6757246,13.2628459 L18.6757246,18.7628459 C19.0828436,19.1360383 19.1103465,19.7686056 18.7371541,20.1757246 C18.3639617,20.5828436 17.7313944,20.6103465 17.3242754,20.2371541 L12.0300757,15.3841378 L6.70710678,20.7071068 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000003, 16.999999) scale(1, -1) translate(-12.000003, -16.999999) " />
                          <rect fill="#000000" opacity="0.3" x="3" y="3" width="18" height="2" rx="1" />
                      </g>
                  </svg>
								</span>
								=> TX Power - ((((Ont Distance/km)+ LossDb)+ LossFixBot)+ 17).
							</div>
						</div>
					</div>
					<div class="mb-1" hidden>
						<div class="form-group row mb-0">
							<label class="col-lg-4 font-weight-bolder">Code <span class="text-danger">*</span></label>
							<div class="col-lg-8">
								{!! csrf_field() !!} <input type="hidden"
									class="form-control form-control-sm" id="method" name="_method"
									placeholder="Enter method" value="" /> <input type="text"
									class="form-control form-control-sm" id="Code" name="Code"
									placeholder="Enter Code" value="" />
							</div>
						</div>
					</div>
					<div class="mb-1">
						<div class="form-group row mb-0">
							<label class="col-lg-4 font-weight-bolder">Formula <span class="text-danger">*</span></label>
							<div class="col-lg-8">
								<input type="text" class="form-control form-control-sm"
									id="Formula" name="Formula" placeholder="Enter Formula Name"
									value="" />
							</div>
						</div>
					</div>
					<div class="mb-1" >
						<div class="form-group row mb-0">
							<label class="col-lg-4 font-weight-bolder">Tx Power <span class="text-danger">*</span></label>
							<div class="col-lg-8">
								<input type="text" class="form-control form-control-sm"
									id="TxPower" name="TxPower" placeholder="Enter Tx Power"
									value=""/>
							</div>
						</div>
					</div>
					<div class="mb-1">
						<div class="form-group row mb-0">
							<label class="col-lg-4 font-weight-bolder">Distance <span class="text-danger">*</span></label>
							<div class="col-lg-8">
								<input type="text" class="form-control form-control-sm"
									id="Distance" name="Distance" placeholder="Enter Distance"
									value="" />
							</div>
						</div>
					</div>
					<div class="mb-1">
						<div class="form-group row mb-0">
							<label class="col-lg-4 font-weight-bolder">Loss DB <span class="text-danger">*</span></label>
							<div class="col-lg-8">
								<input type="text" class="form-control form-control-sm"
									id="LossDb" name="LossDb" placeholder="Enter Loss DB" value="" required/>
							</div>
						</div>
					</div>
					<div class="mb-1">
						<div class="form-group row mb-0">
							<label class="col-lg-4 font-weight-bolder">Loss Fix Top <span class="text-danger">*</span></label>
							<div class="col-lg-8">
								<input type="text" class="form-control form-control-sm"
									id="LossFixTop" name="LossFixTop" placeholder="Enter Loss Fix"
									value="" />
							</div>
						</div>
					</div>
					<div class="mb-1">
						<div class="form-group row mb-0">
							<label class="col-lg-4 font-weight-bolder">Loss Fix Bot <span class="text-danger">*</span></label>
							<div class="col-lg-8">
								<input type="text" class="form-control form-control-sm"
									id="LossFixBot" name="LossFixBot" placeholder="Enter Loss Fix"
									value="" />
							</div>
						</div>
					</div>
					<div class="mb-2">
						<div class="form-group row mb-0">
							<label class="col-lg-4 col-form-label">Status</label>
							<div class="col-lg-8">
								<input id="ActiveStatus" name="ActiveStatus" data-switch="true"
									type="checkbox" checked="checked" data-on-text="Enabled"
									data-handle-width="200" data-handle-font="1"
									data-off-text="Disabled" data-on-color="warning" />
							</div>
						</div>
					</div>
					<div class="mb-2">
						<div class="form-group row mb-0">
							<label class="col-lg-4 col-form-label"></label>
							<div class="col-lg-8">
								<button type="submit" class="btn btn-success font-weight-bold">Save</button>
								<button type="button" data-dismiss="modal"
									class="btn btn-danger">Cancel</button>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection

{{-- Styles Section --}}
@section('styles')
<link rel="stylesheet"
	href="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.css">
<link rel="stylesheet"
	href="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.min.css">
<link rel="stylesheet"
	href="{{ config('app.url') }}plugins/custom/datatables/datatables.bundle.css">
<link rel="stylesheet" href="{{ config('app.url') }}css/inject.css">
@endsection

{{-- Scripts Section --}}
@section('scripts')
@include('inc.confirm-delete-modal')
<script
	src="{{ config('app.url') }}global/vendor/datatables/jquery.dataTables.js"></script>
<script
	src="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.js"></script>
<script
	src="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.js"></script>
<script type="text/javascript">
  $("html").keydown(function(e){
    if(e.shiftKey && e.keyCode == 'N'.charCodeAt(0) && !e.altKey){
         show_data('');
     }
  });
  $(document).ready(function() {
    $("#datatable_wrapper").removeClass("dataTables_wrapper form-inline dt-bootstrap no-footer");
    $("#datatable_wrapper").addClass("dataTables_wrapper dt-bootstrap4 no-footer");
  });

  $('[data-switch=true]').bootstrapSwitch();
  $("#DeviceModel").select2();
  $("#DeviceVersion").select2();
  var table = $('#datatable').dataTable({
    pageLength: 5,
    responsive: true,
    searchDelay: 800,
		pagingTypeSince : 'numbers',
    pagingType      : 'full_numbers',
		deferRender: true,
		render: true,
		stateSave: true,
		serverSide: true,
    processing: true,
    searching: true,
		select: true,
		rowId: 'id',
    lengthMenu: [[5, 10, 25, 50, 100, 200], [5, 10, 25, 50, 100, 200]],
    ajax: {
      method: 'POST',
      url : '{{ route('olt.signal-formula.data') }}',
      headers: {
        'X-CSRF-TOKEN': '{{ csrf_token() }}'
      }
    },
    columns: [
      {title: "No", data: 'DT_RowIndex', orderable: false, searchable: false, autoHide: false},
      {title: "Code", data: 'Code', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "Formula", data: 'Formula', autoHide: false, defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "TxPower", data: 'TxPower', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "Distance(m)", data: 'Distance', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "Loss DB", data: 'LossDb', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "Loss Fix Top", data: 'LossFixTop', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "Loss Fix Bot", data: 'LossFixBot', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "Status", data: 'active', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "Actions", data: 'action', orderable: false, responsivePriority: -1},
    ],
    order: [[1, 'asc']],
    bStateSave: true,
    columnDefs: [
      {
        render: function ( data, type, row ) {
						var url_destroy = "{{route('olt.signal-formula.destroy', 'prm_code')}}";
						var url_update = "{{route('olt.signal-formula.edit', 'prm_code')}}";
						var code = "'"+row.Code+"'";
						var status = "'"+row.ClientStatus+"'";
						var html = ""
        				html+= '<a onclick="show_data('+code+')" class="btn btn-icon btn-light btn-hover-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Update"><span class="svg-icon svg-icon-md svg-icon-primary"><i class="far flaticon-edit icon-lg"></i></span></a>';
        				html+= '<a data-href="'+url_destroy.replaceAll('prm_code',row.Code)+'" class="btn btn-icon btn-light btn-hover-danger btn-sm" "data-toggle="tooltip" data-placement="top" title="Delete" data-toggle="modal" data-target="#confirm-delete-modal" ><span class="svg-icon svg-icon-md svg-icon-danger"><i class="far flaticon-delete icon-lg"></i></span></a>';

						return html;
        },
        targets: [-1],
        className: 'text-center dt-body-nowrap'
      },
    ],

    initComplete: function() {
      $('.tl-tip').tooltip();
      @if (count($errors) > 0)
      // jQuery("html, body").animate({
      //   scrollTop: $('#add-form').offset().top - 100 // 66 for sticky bar height
      // }, "slow");
      @endif
    }
  });

  function show_data(id = "") {
      if (id !== "") {
          $.ajax({
              url: "{{ route('olt.signal-formula.store')}}/" + id,
              type: "GET",
              headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
              },
              success: function (response) {
                  $("#form-input").attr("action", "{{ route('olt.signal-formula.update','')}}/"+id);
                  $('#form-input').trigger("reset");
                  $('#method').val("PUT");
                  $('#Code').attr("readonly", true);
                  $('#Code').val(response.data.Code);
                  $('#Formula').val(response.data.Formula);
                  $('#TxPower').val(response.data.TxPower);
                  $('#Distance').val(response.data.Distance);
                  $('#LossDb').val(response.data.LossDb);
                  $('#LossFixTop').val(response.data.LossFixTop);
                  $('#LossFixBot').val(response.data.LossFixBot);
                  if (response.data.ActiveStatus === 1) {
                      $('#ActiveStatus').bootstrapSwitch('state', true);
                  } else {
                      $('#ActiveStatus').bootstrapSwitch('state', false);
                  }
                  $('#modal-form').modal('show');
                  $('#Code').focus();
              },
              error: function (xhr, status, error) {
                  alert_show(xhr.status + " " + status + " " + error, false);
              }
          });
      } else {
          $("#form-input").attr("action", "{{ route('olt.signal-formula.store')}}");
          $('#method').val("POST");
          $('#Code').attr("readonly", false);
          $('#Code').focus();
          $('#form-input').trigger("reset");
          $('#modal-form').modal('show');
          $('#Code').focus();
      }
  }

  function refresh_table() {
      $('#datatable').dataTable().ajax.reload();
  }

  function format_date(val) {
      var date_arr = val.split(" ");
      var time = date_arr[1];
      var date_id_arr = date_arr[0].split("-");
      var date_fix = date_id_arr[2] + "-" + date_id_arr[1] + "-" + date_id_arr[0];
      var date_transaction = date_fix + " " + time;
      return date_transaction;
  }
</script>
@endsection
