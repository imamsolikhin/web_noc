@extends('layout.default') @section('content')
<div
	class="subheader py-2 {{ Metronic::printClasses('subheader', false) }}"
	id="kt_subheader">
	<div
		class="{{ Metronic::printClasses('subheader-container', false) }} d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
		<div class="d-flex align-items-baseline flex-wrap mr-5">
			<h5 class="text-dark font-weight-bold my-1 mr-5">Customer Registration</h5>
			<ul
				class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
				<li class="breadcrumb-item"><a href="{{ route('dashboard') }}"
					class="text-muted">Dashboard</a></li>
				<li class="breadcrumb-item"><a
					href="{{ route('olt.customer-registration.index') }}" class="text-muted">Olt</a>
				</li>
				<li class="breadcrumb-item"><a href="#view" class="text-muted">Customer Registration</a>
				</li>
			</ul>
		</div>
	</div>
</div>

@include('inc.error-list') @include('inc.success-notif')
<div class="card card-custom">
	<div class="card-header bg-danger flex-wrap border-1 pt-1 pb-0 mb-2"
		style="min-height: 0;">
		<div class="card-title pt-1 pb-1">
			<h3 class="card-label font-weight-bolder text-white">
				Customer
				<div class="text-muted pt-2 font-size-lg">show Datatable from table
					Customer</div>
			</h3>
		</div>
	</div>
	<div class="card-body pt-1">
		<table class="table table-bordered table-hover w100" cellspacing="0"
			id="datatable" style="width: 1070px !important;"></table>
	</div>
</div>


<!-- Modal-->
<div class="modal fade" id="modal-form" data-backdrop="static"
	tabindex="-1" role="dialog" aria-labelledby="staticBackdrop"
	aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-danger pt-3 pb-3">
				<h5 class="modal-title text-white bold" id="modal">New Customer</h5>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<i aria-hidden="true" class="ki ki-close"></i>
				</button>
			</div>
			<form class="form" id="form-input"
				action="{{ route('olt.customer-registration.store') }}" method="POST">
				{!! csrf_field() !!} <input type="hidden"
					class="form-control form-control-sm" id="method" name="_method"
					placeholder="Enter method" value="POST" />
				<div class="card-body pt-3">
					<div class="mb-1">
						<div class="form-group row mb-0">
							<div class="col-lg-12">
								<label>Code</label> <input type="text"
									class="form-control form-control-sm" id="Code" name="Code"
									value="" placeholder="Enter Code" / required>
							</div>
						</div>
					</div>
					<div class="mb-1">
						<div class="form-group row mb-0">
							<div class="col-lg-12">
								<label>Name</label> <input type="text"
									class="form-control form-control-sm" id="Name" name="Name"
									value="" placeholder="Enter Name" / required>
							</div>
						</div>
					</div>
					<div class="mb-1">
						<div class="form-group row mb-0">
							<div class="col-lg-12">
								<label>Contact Person</label> <input type="text"
									class="form-control form-control-sm" id="ContactPerson"
									name="ContactPerson" value=""
									placeholder="Enter Contact Person" / required>
							</div>
						</div>
					</div>
					<div class="mb-1">
						<div class="form-group row mb-0">
							<div class="col-lg-12">
								<label>Address </label>
								<textarea class="form-control form-control-sm" rows="5"
									id="Address" name="Address"></textarea>
							</div>
						</div>
					</div>
					<div class="mb-1">
						<div class="form-group row mb-0">
							<div class="col-lg-12">
								<label>City Code</label> <input type="text"
									class="form-control form-control-sm" id="CityCode"
									name="CityCode" value="" placeholder="Enter CityCode"
									/ required>
							</div>
						</div>
					</div>
					<div class="mb-1">
						<div class="form-group row mb-0">
							<div class="col-lg-12">
								<label>Phone1</label> <input type="text"
									class="form-control form-control-sm" id="Phone1" name="Phone1"
									value="" placeholder="Enter Phone1" />
							</div>
						</div>
					</div>
					<div class="mb-1">
						<div class="form-group row mb-0">
							<div class="col-lg-12">
								<label>Phone2</label> <input type="text"
									class="form-control form-control-sm" id="Phone2" name="Phone2"
									value="" placeholder="Enter Phone2" />
							</div>
						</div>
					</div>
					<div class="mb-1">
						<div class="form-group row mb-0">
							<div class="col-lg-12">
								<label>Fax</label> <input type="text"
									class="form-control form-control-sm" id="Fax" name="Fax"
									value="" placeholder="Enter Fax" />
							</div>
						</div>
					</div>
					<div class="mb-1">
						<div class="form-group row mb-0">
							<div class="col-lg-12">
								<label>Email</label> <input type="email"
									class="form-control form-control-sm" id="Email" name="Email"
									value="" placeholder="Enter Email" />
							</div>
						</div>
					</div>
					<div class="mb-2">
						<div class="form-group row mb-0">
							<label class="col-lg-4 col-form-label">Status</label>
							<div class="col-lg-8">
								<input id="ActiveStatus" name="ActiveStatus" data-switch="true"
									type="checkbox" checked="checked" data-on-text="Enabled"
									data-handle-width="200" data-handle-font="1"
									data-off-text="Disabled" data-on-color="warning" />
							</div>
						</div>
					</div>
					<div class="mb-2">
						<div class="form-group row mb-0">
							<label class="col-lg-4 col-form-label"></label>
							<div class="col-lg-8">
								<button type="submit" class="btn btn-success font-weight-bold">Save</button>
								<button type="button" data-dismiss="modal"
									class="btn btn-danger">Cancel</button>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection {{-- Styles Section --}} @section('styles')
<link rel="stylesheet"
	href="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.css">
<link rel="stylesheet"
	href="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.min.css">
<link rel="stylesheet"
	href="{{ config('app.url') }}plugins/custom/datatables/datatables.bundle.css">
<link rel="stylesheet" href="{{ config('app.url') }}css/inject.css">
@endsection {{-- Scripts Section --}} @section('scripts') @include('inc.confirm-delete-modal')
<script
	src="{{ config('app.url') }}global/vendor/datatables/jquery.dataTables.js"></script>
<script
	src="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.js"></script>
<script
	src="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.js"></script>
<script type="text/javascript">
  $("html").keydown(function(e){
    if(e.shiftKey && e.keyCode == 'N'.charCodeAt(0) && !e.altKey){
         show_data('');
     }
  });
  $(document).ready(function() {
    $("#datatable_wrapper").removeClass("dataTables_wrapper form-inline dt-bootstrap no-footer");
    $("#datatable_wrapper").addClass("dataTables_wrapper dt-bootstrap4 no-footer");
  });

  $('[data-switch=true]').bootstrapSwitch();
  $("#DeviceModel").select2();
  $("#DeviceVersion").select2();
  var table = $('#datatable').dataTable({
    pageLength: 5,
    responsive: true,
    searchDelay: 800,
    processing: true,
    serverSide: true,
    select: true,
    searching: true,
    lengthMenu: [[5, 10, 25, 50, 100, 200, -1], [5, 10, 25, 50, 100, 200, "All"]],
    ajax: {
      method: 'POST',
      url : '{{ route('olt.customer-registration.data') }}',
      headers: {
        'X-CSRF-TOKEN': '{{ csrf_token() }}'
      }
    },
    columns: [
      {title: "No", data: 'DT_RowIndex', defaultContent: '-', class: 'text-center dt-body-nowrap', orderable: false, searchable: false, autoHide: false},
      {title:'Code',  data: 'Code', name: 'Code', defaultContent: '-', class: 'text-center dt-body-nowrap', },
			{title:'Company Code',  data: 'CompanyCode', name: 'CompanyCode', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'Company Name', data: 'CompanyName', name: 'CompanyName', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'CompanyInisial', data: 'CompanyInisial', name: 'CompanyInisial', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'CompanyGroup', data: 'CompanyGroup', name: 'CompanyGroup', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'LineofBusiness', data: 'LineofBusiness', name: 'LineofBusiness', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'CustomerCode', data: 'CustomerCode', name: 'CustomerCode', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      // {title:'Phone', data: 'Phone2', name: 'Phone2', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'CustomerCompanyCode', data: 'CustomerCompanyCode', name: 'CustomerCompanyCode', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'CustomerCompanyName', data: 'CustomerCompanyName', name: 'CustomerCompanyName', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'CustomerProvinceBirth', data: 'CustomerProvinceBirth', name: 'CustomerProvinceBirth', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'CustomerCityBirth', data: 'CustomerCityBirth', name: 'CustomerCityBirth', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'CustomerBirthDate', data: 'CustomerBirthDate', name: 'CustomerBirthDate', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'CustomerJobTitle', data: 'CustomerJobTitle', name: 'CustomerJobTitle', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'CustomerPhone', data: 'CustomerPhone', name: 'CustomerPhone', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'CustomerFax', data: 'CustomerFax', name: 'CustomerFax', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'CustomerEmail', data: 'CustomerEmail', name: 'CustomerEmail', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'CustomerPhoneNumber', data: 'CustomerPhoneNumber', name: 'CustomerPhoneNumber', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'CustomerNPWP', data: 'CustomerNPWP', name: 'CustomerNPWP', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'CustomerAddress', data: 'CustomerAddress', name: 'CustomerAddress', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'CustomerProvinceCode', data: 'CustomerProvinceCode', name: 'CustomerProvinceCode', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'CustomerProvinceName', data: 'CustomerProvinceName', name: 'CustomerProvinceName', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'CustomerCityCode', data: 'CustomerCityCode', name: 'CustomerCityCode', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'CustomerCityName', data: 'CustomerCityName', name: 'CustomerCityName', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'CustomerZipCode', data: 'CustomerZipCode', name: 'CustomerZipCode', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'CustomerWebsite', data: 'CustomerWebsite', name: 'CustomerWebsite', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'CustomerIDCard', data: 'CustomerIDCard', name: 'CustomerIDCard', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'CustomerIDCardNumber', data: 'CustomerIDCardNumber', name: 'CustomerIDCardNumber', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'CustomerIDCardExpired', data: 'CustomerIDCardExpired', name: 'CustomerIDCardExpired', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'CustomerProductSite', data: 'CustomerProductSite', name: 'CustomerProductSite', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'CustomerRegistrationDate', data: 'CustomerRegistrationDate', name: 'CustomerRegistrationDate', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'CustomerProductCode', data: 'CustomerProductCode', name: 'CustomerProductCode', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'CustomerStatusClient', data: 'CustomerStatusClient', name: 'CustomerStatusClient', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'CustomerSalesCode', data: 'CustomerSalesCode', name: 'CustomerSalesCode', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'CustomerSalesName', data: 'CustomerSalesName', name: 'CustomerSalesName', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'TechnicalPicCode', data: 'TechnicalPicCode', name: 'TechnicalPicCode', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'TechnicalPicCompanyCode', data: 'TechnicalPicCompanyCode', name: 'TechnicalPicCompanyCode', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'TechnicalPicName', data: 'TechnicalPicName', name: 'TechnicalPicName', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'TechnicalPicCompanyName', data: 'TechnicalPicCompanyName', name: 'TechnicalPicCompanyName', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'TechnicalPicName', data: 'TechnicalPicName', name: 'TechnicalPicName', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'TechnicalPicDepartment', data: 'TechnicalPicDepartment', name: 'TechnicalPicDepartment', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'TechnicalPicJobTitle', data: 'TechnicalPicJobTitle', name: 'TechnicalPicJobTitle', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'TechnicalPicPhoneNumber', data: 'TechnicalPicPhoneNumber', name: 'TechnicalPicPhoneNumber', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'TechnicalPicFax', data: 'TechnicalPicFax', name: 'TechnicalPicFax', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'TechnicalPicEmail', data: 'TechnicalPicEmail', name: 'TechnicalPicEmail', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'TechnicalPicSalesInfo', data: 'TechnicalPicSalesInfo', name: 'TechnicalPicSalesInfo', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'TechnicalPicNIP', data: 'TechnicalPicNIP', name: 'TechnicalPicNIP', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title: "Status", data: 'active', defaultContent: '-', class: 'text-center dt-body-nowrap'},
    ],
    order: [[1, 'asc']],
    bStateSave: true,
    columnDefs: [
      {
        targets: [0,-1],
        className: 'text-center visible dt-body-nowrap'
      },
    ],
    buttons: [
      {
        extend: 'print',
        text: 'Print current page',
        autoPrint: true,
        customize: function (win) {
          $(win.document.body)
          .css('font-size', '10pt')
          .prepend(
            '<img src="http://datatables.net/media/images/logo-fade.png" style="position:absolute; top:0; left:0;" />'
          );
          $(win.document.body).find('table')
          .addClass('compact')
          .css('font-size', 'inherit');
        },
        exportOptions: {
          columns: [0, 1, 'visible'],
          modifier: {
            page: 'current'
          },
        }
      }, {
        extend: 'copy',
        className: 'btn default',
        exportOptions: {
          columns: [0, 1, 'visible']
        }
      }, {
        extend: 'pdf',
        className: 'btn default',
        exportOptions: {
          columns: [0, 1, 'visible']
        }
      }, {
        extend: 'excelHtml5',
        className: 'btn default',
        excelStyles: {
          template: 'blue_medium',
        },
        exportOptions: {
          columns: [0, 1, 'visible']
        }
      }, {
        extend: 'csvHtml5',
        className: 'btn default',
        exportOptions: {
          columns: [0, 1, 'visible']
        }
      }, {
        text: 'Reload',
        className: 'btn default',
        action: function (e, dt, node, config) {
          dt.ajax.reload();
          alert_show('Datatable reloaded!', false);
        }
      },
      'colvis'
    ],
    initComplete: function() {
      $('.tl-tip').tooltip();
      @if (count($errors) > 0)
      // jQuery("html, body").animate({
      //   scrollTop: $('#add-form').offset().top - 100 // 66 for sticky bar height
      // }, "slow");
      @endif
    }
  });

  function show_data(id = "") {
      if (id !== "") {
          $.ajax({
              url: "{{ route('olt.customer-registration.store')}}/" + id,
              type: "GET",
              headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
              },
              success: function (response) {
                  $("#form-input").attr("action", "{{ route('olt.customer-registration.update','')}}/"+id);
                  $('#form-input').trigger("reset");
                  $('#method').val("PUT");
                  $('#Code').attr("readonly", true);
                  $('#Code').val(response.data.Code);
                  $('#Name').val(response.data.Name);
                  $('#ContactPerson').val(response.data.ContactPerson);
                  $('#Address').val(response.data.Address);
                  $('#CityCode').val(response.data.CityCode);
                  $('#Phone1').val(response.data.Phone1);
                  $('#Phone2').val(response.data.Phone2);
                  $('#Fax').val(response.data.Fax);
                  $('#Email').val(response.data.Email);
                  if (response.data.ActiveStatus === 1) {
                      $('#ActiveStatus').bootstrapSwitch('state', true);
                  } else {
                      $('#ActiveStatus').bootstrapSwitch('state', false);
                  }
                  $('#modal-form').modal('show');
                  $('#Code').focus();
              },
              error: function (xhr, status, error) {
                  alert_show(xhr.status + " " + status + " " + error, false);
              }
          });
      } else {
          $("#form-input").attr("action", "{{ route('olt.customer-registration.store')}}");
          $('#method').val("POST");
          $('#Code').attr("readonly", false);
          $('#Code').focus();
          $('#form-input').trigger("reset");
          $('#modal-form').modal('show');
          $('#Code').focus();
      }
  }

  function refresh_table() {
      $('#datatable').dataTable().ajax.reload();
  }

  function format_date(val) {
      var date_arr = val.split(" ");
      var time = date_arr[1];
      var date_id_arr = date_arr[0].split("-");
      var date_fix = date_id_arr[2] + "-" + date_id_arr[1] + "-" + date_id_arr[0];
      var date_transaction = date_fix + " " + time;
      return date_transaction;
  }
</script>
@endsection
