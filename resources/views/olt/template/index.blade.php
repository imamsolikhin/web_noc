@extends('layout.default') @section('content')
<div class="subheader py-2 {{ Metronic::printClasses('subheader', false) }}" id="kt_subheader">
  <div class="{{ Metronic::printClasses('subheader-container', false) }} d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
    <div class="d-flex align-items-baseline flex-wrap mr-5">
      <h5 class="text-dark font-weight-bold my-1 mr-5">
        Template
      </h5>
      <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
        <li class="breadcrumb-item">
          <a href="{{ route('dashboard') }}" class="text-muted">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
          <a href="{{ route('olt.template.index') }}" class="text-muted">Olt</a>
        </li>
        <li class="breadcrumb-item">
          <a href="#view" class="text-muted">Template </a>
        </li>
      </ul>
    </div>
  </div>
</div>

@include('inc.error-list')
@include('inc.success-notif')
    <div class="card card-custom">
      <div class="card-header bg-danger flex-wrap border-1 pt-1 pb-0 mb-2" style="min-height: 0;">
        <div class="card-title pt-1 pb-1">
          <h3 class="card-label font-weight-bolder text-white">Template
            <div class="text-muted pt-2 font-size-lg">show Datatable from table Template </div>
          </h3>
        </div>
        <div class="card-toolbar pt-1 pb-0">
          <a  href="{{ route('olt.template.page','new') }}" onclick="show_datas('')" class="btn btn-primary font-weight-bolder" style="background-color: #1e1e2d;border-color: #0c8eff;">
            <span class="svg-icon svg-icon-md">
              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                  <rect x="0" y="0" width="24" height="24"></rect>
                  <circle fill="#000000" cx="9" cy="15" r="6"></circle>
                  <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3"></path>
                </g>
              </svg>
            </span>Add New
          </a>
        </div>
      </div>
	<div class="card-body pt-1">
		<table class="table table-bordered table-hover w100" cellspacing="0"
			id="datatable" style="width: 1070px !important;"></table>
	</div>
  <div class="card-body py-1">
		<div class="col-lg-12">
			<div class="row justify-content-center py-2 px-2 py-md-10 px-md-0">
				<div class="table-responsive">
						<h3 class="pt-0"><div id="color-picker-1" class="mx-auto">Template Detail</div></h3>
							<table id="datatable_template_detail" class="table dataTable table-bordered table-hover w100" cellspacing="0">
									<thead>
										<tr>
												<th class="text-center text-white dt-body-nowrap">GemPort</th>
												<th class="text-center text-white dt-body-nowrap">Vlan</th>
												<th class="text-center text-white dt-body-nowrap">UserVlan</th>
												<th class="text-center text-white dt-body-nowrap">InnerVlan</th>
												<th class="text-center text-white dt-body-nowrap">TagTransform</th>
												<th class="text-center text-white dt-body-nowrap">TrafficTable</th>
												<th class="text-center text-white dt-body-nowrap">Remark</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
							</table>
					</div>
			</div>
		</div>
	</div>
</div>

@endsection {{-- Styles Section --}} @section('styles')
<link rel="stylesheet"
	href="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.css">
<link rel="stylesheet"
	href="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.min.css">
<link rel="stylesheet"
	href="{{ config('app.url') }}plugins/custom/datatables/datatables.bundle.css">
<link rel="stylesheet" href="{{ config('app.url') }}css/inject.css">
@endsection {{-- Scripts Section --}} @section('scripts') @include('inc.confirm-delete-modal')
<script
	src="{{ config('app.url') }}global/vendor/datatables/jquery.dataTables.js"></script>
<script
	src="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.js"></script>
<script
	src="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.js"></script>
<script type="text/javascript">
  $("html").keydown(function(e){
    if(e.shiftKey && e.keyCode == 'N'.charCodeAt(0) && !e.altKey){
         show_data('');
     }
  });
  $(document).ready(function() {
    $("#datatable_wrapper").removeClass("dataTables_wrapper form-inline dt-bootstrap no-footer");
    $("#datatable_wrapper").addClass("dataTables_wrapper dt-bootstrap4 no-footer");
  });

  $('[data-switch=true]').bootstrapSwitch();
  $("#TemplateModel").select2();
  $("#TemplateVersion").select2();
  var table = $('#datatable').dataTable({
    pageLength: 5,
    responsive: true,
    searchDelay: 800,
		pagingTypeSince : 'numbers',
    pagingType      : 'full_numbers',
		deferRender: true,
		render: true,
		stateSave: true,
		serverSide: true,
    processing: true,
    searching: true,
		select: true,
		rowId: 'id',
    lengthMenu: [[5, 10, 25, 50, 100, 200], [5, 10, 25, 50, 100, 200]],
    ajax: {
      method: 'POST',
      url : '{{ route('olt.template.data') }}',
      headers: {
        'X-CSRF-TOKEN': '{{ csrf_token() }}'
      }
    },
    columns: [
      {title: "No", data: 'DT_RowIndex', defaultContent: '-', class: 'text-center dt-body-nowrap', orderable: false, searchable: false, autoHide: false},
			{title: "Code", data: 'Code', defaultContent: '-', class: 'text-center dt-body-nowrap', autoHide: false},
			{title: "Name", data: 'Name', defaultContent: '-', class: 'text-center dt-body-nowrap', autoHide: false},
			{title: "VlanDownLink", data: 'VlanDownLink', defaultContent: '-', class: 'text-center dt-body-nowrap', autoHide: false},
			{title: "OntLineProfileId", data: 'OntLineProfileId', defaultContent: '-', class: 'text-center dt-body-nowrap', autoHide: false},
			{title: "OntSrvProfileId", data: 'OntSrvProfileId', defaultContent: '-', class: 'text-center dt-body-nowrap', autoHide: false},
			{title: "NativeVlanEth1", data: 'NativeVlanEth1', defaultContent: '-', class: 'text-center dt-body-nowrap', autoHide: false},
			{title: "NativeVlanEth2", data: 'NativeVlanEth2', defaultContent: '-', class: 'text-center dt-body-nowrap', autoHide: false},
			{title: "NativeVlanEth3", data: 'NativeVlanEth3', defaultContent: '-', class: 'text-center dt-body-nowrap', autoHide: false},
			{title: "NativeVlanEth4", data: 'NativeVlanEth4', defaultContent: '-', class: 'text-center dt-body-nowrap', autoHide: false},
      {title: "VlanAttribut", data: 'VlanAttribut', defaultContent: '-', class: 'text-center dt-body-nowrap', autoHide: false},
      {title: "Remark", data: 'Remark', defaultContent: '-', class: 'text-center dt-body-nowrap', autoHide: false},
      {title: "Actions", data: 'action', orderable: false, responsivePriority: -1},
    ],
    order: [[1, 'asc']],
    bStateSave: true,
    columnDefs: [
      {
        render: function ( data, type, row ) {
						var url = "{{route('olt.template.page', 'prm_code')}}";
						var url_destroy = "{{route('olt.template.destroy', 'prm_code')}}";
						var code = "'"+row.Code+"'";
						var status = "'"+row.ClientStatus+"'";
						var html = ""
        				html+= '<a href="'+url.replaceAll('prm_code',row.Code)+'" class="btn btn-icon btn-light btn-hover-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Edit"><span class="svg-icon svg-icon-md svg-icon-primary"><i class="far flaticon-edit icon-lg"></i></span></a>';
        				html+= '<a data-href="'+url_destroy.replaceAll('prm_code',row.Code)+'" class="btn btn-icon btn-light btn-hover-danger btn-sm" "data-toggle="tooltip" data-placement="top" title="Delete" data-toggle="modal" data-target="#confirm-delete-modal" ><span class="svg-icon svg-icon-md svg-icon-danger"><i class="far flaticon-delete icon-lg"></i></span></a>';

						return html;
        },
        targets: [-1],
        className: 'text-center dt-body-nowrap'
      },
    ],
    initComplete: function() {
      $('.tl-tip').tooltip();
      @if (count($errors) > 0)
      // jQuery("html, body").animate({
      //   scrollTop: $('#add-form').offset().top - 100 // 66 for sticky bar height
      // }, "slow");
      @endif
    }
  });

	$('#datatable tbody').on( 'click', 'tr', function () {
    $("#datatable_template_detail tbody>tr").remove();
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
            $('.Code').html("Data not selected");
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            var id = $(this).find("td:eq(1)").html();
            $("#data-olt").loading("start");
            $.ajax({
                url: "{{ route('olt.template.show-header','')}}/" + id,
                type: "GET",
                headers: {
                  'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                success: function (response) {
                  if(response.data["template_detail"]){
                      $("#datatable_template_detail tbody>tr").remove();
                      for (var i in response.data["template_detail"]){
                          var table = document.getElementById("datatable_template_detail").getElementsByTagName('tbody')[0];
                          var row = table.insertRow(-1);
                          row.insertCell(0).innerHTML = '<label class="col-12 text-center">'+response.data["template_detail"][i].GemPort+'</label>';
                          row.insertCell(1).innerHTML = '<label class="col-12 text-center">'+response.data["template_detail"][i].Vlan+'</label>';
                          row.insertCell(2).innerHTML = '<label class="col-12 text-center">'+response.data["template_detail"][i].UserVlan+'</label>';
                          row.insertCell(3).innerHTML = '<label class="col-12 text-center">'+response.data["template_detail"][i].InnerVlan+'</label>';
                          row.insertCell(4).innerHTML = '<label class="col-12 text-center">'+response.data["template_detail"][i].TagTransform+'</label>';
													row.insertCell(5).innerHTML = '<label class="col-12 text-center">'+response.data["template_detail"][i].TrafficTable+'</label>';
                          row.insertCell(6).innerHTML = '<label class="col-12 text-center">'+response.data["template_detail"][i].Remark+'</label>';
                      }
                    }
										$("#data-olt").loading("stop");
                },
                error: function (xhr, status, error) {
                  $("#data-olt").loading("stop");
                  // showDialog.show(xhr.status + " " + status + " " + error, false);
                }
            });
        }
    });


  function refresh_table() {
      $('#datatable').dataTable().ajax.reload();
  }

  function format_date(val) {
      var date_arr = val.split(" ");
      var time = date_arr[1];
      var date_id_arr = date_arr[0].split("-");
      var date_fix = date_id_arr[2] + "-" + date_id_arr[1] + "-" + date_id_arr[0];
      var date_transaction = date_fix + " " + time;
      return date_transaction;
  }
</script>
@endsection
