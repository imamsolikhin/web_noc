@extends('layout.default')

@section('content')
<div class="subheader py-2 {{ Metronic::printClasses('subheader', false) }}" id="kt_subheader">
  <div class="{{ Metronic::printClasses('subheader-container', false) }} d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
    <div class="d-flex align-items-baseline flex-wrap mr-5">
      <h5 class="text-dark font-weight-bold my-1 mr-5">
        template
      </h5>
      <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
        <li class="breadcrumb-item">
          <a href="{{ route('dashboard') }}" class="text-muted">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
          <a href="{{ route('olt.template.index') }}" class="text-muted">Olt</a>
        </li>
        <li class="breadcrumb-item">
            <a href="#form" class="text-muted">Form Input</a>
        </li>
      </ul>
    </div>
  </div>
</div>

@include('inc.error-list')
@include('inc.success-notif')
<div class="card card-custom">
  <div class="card-header bg-danger flex-wrap border-1 pt-1 pb-0" style="min-height: 0;">
    <div class="card-title pt-1 pb-0">
      <h3 class="card-label font-weight-bolder text-white">Form Input
      </h3>
    </div>
  </div>
  <div class="card-body pt-1">
    <form class="form pt-0" id="form" action="@if (isset($template->Code)) {{ route('olt.template.update',$template->Code) }} @else {{ route('olt.template.store') }} @endif" method="POST">
        {!! csrf_field() !!}
        @if (isset($template->Code)) {!! method_field('PUT') !!}  @else {!! method_field('POST') !!} @endif
        <div class="card-body pt-0 pl-3 pr-3">
            <div class="row">
              <div class="mb-1" hidden>
                  <div class="form-group row mb-0 pt-2">
                      <label class="col-lg-4 col-form-label">Code</label>
                      <div class="col-lg-5">
                          <input type="text" class="form-control form-control-sm" id="Code" name="Code" placeholder="Enter Code" value="@isset ($template->Code) {{ $template->Code }} @endisset" />
                      </div>
                  </div>
              </div>
                <div class="col-lg-6 pt-3">
                  <div class="mb-1">
                      <div class="form-group row mb-0">
                          <label class="col-lg-4 font-weight-bolder">Template Name <span class="text-danger">*</span></label>
                          <div class="col-lg-5">
                               <input class="form-control form-control-sm" id="Name" name="Name" placeholder="Enter Name" value="@isset ($template->Name) {{ $template->Name }} @endisset" />
                          </div>
                      </div>
                  </div>
                  <div class="mb-1">
                      <div class="form-group row mb-0">
                          <label class="col-lg-4 font-weight-bolder">Line ProfileId <span class="text-danger">*</span></label>
                          <div class="col-lg-5">
                              <input type="text" class="form-control form-control-sm" id="OntLineProfileId" name="OntLineProfileId" placeholder="Enter OntLineProfileId" value="@isset ($template->OntLineProfileId) {{ $template->OntLineProfileId }} @endisset" />
                          </div>
                      </div>
                  </div>
                  <div class="mb-1">
                    <div class="form-group row mb-0">
                        <label class="col-lg-4 font-weight-bolder">Srv ProfileId <span class="text-danger">*</span></label>
                        <div class="col-lg-5">
                            <input type="text" class="form-control form-control-sm" id="OntSrvProfileId" name="OntSrvProfileId" placeholder="Enter OntSrvProfileId" value="@isset ($template->OntSrvProfileId) {{ $template->OntSrvProfileId }} @endisset" />
                        </div>
                    </div>
                  </div>
                  <div class="mb-1">
                      <div class="form-group row mb-0">
                          <label class="col-lg-4 font-weight-bolder">Vlan Attribut <span class="text-danger">*</span></label>
                          <div class=" col-lg-8 col-md-8 col-sm-8">
                              <div class="radio-inline">
                                @if (isset($template->VlanAttribut))
                                  @if ($template->VlanAttribut === "Stacking")
                                    <label class="radio"><input type="radio" name="VlanAttribut" value="Standard" /><span></span>Stand</label>
                                    <label class="radio"><input type="radio" name="VlanAttribut" value="Stacking" checked="checked" /><span></span>Stack</label>
                                    <label class="radio"><input type="radio" name="VlanAttribut" value="QinQ"/><span></span>QinQ</label>
                                  @elseif ($template->VlanAttribut === "QinQ")
                                    <label class="radio"><input type="radio" name="VlanAttribut" value="Standard" /><span></span>Stand</label>
                                    <label class="radio"><input type="radio" name="VlanAttribut" value="Stacking"/><span></span>Stack</label>
                                    <label class="radio"><input type="radio" name="VlanAttribut" value="QinQ" checked="checked" /><span></span>QinQ</label>
                                  @else
                                      <label class="radio"><input type="radio" name="VlanAttribut" value="Standard" checked="checked" /><span></span>Stand</label>
                                      <label class="radio"><input type="radio" name="VlanAttribut" value="Stacking"/><span></span>Stack</label>
                                      <label class="radio"><input type="radio" name="VlanAttribut" value="QinQ"/><span></span>QinQ</label>
                                  @endif
                                @else
                                    <label class="radio"><input type="radio" name="VlanAttribut" value="Standard" checked="checked" /><span></span>Stand</label>
                                    <label class="radio"><input type="radio" name="VlanAttribut" value="Stacking"/><span></span>Stack</label>
                                    <label class="radio"><input type="radio" name="VlanAttribut" value="QinQ"/><span></span>QinQ</label>
                                @endif
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="mb-3">
                      <div class="form-group row mb-0">
                          <label class="col-lg-4 col-form-label">Note</label>
                          <div class="col-lg-5">
                              <textarea type="text" class="form-control form-control-sm" id="Remark" name="Remark" placeholder="Enter Remark" value="@isset ($template->Remark) {{ $template->Remark }} @endisset"/>@isset ($template->Remark) {{ $template->Remark }} @endisset </textarea>
                          </div>
                      </div>
                  </div>
                  <div class="mb-2">
                    <div class="form-group row mb-0">
                        <label class="col-lg-4 col-form-label"></label>
                        <div class="col-lg-8">
                            <button type="submit" class="btn btn-success font-weight-bold">Save</button>
                            <a type="button" class="btn btn-danger" href="{{ route('olt.template.index') }}">Cancel</a>
                        </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6 pt-3">
                  <div class="mb-1">
                      <div class="form-group row mb-0">
                          <label class="col-lg-4 col-form-label">Vlan DownLink</label>
                          <div class="col-lg-5">
                              <input type="text" class="form-control form-control-sm" id="VlanDownLink" name="VlanDownLink" placeholder="Enter VlanDownLink" value="@isset ($template->VlanDownLink) {{ $template->VlanDownLink }} @endisset" />
                          </div>
                      </div>
                  </div>
                  <div class="mb-1">
                      <div class="form-group row mb-0">
                          <label class="col-lg-4 col-form-label">Native Vlan Eth 1</label>
                          <div class="col-lg-5">
                              <input type="text" class="form-control form-control-sm" id="NativeVlanEth1" name="NativeVlanEth1" placeholder="Enter NativeVlanEth1" value="@isset ($template->NativeVlanEth1) {{ $template->NativeVlanEth1 }} @endisset"/>
                          </div>
                      </div>
                  </div>
                  <div class="mb-1">
                      <div class="form-group row mb-0">
                          <label class="col-lg-4 col-form-label">Native Vlan Eth 2</label>
                          <div class="col-lg-5">
                              <input type="text" class="form-control form-control-sm" id="NativeVlanEth2" name="NativeVlanEth2" placeholder="Enter NativeVlanEth2" value="@isset ($template->NativeVlanEth2) {{ $template->NativeVlanEth2 }} @endisset"/>
                          </div>
                      </div>
                  </div>
                  <div class="mb-1">
                      <div class="form-group row mb-0">
                          <label class="col-lg-4 col-form-label">Native Vlan Eth 3</label>
                          <div class="col-lg-5">
                              <input type="text" class="form-control form-control-sm" id="NativeVlanEth3" name="NativeVlanEth3" placeholder="Enter NativeVlanEth3" value="@isset ($template->NativeVlanEth3) {{ $template->NativeVlanEth3 }} @endisset"/>
                          </div>
                      </div>
                  </div>
                  <div class="mb-1">
                      <div class="form-group row mb-0">
                          <label class="col-lg-4 col-form-label">Native Vlan Eth 4</label>
                          <div class="col-lg-5">
                              <input type="text" class="form-control form-control-sm" id="NativeVlanEth4" name="NativeVlanEth4" placeholder="Enter NativeVlanEth4" value="@isset ($template->NativeVlanEth4) {{ $template->NativeVlanEth4 }} @endisset"/>
                          </div>
                      </div>
                  </div>
                </div>
                <div class="col-lg-12">
                    <div class="card-custom mb-1">
                        <div class="card-header border-0 pt-1 pb-1 pr-0 pl-0">
                          <div class="card-toolbar float-left">
                              <h3 class="card-title font-weight-bolder text-dark">Template Detail</h3>
                          </div>
                          <div class="card-toolbar float-right">
                              <a type="button" onclick="addService()" class="btn btn-warning btn-sm"><i class="fas fa-plus"></i> Add</a>
                          </div>
                        </div>
                        <div class="card-body pr-0 pl-0 pt-1 pb-1 pr-0 pl-0">
                          <table id="datatable" class="table dataTable table-bordered table-hover w100" cellspacing="0">
                              <thead>
                                  <tr>
                                      <th class="text-center text-white dt-body-nowrap">GemPort</th>
                                      <th class="text-center text-white dt-body-nowrap">Vlan</th>
                                      <th class="text-center text-white dt-body-nowrap">UserVlan</th>
                                      <th class="text-center text-white dt-body-nowrap">InnerVlan</th>
                                      <th class="text-center text-white dt-body-nowrap" style="width:15rem;">TagTransform</th>
                                      <th class="text-center text-white dt-body-nowrap">TrafficTable</th>
                                      <th class="text-center text-white dt-body-nowrap">Remark</th>
                                      <th class="text-center text-white dt-body-nowrap">Act</th>
                                  </tr>
                              </thead>
                              <tbody>
                               @isset ($template_detail)
                                 @foreach($template_detail as $template)
                                   <tr>
                                      <td><input name="its_gemport[]" class="form-input col-12" value="{{ $template->GemPort }}"></td>
                                      <td><input name="its_vlan[]" class="form-input col-12" value="{{ $template->Vlan }}"></td>
                                      <td><input name="its_uservlan[]" class="form-input col-12" value="{{ $template->UserVlan }}"></td>
                                      <td><input name="its_innervlan[]" class="form-input col-12" value="{{ $template->InnerVlan }}"></td>
                                      <td>
                                        <select name="its_tagtransform[]" class="col-12" value="{{ $template->TagTransform }}">
                                          @if ($template->TagTransform === "translate-and-add")
                                            <option value="default">default</option>
                                            <option value="translate">translate</option>
                                            <option value="translate-and-add" selected>translate-and-add</option>
                                          @elseif ($template->TagTransform === "translate")
                                            <option value="default">default</option>
                                            <option value="translate" selected>translate</option>
                                            <option value="translate-and-add">translate-and-add</option>
                                          @else
                                            <option value="default" selected>default</option>
                                            <option value="translate">translate</option>
                                            <option value="translate-and-add" >translate-and-add</option>
                                          @endif
                                        </select>
                                      </td>
                                      <td><input name="its_traffictable[]" class="form-input col-12" value="{{ $template->TrafficTable }}"></td>
                                      <td><input name="its_remark[]" class="form-input col-12" value="{{ $template->Remark }}"></td>
                                      <td><a type="button" value="del" onclick="deleteRow(this)" class="pt-2"><i class="text-center text-danger fas fa-backspace"></i><a/></td>
                                  </tr>
                                 @endforeach
                               @endisset
                              </tbody>
                          </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </form>
    </div>
    @endsection

{{-- Styles Section --}}
@section('styles')
<link rel="stylesheet" href="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.css">
<link rel="stylesheet" href="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.min.css">
<link rel="stylesheet" href="{{ config('app.url') }}plugins/custom/datatables/datatables.bundle.css">
<link rel="stylesheet" href="{{ config('app.url') }}css/inject.css">
@endsection

{{-- Scripts Section --}}
@section('scripts')
@include ('inc.confirm-delete-modal')
<script src="{{ config('app.url') }}global/vendor/datatables/jquery.dataTables.js"></script>
<script src="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.js"></script>
<script src="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.js"></script>
<script type="text/javascript">
 $(document).ready(function() {
     $('[data-switch=true]').bootstrapSwitch();
     if($("#Code").val() != ""){
       $("#Code").attr('readonly',true);
     }

 });

 function addService() {
   var option = "";
   option += ' <select name="its_tagtransform[]" class="col-12" >';
   option += '  <option value=""></option>';
   option += '  <option value="translate">translate</option>';
   option += '  <option value="translate-and-add">translate-and-add</option>';
   option += '  <option value="default" selected>default</option>';
   option += ' </select>';
   var table = document.getElementById("datatable").getElementsByTagName('tbody')[0];
   var row = table.insertRow(-1);
   var cell1 = row.insertCell(0).innerHTML = '<input name="its_gemport[]" class="form-input col-12" value="">';
   var cell1 = row.insertCell(1).innerHTML = '<input name="its_vlan[]" class="form-input col-12" value="">';
   var cell2 = row.insertCell(2).innerHTML = '<input name="its_uservlan[]" class="form-input col-12" value="">';
   var cell3 = row.insertCell(3).innerHTML = '<input name="its_innervlan[]" class="form-input col-12" value="">';
   var cell4 = row.insertCell(4).innerHTML = option;
   var cell5 = row.insertCell(5).innerHTML = '<input name="its_traffictable[]" class="form-input col-12" value="">';
   var cell6 = row.insertCell(6).innerHTML = '<input name="its_remark[]" class="form-input col-12" value="">';
   var cell7 = row.insertCell(7).innerHTML = '<a type="button" value="del" onclick="deleteRow(this)" class="pt-2"><i class="text-center text-danger fas fa-backspace"></i><a/>';
 }
 function deleteRow(btn) {
   var row = btn.parentNode.parentNode;
   row.parentNode.removeChild(row);
 }



</script>
@endsection
