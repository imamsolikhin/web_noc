@extends('layout.default') @section('content')
<div
	class="subheader py-2 {{ Metronic::printClasses('subheader', false) }}"
	id="kt_subheader">
	<div
		class="{{ Metronic::printClasses('subheader-container', false) }} d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
		<div class="d-flex align-items-baseline flex-wrap mr-5">
			<h5 class="text-dark font-weight-bold my-1 mr-5">ISP</h5>
			<ul
				class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
				<li class="breadcrumb-item"><a href="{{ route('dashboard') }}"
					class="text-muted">Dashboard</a></li>
				<li class="breadcrumb-item"><a href="{{ route('olt.isp.index') }}"
					class="text-muted">Olt</a></li>
				<li class="breadcrumb-item"><a href="#view" class="text-muted">ISP</a>
				</li>
			</ul>
		</div>
	</div>
</div>

@include('inc.error-list') @include('inc.success-notif')
<div class="card card-custom">
	<div class="card-header bg-danger flex-wrap border-1 pt-1 pb-0 mb-2"
		style="min-height: 0;">
		<div class="card-title pt-1 pb-1">
			<h3 class="card-label font-weight-bolder text-white">
				ISP
				<div class="text-muted pt-2 font-size-lg">show Datatable from table
					ISP</div>
			</h3>
		</div>
		<div class="card-toolbar pt-1 pb-0">
			<a href="#" onclick="show_data('')" class="btn btn-primary font-weight-bolder"
				style="background-color: #1e1e2d; border-color: #0c8eff;"> <span
				class="svg-icon svg-icon-md"> <svg
						xmlns="http://www.w3.org/2000/svg"
						xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
						height="24px" viewBox="0 0 24 24" version="1.1">
            <g stroke="none" stroke-width="1" fill="none"
							fill-rule="evenodd">
              <rect x="0" y="0" width="24" height="24"></rect>
              <circle fill="#000000" cx="9" cy="15" r="6"></circle>
              <path
							d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z"
							fill="#000000" opacity="0.3"></path>
            </g>
          </svg>
			</span>Add New
			</a>
		</div>
	</div>
  <div class="card-body pt-1">
    <div hidden><input id="start-date"/><input id="end-date"/></div>
    <table class="table table-bordered table-hover w100" cellspacing="0" id="datatable" style="width: 1070px !important;"></table>
  </div>
	<div class="card-body pt-1">
		<table class="table table-bordered table-hover w100" cellspacing="0"
			id="datatable" style="width: 1070px !important;"></table>
	</div>
</div>


<!-- Modal-->
<div class="modal fade" id="modal-form" data-backdrop="static"
	tabindex="-1" role="dialog" aria-labelledby="staticBackdrop"
	aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-danger pt-3 pb-3">
				<h5 class="modal-title text-white bold" id="modal">New ISP</h5>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<i aria-hidden="true" class="ki ki-close"></i>
				</button>
			</div>
			<form class="form" id="form-input"
				action="{{ route('olt.isp.store') }}" method="POST">
				{!! csrf_field() !!} <input type="hidden"
					class="form-control form-control-sm" id="method" name="_method"
					placeholder="Enter method" value="POST" />
				<div class="card-body pt-3">
					<div class="mb-1">
						<div class="form-group row mb-0">
							<div class="col-lg-12">
								<label class="font-weight-bolder">ISP Code <span class="text-danger">*</span></label> <input type="text"
									class="form-control form-control-sm" id="Code" name="Code"
									value="" placeholder="Enter Code" required/>
							</div>
						</div>
					</div>
					<div class="mb-1">
						<div class="form-group row mb-0">
							<div class="col-lg-12">
								<label class="font-weight-bolder">ISP Name <span class="text-danger">*</span></label> <input type="text"
									class="form-control form-control-sm" id="Name" name="Name"
									value="" placeholder="Enter Name" required/>
							</div>
						</div>
					</div>
					<div class="mb-1">
						<div class="form-group row mb-0">
							<div class="col-lg-12">
								<label class="form-label">ContactPerson</label> <input type="text"
									class="form-control form-control-sm" id="ContactPerson"
									name="ContactPerson" value="" placeholder="Enter ContactPerson" />
							</div>
						</div>
					</div>
					<div class="mb-1">
						<div class="form-group row mb-0">
							<div class="col-lg-12">
								<label class="font-weight-bolder">Address <span class="text-danger">*</span></label>
								<textarea class="form-control form-control-sm" rows="5"
									id="Address" name="Address" required></textarea>
							</div>
						</div>
					</div>
					<div class="mb-1">
						<div class="form-group row mb-0">
							<div class="col-lg-12">
								<label class="font-weight-bolder">CityCode <span class="text-danger">*</span></label> <input type="text"
									class="form-control form-control-sm" id="CityCode"
									name="CityCode" value="" placeholder="Enter Name" required/>
							</div>
						</div>
					</div>
					<div class="mb-1">
						<div class="form-group row mb-0">
							<div class="col-lg-12">
								<label class="font-weight-bolder">Phone1 <span class="text-danger">*</span></label> <input type="text"
									class="form-control form-control-sm" id="Phone1" name="Phone1"
									value="" placeholder="Enter Phone1" required/>
							</div>
						</div>
					</div>
					<div class="mb-1">
						<div class="form-group row mb-0">
							<div class="col-lg-12">
								<label class="form-label">Phone2</label> <input type="text"
									class="form-control form-control-sm" id="Phone2" name="Phone2"
									value="" placeholder="Enter Phone2" />
							</div>
						</div>
					</div>
					<div class="mb-1">
						<div class="form-group row mb-0">
							<div class="col-lg-12">
								<label class="form-label">Fax</label> <input type="text"
									class="form-control form-control-sm" id="Fax" name="Fax"
									value="" placeholder="Enter Fax" />
							</div>
						</div>
					</div>
					<div class="mb-1">
						<div class="form-group row mb-0">
							<div class="col-lg-12">
								<label class="form-label">Email</label> <input type="email"
									class="form-control form-control-sm" id="Email" name="Email"
									value="" placeholder="Enter Email" />
							</div>
						</div>
					</div>
					<div class="mb-2">
						<div class="form-group row mb-0">
							<label class="col-lg-4 col-form-label">Status</label>
							<div class="col-lg-8">
								<input id="ActiveStatus" name="ActiveStatus" data-switch="true"
									type="checkbox" checked="checked" data-on-text="Enabled"
									data-handle-width="200" data-handle-font="1"
									data-off-text="Disabled" data-on-color="warning" />
							</div>
						</div>
					</div>
					<div class="mb-2">
						<div class="form-group row mb-0">
							<label class="col-lg-4 col-form-label"></label>
							<div class="col-lg-8">
								<button type="submit" class="btn btn-success font-weight-bold">Save</button>
								<button type="button" data-dismiss="modal"
									class="btn btn-danger">Cancel</button>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection {{-- Styles Section --}} @section('styles')
<link rel="stylesheet" href="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.css">
<link rel="stylesheet" href="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.min.css">
<link rel="stylesheet" href="{{ config('app.url') }}plugins/custom/datatables/datatables.bundle.css">
<link rel="stylesheet" href="{{ config('app.url') }}css/inject.css">
@endsection {{-- Scripts Section --}}
@section('scripts') @include ('inc.confirm-delete-modal')
<script src="{{ config('app.url') }}global/vendor/datatables/jquery.dataTables.js"></script>
<script src="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.js"></script>
<script src="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.js"></script>
<script type="text/javascript">

$(document).ready(function() {
  $('[data-switch=true]').bootstrapSwitch();
  $("#DeviceCode").select2();

	$("div.datesearchbox").html('<div class="input-group"> <div class="input-group-addon"> <i class="glyphicon glyphicon-calendar"></i> </div><input type="text" class="form-control pull-right" id="datesearch" placeholder="Search by date range"> </div>');
	document.getElementsByClassName("datesearchbox")[0].style.textAlign = "center";
	$("#datesearch").attr("readonly",true);
	$('#datesearch').daterangepicker({
		 autoUpdateInput: false
	 });

	//menangani proses saat apply date range
	 $('#datesearch').on('apply.daterangepicker', function(ev, picker) {
			$(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
			$("#start-date").val(picker.startDate.format('YYYY-MM-DD'));
			$("#end-date").val(picker.endDate.format('YYYY-MM-DD'));
			refresh_table();
	 });

	 $('#datesearch').on('cancel.daterangepicker', function(ev, picker) {
		 $(this).val('');
		 $("#start-date").val('');
		 $("#end-date").val('');
		 refresh_table();
	 });
 });

  $(document).ready(function() {
    $("#datatable_wrapper").removeClass("dataTables_wrapper form-inline dt-bootstrap no-footer");
    $("#datatable_wrapper").addClass("dataTables_wrapper dt-bootstrap4 no-footer");
	  $('[data-switch=true]').bootstrapSwitch();
  });
  var table = $('#datatable').dataTable({
    pageLength: 5,
    responsive: true,
    searchDelay: 800,
		pagingTypeSince : 'numbers',
    pagingType      : 'full_numbers',
		deferRender: true,
		render: true,
		stateSave: true,
		serverSide: true,
    processing: true,
    searching: true,
		select: true,
		rowId: 'id',
    lengthMenu: [[5, 10, 25, 50, 100, 200], [5, 10, 25, 50, 100, 200]],
    ajax: {
      method: 'POST',
      url : '{{ route('olt.isp.data') }}',
      headers: {
        'X-CSRF-TOKEN': '{{ csrf_token() }}'
      },
			data: function (d) {
				d.from_date = $("#start-date").val();
				d.to_date = $(" #end-date").val();
			}
    },
    columns: [
      {title: "No", data: 'DT_RowIndex', defaultContent: '-', class: 'text-center dt-body-nowrap', orderable: false, searchable: false, autoHide: false},
      {title:'Code',  data: 'Code',  defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'Company Name',  data: 'Name',  defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'ISP Contact Person',  data: 'ContactPerson', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'Address', data: 'Address', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'CityCode', data: 'CityCode', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'CustomerPhone1', data: 'Phone1',  defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'CustomerPhone2', data: 'Phone2', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'Fax', data: 'Fax', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'Email', data: 'Email', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title: "Status", data: 'active', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "Actions", data: 'action', orderable: false, responsivePriority: -1},
    ],
		dom:  "<'row'<'col-sm-5'l><'col-sm-3' <'datesearchbox'>><'col-sm-3'f>>" +
          "<'row'<'col-sm-12'tr>>" +
          "<'row'<'col-sm-5'i><'col-sm-7'p>>",

    order: [[1, 'asc']],
    bStateSave: true,
    columnDefs: [
      {
        render: function ( data, type, row ) {
						var url_destroy = "{{route('olt.isp.destroy', 'prm_code')}}";
						var url_update = "{{route('olt.isp.edit', 'prm_code')}}";
						var code = "'"+row.Code+"'";
						var status = "'"+row.ClientStatus+"'";
						var html = ""
        				html+= '<a onclick="show_data('+code+')" class="btn btn-icon btn-light btn-hover-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Update"><span class="svg-icon svg-icon-md svg-icon-primary"><i class="far flaticon-edit icon-lg"></i></span></a>';
        				html+= '<a data-href="'+url_destroy.replaceAll('prm_code',row.Code)+'" class="btn btn-icon btn-light btn-hover-danger btn-sm" "data-toggle="tooltip" data-placement="top" title="Delete" data-toggle="modal" data-target="#confirm-delete-modal" ><span class="svg-icon svg-icon-md svg-icon-danger"><i class="far flaticon-delete icon-lg"></i></span></a>';

						return html;
        },
        targets: [-1],
        className: 'text-center dt-body-nowrap'
      },
    ],

    initComplete: function() {
      $('.tl-tip').tooltip();
    }
  });

  function show_data(id = "") {
      if (id !== "") {
          $.ajax({
              url: "{{ route('olt.isp.store')}}/" + id,
              type: "GET",
              headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
              },
              success: function (response) {
                  $("#form-input").attr("action", "{{ route('olt.isp.update','')}}/"+id);
                  $('#form-input').trigger("reset");
                  $('#method').val("PUT");
                  $('#Code').attr("readonly", true);
                  $('#Code').val(response.data.Code);
                  $('#Name').val(response.data.Name);
                  $('#ContactPerson').val(response.data.ContactPerson);
                  $('#Address').val(response.data.Address);
                  $('#CityCode').val(response.data.CityCode);
                  $('#Phone1').val(response.data.Phone1);
                  $('#Phone2').val(response.data.Phone2);
                  $('#Fax').val(response.data.Fax);
                  $('#Email').val(response.data.Email);
									if(response.data.ActiveStatus == "1"){
	                  $('#ActiveStatus').bootstrapSwitch('state', true);
									}else{
										$('#ActiveStatus').bootstrapSwitch('state', false);
									}
                  $('#modal-form').modal('show');
              },
              error: function (xhr, status, error) {
                  alert_show(xhr.status + " " + status + " " + error, false);
              }
          });
      } else {
          $("#form-input").attr("action", "{{ route('olt.isp.store')}}");
          $('#method').val("POST");
          $('#Code').attr("readonly", false);
          $('#Code').focus();
          $('#form-input').trigger("reset");
          $('#modal-form').modal('show');
          $('#Code').focus();
      }
  }

  function refresh_table() {
      $('#datatable').dataTable().ajax.reload();
  }

  function format_date(val) {
      var date_arr = val.split(" ");
      var time = date_arr[1];
      var date_id_arr = date_arr[0].split("-");
      var date_fix = date_id_arr[2] + "-" + date_id_arr[1] + "-" + date_id_arr[0];
      var date_transaction = date_fix + " " + time;
      return date_transaction;
  }

	function refresh_table() {
			table.fnDraw();
		}

</script>
@endsection
