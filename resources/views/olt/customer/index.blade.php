@extends('layout.default')
 @section('content')
<div
	class="subheader py-2 {{ Metronic::printClasses('subheader', false) }}"
	id="kt_subheader">
	<div
		class="{{ Metronic::printClasses('subheader-container', false) }} d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
		<div class="d-flex align-items-baseline flex-wrap mr-5">
			<h5 class="text-dark font-weight-bold my-1 mr-5">Customer</h5>
			<ul
				class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
				<li class="breadcrumb-item"><a href="{{ route('dashboard') }}"
					class="text-muted">Dashboard</a></li>
				<li class="breadcrumb-item"><a
					href="{{ route('olt.customer.index') }}" class="text-muted">Olt</a>
				</li>
				<li class="breadcrumb-item"><a href="#view" class="text-muted">Customer</a>
				</li>
			</ul>
		</div>
	</div>
</div>

@include('inc.error-list') @include('inc.success-notif')
<div class="card card-custom">
	<div class="card-header bg-danger flex-wrap border-1 pt-1 pb-0 mb-2"
		style="min-height: 0;">
		<div class="card-title pt-1 pb-1">
			<h3 class="card-label font-weight-bolder text-white">
				Customer
				<div class="text-muted pt-2 font-size-lg">show Datatable from table
					Customer</div>
			</h3>
		</div>
		<div class="card-toolbar pt-1 pb-0">
			<a  href="{{ route('olt.customer.page','new') }}" onclick="show_datas('')" class="btn btn-primary font-weight-bolder" style="background-color: #1e1e2d;border-color: #0c8eff;">
				<span class="svg-icon svg-icon-md">
					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
						<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
							<rect x="0" y="0" width="24" height="24"></rect>
							<circle fill="#000000" cx="9" cy="15" r="6"></circle>
							<path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3"></path>
						</g>
					</svg>
				</span>Add New
			</a>
		</div>
	</div>
  <div class="card-body pt-1">
    <div hidden><input id="start-date"/><input id="end-date"/></div>
    <table class="table table-bordered table-hover w100" cellspacing="0" id="datatable" style="width: 1070px !important;"></table>
  </div>
	<div class="card-body pt-1">
		<table class="table table-bordered table-hover w100" cellspacing="0"
			id="datatable" style="width: 1070px !important;"></table>
	</div>
</div>



@endsection {{-- Styles Section --}}
@section('styles')
<link rel="stylesheet"
	href="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.css">
<link rel="stylesheet"
	href="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.min.css">
<link rel="stylesheet"
	href="{{ config('app.url') }}plugins/custom/datatables/datatables.bundle.css">
<link rel="stylesheet" href="{{ config('app.url') }}css/inject.css">
@endsection {{-- Scripts Section --}} @section('scripts') @include('inc.confirm-delete-modal')
<script
	src="{{ config('app.url') }}global/vendor/datatables/jquery.dataTables.js"></script>
<script
	src="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.js"></script>
<script
	src="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.js"></script>
<script type="text/javascript">
$(document).ready(function() {
  $('[data-switch=true]').bootstrapSwitch();
  $("#DeviceCode").select2();

	$("div.datesearchbox").html('<div class="input-group"> <div class="input-group-addon"> <i class="glyphicon glyphicon-calendar"></i> </div><input type="text" class="form-control pull-right" id="datesearch" placeholder="Search by date range"> </div>');
	document.getElementsByClassName("datesearchbox")[0].style.textAlign = "center";
	$("#datesearch").attr("readonly",true);
	$('#datesearch').daterangepicker({
		 autoUpdateInput: false
	 });

	//menangani proses saat apply date range
	 $('#datesearch').on('apply.daterangepicker', function(ev, picker) {
			$(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
			$("#start-date").val(picker.startDate.format('YYYY-MM-DD'));
			$("#end-date").val(picker.endDate.format('YYYY-MM-DD'));
			refresh_table();
	 });

	 $('#datesearch').on('cancel.daterangepicker', function(ev, picker) {
		 $(this).val('');
		 $("#start-date").val('');
		 $("#end-date").val('');
		 refresh_table();
	 });
 });

  $("html").keydown(function(e){
    if(e.shiftKey && e.keyCode == 'N'.charCodeAt(0) && !e.altKey){
         show_data('');
     }
  });
  $(document).ready(function() {
    $("#datatable_wrapper").removeClass("dataTables_wrapper form-inline dt-bootstrap no-footer");
    $("#datatable_wrapper").addClass("dataTables_wrapper dt-bootstrap4 no-footer");
  });

  $('[data-switch=true]').bootstrapSwitch();
  $("#DeviceModel").select2();
  $("#DeviceVersion").select2();
  var table = $('#datatable').dataTable({
    pageLength: 5,
    responsive: true,
    searchDelay: 800,
		pagingTypeSince : 'numbers',
    pagingType      : 'full_numbers',
		deferRender: true,
		render: true,
		stateSave: true,
		serverSide: true,
    processing: true,
    searching: true,
		select: true,
		rowId: 'id',
    lengthMenu: [[5, 10, 25, 50, 100, 200], [5, 10, 25, 50, 100, 200]],
    ajax: {
      method: 'POST',
      url : '{{ route('olt.customer.data') }}',
      headers: {
        'X-CSRF-TOKEN': '{{ csrf_token() }}'
      },
      data: function (d) {
				d.from_date = $("#start-date").val();
				d.to_date = $(" #end-date").val();
			}
    },
    columns: [
			{title: "No", data: 'DT_RowIndex', defaultContent: '-', class: 'text-center dt-body-nowrap', orderable: false, searchable: false, autoHide: false},
      // {title: "CustomerID", data: 'Code', defaultContent: '-', class: 'text-center dt-body-nowrap'},
			{title: "ISP ID", data: 'IspCode', defaultContent: '-', class: 'text-center dt-body-nowrap'},
			{title: "ISP Name", data: 'IspName', defaultContent: '-', class: 'text-center dt-body-nowrap'},
			{title: "PelangganID", data: 'MemberID', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "CompanyName", data: 'Name', defaultContent: '-', class: 'text-center dt-body-nowrap'},
			{title: "Email", data: 'Email', defaultContent: '-', class: 'text-center'},
			{title: "NPWP", data: 'NPWP', defaultContent: '-', class: 'text-center'},
			{title: "Address", data: 'Address', defaultContent: '-', class: 'text-center'},
      {title: "Status", data: 'active', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "Actions", data: 'action', orderable: false, responsivePriority: -1},
    ],
    dom:  "<'row'<'col-sm-5'l><'col-sm-3' <'datesearchbox'>><'col-sm-3'f>>" +
          "<'row'<'col-sm-12'tr>>" +
          "<'row'<'col-sm-5'i><'col-sm-7'p>>",

    order: [[1, 'asc']],
    bStateSave: true,
    columnDefs: [
      {
        render: function ( data, type, row ) {
						var url = "{{route('olt.customer.page', 'prm_code')}}";
						var url_destroy = "{{route('olt.customer.destroy', 'prm_code')}}";
						var url_active = "{{route('olt.customer.regist', 'prm_code')}}";
						var code = "'"+row.Code+"'";
						var status = "'"+row.ClientStatus+"'";
						var html = ""
        				html+= '<a href="'+url_active.replaceAll('prm_code',row.Code)+'" class="btn btn-icon btn-light btn-hover-info btn-sm" data-toggle="tooltip" data-placement="top" title="Activation"><span class="svg-icon svg-icon-md svg-icon-info"><i class="far flaticon2-rocket icon-lg"></i></span></a>';
        				html+= '<a href="'+url.replaceAll('prm_code',row.Code)+'" class="btn btn-icon btn-light btn-hover-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Setting"><span class="svg-icon svg-icon-md svg-icon-primary"><i class="far flaticon-edit icon-lg"></i></span></a>';
        				html+= '<a data-href="'+url_destroy.replaceAll('prm_code',row.Code)+'" class="btn btn-icon btn-light btn-hover-danger btn-sm" "data-toggle="tooltip" data-placement="top" title="Delete" data-toggle="modal" data-target="#confirm-delete-modal" ><span class="svg-icon svg-icon-md svg-icon-danger"><i class="far flaticon-delete icon-lg"></i></span></a>';

						return html;
        },
        targets: [-1],
        className: 'text-center dt-body-nowrap'
      },
    ],
    initComplete: function() {
      $('.tl-tip').tooltip();
    }
  });


  function refresh_table() {
      $('#datatable').dataTable().ajax.reload();
  }

  function format_date(val) {
      var date_arr = val.split(" ");
      var time = date_arr[1];
      var date_id_arr = date_arr[0].split("-");
      var date_fix = date_id_arr[2] + "-" + date_id_arr[1] + "-" + date_id_arr[0];
      var date_transaction = date_fix + " " + time;
      return date_transaction;
  }

  function refresh_table() {
      table.fnDraw();
    }

</script>
@endsection
