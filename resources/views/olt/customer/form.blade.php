@extends('layout.default')

@section('content')
<div class="subheader py-2 {{ Metronic::printClasses('subheader', false) }}" id="kt_subheader">
  <div class="{{ Metronic::printClasses('subheader-container', false) }} d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
    <div class="d-flex align-items-baseline flex-wrap mr-5">
      <h5 class="text-dark font-weight-bold my-1 mr-5">
        Customer
      </h5>
      <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
        <li class="breadcrumb-item">
          <a href="{{ route('dashboard') }}" class="text-muted">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
          <a href="{{ route('olt.customer.index') }}" class="text-muted">Olt</a>
        </li>
        <li class="breadcrumb-item">
            <a href="#form" class="text-muted">Form Input</a>
        </li>
      </ul>
    </div>
  </div>
</div>

@include('inc.error-list')
@include('inc.success-notif')
<div class="card card-custom">
  <div class="card-header bg-danger flex-wrap border-1 pt-1 pb-0" style="min-height: 0;">
    <div class="card-title pt-1 pb-0">
      <h3 class="card-label font-weight-bolder text-white">Form Customer Input
      </h3>
    </div>
  </div>
  <!-- <div class="card-body pt-1"> -->
  <!-- <div> -->
    <form class="form pt-0" id="form" action="@if (isset($customer->Code)) {{ route('olt.customer.update',$customer->Code) }} @else {{ route('olt.customer.store') }} @endif" method="POST">
        {!! csrf_field() !!}
        @if (isset($customer->Code)) {!! method_field('PUT') !!}  @else {!! method_field('POST') !!} @endif
        <div class="card-body pt-0 pl-0 pr-0">
            <div class="card-custom row">
                <div class="col-lg-10">
                    <div class="card card-custom pt-3 mb-1 col-lg-12">
                        <div class="mb-1">
                            <div class="form-group row mb-0 pt-2">
                                <label class="col-lg-4 col-form-label">Pelanggan ID</label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control form-control-sm" id="Code" name="Code" placeholder="Enter Code" value="@isset ($customer->Code) {{ $customer->Code }} @endisset" />
                                </div>
                            </div>
                        </div>
                        <div class="mb-1">
                            <div class="form-group row mb-0">
                                <label class="col-lg-4 font-weight-bolder">Company Name <span class="text-danger">*</span></label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control form-control-sm" id="Name" name="Name" placeholder="Enter Company Name" value="@isset ($customer->Name) {{ $customer->Name }} @endisset" required/>
                                </div>
                            </div>
                        </div>
                        <div class="mb-1">
                            <div class="form-group row mb-0">
                                <label class="col-lg-4 font-weight-bolder">ISP ID <span class="text-danger">*</span></label>
                  							<div class="col-lg-5">
                                @if ($customer)
                                    <input type="text" class="form-control form-control-sm" id="LinkID" name="LinkID" placeholder="Enter LinkID" value="@isset ($customer->LinkID) {{ $customer->LinkID }} @endisset" required readonly/>
                                @else
                  								<select class="form-control form-control-sm" id="LinkID" name="LinkID" style="width: 100%" required>
                  									<option value="" selected>Chose</option>
                                      @isset ($isp_list)
                      									@foreach($isp_list as $isp)
                                            <option value="{{ $isp->Code }}">{{ $isp->Code }} {{ $isp->Name }}</option>
                                        @endforeach
                    									@endisset
                  								</select>
                                @endif
                  							</div>
                            </div>
                        </div>
                        <div class="mb-1" hidden>
                            <div class="form-group row mb-0">
                                <label class="col-lg-4 font-weight-bolder">MemberID <span class="text-danger">*</span></label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control form-control-sm" id="MemberID" name="MemberID" placeholder="Enter MemberID" value="@isset ($customer->MemberID) {{ $customer->MemberID }} @endisset" />
                                </div>
                            </div>
                        </div>
                        <div class="mb-1">
                            <div class="form-group row mb-0">
                                <label class="col-lg-4 font-weight-bolder">JobTitle <span class="text-danger">*</span></label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control form-control-sm" id="JobTitle" name="JobTitle" placeholder="Enter JobTitle" value="@isset ($customer->JobTitle) {{ $customer->JobTitle }} @endisset" />
                                </div>
                            </div>
                        </div>
                        <div class="mb-1">
                            <div class="form-group row mb-0">
                                <label class="col-lg-4 font-weight-bolder">ContactPerson <span class="text-danger">*</span></label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control form-control-sm" id="ContactPerson" name="ContactPerson" placeholder="Enter ContactPerson" value="@isset ($customer->ContactPerson) {{ $customer->ContactPerson }} @endisset" required/>
                                </div>
                            </div>
                        </div>
                        <div class="mb-1">
                            <div class="form-group row mb-0">
                                <label class="col-lg-4 font-weight-bolder">Email <span class="text-danger">*</span></label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control form-control-sm" id="Email" name="Email" placeholder="Enter Email" value="@isset ($customer->Email) {{ $customer->Email }} @endisset" required/>
                                </div>
                            </div>
                        </div>
                        <div class="mb-1">
                            <div class="form-group row mb-0">
                                <label class="col-lg-4 font-weight-bolder">Phone1 <span class="text-danger">*</span></label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control form-control-sm" id="Phone1" name="Phone1" placeholder="Enter Phone1" value="@isset ($customer->Phone1) {{ $customer->Phone1 }} @endisset" required/>
                                </div>
                            </div>
                        </div>
                        <div class="mb-1">
                            <div class="form-group row mb-0">
                                <label class="col-lg-4 font-weight-bolder">Phone2 <span class="text-danger">*</span></label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control form-control-sm" id="Phone2" name="Phone2" placeholder="Enter Phone2" value="@isset ($customer->Phone2) {{ $customer->Phone2 }} @endisset" required/>
                                </div>
                            </div>
                        </div>
                        <div class="mb-1">
                            <div class="form-group row mb-0">
                                <label class="col-lg-4 font-weight-bolder">PhoneNumber <span class="text-danger">*</span></label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control form-control-sm" id="PhoneNumber" name="PhoneNumber" placeholder="Enter PhoneNumber" value="@isset ($customer->PhoneNumber) {{ $customer->PhoneNumber }} @endisset" required/>
                                </div>
                            </div>
                        </div>
                        <div class="mb-1">
                            <div class="form-group row mb-0">
                                <label class="col-lg-4 font-weight-bolder">NPWP <span class="text-danger">*</span></label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control form-control-sm" id="NPWP" name="NPWP" placeholder="Enter NPWP" value="@isset ($customer->NPWP) {{ $customer->NPWP }} @endisset" required/>
                                </div>
                            </div>
                        </div>
                        <div class="mb-1">
                            <div class="form-group row mb-0">
                                <label class="col-lg-4 font-weight-bolder">Fax <span class="text-danger">*</span></label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control form-control-sm" id="Fax" name="Fax" placeholder="Enter Fax" value="@isset ($customer->Fax) {{ $customer->Fax }} @endisset" required/>
                                </div>
                            </div>
                        </div>
                        <div class="mb-1">
                            <div class="form-group row mb-0">
                                <label class="col-lg-4 font-weight-bolder">Address <span class="text-danger">*</span></label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control form-control-sm" id="Address" name="Address" placeholder="Enter Address" value="@isset ($customer->Address) {{ $customer->Address }} @endisset" required/>
                                </div>
                            </div>
                        </div>
                        <div class="mb-1">
                            <div class="form-group row mb-0">
                                <label class="col-lg-4 font-weight-bolder">CityCode <span class="text-danger">*</span></label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control form-control-sm" id="CityCode" name="CityCode" placeholder="Enter CityCode" value="@isset ($customer->CityCode) {{ $customer->CityCode }} @endisset" required/>
                                </div>
                            </div>
                        </div>
                        <div class="mb-1">
                            <div class="form-group row mb-0">
                                <label class="col-lg-4 font-weight-bolder">City <span class="text-danger">*</span></label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control form-control-sm" id="City" name="City" placeholder="Enter City" value="@isset ($customer->City) {{ $customer->City }} @endisset" required/>
                                </div>
                            </div>
                        </div>
                        <div class="mb-1">
                            <div class="form-group row mb-0">
                                <label class="col-lg-4 font-weight-bolder">Province <span class="text-danger">*</span></label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control form-control-sm" id="Province" name="Province" placeholder="Enter Province" value="@isset ($customer->Province) {{ $customer->Province }} @endisset" required/>
                                </div>
                            </div>
                        </div>
                        <div class="mb-1">
                            <div class="form-group row mb-0">
                                <label class="col-lg-4 font-weight-bolder">ZipCode <span class="text-danger">*</span></label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control form-control-sm" id="ZipCode" name="ZipCode" placeholder="Enter ZipCode" value="@isset ($customer->ZipCode) {{ $customer->ZipCode }} @endisset" required/>
                                </div>
                            </div>
                        </div>
                      </div>
                  </div>
                  <div class="col-lg-6" hidden>
                      <div class="card card-custom pt-3 mb-1 col-lg-12">
                        <div class="mb-1">
                            <div class="form-group row mb-0">
                                <label class="col-lg-4 col-form-label">Website</label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control form-control-sm" id="Website" name="Website" placeholder="Enter Website" value="@isset ($customer->Website) {{ $customer->Website }} @endisset"/>
                                </div>
                            </div>
                        </div>
                        <div class="mb-1">
                            <div class="form-group row mb-0">
                                <label class="col-lg-4 col-form-label">IDCard</label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control form-control-sm" id="IDCard" name="IDCard" placeholder="Enter IDCard" value="@isset ($customer->IDCard) {{ $customer->IDCard }} @endisset"/>
                                </div>
                            </div>
                        </div>
                        <div class="mb-1">
                            <div class="form-group row mb-0">
                                <label class="col-lg-4 col-form-label">IDCardNumber</label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control form-control-sm" id="IDCardNumber" name="IDCardNumber" placeholder="Enter IDCardNumber" value="@isset ($customer->IDCardNumber) {{ $customer->IDCardNumber }} @endisset"/>
                                </div>
                            </div>
                        </div>
                        <div class="mb-1">
                            <div class="form-group row mb-0">
                                <label class="col-lg-4 col-form-label">IDCardExpired</label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control form-control-sm" id="IDCardExpired" name="IDCardExpired" placeholder="Enter IDCardExpired" value="@isset ($customer->IDCardExpired) {{ $customer->IDCardExpired }} @endisset"/>
                                </div>
                            </div>
                        </div>
                        <div class="mb-1">
                            <div class="form-group row mb-0">
                                <label class="col-lg-4 col-form-label">ProductCode</label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control form-control-sm" id="ProductCode" name="ProductCode" placeholder="Enter ProductCode" value="@isset ($customer->ProductCode) {{ $customer->ProductCode }} @endisset"/>
                                </div>
                            </div>
                        </div>
                        <div class="mb-1">
                            <div class="form-group row mb-0">
                                <label class="col-lg-4 col-form-label">ClientStatus</label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control form-control-sm" id="ClientStatus" name="ClientStatus" placeholder="Enter ClientStatus" value="@isset ($customer->ClientStatus) {{ $customer->ClientStatus }} @endisset"/>
                                </div>
                            </div>
                        </div>
                        <div class="mb-1">
                            <div class="form-group row mb-0">
                                <label class="col-lg-4 col-form-label">ProductSite</label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control form-control-sm" id="ProductSite" name="ProductSite" placeholder="Enter ProductSite" value="@isset ($customer->ProductSite) {{ $customer->ProductSite }} @endisset"/>
                                </div>
                            </div>
                        </div>
                        <div class="mb-1">
                            <div class="form-group row mb-0">
                                <label class="col-lg-4 col-form-label">ProductGrup</label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control form-control-sm" id="ProductGrup" name="ProductGrup" placeholder="Enter ProductGrup" value="@isset ($customer->ProductGrup) {{ $customer->ProductGrup }} @endisset"/>
                                </div>
                            </div>
                        </div>
                        <div class="mb-1">
                            <div class="form-group row mb-0">
                                <label class="col-lg-4 col-form-label">ProductPort</label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control form-control-sm" id="ProductPort" name="ProductPort" placeholder="Enter ProductPort" value="@isset ($customer->ProductPort) {{ $customer->ProductPort }} @endisset"/>
                                </div>
                            </div>
                        </div>
                        <div class="mb-1">
                            <div class="form-group row mb-0">
                                <label class="col-lg-4 col-form-label">ProductCategory</label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control form-control-sm" id="ProductCategory" name="ProductCategory" placeholder="Enter ProductCategory" value="@isset ($customer->ProductCategory) {{ $customer->ProductCategory }} @endisset"/>
                                </div>
                            </div>
                        </div>
                        <div class="mb-1">
                            <div class="form-group row mb-0">
                                <label class="col-lg-4 col-form-label">BaseTransmissionStationCode</label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control form-control-sm" id="BaseTransmissionStationCode" name="BaseTransmissionStationCode" placeholder="Enter BaseTransmissionStationCode" value="@isset ($customer->BaseTransmissionStationCode) {{ $customer->BaseTransmissionStationCode }} @endisset"/>
                                </div>
                            </div>
                        </div>
                        <div class="mb-1">
                            <div class="form-group row mb-0">
                                <label class="col-lg-4 col-form-label">ProductName</label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control form-control-sm" id="ProductName" name="ProductName" placeholder="Enter ProductName" value="@isset ($customer->ProductName) {{ $customer->ProductName }} @endisset"/>
                                </div>
                            </div>
                        </div>
                        <div class="mb-1">
                            <div class="form-group row mb-0">
                                <label class="col-lg-4 col-form-label">ProductNotes</label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control form-control-sm" id="ProductNotes" name="ProductNotes" placeholder="Enter ProductNotes" value="@isset ($customer->ProductNotes) {{ $customer->ProductNotes }} @endisset"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-10">
                  <div class="card card-custom pt-3 mb-1 col-lg-12">
                    <div class="mb-2">
                      <div class="form-group row mb-0">
                        <label class="col-lg-4 col-form-label">Status</label>
                        <div class="col-lg-8">
                          <input id="ActiveStatus" name="ActiveStatus" data-switch="true"
                            type="checkbox" checked="checked" data-on-text="Enabled" data-handle-width="200" data-handle-font="1"
                            data-off-text="Disabled" data-on-color="warning" />
                        </div>
                      </div>
                    </div>
                    <div class="mb-2">
                      <div class="form-group row mb-0">
                          <label class="col-lg-4 col-form-label"></label>
                          <div class="col-lg-8">
                              <button type="submit" class="btn btn-success font-weight-bold">Save</button>
                              <a type="button" class="btn btn-danger" href="{{ route('olt.customer.index') }}">Cancel</a>
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </form>
  <!-- </div> -->
</div>
  @endsection

{{-- Styles Section --}}
@section('styles')
<link rel="stylesheet" href="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.css">
<link rel="stylesheet" href="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.min.css">
<link rel="stylesheet" href="{{ config('app.url') }}plugins/custom/datatables/datatables.bundle.css">
<link rel="stylesheet" href="{{ config('app.url') }}css/inject.css">
@endsection

{{-- Scripts Section --}}
@section('scripts')
@include ('inc.confirm-delete-modal')
<script src="{{ config('app.url') }}global/vendor/datatables/jquery.dataTables.js"></script>
<script src="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.js"></script>
<script src="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.js"></script>
<script type="text/javascript">
 $(document).ready(function() {
      @if (!$customer)
       $("#LinkID").select2();
      @endif
     $('[data-switch=true]').bootstrapSwitch();

 });
</script>
@endsection
