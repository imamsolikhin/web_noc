@extends('layout.default') @section('content')
<div
	class="subheader py-2 {{ Metronic::printClasses('subheader', false) }}"
	id="kt_subheader">
	<div
		class="{{ Metronic::printClasses('subheader-container', false) }} d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
		<div class="d-flex align-items-baseline flex-wrap mr-5">
			<h5 class="text-dark font-weight-bold my-1 mr-5">Customer Site</h5>
			<ul
				class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
				<li class="breadcrumb-item"><a href="{{ route('dashboard') }}"
					class="text-muted">Dashboard</a></li>
				<li class="breadcrumb-item"><a
					href="{{ route('olt.customer-site.index') }}" class="text-muted">Olt</a>
				</li>
				<li class="breadcrumb-item"><a href="#view" class="text-muted">Customer Site</a>
				</li>
			</ul>
		</div>
	</div>
</div>

@include('inc.error-list') @include('inc.success-notif')
<div class="card card-custom">
	<div class="card-header bg-danger flex-wrap border-1 pt-1 pb-0 mb-2"
		style="min-height: 0;">
		<div class="card-title pt-1 pb-1">
			<h3 class="card-label font-weight-bolder text-white">
				Customer Site
				<div class="text-muted pt-2 font-size-lg">show Datatable from table
					Customer Site</div>
			</h3>
		</div>
	</div>
	<div class="card-body pt-1">
		<table class="table table-bordered table-hover w100" cellspacing="0"
			id="datatable" style="width: 1070px !important;"></table>
	</div>
</div>


<!-- Modal-->
<div class="modal fade" id="modal-form" data-backdrop="static"
	tabindex="-1" role="dialog" aria-labelledby="staticBackdrop"
	aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-danger pt-3 pb-3">
				<h5 class="modal-title text-white bold" id="modal">New Customer Site</h5>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<i aria-hidden="true" class="ki ki-close"></i>
				</button>
			</div>
			<form class="form" id="form-input"
				action="{{ route('olt.customer-site.store') }}" method="POST">
				{!! csrf_field() !!} <input type="hidden"
					class="form-control form-control-sm" id="method" name="_method"
					placeholder="Enter method" value="POST" />
				<div class="card-body pt-3">
					<div class="mb-1">
						<div class="form-group row mb-0">
							<div class="col-lg-12">
								<label>Code</label> <input type="text"
									class="form-control form-control-sm" id="Code" name="Code"
									value="" placeholder="Enter Code" / required>
							</div>
						</div>
					</div>
					<div class="mb-1">
						<div class="form-group row mb-0">
							<div class="col-lg-12">
								<label>Link ID</label> <input type="text"
									class="form-control form-control-sm" id="LinkID" name="LinkID"
									value="" placeholder="Enter LinkID" / required>
							</div>
						</div>
					</div>
					<div class="mb-1">
						<div class="form-group row mb-0">
							<div class="col-lg-12">
								<label>IDPelanggan</label> <input type="text"
									class="form-control form-control-sm" id="IDPelanggan"
									name="IDPelanggan" value=""
									placeholder="Enter ID Pelanggan" / required>
							</div>
						</div>
					</div>
					<div class="mb-2">
						<div class="form-group row mb-0">
							<label class="col-lg-4 col-form-label">Status</label>
							<div class="col-lg-8">
								<input id="ActiveStatus" name="ActiveStatus" data-switch="true"
									type="checkbox" checked="checked" data-on-text="Enabled"
									data-handle-width="200" data-handle-font="1"
									data-off-text="Disabled" data-on-color="warning" />
							</div>
						</div>
					</div>
					<div class="mb-2">
						<div class="form-group row mb-0">
							<label class="col-lg-4 col-form-label"></label>
							<div class="col-lg-8">
								<button type="submit" class="btn btn-success font-weight-bold">Save</button>
								<button type="button" data-dismiss="modal"
									class="btn btn-danger">Cancel</button>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection {{-- Styles Section --}} @section('styles')
<link rel="stylesheet"
	href="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.css">
<link rel="stylesheet"
	href="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.min.css">
<link rel="stylesheet"
	href="{{ config('app.url') }}plugins/custom/datatables/datatables.bundle.css">
<link rel="stylesheet" href="{{ config('app.url') }}css/inject.css">
@endsection {{-- Scripts Section --}} @section('scripts') @include('inc.confirm-delete-modal')
<script
	src="{{ config('app.url') }}global/vendor/datatables/jquery.dataTables.js"></script>
<script
	src="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.js"></script>
<script
	src="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.js"></script>
<script type="text/javascript">
  $("html").keydown(function(e){
    if(e.shiftKey && e.keyCode == 'N'.charCodeAt(0) && !e.altKey){
         show_data('');
     }
  });
  $(document).ready(function() {
    $("#datatable_wrapper").removeClass("dataTables_wrapper form-inline dt-bootstrap no-footer");
    $("#datatable_wrapper").addClass("dataTables_wrapper dt-bootstrap4 no-footer");
  });

  $('[data-switch=true]').bootstrapSwitch();
  $("#DeviceModel").select2();
  $("#DeviceVersion").select2();
  var table = $('#datatable').dataTable({
    pageLength: 5,
    responsive: true,
    searchDelay: 800,
    processing: true,
    serverSide: true,
    select: true,
    searching: true,
    lengthMenu: [[5, 10, 25, 50, 100, 200, -1], [5, 10, 25, 50, 100, 200, "All"]],
    ajax: {
      method: 'POST',
      url : '{{ route('olt.customer-site.data') }}',
      headers: {
        'X-CSRF-TOKEN': '{{ csrf_token() }}'
      }
    },
    columns: [
      {title: "No", data: 'DT_RowIndex', defaultContent: '-', class: 'text-center dt-body-nowrap', orderable: false, searchable: false, autoHide: false},
      {title:'Code',  data: 'Code', name: 'Code', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'Link ID', data: 'LinkID', name: 'LinkID', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'IDPelanggan', data: 'IDPelanggan', name: 'IDPelanggan', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'CompanyName', data: 'CompanyName', name: 'CompanyName', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'Province', data: 'Province', name: 'Province', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'City', data: 'City', name: 'City', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'Address', data: 'Address', name: 'Address', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'ZipCode', data: 'ZipCode', name: 'ZipCode', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'Website', data: 'Website', name: 'Website', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'Email', data: 'Email', name: 'Email', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'Phone', data: 'Phone', name: 'Phone', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'Fax', data: 'Fax', name: 'Fax', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'ProductSite', data: 'ProductSite', name: 'ProductSite', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'ProductGrup', data: 'ProductGrup', name: 'ProductGrup', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'ProductPort', data: 'ProductPort', name: 'ProductPort', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'ProductCategory', data: 'ProductCategory', name: 'ProductCategory', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'ProductName', data: 'ProductName', name: 'ProductName', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title:'IDPelanggan', data: 'IDPelanggan', name: 'IDPelanggan', defaultContent: '-', class: 'text-center dt-body-nowrap', },
      {title: "Status", data: 'active', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "Actions", data: 'action', orderable: false, responsivePriority: -1},
    ],
    order: [[1, 'asc']],
    bStateSave: true,
    columnDefs: [
      {
        targets: [0,-1],
        className: 'text-center visible dt-body-nowrap'
      },
    ],
    buttons: [
      {
        extend: 'print',
        text: 'Print current page',
        autoPrint: true,
        customize: function (win) {
          $(win.document.body)
          .css('font-size', '10pt')
          .prepend(
            '<img src="http://datatables.net/media/images/logo-fade.png" style="position:absolute; top:0; left:0;" />'
          );
          $(win.document.body).find('table')
          .addClass('compact')
          .css('font-size', 'inherit');
        },
        exportOptions: {
          columns: [0, 1, 'visible'],
          modifier: {
            page: 'current'
          },
        }
      }, {
        extend: 'copy',
        className: 'btn default',
        exportOptions: {
          columns: [0, 1, 'visible']
        }
      }, {
        extend: 'pdf',
        className: 'btn default',
        exportOptions: {
          columns: [0, 1, 'visible']
        }
      }, {
        extend: 'excelHtml5',
        className: 'btn default',
        excelStyles: {
          template: 'blue_medium',
        },
        exportOptions: {
          columns: [0, 1, 'visible']
        }
      }, {
        extend: 'csvHtml5',
        className: 'btn default',
        exportOptions: {
          columns: [0, 1, 'visible']
        }
      }, {
        text: 'Reload',
        className: 'btn default',
        action: function (e, dt, node, config) {
          dt.ajax.reload();
          alert_show('Datatable reloaded!', false);
        }
      },
      'colvis'
    ],
    initComplete: function() {
      $('.tl-tip').tooltip();
      @if (count($errors) > 0)
      // jQuery("html, body").animate({
      //   scrollTop: $('#add-form').offset().top - 100 // 66 for sticky bar height
      // }, "slow");
      @endif
    }
  });

  function show_data(id = "") {
      if (id !== "") {
          $.ajax({
              url: "{{ route('olt.customer-site.store')}}/" + id,
              type: "GET",
              headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
              },
              success: function (response) {
                  $("#form-input").attr("action", "{{ route('olt.customer-site.update','')}}/"+id);
                  $('#form-input').trigger("reset");
                  $('#method').val("PUT");
                  $('#Code').attr("readonly", true);
                  $('#Code').val(response.data.Code);
                  $('#LinkID').val(response.data.LinkID);
                  $('#IDPelanggan').val(response.data.IDPelanggan);
                  $('#CompanyName').val(response.data.CompanyName);
                  $('#Province').val(response.data.Province);
                  $('#City').val(response.data.City);
                  $('#Address').val(response.data.Address);
                  $('#ZipCode').val(response.data.ZipCode);
                  $('#Website').val(response.data.Website);
                  $('#Email').val(response.data.Email);
                  $('#Phone').val(response.data.Phone);
                  $('#Fax').val(response.data.Fax);
                  $('#ProductSite').val(response.data.ProductSite);
                  $('#ProductGrup').val(response.data.ProductGrup);
                  $('#ProductPort').val(response.data.ProductPort);
                  $('#ProductCategory').val(response.data.ProductCategory);
                  $('#ProductName').val(response.data.ProductName);
                  $('#ProductNotes').val(response.data.ProductNotes);
                  if (response.data.ActiveStatus === 1) {
                      $('#ActiveStatus').bootstrapSwitch('state', true);
                  } else {
                      $('#ActiveStatus').bootstrapSwitch('state', false);
                  }
                  $('#modal-form').modal('show');
                  $('#Code').focus();
              },
              error: function (xhr, status, error) {
                  alert_show(xhr.status + " " + status + " " + error, false);
              }
          });
      } else {
          $("#form-input").attr("action", "{{ route('olt.customer-site.store')}}");
          $('#method').val("POST");
          $('#Code').attr("readonly", false);
          $('#Code').focus();
          $('#form-input').trigger("reset");
          $('#modal-form').modal('show');
          $('#Code').focus();
      }
  }

  function refresh_table() {
      $('#datatable').dataTable().ajax.reload();
  }

  function format_date(val) {
      var date_arr = val.split(" ");
      var time = date_arr[1];
      var date_id_arr = date_arr[0].split("-");
      var date_fix = date_id_arr[2] + "-" + date_id_arr[1] + "-" + date_id_arr[0];
      var date_transaction = date_fix + " " + time;
      return date_transaction;
  }
</script>
@endsection
