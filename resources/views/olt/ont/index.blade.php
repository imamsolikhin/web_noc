@extends('layout.default') @section('content')
<div
	class="subheader py-2 {{ Metronic::printClasses('subheader', false) }}"
	id="kt_subheader">
	<div
		class="{{ Metronic::printClasses('subheader-container', false) }} d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
		<div class="d-flex align-items-baseline flex-wrap mr-5">
			<h5 class="text-dark font-weight-bold my-1 mr-5">Ont</h5>
			<ul
				class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
				<li class="breadcrumb-item"><a href="{{ route('dashboard') }}"
					class="text-muted">Dashboard</a></li>
				<li class="breadcrumb-item"><a
					href="{{ route('olt.ont.index') }}" class="text-muted">Olt</a>
				</li>
				<li class="breadcrumb-item"><a href="#view" class="text-muted">Ont</a></li>
			</ul>
		</div>
	</div>
</div>

@include('inc.error-list')
@include('inc.success-notif')
<div class="card card-custom" id="ont-group">
	<div class="card-header bg-danger flex-wrap border-1 pt-1 pb-0 mb-2"
		style="min-height: 0;">
		<div class="card-title pt-1 pb-1">
			<h3 class="card-label font-weight-bolder text-white">
				Ont
				<div class="text-muted pt-2 font-size-lg">show Datatable from table
					Ont</div>
			</h3>
		</div>
		<div class="card-toolbar">
				<div class="dropdown dropdown-inline">
						<button type="button" class="btn btn-tool btn-lg" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="la la-download text-white"></i> Tools
						</button>
						<div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
								<ul class="navi flex-column navi-hover py-2" id="sample_3_tools">
										<li class="navi-header font-weight-bolder text-uppercase font-size-xs text-primary pb-2">
												Export Tools
										</li>
										<li class="navi-item">
												<a href="javascript:;" data-action="0" class="navi-link tool-action" id="btn-auto-find">
														<span class="navi-icon"><i class="fa fa-search"></i></span>
														<span class="navi-text">Auto Find SN</span>
												</a>
										</li>
										<li class="navi-item">
												<a href="javascript:;" data-action="0" class="navi-link tool-action" id="btn-find-by-host">
														<span class="navi-icon"><i class="fa fa-search"></i></span>
														<span class="navi-text">Find By Host</span>
												</a>
										</li>
								</ul>
						</div>
				</div>
		</div>
	</div>
	<div class="card-body pt-1">
		<table class="table table-bordered table-hover w100" cellspacing="0"
			id="datatable" style="width: 1070px !important;"></table>
	</div>
</div>


<!-- Modal-->
<div class="modal fade" id="modal-find-by-host" data-backdrop="static"
	tabindex="-1" role="dialog" aria-labelledby="staticBackdrop"
	aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-danger pt-3 pb-3">
				<h5 class="modal-title text-white bold" id="modal">Select By Host</h5>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<i aria-hidden="true" class="ki ki-close"></i>
				</button>
			</div>
			<form class="form" action="{{ route('olt.host.store') }}" method="POST">
				<div class="card-body pt-3">
          <div class="mb-2">
            <div class="form-group row mb-0">
              <label class="col-lg-4 col-form-label">Host Code</label>
              <div class="col-lg-8">
                <select class="form-control form-control-sm" id="SerachHostCode" name="SerachHostCode" style="width: 100%" required>
                  <option value="" selected>Chose</option>
                  @isset ($host_list)
                    @foreach($host_list as $host)
                      <option value="{{ $host->Code }}">{{$host->Hostname }}</option>
                    @endforeach
                  @endisset
                </select>
              </div>
            </div>
          </div>
					<div class="mb-2">
						<div class="form-group row mb-0">
							<label class="col-lg-4 col-form-label"></label>
							<div class="col-lg-8">
								<button type="button" id="find-by-host" class="btn btn-info font-weight-bold">Find ONT IDLE</button>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection

{{-- Styles Section --}}
@section('styles')
<link rel="stylesheet"
	href="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.css">
<link rel="stylesheet"
	href="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.min.css">
<link rel="stylesheet"
	href="{{ config('app.url') }}plugins/custom/datatables/datatables.bundle.css">
<link rel="stylesheet" href="{{ config('app.url') }}css/inject.css">
@endsection

{{-- Scripts Section --}}
@section('scripts')
@include('inc.confirm-delete-modal')
<script
	src="{{ config('app.url') }}global/vendor/datatables/jquery.dataTables.js"></script>
<script
	src="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.js"></script>
<script
	src="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.js"></script>
<script type="text/javascript">
	var list_ont = [];
  $("html").keydown(function(e){
    if(e.shiftKey && e.keyCode == 'N'.charCodeAt(0) && !e.altKey){
         show_data('');
     }
  });
  $(document).ready(function() {
		show_data([]);
    $("#datatable_wrapper").removeClass("dataTables_wrapper form-inline dt-bootstrap no-footer");
    $("#datatable_wrapper").addClass("dataTables_wrapper dt-bootstrap4 no-footer");
    $("#SerachHostCode").select2();

		$.ajax({
				url: "{{ route('olt.ont.data')}}",
				type: "POST",
				headers: {
	        'X-CSRF-TOKEN': '{{ csrf_token() }}'
	      },
				success: function (datas) {
					if(datas.data){
						show_data(datas.data);
					}
				},
				error: function (xhr, status, error) {
					 // $("#ont-group").loading("stop");
					// showDialog.show(xhr.status + " " + status + " " + error, false);
				}
		});
  });

	$('#btn-auto-find').on( 'click', function () {
		$("#ont-group").loading("toggle");
		$.ajax({
				url: "{{ route('telnet.olt-config.autofindall')}}",
				type: "GET",
				success: function (datas) {
					// destroy_data();
					show_data(datas.data);
				  $("#ont-group").loading("stop");
				},
				error: function (xhr, status, error) {
					 $("#ont-group").loading("stop");
					// showDialog.show(xhr.status + " " + status + " " + error, false);
				}
		});
	});

	$('#find-by-host').on( 'click', function () {
		$("#ont-group").loading("toggle");
		$('#modal-find-by-host').modal('hide');
		var host = $("#SerachHostCode").val();
		$.ajax({
				url: "{{ route('telnet.olt-config.autofindhost','')}}/"+host,
				type: "GET",
				success: function (datas) {
					// destroy_data();
					show_data(datas.data);
					$("#ont-group").loading("stop");
				},
				error: function (xhr, status, error) {
					 $("#ont-group").loading("stop");
					// showDialog.show(xhr.status + " " + status + " " + error, false);
				}
		});
	});

	$('#btn-find-by-host').on( 'click', function () {
		$('#modal-find-by-host').modal('show');
	});

	function checkPower (host="", sn = "") {
		if (sn != ""){
			$("#ont-group").loading("start");
      $.ajax({
          url: "{{ route('telnet.olt-config.checkpower',['',''])}}/"+host+"/"+sn,
          type: "GET",
          success: function (datas) {
             $("#ont-group").loading("stop");
             showDialog.show(datas.data, false);
          },
          error: function (xhr, status, error) {
             $("#ont-group").loading("stop");
             // showDialog.show(xhr.status + " " + status + " " + error, false);
          }
      });
    }
	}


  function destroy_data() {
			$('#datatable').dataTable().destroy();
	}
  function show_data(list_ont) {
		if ( $.fn.DataTable.isDataTable('#datatable') ) {
		  $('#datatable').DataTable().destroy();
		}
		$('#datatable tbody').empty();
		$('#datatable').dataTable({
			pageLength: 5,
			responsive: true,
			searchDelay: 800,
			select: true,
			searching: true,
			lengthMenu: [[5, 10, 25, 50, 100, 200, -1], [5, 10, 25, 50, 100, 200, "All"]],
			data: list_ont,
	    bStateSave: true,
			columns: [
				{title: "#", data: 'action', orderable: false},
				{title: "Sn", data: 'Sn', defaultContent: '-', class: 'text-center dt-body-nowrap'},
				{title: "HostCode", data: 'HostCode', defaultContent: '-', class: 'text-center dt-body-nowrap'},
				{title: "Hostname", data: 'Hostname', defaultContent: '-', class: 'text-center dt-body-nowrap'},
				{title: "FrameId", data: 'FrameId', defaultContent: '-', class: 'text-center dt-body-nowrap'},
				{title: "SlotId", data: 'SlotId', defaultContent: '-', class: 'text-center dt-body-nowrap'},
				{title: "PortId", data: 'PortId', defaultContent: '-', class: 'text-center dt-body-nowrap'},
				// {title: "SoftwareVersion", data: 'SoftwareVersion', defaultContent: '-', class: 'text-center dt-body-nowrap'},
				{title: "EquipmentId", data: 'EquipmentId', defaultContent: '-', class: 'text-center dt-body-nowrap'},
				{title: "Last Update", data: 'CreatedDate', defaultContent: '-', class: 'text-center dt-body-nowrap'},
			],
			dom:  "<'row'<'col-sm-5'l><'col-sm-3' <'datesearchbox'>><'col-sm-3'f>>" +
	          "<'row'<'col-sm-12'tr>>" +
	          "<'row'<'col-sm-5'i><'col-sm-7'p>>",
			order: [[1, 'asc']],
			columnDefs: [
				{
					render: function ( data, type, row ) {
						var sn = "'"+row.Sn+"'";
						var host = "'"+row.HostCode+"'";
						var code = "'"+row.Code+"'";
						var html = ""
								html+= '<a onclick="checkPower('+host+','+sn+')" class="btn btn-icon btn-light btn-hover-info btn-sm" data-toggle="tooltip" data-placement="top" title="CheckPower"><span class="svg-icon svg-icon-md svg-icon-info"><i class="far flaticon-technology-2 icon-lg"></i></span></a>';

								return html;
					},
					targets: [0],
					className: 'text-center dt-body-nowrap'
				},
			],

			initComplete: function() {
				$('.tl-tip').tooltip();
			}
		});
  }
</script>
@endsection
