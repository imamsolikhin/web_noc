@extends('layout.default')

@section('content')
    <div class="subheader py-2 {{ Metronic::printClasses('subheader', false) }}" id="kt_subheader">
        <div class="{{ Metronic::printClasses('subheader-container', false) }} d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-baseline flex-wrap mr-5">
                <h5 class="text-dark font-weight-bold my-1 mr-5">
                    Dashboard
                </h5>
                <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                    <li class="breadcrumb-item">
                        <a href="{{ route('dashboard') }}" class="text-muted">Home</a>
                    </li>
                    <li class="breadcrumb-item">
                    	<a href="" class="text-muted">Dashboard</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row">

        <div class="col-lg-12">

            {{-- Mixed Widget 1 --}}

            <div class="card card-custom bg-gray-100 {{ @$class }}">
                {{-- Header --}}
                <div class="card-header border-0 bg-danger py-5">
                    <h3 class="card-title font-weight-bolder text-white">Sales Stat</h3>
                    <div class="card-toolbar">
                        <div class="dropdown dropdown-inline">
                            <a href="#" class="btn btn-transparent-white btn-sm font-weight-bolder dropdown-toggle px-5" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Export
                            </a>
                            <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                                {{-- Navigation --}}
                                <ul class="navi navi-hover">
                                    <li class="navi-header">
                                        <span class="text-primary text-uppercase font-weight-bold">Add new:</span>
                                    </li>
                                    <li class="navi-item">
                                        <a href="#" class="navi-link">
                                            <i class="navi-icon flaticon2-graph-1"></i>
                                            <span class="navi-text">Order</span>
                                        </a>
                                    </li>
                                    <li class="navi-item">
                                        <a href="#" class="navi-link">
                                            <i class="navi-icon flaticon2-calendar-4"></i>
                                            <span class="navi-text">Event</span>
                                        </a>
                                    </li>
                                    <li class="navi-item">
                                        <a href="#" class="navi-link">
                                            <i class="navi-icon flaticon2-layers-1"></i>
                                            <span class="navi-text">Report</span>
                                        </a>
                                    </li>
                                    <li class="navi-item">
                                        <a href="#" class="navi-link">
                                            <i class="navi-icon flaticon2-calendar-4"></i>
                                            <span class="navi-text">Post</span>
                                        </a>
                                    </li>
                                    <li class="navi-item">
                                        <a href="#" class="navi-link">
                                            <i class="navi-icon flaticon2-file-1"></i>
                                            <span class="navi-text">File</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- Body --}}
                <div class="card-body p-0 position-relative overflow-hidden">
                    {{-- Chart --}}
                    <div id="kt_mixed_widget_1_chart" class="card-rounded-bottom bg-danger" style="height: 200px"></div>

                    {{-- Stats --}}
                    <div class="card-spacer mt-n25">
                        {{-- Row --}}
                        <div class="row m-0">
                            <div class="col bg-light-success px-6 py-8 rounded-xl mr-7">
                                {{ Metronic::getSVG("media/svg/icons/Communication/Urgent-mail.svg", "svg-icon-3x svg-icon-success d-block my-2") }}
                                <a href="#" class="text-success font-weight-bold font-size-h6 mt-2">
                                    Clients Active : @isset ($clients_dashboard) {{ $clients_dashboard->CountClientsActive }} @endisset
                                </a>
                            </div>
                            <div class="col bg-light-warning px-6 py-8 rounded-xl mr-7">
                                {{ Metronic::getSVG("media/svg/icons/Home/Flower3.svg", "svg-icon-3x svg-icon-warning d-block my-2") }}
                                <a href="#" class="text-warning font-weight-bold font-size-h6 mt-2">
                                    Clients Postpone : @isset ($clients_dashboard) {{ $clients_dashboard->CountClientsPostpone }} @endisset
                                </a>
                            </div>
                            <div class="col bg-light-danger px-6 py-8 rounded-xl mr-2">
                                {{ Metronic::getSVG("media/svg/icons/Design/Layers.svg", "svg-icon-3x svg-icon-danger d-block my-2") }}
                                <a href="#" class="text-danger font-weight-bold font-size-h6 mt-2">
                                    Clients Closed : @isset ($clients_dashboard) {{ $clients_dashboard->CountClientsClosed }} @endisset
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
@endsection
