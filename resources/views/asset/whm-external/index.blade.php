@extends('layout.default')

@section('content')
<div class="subheader py-2 {{ Metronic::printClasses('subheader', false) }}" id="kt_subheader">
  <div class="{{ Metronic::printClasses('subheader-container', false) }} d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
    <div class="d-flex align-items-baseline flex-wrap mr-5">
      <h5 class="text-dark font-weight-bold my-1 mr-5">
        Warehouse External
      </h5>
      <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
        <li class="breadcrumb-item">
          <a href="{{ route('dashboard') }}" class="text-muted">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
          <a href="{{ route('asset.whm-external.index') }}" class="text-muted">Asset</a>
        </li>
        <li class="breadcrumb-item">
          <a href="#view" class="text-muted">Warehouse External </a>
        </li>
      </ul>
    </div>
  </div>
</div>

@include('inc.error-list')
@include('inc.success-notif')
    <div class="card card-custom">
      <div class="card-header bg-danger flex-wrap border-1 pt-1 pb-0 mb-2" style="min-height: 0;">
        <div class="card-title pt-1 pb-1">
          <h3 class="card-label font-weight-bolder text-white">Warehouse External
            <div class="text-muted pt-2 font-size-lg">show Datatable from table Warehouse External </div>
          </h3>
        </div>
        <div class="card-toolbar pt-1 pb-0">
          <a  href="{{ route('asset.whm-external.page','new') }}" onclick="show_datas('')" class="btn btn-primary font-weight-bolder" style="background-color: #1e1e2d;border-color: #0c8eff;">
            <span class="svg-icon svg-icon-md">
              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                  <rect x="0" y="0" width="24" height="24"></rect>
                  <circle fill="#000000" cx="9" cy="15" r="6"></circle>
                  <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3"></path>
                </g>
              </svg>
            </span>Add New
          </a>
        </div>
      </div>
      <div class="card-body pt-1">
        <table class="table table-bordered table-hover w100" cellspacing="0" id="datatable" style="width: 1070px !important;"></table>
      </div>
    </div>
    <div class="card-body pt-1">
      <table class="table table-bordered table-hover w100" cellspacing="0"
        id="datatable" style="width: 1080px !important;"></table>
    </div>
    <div class="card-body py-1">
        <div class="card mb-8">
            <div class="col-lg-9">
              <div class="row justify-content-center py-8 px-8 py-md-10 px-md-0 mr-2" style="background-color: #fafad252;">
                <div class="col-md-12">
                  <div class="border-bottom w-100"></div>
                  <div class="table-responsive">
                    <h3 class="pt-3"><div id="color-picker-1" class="mx-auto">Item Detail</div></h3>
                      <table id="datatable_item_detail" class="table dataTable table-bordered table-hover w100" cellspacing="0">
                          <thead>
                            <tr>
                                <th class="text-center text-white dt-body-nowrap">Item Code</th>
                                <th class="text-center text-white dt-body-nowrap">Quantity</th>
                                <th class="text-center text-white dt-body-nowrap">Remark</th>
                            </tr>
                          </thead>
                          <tbody>
                          </tbody>
                      </table>
                  </div>
                </div>
              </div>
              <div class="row justify-content-center py-8 px-8 py-md-10 px-md-0 mr-2" style="background-color: #fafad252;">
                <div class="col-md-12">
                  <div class="border-bottom w-100"></div>
                  <div class="table-responsive">
                    <h3 class="pt-3"><div id="color-picker-1" class="mx-auto">Serial No</div></h3>
                      <table id="datatable_serial_no" class="table dataTable table-bordered table-hover w100" cellspacing="0">
                          <thead>
                            <tr>
                                <th class="text-center text-white dt-body-nowrap">ItemCode</th>
                                <th class="text-center text-white dt-body-nowrap">SerialNo</th>
                                <th class="text-center text-white dt-body-nowrap">Remark</th>
                            </tr>
                          </thead>
                          <tbody>
                          </tbody>
                      </table>
                  </div>
                </div>
              </div>
              <div class="tab-content">
                <div class="tab-pane fade show active mt-2" id="v-pills-home" role="tab">
                  <div class="card-body px-0">
                    <div class="d-flex">
                      <div class="flex-grow-1">
                        <div class="d-flex align-items-center flex-wrap justify-content-between">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>


@endsection

{{-- Styles Section --}}
@section('styles')
<link rel="stylesheet" href="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.css">
<link rel="stylesheet" href="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.min.css">
<link rel="stylesheet" href="{{ config('app.url') }}plugins/custom/datatables/datatables.bundle.css">
<link rel="stylesheet" href="{{ config('app.url') }}css/inject.css">
@endsection

{{-- Scripts Section --}}
@section('scripts')
@include ('inc.confirm-delete-modal')
<script src="{{ config('app.url') }}global/vendor/datatables/jquery.dataTables.js"></script>
<script src="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.js"></script>
<script src="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.js"></script>
<script type="text/javascript">

  var table = $('#datatable').dataTable({
    pageLength: 5,
    responsive: true,
    searchDelay: 800,
    processing: true,
    serverSide: true,
    select: true,
    searching: true,
    lengthMenu: [[5, 10, 25, 50, 100, 200, -1], [5, 10, 25, 50, 100, 200, "All"]],
    ajax: {
      method: 'POST',
      url : '{{ route('asset.whm-external.data') }}',
      headers: {
        'X-CSRF-TOKEN': '{{ csrf_token() }}'
      }
    },
    columns: [
      {title: "No", data: 'DT_RowIndex', defaultContent: '-', class: 'text-center dt-body-nowrap', orderable: false, searchable: false, autoHide: false},
      {title: "WHT-No", data: 'Code', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "Transaction Date", data: 'Transactiondate', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "BranchCode", data: 'BranchCode', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "CompanyCode", data: 'CompanyCode', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "CurrencyCode", data: 'CurrencyCode', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "ExchangeRate", data: 'ExchangeRate', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "Transactiondate", data: 'Transactiondate', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "SourceWarehouseCode", data: 'SourceWarehouseCode', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "DestinationWarehouseCode", data: 'DestinationWarehouseCode', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "ApprovalStatus", data: 'ApprovalStatus', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "ApprovalDate", data: 'ApprovalDate', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "ApprovalBy", data: 'ApprovalBy', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "Ref No", data: 'RefNo', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "Remark", data: 'Remark', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "Actions", data: 'action', orderable: false, responsivePriority: -1},
    ],
    order: [[1, 'asc']],
    bStateSave: true,
    columnDefs: [
      {
        targets: [0,-1],
        className: 'text-center visible dt-body-nowrap'
      },
    ],
    buttons: [
      {
        extend: 'print',
        text: 'Print current page',
        autoPrint: true,
        customize: function (win) {
          $(win.document.body)
          .css('font-size', '10pt')
          .prepend(
            '<img src="http://datatables.net/media/images/logo-fade.png" style="position:absolute; top:0; left:0;" />'
          );
          $(win.document.body).find('table')
          .addClass('compact')
          .css('font-size', 'inherit');
        },
        exportOptions: {
          columns: [0, 1, 'visible'],
          modifier: {
            page: 'current'
          },
        }
      }, {
        extend: 'copy',
        className: 'btn default',
        exportOptions: {
          columns: [0, 1, 'visible']
        }
      }, {
        extend: 'pdf',
        className: 'btn default',
        exportOptions: {
          columns: [0, 1, 'visible']
        }
      }, {
        extend: 'excelHtml5',
        className: 'btn default',
        excelStyles: {
          template: 'blue_medium',
        },
        exportOptions: {
          columns: [0, 1, 'visible']
        }
      }, {
        extend: 'csvHtml5',
        className: 'btn default',
        exportOptions: {
          columns: [0, 1, 'visible']
        }
      }, {
        text: 'Reload',
        className: 'btn default',
        action: function (e, dt, node, config) {
          dt.ajax.reload();
          alert_show('Datatable reloaded!', false);
        }
      },
      'colvis'
    ],
    initComplete: function() {
      $('.tl-tip').tooltip();
      @if (count($errors) > 0)
      // jQuery("html, body").animate({
      //   scrollTop: $('#add-form').offset().top - 100 // 66 for sticky bar height
      // }, "slow");
      @endif
    }
  });


  $('#datatable tbody').on( 'click', 'tr', function () {
    $("#datatable_item_detail tbody>tr").remove();
    $("#datatable_serial_no tbody>tr").remove();
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
            $('.Code').html("Data not selected");
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            var id = $(this).find("td:eq(1)").html();
            $("#data-asset").loading("start");
            $.ajax({
                url: "{{ route('asset.whm-external.show-header','')}}/" + id,
                type: "GET",
                headers: {
                  'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                success: function (response) {
                  if(response.data["item_list"]){
                      $("#datatable_item_detail tbody>tr").remove();
                      for (var i in response.data["item_list"]){
                          var table = document.getElementById("datatable_item_detail").getElementsByTagName('tbody')[0];
                          var row = table.insertRow(-1);
                          var cell0 = row.insertCell(0);
                          var cell1 = row.insertCell(1);
                          var cell2 = row.insertCell(2);
                          cell0.innerHTML = '<label class="col-12 text-center">'+response.data["item_list"][i].ItemCode+'</label>';
                          cell1.innerHTML = '<label class="col-12 text-center">'+response.data["item_list"][i].Quantity+'</label>';
                          cell2.innerHTML = '<label class="col-12 text-center">'+response.data["item_list"][i].Remark+'</label>';
                      }
                    }
                      if(response.data["serial_list"]){
                          $("#datatable_serial_no tbody>tr").remove();
                          for (var i in response.data["serial_list"]){
                              var table = document.getElementById("datatable_serial_no").getElementsByTagName('tbody')[0];
                              var row = table.insertRow(-1);
                              var cell0 = row.insertCell(0);
                              var cell1 = row.insertCell(1);
                              var cell2 = row.insertCell(2);
                              cell0.innerHTML = '<label class="col-12 text-center">'+response.data["serial_list"][i].ItemCode+'</label>';
                              cell1.innerHTML = '<label class="col-12 text-center">'+response.data["serial_list"][i].SerialNo+'</label>';
                              cell2.innerHTML = '<label class="col-12 text-center">'+response.data["serial_list"][i].RemarkText+'</label>';
                          }
                        }
                    $("#data-asset").loading("stop");
                },
                error: function (xhr, status, error) {
                  $("#data-asset").loading("stop");
                  // showDialog.show(xhr.status + " " + status + " " + error, false);
                }
            });
        }
    });

  function refresh_tarefresh_table() {
      $('#datatable').dataTable().ajax.reload();
  }



  function format_date(val) {
      var date_arr = val.split(" ");
      var time = date_arr[1];
      var date_id_arr = date_arr[0].split("-");
      var date_fix = date_id_arr[2] + "-" + date_id_arr[1] + "-" + date_id_arr[0];
      var date_transaction = date_fix + " " + time;
      return date_transaction;
  }
</script>
@endsection
