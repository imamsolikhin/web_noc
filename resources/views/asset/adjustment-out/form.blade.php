@extends('layout.default')

@section('content')
<div class="subheader py-2 {{ Metronic::printClasses('subheader', false) }}" id="kt_subheader">
  <div class="{{ Metronic::printClasses('subheader-container', false) }} d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
    <div class="d-flex align-items-baseline flex-wrap mr-5">
      <h5 class="text-dark font-weight-bold my-1 mr-5">
        Adjustment Out
      </h5>
      <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
        <li class="breadcrumb-item">
          <a href="{{ route('dashboard') }}" class="text-muted">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
          <a href="{{ route('asset.adjustment-out.index') }}" class="text-muted">Asset</a>
        </li>
        <li class="breadcrumb-item">
            <a href="#form" class="text-muted">Form Input</a>
        </li>
      </ul>
    </div>
  </div>
</div>

@include('inc.error-list')
@include('inc.success-notif')
<div class="card card-custom">
  <div class="card-header bg-danger flex-wrap border-1 pt-1 pb-0" style="min-height: 0;">
    <div class="card-title pt-1 pb-0">
      <h3 class="card-label font-weight-bolder text-white">Form Input
      </h3>
    </div>
  </div>
  <!-- <div class="card-body pt-1"> -->
  <div>
    <form class="form pt-0" id="form" action="@if (isset($AdjustmentOut->Code)) {{ route('asset.adjustment-out.update',$AdjustmentOut->Code) }} @else {{ route('asset.adjustment-out.store') }} @endif" method="POST">
        {!! csrf_field() !!}
        @if (isset($AdjustmentOut->Code)) {!! method_field('PUT') !!}  @else {!! method_field('POST') !!} @endif
        <div class="card-body pt-0 pl-3 pr-3">
            <div class="row">
                <div class="col-lg-12">
                    <!-- <div class="card card-custom mb-1"> -->
                        <div id="formInputAdjusmentIn" class="col-lg-8">
                            <div class="mb-1" hidden>
                                <div class="form-group row mb-0 pt-2">
                                    <label class="col-lg-4 col-form-label">Code</label>
                                    <div class="col-lg-5">
                                        <input type="text" class="form-control form-control-sm" id="Code" name="Code" placeholder="Enter Code" value="@isset ($AdjustmentOut->Code) {{ $AdjustmentOut->Code }} @endisset" />
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">Branch Code</label>
                                    <div class="col-lg-5">
                                         <input class="form-control form-control-sm" id="BranchCode" name="BranchCode" value="@isset ($AdjustmentOut->BranchCode) {{ $AdjustmentOut->BranchCode }} @endisset" />
                                    </div>
                                      <div class="col-lg-3">
                        								<select class="form-control form-control-sm" id="Branch" name="Branch" style="width: 100%">
                        									<option value="" selected>Chose</option> @isset ($branch_list)
                        									@foreach($branch_list as $branch)
                        									<option value="{{ $branch->Name }}">{{ $branch->Name }}</option> @endforeach
                        									@endisset
                        								</select>
                        							</div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">Company Code</label>
                                    <div class="col-lg-5">
                                        <input type="text" class="form-control form-control-sm" id="CompanyCode" name="CompanyCode" placeholder="Enter CompanyCode" value="@isset ($AdjustmentOut->CompanyCode) {{ $AdjustmentOut->CompanyCode }} @endisset" />
                                    </div>
                                      <div class="col-lg-3">
                        								<input class="form-control form-control-sm" id="Company"
                        									name="Company" style="width: 100%" value="FMI" readonly>
                        								</input>
                        							</div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">Transaction Date</label>
                                    <div class="col-lg-3">
                                        <input type="date" class="form-control form-control-sm" id="Transactiondate" name="Transactiondate" value="@isset ($AdjustmentOut->Transactiondate) {{ $AdjustmentOut->Transactiondate }} @endisset"/>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">Warehouse Code</label>
                                    <div class="col-lg-5">
                                        <input type="text" class="form-control form-control-sm" id="WarehouseCode" name="WarehouseCode" placeholder="Enter WarehouseCode" value="@isset ($AdjustmentOut->WarehouseCode) {{ $AdjustmentOut->WarehouseCode }} @endisset" />
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">Ref No</label>
                                    <div class="col-lg-5">
                                        <input type="text" class="form-control form-control-sm" id="Refno" name="Refno" placeholder="Enter Refno" value="@isset ($AdjustmentOut->Refno) {{ $AdjustmentOut->Refno }} @endisset"/>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">Remark</label>
                                    <div class="col-lg-5">
                                        <textarea type="text" class="form-control form-control-sm" id="Remark" name="Remark" placeholder="Enter Remark" value="@isset ($AdjustmentOut->Remark) {{ $AdjustmentOut->Remark }} @endisset"/> @isset ($AdjustmentOut->Remark) {{ $AdjustmentOut->Remark }} @endisset</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-2">
                              <div class="form-group row mb-0">
                                  <label class="col-lg-4 col-form-label"></label>
                                  <div class="col-lg-8">
                                      <button type="submit" class="btn btn-success font-weight-bold">Save</button>
                                      <a type="button" class="btn btn-danger" href="{{ route('asset.adjustment-out.index') }}">Cancel</a>
                                  </div>
                              </div>
                            </div>
                            <div class="py-1">
                              <div class="card-custom mb-1">
                                  <div class="card-header border-0 pt-1 pb-1 pr-0 pl-0">
                                      <div class="card-toolbar float-left">
                                          <h3 class="card-title font-weight-bolder text-dark">Adjustment Out Item Detail</h3>
                                      </div>
                                      <div class="card-toolbar float-right">
                                          <a type="button" onclick="addService()" class="btn btn-warning btn-sm"><i class="fas fa-plus"></i> Add</a>
                                      </div>
                                    </div>
                                      <div class="card-body pr-0 pl-0 pt-1 pb-1 pr-0 pl-0">
                                        <table id="datatable" class="table dataTable table-bordered table-hover w100" cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <th class="text-center text-white dt-body-nowrap">ItemCode</th>
                                                    <th class="text-center text-white dt-body-nowrap">ReasonCode</th>
                                                    <th class="text-center text-white dt-body-nowrap">Quantity</th>
                                                    <th class="text-center text-white dt-body-nowrap">Remark</th>
                                                    <th class="text-center text-white dt-body-nowrap">Act</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                             @isset ($item_list)
                                               @foreach($item_list as $item)
                                                 <tr>
                                                    <td><input name="its_itemcode[]" class="form-input col-12" value="{{ $item->ItemCode }}"></td>
                                                    <td><input name="its_reasoncode[]" class="form-input col-12" value="{{ $item->ReasonCode }}"></td>
                                                    <td><input name="its_quantity[]" class="form-input col-12" value="{{ $item->Quantity }}"></td>
                                                    <td><input name="its_remark[]" class="form-input col-12" value="{{ $item->Remark }}"></td>
                                                    <td><a type="button" value="del" onclick="deleteRow(this)" class="pt-2"><i class="text-center text-danger fas fa-backspace"></i><a/></td>
                                                </tr>
                                             @endforeach
                                           @endisset
                                            </tbody>
                                        </table>
                                      </div>
                                  </div>
                                </div>
                                <div class="py-1">
                                  <div class="card-custom mb-1">
                                      <div class="card-header border-0 pt-1 pb-1 pr-0 pl-0">
                                          <div class="card-toolbar float-left">
                                              <h3 class="card-title font-weight-bolder text-dark">Serial No</h3>
                                          </div>
                                          <div class="card-toolbar float-right">
                                              <a type="button" onclick="Adjadd()" class="btn btn-warning btn-sm"><i class="fas fa-plus"></i> Add</a>
                                          </div>
                                        </div>
                                          <div class="card-body pr-0 pl-0 pt-1 pb-1 pr-0 pl-0">
                                            <table id="datatables" class="table dataTable table-bordered table-hover w100" cellspacing="0">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center text-white dt-body-nowrap">ItemCode</th>
                                                        <th class="text-center text-white dt-body-nowrap">Serial No</th>
                                                        <th class="text-center text-white dt-body-nowrap">Remark</th>
                                                        <th class="text-center text-white dt-body-nowrap">RemarkQuantity</th>
                                                        <th class="text-center text-white dt-body-nowrap">LocationCode</th>
                                                        <th class="text-center text-white dt-body-nowrap">Act</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                 @isset ($serial_list)
                                                   @foreach($serial_list as $serial)
                                                     <tr>
                                                        <td><input name="ins_itemcode[]" class="form-input col-12" value="{{ $serial->ItemCode }}"></td>
                                                        <td><input name="ins_serialno[]" class="form-input col-12" value="{{ $serial->SerialNo }}"></td>
                                                        <td><input name="ins_remarktext[]" class="form-input col-12" value="{{ $serial->RemarkText }}"></td>
                                                        <td><input name="ins_remarkquantity[]" class="form-input col-12" value="{{ $serial->RemarkQuantity }}"></td>
                                                        <td><input name="ins_locationcode[]" class="form-input col-12" value="{{ $serial->LocationCode }}"></td>
                                                        <td><a type="button" value="del" onclick="deleteRow(this)" class="pt-2"><i class="text-center text-danger fas fa-backspace"></i><a/></td>
                                                    </tr>
                                                 @endforeach
                                               @endisset
                                                </tbody>
                                            </table>
                                          </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    @endsection

{{-- Styles Section --}}
@section('styles')
<link rel="stylesheet" href="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.css">
<link rel="stylesheet" href="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.min.css">
<link rel="stylesheet" href="{{ config('app.url') }}plugins/custom/datatables/datatables.bundle.css">
<link rel="stylesheet" href="{{ config('app.url') }}css/inject.css">
@endsection

{{-- Scripts Section --}}
@section('scripts')
@include ('inc.confirm-delete-modal')
<script src="{{ config('app.url') }}global/vendor/datatables/jquery.dataTables.js"></script>
<script src="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.js"></script>
<script src="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.js"></script>
<script type="text/javascript">
 $(document).ready(function() {
     $('[data-switch=true]').bootstrapSwitch();
     $("#Branch").select2();
     if($("#Code").val() != ""){
       $("#Code").attr('readonly',true);
     }

 });

 function addService() {
   var table = document.getElementById("datatable").getElementsByTagName('tbody')[0];
   var row = table.insertRow(-1);
   var cell1 = row.insertCell(0);
   var cell2 = row.insertCell(1);
   var cell3 = row.insertCell(2);
   var cell4 = row.insertCell(3);
   var cell5 = row.insertCell(4);
   cell1.innerHTML = '<input name="its_itemcode[]" class="form-input col-12" value="">';
   cell2.innerHTML = '<input name="its_reasoncode[]" class="form-input col-12" value="">';
   cell3.innerHTML = '<input name="its_quantity[]" class="form-input col-12" value="">';
   cell4.innerHTML = '<input name="its_remark[]" class="form-input col-12" value="">';
   cell5.innerHTML = '<a type="button" value="del" onclick="deleteRow(this)" class="pt-2"><i class="text-center text-danger fas fa-backspace"></i><a/>';
 }

 function Adjadd() {
   var table = document.getElementById("datatables").getElementsByTagName('tbody')[0];
   var row = table.insertRow(-1);
   var cell1 = row.insertCell(0);
   var cell2 = row.insertCell(1);
   var cell3 = row.insertCell(2);
   var cell4 = row.insertCell(3);
   var cell5 = row.insertCell(4);
   var cell6 = row.insertCell(5);
   cell1.innerHTML = '<input name="ins_itemcode[]" class="form-input col-12" value="">';
   cell2.innerHTML = '<input name="ins_serialno[]" class="form-input col-12" value="">';
   cell3.innerHTML = '<input name="ins_remarktext[]" class="form-input col-12" value="">';
   cell4.innerHTML = '<input name="ins_remarkquantity[]" class="form-input col-12" value="">';
   cell5.innerHTML = '<input name="ins_locationcode[]" class="form-input col-12" value="">';
   cell6.innerHTML = '<a type="button" value="del" onclick="deleteRow(this)" class="pt-2"><i class="text-center text-danger fas fa-backspace"></i><a/>';
 }

     function deleteRow(btn) {
       var row = btn.parentNode.parentNode;
       row.parentNode.removeChild(row);
     }
</script>
@endsection
