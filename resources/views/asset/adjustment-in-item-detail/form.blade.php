@extends('layout.default')

@section('content')
<div class="subheader py-2 {{ Metronic::printClasses('subheader', false) }}" id="kt_subheader">
  <div class="{{ Metronic::printClasses('subheader-container', false) }} d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
    <div class="d-flex align-items-baseline flex-wrap mr-5">
      <h5 class="text-dark font-weight-bold my-1 mr-5">
        Adjustment In
      </h5>
      <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
        <li class="breadcrumb-item">
          <a href="{{ route('dashboard') }}" class="text-muted">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
          <a href="{{ route('asset.adjustment-in-item-detail.index') }}" class="text-muted">Asset</a>
        </li>
        <li class="breadcrumb-item">
          <a href="#form" class="text-muted">Form Input</a>
        </li>
      </ul>
    </div>
  </div>
</div>

@include('inc.error-list')
@include('inc.success-notif')
<div class="card card-custom">
  <div class="card-header bg-danger flex-wrap border-1 pt-1 pb-0" style="min-height: 0;">
    <div class="card-title pt-1 pb-0">
      <h3 class="card-label font-weight-bolder text-white">Form Input 
      </h3>
    </div>
  </div>
  <!-- <div class="card-body pt-1"> -->
    <form class="form pt-0" id="form-input" action="{{ route('asset.adjustment-in-item-detail.store') }}" method="POST">
        {!! csrf_field() !!}
        <input type="hidden" class="form-control form-control-sm" id="method" name="_method" placeholder="Enter method" value="POST"/>
        <div class="card-body pt-0 pl-3 pr-3">
            <div class="row">
                <div class="col-lg-8">
                    <div class="card card-custom mb-1 col-lg-12 ">
                        <div class="card-body pt-1">
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">Code</label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control form-control-sm" id="Code" name="Code" placeholder="Enter Code"/>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">Branch Code</label>
                                    <div class="col-lg-5">
                                         <input   class="form-control form-control-sm" id="BranchCode" name="BranchCode" readonly="" />
                                    </div>
                                    <div>
                                        <td>
                                                <select class="form-control form-control-sm" tabindex="-1" aria-hidden="true" style="width:100%">
                                                  <option value="Jakarta" selected>Jakarta</option>
                                                  <option value="Bandung" selected>Bandung</option>
                                                  <option value="Semarang" selected>Semarang</option>
                                                </select>
                                    </td>
                                  </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">Company Code</label>
                                    <div class="col-lg-5">
                                        <input type="text" class="form-control form-control-sm" id="CompanyCode" name="CompanyCode" placeholder="Enter CompanyCode" />
                                    </div>
                                    <div>
                                        <td>
                                                <select class="form-control form-control-sm" tabindex="-1" aria-hidden="true" style="width:100%">
                                                  <option value="FMI" selected>FMI</option>
                                                  <option value="NOC" selected>NOC</option>
                                                </select>
                                    </td>
                                  </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">Transaction Date</label>
                                    <div class="col-lg-3">
                                        <input type="date" class="form-control form-control-sm" id="Transactiondate" name="Transactiondate" />
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">Warehouse Code</label>
                                    <div class="col-lg-5">
                                        <input type="text" class="form-control form-control-sm" id="Warehousecode" name="Warehousecode" placeholder="Enter Warehousecode" />
                                    </div>
                                    <div>
                                        <td>
                                                <select class="form-control form-control-sm" tabindex="-1" aria-hidden="true" style="width:100%">
                                                  <option value="Pusat" selected>PST</option>
                                                  <option value="Barat" selected>BRT</option>
                                                  <option value="Selatan" selected>SLT</option>
                                                  <option value="Utara" selected>UTR</option>
                                                </select>
                                    </td>
                                  </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">Ref No</label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control form-control-sm" id="RefNo" name="RefNo" placeholder="Enter RefNo" />
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row mb-0">
                                    <label class="col-lg-4 col-form-label">Remark</label>
                                    <div class="col-lg-5">
                                        <textarea type="text" class="form-control form-control-sm" id="Remark" name="Remark" placeholder="Enter Remark"> </textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="py-1">
                              <div class="form-group row mb-0 pr-50">
                                  <h5 class="col-lg-4 font-weight-bolder"></h5>
                                  <div class="col-lg-8 col-form-label">
                                      <button type="#btnConfirmAdjustmentIn">Confirm</button>
                                  </div>
                              </div>
                            </div>
                            <div class="py-1">
<!-- 
                                <ul id="dynamic-list"></ul>

                                <input type="text" id="candidate"/>
                                <button type="button" onclick="addItem()">add item</button>
                                <button type="button" onclick="removeItem()">remove item</button> -->
                            <th class="text-center dt-body-nowrap">Adjustment In Item Detail</th>
                            <button type="" class="btn btn-success font-weight-bold">Search Item</button>
                                <table class="table table-bordered table-hover w100" cellspacing="0" id="datatable1" >
                                    <thead>
                                        <tr>
                                            <th class="text-center dt-body-nowrap">Item Code</th>
                                            <th class="text-center dt-body-nowrap">Item Name</th>
                                            <th class="text-center dt-body-nowrap">SN-Status</th>
                                            <th class="text-center dt-body-nowrap">Reason Code</th>
                                            <th class="text-center dt-body-nowrap">Reason Name</th>
                                            <th class="text-center dt-body-nowrap">Qantity</th>
                                            <th class="text-center dt-body-nowrap">UOM</th>
                                            <th class="text-center dt-body-nowrap">Remark</th>
                                            <th class="text-center dt-body-nowrap">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td> <input type="text"class="form-control form-control-sm" style="width:4rem"/> </td>
                                            <td> <input type="text"class="form-control form-control-sm" style="width:4rem"/> </td>
                                            <td> <input type="text"class="form-control form-control-sm" style="width:4rem" readonly/> </td>
                                            <td>
                                                <select class="form-control form-control-sm" tabindex="-1" aria-hidden="true" style="width:100%">
                                                  <option value="" selected>Opt 1</option>
                                                  <option value="" selected>Opt 1</option>
                                                  <option value="" selected>Opt 1</option>
                                                </select>
                                             </td>
                                            <td> <input type="text"class="form-control form-control-sm" style="width:4rem"/> </td>
                                            <td> <input type="text"class="form-control form-control-sm" style="width:4rem"/> </td>
                                            <td> <input type="button" style="width:5rem" value="del"/> </td>
                                        </tr>
                                        <tr>
                                            <td> <input type="text"class="form-control form-control-sm" style="width:4rem"/> </td>
                                            <td> <input type="text"class="form-control form-control-sm" style="width:4rem"/> </td>
                                            <td> <input type="text"class="form-control form-control-sm" style="width:4rem"/> </td>
                                            <td>
                                                <select class="form-control form-control-sm" tabindex="-1" aria-hidden="true" style="width:100%">
                                                  <option value="" selected>Opt 1</option>
                                                  <option value="" selected>Opt 1</option>
                                                  <option value="" selected>Opt 1</option>
                                                </select>
                                             </td>
                                            <td> <input type="text"class="form-control form-control-sm" style="width:4rem"/> </td>
                                            <td> <input type="text"class="form-control form-control-sm" style="width:4rem"/> </td>
                                            <td> <input type="button" style="width:5rem" value="del"/> </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                            
                            <th class="text-center dt-body-nowrap">Serial No.</th>
                            <button type="" class="btn btn-success font-weight-bold">Confirm</button>
                            
                            <table class="table table-bordered table-hover w100" cellspacing="0" id="datatable1" >
                                    <thead>
                                        <tr>
                                            <th class="text-center dt-body-nowrap">No.</th>
                                            <th class="text-center dt-body-nowrap">Item Code</th>
                                            <th class="text-center dt-body-nowrap">Item Name</th>
                                            <th class="text-center dt-body-nowrap">Serial No</th>
                                            <th class="text-center dt-body-nowrap">UOM</th>
                                            <th class="text-center dt-body-nowrap">Remark</th>
                                            <th class="text-center dt-body-nowrap">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td> <input type="text"class="form-control form-control-sm" style="width:4rem"/> </td>
                                            <td> <input type="text"class="form-control form-control-sm" style="width:4rem"/> </td>
                                            <td> <input type="text"class="form-control form-control-sm" style="width:4rem"/> </td>
                                            <td>
                                                <select class="form-control form-control-sm" tabindex="-1" aria-hidden="true" style="width:100%">
                                                  <option value="" selected>Opt 1</option>
                                                  <option value="" selected>Opt 1</option>
                                                  <option value="" selected>Opt 1</option>
                                                </select>
                                             </td>
                                            <td> <input type="text"class="form-control form-control-sm" style="width:4rem"/> </td>
                                            <td> <input type="text"class="form-control form-control-sm" style="width:4rem"/> </td>
                                            <td> <input type="button" style="width:5rem" value="del"/> </td>
                                        </tr>
                                        <tr>
                                            <td> <input type="text"class="form-control form-control-sm" style="width:4rem"/> </td>
                                            <td> <input type="text"class="form-control form-control-sm" style="width:4rem"/> </td>
                                            <td> <input type="text"class="form-control form-control-sm" style="width:4rem"/> </td>
                                            <td>
                                                <select class="form-control form-control-sm" tabindex="-1" aria-hidden="true" style="width:100%">
                                                  <option value="" selected>Opt 1</option>
                                                  <option value="" selected>Opt 1</option>
                                                  <option value="" selected>Opt 1</option>
                                                </select>
                                             </td>
                                            <td> <input type="text"class="form-control form-control-sm" style="width:4rem"/> </td>
                                            <td> <input type="text"class="form-control form-control-sm" style="width:4rem"/> </td>
                                            <td> <input type="button" style="width:5rem" value="del"/> </td>
                                        </tr>
                                  </tbody>
                          </table> 
                             <div class="mb-2">
                        <div class="form-group row mb-0">
                            <label class="col-lg-4 col-form-label"></label>
                            <div class="col-lg-8">
                                <button type="submit" class="btn btn-success font-weight-bold">Save</button>
                                <button type="button" class="btn btn-danger">Cancel</button>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
    </form>
  <!-- </div> -->
</div>

@endsection

{{-- Styles Section --}}
@section('styles')
<link rel="stylesheet" href="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.css">
<link rel="stylesheet" href="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.min.css">
<link rel="stylesheet" href="{{ config('app.url') }}plugins/custom/datatables/datatables.bundle.css">
<link rel="stylesheet" href="{{ config('app.url') }}css/inject.css">
@endsection

{{-- Scripts Section --}}
@section('scripts')
@include ('inc.confirm-delete-modal')
<script src="{{ config('app.url') }}global/vendor/datatables/jquery.dataTables.js"></script>
<script src="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.js"></script>
<script src="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.js"></script>
<script type="text/javascript">
    $("html").keydown(function(e){
      if(e.shiftKey && e.keyCode == 'N'.charCodeAt(0) && !e.altKey){
           show_data('');
       }
    });
    $(document).ready(function() {
      $("#datatable_wrapper").removeClass("dataTables_wrapper form-inline dt-bootstrap no-footer");
      $("#datatable_wrapper").addClass("dataTables_wrapper dt-bootstrap4 no-footer");
    });

    var table_service = $('#datatable1').dataTable({
      "searching":false,
      "paging":   false,
  		"ordering": false,
  		"info":     false
    });

    var counter = 1;
    $('#add-service').on( 'click', function () {
        table_service.row.add( [
           '<input type="text" id="R_' + counter + '_1" />',
        ] ).draw( false );

        counter++;
    } );

    $('#btn-auto-find').on( 'click', function () {
      $.ajax({
          url: "http://localhost:5000/api/config",
          type: "GET",
          success: function (datas) {
            var i=0;
            var data = "";
            for (i=0; i< datas["response"].length;i++){
              data += datas["response"][i]+"\n";
              i++;
            }
            alert("<pre>"+data+"</pre>");
          },
          error: function (xhr, status, error) {
            alert("<pre>"+xhr.status + " " + status + " " + error+"</pre>");
          }
      });
    } );
    
        $("#btnConfirmAdjustmentIn").on ('click', (function(ev) {
            if(!$("#frmAdjustmentInInput").valid()) {
                alertMessage("Field(s) Can't empty!");
                ev.preventDefault();
                return;
            }
            
            if($("#adjustmentInUpdateMode").val()==="true"){
                autoLoadDataAdjustmentInItemDetail();
            }
                
            $("#btnUnConfirmAdjustmentIn").val("display", "block");
            $("#btnConfirmAdjustmentIn").val("display", "none");
            $("#btnConfirmAdjustmentInDetail").val("display", "block");
            $("#btnUnConfirmAdjustmentInDetail").val("display", "none");    
            $("#div-header-adj-in").block({ message: null, overlayCSS:{ backgroundColor: '#000', opacity: 0.1, cursor: null}});
            $("#adjustmentInItemDetailInputGrid").unblock();
            $("#adjustmentInSerialNoDetailInputGrid").block({ message: null, overlayCSS:{ backgroundColor: '#000', opacity: 0.1, cursor: null}});
        });
    );

    $('[data-switch=true]').bootstrapSwitch();
   

//  function show_data(id = "") {
//      if (id !== "") {
//          $.ajax({
//              url: "{{ route('asset.adjustment-in.store')}}/" + id,
//              type: "GET",
//              headers: {
//                'X-CSRF-TOKEN': '{{ csrf_token() }}'
//              },
//              success: function (response) {
//                  $("#form-input").attr("action", "{{ route('asset.adjustment-in.update','')}}/"+id);
//                  $('#form-input').trigger("reset");
//                  $('#method').val("PUT");
//                  $('#Code').attr("readonly", true);
//                  $('#Code').val(response.data.Code);
//
//                  $('#BranchCode').val(response.data.BranchCode);
//                  $('#CompanyCode').val(response.data.CompanyCode);
//                  $('#Transactiondate').val(response.data.Transactiondate);
//                  $('#Warehousecode').val(response.data.Warehousecode);
//                  $('#RefNo').val(response.data.RefNo);
//                  $('#Remark').val(response.data.Remark);
//                  if (response.data.ActiveStatus === 1) {
//                      $('#ActiveStatus').bootstrapSwitch('state', true);
//                  } else {
//                      $('#ActiveStatus').bootstrapSwitch('state', false);
//                  }
//                  $('#modal-form').modal('show');
//                  $('#Code').focus();
//              },
//              error: function (xhr, status, error) {
//                  alert_show(xhr.status + " " + status + " " + error, false);
//              }
//          });
//      } else {
//          $("#form-input").attr("action", "{{ route('asset.adjustment-in.store')}}");
//          $('#method').val("POST");
//          $('#Code').attr("readonly", false);
//          $('#Code').focus();
//          $('#form-input').trigger("reset");
//          $('#modal-form').modal('show');
//          $('#Code').focus();
//      }
//  }

  function format_date(val) {
      var date_arr = val.split(" ");
      var time = date_arr[1];
      var date_id_arr = date_arr[0].split("-");
      var date_fix = date_id_arr[2] + "-" + date_id_arr[1] + "-" + date_id_arr[0];
      var date_transaction = date_fix + " " + time;
      return date_transaction;
  }

  function addItem(){
  	var ul = document.getElementById("dynamic-list");
    var candidate = document.getElementById("candidate");
    var li = document.createElement("li");
    li.setAttribute('id',candidate.value);
    li.appendChild(document.createTextNode(candidate.value));
    ul.appendChild(li);
  }

  function removeItem(){
  	var ul = document.getElementById("dynamic-list");
    var candidate = document.getElementById("candidate");
    var item = document.getElementById(candidate.value);
    ul.removeChild(item);
  }
</script>
@endsection
