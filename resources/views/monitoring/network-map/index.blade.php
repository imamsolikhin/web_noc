@extends('layout.default')

@section('content')
<div class="subheader py-2 {{ Metronic::printClasses('subheader', false) }}" id="kt_subheader">
  <div class="{{ Metronic::printClasses('subheader-container', false) }} d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
    <div class="d-flex align-items-baseline flex-wrap mr-5">
      <h5 class="text-dark font-weight-bold my-1 mr-5">
        Host
      </h5>
      <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
        <li class="breadcrumb-item">
          <a href="{{ route('dashboard') }}" class="text-muted">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
          <a href="{{ route('monitoring.network-map.index') }}" class="text-muted">Network Maping</a>
        </li>
        <li class="breadcrumb-item">
          <a href="#view" class="text-muted">Network Maping List</a>
        </li>
      </ul>
    </div>
  </div>
</div>

@include('inc.error-list')
@include('inc.success-notif')
<div class="card card-custom">
  <div class="card-header bg-danger flex-wrap border-1 pt-1 pb-0 mb-2" style="min-height: 0;">
    <div class="card-title pt-1 pb-1">
      <h3 class="card-label font-weight-bolder text-white">Network Maping
        <!-- <div class="text-muted pt-2 font-size-lg">show Datatable from table Network Maping Clients</div> -->
      </h3>
    </div>
    <!-- <div class="card-toolbar pt-1 pb-0">
      <a href="#" onclick="show_data('')" class="btn btn-primary font-weight-bolder" style="background-color: #1e1e2d;border-color: #0c8eff;">
        <span class="svg-icon svg-icon-md">
          <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
              <rect x="0" y="0" width="24" height="24"></rect>
              <circle fill="#000000" cx="9" cy="15" r="6"></circle>
              <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3"></path>
            </g>
          </svg>
        </span>Add New
      </a>
    </div> -->
  </div>
  <div class="card-body pt-1">
    <div id="ticketing_1"></div>
  </div>
</div>

@endsection

{{-- Styles Section --}}
@section('styles')
<link rel="stylesheet" href="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.css">
<link rel="stylesheet" href="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.min.css">
<link rel="stylesheet" href="{{ config('app.url') }}plugins/custom/datatables/datatables.bundle.css">
<link rel="stylesheet" href="{{ config('app.url') }}css/inject.css">
@endsection

{{-- Scripts Section --}}
@section('scripts')
@include ('inc.confirm-delete-modal')
<script src="{{ config('app.url') }}global/vendor/datatables/jquery.dataTables.js"></script>
<script src="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.js"></script>
<script src="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    var apexChart = "#ticketing_1";
    var options = {
        series: [{
                name: 'Jan',
                data: generateData(31, {
                    min: -30,
                    max: 55
                })
            },
            {
                name: 'Feb',
                data: generateData(31, {
                    min: -30,
                    max: 55
                })
            },
            {
                name: 'Mar',
                data: generateData(31, {
                    min: -30,
                    max: 55
                })
            },
            {
                name: 'Apr',
                data: generateData(31, {
                    min: -30,
                    max: 55
                })
            },
            {
                name: 'May',
                data: generateData(31, {
                    min: -30,
                    max: 55
                })
            },
            {
                name: 'Jun',
                data: generateData(31, {
                    min: -30,
                    max: 55
                })
            },
            {
                name: 'Jul',
                data: generateData(31, {
                    min: -30,
                    max: 55
                })
            },
            {
                name: 'Aug',
                data: generateData(31, {
                    min: -30,
                    max: 55
                })
            },
            {
                name: 'Sep',
                data: generateData(31, {
                    min: -30,
                    max: 55
                })
            }
        ],
        chart: {
            height: 350,
            type: 'heatmap',
        },
        plotOptions: {
            heatmap: {
                shadeIntensity: 0.5,
                colorScale: {
                    ranges: [{
                            from: -30,
                            to: 5,
                            name: 'low',
                            color: success
                        },
                        {
                            from: 6,
                            to: 20,
                            name: 'medium',
                            color: primary
                        },
                        {
                            from: 21,
                            to: 45,
                            name: 'high',
                            color: warning
                        },
                        {
                            from: 46,
                            to: 55,
                            name: 'extreme',
                            color: danger
                        }
                    ]
                }
            }
        },
        dataLabels: {
            enabled: false
        },
    };

    var chart = new ApexCharts(document.querySelector(apexChart), options);
    chart.render();


    function generateData(count, yrange) {
        var i = 0;
        var series = [];
        while (i < count) {
            var x = '' + (i + 1).toString();
            var y = Math.floor(Math.random() * (yrange.max - yrange.min + 1)) + yrange.min;

            series.push({
                x: x,
                y: y
            });
            i++;
        }
        return series;
    }

</script>
@endsection
