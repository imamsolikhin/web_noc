@extends('auth.master')

@section('head')
    @if (config('layout.header.self.theme') === 'light')
        @php $kt_logo_image = 'logo-fmi.jpg' @endphp
    @elseif (config('layout.header.self.theme') === 'dark')
        @php $kt_logo_image = 'logo-fmi.jpg' @endphp
    @endif
    <title>Sign In | {{ config('app.name') }}</title>

    <meta itemprop="name" content="Sign In | {{ config('app.name') }}">
    <meta itemprop="description" content="Sign In | {{ config('app.name') }}">
    <meta itemprop="image" content="{{ asset('media/logos/'.$kt_logo_image) }}">

    <meta name="twitter:card" content="summary">

    <meta property="og:title" content="Sign In | {{ config('app.name') }}" />
    <meta property="og:description" content="Sign In | {{ config('app.name') }}" />
    <meta property="og:url" content="http://oltman.tachyon.net.id/login" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="{{ asset('media/logos/'.$kt_logo_image) }}" />
@endsection

@section('content')
  <div class="d-flex flex-column flex-root">
      <div class="login login-1 login-signin-on d-flex flex-column flex-lg-row flex-row-fluid bg-white" id="kt_login">
          <div class="login-aside d-flex flex-row-auto bgi-size-cover bgi-no-repeat p-10 p-lg-40" style="background-image:url({{APP_LOGIN}})">
              <div class="d-flex flex-row-fluid flex-column justify-content-between">
                  <div class="flex-column-fluid d-flex flex-column justify-content-center">
                      <h3 class="font-size-h1 mb-5 text-white">{{APP_NAME}}</h3>
                      <p class="font-weight-lighter text-white opacity-80">-.</p>
                  </div>
                  <div class="d-none flex-column-auto d-lg-flex justify-content-between mt-10">
                      <!-- <div class="opacity-70 font-weight-bold text-white">Powered by DEVJR 2020</div> -->
                  </div>
              </div>
          </div>
          <div class="flex-row-fluid d-flex flex-column position-relative p-1 overflow-hidden">
              <div class="d-flex flex-column-fluid flex-center mt-30 mt-lg-0">
                  <div class="login-form login-signin col-md-6 col-md-6">
                      <div class="text-center mb-0 mb-lg-6">
                        <h3 class="font-size-h1"><b>LOGIN NOC</b></h3>
                      </div>
                      <form class="form" method="POST" action="{{ route('login') }}">
                          {{ csrf_field() }}

                          @if (Session::has('notif_error'))
                              @component('inc.alert')
                                  {{ Session::get('notif_error') }}
                              @endcomponent
                          @endif
                          @include('inc.error-list')
                          @include('inc.success-notif')

                          <div class="form-group">
                              <input style="font-weight: 900;" class="form-control h-auto text-dark placeholder-white opacity-70 bg-dark-o-50 rounded-pill border-0 py-4 px-8 mb-5" type="text" placeholder="username" name="username" value="{{ old('email') }}" required autocomplete="off"/>
                          </div>
                          <div class="form-group">
                              <input style="font-weight: 900;" class="form-control h-auto text-dark placeholder-white opacity-70 bg-dark-o-50 rounded-pill border-0 py-4 px-8 mb-5" type="password" placeholder="password" name="password" required autocomplete="off"/>
                              <div class="checkbox">
                                  <label class="checkbox checkbox-outline checkbox-blue text-dark m-0">
                                  <input type="checkbox" name="remember"/>
                                  <span></span>
                                  &nbsp;&nbsp;Remember me to login system
                                  </label>
                              </div>
                          </div>
                          <div class="form-group d-flex flex-wrap justify-content-between align-items-center">
                              <div class="col-md-12 row">
                                  <div class="col-4">
                                      <div class="form-group row">
                                          <button type="submit" href="javascript:void(0)" class="btn btn-primary font-weight-bold px-9 py-2 my-2">Login</button>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </form>
                  </div>
              </div>
              <div class="d-flex d-lg-none flex-column-auto flex-column flex-sm-row justify-content-between align-items-center mt-5 p-5">
                  <div class="text-dark-50 font-weight-bold order-2 order-sm-1 my-2">� 2020 TEAM&nbsp;&nbsp;R&D</div>
              </div>
          </div>
      </div>
  </div>
@endsection
