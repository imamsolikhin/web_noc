@extends('layout.default')

@section('content')
    <div class="subheader py-2 {{ Metronic::printClasses('subheader', false) }}" id="kt_subheader">
        <div class="{{ Metronic::printClasses('subheader-container', false) }} d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-baseline flex-wrap mr-5">
                <h5 class="text-dark font-weight-bold my-1 mr-5">
                    Edit Product                                 
                </h5>
                <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                    <li class="breadcrumb-item">
                        <a href="{{ route('dashboard') }}" class="text-muted">Home</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="{{ route('master.product.index') }}" class="text-muted">Product</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="" class="text-muted">Edit Product</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="card card-custom mb-0">
	    <form class="form" action="{{ route('master.product.update', $product->Code) }}" method="POST">
	        {!! csrf_field() !!}
	        {!! method_field('PUT') !!}
	        <div class="card-header border-1 pt-6 pb-0">
	            <div class="card-title">
	                <h3 class="card-label">Edit Product
	                </h3>
	            </div>
	        </div>
	        <div class="card-body">
	        	<div class="mb-1">
		            <div class="form-group row mb-0">
		                <div class="col-lg-4">
		                    <label>Code</label>
		                    <input type="text" class="form-control form-control-sm" name="Code" value="{{ old('Code', $product->Code) }}" placeholder="Enter Code" disabled />
		                </div>
		                <div class="col-lg-8">
		                    <label>Name</label>
		                    <input type="text" class="form-control form-control-sm" name="Name" value="{{ old('Name', $product->name) }}" placeholder="Enter Name" />
		                </div>
		            </div>
	        	</div>
	        </div>
	        <div class="card-footer">
	            <div class="row">
	                <div class="col-lg-6">
	                    <a href="{{ route('master.product.index') }}" id="cancel-btn" class="btn btn-danger">Cancel</a>
	                </div>
	                <div class="col-lg-6 text-right">
	                    <button type="submit" class="btn btn-primary mr-2">Update</button>
	                </div>
	            </div>
	        </div>
	    </form>
    </div>
@endsection