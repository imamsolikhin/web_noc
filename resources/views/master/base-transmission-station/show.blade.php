@extends('layout.default')

@section('content')
    <div class="subheader py-2 {{ Metronic::printClasses('subheader', false) }}" id="kt_subheader">
        <div class="{{ Metronic::printClasses('subheader-container', false) }} d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-baseline flex-wrap mr-5">
                <h5 class="text-dark font-weight-bold my-1 mr-5">
                    Detail Base Transmission Station                                 
                </h5>
                <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                    <li class="breadcrumb-item">
                        <a href="{{ route('dashboard') }}" class="text-muted">Home</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="{{ route('master.base-transmission-station.index') }}" class="text-muted">Base Transmission Station</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="" class="text-muted">Detail Base Transmission Station</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="card card-custom mb-0">
	    <div class="card-header border-1 pt-6 pb-0">
	        <div class="card-title">
	            <h3 class="card-label">Detail Base Transmission Station
	            </h3>
	        </div>
	    </div>
        <div class="card-body">
        	<div class="row">
        		<div class="col-lg-6">
        			<div class="card card-custom mb-1 col-lg-12 ">
                        <div class="card-header border-0">
                            <h3 class="card-title font-weight-bolder text-dark">Information</h3>
                        </div>
                        <div class="card-body pt-0">
                        	<div class="mb-1">
                        		<div class="form-group row mb-0">
                        			<div class="col-lg-12">
                        				<label>Code</label>
                        				<input type="text" class="form-control form-control-sm" name="Code" value="{{ old('Code', $baseTransmissionStation->Code) }}" placeholder="Enter Code" / disabled>
                        			</div>
                        		</div>
                        	</div>
                        	<div class="mb-1">
                        		<div class="form-group row mb-0">
                        			<div class="col-lg-12">
                        				<label>Name</label>
                        				<input type="text" class="form-control form-control-sm" name="Name" value="{{ old('Name', $baseTransmissionStation->Name) }}" placeholder="Enter Name" / disabled>
                        			</div>
                        		</div>
                        	</div>
                        	<div class="mb-1">
                        		<div class="form-group row mb-0">
                        			<div class="col-lg-12">
                        				<label>Default Bill To Code</label>
                        				<input type="text" class="form-control form-control-sm" name="DefaultBillToCode" value="{{ old('DefaultBillToCode', $baseTransmissionStation->DefaultBillToCode) }}" placeholder="Enter DefaultBillToCode" disabled />
                        			</div>
                        		</div>
                        	</div>
                        	<div class="mb-1">
                        		<div class="form-group row mb-0">
                        			<div class="col-lg-12">
                        				<label>Default Ship To Code</label>
                        				<input type="text" class="form-control form-control-sm" name="DefaultShipToCode" value="{{ old('DefaultShipToCode', $baseTransmissionStation->DefaultShipToCode) }}" placeholder="Enter DefaultShipToCode" disabled />
                        			</div>
                        		</div>
                        	</div>
                        	<div class="mb-1">
                        		<div class="form-group row mb-0">
                        			<div class="col-lg-12">
                        				<label>Remark</label>
                        				<textarea class="form-control form-control-sm" rows="5" name="Remark" disabled>{{ old('Remark', $baseTransmissionStation->Remark) }}</textarea>
                        			</div>
                        		</div>
                        	</div>
                        	<div class="mb-1">
                        		<div class="form-group row mb-0">
                        			<div class="col-lg-12">
                        				<label>Active Status</label>
										<span class="switch switch-primary">
											<label>
												<input type="checkbox" name="ActiveStatus"  value="1" {{ old('ActiveStatus', $baseTransmissionStation->ActiveStatus) == 1 ? 'checked' : '' }} disabled/>
												<span></span>
											</label>
										</span>
                        			</div>
                        		</div>
                        	</div>
                        </div>
        			</div>
        		</div>
        		<div class="col-lg-6">
        			<div class="card card-custom mb-1 col-lg-12 ">
                        <div class="card-header border-0">
                            <h3 class="card-title font-weight-bolder text-dark">Contact</h3>
                        </div>
                        <div class="card-body pt-0">
                        	<div class="mb-1">
                        		<div class="form-group row mb-0">
                        			<div class="col-lg-12">
                        				<label>Address <code>Optional</code> </label>
                        				<textarea class="form-control form-control-sm" rows="5" name="Address" disabled>{{ old('Address', $baseTransmissionStation->Address) }}</textarea>
                        			</div>
                        		</div>
                        	</div>
                        	<div class="mb-1">
                        		<div class="form-group row mb-0">
                        			<div class="col-lg-12">
                        				<label>City Code <code>Optional</code></label>
                        				<input type="text" class="form-control form-control-sm" name="CityCode" value="{{ old('CityCode', $baseTransmissionStation->CityCode) }}" placeholder="Enter CityCode" disabled />
                        			</div>
                        		</div>
                        	</div>
                        	<div class="mb-1">
                        		<div class="form-group row mb-0">
                        			<div class="col-lg-12">
                        				<label>Zip Code<code>Optional</code></label>
                        				<input type="text" class="form-control form-control-sm" name="ZipCode" value="{{ old('ZipCode', $baseTransmissionStation->ZipCode) }}" placeholder="Enter ZipCode" disabled />
                        			</div>
                        		</div>
                        	</div>
                        	<div class="mb-1">
                        		<div class="form-group row mb-0">
                        			<div class="col-lg-12">
                        				<label>Phone1<code>Optional</code></label>
                        				<input type="text" class="form-control form-control-sm" name="Phone1" value="{{ old('Phone1', $baseTransmissionStation->Phone1) }}" placeholder="Enter Phone1" disabled />
                        			</div>
                        		</div>
                        	</div>
                        	<div class="mb-1">
                        		<div class="form-group row mb-0">
                        			<div class="col-lg-12">
                        				<label>Phone2<code>Optional</code></label>
                        				<input type="text" class="form-control form-control-sm" name="Phone2" value="{{ old('Phone2', $baseTransmissionStation->Phone2) }}" placeholder="Enter Phone2" disabled />
                        			</div>
                        		</div>
                        	</div>
                        	<div class="mb-1">
                        		<div class="form-group row mb-0">
                        			<div class="col-lg-12">
                        				<label>Fax<code>Optional</code></label>
                        				<input type="text" class="form-control form-control-sm" name="Fax" value="{{ old('Fax', $baseTransmissionStation->Fax) }}" placeholder="Enter Fax" disabled />
                        			</div>
                        		</div>
                        	</div>
                        	<div class="mb-1">
                        		<div class="form-group row mb-0">
                        			<div class="col-lg-12">
                        				<label>Email Address<code>Optional</code></label>
                        				<input type="email" class="form-control form-control-sm" name="EmailAddress" value="{{ old('EmailAddress', $baseTransmissionStation->EmailAddress) }}" placeholder="Enter EmailAddress" disabled />
                        			</div>
                        		</div>
                        	</div>
                        	<div class="mb-1">
                        		<div class="form-group row mb-0">
                        			<div class="col-lg-12">
                        				<label>Contact Person<code>Optional</code></label>
                        				<input type="text" class="form-control form-control-sm" name="ContactPerson" value="{{ old('ContactPerson', $baseTransmissionStation->ContactPerson) }}" placeholder="Enter ContactPerson" disabled />
                        			</div>
                        		</div>
                        	</div>
                        </div>
        			</div>
        		</div>
        	</div>
        </div>
	    <div class="card-footer">
	        <div class="row">
	            <div class="col-lg-6">
	                <a href="{{ route('master.base-transmission-station.index') }}" id="cancel-btn" class="btn btn-danger">Back</a>
	            </div>
	            <div class="col-lg-6 text-right">
	                <a href="{{ route('master.base-transmission-station.edit', $baseTransmissionStation->Code) }}" class="btn btn-primary mr-2">Edit</a>
	            </div>
	        </div>
	    </div>
    </div>
@endsection