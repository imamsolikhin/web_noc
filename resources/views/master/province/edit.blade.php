@extends('layout.default')

@section('content')
    <div class="subheader py-2 {{ Metronic::printClasses('subheader', false) }}" id="kt_subheader">
        <div class="{{ Metronic::printClasses('subheader-container', false) }} d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-baseline flex-wrap mr-5">
                <h5 class="text-dark font-weight-bold my-1 mr-5">
                    Edit Province                                 
                </h5>
                <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                    <li class="breadcrumb-item">
                        <a href="{{ route('dashboard') }}" class="text-muted">Home</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="{{ route('master.province.index') }}" class="text-muted">Province</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="" class="text-muted">Edit Province</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="card card-custom mb-0">
	    <form class="form" action="{{ route('master.province.update', $province->Code) }}" method="POST">
	        {!! csrf_field() !!}
	        {!! method_field('PUT') !!}
	        <div class="card-header border-1 pt-6 pb-0">
	            <div class="card-title">
	                <h3 class="card-label">Edit Province
	                </h3>
	            </div>
	        </div>
	        <div class="card-body">
	        	<div class="mb-1">
		            <div class="form-group row mb-0">
		                <div class="col-lg-4">
		                    <label>Code</label>
		                    <input type="text" class="form-control form-control-sm" name="Code" value="{{ old('Code', $province->Code) }}" placeholder="Enter Code" disabled />
		                </div>
		                <div class="col-lg-8">
		                    <label>Name</label>
		                    <input type="text" class="form-control form-control-sm" name="Name" value="{{ old('Name', $province->Name) }}" placeholder="Enter Name" required />
		                </div>
		            </div>
	        	</div>
	        	<div class="mb-1">
		            <div class="form-group row mb-0">
		                <div class="col-lg-12">
		                    <label>Remark <code>Optional</code></label>
		                    <textarea class="form-control form-control-sm" rows="5" name="Remark" >{{ $province->Remark }}</textarea>
		                </div>
		            </div>
	        	</div>
	        	<div class="mb-1">
		            <div class="form-group row mb-0">
		                <div class="col-lg-6">
		                    <label>Island Code</label>
		                    <input type="text" class="form-control form-control-sm" name="IslandCode" value="{{ old('IslandCode', $province->IslandCode) }}" placeholder="Enter Island Code"  />
		                </div>
		                <div class="col-lg-6">
	            			<div class="col-lg-12">
	            				<label>Active Status</label>
								<span class="switch switch-primary">
									<label>
										<input type="checkbox" name="ActiveStatus"  value="1" {{ old('ActiveStatus', $province->ActiveStatus) == 1 ? 'checked' : '' }} />
										<span></span>
									</label>
								</span>
	            			</div>
		                </div>
		            </div>
	        	</div>
	        </div>
	        <div class="card-footer">
	            <div class="row">
	                <div class="col-lg-6">
	                    <a href="{{ route('master.province.index') }}" id="cancel-btn" class="btn btn-danger">Cancel</a>
	                </div>
	                <div class="col-lg-6 text-right">
	                    <button type="submit" class="btn btn-primary mr-2">Update</button>
	                </div>
	            </div>
	        </div>
	    </form>
    </div>
@endsection