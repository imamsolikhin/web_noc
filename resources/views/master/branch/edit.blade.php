@extends('layout.default')

@section('content')
    <div class="subheader py-2 {{ Metronic::printClasses('subheader', false) }}" id="kt_subheader">
        <div class="{{ Metronic::printClasses('subheader-container', false) }} d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-baseline flex-wrap mr-5">
                <h5 class="text-dark font-weight-bold my-1 mr-5">
                    Edit Branch                                 
                </h5>
                <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                    <li class="breadcrumb-item">
                        <a href="{{ route('dashboard') }}" class="text-muted">Home</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="{{ route('master.branch.index') }}" class="text-muted">Branch</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="" class="text-muted">Edit Branch</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="card card-custom mb-0">
	    <form class="form" action="{{ route('master.branch.update', $branch->Code) }}" method="POST">
	        {!! csrf_field() !!}
	        {!! method_field('PUT') !!}
	        <div class="card-header border-1 pt-6 pb-0">
	            <div class="card-title">
	                <h3 class="card-label">Edit Branch
	                </h3>
	            </div>
	        </div>
	        <div class="card-body">
	        	<div class="row">
	        		<div class="col-lg-6">
	        			<div class="card card-custom mb-1 col-lg-12 ">
	                        <div class="card-header border-0">
	                            <h3 class="card-title font-weight-bolder text-dark">Information</h3>
	                        </div>
	                        <div class="card-body pt-0">
	                        	<div class="mb-1">
	                        		<div class="form-group row mb-0">
	                        			<div class="col-lg-12">
	                        				<label>Code</label>
	                        				<input type="text" class="form-control form-control-sm" name="Code" value="{{ old('Code', $branch->Code) }}" placeholder="Enter Code" / disabled>
	                        			</div>
	                        		</div>
	                        	</div>
	                        	<div class="mb-1">
	                        		<div class="form-group row mb-0">
	                        			<div class="col-lg-12">
	                        				<label>Name</label>
	                        				<input type="text" class="form-control form-control-sm" name="Name" value="{{ old('Name', $branch->Name) }}" placeholder="Enter Name" / required>
	                        			</div>
	                        		</div>
	                        	</div>
	                        	<div class="mb-1">
	                        		<div class="form-group row mb-0">
	                        			<div class="col-lg-12">
	                        				<label>Default Bill To Code</label>
	                        				<input type="text" class="form-control form-control-sm" name="DefaultBillToCode" value="{{ old('DefaultBillToCode', $branch->DefaultBillToCode) }}" placeholder="Enter DefaultBillToCode"  />
	                        			</div>
	                        		</div>
	                        	</div>
	                        	<div class="mb-1">
	                        		<div class="form-group row mb-0">
	                        			<div class="col-lg-12">
	                        				<label>Default Ship To Code</label>
	                        				<input type="text" class="form-control form-control-sm" name="DefaultShipToCode" value="{{ old('DefaultShipToCode', $branch->DefaultShipToCode) }}" placeholder="Enter DefaultShipToCode"  />
	                        			</div>
	                        		</div>
	                        	</div>
	                        	<div class="mb-1">
	                        		<div class="form-group row mb-0">
	                        			<div class="col-lg-12">
	                        				<label>Remark</label>
	                        				<textarea class="form-control form-control-sm" rows="5" name="Remark" >{{ old('Remark', $branch->Remark) }}</textarea>
	                        			</div>
	                        		</div>
	                        	</div>
	                        	<div class="mb-1">
	                        		<div class="form-group row mb-0">
	                        			<div class="col-lg-12">
	                        				<label>Active Status</label>
											<span class="switch switch-primary">
												<label>
													<input type="checkbox" name="ActiveStatus"  value="1" {{ old('ActiveStatus', $branch->ActiveStatus) == 1 ? 'checked' : '' }} />
													<span></span>
												</label>
											</span>
	                        			</div>
	                        		</div>
	                        	</div>
	                        </div>
	        			</div>
	        		</div>
	        		<div class="col-lg-6">
	        			<div class="card card-custom mb-1 col-lg-12 ">
	                        <div class="card-header border-0">
	                            <h3 class="card-title font-weight-bolder text-dark">Contact</h3>
	                        </div>
	                        <div class="card-body pt-0">
	                        	<div class="mb-1">
	                        		<div class="form-group row mb-0">
	                        			<div class="col-lg-12">
	                        				<label>Address <code>Optional</code> </label>
	                        				<textarea class="form-control form-control-sm" rows="5" name="Address" >{{ old('Address', $branch->Address) }}</textarea>
	                        			</div>
	                        		</div>
	                        	</div>
	                        	<div class="mb-1">
	                        		<div class="form-group row mb-0">
	                        			<div class="col-lg-12">
	                        				<label>City Code <code>Optional</code></label>
	                        				<input type="text" class="form-control form-control-sm" name="CityCode" value="{{ old('CityCode', $branch->CityCode) }}" placeholder="Enter CityCode"  />
	                        			</div>
	                        		</div>
	                        	</div>
	                        	<div class="mb-1">
	                        		<div class="form-group row mb-0">
	                        			<div class="col-lg-12">
	                        				<label>Zip Code<code>Optional</code></label>
	                        				<input type="text" class="form-control form-control-sm" name="ZipCode" value="{{ old('ZipCode', $branch->ZipCode) }}" placeholder="Enter ZipCode"  />
	                        			</div>
	                        		</div>
	                        	</div>
	                        	<div class="mb-1">
	                        		<div class="form-group row mb-0">
	                        			<div class="col-lg-12">
	                        				<label>Phone1<code>Optional</code></label>
	                        				<input type="text" class="form-control form-control-sm" name="Phone1" value="{{ old('Phone1', $branch->Phone1) }}" placeholder="Enter Phone1"  />
	                        			</div>
	                        		</div>
	                        	</div>
	                        	<div class="mb-1">
	                        		<div class="form-group row mb-0">
	                        			<div class="col-lg-12">
	                        				<label>Phone2<code>Optional</code></label>
	                        				<input type="text" class="form-control form-control-sm" name="Phone2" value="{{ old('Phone2', $branch->Phone2) }}" placeholder="Enter Phone2"  />
	                        			</div>
	                        		</div>
	                        	</div>
	                        	<div class="mb-1">
	                        		<div class="form-group row mb-0">
	                        			<div class="col-lg-12">
	                        				<label>Fax<code>Optional</code></label>
	                        				<input type="text" class="form-control form-control-sm" name="Fax" value="{{ old('Fax', $branch->Fax) }}" placeholder="Enter Fax"  />
	                        			</div>
	                        		</div>
	                        	</div>
	                        	<div class="mb-1">
	                        		<div class="form-group row mb-0">
	                        			<div class="col-lg-12">
	                        				<label>Email Address<code>Optional</code></label>
	                        				<input type="email" class="form-control form-control-sm" name="EmailAddress" value="{{ old('EmailAddress', $branch->EmailAddress) }}" placeholder="Enter EmailAddress"  />
	                        			</div>
	                        		</div>
	                        	</div>
	                        	<div class="mb-1">
	                        		<div class="form-group row mb-0">
	                        			<div class="col-lg-12">
	                        				<label>Contact Person<code>Optional</code></label>
	                        				<input type="text" class="form-control form-control-sm" name="ContactPerson" value="{{ old('ContactPerson', $branch->ContactPerson) }}" placeholder="Enter ContactPerson"  />
	                        			</div>
	                        		</div>
	                        	</div>
	                        </div>
	        			</div>
	        		</div>
	        	</div>
	        </div>
	        <div class="card-footer">
	            <div class="row">
	                <div class="col-lg-6">
	                    <a href="{{ route('master.branch.index') }}" id="cancel-btn" class="btn btn-danger">Cancel</a>
	                </div>
	                <div class="col-lg-6 text-right">
	                    <button type="submit" class="btn btn-primary mr-2">Update</button>
	                </div>
	            </div>
	        </div>
	    </form>
    </div>
@endsection