@extends('layout.default')

@section('content')
    <div class="subheader py-2 {{ Metronic::printClasses('subheader', false) }}" id="kt_subheader">
        <div class="{{ Metronic::printClasses('subheader-container', false) }} d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-baseline flex-wrap mr-5">
                <h5 class="text-dark font-weight-bold my-1 mr-5">
                    Edit Customer NOC                                 
                </h5>
                <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                    <li class="breadcrumb-item">
                        <a href="{{ route('dashboard') }}" class="text-muted">Home</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="{{ route('master.customer-noc.index') }}" class="text-muted">Bandwidth Package</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="" class="text-muted">Edit Customer NOC</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    @include('inc.error-list')
    <div class="card card-custom mb-0">
	    <form class="form" action="{{ route('master.customer-noc.update', $customerNoc->Code) }}" method="POST">
	        {!! csrf_field() !!}
	        {!! method_field('PUT') !!}
	        <div class="card-header border-1 pt-6 pb-0">
	            <div class="card-title">
	                <h3 class="card-label">Edit Customer NOC
	                </h3>
	            </div>
	        </div>
	        <div class="card-body">
	        	<div class="mb-1">
		            <div class="form-group row mb-0">
		                <div class="col-lg-12">
		                    <label>Code</label>
		                    <input type="text" class="form-control form-control-sm" name="Code" value="{{ old('Code', $customerNoc->Code) }}" placeholder="Enter Code" disabled />
		                </div>

		            </div>
	        	</div>
	        	<div class="mb-1">
		            <div class="form-group row mb-0">
		                <div class="col-lg-4">
		                    <label>Local AP</label>
		                    <input type="text" class="form-control form-control-sm" name="IPLocalAP" value="{{ old('IPLocalAP', $customerNoc->IPLocalAP) }}" placeholder="xxx.xxx.xxx" />
		                </div>
		                <div class="col-lg-4">
		                    <label>Local CPE</label>
		                    <input type="text" class="form-control form-control-sm" name="IPLocalCPE" value="{{ old('IPLocalCPE', $customerNoc->IPLocalCPE) }}" placeholder="xxx.xxx.xxx" />
		                </div>
		                <div class="col-lg-4">
		                    <label>Public</label>
		                    <input type="text" class="form-control form-control-sm" name="IPPublic" value="{{ old('IPPublic', $customerNoc->IPPublic) }}" placeholder="xxx.xxx.xxx" />
		                </div>
		            </div>
	        	</div>
	        	<div class="mb-1">
		            <div class="form-group row mb-0">
		                <div class="col-lg-4">
		                    <label>NI BTS Code</label>
		                    <input type="text" class="form-control form-control-sm" name="NIBTSCode" value="{{ old('NIBTSCode', $customerNoc->NIBTSCode) }}" placeholder="xxx.xxx.xxx" />
		                </div>
		                <div class="col-lg-4">
		                    <label>NI VLANID</label>
		                    <input type="text" class="form-control form-control-sm" name="NIVLANID" value="{{ old('NIVLANID', $customerNoc->NIVLANID) }}" placeholder="xxx.xxx.xxx" />
		                </div>
		                <div class="col-lg-4">
		                    <label>Avl AP</label>
		                    <input type="text" class="form-control form-control-sm" name="AvlAP" value="{{ old('AvlAP', $customerNoc->AvlAP) }}" placeholder="xxx.xxx.xxx" />
		                </div>
		            </div>
	        	</div>
	        	<div class="mb-1">
		            <div class="form-group row mb-0">
		                <div class="col-lg-6">
		                    <label>Avl CPE</label>
		                    <input type="text" class="form-control form-control-sm" name="AvlCPE" value="{{ old('AvlCPE', $customerNoc->AvlCPE) }}" placeholder="Enter AvlCPE" />
		                </div>
		                <div class="col-lg-6">
		                    <label>Avl Router</label>
		                    <input type="text" class="form-control form-control-sm" name="AvlRouter" value="{{ old('AvlRouter', $customerNoc->AvlRouter) }}" placeholder="Enter AvlRouter" />
		                </div>
		            </div>
	        	</div>
	        	<div class="mb-1">
		            <div class="form-group row mb-0">
		                <div class="col-lg-4">
		                    <label>Border1 Code</label>
		                    <input type="text" class="form-control form-control-sm" name="Border1Code" value="{{ old('Border1Code', $customerNoc->Border1Code) }}" placeholder="Enter Border1Code" />
		                </div>
		                <div class="col-lg-4">
		                    <label>Border2 Code</label>
		                    <input type="text" class="form-control form-control-sm" name="Border2Code" value="{{ old('Border2Code', $customerNoc->Border2Code) }}" placeholder="Enter Border2Code" />
		                </div>
		                <div class="col-lg-4">
		                    <label>Border3 Code</label>
		                    <input type="text" class="form-control form-control-sm" name="Border3Code" value="{{ old('Border3Code', $customerNoc->Border3Code) }}" placeholder="Enter Border3Code" />
		                </div>
		            </div>
	        	</div>
	        	<div class="mb-1">
		            <div class="form-group row mb-0">
		                <div class="col-lg-4">
		                    <label>Distribution Code</label>
		                    <input type="text" class="form-control form-control-sm" name="DistributionCode" value="{{ old('DistributionCode', $customerNoc->DistributionCode) }}" placeholder="Enter DistributionCode" />
		                </div>
		                <div class="col-lg-4">
		                    <label>Consentrator Code</label>
		                    <input type="text" class="form-control form-control-sm" name="ConsentratorCode" value="{{ old('ConsentratorCode', $customerNoc->ConsentratorCode) }}" placeholder="Enter Consentrator Code" />
		                </div>
		                <div class="col-lg-4">
		                    <label>BTS Code</label>
		                    <input type="text" class="form-control form-control-sm" name="BTSCode" value="{{ old('BTSCode', $customerNoc->BTSCode) }}" placeholder="Enter BTS Code" />
		                </div>
		            </div>
	        	</div>
	        	<div class="mb-1">
		            <div class="form-group row mb-0">
		                <div class="col-lg-4">
		                    <label>Shaper1 Code</label>
		                    <input type="text" class="form-control form-control-sm" name="Shaper1Code" value="{{ old('Shaper1Code', $customerNoc->Shaper1Code) }}" placeholder="Enter Shaper1 Code" />
		                </div>
		                <div class="col-lg-4">
		                    <label>Shaper2 Code</label>
		                    <input type="text" class="form-control form-control-sm" name="Shaper2Code" value="{{ old('Shaper2Code', $customerNoc->Shaper2Code) }}" placeholder="Enter Shaper2 Code" />
		                </div>
		                <div class="col-lg-4">
		                    <label>Shaper3 Code</label>
		                    <input type="text" class="form-control form-control-sm" name="Shaper3Code" value="{{ old('Shaper3Code', $customerNoc->Shaper3Code) }}" placeholder="Enter Shaper3 Code" />
		                </div>
		            </div>
	        	</div>
	        	<div class="mb-1">
		            <div class="form-group row mb-0">
		                <div class="col-lg-6">
		                    <label>VLAN ID</label>
		                    <input type="text" class="form-control form-control-sm" name="VLANID" value="{{ old('VLANID', $customerNoc->VLANID) }}" placeholder="Enter VLAN ID" />
		                </div>
		                <div class="col-lg-6">
		                    <label>VMAN ID</label>
		                    <input type="text" class="form-control form-control-sm" name="VMANID" value="{{ old('VMANID', $customerNoc->VMANID) }}" placeholder="Enter VMAN ID" />
		                </div>
		            </div>
	        	</div>
	        </div>
	        <div class="card-footer">
	            <div class="row">
	                <div class="col-lg-6">
	                    <a href="{{ route('master.customer-noc.index') }}" id="cancel-btn" class="btn btn-danger">Cancel</a>
	                </div>
	                <div class="col-lg-6 text-right">
	                    <button type="submit" class="btn btn-primary mr-2">Update</button>
	                </div>
	            </div>
	        </div>
	    </form>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#CityCode').select2({
                'placeholder' : 'Choose One',
                'width' : '100%',
                tags : true,
                'allowClear' : true
            });
        });
    </script>
@endsection