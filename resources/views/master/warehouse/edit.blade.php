@extends('layout.default')

@section('content')
    <div class="subheader py-2 {{ Metronic::printClasses('subheader', false) }}" id="kt_subheader">
        <div class="{{ Metronic::printClasses('subheader-container', false) }} d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-baseline flex-wrap mr-5">
                <h5 class="text-dark font-weight-bold my-1 mr-5">
                    Edit Warehouse                                 
                </h5>
                <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                    <li class="breadcrumb-item">
                        <a href="{{ route('dashboard') }}" class="text-muted">Home</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="{{ route('master.warehouse.index') }}" class="text-muted">Bandwidth Package</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="" class="text-muted">Edit Warehouse</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    @include('inc.error-list')
    <div class="card card-custom mb-0">
	    <form class="form" action="{{ route('master.warehouse.update', $warehouse->Code) }}" method="POST">
	        {!! csrf_field() !!}
	        {!! method_field('PUT') !!}
	        <div class="card-header border-1 pt-6 pb-0">
	            <div class="card-title">
	                <h3 class="card-label">Edit Warehouse
	                </h3>
	            </div>
	        </div>
	        <div class="card-body">
	            <div class="row">
	                <div class="col-lg-6">
	                    <div class="card card-custom mb-1 col-lg-12 ">
	                        <div class="card-header border-0">
	                            <h3 class="card-title font-weight-bolder text-dark">Information</h3>
	                        </div>
	                        <div class="card-body pt-0">
	                            <div class="mb-1">
	                                <div class="form-group row mb-0">
	                                    <div class="col-lg-12">
	                                        <label>Code</label>
	                                        <input type="text" class="form-control form-control-sm" name="Code" value="{{ old('Code', $warehouse->Code) }}" placeholder="Enter Code" / disabled>
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="mb-1">
	                                <div class="form-group row mb-0">
	                                    <div class="col-lg-12">
	                                        <label>Name</label>
	                                        <input type="text" class="form-control form-control-sm" name="Name" value="{{ old('Name', $warehouse->Name) }}" placeholder="Enter Name" / required>
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="mb-1">
	                                <div class="form-group row mb-0">
	                                    <div class="col-lg-12">
	                                        <label>Warehouse Type</label>
	                                        <div class="input-group">
	                                            <select class="form-control form-control-sm select2" id="WarehouseType" name="WarehouseType" data-placeholder="Choose One">
	                                            <option></option>
	                                            <option value="INTERNAL" {{ old('WarehouseType', $warehouse->WarehouseType) == 'INTERNAL' ? 'selected' : '' }}>INTERNAL</option>
	                                            <option value="EXTERNAL" {{ old('WarehouseType', $warehouse->WarehouseType) == 'EXTERNAL' ? 'selected' : '' }}>EXTERNAL</option>
	                                            <option value="POP" {{ old('WarehouseType', $warehouse->WarehouseType) == 'POP' ? 'selected' : '' }}>POP</option>
	                                            <option value="CLIENT" {{ old('WarehouseType', $warehouse->WarehouseType) == 'CLIENT' ? 'selected' : '' }}>CLIENT</option>
	                                            </select>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="mb-1">
	                                <div class="form-group row mb-0">
	                                    <div class="col-lg-12">
	                                        <label>Active Status</label>
	                                        <span class="switch switch-primary">
	                                            <label>
	                                                <input type="checkbox" name="ActiveStatus"  value="1" {{ old('ActiveStatus', $warehouse->ActiveStatus) == 1 ? 'checked' : '' }}>
	                                                <span></span>
	                                            </label>
	                                        </span>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	                <div class="col-lg-6">
	                    <div class="card card-custom mb-1 col-lg-12 ">
	                        <div class="card-header border-0">
	                            <h3 class="card-title font-weight-bolder text-dark">Contact</h3>
	                        </div>
	                        <div class="card-body pt-0">
	                            <div class="mb-1">
	                                <div class="form-group row mb-0">
	                                    <div class="col-lg-12">
	                                        <label>Address <code>Optional</code> </label>
	                                        <textarea class="form-control form-control-sm" rows="5" name="Address">{{ old('Address', $warehouse->Address) }}</textarea>
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="mb-1">
	                                <div class="form-group row mb-0">
	                                    <div class="col-lg-12">
	                                        <label>City Code <code>Optional</code></label>
	                                        <input type="text" class="form-control form-control-sm" name="CityCode" value="{{ old('CityCode', $warehouse->CityCode) }}" placeholder="Enter CityCode" />
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="mb-1">
	                                <div class="form-group row mb-0">
	                                    <div class="col-lg-12">
	                                        <label>Country Code <code>Optional</code></label>
	                                        <input type="text" class="form-control form-control-sm" name="CountryCode" value="{{ old('CountryCode', $warehouse->CountryCode) }}" placeholder="Enter Country Code" />
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="mb-1">
	                                <div class="form-group row mb-0">
	                                    <div class="col-lg-12">
	                                        <label>Zip Code<code>Optional</code></label>
	                                        <input type="text" class="form-control form-control-sm" name="ZipCode" value="{{ old('ZipCode', $warehouse->ZipCode) }}" placeholder="Enter ZipCode" />
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="mb-1">
	                                <div class="form-group row mb-0">
	                                    <div class="col-lg-12">
	                                        <label>Phone1<code>Optional</code></label>
	                                        <input type="text" class="form-control form-control-sm" name="Phone1" value="{{ old('Phone1', $warehouse->Phone1) }}" placeholder="Enter Phone1" />
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="mb-1">
	                                <div class="form-group row mb-0">
	                                    <div class="col-lg-12">
	                                        <label>Phone2<code>Optional</code></label>
	                                        <input type="text" class="form-control form-control-sm" name="Phone2" value="{{ old('Phone2',$warehouse->Phone2) }}" placeholder="Enter Phone2"/>
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="mb-1">
	                                <div class="form-group row mb-0">
	                                    <div class="col-lg-12">
	                                        <label>Contact Person<code>Optional</code></label>
	                                        <input type="text" class="form-control form-control-sm" name="ContactPerson" value="{{ old('ContactPerson', $warehouse->ContactPerson) }}" placeholder="Enter ContactPerson"/>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <div class="card-footer">
	            <div class="row">
	                <div class="col-lg-6">
	                    <a href="{{ route('master.warehouse.index') }}" id="cancel-btn" class="btn btn-danger">Cancel</a>
	                </div>
	                <div class="col-lg-6 text-right">
	                    <button type="submit" class="btn btn-primary mr-2">Update</button>
	                </div>
	            </div>
	        </div>
	    </form>
    </div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#WarehouseType').select2({
                'placeholder' : 'Choose One',
                'width' : '100%',
                tags : true,
                'allowClear' : true
            });
        });
    </script>
@endsection