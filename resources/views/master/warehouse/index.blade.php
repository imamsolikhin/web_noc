@extends('layout.default')

@section('content')
    <div class="subheader py-2 {{ Metronic::printClasses('subheader', false) }}" id="kt_subheader">
        <div class="{{ Metronic::printClasses('subheader-container', false) }} d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-baseline flex-wrap mr-5">
                <h5 class="text-dark font-weight-bold my-1 mr-5">
                    Warehouse
                </h5>
                <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                    <li class="breadcrumb-item">
                        <a href="{{ route('dashboard') }}" class="text-muted">Home</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="{{ route('master.warehouse.index') }}" class="text-muted">Master</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="" class="text-muted">Warehouse </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    @include('inc.success-notif')
    <div class="card card-custom">
        <div class="card-header flex-wrap border-1 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Warehouse
                    <div class="text-muted pt-2 font-size-sm">show Datatable from table Warehouse </div>
                </h3>
            </div>
            <div class="card-toolbar">
                <a href="#" class="btn btn-primary py-2" id="add-btn">
                    <i class="menu-icon">
		                <span class="svg-icon svg-icon-md">
		                    <!--begin::Svg Icon | path:media/svg/icons/Design/Flatten.svg-->
		                    <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
		                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
		                            <rect x="0" y="0" width="24" height="24"/>
		                            <circle fill="#000000" cx="9" cy="15" r="6"/>
		                            <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3"/>
		                        </g>
		                    </svg>
		                    <!--end::Svg Icon-->
		                </span>
                    </i>
                    New Warehouse
                </a>
            </div>
        </div>
        <div class="card-body">
	        <div class="table-responsive">
	            <table style="width: 1070px !important;" class="table table-borderless table-vertical-center" id="datatable"></table>
	        </div>
        </div>
    </div>
    <br>
	<div class="card card-custom mb-0"  id="add-form" {!! (count($errors) == 0) ? "style='display: none;'" : '' !!}>
	    @include('inc.error-list')
	    <form class="form" action="{{ route('master.warehouse.store') }}" method="POST">
	        {!! csrf_field() !!}
	        <div class="card-header border-1 pt-6 pb-0">
	            <div class="card-title">
	                <h3 class="card-label">Warehouse
	                </h3>
	            </div>
	        </div>
	        <div class="card-body">
	        	<div class="row">
	        		<div class="col-lg-6">
	        			<div class="card card-custom mb-1 col-lg-12 ">
	                        <div class="card-header border-0">
	                            <h3 class="card-title font-weight-bolder text-dark">Information</h3>
	                        </div>
	                        <div class="card-body pt-0">
	                        	<div class="mb-1">
	                        		<div class="form-group row mb-0">
	                        			<div class="col-lg-12">
	                        				<label>Code</label>
	                        				<input type="text" class="form-control form-control-sm" name="Code" value="{{ old('Code') }}" placeholder="Enter Code" / required>
	                        			</div>
	                        		</div>
	                        	</div>
	                        	<div class="mb-1">
	                        		<div class="form-group row mb-0">
	                        			<div class="col-lg-12">
	                        				<label>Name</label>
	                        				<input type="text" class="form-control form-control-sm" name="Name" value="{{ old('Name') }}" placeholder="Enter Name" / required>
	                        			</div>
	                        		</div>
	                        	</div>
	                        	<div class="mb-1">
	                        		<div class="form-group row mb-0">
	                        			<div class="col-lg-12">
					                        <label>Warehouse Type</label>
					                        <div class="input-group">
					                            <select class="form-control form-control-sm select2" id="WarehouseType" name="WarehouseType" data-placeholder="Choose One" required>
				                                    <option></option>
				                                    <option value="INTERNAL" {{ old('WarehouseType') == 'INTERNAL' ? 'selected' : '' }}>INTERNAL</option>
				                                    <option value="EXTERNAL" {{ old('WarehouseType') == 'EXTERNAL' ? 'selected' : '' }}>EXTERNAL</option>
				                                    <option value="POP" {{ old('WarehouseType') == 'POP' ? 'selected' : '' }}>POP</option>
				                                    <option value="CLIENT" {{ old('WarehouseType') == 'CLIENT' ? 'selected' : '' }}>CLIENT</option>
					                            </select>
					                        </div>
	                        			</div>
	                        		</div>
	                        	</div>
	                        	<div class="mb-1">
	                        		<div class="form-group row mb-0">
	                        			<div class="col-lg-12">
	                        				<label>Active Status</label>
											<span class="switch switch-primary">
												<label>
													<input type="checkbox" name="ActiveStatus"  value="1" {{ old('ActiveStatus') }} checked disabled />
													<span></span>
												</label>
											</span>
	                        			</div>
	                        		</div>
	                        	</div>
	                        </div>
	        			</div>
	        		</div>
	        		<div class="col-lg-6">
	        			<div class="card card-custom mb-1 col-lg-12 ">
	                        <div class="card-header border-0">
	                            <h3 class="card-title font-weight-bolder text-dark">Contact</h3>
	                        </div>
	                        <div class="card-body pt-0">
	                        	<div class="mb-1">
	                        		<div class="form-group row mb-0">
	                        			<div class="col-lg-12">
	                        				<label>Address <code>Optional</code> </label>
	                        				<textarea class="form-control form-control-sm" rows="5" name="Address">{{ old('Address') }}</textarea>
	                        			</div>
	                        		</div>
	                        	</div>
	                        	<div class="mb-1">
	                        		<div class="form-group row mb-0">
	                        			<div class="col-lg-12">
	                        				<label>City Code <code>Optional</code></label>
	                        				<input type="text" class="form-control form-control-sm" name="CityCode" value="{{ old('CityCode') }}" placeholder="Enter CityCode" />
	                        			</div>
	                        		</div>
	                        	</div>
	                        	<div class="mb-1">
	                        		<div class="form-group row mb-0">
	                        			<div class="col-lg-12">
	                        				<label>Country Code <code>Optional</code></label>
	                        				<input type="text" class="form-control form-control-sm" name="CountryCode" value="{{ old('CountryCode') }}" placeholder="Enter Country Code" />
	                        			</div>
	                        		</div>
	                        	</div>
	                        	<div class="mb-1">
	                        		<div class="form-group row mb-0">
	                        			<div class="col-lg-12">
	                        				<label>Zip Code<code>Optional</code></label>
	                        				<input type="text" class="form-control form-control-sm" name="ZipCode" value="{{ old('ZipCode') }}" placeholder="Enter ZipCode" />
	                        			</div>
	                        		</div>
	                        	</div>
	                        	<div class="mb-1">
	                        		<div class="form-group row mb-0">
	                        			<div class="col-lg-12">
	                        				<label>Phone1<code>Optional</code></label>
	                        				<input type="text" class="form-control form-control-sm" name="Phone1" value="{{ old('Phone1') }}" placeholder="Enter Phone1" />
	                        			</div>
	                        		</div>
	                        	</div>
	                        	<div class="mb-1">
	                        		<div class="form-group row mb-0">
	                        			<div class="col-lg-12">
	                        				<label>Phone2<code>Optional</code></label>
	                        				<input type="text" class="form-control form-control-sm" name="Phone2" value="{{ old('Phone2') }}" placeholder="Enter Phone2" />
	                        			</div>
	                        		</div>
	                        	</div>
	                        	<div class="mb-1">
	                        		<div class="form-group row mb-0">
	                        			<div class="col-lg-12">
	                        				<label>Contact Person<code>Optional</code></label>
	                        				<input type="text" class="form-control form-control-sm" name="ContactPerson" value="{{ old('ContactPerson') }}" placeholder="Enter ContactPerson" />
	                        			</div>
	                        		</div>
	                        	</div>
	                        </div>
	        			</div>
	        		</div>
	        	</div>
	        </div>
	        <div class="card-footer">
	            <div class="row">
	                <div class="col-lg-6">
	                    <button type="button" id="cancel-btn" class="btn btn-danger">Cancel</button>
	                </div>
	                <div class="col-lg-6 text-right">
	                    <button type="submit" class="btn btn-primary mr-2">Submit</button>
	                </div>
	            </div>
	        </div>
	    </form>
	</div>
@endsection

{{-- Styles Section --}}
@section('styles')
    <link rel="stylesheet" href="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.css">
    <link rel="stylesheet" href="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.min.css">
    <link rel="stylesheet" href="{{ config('app.url') }}plugins/custom/datatables/datatables.bundle.css">
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ config('app.url') }}global/vendor/datatables/jquery.dataTables.js"></script>
    <script src="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.js"></script>
    <script src="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable').dataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    method: 'POST',
                    url : '{{ route('master.warehouse.data') }}',
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                },
                columns : [
                    { title:'Code',  data: 'Code', name: 'Code', defaultContent: '-', class: 'text-center dt-body-nowrap', },
                    { title:'Name', data: 'Name', name: 'Name', defaultContent: '-', class: 'text-center dt-body-nowrap', },
                    { title:'Phone', data: 'Phone1', name: 'Phone1', defaultContent: '-', class: 'text-center dt-body-nowrap', },
                    { title:'Active', data: 'active', name: 'active', defaultContent: '-', class: 'text-center dt-body-nowrap', },
                    { title:'Action', data: 'action', name: 'action', searchable: false, orderable: false, class: 'text-center dt-body-nowrap', }
                ],
                initComplete: function() {
                    $('.tl-tip').tooltip();
                    @if (count($errors) > 0)
                        jQuery("html, body").animate({
                            scrollTop: $('#add-form').offset().top - 100 // 66 for sticky bar height
                        }, "slow");
                    @endif
                }
            });

            $('#add-btn').click(function(e) {
                $('#add-form').toggle();
                jQuery("html, body").animate({
                    scrollTop: $('#add-form').offset().top - 100 // 66 for sticky bar height
                }, "slow");
            });

            $('#cancel-btn').click(function(e) {
                $('#add-form').toggle();
                jQuery("html, body").animate({
                    scrollTop: $('body').offset().top - 100 // 66 for sticky bar height
                }, "slow");
            });

            $('#WarehouseType').select2({
                'placeholder' : 'Choose One',
                'width' : '100%',
                tags : true,
                'allowClear' : true
            });
        });
    </script>
@endsection
