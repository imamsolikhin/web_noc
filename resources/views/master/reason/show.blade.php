@extends('layout.default')

@section('content')
    <div class="subheader py-2 {{ Metronic::printClasses('subheader', false) }}" id="kt_subheader">
        <div class="{{ Metronic::printClasses('subheader-container', false) }} d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-baseline flex-wrap mr-5">
                <h5 class="text-dark font-weight-bold my-1 mr-5">
                    Show Reasoon                                
                </h5>
                <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                    <li class="breadcrumb-item">
                        <a href="{{ route('dashboard') }}" class="text-muted">Home</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="{{ route('master.reason.index') }}" class="text-muted">Reason</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="" class="text-muted">Show Reason</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="card card-custom mb-0">
	    <div class="card-header border-1 pt-6 pb-0">
	        <div class="card-title">
	            <h3 class="card-label">Show Reason
	            </h3>
	        </div>
	    </div>
        <div class="card-body">
        	<div class="mb-1">
	            <div class="form-group row mb-0">
	                <div class="col-lg-4">
	                    <label>Code</label>
	                    <input type="text" class="form-control form-control-sm" name="Code" value="{{ old('Code', $reason->Code) }}" placeholder="Enter Code"  disabled />
	                </div>
	                <div class="col-lg-8">
	                    <label>Name</label>
	                    <input type="text" class="form-control form-control-sm" name="Name" value="{{ old('Name', $reason->Name) }}" placeholder="Enter Name" disabled />
	                </div>
	            </div>
        	</div>
        	<div class="mb-1">
	            <div class="form-group row mb-0">
	                <div class="col-lg-12">
	                    <label>Remark <code>Optional</code></label>
	                    <textarea class="form-control form-control-sm" rows="5" name="Remark" disabled>{{ old('Remark', $reason->Remark) }}</textarea>
	                </div>
	            </div>
        	</div>
        	<div class="mb-1">
	            <div class="form-group row mb-0">
	                <div class="col-lg-6">
            			<div class="col-lg-12">
            				<label>Active Status</label>
							<span class="switch switch-primary">
								<label>
									<input type="checkbox" name="ActiveStatus"  value="1" {{ old('ActiveStatus', $reason->ActiveStatus) == 1 ? 'checked' : '' }} disabled/>
									<span></span>
								</label>
							</span>
            			</div>
	                </div>
	            </div>
        	</div>
        </div>
	    <div class="card-footer">
	        <div class="row">
	            <div class="col-lg-6">
	                <a href="{{ route('master.reason.index') }}" id="cancel-btn" class="btn btn-danger">Back</a>
	            </div>
	            <div class="col-lg-6 text-right">
	                <a href="{{ route('master.reason.edit', $reason->Code) }}" class="btn btn-primary mr-2">Edit</a>
	            </div>
	        </div>
	    </div>
    </div>
@endsection