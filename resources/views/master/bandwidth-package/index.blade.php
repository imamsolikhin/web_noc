@extends('layout.default')

@section('content')
    <div class="subheader py-2 {{ Metronic::printClasses('subheader', false) }}" id="kt_subheader">
        <div class="{{ Metronic::printClasses('subheader-container', false) }} d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-baseline flex-wrap mr-5">
                <h5 class="text-dark font-weight-bold my-1 mr-5">
                    Bandwidth Package
                </h5>
                <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                    <li class="breadcrumb-item">
                        <a href="{{ route('dashboard') }}" class="text-muted">Home</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="{{ route('master.bandwidth-package.index') }}" class="text-muted">Master</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="" class="text-muted">Bandwidth Package</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    @include('inc.success-notif')
    <div class="card card-custom">
        <div class="card-header flex-wrap border-1 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Bandwidth Package
                    <div class="text-muted pt-2 font-size-sm">show Datatable from table Bandwidth Package</div>
                </h3>
            </div>
            <div class="card-toolbar">
                <a href="#" class="btn btn-primary py-2" id="add-btn">
                    <i class="menu-icon">
		                <span class="svg-icon svg-icon-md">
		                    <!--begin::Svg Icon | path:media/svg/icons/Design/Flatten.svg-->
		                    <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
		                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
		                            <rect x="0" y="0" width="24" height="24"/>
		                            <circle fill="#000000" cx="9" cy="15" r="6"/>
		                            <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3"/>
		                        </g>
		                    </svg>
		                    <!--end::Svg Icon-->
		                </span>
                    </i>
                    New Bandwidth Package
                </a>
            </div>
        </div>
        <div class="card-body">
	        <div class="table-responsive">
	            <table style="width: 1070px !important;" class="table table-borderless table-vertical-center" id="datatable"></table>
	        </div>
        </div>
    </div>
    <br>
	<div class="card card-custom mb-0"  id="add-form" {!! (count($errors) == 0) ? "style='display: none;'" : '' !!}>
	    @include('inc.error-list')
	    <form class="form" action="{{ route('master.bandwidth-package.store') }}" method="POST">
	        {!! csrf_field() !!}
	        <div class="card-header border-1 pt-6 pb-0">
	            <div class="card-title">
	                <h3 class="card-label">Bandwidth Package
	                </h3>
	            </div>
	        </div>
	        <div class="card-body">
	        	<div class="mb-1">
		            <div class="form-group row mb-0">
		                <div class="col-lg-4">
		                    <label>Code</label>
		                    <input type="text" class="form-control form-control-sm" name="Code" value="{{ old('Code') }}" placeholder="Enter Code"  required />
		                </div>
		                <div class="col-lg-8">
		                    <label>Name</label>
		                    <input type="text" class="form-control form-control-sm" name="Name" value="{{ old('Name') }}" placeholder="Enter Name" required />
		                </div>
		            </div>
	        	</div>
	        	<div class="mb-1">
		            <div class="form-group row mb-0">
		                <div class="col-lg-6">
		                    <label>Client Type</label>
                            <select class="form-control form-control-sm select2" id="ClientTypeCode" name="ClientTypeCode" data-placeholder="Choose One" required>
                                <option></option>
                                @foreach($clientTypes as $clientType)
                                <option value="{{ $clientType->Code }}" {{ old('ClientTypeCode') == $clientType->Code ? 'selected' : '' }}>{{ $clientType->Name }}</option>
                                @endforeach
                            </select>
		                </div>
		                <div class="col-lg-6">
		                    <label>IIX</label>
		                    <input type="number" class="form-control form-control-sm" name="IIX" value="{{ old('IIX') }}" placeholder="Enter IIX" />
		                </div>
		            </div>
	        	</div>
	        	<div class="mb-1">
		            <div class="form-group row mb-0">
		                <div class="col-lg-6">
		                    <label>Intl</label>
		                    <input type="number" class="form-control form-control-sm" name="Intl" value="{{ old('Intl') }}" placeholder="Enter Intl" />
		                </div>
		                <div class="col-lg-6">
		                    <label>SDIX</label>
		                    <input type="number" class="form-control form-control-sm" name="SDIX" value="{{ old('SDIX') }}" placeholder="Enter SDIX" />
		                    <input type="hidden" name="ActiveStatus" value="1">
		                </div>
		            </div>
	        	</div>
	        	<div class="mb-1">
		            <div class="form-group row mb-0">
		                <div class="col-lg-12">
		                    <label>Remark <code>Optional</code></label>
		                    <textarea class="form-control form-control-sm" rows="5" name="Remark"></textarea>
		                </div>
		            </div>
	        	</div>
	        </div>
	        <div class="card-footer">
	            <div class="row">
	                <div class="col-lg-6">
	                    <button type="button" id="cancel-btn" class="btn btn-danger">Cancel</button>
	                </div>
	                <div class="col-lg-6 text-right">
	                    <button type="submit" class="btn btn-primary mr-2">Submit</button>
	                </div>
	            </div>
	        </div>
	    </form>
	</div>
@endsection

{{-- Styles Section --}}
@section('styles')
    <link rel="stylesheet" href="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.css">
    <link rel="stylesheet" href="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.min.css">
    <link rel="stylesheet" href="{{ config('app.url') }}plugins/custom/datatables/datatables.bundle.css">
@endsection

{{-- Scripts Section --}}
@section('scripts')
    @include ('inc.confirm-delete-modal')
    <script src="{{ config('app.url') }}global/vendor/datatables/jquery.dataTables.js"></script>
    <script src="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.js"></script>
    <script src="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable').dataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    method: 'POST',
                    url : '{{ route('master.bandwidth-package.data') }}',
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                },
                columns : [
                    { title:'Code',  data: 'Code', name: 'Code', defaultContent: '-', class: 'text-center dt-body-nowrap', },
                    { title:'Name', data: 'Name', name: 'Name', defaultContent: '-', class: 'text-center dt-body-nowrap', },
                    { title:'Client', data: 'ClientTypeCode', name: 'ClientTypeCode', defaultContent: '-', class: 'text-center dt-body-nowrap', },
                    { title:'IIX', data: 'IIX', name: 'IIX', defaultContent: '-', class: 'text-center dt-body-nowrap', },
                    { title:'Intl', data: 'Intl', name: 'Intl', defaultContent: '-', class: 'text-center dt-body-nowrap', },
                    { title:'Active', data: 'active', name: 'active', defaultContent: '-', class: 'text-center dt-body-nowrap', },
                    { title:'Action', data: 'action', name: 'action', searchable: false, orderable: false, class: 'text-center dt-body-nowrap', }
                ],
                initComplete: function() {
                    $('.tl-tip').tooltip();
                    @if (count($errors) > 0)
                        jQuery("html, body").animate({
                            scrollTop: $('#add-form').offset().top - 100 // 66 for sticky bar height
                        }, "slow");
                    @endif
                }
            });

            $('#add-btn').click(function(e) {
                $('#add-form').toggle();
                jQuery("html, body").animate({
                    scrollTop: $('#add-form').offset().top - 100 // 66 for sticky bar height
                }, "slow");
            });

            $('#cancel-btn').click(function(e) {
                $('#add-form').toggle();
                jQuery("html, body").animate({
                    scrollTop: $('body').offset().top - 100 // 66 for sticky bar height
                }, "slow");
            });

            $('#ClientTypeCode').select2({
                'placeholder' : 'Choose One',
                'width' : '100%',
                tags : true,
                'allowClear' : true
            });
        });
    </script>
@endsection
