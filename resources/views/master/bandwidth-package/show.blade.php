@extends('layout.default')

@section('content')
    <div class="subheader py-2 {{ Metronic::printClasses('subheader', false) }}" id="kt_subheader">
        <div class="{{ Metronic::printClasses('subheader-container', false) }} d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-baseline flex-wrap mr-5">
                <h5 class="text-dark font-weight-bold my-1 mr-5">
                    Detail Bandwidth Package                                 
                </h5>
                <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                    <li class="breadcrumb-item">
                        <a href="{{ route('dashboard') }}" class="text-muted">Home</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="{{ route('master.bandwidth-package.index') }}" class="text-muted">Bandwidth Package</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="" class="text-muted">Detail Bandwidth Package</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="card card-custom mb-0">
	    <div class="card-header border-1 pt-6 pb-0">
	        <div class="card-title">
	            <h3 class="card-label">Detail Bandwidth Package
	            </h3>
	        </div>
	    </div>
	    <div class="card-body">
	    	<div class="mb-1">
	            <div class="form-group row mb-0">
	                <div class="col-lg-4">
	                    <label>Code <code>(Optional)</code></label>
	                    <input type="text" class="form-control form-control-sm" name="Code" value="{{ old('Code', $bandwidthPackage->Code) }}" placeholder="Enter Code" / disabled>
	                </div>
	                <div class="col-lg-8">
	                    <label>Name</label>
	                    <input type="text" class="form-control form-control-sm" name="Name" value="{{ old('Name', $bandwidthPackage->Name) }}" placeholder="Enter Name" disabled />
	                </div>
	            </div>
	    	</div>
	    	<div class="mb-1">
	            <div class="form-group row mb-0">
	                <div class="col-lg-6">
	                    <label>Client Type Code</label>
	                    <input type="text" class="form-control form-control-sm" name="ClientTypeCode" value="{{ old('ClientTypeCode', $bandwidthPackage->ClientTypeCode) }}" placeholder="Enter Client Type Code" disabled />
	                </div>
	                <div class="col-lg-6">
	                    <label>IIX</label>
	                    <input type="text" class="form-control form-control-sm" name="IIX" value="{{ old('IIX', $bandwidthPackage->IIX) }}" placeholder="Enter IIX" disabled />
	                </div>
	            </div>
	    	</div>
	    	<div class="mb-1">
	            <div class="form-group row mb-0">
	                <div class="col-lg-6">
	                    <label>Intl</label>
	                    <input type="text" class="form-control form-control-sm" name="Intl" value="{{ old('Intl', $bandwidthPackage->Intl) }}" placeholder="Enter Intl" disabled />
	                </div>
	                <div class="col-lg-6">
	                    <label>SDIX</label>
	                    <input type="text" class="form-control form-control-sm" name="SDIX" value="{{ old('SDIX', $bandwidthPackage->SDIX) }}" placeholder="Enter SDIX" disabled />
	                </div>
	            </div>
	    	</div>
	    	<div class="mb-1">
	            <div class="form-group row mb-0">
	                <div class="col-lg-12">
	                    <label>Remark <code>Optional</code></label>
	                    <textarea class="form-control form-control-sm" rows="5" name="Remark" disabled>{{ old('Remark', $bandwidthPackage->Remark) }}</textarea>
	                </div>
	            </div>
	    	</div>
	    </div>
	    <div class="card-footer">
	        <div class="row">
	            <div class="col-lg-6">
	                <a href="{{ route('master.bandwidth-package.index') }}" id="cancel-btn" class="btn btn-danger">Back</a>
	            </div>
	            <div class="col-lg-6 text-right">
	                <a href="{{ route('master.bandwidth-package.edit', $bandwidthPackage->Code) }}" class="btn btn-primary mr-2">Edit</a>
	            </div>
	        </div>
	    </div>
    </div>
@endsection