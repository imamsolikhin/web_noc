@extends('layout.default')

@section('content')
	<div class="subheader py-2 {{ Metronic::printClasses('subheader', false) }}" id="kt_subheader">
	    <div class="{{ Metronic::printClasses('subheader-container', false) }} d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
	        <div class="d-flex align-items-baseline flex-wrap mr-5">
	            <h5 class="text-dark font-weight-bold my-1 mr-5">
	                Update Task Assignment                               
	            </h5>
	            <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
	                <li class="breadcrumb-item">
	                    <a href="{{ route('dashboard') }}" class="text-muted">Home</a>
	                </li>
	                <li class="breadcrumb-item">
	                    <a href="" class="text-muted">Task Assignment</a>
	                </li>
	            </ul>
	        </div>
	    </div>
	</div>

	<form class="form" action="{{ route('ticketing.fmi.task-assignment.update', $taskAssigments->code) }}" method="POST">
		{!! csrf_field() !!}
	        {!! method_field('PUT') !!}
		<div class="card card-custom mb-0">
			<div class="card-header border-1 pt-6 pb-0">
			    <div class="card-title">
			        <h3 class="card-label">Task Assignment
			        </h3>
			    </div>
			</div>
			<div class="card-body">
			    <div class="mb-1">
			        <div class="form-group row">
			            <div class="col-lg-4">
			                <label>TCK No</label>
			                <input type="text" class="form-control" name="tck_no" value="{{ old('tck_no', $fMIOpenTicket->code) }}" disabled>
			            </div>

			            <div class="col-lg-4">
	                        <label>Open Ticket Date</label>
							<div class="input-group date" >
							    <input type="text" id="TransactionDate" class="form-control" readonly  placeholder="Select date" name="TransactionDate" value="{{old('TransactionDate', $transactionDate)}}" disabled/>
							    <div class="input-group-append">
							        <span class="input-group-text">
							        <i class="la la-calendar-check-o"></i>
							        </span>
							    </div>
							</div>
			            </div>
			        </div>

			        <div class="form-group row">
			            <div class="col-lg-6">
			                <label>Branch</label>
			                <select class="form-control select2" id="branch" name="branch" data-placeholder="Choose One" disabled>
			                	<option></option>
			                    @foreach($branchs as $list)
			                    <option value="{{$list->Code}}" {{ old('branch', $fMIOpenTicket->BranchCode) == $list->Code ? 'selected' : '' }}>{{ $list->Name }}</option>
			                    @endforeach
			                </select>
			            </div>
			            <div class="col-lg-6">
			                <label>Problem Type</label>
			                <select class="form-control" id="problemType" name="problemType" data-placeholder="Choose One" disabled>
			                	<option></option>
			                    @foreach($problemTypes as $list)
			                    <option value="{{$list->Code}}"  {{ old('problemType', $fMIOpenTicket->ProblemTypeCode) == $list->Code ? 'selected' : '' }}>{{ $list->name }}</option>
			                    @endforeach
			                </select>
			            </div>
			        </div>

			        <div class="form-group row">
			            <div class="col-lg-6">
			                <label>Responsibility Department</label>
			                <select class="form-control" id="responsibilityDepartmentCode" name="responsibilityDepartmentCode" data-placeholder="Choose One" disabled>
			                	<option></option>
			                    @foreach($departments as $list)
			                    <option value="{{$list->Code}}" {{ old('responsibilityDepartmentCode', $fMIOpenTicket->ResponsibilityDepartmentCode) == $list->Code ? 'selected' : '' }}>{{ $list->Name }}</option>
			                    @endforeach
			                </select>
			            </div>
			            <div class="col-lg-6">
			                <label>TCK Ref No</label>
			                <input type="text" class="form-control" name="ref_no" value="{{ old('ref_no', $fMIOpenTicket->RefNo) }}" placeholder="Enter Ref No" / disabled>
			            </div>
			        </div>

			        <div class="form-group row">
			            <div class="col-lg-6">
			                <label>Ticket Description</label>
			                <textarea class="form-control" rows="5" name="ticket_description" disabled>{{ $fMIOpenTicket->TicketDescription }}</textarea>
			            </div> 

			            <div class="col-lg-6">
			                <label>TCK Remark</label>
			                <textarea class="form-control" rows="5" name="Remark" disabled>{{ $fMIOpenTicket->Remark }}</textarea>
			            </div>
			        </div>

			        <div class="form-group row">
	                    <div class="col-lg-6">
	                        <label>Schedule Date</label>
							<div class="input-group date" >
							    <input type="text" id="ScheduleDate" class="form-control" readonly  placeholder="Select date" name="ScheduleDate" value="{{old('ScheduleDate', $scheduleDate)}}"/>
							    <div class="input-group-append">
							        <span class="input-group-text">
							        <i class="la la-calendar-check-o"></i>
							        </span>
							    </div>
							</div>
	                    </div>
		                <div class="col-lg-6">
		                    <label>Mitra Code</label>
                            <select class="form-control select2" id="mitra" name="MitraCode" data-placeholder="Choose One">
                                <option></option>
                                @foreach($mitras as $mitra)
                                <option value="{{ $mitra->Code }}" {{ old('MitraCode') == $mitra->Code ? 'selected' : '' }}>{{ $mitra->name }}</option>
                                @endforeach
                            </select>
		                </div>
			        </div> 

			        <div class="form-group row">
		                <div class="col-lg-6">
		                    <label>Team</label>
                            <select class="form-control select2" id="team" name="team" data-placeholder="Choose One">
                                <option></option>
                                @foreach($teams as $team)
                                <option value="{{ $team->Code }}" {{ old('team') == $team->Code ? 'selected' : '' }}>{{ $team->name }}</option>
                                @endforeach
                            </select>
		                </div>
			            <div class="col-lg-6">
			                <label>Ref No</label>
			                <input type="text" class="form-control" name="ref_no_status" value="{{ old('ref_no_status', $taskAssigments->RefNo) }}" placeholder="Enter Ref No" / >
			            </div>
			        </div>

			        <div class="form-group row">
			            <div class="col-lg-6">
			                <label>Remark</label>
			                <textarea class="form-control" rows="5" name="remark_staus">{{ $taskAssigments->Remark }}</textarea>
			            </div>
			        </div>
			    </div>
			</div>
			<div class="card-footer">
			    <div class="row">
			        <div class="col-lg-6">
			            <a href="{{ route('ticketing.fmi.ticket-open.show-assigment', $taskAssigments->TicketCode) }}" id="cancel-btn" class="btn btn-danger">Back</a>
			        </div>
			        <div class="col-lg-6 text-right">
			            <button type="submit" class="btn btn-primary mr-2">Update</button>
			        </div>
			    </div>
			</div>
		</div>
	</form>
@endsection

{{-- Styles Section --}}
@section('styles')
@endsection

{{-- Scripts Section --}}
@section('scripts')
	<script type="text/javascript">
		$(document).ready(function() {

	        $('#mitra').select2({
	            'placeholder' : 'Choose One',
	            'width' : '100%',
	            tags : true,
	            'allowClear' : true
	        });

	        $('#team').select2({
	            'placeholder' : 'Choose One',
	            'width' : '100%',
	            tags : true,
	            'allowClear' : true
	        });

            $('#problemType').select2({
                'placeholder' : 'Choose One',
                'width' : '100%',
                tags : true,
                'allowClear' : true
            });

            $('#responsibilityDepartmentCode').select2({
                'placeholder' : 'Choose One',
                'width' : '100%',
                tags : true,
                'allowClear' : true
            });
		});

		var KTBootstrapDatepicker = function () {
			var arrows;
			if (KTUtil.isRTL()) {
				arrows = {
					leftArrow: '<i class="la la-angle-right"></i>',
					rightArrow: '<i class="la la-angle-left"></i>'
				}
			} else {
				arrows = {
					leftArrow: '<i class="la la-angle-left"></i>',
					rightArrow: '<i class="la la-angle-right"></i>'
				}
			}
			
			// Private functions
			var demos = function () {
				$('#TransactionDate').datepicker({
					rtl: KTUtil.isRTL(),
                    format: 'dd-mm-yyyy',
					todayHighlight: true,
					orientation: "bottom left",
					templates: arrows
				});

				$('#ScheduleDate').datepicker({
					rtl: KTUtil.isRTL(),
                    format: 'dd-mm-yyyy',
					todayHighlight: true,
					orientation: "bottom left",
					templates: arrows
				});
			}

			return {
				// public functions
				init: function() {
					demos();
				}
			};
		}();

		jQuery(document).ready(function() {
			KTBootstrapDatepicker.init();
		});
	</script>
@endsection
