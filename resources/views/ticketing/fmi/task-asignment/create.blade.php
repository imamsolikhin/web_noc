@extends('layout.default')

@section('content')
    <div class="subheader py-2 {{ Metronic::printClasses('subheader', false) }}" id="kt_subheader">
        <div class="{{ Metronic::printClasses('subheader-container', false) }} d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-baseline flex-wrap mr-5">
                <h5 class="text-dark font-weight-bold my-1 mr-5">
                    FMI                                
                </h5>
                <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                    <li class="breadcrumb-item">
                        <a href="{{ route('dashboard') }}" class="text-muted">Home</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="{{ route('ticketing.fmi.task-assignment.index') }}" class="text-muted">FMI</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="" class="text-muted">Create FMI</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="card card-custom mb-0">
	    @include('inc.error-list')
	    <form class="form" action="{{ route('ticketing.fmi.task-assignment.store') }}" method="POST">
	        {!! csrf_field() !!}
	        <div class="card-header border-1 pt-6 pb-0">
	            <div class="card-title">
	                <h3 class="card-label">Create FMI
	                </h3>
	            </div>
	        </div>
	        <div class="card-body">
	        	<div class="mb-1">
		            <div class="form-group row">
		                <div class="col-lg-4">
		                    <label>Code</label>
		                    <input type="text" class="form-control" name="code" value="{{ old('code') }}" placeholder="Enter Code" / required>
		                </div>
	                    <div class="col-lg-8">
	                        <label>Transaction Date</label>
							<div class="input-group date" >
							    <input type="text" id="TransactionDate" class="form-control" readonly  placeholder="Select date" name="TransactionDate" value="{{old('TransactionDate')}}" />
							    <div class="input-group-append">
							        <span class="input-group-text">
							        <i class="la la-calendar-check-o"></i>
							        </span>
							    </div>
							</div>
	                    </div>
		            </div>
	        	</div>
	        	<div class="mb-1">
		            <div class="form-group row">
		                <div class="col-lg-6">
		                    <label>Branch</label>
                            <select class="form-control select2" id="branch" name="branch" data-placeholder="Choose One">
                                <option></option>
                                @foreach($branchs as $branch)
                                <option value="{{ $branch->Code }}" {{ old('branch') == $branch->Code ? 'selected' : '' }}>{{ $branch->Name }}</option>
                                @endforeach
                            </select>
		                </div>
		                <div class="col-lg-6">
		                    <label>Ticket Code</label>
                            <select class="form-control select2" id="ticket" name="ticket" data-placeholder="Choose One">
                                <option></option>
                                @foreach($openTickets as $ticket)
                                <option value="{{ $ticket->code }}" {{ old('ticket') == $ticket->code ? 'selected' : '' }}>{{ $ticket->code }}</option>
                                @endforeach
                            </select>
		                </div>
		            </div>
	        	</div>
	        	<div class="mb-1">
		            <div class="form-group row">
	                    <div class="col-lg-6">
	                        <label>Schedule Date</label>
							<div class="input-group date" >
							    <input type="text" id="ScheduleDate" class="form-control" readonly  placeholder="Select date" name="ScheduleDate" value="{{old('ScheduleDate')}}" />
							    <div class="input-group-append">
							        <span class="input-group-text">
							        <i class="la la-calendar-check-o"></i>
							        </span>
							    </div>
							</div>
	                    </div>
		                <div class="col-lg-6">
		                    <label>Mitra Code</label>
                            <select class="form-control select2" id="mitra" name="MitraCode" data-placeholder="Choose One">
                                <option></option>
                                @foreach($mitras as $mitra)
                                <option value="{{ $mitra->Code }}" {{ old('MitraCode') == $mitra->Code ? 'selected' : '' }}>{{ $mitra->name }}</option>
                                @endforeach
                            </select>
		                </div>
		            </div>
	        	</div>
	        	<div class="mb-1">
		            <div class="form-group row">
		                <div class="col-lg-6">
		                    <label>Team Code</label>
		                    <input type="text" class="form-control" name="TeamCode" value="{{ old('TeamCode') }}" placeholder="Enter Team Code" />
		                </div>
		                <div class="col-lg-6">
		                    <label>ERP Task Code</label>
		                    <input type="text" class="form-control" name="ERPTaskCode" value="{{ old('ERPTaskCode') }}" placeholder="Enter ERP Task Code" />
		                </div>
		            </div>
	        	</div>
	        	<div class="mb-1">
		            <div class="form-group row">
		                <div class="col-lg-6">
		                    <label>Ref No</label>
		                    <input type="text" class="form-control" name="RefNo" value="{{ old('RefNo') }}" placeholder="Enter Ref No" />
		                </div>
		            </div>
	        	</div>
	        	<div class="mb-1">
		            <div class="form-group row">
		                <div class="col-lg-12">
		                    <label>Remark</label>
		                    <textarea class="form-control" rows="5" name="Remark"></textarea>
		                </div>
		            </div>
	        	</div>
	        </div>
	        <div class="card-footer">
	            <div class="row">
	                <div class="col-lg-6">
	                    <a href="{{ route('ticketing.fmi.task-assignment.index') }}" id="cancel-btn" class="btn btn-danger">Cancel</a>
	                </div>
	                <div class="col-lg-6 text-right">
	                    <button type="submit" class="btn btn-primary mr-2">Submit</button>
	                </div>
	            </div>
	        </div>
	    </form>
    </div>
@endsection

@section('styles')

@endsection

@section('scripts')
    <script type="text/javascript">
        $('#ticket').select2({
            'placeholder' : 'Choose One',
            'width' : '100%',
            tags : true,
            'allowClear' : true
        });

        $('#branch').select2({
            'placeholder' : 'Choose One',
            'width' : '100%',
            tags : true,
            'allowClear' : true
        });

        $('#mitra').select2({
            'placeholder' : 'Choose One',
            'width' : '100%',
            tags : true,
            'allowClear' : true
        });

		var KTBootstrapDatepicker = function () {
			var arrows;
			if (KTUtil.isRTL()) {
				arrows = {
					leftArrow: '<i class="la la-angle-right"></i>',
					rightArrow: '<i class="la la-angle-left"></i>'
				}
			} else {
				arrows = {
					leftArrow: '<i class="la la-angle-left"></i>',
					rightArrow: '<i class="la la-angle-right"></i>'
				}
			}
			
			// Private functions
			var demos = function () {
				$('#TransactionDate').datepicker({
					rtl: KTUtil.isRTL(),
                    format: 'dd-mm-yyyy',
					todayHighlight: true,
					orientation: "bottom left",
					templates: arrows
				});

				$('#ScheduleDate').datepicker({
					rtl: KTUtil.isRTL(),
                    format: 'dd-mm-yyyy',
					todayHighlight: true,
					orientation: "bottom left",
					templates: arrows
				});
			}

			return {
				// public functions
				init: function() {
					demos();
				}
			};
		}();

		jQuery(document).ready(function() {
			KTBootstrapDatepicker.init();
		});
    </script>
@endsection