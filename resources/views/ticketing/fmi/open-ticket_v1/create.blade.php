@extends('layout.default')

@section('content')
    <div class="subheader py-2 {{ Metronic::printClasses('subheader', false) }}" id="kt_subheader">
        <div class="{{ Metronic::printClasses('subheader-container', false) }} d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-baseline flex-wrap mr-5">
                <h5 class="text-dark font-weight-bold my-1 mr-5">
                    FMI
                </h5>
                <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                    <li class="breadcrumb-item">
                        <a href="{{ route('dashboard') }}" class="text-muted">Home</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="{{ route('ticketing.fmi.ticket-open.index') }}" class="text-muted">Open Ticket</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="" class="text-muted">Create Open Ticket</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="card card-custom mb-0">
	    @include('inc.error-list')
	    <form class="form" action="{{ route('ticketing.fmi.ticket-open.store') }}" method="POST">
	        {!! csrf_field() !!}
	        <div class="card-header border-1 pt-6 pb-0">
	            <div class="card-title">
	                <h3 class="card-label">Create Open Ticket
	                </h3>
	            </div>
	        </div>
	        <div class="card-body">
	        	<div class="mb-1">
		            <div class="form-group row">
		                <div class="col-lg-6">
		                    <label>TCK No</label>
		                    <input type="text" class="form-control" name="tck_no" value="{{ old('tck_no') }}" placeholder="Enter TCK No" / required>
		                </div>
		                <div class="col-lg-6">
	                        <label>Open Ticket Date</label>
							<div class="input-group date" >
							    <input type="text" id="TransactionDate" class="form-control" readonly  placeholder="Select date" name="TransactionDate" value="{{old('TransactionDate')}}" />
							    <div class="input-group-append">
							        <span class="input-group-text">
							        <i class="la la-calendar-check-o"></i>
							        </span>
							    </div>
							</div>
		                </div>
		            </div>
	        	</div>
	        	<div class="mb-1">
		            <div class="form-group row">
		                <div class="col-lg-6">
		                    <label>Ticket Category</label>
                            <select class="form-control select2 ticket_category" id="ticket_category" name="ticket_category" data-placeholder="Choose One" required>
                                <option></option>
                                <option value="CRITICAL" {{ old('ticket_category') == 'CRITICAL' ? 'selected' : '' }}>Critical</option>
                                <option value="MAJOR" {{ old('ticket_category') == 'MAJOR' ? 'selected' : '' }}>Major</option>
                                <option value="MINOR" {{ old('ticket_category') == 'MINOR' ? 'selected' : '' }}>Minor</option>
                                <option value="PLN" {{ old('ticket_category') == 'PLN' ? 'selected' : '' }}>PLN</option>
                            </select>
		                </div>
	                    <div class="col-lg-6">
	                        <label>Schedule Date</label>
							<div class="input-group date" >
							    <input type="text" id="ScheduleDate" class="form-control" readonly  placeholder="Select date" name="ScheduleDate" value="{{old('ScheduleDate')}}" />
							    <div class="input-group-append">
							        <span class="input-group-text">
							        <i class="la la-calendar-check-o"></i>
							        </span>
							    </div>
							</div>
	                    </div>
		            </div>
	        	</div>
	        	<div class="mb-1">
		            <div class="form-group row">
		                <div class="col-lg-6">
		                    <label>Branch</label>
                            <select class="form-control select2" id="branch" name="branch" data-placeholder="Choose One">
                                <option></option>
                                @foreach($branchs as $branch)
                                <option value="{{ $branch->Code }}" {{ old('branch') == $branch->Code ? 'selected' : '' }}>{{ $branch->Name }}</option>
                                @endforeach
                            </select>
		                </div>
		                <div class="col-lg-6">
		                    <label>Problem Type</label>
                            <select class="form-control select2" id="problemType" name="problemType" data-placeholder="Choose One">
                                <option></option>
                                @foreach($problemTypes as $problemType)
                                <option value="{{ $problemType->Code }}" {{ old('problemType') == $problemType->Code ? 'selected' : '' }}>{{ $problemType->name }}</option>
                                @endforeach
                            </select>
		                </div>
		            </div>
	        	</div>
	        	<div class="mb-1">
		            <div class="form-group row">
		                <div class="col-lg-6" id="div-customer" hidden>
		                    <label>Customer</label>
                            <select class="form-control select2" id="open_customer" name="open_customer[]" multiple data-placeholder="Choose One">
                                <option></option>
                                <option value="customer1" {{ old('open_customer') == 'customer1' ? 'selected' : '' }}>Customer 1</option>
                                <option value="customer2" {{ old('open_customer') == 'customer2' ? 'selected' : '' }}>Customer 2</option>
                                <option value="customer3" {{ old('open_customer') == 'customer3' ? 'selected' : '' }}>Customer 3</option>
                            </select>
		                </div>
		                <div class="col-lg-6">
		                    <label>Responsibility Department</label>
		                    <input type="text" class="form-control" name="responsibility_department" value="{{ old('responsibility_department') }}" placeholder="Enter Responsibility Deparment" />
		                </div>
                    <div class="col-lg-12" id="div-search">
                      <button type="button" class="btn btn-primary btn-lg mt-8" data-toggle="modal" data-target="#modalFindCustomer">
                        <i class="fa fa-search"></i> Search Olt / BTS
                      </button>
                      <hr>
                      <label>Customer ID: </label>
                      <ul id="customer_id_list"></ul>
                      <hr>
                      <div class="modal fade" id="modalFindCustomer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title">Search OLT/BTS</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body">
                              <div class="row">
                                <div class="col-lg-6" id="div-bts">
            		                    <label>BTS</label>
                                        <select class="form-control select2" id="open_bts" name="open_bts" multiple data-placeholder="Choose needed Item">
                                            @foreach($baseTransmissionStations as $bts)
                                            <option value="{{ $bts->Code }}" {{ old('bts') == $bts->Code ? 'selected' : '' }}>{{ $bts->name }}</option>
                                            @endforeach
                                        </select>
            		                </div>
                                <div class="col-lg-6" id="div-olt">
                                  <label>OLT</label>
                                      <select class="form-control select2" id="olt_host" name="olt_host" multiple data-placeholder="Choose needed item">
                                          @foreach($oltHost as $oh)
                                          <option value="{{ $oh->IpAddress }}" {{ old('oh') == $oh->IpAddress ? 'selected' : '' }}>{{ $oh->Hostname }}</option>
                                          @endforeach
                                      </select>
                                </div>
                                <div class="col-lg-12 mt-5">
                                  <table class="table table-stripped">
                                    <thead class="bg-danger text-white rounded">
                                      <tr>
                                        <th>Act</th>
                                        <th>Customer ID</th>
                                        <th>Customer Name</th>
                                        <th>Contact Person</th>
                                        <th>Address</th>
                                        <th>BTS</th>
                                        <th>OLT</th>
                                      </tr>
                                    </thead>
                                    <tbody id="customer_noc_list">

                                    </tbody>
                                  </table>
                                </div>
                              </div>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                              <button type="button" class="btn btn-primary" id="select_customer">Select</button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
		            </div>
	        	</div>
	        	<div class="mb-1">
		            <div class="form-group row">
		                <div class="col-lg-6">
		                    <label>Ref No</label>
		                    <input type="text" class="form-control" name="ref_no" value="{{ old('ref_no') }}" placeholder="Enter Ref No" />
		                </div>
		            </div>
	        	</div>

	        	<div class="mb-1">
		            <div class="form-group row">
		                <div class="col-lg-6" id="div-device-type-fo" hidden>
		                    <label>Device Type (FO)</label>
                            <select class="form-control select2" id="open_device_type_fo" name="open_device_type_fo[]" multiple data-placeholder="Choose One">
                                <option></option>
                                <option value="fo1" {{ old('open_device_type_fo') == 'fo1' ? 'selected' : '' }}>fo 1</option>
                                <option value="fo2" {{ old('open_device_type_fo') == 'fo2' ? 'selected' : '' }}>fo 2</option>
                                <option value="fo3" {{ old('open_device_type_fo') == 'fo3' ? 'selected' : '' }}>fo 3</option>
                            </select>
		                </div>
		                <div class="col-lg-6" id="div-device-type-wll" hidden>
		                    <label>Device Type (WLL)</label>
                            <select class="form-control select2" id="open_device_type_wll" name="open_device_type_wll[]" multiple data-placeholder="Choose One">
                                <option></option>
                                <option value="wll1" {{ old('open_device_type_wll') == 'wll1' ? 'selected' : '' }}>wll 1</option>
                                <option value="wll2" {{ old('open_device_type_wll') == 'wll2' ? 'selected' : '' }}>wll 2</option>
                                <option value="wll3" {{ old('open_device_type_wll') == 'wll3' ? 'selected' : '' }}>wll 3</option>
                            </select>
		                </div>
		            </div>
	        	</div>
	        	<div class="mb-1">
		            <div class="form-group row">
		                <div class="col-lg-6">
		                    <label>Ticket Description</label>
		                    <textarea class="form-control" rows="5" name="ticket_description"></textarea>
		                </div>
		                <div class="col-lg-6">
		                    <label>Remark</label>
		                    <textarea class="form-control" rows="5" name="Remark"></textarea>
		                </div>
		            </div>
	        	</div>
	        </div>
	        <div class="card-footer">
	            <div class="row">
	                <div class="col-lg-6">
	                    <a href="{{ route('ticketing.fmi.ticket-open.index') }}" id="cancel-btn" class="btn btn-danger">Cancel</a>
	                </div>
	                <div class="col-lg-6 text-right">
	                    <button type="submit" class="btn btn-primary mr-2">Submit</button>
	                </div>
	            </div>
	        </div>
	    </form>
    </div>
@endsection

@section('styles')

@endsection

@section('scripts')

    <script type="text/javascript">
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
    </script>
    <script type="text/javascript">
        Array.prototype.remove = function() {
        var what, a = arguments, L = a.length, ax;
        while (L && this.length) {
            what = a[--L];
            while ((ax = this.indexOf(what)) !== -1) {
                this.splice(ax, 1);
            }
        }
        return this;
        };

        var checkbox_selected=[];

        $(document).on('change','.checkbox-selected',function(){
          if($(this).prop('checked')==true)
          {
            checkbox_selected.push($(this).val());
          }
          else
          {
            checkbox_selected.remove($(this).val());
          }
        });

        $('#select_customer').on('click',function(){
          var html = '';
          var list = '';
          $('#customer-item').html(html);
          $('#customer_id_list').html(list);
          for(var i = 0; i < checkbox_selected.length; i++)
          {
            list += '<li>' + checkbox_selected[i] + '</li>';
            html += '<input type="hidden" readonly name="CustCode[]" value="'+checkbox_selected[i]+'">';
          }
          $('#customer_id_list').html(list);
          $('#customer-item').html(html);
          $('#modalFindCustomer').modal('hide');
        });

        $(document).ready(function() {
          var selected_customer = [];
          $('#open_bts, #olt_host').on('change',function(){
            $.ajax({
              url: '{{route("ticketing.fmi.ticket-open.get-by-olt-bts")}}',
              method: 'POST',
              enctype: 'multipart/form-data',
              data: {
                "_token": "{{ csrf_token() }}",
                "open_bts": $('#open_bts').val(),
                'olt_host': $('#olt_host').val()
              },
              processData: true,
              cache: false,
              beforeSend: function()
              {
                $('#customer_noc_list').html('<tr><td colspan="7"><div class="progress">\
                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">Fetch Data</div>\
                  </div></td></tr>'
                );
              },
              success: function(response)
              {
                if(response.status == 'true')
                {
                  var html = '';
                  $('#customer_noc_list').html(html);
                  for(var i = 0; i < response.data.length; i++)
                  {
                    html += '<tr>';

                    if(response.data[i].Code)
                    {
                      html += '<td><input type="checkbox" class="checkbox-selected" value="'+response.data[i].Code+'"></td>';
                    }
                    else
                    {
                      html += '<td><input type="checkbox" class="checkbox-selected" value="'+response.data[i].Code+'"></td>';
                    }

                    if(response.data[i].BTSCode)
                    {
                      selected_customer
                      html += '<td>'+response.data[i].BTSCode+'</td>';
                    }
                    else
                    {
                      html += '<td></td>';
                    }

                    if(response.data[i].BTSCode)
                    {
                      html += '<td>'+response.data[i].BTSCode+'</td>';
                    }
                    else
                    {
                      html += '<td></td>';
                    }

                    if(response.data[i].BTSCode)
                    {
                      html += '<td>'+response.data[i].BTSCode+'</td>';
                    }
                    else
                    {
                      html += '<td></td>';
                    }

                    if(response.data[i].BTSCode)
                    {
                      html += '<td>'+response.data[i].BTSCode+'</td>';
                    }
                    else
                    {
                      html += '<td></td>';
                    }

                    if(response.data[i].BTSCode)
                    {
                      html += '<td>'+response.data[i].BTSCode+'</td>';
                    }
                    else
                    {
                      html += '<td></td>';
                    }

                    if(response.data[i].BTSCode)
                    {
                      html += '<td>'+response.data[i].BTSCode+'</td>';
                    }
                    else
                    {
                      html += '<td></td>';
                    }

                    html += '</tr>';
                  }
                  checkbox_selected = [];
                  $('#customer_noc_list').html(html);

                }
                else
                {

                }
              },
              error: function(xhr, ajaxOptions, thrownError)
              {

              }
            });
          });

        $('#ticket_category').on('change', function() {
            if ($('#ticket_category').val() == "") {
                $('#div-customer').attr('hidden', '');
                $('#div-bts').attr('hidden', '');
                $('#div-olt').attr('hidden', '');
                $('#div-search').attr('hidden', '');
                $('#div-device-type-fo').attr('hidden', '');
                $('#div-device-type-wll').attr('hidden', '');
            } else if ($('#ticket_category').val() == 'MINOR') {
                $('#div-customer').removeAttr('hidden');
                $('#div-bts').removeAttr('hidden');
                $('#div-olt').removeAttr('hidden');
                $('#div-search').removeAttr('hidden');
                $('#div-device-type-fo').removeAttr('hidden');
                $('#div-device-type-wll').removeAttr('hidden');
            } else if ($('#ticket_category').val() == 'CRITICAL') {
                $('#div-customer').removeAttr('hidden');
                $('#div-bts').removeAttr('hidden');
                $('#div-olt').removeAttr('hidden');
                $('#div-search').removeAttr('hidden');
                $('#div-device-type-fo').removeAttr('hidden');
                $('#div-device-type-wll').removeAttr('hidden');
            } else if ($('#ticket_category').val() == 'MAJOR') {
                $('#div-customer').removeAttr('hidden');
                $('#div-bts').removeAttr('hidden');
                $('#div-olt').removeAttr('hidden');
                $('#div-search').removeAttr('hidden');
                $('#div-device-type-fo').removeAttr('hidden');
                $('#div-device-type-wll').removeAttr('hidden');
            } else if ($('#ticket_category').val() == 'PLN') {
                $('#div-customer').attr('hidden', '');
                $('#div-bts').removeAttr('hidden');
                $('#div-olt').removeAttr('hidden');
                $('#div-search').removeAttr('hidden');
                $('#div-device-type-fo').removeAttr('hidden');
                $('#div-device-type-wll').removeAttr('hidden');
            }
        });
        $('#ticket_category').change();

            $('.ticket_category').select2({
                'placeholder' : 'Choose One',
                'width' : '100%',
                tags : true,
                'allowClear' : true
            });

            $('#branch').select2({
                'placeholder' : 'Choose One',
                'width' : '100%',
                tags : true,
                'allowClear' : true
            });

            $('#problemType').select2({
                'placeholder' : 'Choose One',
                'width' : '100%',
                tags : true,
                'allowClear' : true
            });

            $('#open_customer').select2({
                'placeholder' : 'Choose One',
                'width' : '100%',
                tags : true,
                'allowClear' : true
            });

            $('#open_bts').select2({
                'placeholder' : 'Choose One',
                'width' : '100%',
                tags : true,
                'allowClear' : true
            });

            $('#olt_host').select2({
                'placeholder' : 'Choose One',
                'width' : '100%',
                tags : true,
                'allowClear' : true
            });

            $('#open_device_type_fo').select2({
                'placeholder' : 'Choose One',
                'width' : '100%',
                tags : true,
                'allowClear' : true
            });

            $('#open_device_type_wll').select2({
                'placeholder' : 'Choose One',
                'width' : '100%',
                tags : true,
                'allowClear' : true
            });
        });

		var KTBootstrapDatepicker = function () {
			var arrows;
			if (KTUtil.isRTL()) {
				arrows = {
					leftArrow: '<i class="la la-angle-right"></i>',
					rightArrow: '<i class="la la-angle-left"></i>'
				}
			} else {
				arrows = {
					leftArrow: '<i class="la la-angle-left"></i>',
					rightArrow: '<i class="la la-angle-right"></i>'
				}
			}

			// Private functions
			var demos = function () {
				$('#TransactionDate').datepicker({
					rtl: KTUtil.isRTL(),
                    format: 'dd-mm-yyyy',
					todayHighlight: true,
					orientation: "bottom left",
					templates: arrows
				});

				$('#ScheduleDate').datepicker({
					rtl: KTUtil.isRTL(),
                    format: 'dd-mm-yyyy',
					todayHighlight: true,
					orientation: "bottom left",
					templates: arrows
				});
			}

			return {
				// public functions
				init: function() {
					demos();
				}
			};
		}();

		jQuery(document).ready(function() {
			KTBootstrapDatepicker.init();
		});
    </script>
@endsection
