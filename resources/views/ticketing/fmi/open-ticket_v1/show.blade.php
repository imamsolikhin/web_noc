@extends('layout.default')

@section('content')
	<div class="subheader py-2 {{ Metronic::printClasses('subheader', false) }}" id="kt_subheader">
	    <div class="{{ Metronic::printClasses('subheader-container', false) }} d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
	        <div class="d-flex align-items-baseline flex-wrap mr-5">
	            <h5 class="text-dark font-weight-bold my-1 mr-5">
	                Ticket Status                               
	            </h5>
	            <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
	                <li class="breadcrumb-item">
	                    <a href="{{ route('dashboard') }}" class="text-muted">Home</a>
	                </li>
	                <li class="breadcrumb-item">
	                    <a href="" class="text-muted">Ticket Status</a>
	                </li>
	            </ul>
	        </div>
	    </div>
	</div>

	<form class="form" action="{{ route('ticketing.fmi.ticket-open.status', $fMIOpenTicket->code) }}" method="POST">
		{!! csrf_field() !!}
	    {!! method_field('PUT') !!}
		<div class="card card-custom mb-0">
			<div class="card-header border-1 pt-6 pb-0">
			    <div class="card-title">
			        <h3 class="card-label">Ticket Status
			        </h3>
			    </div>
			</div>
			<div class="card-body">
			    <div class="mb-1">
			        <div class="form-group row">
			            <div class="col-lg-4">
			                <label>TCK No</label>
			                <input type="text" class="form-control" name="tck_no" value="{{ old('tck_no', $fMIOpenTicket->code) }}" disabled>
			            </div>

			            <div class="col-lg-4">
	                        <label>Open Ticket Date</label>
							<div class="input-group date" >
							    <input type="text" id="TransactionDate" class="form-control" readonly  placeholder="Select date" name="TransactionDate" value="{{old('TransactionDate', $transactionDate)}}" disabled/>
							    <div class="input-group-append">
							        <span class="input-group-text">
							        <i class="la la-calendar-check-o"></i>
							        </span>
							    </div>
							</div>
			            </div>
			        </div>

			        <div class="form-group row">
			            <div class="col-lg-6">
			                <label>Ticket Category</label>
			                @foreach (['CRITICAL', 'MAJOR', 'PLN'] as $ticketCategory)
			                <div>
			                	<input type="radio" name="TicketCategory" value="{{ $ticketCategory }}" class="ticketCategory" data-id="{{ $ticketCategory }}" id="{{ $ticketCategory }}" disabled
			                	@if ($ticketCategory == $fMIOpenTicket->TicketCategory)
			                		checked
			                	@endif
			                	> <label for="{{ $ticketCategory }}">{{ $ticketCategory }}</label>
			                </div>
			                @endforeach
			            </div>
			        </div>

			        <div class="form-group row">
	                    <div class="col-lg-6">
	                        <label>Schedule Date</label>
							<div class="input-group date" >
							    <input type="text" id="ScheduleDate" class="form-control" readonly  placeholder="Select date" name="ScheduleDate" value="{{old('ScheduleDate', $scheduleDate)}}" disabled/>
							    <div class="input-group-append">
							        <span class="input-group-text">
							        <i class="la la-calendar-check-o"></i>
							        </span>
							    </div>
							</div>
	                    </div>
			        </div> 

			        <div class="form-group row">
			            <div class="col-lg-6">
			                <label>Branch</label>
			                <select class="form-control select2" id="branch" name="branch" data-placeholder="Choose One" disabled>
			                	<option></option>
			                    @foreach($branchs as $list)
			                    <option value="{{$list->Code}}" {{ old('branch', $fMIOpenTicket->BranchCode) == $list->Code ? 'selected' : '' }}>{{ $list->Name }}</option>
			                    @endforeach
			                </select>
			            </div>
			            <div class="col-lg-6">
			                <label>Problem Type</label>
			                <select class="form-control" id="problemType" name="problemType" data-placeholder="Choose One" disabled>
			                	<option></option>
			                    @foreach($problemTypes as $list)
			                    <option value="{{$list->Code}}"  {{ old('problemType', $fMIOpenTicket->ProblemTypeCode) == $list->Code ? 'selected' : '' }}>{{ $list->name }}</option>
			                    @endforeach
			                </select>
			            </div>
			        </div>

			        <div class="form-group row">
			            <div class="col-lg-6">
			                <label>Responsibility Department</label>
			                <select class="form-control" id="responsibilityDepartmentCode" name="responsibilityDepartmentCode" data-placeholder="Choose One" disabled>
			                	<option></option>
			                    @foreach($departments as $list)
			                    <option value="{{$list->Code}}" {{ old('responsibilityDepartmentCode', $fMIOpenTicket->ResponsibilityDepartmentCode) == $list->Code ? 'selected' : '' }}>{{ $list->Name }}</option>
			                    @endforeach
			                </select>
			            </div>
			            <div class="col-lg-6">
			                <label>TCK Ref No</label>
			                <input type="text" class="form-control" name="ref_no" value="{{ old('ref_no', $fMIOpenTicket->RefNo) }}" placeholder="Enter Ref No" / disabled>
			            </div>
			        </div>

			        <div class="form-group row">
			            <div class="col-lg-6">
			                <label>Ticket Description</label>
			                <textarea class="form-control" rows="5" name="ticket_description" disabled>{{ $fMIOpenTicket->TicketDescription }}</textarea>
			            </div> 

			            <div class="col-lg-6">
			                <label>TCK Remark</label>
			                <textarea class="form-control" rows="5" name="Remark" disabled>{{ $fMIOpenTicket->Remark }}</textarea>
			            </div>
			        </div>

			        <div class="form-group row">
			            <div class="col-lg-6">
			                <label>Status</label>
			                <div>
			                    <input type="radio" name="TicketStatus" value="OPEN" class="TicketStatus" data-id='OPEN' id="OPEN"> <label for="OPEN">OPEN</label>
			                </div>
			                <div>
			                    <input type="radio" name="TicketStatus" value="OPC" class="TicketStatus" data-id='OPC' id="OPC"> <label for="OPC">OPC</label>
			                </div>
			                <div>
			                    <input type="radio" name="TicketStatus" value="OPM" class="TicketStatus" data-id='OPM' id="OPM"> <label for="OPM">OPM</label>
			                </div>
			                <div>
			                    <input type="radio" name="TicketStatus" value="CLOSED" class="TicketStatus" data-id='CLOSED' id="CLOSED"> <label for="CLOSED">CLOSED</label>
			                </div>
			                <div>
			                    <input type="radio" name="TicketStatus" value="POSTPONED" class="TicketStatus" data-id='POSTPONED' id="POSTPONED"> <label for="POSTPONED">POSTPONED</label>
			                </div>
			            </div>
			        </div>

			        <div class="form-group row">
			            <div class="col-lg-6">
			                <label>Ref No</label>
			                <input type="text" class="form-control" name="ref_no_status" value="{{ old('ref_no_status') }}" placeholder="Enter Ref No" / >
			            </div>
			        </div>

			        <div class="form-group row">
			            <div class="col-lg-6">
			                <label>Remark</label>
			                <textarea class="form-control" rows="5" name="remark_staus"></textarea>
			            </div>
			        </div>
			    </div>
			</div>
		</div>

		<div class="card card-custom mt-5">
	        <div class="card-header flex-wrap border-1 pt-6 pb-0">
	            <div class="card-title">
	                <h3 class="card-label">Ticket Status  
	                </h3>
	            </div>
	        </div>
	        <div class="card-body">
	            <div class="table-responsive">
	                <table style="width: 1070px !important;" class="table table-borderless table-vertical-center" id="datatable-status">
	                    <thead>
	                        <tr>
	                           <th>Code</th>
	                           <th>Update Date</th>
	                           <th>Update By</th>
	                           <th>Status</th>
	                        </tr>
	                    </thead>
	                </table>
	            </div>
	        </div>
			<div class="card-footer">
			    <div class="row">
			        <div class="col-lg-6">
			            <a href="{{ route('ticketing.fmi.ticket-open.index') }}" id="cancel-btn" class="btn btn-danger">Back</a>
			        </div>
			        <div class="col-lg-6 text-right">
			            <button type="submit" class="btn btn-primary mr-2">Update Status</button>
			        </div>
			    </div>
			</div>
		</div>
	</form>
@endsection

{{-- Styles Section --}}
@section('styles')
    <link rel="stylesheet" href="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.css">
    <link rel="stylesheet" href="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.min.css">
    <link rel="stylesheet" href="{{ config('app.url') }}plugins/custom/datatables/datatables.bundle.css">
@endsection

{{-- Scripts Section --}}
@section('scripts')
    @include ('inc.confirm-delete-modal')
    <script src="{{ config('app.url') }}global/vendor/datatables/jquery.dataTables.js"></script>
    <script src="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.js"></script>
    <script src="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
            $('#datatable-status').dataTable({
                pageLength: 10,
                processing: true,
                serverSide: true,
                responsive: false,
                ajax: {
                    method: 'POST',
                    url : '{{ route('ticketing.fmi.ticket-open.data-status', $fMIOpenTicket->code) }}',
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                },
                columns : [
                    { title:'Code',  data: 'code', name: 'code', defaultContent: '-', class: 'text-center' },
                    { title: 'Transaction Date', data: 'transaction_date', name: 'transaction_date', defaultContent: '-', class: 'text-center', searchable: false },
                    { title: 'Update By', data: 'CreatedBy', name: 'CreatedBy', defaultContent: '-', class: 'text-center', searchable: false },
                    { title: 'Status', data: 'TicketStatus', name: 'TicketStatus', defaultContent: '-', class: 'text-center', searchable: false }
                ]
            });
			$('.ticketCategory').on('change', function() {
				if ($(this).data('id') == "") {
					$('#div-customer').attr('hidden', '');
				} else if ($(this).data('id') == "PLN") {
					$('#div-customer').attr('hidden', '');
				} else if($(this).data('id') == "CRITICAL" || $(this).data('id') == "MAJOR") {
					$('#div-customer').removeAttr('hidden');
				}
			});

			$("#checkAll-bts").change(function () {
			    $(".filter-bts").prop('checked', $(this).prop("checked"));
			});

			$("#checkAll-fo").change(function () {
				$(".filter-fo").prop('checked', $(this).prop("checked"));
			});

			$("#checkAll-wll").change(function () {
				$(".filter-wll").prop('checked', $(this).prop("checked"));
			});

            $('#branch').select2({
                'placeholder' : 'Choose One',
                'width' : '100%',
                tags : true,
                'allowClear' : true
            });

            $('#problemType').select2({
                'placeholder' : 'Choose One',
                'width' : '100%',
                tags : true,
                'allowClear' : true
            });

            $('#responsibilityDepartmentCode').select2({
                'placeholder' : 'Choose One',
                'width' : '100%',
                tags : true,
                'allowClear' : true
            });
		});

		var KTBootstrapDatepicker = function () {
			var arrows;
			if (KTUtil.isRTL()) {
				arrows = {
					leftArrow: '<i class="la la-angle-right"></i>',
					rightArrow: '<i class="la la-angle-left"></i>'
				}
			} else {
				arrows = {
					leftArrow: '<i class="la la-angle-left"></i>',
					rightArrow: '<i class="la la-angle-right"></i>'
				}
			}
			
			// Private functions
			var demos = function () {
				$('#TransactionDate').datepicker({
					rtl: KTUtil.isRTL(),
                    format: 'dd-mm-yyyy',
					todayHighlight: true,
					orientation: "bottom left",
					templates: arrows
				});

				$('#ScheduleDate').datepicker({
					rtl: KTUtil.isRTL(),
                    format: 'dd-mm-yyyy',
					todayHighlight: true,
					orientation: "bottom left",
					templates: arrows
				});
			}

			return {
				// public functions
				init: function() {
					demos();
				}
			};
		}();

		jQuery(document).ready(function() {
			KTBootstrapDatepicker.init();
		});
	</script>
@endsection
