@extends('layout.default')

@section('content')
    <div class="subheader py-2 {{ Metronic::printClasses('subheader', false) }}" id="kt_subheader">
        <div class="{{ Metronic::printClasses('subheader-container', false) }} d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-baseline flex-wrap mr-5">
                <h5 class="text-dark font-weight-bold my-1 mr-5">
                    FMI
                </h5>
                <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                    <li class="breadcrumb-item">
                        <a href="{{ route('dashboard') }}" class="text-muted">Home</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="{{ route('ticketing.fmi.task-assignment.index') }}" class="text-muted">Ticketing</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="" class="text-muted">Task Assignment</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    @include('inc.success-notif')
    <div class="card card-custom">
        <div class="card-header flex-wrap border-1 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Task Assignment - Search Option:
                    <div class="text-muted pt-2 font-size-sm">search option for data</div>
                </h3>
            </div>
            <div class="card-toolbar">

            </div>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-12">
              <div class="row">
                <div class="col-lg-4 col-sm-12">
                  <div class="row">
                    <div class="col-12">
                      <div class="form-group">
                        <label class="font-weight-bolder">TCK No:</label>
                        <input class="form-control" type="text" id="tck_no">
                      </div>
                    </div>
                    <div class="col-6">
                      <div class="form-group">
                        <label class="font-weight-bolder">Periode:</label>
                        <input class="form-control datepicker" style="width: 100%" type="date" id="periode">
                      </div>
                    </div>
                    <div class="col-6">
                      <div class="form-group">
                        <label class="font-weight-bolder">Up to:</label>
                        <input class="form-control datepicker" style="width: 100%" type="date" id="up_to">
                      </div>
                    </div>
                    <div class="col-12">
                      <label class="mr-4">Action: </label>
                      <a href="{{ route('ticketing.fmi.task-assignment.create') }}" class="btn btn-warning py-2 text-light" id="add-btn">
                          <i class="menu-icon">
                              <span class="svg-icon svg-icon-md">
                                  <!--begin::Svg Icon | path:media/svg/icons/Design/Flatten.svg-->
                                  <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                      <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                          <rect x="0" y="0" width="24" height="24"/>
                                          <circle fill="#000000" cx="9" cy="15" r="6"/>
                                          <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3"/>
                                      </g>
                                  </svg>
                                  <!--end::Svg Icon-->
                              </span>
                          </i>
                          New Task Assignment
                      </a>
                    </div>
                  </div>
                </div>
                <div class="col-lg-4 col-sm-12">
                  <div class="row">
                    <div class="col-12">
                      <div class="form-group">
                        <label class="font-weight-bolder">Customer Name:</label>
                        <input class="form-control" type="text" id="customer_name">
                      </div>
                    </div>
                    <div class="col-12">
                      <div class="form-group">
                        <label class="font-weight-bolder">Customer Code:</label>
                        <input class="form-control" type="text" id="customer_code">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-4 col-sm-12">
                  <div class="row">
                    <div class="row">
                      <div class="col-12">
                        <div class="form-group">
                          <label class="font-weight-bolder">Ref No:</label>
                          <input class="form-control" type="text" id="ref_no">
                        </div>
                      </div>
                      <div class="col-12">
                        <div class="form-group">
                          <label class="font-weight-bolder">Remark:</label>
                          <input class="form-control" type="text" id="remark">
                          <button class="btn btn-success float-right mt-4 mb-4"><i class="fa fa-search"></i> Search</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12">
              <div class="table-responsive">
                  <table style="width: 1070px !important;" class="table table-borderless table-vertical-center" id="datatable">
                      <thead>
                          <tr>
                             <th>TCK NO</th>
                             <th>Transaction Date</th>
                             <th>TCK Category</th>
                             <th>Action</th>
                          </tr>
                      </thead>
                  </table>
              </div>
            </div>
          </div>
        </div>
    </div>
@endsection

{{-- Styles Section --}}
@section('styles')
  <link rel="stylesheet" href="{{ config('app.url') }}plugins/custom/datatables/datatables.bundle.css">
@endsection

{{-- Scripts Section --}}
@section('scripts')
    @include ('inc.confirm-delete-modal')
    <script src="{{ config('app.url') }}global/vendor/datatables/jquery.dataTables.js"></script>
    <script src="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.js"></script>
    <script src="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.js"></script>
    <script src="{{ config('app.url') }}plugins/custom/datatables/datatables.bundle.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable').dataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    method: 'POST',
                    url : '{{ route('ticketing.fmi.task-assignment.data') }}',
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                },
                columns : [
                    { title:'TSK No',  data: 'code', name: 'code', defaultContent: '-', class: 'text-center' },
                    { title: 'Transaction Date', data: 'transaction_date', name: 'transaction_date', defaultContent: '-', class: 'text-center', searchable: false },
                    { title: 'TCK Kategory', data: 'TicketCategory', name: 'TicketCategory', defaultContent: '-', class: 'text-center', searchable: false },
                    { title:'Action', data: 'action', name: 'action', searchable: false, orderable: false, class: 'text-center' }
                ],
                initComplete: function() {
                    $('.tl-tip').tooltip();
                    @if (count($errors) > 0)
                        jQuery("html, body").animate({
                            scrollTop: $('#add-form').offset().top - 100 // 66 for sticky bar height
                        }, "slow");
                    @endif
                }
            });
            $('#add-btn').click(function(e) {
                $('#add-form').toggle();
                jQuery("html, body").animate({
                    scrollTop: $('#add-form').offset().top - 100 // 66 for sticky bar height
                }, "slow");
            });

            $('#cancel-btn').click(function(e) {
                $('#add-form').toggle();
                jQuery("html, body").animate({
                    scrollTop: $('body').offset().top - 100 // 66 for sticky bar height
                }, "slow");
            });
        });
    </script>
@endsection
