@extends('layout.default')

@section('content')
<div class="subheader py-2 {{ Metronic::printClasses('subheader', false) }}" id="kt_subheader">
  <div class="{{ Metronic::printClasses('subheader-container', false) }} d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
    <div class="d-flex align-items-baseline flex-wrap mr-5">
      <h5 class="text-dark font-weight-bold my-1 mr-5">
        FMI
      </h5>
      <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
        <li class="breadcrumb-item">
          <a href="{{ route('dashboard') }}" class="text-muted">Home</a>
        </li>
        <li class="breadcrumb-item">
          <a href="{{ route('ticketing.fmi.ticket-open.index') }}" class="text-muted">Open Ticket</a>
        </li>
        <li class="breadcrumb-item">
          <a href="" class="text-muted">Status Open Ticket</a>
        </li>
      </ul>
    </div>
  </div>
</div>
<div class="card card-custom mb-0">
  @include('inc.error-list')
  <form class="form" action="{{ route('ticketing.fmi.ticket-open.store') }}" method="POST">
    {!! csrf_field() !!}
    <div class="card-header border-1 pt-6 pb-0 bg-danger">
      <div class="card-title">
        <h3 class="card-label text-white">Status Open Ticket <span class="btn btn-success font-weight-bolder ml-3">{{@$fMITicketStatus[0]->TicketStatus}}</span>

        </h3>
      </div>
    </div>
    <div class="card-body">
      <div class="mb-1">
        <div class="form-group row">
          <div class="col-lg-6">
            <label class="font-weight-bolder">TCK No <span class="text-danger">*</span></label>
            <input disabled type="text" class="form-control" name="tck_no" value="{{ old('tck_no',$fMIOpenTicket->code) }}" placeholder="Enter TCK No" / required>
          </div>
          <div class="col-lg-6">
            <label class="font-weight-bolder">Open Ticket Date <span class="text-danger">*</span></label>
            <div class="input-group date" >
              <input disabled type="text" id="TransactionDate" class="form-control" readonly  placeholder="Select date" name="TransactionDate" value="{{old('TransactionDate',date('Y-m-d',strtotime($fMIOpenTicket->TransactionDate)))}}" />
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="la la-calendar-check-o"></i>
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="mb-1">
        <div class="form-group row">
          <div class="col-lg-6">
            <label class="font-weight-bolder">Ticket Category <span class="text-danger">*</span></label>
            <br>
            <select disabled class="form-control select2 ticket_category" id="ticket_category" name="TicketCategory" data-placeholder="Choose One" required>
              <option></option>
              <option value="CRITICAL" {{ old('ticket_category',$fMIOpenTicket->TicketCategory) == 'CRITICAL' ? 'selected' : '' }}>Critical</option>
              <option value="MAJOR" {{ old('ticket_category',$fMIOpenTicket->TicketCategory) == 'MAJOR' ? 'selected' : '' }}>Major</option>
              <option value="MINOR" {{ old('ticket_category',$fMIOpenTicket->TicketCategory) == 'MINOR' ? 'selected' : '' }}>Minor</option>
              <option value="PLN" {{ old('ticket_category',$fMIOpenTicket->TicketCategory) == 'PLN' ? 'selected' : '' }}>PLN</option>
            </select>
          </div>
          <div class="col-lg-6">
            <label class="font-weight-bolder">Schedule Date <span class="text-danger">*</span></label>
            <div class="input-group date" >
              <input disabled type="text" id="ScheduleDate" class="form-control" readonly  placeholder="Select date" name="ScheduleDate" value="{{old('ScheduleDate',date('Y-m-d',strtotime($fMIOpenTicket->ScheduleDate)))}}" />
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="la la-calendar-check-o"></i>
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="mb-1">
        <div class="form-group row">
          <div class="col-lg-6">
            <label class="font-weight-bolder">Branch <span class="text-danger">*</span></label>
            <select disabled class="form-control select2" id="branch" name="branch" data-placeholder="Choose One">
              <option></option>
              @foreach($branchs as $branch)
              <option value="{{ $branch->Code }}" {{ old('branch',$fMIOpenTicket->BranchCode) == $branch->Code ? 'selected' : '' }}>{{ $branch->Name }}</option>
              @endforeach
            </select>
          </div>
          <div class="col-lg-6">
            <label class="font-weight-bolder">Problem Type <span class="text-danger">*</span></label>
            <select disabled class="form-control select2" id="problemType" name="problemType" data-placeholder="Choose One">
              <option></option>
              @foreach($problemTypes as $problemType)
              <option value="{{ $problemType->Code }}" {{ old('problemType',$fMIOpenTicket->ProblemTypeCode) == $problemType->Code ? 'selected' : '' }}>{{ $problemType->name }}</option>
              @endforeach
            </select>
          </div>
        </div>
      </div>
      <div class="mb-1">
        <div class="form-group row">
          <div class="col-lg-6" id="div-customer">
            <label class="font-weight-bolder">Customer <span class="text-danger">*</span></label>
            <select disabled class="form-control select2" id="open_customer" name="open_customer" data-placeholder="Choose One">
              <option></option>
              @foreach($openCustomers as $openCustomer)
              <option value="{{ $openCustomer->code }}" {{ old('code') == $openCustomer->code ? 'selected' : '' }}>{{ $openCustomer->CustomerCode }}</option>
              @endforeach
            </select>
          </div>
          <div class="col-lg-6">
            <label class="font-weight-bolder">Responsibility Department <span class="text-danger">*</span></label>
            <select disabled class="form-control select2" id="ResponsibilityDepartmentCode" name="ResponsibilityDepartmentCode" data-placeholder="Choose One">
              @foreach($departments as $department)
              <option value="{{ $department->Code }}" {{ old('ResponsibilityDepartmentCode') == $department->Code ? 'selected' : '' }}>{{ $department->Name }}</option>
              @endforeach
            </select>
          </div>
        </div>
      </div>
      <div class="mb-1">
        <div class="form-group row">
          <div class="col-lg-6" id="div-bts">
            <label class="font-weight-bolder">BTS <span class="text-danger">*</span></label>
            <select disabled class="form-control select2" id="open_bts" name="open_bts" multiple data-placeholder="Choose needed Item">
              @foreach($baseTransmissionStations as $bts)
              <option value="{{ $bts->Code }}" {{ old('bts') == $bts->Code ? 'selected' : '' }}>{{ $bts->Name }}</option>
              @endforeach
            </select>
          </div>
          <div class="col-lg-6" id="div-olt">
            <label class="font-weight-bolder">OLT <span class="text-danger">*</span></label>
            <select disabled class="form-control select2" id="olt_host" name="olt_host" multiple data-placeholder="Choose needed item">
              @foreach($oltHost as $oh)
              <option value="{{ $oh->Code }}" {{ old('oh') == $oh->Code ? 'selected' : '' }}>{{ $oh->Hostname }}</option>
              @endforeach
            </select>
          </div>
          <div class="col-lg-12">
            <hr>
            <table class="table table-stripped">
              <thead class="bg-light text-dark rounded">
                <tr>
                  <th>Customer ID</th>
                  <th>Customer Name</th>
                  <th>Contact Person</th>
                  <th>Address</th>
                  <th>BTS</th>
                  <th>OLT</th>
                </tr>
              </thead>
              <tbody id="customer_noc_list">
                @foreach($openCustomers as $key => $row)
                <tr>
                  <td>{{$row->CustomerCode}}</td>
                  <td>{{$row->CustomerCode}}</td>
                  <td>{{$row->CustomerCode}}</td>
                  <td>{{$row->CustomerCode}}</td>
                  <td>{{$row->CustomerCode}}</td>
                  <td>{{$row->CustomerCode}}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
        <div class="mb-1">
          <div class="form-group row">
            <div class="col-lg-6">
              <label class="font-weight-bolder">Ref No</label>
              <input disabled type="text" class="form-control" name="ref_no" value="{{ old('ref_no',$fMIOpenTicket->RefNo) }}" placeholder="Enter Ref No" />
            </div>
          </div>
        </div>

        <div class="mb-1">
          <div class="form-group row">
            <div class="col-lg-6">
              <label class="font-weight-bolder">Ticket Description</label>
              <textarea disabled  class="form-control" rows="5" name="ticket_description">{{ @$fMIOpenTicket->TicketDescription }}</textarea disabled >
              </div>
              <div class="col-lg-6">
                <label class="font-weight-bolder">Remark</label>
                <textarea disabled  class="form-control" rows="5" name="Remark">{{ @$fMIOpenTicket->Remark }}</textarea disabled >
                </div>
              </div>
            </div>
          </div>
          <div class="card-footer">
            <div class="row">
              <div class="col-lg-6">
                <a href="{{ route('ticketing.fmi.ticket-open.index') }}" id="cancel-btn" class="btn btn-danger">Back</a>
              </div>
              <!-- <div class="col-lg-6 text-right">
              <button type="submit" class="btn btn-primary mr-2">Submit</button>
            </div> -->
          </div>
        </div>
      </form>
    </div>
    @endsection

    @section('styles')

    @endsection

    @section('scripts')

    <script type="text/javascript">
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    </script>
    <script type="text/javascript">
    var selectedBts = new Array();
    @foreach($openBts as $key => $row)
    @php echo 'selectedBts['.$key.'] = "'.$row->BTSCode.'";'; @endphp
    @endforeach

    var selectedOlt = new Array();
    @foreach($openOlt as $key => $row)
    @php echo 'selectedOlt['.$key.'] = "'.explode('/',$row->code)[0].'";'; @endphp
    @endforeach

    var selectedCustomer = new Array();
    @foreach($openCustomers as $key => $row)
    @php echo 'selectedCustomer['.$key.'] = "'.$row->CustomerCode.'";'; @endphp
    @endforeach

    function do_select_customer()
    {
      var html = '';
      //var list = '';
      $('#customer-item').html(html);
      //$('#customer_id_list').html(list);
      for(var i = 0; i < checkbox_selected.length; i++)
      {
        //list += '<li>' + checkbox_selected[i] + '</li>';
        html += '<input disabled type="hidden" readonly name="CustCode[]" value="'+checkbox_selected[i]+'">';
      }
      //$('#customer_id_list').html(list);
      $('#customer-item').html(html);
      //$('#modalFindCustomer').modal('hide');
    }

    Array.prototype.remove = function() {
      var what, a = arguments, L = a.length, ax;
      while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
          this.splice(ax, 1);
        }
      }
      return this;
    };

    var checkbox_selected=[];

    $(document).on('change','.checkbox-selected',function(){
      if($(this).prop('checked')==true)
      {
        checkbox_selected.push($(this).val());
      }
      else
      {
        checkbox_selected.remove($(this).val());
      }
      do_select_customer();
    });

    $(document).ready(function() {
      var selected_customer = [];
      // $('#open_bts, #olt_host').on('change',function(){
      //   $.ajax({
      //     url: '{{route("ticketing.fmi.ticket-open.get-by-olt-bts")}}',
      //     method: 'POST',
      //     enctype: 'multipart/form-data',
      //     data: {
      //       "_token": "{{ csrf_token() }}",
      //       "open_bts": $('#open_bts').val(),
      //       'olt_host': $('#olt_host').val()
      //     },
      //     processData: true,
      //     cache: false,
      //     beforeSend: function()
      //     {
      //       $('#customer_noc_list').html('<tr><td colspan="7"><div class="progress">\
      //           <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">Fetch Data</div>\
      //         </div></td></tr>'
      //       );
      //     },
      //     success: function(response)
      //     {
      //       if(response.status == 'true')
      //       {
      //         var html = '';
      //         $('#customer_noc_list').html(html);
      //         for(var i = 0; i < response.data.length; i++)
      //         {
      //           html += '<tr>';
      //
      //           if(response.data[i].Code)
      //           {
      //             html += '<td><input disabled type="checkbox" class="checkbox-selected" value="'+response.data[i].Code+'"></td>';
      //           }
      //           else
      //           {
      //             html += '<td><input disabled type="checkbox" class="checkbox-selected" value="'+response.data[i].Code+'"></td>';
      //           }
      //
      //           if(response.data[i].Code)
      //           {
      //             selected_customer
      //             html += '<td>'+response.data[i].Code+'</td>';
      //           }
      //           else
      //           {
      //             html += '<td></td>';
      //           }
      //
      //           if(response.data[i].Code)
      //           {
      //             selected_customer
      //             html += '<td>'+response.data[i].Code+'</td>';
      //           }
      //           else
      //           {
      //             html += '<td></td>';
      //           }
      //
      //           if(response.data[i].Code)
      //           {
      //             selected_customer
      //             html += '<td>'+response.data[i].Code+'</td>';
      //           }
      //           else
      //           {
      //             html += '<td></td>';
      //           }
      //
      //           if(response.data[i].Code)
      //           {
      //             selected_customer
      //             html += '<td>'+response.data[i].Code+'</td>';
      //           }
      //           else
      //           {
      //             html += '<td></td>';
      //           }
      //
      //           if(response.data[i].Code)
      //           {
      //             selected_customer
      //             html += '<td>'+response.data[i].Code+'</td>';
      //           }
      //           else
      //           {
      //             html += '<td></td>';
      //           }
      //
      //           if(response.data[i].Code)
      //           {
      //             selected_customer
      //             html += '<td>'+response.data[i].Code+'</td>';
      //           }
      //           else
      //           {
      //             html += '<td></td>';
      //           }
      //
      //           html += '</tr>';
      //         }
      //         checkbox_selected = [];
      //         $('#customer_noc_list').html(html);
      //
      //       }
      //       else
      //       {
      //
      //       }
      //     },
      //     error: function(xhr, ajaxOptions, thrownError)
      //     {
      //
      //     }
      //   });
      // });

      $('#ticket_category').on('change', function() {
        if($(this).val() == 'MINOR')
        {
          $('#div-customer').slideDown();
        }
        else {
          $('#div-customer').slideUp();
        }
      });

      $('.ticket_category').select2({
        'placeholder' : 'Choose One',
        'width' : '100%',
        tags : true,
        'allowClear' : true
      });

      $('#branch').select2({
        'placeholder' : 'Choose One',
        'width' : '100%',
        tags : true,
        'allowClear' : true
      });

      $('#ResponsibilityDepartmentCode').select2({
        'placeholder' : 'Choose One',
        'width' : '100%',
        tags : true,
        'allowClear' : true
      });

      $('#problemType').select2({
        'placeholder' : 'Choose One',
        'width' : '100%',
        tags : true,
        'allowClear' : true
      });

      $('#open_customer').select2({
        'placeholder' : 'Choose One',
        'width' : '100%',
        tags : true,
        'allowClear' : true
      });

      $('#open_bts').select2({
        'placeholder' : 'Choose One',
        'width' : '100%',
        tags : true,
        'allowClear' : true
      });

      $('#olt_host').select2({
        'placeholder' : 'Choose One',
        'width' : '100%',
        tags : true,
        'allowClear' : true
      });

      $('#open_device_type_fo').select2({
        'placeholder' : 'Choose One',
        'width' : '100%',
        tags : true,
        'allowClear' : true
      });

      $('#open_device_type_wll').select2({
        'placeholder' : 'Choose One',
        'width' : '100%',
        tags : true,
        'allowClear' : true
      });
    });

    var KTBootstrapDatepicker = function () {
      var arrows;
      if (KTUtil.isRTL()) {
        arrows = {
          leftArrow: '<i class="la la-angle-right"></i>',
          rightArrow: '<i class="la la-angle-left"></i>'
        }
      } else {
        arrows = {
          leftArrow: '<i class="la la-angle-left"></i>',
          rightArrow: '<i class="la la-angle-right"></i>'
        }
      }

      // Private functions
      var demos = function () {
        $('#TransactionDate').datetimepicker({
          rtl: KTUtil.isRTL(),
          format: 'dd-mm-yyyy H:i:s',
          todayHighlight: true,
          orientation: "bottom left",
          templates: arrows
        });

        $('#ScheduleDate').datepicker({
          rtl: KTUtil.isRTL(),
          format: 'dd-mm-yyyy',
          todayHighlight: true,
          orientation: "bottom left",
          templates: arrows
        });
      }

      return {
        // public functions
        init: function() {
          demos();
        }
      };
    }();

    jQuery(document).ready(function() {
      KTBootstrapDatepicker.init();
      $('#open_bts').select2('val',selectedBts);
      $('#olt_host').select2('val',selectedOlt);
    });
    </script>
    @endsection
