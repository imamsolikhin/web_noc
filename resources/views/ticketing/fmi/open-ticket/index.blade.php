@extends('layout.default')

@section('content')
<div class="subheader py-2 {{ Metronic::printClasses('subheader', false) }}" id="kt_subheader">
    <div class="{{ Metronic::printClasses('subheader-container', false) }} d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
        <div class="d-flex align-items-baseline flex-wrap mr-5">
            <h5 class="text-dark font-weight-bold my-1 mr-5">
                FMI
            </h5>
            <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                <li class="breadcrumb-item">
                    <a href="{{ route('dashboard') }}" class="text-muted">Home</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="" class="text-muted">Ticket FMI</a>
                </li>
            </ul>
        </div>
    </div>
</div>
@include('inc.success-notif')
<div class="row">
    <div class="col-lg-12">
        <div class="row">

            <div class="col-lg-12">
                <div class="card card-custom">
                    <div class="card-header flex-wrap border-1 pt-6 pb-0 bg-danger">
                        <div class="card-title">
                            <h3 class="card-label text-white">Open Ticket List
                                <div class="text-white pt-2 font-size-sm">show data ticket</div>
                            </h3>
                        </div>
                        <div class="card-toolbar">
                            <a href="{{ route('ticketing.fmi.ticket-open.createType','NON-MINOR') }}" class="btn btn-dark py-2" id="add-btn">
                                <i class="menu-icon">
                                    <span class="svg-icon svg-icon-md">
                                        <!--begin::Svg Icon | path:media/svg/icons/Design/Flatten.svg-->
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <rect x="0" y="0" width="24" height="24"/>
                                                <circle fill="#000000" cx="9" cy="15" r="6"/>
                                                <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3"/>
                                            </g>
                                        </svg>
                                        <!--end::Svg Icon-->
                                    </span>
                                </i>
                                New Ticket Non Minor
                            </a>
                            <a href="{{ route('ticketing.fmi.ticket-open.createType','MINOR') }}" class="ml-2 btn btn-dark py-2" id="add-btn">
                                <i class="menu-icon">
                                    <span class="svg-icon svg-icon-md">
                                        <!--begin::Svg Icon | path:media/svg/icons/Design/Flatten.svg-->
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <rect x="0" y="0" width="24" height="24"/>
                                                <circle fill="#000000" cx="9" cy="15" r="6"/>
                                                <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3"/>
                                            </g>
                                        </svg>
                                        <!--end::Svg Icon-->
                                    </span>
                                </i>
                                New Ticket Minor
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-3 bg-secondary pt-4">
                                <div class="navi navi-bold navi-hover navi-active navi-link-rounded">
                                    <div class="navi-item mb-2">
                                        <b><h3>Summary: </h3></b>
                                    </div>
                                    <div class="navi-item mb-2">
                                    <a href="#" class="navi-link py-4 bg-light">
                                        <span class="navi-icon mr-2">
                                            <i class="flaticon2-warning"></i>
                                        </span>
                                        <span class="navi-text font-size-lg">Critical</span>
                                        <span class="navi-label">
                                            <span class="label label-light-danger label-rounded font-weight-bold" id="summary_critical">0</span>
                                        </span>
                                    </a>
                                    </div>
                                    <div class="navi-item mb-2">
                                    <a href="#" class="navi-link py-4 bg-light">
                                        <span class="navi-icon mr-2">
                                            <i class="flaticon2-information"></i>
                                        </span>
                                        <span class="navi-text font-size-lg">Major</span>
                                        <span class="navi-label">
                                            <span class="label label-light-danger label-rounded font-weight-bold" id="summary_major">0</span>
                                        </span>
                                    </a>
                                    </div>
                                    <div class="navi-item mb-2">
                                    <a href="#" class="navi-link py-4 bg-light">
                                        <span class="navi-icon mr-2">
                                            <i class="flaticon2-layers-2"></i>
                                        </span>
                                        <span class="navi-text font-size-lg">PLN</span>
                                        <span class="navi-label">
                                            <span class="label label-light-danger label-rounded font-weight-bold" id="summary_pln">0</span>
                                        </span>
                                    </a>
                                    </div>
                                    <div class="navi-item mb-2">
                                    <a href="#" class="navi-link py-4 bg-light">
                                        <span class="navi-icon mr-2">
                                            <i class="flaticon2-black-back-closed-envelope-shape"></i>
                                        </span>
                                        <span class="navi-text font-size-lg">Minor</span>
                                        <span class="navi-label">
                                            <span class="label label-light-danger label-rounded font-weight-bold" id="summary_minor">0</span>
                                        </span>
                                    </a>
                                    </div>
                                    <div class="navi-item mb-2">
                                        <div class="form-group mt-4">
                                            <h3>Ticket Status: </h3>
                                            <select class="form-control" id="ticketStatus">
                                                <option value="">--All--</option>
                                                <option value="OPEN">Open</option>
                                                <option value="OPC">On Progress CS</option>
                                                <option value="OPM">On Progress Maintenance</option>
                                                <option value="CLOSED">Closed</option>
                                                <option value="POSTPONED">Postponed</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-9">
                                <h3 class="mb-4"><i class="flaticon2-sms"></i> Critical / Major / PLN Ticket</h3>
                                <div class="table-responsive">
                                    <table style="width: 1070px !important;" class="table table-borderless table-vertical-center bg-light" id="datatable-non-minor">
                                        <thead>
                                            <tr>
                                               <th>TCK NO</th>
                                               <th>Transaction Date</th>
                                               <th>TCK Category</th>
                                               <th>Status</th>
                                               <th>Action</th>
                                           </tr>
                                       </thead>
                                   </table>
                               </div>
                            </div>
                            <div class="col-lg-12 mt-5">
                                <h3 class="mb-4"><i class="flaticon2-sms"></i> Minor Ticket</h3>
                                <div class="table-responsive">
                                    <table style="width: 1070px !important;" class="table table-borderless table-vertical-center bg-light" id="datatable-minor">
                                        <thead>
                                            <tr>
                                               <th>TCK NO</th>
                                               <th>Transaction Date</th>
                                               <th>TCK Category</th>
                                               <th>Customer Code</th>
                                               <th>Customer Name</th>
                                               <th>Status</th>
                                               <th>Action</th>
                                           </tr>
                                       </thead>
                                   </table>
                               </div>
                            </div>
                        </div>
                   </div>
               </div>
           </div>
       </div>

   </div>
</div>
<br>
@endsection

{{-- Styles Section --}}
@section('styles')
<link rel="stylesheet" href="{{ config('app.url') }}plugins/custom/datatables/datatables.bundle.css">
@endsection

{{-- Scripts Section --}}
@section('scripts')
@include ('inc.confirm-delete-modal')
<!-- <script src="{{ config('app.url') }}global/vendor/datatables/jquery.dataTables.js"></script> -->
<!-- <script src="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.js"></script> -->
<!-- <script src="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.js"></script> -->
<script src="{{ config('app.url') }}plugins/custom/datatables/datatables.bundle.js"></script>
<script type="text/javascript">
    var summary_critical = 0;
    var summary_major = 0;
    var summary_pln = 0;
    var summary_minor = 0;

    var dnm = $('#datatable-non-minor').dataTable({
        pageLength: 10,
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            method: 'POST',
            url : '{{ route('ticketing.fmi.data-non-minor.data') }}',
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            //async: false,
            beforeSend: function(){
                summary_critical = 0;
                summary_major = 0;
                summary_pln = 0;
                $('#summary_critical').html(summary_critical);
                $('#summary_major').html(summary_major);
                $('#summary_pln').html(summary_pln);
            }
        },
        columns : [
        { title:'TCK No',  data: 'code', name: 'code', defaultContent: '-', class: 'text-center' },
        { title: 'Transaction Date', data: 'transaction_date', name: 'transaction_date', defaultContent: '-', class: 'text-center', searchable: false },
        { title: 'TCK Category', data: 'TicketCategory', name: 'TicketCategory', defaultContent: '-', class: 'text-center', searchable: false },
        { title: 'Status', data: 'TicketStatus', name: 'TicketStatus', orderable: false, class: 'text-center' },
        { title:'Action', data: 'action', name: 'action', searchable: false, orderable: false, class: 'text-center' }
        ],
        columnDefs: [
        {
          targets: 2,
          render: function render(data, type, full, meta) {
            console.log(meta);
            if(meta == 0 && type === 'display')
            {
                summary_critical = 0;
                summary_major = 0;
                summary_pln = 0;
            }
            if(full.TicketCategory == 'CRITICAL' && type === 'display')
            {
                summary_critical++;
            }
            else if(full.TicketCategory == 'MAJOR' && type === 'display')
            {
                summary_major++;
            }
            else if(full.TicketCategory == 'PLN' && type === 'display')
            {
                summary_pln++;
            }
            $('#summary_critical').html(summary_critical);
            $('#summary_major').html(summary_major);
            $('#summary_pln').html(summary_pln);
            return data;
          }
        }],
    });

    var dm = $('#datatable-minor').dataTable({
        pageLength: 10,
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            method: 'POST',
            url : '{{ route('ticketing.fmi.data-minor.data') }}',
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            //async: false,
            beforeSend: function(){
                summary_minor = 0;
                $('#summary_minor').html(summary_minor);
            }
        },
        columns : [
        { data: 'code', name: 'code', defaultContent: '-', class: 'text-center' },
        { data: 'transaction_date', name: 'transaction_date', defaultContent: '-', class: 'text-center', searchable: false },
        { title: 'TCK Category', data: 'TicketCategory', name: 'TicketCategory', defaultContent: '-', class: 'text-center', searchable: false },
        { data: 'CustomerCode', name: 'CustomerCode', defaultContent: '-', class: 'text-center', searchable: true },
        { data: 'CustomerName', name: 'CustomerName', defaultContent: '-', class: 'text-center', searchable: true },
        { data: 'TicketStatus', name: 'TicketStatus', orderable: false, class: 'text-center' },
        { data: 'action', name: 'action', searchable: false, orderable: false, class: 'text-center' }
        ],
        columnDefs: [
        {
          targets: 2,
          render: function render(data, type, full, meta) {
            if(full.TicketCategory == 'MINOR' && type === 'display')
            {
                summary_minor++;   
            }
            $('#summary_minor').html(summary_minor);
            return data;
          }
        }],
    });

    $(document).ready(function() {
        //dnm.api().ajax.reload();
        //dm.api().ajax.reload();
        $('#ticketStatus').on('change',function(){
            dnm.api().columns(3).search('');
            dnm.api().columns(3).search($(this).val());
            dnm.api().ajax.reload();
            dm.api().columns(5).search('');
            dm.api().columns(5).search($(this).val());
            dm.api().ajax.reload();
        });

        $('#add-btn').click(function(e) {
            $('#add-form').toggle();
            jQuery("html, body").animate({
                    scrollTop: $('#add-form').offset().top - 100 // 66 for sticky bar height
                }, "slow");
        });

        $('#cancel-btn').click(function(e) {
            $('#add-form').toggle();
            jQuery("html, body").animate({
                    scrollTop: $('body').offset().top - 100 // 66 for sticky bar height
                }, "slow");
        });
    });
</script>
@endsection
