@extends('layout.default')

@section('content')
<div class="subheader py-2 {{ Metronic::printClasses('subheader', false) }}" id="kt_subheader">
  <div class="{{ Metronic::printClasses('subheader-container', false) }} d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
    <div class="d-flex align-items-baseline flex-wrap mr-5">
      <h5 class="text-dark font-weight-bold my-1 mr-5">
        FMI
      </h5>
      <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
        <li class="breadcrumb-item">
          <a href="{{ route('dashboard') }}" class="text-muted">Home</a>
        </li>
        <li class="breadcrumb-item">
          <a href="{{ route('ticketing.fmi.ticket-open.index') }}" class="text-muted">Open Ticket</a>
        </li>
        <li class="breadcrumb-item">
          <a href="" class="text-muted">Create Open Ticket</a>
        </li>
      </ul>
    </div>
  </div>
</div>
<div class="card card-custom mb-0">
  @include('inc.error-list')
  @include('inc.success-notif')
  <form class="form" action="{{ route('ticketing.fmi.ticket-open.store') }}" method="POST">
    {!! csrf_field() !!}
    <div class="card-header border-1 pt-6 pb-0 bg-danger">
      <div class="card-title">
        <h3 class="card-label text-white">Create Open Ticket {{ucwords(strtolower(@$ticketType))}}

        </h3>
      </div>
    </div>
    <div class="card-body">
      <div class="mb-1">
        <div class="form-group row">
          <div class="col-lg-6">
            <label class="font-weight-bolder">TCK No <span class="text-danger">*</span></label>
            <input type="hidden" class="form-control" name="tck_no" value="." readonly="" placeholder="Enter TCK No" / required>
            <input type="text" class="form-control" readonly="" placeholder="Will be generated">
          </div>
          <div class="col-lg-6">
            <label class="font-weight-bolder">Open Ticket Date <span class="text-danger">*</span></label>
            <div class="input-group date" >
              <input type="text" id="TransactionDate" class="form-control" readonly  placeholder="{{date('d-m-Y H:i:s')}}" name="TransactionDate" value="{{old('TransactionDate',date('d-m-Y H:i:s'))}}" />
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="la la-calendar-check-o"></i>
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="mb-1">
        <div class="form-group row">
          <div class="col-lg-6">
            <label class="font-weight-bolder">Ticket Category <span class="text-danger">*</span></label>
            <br>
            <select class="form-control select2 ticket_category" id="ticket_category" name="TicketCategory" data-placeholder="Choose One" required>
              <option></option>
              @if(@$ticketType == 'MINOR')
              <option value="MINOR" {{ old('TicketCategory',$ticketType) == 'MINOR' ? 'selected' : '' }}>Minor</option>
              @else if(@$ticketType == 'NON-MINOR')
              <option value="CRITICAL" {{ old('TicketCategory',$ticketType) == 'CRITICAL' ? 'selected' : '' }}>Critical</option>
              <option value="MAJOR" {{ old('TicketCategory',$ticketType) == 'MAJOR' ? 'selected' : '' }}>Major</option>
              <option value="PLN" {{ old('TicketCategory',$ticketType) == 'PLN' ? 'selected' : '' }}>PLN</option>
              @endif
            </select>
          </div>
          <div class="col-lg-6">
            <label class="font-weight-bolder">Schedule Date <span class="text-danger">*</span></label>
            <div class="input-group date" >
              <input type="text" id="ScheduleDate" class="form-control" readonly  placeholder="{{date('d-m-Y H:i:s')}}" name="ScheduleDate" value="{{old('ScheduleDate',date('d-m-Y H:i:s'))}}" />
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="la la-calendar-check-o"></i>
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="mb-1">
        <div class="form-group row">
          <div class="col-lg-6">
            <label class="font-weight-bolder">Branch <span class="text-danger">*</span></label>
            <select class="form-control select2" id="branch" name="BranchCode" data-placeholder="Choose One">
              <option></option>
              @foreach($branchs as $branch)
              <option value="{{ $branch->Code }}" {{ old('BranchCode') == $branch->Code ? 'selected' : '' }}>{{ $branch->Name }}</option>
              @endforeach
            </select>
          </div>
          <div class="col-lg-6">
            <label class="font-weight-bolder">Problem Type <span class="text-danger">*</span></label>
            <select class="form-control select2" id="problemType" name="ProblemTypeCode" data-placeholder="Choose One">
              <option></option>
              @foreach($problemTypes as $problemType)
              <option value="{{ $problemType->Code }}" {{ old('ProblemTypeCode') == $problemType->Code ? 'selected' : '' }}>{{ $problemType->name }}</option>
              @endforeach
            </select>
          </div>
        </div>
      </div>
      <div class="mb-1">
        <div class="form-group row">
          <div class="col-lg-6" @php echo $ticketType == 'MINOR' ? '' : 'style="display: none;"' @endphp>
            <label class="font-weight-bolder">Customer <span class="text-danger">*</span></label>
            <select class="form-control select2 searchCustomer" id="customerCode" name="customerCode" data-placeholder="Choose One">
              <option></option>
              
            </select>
          </div>
          <div class="col-lg-6">
            <label class="font-weight-bolder">Responsibility Department <span class="text-danger">*</span></label>
            <select class="form-control select2" id="ResponsibilityDepartmentCode" name="ResponsibilityDepartmentCode" data-placeholder="Choose One">
              @foreach($departments as $department)
              <option value="{{ $department->Code }}" {{ old('ResponsibilityDepartmentCode') == $department->Code ? 'selected' : '' }}>{{ $department->Name }}</option>
              @endforeach
            </select>
          </div>
        </div>
      </div>
      <div class="mb-1">
        <div @php echo $ticketType == 'MINOR' ? 'style="display: none;"' : '' @endphp>
          <div class="form-group row">
            <div class="col-lg-4" id="div-bts">
              <label class="font-weight-bolder">BTS <span class="text-danger">*</span></label>
              <select class="form-control select2" id="open_bts" name="open_bts[]" multiple="multiple" data-placeholder="Choose needed Item">
                @foreach($baseTransmissionStations as $bts)
                <option value="{{ $bts->Code }}" {{ old('bts') == $bts->Code ? 'selected' : '' }}>{{ $bts->Name }}</option>
                @endforeach
              </select>
            </div>
            <div class="col-lg-4" id="div-olt">
              <label class="font-weight-bolder">Device <span class="text-danger">*</span></label>
              <select class="form-control select2" id="olt_host" name="olt_host[]" multiple="multiple" data-placeholder="Choose needed item">
                @foreach($oltHost as $oh)
                <option value="{{ $oh->Code }}" {{ old('oh') == $oh->Code ? 'selected' : '' }}>{{ $oh->Hostname }}</option>
                @endforeach
              </select>
            </div>
            <div class="col-lg-4"  id="search_customer">
              <label class="font-weight-bolder">Search</label>
              <div class="input-group">
                <input type="text" class="form-control" aria-label="Search" aria-describedby="basic-addon2">
                <div class="input-group-append">
                  <button class="btn btn-outline-secondary" type="button"><i class="fa fa-search"></i> </button>
                </div>
              </div>
            </div>
            <div class="col-lg-12"><span class="text-muted" id="found-customer"></span></div>
            <div class="col-lg-12" style="height: 200px; overflow-y: scroll">
              <hr>
              <table class="table table-stripped">
                <thead class="bg-light text-dark rounded">
                  <tr>
                    <th>Act</th>
                    <th width="15%">Customer ID</th>
                    <th width="25%">Customer Name</th>
                    <th width="15%">Contact Person</th>
                    <th width="30%">Address</th>
                    <th width="5%">BTS</th>
                    <th width="5%">Device</th>
                  </tr>
                </thead>
                <tbody id="customer_noc_list">

                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="mb-1">
          <div class="form-group row">
            <div class="col-lg-6">
              <label class="font-weight-bolder">Ref No</label>
              <input type="text" class="form-control" name="ref_no" value="{{ old('ref_no') }}" placeholder="Enter Ref No" />
            </div>
          </div>
        </div>

        <div class="mb-1">
          <div class="form-group row">
            <div class="col-lg-6">
              <label class="font-weight-bolder">Ticket Description</label>
              <textarea class="form-control" rows="5" name="ticket_description"></textarea>
            </div>
            <div class="col-lg-6">
              <label class="font-weight-bolder">Remark</label>
              <textarea class="form-control" rows="5" name="Remark"></textarea>
            </div>
          </div>
        </div>
      </div>
      <div class="card-footer">
        <div class="row">
          <div class="col-lg-6">
            <a href="{{ route('ticketing.fmi.ticket-open.index') }}" id="cancel-btn" class="btn btn-danger">Cancel</a>
          </div>
          <div class="col-lg-6 text-right">
            <button type="submit" class="btn btn-primary mr-2">Submit</button>
          </div>
        </div>
      </div>
    </form>
  </div>
  @endsection

  @section('styles')

  @endsection

  @section('scripts')

  <script type="text/javascript">
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
</script>
<script type="text/javascript">

function do_select_customer()
{
  var html = '';
  //var list = '';
  $('#customer-item').html(html);
  //$('#customer_id_list').html(list);
  for(var i = 0; i < checkbox_selected.length; i++)
  {
    //list += '<li>' + checkbox_selected[i] + '</li>';
    html += '<input type="hidden" readonly name="CustCode[]" value="'+checkbox_selected[i]+'">';
  }
  //$('#customer_id_list').html(list);
  $('#customer-item').html(html);
  //$('#modalFindCustomer').modal('hide');
}

Array.prototype.remove = function() {
  var what, a = arguments, L = a.length, ax;
  while (L && this.length) {
    what = a[--L];
    while ((ax = this.indexOf(what)) !== -1) {
      this.splice(ax, 1);
    }
  }
  return this;
};

var checkbox_selected=[];

$(document).on('change','.checkbox-selected',function(){
  if($(this).prop('checked')==true)
  {
    checkbox_selected.push($(this).val());
  }
  else
  {
    checkbox_selected.remove($(this).val());
  }
  do_select_customer();
});

$(document).ready(function() {
  var selected_customer = [];

  $('#open_bts, #olt_host').on('change',function(){
    $.ajax({
      url: '{{route("ticketing.fmi.ticket-open.get-by-olt-bts")}}',
      method: 'POST',
      async: false,
      enctype: 'multipart/form-data',
      data: {
        "_token": "{{ csrf_token() }}",
        "open_bts": $('#open_bts').val(),
        'olt_host': $('#olt_host').val()
      },
      processData: true,
      cache: false,
      beforeSend: function()
      {
        $('#customer_noc_list').html('<tr><td colspan="7"><div class="progress">\
        <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">Fetch Data</div>\
        </div></td></tr>'
      );
    },
    success: function(response)
    {
      if(response.status == 'true')
      {
        var html = '';
        $('#customer_noc_list').html(html);
        for(var i = 0; i < response.data.length; i++)
        {
          html += '<tr>';

          if(response.data[i].Code)
          {
            html += '<td><input name="open_customer[]" type="checkbox" class="checkbox-selected" value="'+response.data[i].Code+'"></td>';
          }
          else
          {
            html += '<td><input name="open_customer[]" type="checkbox" class="checkbox-selected" value="'+response.data[i].Code+'"></td>';
          }

          if(response.data[i].Code)
          {
            selected_customer
            html += '<td>'+response.data[i].Code+'</td>';
          }
          else
          {
            html += '<td></td>';
          }

          if(response.data[i].Name)
          {
            selected_customer
            html += '<td>'+response.data[i].Name+'</td>';
          }
          else
          {
            html += '<td></td>';
          }

          if(response.data[i].ContactPerson)
          {
            selected_customer
            html += '<td>'+response.data[i].ContactPerson+'</td>';
          }
          else
          {
            html += '<td></td>';
          }

          if(response.data[i].Address)
          {
            selected_customer
            html += '<td>'+response.data[i].Address+'</td>';
          }
          else
          {
            html += '<td></td>';
          }

          if(response.data[i].BaseTransmissionStationCode)
          {
            html += '<td><i class="fa fa-check"></i></td>';
          }
          else
          {
            html += '<td></td>';
          }

          if(!response.data[i].BaseTransmissionStationCode)
          {
            html += '<td><i class="fa fa-check"></i></td>';
          }
          else
          {
            html += '<td></td>';
          }

          html += '</tr>';
        }
        checkbox_selected = [];
        $('#found-customer').html('Data was found  '+response.data.length);
        $('#customer_noc_list').html(html);

      }
      else
      {

      }
    },
    error: function(xhr, ajaxOptions, thrownError)
    {

    }
  });
});

$('#search_customer').on('click','button',function(){
  $('#customer_noc_list').find('tr').each(function(){
    if($(this).html().toLowerCase().includes($('#search_customer').find('input').val().toLowerCase()))
    {
    	//$(this).css('display','block');
      $(this).slideDown();
    }
    else
    {
    	$(this).slideUp();
    	//$(this).css('display','none');
    }
  })
})

$('#ticket_category').on('change', function() {
  if($(this).val() == 'MINOR')
  {
    $('#div-customer').slideDown();
  }
  else {
    $('#div-customer').slideUp();
  }
});

$('.ticket_category').select2({
  'placeholder' : 'Choose One',
  'width' : '100%',
  tags : true,
  'allowClear' : true
});

$('#branch').select2({
  'placeholder' : 'Choose One',
  'width' : '100%',
  tags : true,
  'allowClear' : true
});

$('#ResponsibilityDepartmentCode').select2({
  'placeholder' : 'Choose One',
  'width' : '100%',
  tags : true,
  'allowClear' : true
});

$('#problemType').select2({
  'placeholder' : 'Choose One',
  'width' : '100%',
  tags : true,
  'allowClear' : true
});

$('#customerCode').select2({
  'placeholder' : 'Choose One',
  'width' : '100%',
  tags : true,
  'allowClear' : true
});

$('#open_bts').select2({
  'placeholder' : 'Choose One',
  'width' : '100%',
  tags : true,
  'allowClear' : true
});

$('#olt_host').select2({
  'placeholder' : 'Choose One',
  'width' : '100%',
  tags : true,
  'allowClear' : true
});

$('#open_device_type_fo').select2({
  'placeholder' : 'Choose One',
  'width' : '100%',
  tags : true,
  'allowClear' : true
});

$('#open_device_type_wll').select2({
  'placeholder' : 'Choose One',
  'width' : '100%',
  tags : true,
  'allowClear' : true
});
});

var KTBootstrapDatepicker = function () {
  var arrows;
  if (KTUtil.isRTL()) {
    arrows = {
      leftArrow: '<i class="la la-angle-right"></i>',
      rightArrow: '<i class="la la-angle-left"></i>'
    }
  } else {
    arrows = {
      leftArrow: '<i class="la la-angle-left"></i>',
      rightArrow: '<i class="la la-angle-right"></i>'
    }
  }

  // Private functions
  var demos = function () {
    // $('#TransactionDate').datetimepicker({
    //   rtl: KTUtil.isRTL(),
    //   format: 'dd-mm-yyyy hh:ii:ss',
    //   todayHighlight: true,
    //   orientation: "bottom left",
    //   templates: arrows
    // });

    $('#ScheduleDate').datetimepicker({
      rtl: KTUtil.isRTL(),
      format: 'dd-mm-yyyy hh:ii:ss',
      todayHighlight: true,
      orientation: "bottom left",
      templates: arrows
    });
  }

  return {
    // public functions
    init: function() {
      demos();
    }
  };
}();

jQuery(document).ready(function() {
  KTBootstrapDatepicker.init();
  $('.searchCustomer').select2({
    placeholder: 'Search Customer',
    ajax: {
      url: "{{route("ticketing.fmi.ticket-open.get-customer-noc")}}",
      dataType: 'json',
      delay: 250,
      processResults: function (data) {
        return {
          results:  $.map(data, function (item) {
            return {
              text: item.Name,
              id: item.Code
            }
          })
        };
      },
      cache: true
    }
  });
});
</script>
@endsection
