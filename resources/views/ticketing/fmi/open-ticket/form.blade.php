@extends('layout.default')

@section('content')
	<div class="subheader py-2 {{ Metronic::printClasses('subheader', false) }}" id="kt_subheader">
	    <div class="{{ Metronic::printClasses('subheader-container', false) }} d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
	        <div class="d-flex align-items-baseline flex-wrap mr-5">
	            <h5 class="text-dark font-weight-bold my-1 mr-5">
	                Create Ticket                               
	            </h5>
	            <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
	                <li class="breadcrumb-item">
	                    <a href="{{ route('dashboard') }}" class="text-muted">Home</a>
	                </li>
	                <li class="breadcrumb-item">
	                    <a href="" class="text-muted">Create Ticket</a>
	                </li>
	            </ul>
	        </div>
	    </div>
	</div>
	<form class="form" action="{{ route('ticketing.fmi.ticket-open.store') }}" method="POST">
		{!! csrf_field() !!}
		<div class="card card-custom mb-0">
			<div class="card-header border-1 pt-6 pb-0">
			    <div class="card-title">
			        <h3 class="card-label">Create Ticket
			        </h3>
			    </div>
			</div>
			<div class="card-body">
			    <div class="mb-1">
			        <div class="form-group row">
			            <div class="col-lg-4">
			                <label>TCK No</label>
			                <input type="text" class="form-control" name="tck_no" value="{{ old('tck_no') }}" placeholder="Enter TCK No" / required>
			            </div>

			            <div class="col-lg-4">
	                        <label>Open Ticket Date</label>
							<div class="input-group date" >
							    <input type="text" id="TransactionDate" class="form-control" readonly  placeholder="Select date" name="TransactionDate" value="{{old('TransactionDate')}}" />
							    <div class="input-group-append">
							        <span class="input-group-text">
							        <i class="la la-calendar-check-o"></i>
							        </span>
							    </div>
							</div>
			            </div>
			        </div>

			        <div class="form-group row">
			            <div class="col-lg-6">
			                <label>Ticket Category</label>
			                <div>
			                    <input type="radio" name="TicketCategory" value="CRITICAL" class="ticketCategory" data-id='CRITICAL' id="CRITICAL"> <label for="CRITICAL">CRITICAL</label>
			                </div>
			                <div>
			                    <input type="radio" name="TicketCategory" value="MAJOR" class="ticketCategory" data-id='MAJOR' id="MAJOR"> <label for="MAJOR">MAJOR</label>
			                </div>
			                <div>
			                    <input type="radio" name="TicketCategory" value="PLN" class="ticketCategory" data-id='PLN' id="PLN"> <label for="PLN">PLN</label>
			                </div>
			            </div>
			        </div>

			        <div class="form-group row">
	                    <div class="col-lg-6">
	                        <label>Schedule Date</label>
							<div class="input-group date" >
							    <input type="text" id="ScheduleDate" class="form-control" readonly  placeholder="Select date" name="ScheduleDate" value="{{old('ScheduleDate')}}" />
							    <div class="input-group-append">
							        <span class="input-group-text">
							        <i class="la la-calendar-check-o"></i>
							        </span>
							    </div>
							</div>
	                    </div>
			        </div> 

			        <div class="form-group row">
			            <div class="col-lg-6">
			                <label>Branch</label>
			                <select class="form-control select2" id="branch" name="branch" data-placeholder="Choose One">
			                	<option></option>
			                    @foreach($branchs as $list)
			                    <option value="{{$list->Code}}" {{ old('branch') == $list->Code ? 'selected' : '' }}>{{ $list->Name }}</option>
			                    @endforeach
			                </select>
			            </div>
			            <div class="col-lg-6">
			                <label>Problem Type</label>
			                <select class="form-control" id="problemType" name="problemType" data-placeholder="Choose One">
			                	<option></option>
			                    @foreach($problemTypes as $list)
			                    <option value="{{$list->Code}}"  {{ old('problemType') == $list->Code ? 'selected' : '' }}>{{ $list->name }}</option>
			                    @endforeach
			                </select>
			            </div>
			        </div>

			        <div class="form-group row">
			            <div class="col-lg-6">
			                <label>Responsibility Department</label>
			                <select class="form-control" id="responsibilityDepartmentCode" name="responsibilityDepartmentCode" data-placeholder="Choose One">
			                	<option></option>
			                    @foreach($departments as $list)
			                    <option value="{{$list->Code}}" {{ old('responsibilityDepartmentCode') == $list->Code ? 'selected' : '' }}>{{ $list->Name }}</option>
			                    @endforeach
			                </select>
			            </div>
			            <div class="col-lg-6">
			                <label>Ref No</label>
			                <input type="text" class="form-control" name="ref_no" value="{{ old('ref_no') }}" placeholder="Enter Ref No" />
			            </div>
			        </div>
			        <div class="form-group row">
			            <div class="col-lg-6">
			                <label>Ticket Description</label>
			                <textarea class="form-control" rows="5" name="ticket_description"></textarea>
			            </div> 

			            <div class="col-lg-6">
			                <label>Remark</label>
			                <textarea class="form-control" rows="5" name="Remark"></textarea>
			            </div>
			        </div>
			    </div>
			</div>
		</div>
		<div class="card card-custom mt-5">
	        <div class="card-header flex-wrap border-1 pt-6 pb-0">
	            <div class="card-title">
	                <h3 class="card-label">Other Filter  
	                </h3>
	            </div>
	        </div>
	        <div class="card-body">
	        	<div class="row" data-sticky-container>
	                <div class="col-4">
	                    <ul class="nav flex-column nav-pills">
	                        <li class="nav-item mb-2">
	                            <a class="nav-link active" id="bts-tab" data-toggle="tab" href="#base-transmission-station">
	                            <span class="nav-text">Base Transmission Station</span>
	                            </a>
	                        </li>
	                        <li class="nav-item mb-2">
	                            <a class="nav-link" id="fo-tab" data-toggle="tab" href="#device-type-fo"  aria-controls="device-type-fo" >
	                            <span class="nav-text">Device Type FO</span>
	                            </a>
	                        </li>
	                        <li class="nav-item mb-2">
	                            <a class="nav-link" id="wll-tab" data-toggle="tab" href="#device-type-wll"  aria-controls="device-type-wll" >
	                            <span class="nav-text">Device Type Wll</span>
	                            </a>
	                        </li>
	                        <li class="nav-item mb-2" id="div-customer" hidden>
	                            <a class="nav-link" id="customer-tab" data-toggle="tab" href="#customer"  aria-controls="customer" >
	                            <span class="nav-text">Customers</span>
	                            </a>
	                        </li>
	                    </ul>
	                </div>
	                <div class="col-8">
	                    <div class="tab-content" id="myTabContent5">
	                        <div class="tab-pane fade show active" id="base-transmission-station" role="tabpanel" aria-labelledby="bts-tab">
								<div class="form-group col-12">
									<label class="checkbox-primary">
										<input type="checkbox" class="filter-all" id="checkAll-bts"/>
										<span>
											Select All | Unselect All
										</span>
									</label>
		                      	</div>
								@foreach($baseTransmissionStations as $list)
									<div class="col-6">
			                        	<input type="checkbox" class="filter-bts" name="bts[]" value="{{ $list->Code }}">
			                        	<label for="{{ $list->Name }}">{{ $list->Name }}</label>
			                      	</div>
		                      	@endforeach
	                        </div>
	                        <div class="tab-pane fade" id="device-type-fo" role="tabpanel" aria-labelledby="fo-tab">
								<div class="form-group col-12">
									<label class="checkbox-primary">
										<input type="checkbox" class="filter-all" id="checkAll-fo"/>
										<span>
											Select All | Unselect All
										</span>
									</label>
		                      	</div>
		                      	<div class="col-6">
		                      		<input type="checkbox" class="filter-fo" name="fo[]" value="OLT">
		                      		<label for="OLT">OLT</label>
		                      	</div>
		                      	<div class="col-6">
		                      		<input type="checkbox" class="filter-fo" name="fo[]" value="Switch">
		                      		<label for="Switch">Switch</label>
		                      	</div>
		                      	<div class="col-6">
		                      		<input type="checkbox" class="filter-fo" name="fo[]" value="DWDM">
		                      		<label for="DWDM">DWDM</label>
		                      	</div>
		                      	<div class="col-6">
		                      		<input type="checkbox" class="filter-fo" name="fo[]" value="OTB">
		                      		<label for="OTB">OTB</label>
		                      	</div>
	                        </div>
	                        <div class="tab-pane fade" id="device-type-wll" role="tabpanel" aria-labelledby="wll-tab">
								<div class="form-group col-12">
									<label class="checkbox-primary">
										<input type="checkbox" class="filter-all" id="checkAll-wll"/>
										<span>
											Select All | Unselect All
										</span>
									</label>
		                      	</div>
		                      	<div class="col-6">
		                      		<input type="checkbox" class="filter-wll" name="wll[]" value="Radio">
		                      		<label for="Radio">Radio</label>
		                      	</div>
		                      	<div class="col-6">
		                      		<input type="checkbox" class="filter-wll" name="wll[]" value="Microwave">
		                      		<label for="Microwave">Microwave</label>
		                      	</div>
	                        </div>
	                        <div class="tab-pane fade" id="customer" role="tabpanel" aria-labelledby="customer-tab">
	                            Tab Customer
	                        </div>
	                    </div>
	                </div>
	        	</div>
	        </div>
			<div class="card-footer">
			    <div class="row">
			        <div class="col-lg-6">
			            <a href="{{ route('ticketing.fmi.ticket-open.index') }}" id="cancel-btn" class="btn btn-danger">Cancel</a>
			        </div>
			        <div class="col-lg-6 text-right">
			            <button type="submit" class="btn btn-primary mr-2">submit</button>
			        </div>
			    </div>
			</div>
		</div>
	</form>
@endsection


{{-- Styles Section --}}
@section('styles')

@endsection

{{-- Scripts Section --}}
@section('scripts')
    @include ('inc.confirm-delete-modal')
	<script type="text/javascript">
		$(document).ready(function() {
			$('.ticketCategory').on('change', function() {
				// alert($(this).data('id'));
				if ($(this).data('id') == "") {
					$('#div-customer').attr('hidden', '');
				} else if ($(this).data('id') == "PLN") {
					$('#div-customer').attr('hidden', '');
				} else if($(this).data('id') == "CRITICAL" || $(this).data('id') == "MAJOR") {
					$('#div-customer').removeAttr('hidden');
				}
			});
			// $('.ticketCategory').change();

			$("#checkAll-bts").change(function () {
			    $(".filter-bts").prop('checked', $(this).prop("checked"));
			});

			$("#checkAll-fo").change(function () {
				$(".filter-fo").prop('checked', $(this).prop("checked"));
			});

			$("#checkAll-wll").change(function () {
				$(".filter-wll").prop('checked', $(this).prop("checked"));
			});

            $('#branch').select2({
                'placeholder' : 'Choose One',
                'width' : '100%',
                tags : true,
                'allowClear' : true
            });

            $('#problemType').select2({
                'placeholder' : 'Choose One',
                'width' : '100%',
                tags : true,
                'allowClear' : true
            });

            $('#responsibilityDepartmentCode').select2({
                'placeholder' : 'Choose One',
                'width' : '100%',
                tags : true,
                'allowClear' : true
            });
		});

		var KTBootstrapDatepicker = function () {
			var arrows;
			if (KTUtil.isRTL()) {
				arrows = {
					leftArrow: '<i class="la la-angle-right"></i>',
					rightArrow: '<i class="la la-angle-left"></i>'
				}
			} else {
				arrows = {
					leftArrow: '<i class="la la-angle-left"></i>',
					rightArrow: '<i class="la la-angle-right"></i>'
				}
			}
			
			// Private functions
			var demos = function () {
				$('#TransactionDate').datepicker({
					rtl: KTUtil.isRTL(),
                    format: 'dd-mm-yyyy',
					todayHighlight: true,
					orientation: "bottom left",
					templates: arrows
				});

				$('#ScheduleDate').datepicker({
					rtl: KTUtil.isRTL(),
                    format: 'dd-mm-yyyy',
					todayHighlight: true,
					orientation: "bottom left",
					templates: arrows
				});
			}

			return {
				// public functions
				init: function() {
					demos();
				}
			};
		}();

		jQuery(document).ready(function() {
			KTBootstrapDatepicker.init();
		});
	</script>
@endsection
