@extends('layout.default')

@section('content')
	<div class="subheader py-2 {{ Metronic::printClasses('subheader', false) }}" id="kt_subheader">
	    <div class="{{ Metronic::printClasses('subheader-container', false) }} d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
	        <div class="d-flex align-items-baseline flex-wrap mr-5">
	            <h5 class="text-dark font-weight-bold my-1 mr-5">
	                Create Ticket                               
	            </h5>
	            <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
	                <li class="breadcrumb-item">
	                    <a href="{{ route('dashboard') }}" class="text-muted">Home</a>
	                </li>
	                <li class="breadcrumb-item">
	                    <a href="" class="text-muted">Create Ticket</a>
	                </li>
	            </ul>
	        </div>
	    </div>
	</div>
	<form class="form" action="" method="POST">
		{!! csrf_field() !!}
		<div class="card card-custom mb-0">
			<div class="card-header border-1 pt-6 pb-0">
			    <div class="card-title">
			        <h3 class="card-label">Create Ticket
			        </h3>
			    </div>
			</div>
			<div class="card-body">
			    <div class="mb-1">
			        <div class="form-group row">
			            <div class="col-lg-4">
			                <label>TCK No</label>
			                <input type="text" class="form-control" name="code" value="" />
			            </div>

			            <div class="col-lg-4">
			                <label>Transaction Date</label>
			                <input type="text" class="form-control" name="TransactionDate" value="{{ date('Y-m-d H:i:s') }}" />
			            </div>
			        </div>

			        <div class="form-group row">
			            <div class="col-lg-6">
			                <label>Ticket Category</label>
			                <div>
			                    <input type="radio" name="TicketCategory" value="CRITICAL" class="ticketCategory" data-id='critical' id="critical"> <label for="critical">Critical</label>
			                </div>
			                <div>
			                    <input type="radio" name="TicketCategory" value="MAJOR" class="ticketCategory" data-id='major' id="major"> <label for="major">Major</label>
			                </div>
			                <div>
			                    <input type="radio" name="TicketCategory" value="PLN" class="ticketCategory" data-id='pln' id="pln"> <label for="pln">PLN</label>
			                </div>
			            </div>
			        </div>

			        <div class="form-group row">
			            <div class="col-lg-6">
			                <label>Scedule Date</label>
			                <input type="text" name="SceduleDate" class="form-control datepicker">
			            </div>
			        </div> 

			        <div class="form-group row">
			            <div class="col-lg-6">
			                <label>Branch</label>
			                <select class="form-control" name="BranchCode">
			                    @foreach($branchs as $list)
			                    <option value="{{$list->Code}}" >{{ $list->Name }}</option>
			                    @endforeach
			                </select>
			            </div>
			            <div class="col-lg-6">
			                <label>Problem Type</label>
			                <select class="form-control" name="ProblemTypeCode">
			                    @foreach($problemTypes as $list)
			                    <option value="{{$list->Code}}" >{{ $list->name }}</option>
			                    @endforeach
			                </select>
			            </div>
			        </div>

			        <div class="form-group row">
			            <div class="col-lg-6">
			                <label>Responsibility Department</label>
			                <select class="form-control" name="ResponsibilityDepartmentCode">
			                    @foreach($departments as $list)
			                    <option value="{{$list->Code}}" >{{ $list->Name }}</option>
			                    @endforeach
			                </select>
			            </div>
			            <div class="col-lg-6">
			                <label>Ref No</label>
			                <input type="text" class="form-control" name="RefNo">
			            </div>
			        </div>
			        <div class="form-group row">
			            <div class="col-lg-6">
			                <label>Ticket Description</label>
			                <textarea class="form-control" name="TicketDescription"></textarea>
			            </div> 

			            <div class="col-lg-6">
			                <label>Remark</label>
			                <textarea class="form-control" name="Remark"></textarea>
			            </div>
			        </div>
			    </div>
			</div>
			<div class="card-footer">
			    <div class="row">
			        <div class="col-lg-6">
			            <a href="{{ route('ticketing.fmi.ticket-open.index') }}" id="cancel-btn" class="btn btn-danger">Cancel</a>
			        </div>
			        <div class="col-lg-6 text-right">
			            <button type="submit" class="btn btn-primary mr-2">submit</button>
			        </div>
			    </div>
			</div>
		</div>
	    <div class="card card-custom mt-5">
	        <div class="card-header flex-wrap border-1 pt-6 pb-0">
	            <div class="card-title">
	                <h3 class="card-label">Customers  
	                    <div class="text-muted pt-2 font-size-sm">show data customer</div>
	                </h3>
	            </div>
	        </div>
	        <div class="card-body">
	            <div class="table-responsive">
	                <table style="width: 1070px !important;" class="table table-borderless table-vertical-center" id="datatable-major">
	                    <thead>
	                        <tr>
	                           <th></th>
	                           <th></th>
	                           <th></th>
	                           <th></th>
	                        </tr>
	                    </thead>
	                </table>
	            </div>
	        </div>
	    </div>
	</form>
@endsection


{{-- Styles Section --}}
@section('styles')
    <link rel="stylesheet" href="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.css">
    <link rel="stylesheet" href="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.min.css">
    <link rel="stylesheet" href="{{ config('app.url') }}plugins/custom/datatables/datatables.bundle.css">
    <link rel="stylesheet" href="{{ config('app.url') }}global/vendor/datatables-select/select.bootstrap4.min.css">
@endsection

{{-- Scripts Section --}}
@section('scripts')
    @include ('inc.confirm-delete-modal')
    <script src="{{ config('app.url') }}global/vendor/datatables/jquery.dataTables.js"></script>
    <script src="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.js"></script>
    <script src="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.js"></script>
    <script src="{{ config('app.url') }}global/vendor/datatables-select/dataTables.select.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable-major').dataTable({
                pageLength: 10,
                processing: true,
                serverSide: true,
		        select: {
		            style: 'multi'
		        },
                ajax: {
                    method: 'POST',
                    url : '{{ route('ticketing.fmi.data-major.data') }}',
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                },
                columns : [
                	{title: 'Select', data: 'code', render: function() { return ''; }, className: 'select-checkbox', searchable: false, orderable: false},
                    { title:'TSK No',  data: 'code', name: 'code', defaultContent: '-', class: 'text-center' },
                    { title: 'Transaction Date', data: 'transaction_date', name: 'transaction_date', defaultContent: '-', class: 'text-center', searchable: false },
                    { title: 'TCK Kategory', data: 'TicketCategory', name: 'TicketCategory', defaultContent: '-', class: 'text-center', searchable: false }
                ],
                bInfo: false,
                pagingType: 'simple',
                responsive: true,
                order: [[ 1, 'asc' ]]
            });

        });
    </script>
@endsection
