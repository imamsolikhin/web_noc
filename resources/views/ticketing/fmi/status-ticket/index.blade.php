@extends('layout.default')

@section('content')
<div class="subheader py-2 {{ Metronic::printClasses('subheader', false) }}" id="kt_subheader">
    <div class="{{ Metronic::printClasses('subheader-container', false) }} d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
        <div class="d-flex align-items-baseline flex-wrap mr-5">
            <h5 class="text-dark font-weight-bold my-1 mr-5">
                FMI
            </h5>
            <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                <li class="breadcrumb-item">
                    <a href="{{ route('dashboard') }}" class="text-muted">Home</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="" class="text-muted">Ticket FMI</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="" class="text-muted">Ticket Status</a>
                </li>
            </ul>
        </div>
    </div>
</div>
@include('inc.success-notif')
<div class="row">
    <div class="col-md-3 bg-secondary pt-10 pl-10 pr-10">
        <h3><i class="fa fa-search"></i> Filter</h3>
        <hr>
        <div class="form-group">
            <label class="font-weight-bolder">Ticket Status: </label>
            <select class="form-control" id="ticketStatus">
                <option value="">--All--</option>
                <option value="OPEN">Open</option>
                <option value="OPC">On Progress CS</option>
                <option value="OPM">On Progress Maintenance</option>
                <option value="CLOSED">Closed</option>
                <option value="POSTPONED">Postponed</option>
            </select>
        </div>
        <div class="form-group">
            <label class="font-weight-bolder">Ticket Category: </label>
            <select class="form-control" id="ticketCategory">
                <option value="">--All--</option>
                <option value="CRITICAL">Critical</option>
                <option value="MAJOR">Major</option>
                <option value="PLN">PLN</option>
                <option value="MINOR">Minor</option>
            </select>
        </div>
        <div class="form-group">
            <label class="font-weight-bolder">Transaction Date: </label>
            <input class="form-control" id="transactionDate" type="text" placeholder="Choose date">
        </div>
    </div>
    <div class="col-lg-9">
        <div class="card card-custom">
            <div class="card-header flex-wrap border-1 pt-6 pb-0 bg-danger">
                <div class="card-title">
                    <h3 class="card-label text-white">Ticket Status
                        <div class="text-white pt-2 font-size-sm">show data ticket</div>
                    </h3>
                </div>
                <div class="card-toolbar">
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table style="width: 1070px !important;" class="table table-borderless table-vertical-center bg-light" id="datatable">
                        <thead>
                            <tr>
                             <th>TCK NO</th>
                             <th>Transaction Date</th>
                             <th>TCK Category</th>
                             <th>Status</th>
                             <th>Action</th>
                         </tr>
                     </thead>
                 </table>
             </div>
         </div>
     </div>
 </div>
</div>
<br>
@endsection

{{-- Styles Section --}}
@section('styles')
<link rel="stylesheet" href="{{ config('app.url') }}plugins/custom/datatables/datatables.bundle.css">
@endsection

{{-- Scripts Section --}}
@section('scripts')
@include ('inc.confirm-delete-modal')
<script src="{{ config('app.url') }}global/vendor/datatables/jquery.dataTables.js"></script>
<script src="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.js"></script>
<script src="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.js"></script>
<script src="{{ config('app.url') }}plugins/custom/datatables/datatables.bundle.js"></script>
<script type="text/javascript">
    var ds = $('#datatable').dataTable({
        pageLength: 10,
        processing: true,
        serverSide: true,
        responsive: false,
        ajax: {
            method: 'POST',
            url : '{{ route('ticketing.fmi.ticket-status.data') }}',
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        },
        columns : [
        { title:'TCK No',  data: 'code', name: 'code', defaultContent: '-', class: 'text-center' },
        { title: 'Transaction Date', data: 'transaction_date', name: 'transaction_date', defaultContent: '-', class: 'text-center' },
        { title: 'TCK Category', data: 'TicketCategory', name: 'TicketCategory', defaultContent: '-', class: 'text-center' },
        { title: 'Status', data: 'TicketStatus', name: 'TicketStatus', class: 'text-center' },
        { title:'Action', data: 'action', name: 'action', searchable: false, orderable: false, class: 'text-center' }
        ],
        columnDefs: [
        {
          targets: 1,
          render: function render(data, type, full, meta) {
            return full.transaction_date;
          }
        }],
        initComplete: function() {
            $('.tl-tip').tooltip();
            @if (count($errors) > 0)
            jQuery("html, body").animate({
                        scrollTop: $('#add-form').offset().top - 100 // 66 for sticky bar height
                    }, "slow");
            @endif
        }
    });

    $(document).ready(function() {
        $('#ticketStatus').on('change',function(){
            ds.api().columns(3).search('');
            ds.api().columns(3).search($(this).val());
            ds.api().ajax.reload();
        });
        $('#ticketCategory').on('change',function(){
            ds.api().columns(2).search('');
            ds.api().columns(2).search($(this).val());
            ds.api().ajax.reload();
        });
        $('#transactionDate').on('change',function(){
            ds.api().columns(1).search('');
            ds.api().columns(1).search($(this).val());
            ds.api().ajax.reload();
        });
        var KTBootstrapDatepicker = function () {
          var arrows;
          if (KTUtil.isRTL()) {
            arrows = {
              leftArrow: '<i class="la la-angle-right"></i>',
              rightArrow: '<i class="la la-angle-left"></i>'
            }
          } else {
            arrows = {
              leftArrow: '<i class="la la-angle-left"></i>',
              rightArrow: '<i class="la la-angle-right"></i>'
            }
          }

          // Private functions
          var demos = function () {
            $('#transactionDate').datepicker({
              rtl: KTUtil.isRTL(),
              format: 'dd M yyyy',
              todayHighlight: true,
              orientation: "bottom left",
              templates: arrows
            });
          }

          return {
            // public functions
            init: function() {
              demos();
            }
          };
        }();
        KTBootstrapDatepicker.init();
    });
</script>
@endsection
