@extends('layout.default')

@section('content')
<div class="modal fade show" style="display: none;" id="confirm-delete-modal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" style="display: block; padding-right: 19px;" aria-modal="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="" method="post" role="form" id="confirm-delete-modal-action">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="_method" value="DELETE">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Delete Confirmation</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Are you sure want to delete this selected data?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-default font-weight-bold" data-dismiss="modal">No</button>
                    <button type="submit" class="btn btn-danger font-weight-bold">Yes</button>
                </div>
            </form>
        </div>
    </div>
</div>

@include('inc.success-notif')

<div class="subheader py-2 {{ Metronic::printClasses('subheader', false) }}" id="kt_subheader">
  <div class="{{ Metronic::printClasses('subheader-container', false) }} d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
    <div class="d-flex align-items-baseline flex-wrap mr-5">
      <h5 class="text-dark font-weight-bold my-1 mr-5">
        FMI
      </h5>
      <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
        <li class="breadcrumb-item">
          <a href="{{ route('dashboard') }}" class="text-muted">Home</a>
        </li>
        <li class="breadcrumb-item">
          <a href="{{ route('ticketing.fmi.ticket-open.index') }}" class="text-muted">Open Ticket</a>
        </li>
        <li class="breadcrumb-item">
          <a href="" class="text-muted">Task Assignment</a>
        </li>
      </ul>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-12">
    <div class="card card-custom mb-0">
    @include('inc.error-list')

      <div style="display: none">
        @foreach($openBts as $key => $row)
          <input name="graveyard_bts[]" value="{{$row->code}}">
        @endforeach

        @foreach($openOlt as $key => $row)
          <input name="graveyard_olt[]" value="{{$row->code}}">
        @endforeach

        @foreach($openCustomers as $key => $row)
          <input name="graveyard_cust[]" value="{{$row->code}}">
        @endforeach
      </div>
      <div class="card-header border-1 pt-6 pb-0 bg-danger">
        <div class="card-title">
          <h3 class="card-label text-white">Task Assignment ({{old('tck_no',$fMIOpenTicket->code)}}) <span class="btn btn-success font-weight-bolder ml-3">{{@$fMITicketStatus[count($fMITicketStatus)-1]->TicketStatus}}</span>

          </h3>
        </div>
      </div>
      <div class="card-body">
        <div class="form-group row">
          <div class="col-lg-12">
            <h3>TASK ASSIGNMENT</h3><hr>
          </div>
          <div class="col-lg-12">
            <table class="table table-stripped" style="font-size: 10px">
              <tr class="bg-secondary">
                <th>No</th>
                <th>TSK-ASG No</th>
                <th>Schedule Date</th>
                <th>ERP Task ID</th>
                <th>Action</th>
              </tr>
              @foreach($fMITaskAssignment as $key => $row)
              <tr>
                <td>{{$key+1}}</td>
                <td>{{$row->code}}</td>
                <td>{{$row->ScheduleDate}}</td>
                <td>{{$row->ERPTaskCode}}</td>
                <td>
                  <a href="javascript:void(0)" onclick="$('#updateModal{{$key}}').modal('show')" class="btn btn-icon btn-light btn-hover-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Edit">
                        <span class="svg-icon svg-icon-md svg-icon-primary">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <rect x="0" y="0" width="24" height="24"/>
                                    <path d="M12.2674799,18.2323597 L12.0084872,5.45852451 C12.0004303,5.06114792 12.1504154,4.6768183 12.4255037,4.38993949 L15.0030167,1.70195304 L17.5910752,4.40093695 C17.8599071,4.6812911 18.0095067,5.05499603 18.0083938,5.44341307 L17.9718262,18.2062508 C17.9694575,19.0329966 17.2985816,19.701953 16.4718324,19.701953 L13.7671717,19.701953 C12.9505952,19.701953 12.2840328,19.0487684 12.2674799,18.2323597 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.701953, 10.701953) rotate(-135.000000) translate(-14.701953, -10.701953) "/>
                                    <path d="M12.9,2 C13.4522847,2 13.9,2.44771525 13.9,3 C13.9,3.55228475 13.4522847,4 12.9,4 L6,4 C4.8954305,4 4,4.8954305 4,6 L4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 L2,6 C2,3.790861 3.790861,2 6,2 L12.9,2 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                </g>
                            </svg>
                        </span>
                    </a>
                    <a data-href="{{ route('ticketing.fmi.task-assignment.destroy',bin2hex($row->code)) }}" class="btn btn-icon btn-light btn-hover-danger btn-sm" "data-toggle="tooltip" data-placement="top" title="Delete" data-toggle="modal" data-target="#confirm-delete-modal" onclick="$('#confirm-delete-modal-action').attr('action',$(this).attr('data-href'))">
                        <span class="svg-icon svg-icon-md svg-icon-danger">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <rect x="0" y="0" width="24" height="24"></rect>
                                    <path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero"></path>
                                    <path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3"></path>
                                </g>
                            </svg>
                        </span>
                    </a>
                    <form class="form" action="{{ route('ticketing.fmi.task-assignment.update',$row->code) }}" method="POST">
                       {!! csrf_field() !!}
                        {!! method_field('PUT') !!}
                       <div class="modal fade" tabindex="-1" role="dialog" id="updateModal{{$key}}">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <div class="modal-header bg-danger">
                              <h5 class="modal-title text-white">Update Task Assignment</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body">
                              <div class="row">
                                <input type="hidden" name="TransactionDate" value="{{date('Y-m-d',strtotime($fMIOpenTicket->TransactionDate))}}">
                                <input type="hidden" name="TicketCode" value="{{@$fMIOpenTicket->code}}">
                                <input type="hidden" name="BranchCode" value="{{@$fMIOpenTicket->BranchCode}}">
                                <div class="col-12">
                                  <div class="form-group">
                                    <label class="font-weight-bolder">Task Code <span class="text-danger">*</span></label>
                                    <input readonly class="form-control" type="text" name="code" value="{{$row->code}}" placeholder="Select date">
                                  </div>
                                </div>
                                <div class="col-12">
                                  <div class="form-group">
                                    <label class="font-weight-bolder">ERP Task ID <span class="text-danger">*</span></label>
                                    <input class="form-control" type="text" name="ERPTaskCode" value="{{$row->ERPTaskCode}}">
                                  </div>
                                </div>
                                <div class="col-12">
                                  <div class="form-group">
                                    <label class="font-weight-bolder">Schedule Date <span class="text-danger">*</span></label>
                                    <input class="form-control" readonly="" type="text" id="ScheduleDate" name="ScheduleDate" value="{{date('d-m-Y',strtotime($row->ScheduleDate))}}" placeholder="Select date">
                                  </div>
                                </div>
                                <div class="col-12">
                                  <div class="form-group">
                                    <label class="font-weight-bolder">Mitra <span class="text-danger">*</span></label>
                                    <select class="form-control select2 mitra" name="MitraCode" data-placeholder="Choose One" required>
                                        <option></option>
                                        @foreach($mitras as $mitra)
                                        <option value="{{ $mitra->Code }}" {{ $row->MitraCode == $mitra->Code ? 'selected' : '' }}>{{ $mitra->name }}</option>
                                        @endforeach
                                    </select>
                                  </div>
                                </div>
                                <div class="col-12">
                                  <div class="form-group">
                                    <label class="font-weight-bolder">Team <span class="text-danger">*</span></label>
                                    <select class="form-control select2 team" name="TeamCode" data-placeholder="Choose One" required>
                                        <option></option>
                                        @foreach($teams as $team)
                                        <option value="{{ $team->Code }}" {{ $row->TeamCode == $team->Code ? 'selected' : '' }}>{{ $team->name }}</option>
                                        @endforeach
                                    </select>
                                  </div>
                                </div>
                                <div class="col-12">
                                  <div class="form-group">
                                    <label class="font-weight-bolder">Ref No</label>
                                    <input class="form-control" name="RefNo" value="{{$row->RefNo}}">
                                  </div>
                                </div>
                                <div class="col-12">
                                  <div class="form-group">
                                    <label class="font-weight-bolder">Remark</label>
                                    <textarea class="form-control" name="Remark">{{$row->Remark}}</textarea>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="modal-footer">
                              <button class="btn btn-primary" type="submit">Save changes</button>
                              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </form>
                </td>
              </tr>
              @endforeach
              @if(empty($fMITaskAssignment))
              <tr>
                <td colspan="5" class="text-muted text-center">There is no Task Assignment in this ticket</td>
              </tr>
              @endif
              <tr>
                <td colspan="5">

                  <a href="javascript:void(0);" onclick="$('#createModal').modal('show');" class="btn btn-primary py-2 btn-block" id="add-btn">
                      <i class="menu-icon">
                          <span class="svg-icon svg-icon-md">
                              <!--begin::Svg Icon | path:media/svg/icons/Design/Flatten.svg-->
                              <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                  <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                      <rect x="0" y="0" width="24" height="24"></rect>
                                      <circle fill="#000000" cx="9" cy="15" r="6"></circle>
                                      <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3"></path>
                                  </g>
                              </svg>
                              <!--end::Svg Icon-->
                          </span>
                      </i>
                      Assign New Task
                  </a>
                  <form class="form" action="{{ route('ticketing.fmi.task-assignment.store') }}" method="POST">
                     {!! csrf_field() !!}
                     <div class="modal fade" tabindex="-1" role="dialog" id="createModal">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header bg-danger">
                            <h5 class="modal-title text-white">New Task Assignment</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                            <div class="row">
                              <input type="hidden" name="TransactionDate" value="{{date('Y-m-d',strtotime($fMIOpenTicket->TransactionDate))}}">
                              <input type="hidden" name="TicketCode" value="{{@$fMIOpenTicket->code}}">
                              <input type="hidden" name="BranchCode" value="{{@$fMIOpenTicket->BranchCode}}">
                              <div class="col-12">
                                <div class="form-group">
                                  <label class="font-weight-bolder">Task Code <span class="text-danger">*</span></label>
                                  <input type="text" class="form-control" placeholder="Will be generate" readonly="">
                                  <input readonly style="display: none" class="form-control" type="text" name="code" value="{{str_replace('TCK','TSK-ASG',@$fMIOpenTicket->code.'-'.str_pad( empty(@$fMITaskAssignment) ? '1' : count(@$fMITaskAssignment)+1, 2, '0', STR_PAD_LEFT))}}" placeholder="Select date">
                                </div>
                              </div>
                              <div class="col-12">
                                <div class="form-group">
                                  <label class="font-weight-bolder">ERP Task ID <span class="text-danger">*</span></label>
                                  <input class="form-control" type="text" name="ERPTaskCode" value="{{old('ERPTaskCode')}}">
                                </div>
                              </div>
                              <div class="col-12">
                                <div class="form-group">
                                  <label class="font-weight-bolder">Schedule Date <span class="text-danger">*</span></label>
                                  <input class="form-control" type="text" name="ScheduleDate" value="{{old('ScheduleDate',date('d-m-Y'))}}" readonly="" placeholder="Select date">
                                </div>
                              </div>
                              <div class="col-12">
                                <div class="form-group">
                                  <label class="font-weight-bolder">Mitra <span class="text-danger">*</span></label>
                                  <select class="form-control select2 mitra" name="MitraCode" data-placeholder="Choose One" required>
                                      <option></option>
                                      @foreach($mitras as $mitra)
                                      <option value="{{ $mitra->Code }}" {{ old('MitraCode') == $mitra->Code ? 'selected' : '' }}>{{ $mitra->name }}</option>
                                      @endforeach
                                  </select>
                                </div>
                              </div>
                              <div class="col-12">
                                <div class="form-group">
                                  <label class="font-weight-bolder">Team <span class="text-danger">*</span></label>
                                  <select class="form-control select2 team" name="TeamCode" data-placeholder="Choose One" required>
                                      <option></option>
                                      @foreach($teams as $team)
                                      <option value="{{ $team->Code }}" {{ old('TeamCode') == $team->Code ? 'selected' : '' }}>{{ $team->name }}</option>
                                      @endforeach
                                  </select>
                                </div>
                              </div>
                              <div class="col-12">
                                <div class="form-group">
                                  <label class="font-weight-bolder">Ref No</label>
                                  <input class="form-control" name="RefNo" value="{{old('RefNo')}}">
                                </div>
                              </div>
                              <div class="col-12">
                                <div class="form-group">
                                  <label class="font-weight-bolder">Remark</label>
                                  <textarea class="form-control" name="Remark">{{old('Remark')}}</textarea>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button class="btn btn-primary" type="submit">Save changes</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </form>
                  </div>
                </td>
              </tr>
            </table>
          </div>
          <div class="col-lg-6">
            <h3>CURRENT TICKET STATUS</h3><hr>
            <div class="row">
              <div class="col-12">
                <label class="font-weight-bolder">Ticket Status</label>
                <div class="form-check">
                  @php echo @$fMITicketStatus[@count(@$fMITicketStatus)-1]->TicketStatus == 'OPEN' ? '<i class="fa fa-check"></i>' : '<i class="fa fa-minus"></i>'; @endphp
                  <label class="form-check-label ml-2" for="rad1">
                    Open
                  </label>
                </div>
                <div class="form-check">
                  @php echo @$fMITicketStatus[@count(@$fMITicketStatus)-1]->TicketStatus == 'OPC' ? '<i class="fa fa-check"></i>' : '<i class="fa fa-minus"></i>'; @endphp
                  <label class="form-check-label ml-2" for="rad2">
                    On Progress CS
                  </label>
                </div>
                <div class="form-check">
                  @php echo @$fMITicketStatus[@count(@$fMITicketStatus)-1]->TicketStatus == 'OPM' ? '<i class="fa fa-check"></i>' : '<i class="fa fa-minus"></i>';@endphp
                  <label class="form-check-label ml-2" for="rad3">
                    On Progress Maintenance
                  </label>
                </div>
                <div class="form-check">
                  @php echo @$fMITicketStatus[@count(@$fMITicketStatus)-1]->TicketStatus == 'CLOSED' ? '<i class="fa fa-check"></i>' : '<i class="fa fa-minus"></i>';@endphp
                  <label class="form-check-label ml-2" for="rad4">
                    Closed
                  </label>
                </div>
                <div class="form-check">
                  @php echo @$fMITicketStatus[@count(@$fMITicketStatus)-1]->TicketStatus == 'POSTPONED' ? '<i class="fa fa-check"></i>' : '<i class="fa fa-minus"></i>';@endphp
                  <label class="form-check-label ml-2" for="rad5">
                    Postponed
                  </label>
                </div>
              </div>
              <div class="col-12">
                <label class="font-weight-bolder">Ticket Status Ref No</label>
                <input type="text" class="form-control" name="ts_ref_no" disabled value="{{ old('ts_ref_no',@$fMITicketStatus->RefNo) }}" placeholder="Enter Ref No" />
                <label class="font-weight-bolder">Ticket Status Remark</label>
                <textarea  class="form-control" rows="5" disabled name="ts_Remark">{{ old('ts_Remark',@$fMITicketStatus->Remark) }}</textarea disabled >
              </div>
            </div>
          </div>
          <div class="col-lg-6">
            <h3>TICKET STATUS HISTORY</h3><hr>
            <table class="table table-stripped table-bordered" style="font-size: 10px">
              <tr class="bg-secondary">
                <th width="2%">No</th>
                <th width="15%">Ticket Status</th>
                <th width="30%">Change Date</th>
                <th width="15%">Ref No</th>
                <th width="15%">Remark</th>
                <th width="25%">Change By</th>
              </tr>
              @foreach($fMITicketStatus as $key => $row)
              <tr class="{{ $key+1 == count($fMITicketStatus) ? 'bg-success text-white' : ''}}">
                <td>{{$key+1}}</td>
                <td>{{ucwords(strtolower($row->TicketStatus))}}</td>
                <td>{{date('d M Y - H:i:s',strtotime($row->CreatedDate))}}</td>
                <td>{{$row->RefNo}}</td>
                <td>{{$row->Remark}}</td>
                <td>{{$row->CreatedBy}}</td>
              </tr>
              @endforeach
            </table>
          </div>
          <div class="col-lg-12">
            <h3 class="mt-4">TICKET DETAIL</h3><hr>
          </div>
          <div class="col-lg-6">
            <label class="font-weight-bolder">TCK No <span class="text-danger">*</span></label>
            <input type="text" class="form-control" disabled name="tck_no" value="{{ old('tck_no',$fMIOpenTicket->code) }}" placeholder="Enter TCK No" / required>
          </div>
          <div class="col-lg-6">
            <label class="font-weight-bolder">Open Ticket Date <span class="text-danger">*</span></label>
            <div class="input-group date" >
              <input type="text" id="TransactionDate" class="form-control" readonly  placeholder="Select date" name="TransactionDate" value="{{old('TransactionDate',date('Y-m-d H:i:s',strtotime($fMIOpenTicket->TransactionDate)))}}" />
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="la la-calendar-check-o"></i>
                </span>
              </div>
            </div>
          </div>
        </div>
        <div class="mb-1">
          <div class="form-group row">
            <div class="col-lg-6">
              <label class="font-weight-bolder">Ticket Category <span class="text-danger">*</span></label>
              <br>
              <select class="form-control ticket_category" id="ticket_category" name="TicketCategory" data-placeholder="Choose One" required readonly>
                <option value="{{$fMIOpenTicket->TicketCategory}}">{{$fMIOpenTicket->TicketCategory}}</option>
              </select>
            </div>
            <div class="col-lg-6">
              <label class="font-weight-bolder">Schedule Date <span class="text-danger">*</span></label>
              <div class="input-group date" >
                <input type="text" id="ScheduleDate" class="form-control" readonly  placeholder="Select date" name="ScheduleDate" value="{{old('ScheduleDate',date('Y-m-d H:i:s',strtotime($fMIOpenTicket->ScheduleDate)))}}" />
                <div class="input-group-append">
                  <span class="input-group-text">
                    <i class="la la-calendar-check-o"></i>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="mb-1">
          <div class="form-group row">
            <div class="col-lg-6">
              <label class="font-weight-bolder">Branch <span class="text-danger">*</span></label>
              <select class="form-control select2" id="branch" name="branch" data-placeholder="Choose One" disabled>
                <option></option>
                @foreach($branchs as $branch)
                <option value="{{ $branch->Code }}" {{ old('branch',$fMIOpenTicket->BranchCode) == $branch->Code ? 'selected' : '' }}>{{ $branch->Name }}</option>
                @endforeach
              </select>
            </div>
            <div class="col-lg-6">
              <label class="font-weight-bolder">Problem Type <span class="text-danger">*</span></label>
              <select class="form-control select2" id="problemType" name="problemType" data-placeholder="Choose One" disabled>
                <option></option>
                @foreach($problemTypes as $problemType)
                <option value="{{ $problemType->Code }}" {{ old('problemType',$fMIOpenTicket->ProblemTypeCode) == $problemType->Code ? 'selected' : '' }}>{{ $problemType->name }}</option>
                @endforeach
              </select>
            </div>
          </div>
        </div>
        <div class="mb-1">
          <div class="form-group row" @php echo @$fMIOpenTicket->TicketCategory == 'MINOR' ? '' : 'style="display: none;"' @endphp>
            <div class="col-lg-6" id="div-customer">
              <label class="font-weight-bolder">Customer <span class="text-danger">*</span></label>
              <input type="text" class="form-control" name="open_customer" readonly="" value="{{$fMIOpenTicket->CustomerCode}}">
            </div>
            <div class="col-lg-6">
              <label class="font-weight-bolder">Responsibility Department <span class="text-danger">*</span></label>
              <select class="form-control select2" id="ResponsibilityDepartmentCode" name="ResponsibilityDepartmentCode" data-placeholder="Choose One">
                @foreach($departments as $department)
                <option value="{{ $department->Code }}" {{ old('ResponsibilityDepartmentCode') == $department->Code ? 'selected' : '' }}>{{ $department->Name }}</option>
                @endforeach
              </select>
            </div>
          </div>
        </div>
        <div class="mb-1">
          <div @php echo @$fMIOpenTicket->TicketCategory == 'MINOR' ? 'style="display: none;"' : '' @endphp>
            <div class="form-group row">
              <div class="col-lg-4" id="div-bts">
                <label class="font-weight-bolder">BTS <span class="text-danger">*</span></label>
                <select class="form-control select2" id="open_bts" name="open_bts[]" multiple="multiple" data-placeholder="Choose needed Item">
                  @foreach($baseTransmissionStations as $bts)
                  <option value="{{ $bts->Code }}" {{ old('bts') == $bts->Code ? 'selected' : '' }}>{{ $bts->Name }}</option>
                  @endforeach
                </select>
              </div>
              <div class="col-lg-4" id="div-olt">
                <label class="font-weight-bolder">OLT <span class="text-danger">*</span></label>
                <select class="form-control select2" id="olt_host" name="olt_host[]" multiple="multiple" data-placeholder="Choose needed item">
                  @foreach($oltHost as $oh)
                  <option value="{{ $oh->Code }}" {{ old('oh') == $oh->Code ? 'selected' : '' }}>{{ $oh->Hostname }}</option>
                  @endforeach
                </select>
              </div>
              <div class="col-lg-4"  id="search_customer">
                <label class="font-weight-bolder">Search</label>
                <div class="input-group">
                  <input type="text" class="form-control" aria-label="Search" aria-describedby="basic-addon2">
                  <div class="input-group-append">
                    <button class="btn btn-outline-secondary" type="button"><i class="fa fa-search"></i> </button>
                  </div>
                </div>
              </div>
              <div class="col-lg-12"><span class="text-muted" id="found-customer"></span></div>
              <div class="col-lg-12" style="height: 200px; overflow-y: scroll">
                <hr>
                <table class="table table-stripped" >
                  <thead class="bg-light text-dark rounded">
                    <tr>
                      <th>Act</th>
                      <th width="15%">Customer ID</th>
                      <th width="25%">Customer Name</th>
                      <th width="15%">Contact Person</th>
                      <th width="30%">Address</th>
                      <th width="5%">BTS</th>
                      <th width="5%">OLT</th>
                    </tr>
                  </thead>
                  <tbody id="customer_noc_list">

                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="mb-1">
            <div class="form-group row">
              <div class="col-lg-6">
                <label class="font-weight-bolder">Ref No</label>
                <input type="text" class="form-control" name="ref_no" value="{{ old('ref_no',$fMIOpenTicket->RefNo) }}" placeholder="Enter Ref No" disabled/>
              </div>
            </div>
          </div>

          <div class="mb-1">
            <div class="form-group row">
              <div class="col-lg-6">
                <label class="font-weight-bolder">Ticket Description</label>
                <textarea  class="form-control" rows="5" name="ticket_description" disabled="">{{ @$fMIOpenTicket->TicketDescription }}</textarea >
              </div>
              <div class="col-lg-6">
                <label class="font-weight-bolder">Remark</label>
                <textarea  class="form-control" rows="5" name="Remark" disabled="">{{ @$fMIOpenTicket->Remark }}</textarea >
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer">
          <div class="row">
            <div class="col-lg-6">
              <a href="{{ route('ticketing.fmi.task-assignment.index') }}" id="cancel-btn" class="btn btn-danger">Back</a>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
@endsection

  @section('styles')

  @endsection

  @section('scripts')

  <script type="text/javascript">
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
  </script>
  <script type="text/javascript">
    var selectedBts = new Array();
    @foreach($openBts as $key => $row)
    @php echo 'selectedBts['.$key.'] = "'.$row->BTSCode.'";'; @endphp
    @endforeach

    var selectedOlt = new Array();
    @foreach($openOlt as $key => $row)
    @php echo 'selectedOlt['.$key.'] = "'.explode('~',$row->code)[0].'";'; @endphp
    @endforeach

    var selectedCustomer = new Array();
    @foreach($openCustomers as $key => $row)
    @php echo 'selectedCustomer['.$key.'] = "'.$row->CustomerCode.'";'; @endphp
    @endforeach

    function do_select_customer()
    {
      var html = '';
        //var list = '';
        $('#customer-item').html(html);
        //$('#customer_id_list').html(list);
        for(var i = 0; i < checkbox_selected.length; i++)
        {
          //list += '<li>' + checkbox_selected[i] + '</li>';
          html += '<input disabled type="hidden" readonly name="CustCode[]" value="'+checkbox_selected[i]+'">';
        }
        //$('#customer_id_list').html(list);
        $('#customer-item').html(html);
        //$('#modalFindCustomer').modal('hide');
      }

      Array.prototype.remove = function() {
        var what, a = arguments, L = a.length, ax;
        while (L && this.length) {
          what = a[--L];
          while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
          }
        }
        return this;
      };

      var checkbox_selected=[];

      $(document).on('change','.checkbox-selected',function(){
        if($(this).prop('checked')==true)
        {
          checkbox_selected.push($(this).val());
        }
        else
        {
          checkbox_selected.remove($(this).val());
        }
        do_select_customer();
      });

      $(document).ready(function() {
        var selected_customer = [];
        $('#open_bts, #olt_host').on('change',function(){
          $.ajax({
            url: '{{route("ticketing.fmi.ticket-open.get-by-olt-bts")}}',
            method: 'POST',
            enctype: 'multipart/form-data',
            async: false,
            data: {
              "_token": "{{ csrf_token() }}",
              "open_bts": $('#open_bts').val(),
              'olt_host': $('#olt_host').val()
            },
            processData: true,
            cache: false,
            beforeSend: function()
            {
              $('#customer_noc_list').html('<tr><td colspan="7"><div class="progress">\
                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">Fetch Data</div>\
                </div></td></tr>'
                );
            },
            success: function(response)
            {
              if(response.status == 'true')
              {
                var html = '';
                $('#customer_noc_list').html(html);
                for(var i = 0; i < response.data.length; i++)
                {
                  html += '<tr>';

                  if(response.data[i].Code)
                  {
                    html += '<td><input disabled name="open_customer[]" type="checkbox" class="checkbox-selected" value="'+response.data[i].Code+'"></td>';
                  }
                  else
                  {
                    html += '<td><input disabled name="open_customer[]" type="checkbox" class="checkbox-selected" value="'+response.data[i].Code+'"></td>';
                  }

                  if(response.data[i].Code)
                  {
                    selected_customer
                    html += '<td>'+response.data[i].Code+'</td>';
                  }
                  else
                  {
                    html += '<td></td>';
                  }

                  if(response.data[i].Name)
                  {
                    selected_customer
                    html += '<td>'+response.data[i].Name+'</td>';
                  }
                  else
                  {
                    html += '<td></td>';
                  }

                  if(response.data[i].ContactPerson)
                  {
                    selected_customer
                    html += '<td>'+response.data[i].ContactPerson+'</td>';
                  }
                  else
                  {
                    html += '<td></td>';
                  }

                  if(response.data[i].Address)
                  {
                    selected_customer
                    html += '<td>'+response.data[i].Address+'</td>';
                  }
                  else
                  {
                    html += '<td></td>';
                  }

                  if(response.data[i].BaseTransmissionStationCode)
                  {
                    html += '<td><i class="fa fa-check"></i></td>';
                  }
                  else
                  {
                    html += '<td></td>';
                  }

                  if(!response.data[i].BaseTransmissionStationCode)
                  {
                    html += '<td><i class="fa fa-check"></i></td>';
                  }
                  else
                  {
                    html += '<td></td>';
                  }

                  html += '</tr>';
                }
                //checkbox_selected = [];
                $('#found-customer').html('Data was found  '+response.data.length);
                $('#customer_noc_list').html(html);
                if(selectedCustomer.length)
                {
                  for(var index = 0; index < selectedCustomer.length; index++)
                  {
                    $('#customer_noc_list').find('tr').each(function(){
                      if($(this).find('td:eq(0)').find('input').val() == selectedCustomer[index])
                      {
                        $(this).find('td:eq(0)').find('input').prop('checked',true);
                      }
                    })
                  }
                }
                $('#customer_noc_list').find('tr').each(function(){
                  if(!$(this).find('td:eq(0)').find('input').prop('checked'))
                  {
                    $(this).hide();
                  }
                })

              }
              else
              {

              }
            },
            error: function(xhr, ajaxOptions, thrownError)
            {

            }
          });
        });

      $('#search_customer').on('click','button',function(){
        $('#customer_noc_list').find('tr').each(function(){
          if($(this).html().toLowerCase().includes($('#search_customer').find('input').val().toLowerCase()))
          {
            //$(this).css('display','block');
            $(this).slideDown();
          }
          else
          {
            $(this).slideUp();
            //$(this).css('display','none');
          }
        })
      })

      // $('#ticket_category').on('change', function() {
      //   if($(this).val() == 'MINOR')
      //   {
      //     $('#div-customer').slideDown();
      //   }
      //   else {
      //     $('#div-customer').slideUp();
      //   }
      // });

      // $('.ticket_category').select2({
      //   'placeholder' : 'Choose One',
      //   'width' : '100%',
      //   tags : true,
      //   'allowClear' : true
      // });

      $('#branch').select2({
        'placeholder' : 'Choose One',
        'width' : '100%',
        tags : true,
        'allowClear' : true
      });

      $('#ResponsibilityDepartmentCode').select2({
        'placeholder' : 'Choose One',
        'width' : '100%',
        tags : true,
        'allowClear' : true
      });

      $('#problemType').select2({
        'placeholder' : 'Choose One',
        'width' : '100%',
        tags : true,
        'allowClear' : true
      });

      // $('#open_customer').select2({
      //   'placeholder' : 'Choose One',
      //   'width' : '100%',
      //   tags : true,
      //   'allowClear' : true
      // });

      $('#open_bts').select2({
        'placeholder' : 'Choose One',
        'width' : '100%',
        tags : true,
        disabled: true
        //'allowClear' : true
      });

      $('#olt_host').select2({
        'placeholder' : 'Choose One',
        'width' : '100%',
        tags : true,
        disabled: true
        //'allowClear' : true
      });

      $('#open_device_type_fo').select2({
        'placeholder' : 'Choose One',
        'width' : '100%',
        tags : true,
        'allowClear' : true
      });

      $('#open_device_type_wll').select2({
        'placeholder' : 'Choose One',
        'width' : '100%',
        tags : true,
        'allowClear' : true
      });

      $('.mitra').select2({
          'placeholder' : 'Choose One',
          'width' : '100%',
          tags : true,
          'allowClear' : true
        });

        $('.team').select2({
          'placeholder' : 'Choose One',
          'width' : '100%',
          tags : true,
          'allowClear' : true
        });
    });

var KTBootstrapDatepicker = function () {
  var arrows;
  if (KTUtil.isRTL()) {
    arrows = {
      leftArrow: '<i class="la la-angle-right"></i>',
      rightArrow: '<i class="la la-angle-left"></i>'
    }
  } else {
    arrows = {
      leftArrow: '<i class="la la-angle-left"></i>',
      rightArrow: '<i class="la la-angle-right"></i>'
    }
  }

      // Private functions
      var demos = function () {
        $('#TransactionDate').datepicker({
          rtl: KTUtil.isRTL(),
          format: 'dd-mm-yyyy',
          todayHighlight: true,
          orientation: "bottom left",
          templates: arrows
        });

        $('.dtp').datepicker({
          rtl: KTUtil.isRTL(),
          format: 'dd-mm-yyyy',
          todayHighlight: true,
          orientation: "bottom left",
          templates: arrows
        });
      }

      return {
        // public functions
        init: function() {
          demos();
        }
      };
    }();

    jQuery(document).ready(function() {
      KTBootstrapDatepicker.init();
      $('#open_bts').val(selectedBts).change();
      $('#olt_host').val(selectedOlt).change();

      // $('.searchCustomer').select2({
      //   placeholder: 'Search Customer',
      //   ajax: {
      //     url: "{{route("ticketing.fmi.ticket-open.get-customer-noc")}}",
      //     dataType: 'json',
      //     delay: 250,s
      //     processResults: function (data) {
      //       return {
      //         results:  $.map(data, function (item) {
      //           return {
      //             text: item.Name,
      //             id: item.Code
      //           }
      //         })
      //       };
      //     },
      //     cache: true
      //   }
      // });
    });
  </script>
  @endsection
