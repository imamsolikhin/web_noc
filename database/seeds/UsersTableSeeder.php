<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      \App\Models\User::create([
        'id'=>'NOC-GLOBAL',
        'company_id'=>'NOC',
        'name'=>'admin',
        'username'=>'admin',
        'email'=> 'admin@gmail.com',
        'password'=>'$2y$10$j2k/ohKZUiJ8.VLrSgijdujJiTzY27PJIqUrWEMtYikyFfmVopfiC',
        'role_id'=>'NOC',
        'active'=>1
      ]);
      \App\Models\Role::create([
        'id'=>'NOC',
        'name'=>'NOC',
        'display_name'=>'Developer',
        'description'=>'Super User By Developer',
        'status'=>1
      ]);
    }
}
