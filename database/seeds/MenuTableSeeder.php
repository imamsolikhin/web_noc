<?php

use Illuminate\Database\Seeder;

class MenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Menu::create(['name'=>'Monitoring','parent_id'=>0,'description'=>'NOC MONITORING','icon'=>1]);
        \App\Models\Menu::create(['name'=>'OLT Management','parent_id'=>0,'description'=>'OLT MANAGEMENT','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Ticketing','parent_id'=>0,'description'=>'Ticketing','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Asset Management','parent_id'=>0,'description'=>'Asset Management','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Master','parent_id'=>0,'description'=>'Master Management','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Setting','parent_id'=>0,'description'=>'Setting Management','icon'=>1]);

        \App\Models\Menu::create(['name'=>'Fiber Optic Client','url'=>'/monitoring/fiber-optic','parent_id'=>1,'description'=>'NOC MONITORING','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Wireless Clients','url'=>'/monitoring/wireless','parent_id'=>1,'description'=>'NOC MONITORING','icon'=>1]);
        \App\Models\Menu::create(['name'=>'BTS Lists','url'=>'/monitoring/bts','parent_id'=>1,'description'=>'NOC MONITORING','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Graphic','url'=>'/monitoring/graphic','parent_id'=>1,'description'=>'NOC MONITORING','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Network Mapping','url'=>'/monitoring/network-map','parent_id'=>1,'description'=>'NOC MONITORING','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Servers Farm','url'=>'/monitoring/server-farm','parent_id'=>1,'description'=>'NOC MONITORING','icon'=>1]);

        \App\Models\Menu::create(['name'=>'Ont','url'=>'/olt/ont','parent_id'=>2,'description'=>'NOC MONITORING','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Brand','url'=>'/olt/brand','parent_id'=>2,'description'=>'OLT MANAGEMENT','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Signal Formula','url'=>'/olt/signal-formula','parent_id'=>2,'description'=>'OLT MANAGEMENT','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Templates','url'=>'/olt/template','parent_id'=>2,'description'=>'OLT MANAGEMENT','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Host','url'=>'/olt/host','parent_id'=>2,'description'=>'OLT MANAGEMENT','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Client Olt','url'=>'/olt/clients','parent_id'=>2,'description'=>'OLT MANAGEMENT','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Client History','url'=>'/olt/clients-history','parent_id'=>2,'description'=>'OLT MANAGEMENT','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Isp','url'=>'/olt/isp','parent_id'=>2,'description'=>'OLT MANAGEMENT','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Customer','url'=>'/olt/customer','parent_id'=>2,'description'=>'OLT MANAGEMENT','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Customer Registration','url'=>'/olt/customer-registration','parent_id'=>2,'description'=>'OLT MANAGEMENT','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Customer Site','url'=>'/olt/customer-site','parent_id'=>2,'description'=>'OLT MANAGEMENT','icon'=>1]);

        \App\Models\Menu::create(['name'=>'Open Bts','url'=>'/ticketing/master/open-bts','parent_id'=>3,'description'=>'Ticketing','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Open Customer','url'=>'/ticketing/master/open-customer','parent_id'=>3,'description'=>'Ticketing','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Device Type FO','url'=>'/ticketing/master/open-device-type-fo','parent_id'=>3,'description'=>'Ticketing','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Device Type FO','url'=>'/ticketing/master/open-device-type-wll','parent_id'=>3,'description'=>'Ticketing','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Open Ticket List','url'=>'/ticketing/fmi/ticket-open','parent_id'=>3,'description'=>'Ticketing','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Task Assigment','url'=>'/ticketing/fmi/task-assignment','parent_id'=>3,'description'=>'Ticketing','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Ticket Status','url'=>'/ticketing/fmi/ticket-status','parent_id'=>3,'description'=>'Ticketing','icon'=>1]);

        \App\Models\Menu::create(['name'=>'Adjustment In','url'=>'/asset-management/adjustment-in','parent_id'=>4,'description'=>'Asset Management','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Adjustment Out','url'=>'/asset-management/adjustment-out','parent_id'=>4,'description'=>'Asset Management','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Warehouse Customer Installation','url'=>'/asset-management/whm-cus-installa','parent_id'=>4,'description'=>'Asset Management','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Warehouse Customer Uninstallation','url'=>'/asset-management/whm-cus-uninstal','parent_id'=>4,'description'=>'Asset Management','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Warehouse External','url'=>'/asset-management/whm-external','parent_id'=>4,'description'=>'Asset Management','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Warehouse Internal','url'=>'/asset-management/whm-internal','parent_id'=>4,'description'=>'Asset Management','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Pop Instaled','url'=>'/asset-management/whm-pop-instaled','parent_id'=>4,'description'=>'Asset Management','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Pop Uninstaled','url'=>'/asset-management/whm-pop-uninstal','parent_id'=>4,'description'=>'Asset Management','icon'=>1]);

        \App\Models\Menu::create(['name'=>'Bandwidth Package','url'=>'/master/bandwidth-package','parent_id'=>5,'description'=>'Master Management','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Base Transmission Station','url'=>'/master/base-transmission-station','parent_id'=>5,'description'=>'Master Management','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Branch','url'=>'/master/branch','parent_id'=>5,'description'=>'Master Management','icon'=>1]);
        \App\Models\Menu::create(['name'=>'City','url'=>'/master/city','parent_id'=>5,'description'=>'Master Management','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Client Type','url'=>'/master/client-type','parent_id'=>5,'description'=>'Master Management','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Country','url'=>'/master/country','parent_id'=>5,'description'=>'Master Management','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Currency','url'=>'/master/currency','parent_id'=>5,'description'=>'Master Management','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Customer ERP','url'=>'/master/customer-erp','parent_id'=>5,'description'=>'Master Management','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Customer NOC','url'=>'/master/customer-noc','parent_id'=>5,'description'=>'Master Management','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Department','url'=>'/master/department','parent_id'=>5,'description'=>'Master Management','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Division','url'=>'/master/division','parent_id'=>5,'description'=>'Master Management','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Island','url'=>'/master/island','parent_id'=>5,'description'=>'Master Management','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Item','url'=>'/master/item','parent_id'=>5,'description'=>'Master Management','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Item Brand','url'=>'/master/item-brand','parent_id'=>5,'description'=>'Master Management','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Item Category','url'=>'/master/item-category','parent_id'=>5,'description'=>'Master Management','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Item Division','url'=>'/master/item-division','parent_id'=>5,'description'=>'Master Management','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Item Sub Category','url'=>'/master/item-sub-category','parent_id'=>5,'description'=>'Master Management','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Reason','url'=>'/master/reason','parent_id'=>5,'description'=>'Master Management','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Job Position','url'=>'/master/job-position','parent_id'=>5,'description'=>'Master Management','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Mitra','url'=>'/master/mitra','parent_id'=>5,'description'=>'Master Management','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Problem Type','url'=>'/master/problem-type','parent_id'=>5,'description'=>'Master Management','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Product','url'=>'/master/product','parent_id'=>5,'description'=>'Master Management','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Province','url'=>'/master/province','parent_id'=>5,'description'=>'Master Management','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Serial No Detail','url'=>'/master/serial-no-detail','parent_id'=>5,'description'=>'Master Management','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Team','url'=>'/master/team','parent_id'=>5,'description'=>'Master Management','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Unit Of Measure','url'=>'/master/unit-of-measure','parent_id'=>5,'description'=>'Master Management','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Warehouse','url'=>'/master/warehouse','parent_id'=>5,'description'=>'Master Management','icon'=>1]);

        \App\Models\Menu::create(['name'=>'User Management','url'=>'/management/user','parent_id'=>6,'description'=>'Setting Management','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Role & Permission','url'=>'/management/role','parent_id'=>6,'description'=>'Setting Management','icon'=>1]);
        \App\Models\Menu::create(['name'=>'Menu','url'=>'/management/menu','parent_id'=>6,'description'=>'Setting Management','icon'=>1]);
        \App\Models\Menu::create(['name'=>'User Login History Management','url'=>'/management/login-history','parent_id'=>6,'description'=>'Setting Management','icon'=>1]);

        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>1,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>2,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>3,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>4,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>5,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>6,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>7,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>8,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>9,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>10,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>11,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>12,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>13,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>14,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>15,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>16,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>17,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>18,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>19,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>20,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>21,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>22,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>23,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>24,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>25,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>26,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>27,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>28,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>29,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>30,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>31,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>32,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>33,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>34,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>35,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>36,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>37,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>38,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>39,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>40,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>41,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>42,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>43,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>44,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>45,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>46,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>47,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>48,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>49,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>50,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>51,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>52,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>53,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>54,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>55,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>56,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>57,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>58,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>59,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>60,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>61,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>62,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>63,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>64,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);

        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>65,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>66,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>67,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);
        \App\Models\RoleAuth::create(['role_id'=>'NOC','menu_id'=>68,'view'=>1,'create'=>1,'update'=>1,'delete'=>1,'view'=>1]);

    }
}
